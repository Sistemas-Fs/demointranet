<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cbusctramdigesa extends CI_Controller {
	function __construct() {
		parent:: __construct();	
		$this->load->model('ar/tramites/mbusctramdigesa');
		$this->load->model('mglobales');
		$this->load->library('encryption');
		$this->load->helper(array('form','url','download','html','file'));
		$this->load->library('form_validation');
    }
    
   /** TRAMITES DIGESA  **/  
    public function getclientexusu() {	// Visualizar Clientes con propuestas en CBO	
        $idusu= $this->input->post('idusuario');
		$resultado = $this->mbusctramdigesa->getclientexusu($idusu);
		echo json_encode($resultado);
    }
    
    public function getcbomarcaxclie() {	// Visualizar Clientes con propuestas en CBO	
        $ccliente= $this->input->post('ccliente');
		$resultado = $this->mbusctramdigesa->getcbomarcaxclie($ccliente);
		echo json_encode($resultado);
	}
    
    public function getcbotipoprodxentidad() {	// Visualizar Clientes con propuestas en CBO	
        
		$resultado = $this->mbusctramdigesa->getcbotipoprodxentidad();
		echo json_encode($resultado);
	}
    
    public function getcbotramitextipoproducto() {	// Visualizar Clientes con propuestas en CBO	
        $ctipoProducto= $this->input->post('ctipoProducto');
		$resultado = $this->mbusctramdigesa->getcbotramitextipoproducto($ctipoProducto);
		echo json_encode($resultado);
	}

    public function getcaractprodu() {	// Visualizar 	
        $ccliente= $this->input->post('ccliente');
		$resultado = $this->mbusctramdigesa->getcaractprodu($ccliente);
		echo json_encode($resultado);
    }

    public function getconsulta_grid_tr() {	// Recupera Listado de Propuestas	
        		
        $varnull = '';

        $codprod = 		($this->input->post('codprod') == $varnull) ? '%' : '%'.$this->input->post('codprod').'%';
        $nomprod = 		($this->input->post('nomprod') == $varnull) ? '%' : '%'.$this->input->post('nomprod').'%';
        $regsan = 		($this->input->post('regsan') == $varnull) ? '%' : '%'.$this->input->post('regsan').'%';
        $tono = 		($this->input->post('tono') == $varnull) ? '%' : '%'.$this->input->post('tono').'%';
        $estado = 		($this->input->post('estado') == $varnull) ? '%' : '%'.$this->input->post('estado').'%';
        $marca = 		($this->input->post('marca') == $varnull) ? '0' : $this->input->post('marca');
        $tramite = 		($this->input->post('tramite') == $varnull) ? '%' : '%'.$this->input->post('tramite').'%';
        $allf = 		$this->input->post('allf');
        $fini = 		$this->input->post('fi');
        $ffin = 		$this->input->post('ff');
        $ccliente = 	$this->input->post('ccliente'); 
        $numexpdiente = ($this->input->post('numexpdiente') == $varnull) ? '%' : '%'.$this->input->post('numexpdiente').'%';
        $ccategoria = 	($this->input->post('ccategoria') == $varnull) ? '0' : $this->input->post('ccategoria');
        $est = 			($this->input->post('est') == $varnull) ? '%' : '%'.$this->input->post('est').'%';	
        $tipoest = 		($this->input->post('tipoest') == $varnull) ? '%' : $this->input->post('tipoest');
        $tiporeporte = 	'G'; 
        $iln = 		    ($this->input->post('iln') == $varnull) ? '%' : '%'.$this->input->post('iln').'%';
            
        $parametros = array(
            '@codprod' 		=>	$codprod,
            '@nomprod' 		=>  $nomprod,
            '@regsan' 		=>  $regsan,
            '@tono' 		=>  $tono,
            '@estado'		=>  $estado,
            '@marca' 		=>  $marca,
            '@tramite' 		=>  $tramite,
            '@allf' 		=>  $allf,
            '@fi' 			=>  substr($fini, 6, 4).'-'.substr($fini,3 , 2).'-'.substr($fini, 0, 2),
            '@ff' 			=>  substr($ffin, 6, 4).'-'.substr($ffin,3 , 2).'-'.substr($ffin, 0, 2),
            '@ccliente' 	=>  $ccliente,
            '@numexpdiente' =>  $numexpdiente,
            '@ccategoria' 	=>  $ccategoria,		
            '@est' 			=>  $est,
            '@tipoest' 		=>  $tipoest,
            '@TIPOREPORTE'	=>	$tiporeporte,
            '@iln'			=>	$iln
		);		
		$resultado = $this->mbusctramdigesa->getconsulta_grid_tr($parametros);
		echo json_encode($resultado);
	}
    public function getconsulta_excel_tr() {	// Recupera Listado de Propuestas	
        		
        $varnull = '';

        $codprod = 		(empty($this->input->post('codprod'))) ? '%' : '%'.$this->input->post('codprod').'%';
        $nomprod = 		(empty($this->input->post('nomprod'))) ? '%' : '%'.$this->input->post('nomprod').'%';
        $regsan = 		(empty($this->input->post('regsan'))) ? '%' : '%'.$this->input->post('regsan').'%';
        $tono = 		(empty($this->input->post('tono'))) ? '%' : '%'.$this->input->post('tono').'%';
        $estado = 		(empty($this->input->post('estado'))) ? '%' : '%'.$this->input->post('estado').'%';
        $marca = 		(empty($this->input->post('marca'))) ? '%' : '%'.$this->input->post('marca').'%';
        $tramite = 		(empty($this->input->post('tramite'))) ? '%' : '%'.$this->input->post('tramite').'%';
        $allf = 		$this->input->post('allf');
        $fini = 		$this->input->post('fi');
        $ffin = 		$this->input->post('ff');
        $ccliente = 	$this->input->post('ccliente'); 
        $numexpdiente = (empty($this->input->post('numexpdiente'))) ? '%' : '%'.$this->input->post('numexpdiente').'%';
        $ccategoria = 	(empty($this->input->post('ccategoria'))) ? '%' : '%'.$this->input->post('ccategoria').'%';
        $est = 			(empty($this->input->post('est'))) ? '%' : '%'.$this->input->post('est').'%';
        $tipoest = 		(empty($this->input->post('tipoest'))) ? '%' : $this->input->post('tipoest');
        $tiporeporte = 	'E'; 
        $iln = 		    (empty($this->input->post('iln'))) ? '%' : '%'.$this->input->post('iln').'%';
            
        $parametros = array(
            '@codprod' 		=>	$codprod,
            '@nomprod' 		=>  $nomprod,
            '@regsan' 		=>  $regsan,
            '@tono' 		=>  $tono,
            '@estado'		=>  $estado,
            '@marca' 		=>  $marca,
            '@tramite' 		=>  $tramite,
            '@allf' 		=>  $allf,
            '@fi' 			=>  substr($fini, 6, 4).'-'.substr($fini,3 , 2).'-'.substr($fini, 0, 2),
            '@ff' 			=>  substr($ffin, 6, 4).'-'.substr($ffin,3 , 2).'-'.substr($ffin, 0, 2),
            '@ccliente' 	=>  $ccliente,
            '@numexpdiente' =>  $numexpdiente,
            '@ccategoria' 	=>  $ccategoria,		
            '@est' 			=>  $est,
            '@tipoest' 		=>  $tipoest,
            '@TIPOREPORTE'	=>	$tiporeporte,
            '@iln'			=>	$iln
		);		
		$resultado = $this->mbusctramdigesa->getconsulta_excel_tr($parametros);
		echo json_encode($resultado);
	}
    public function getbuscartramite(){
        $parametros['@codaarr'] = $this->input->post('codaarr');
        $parametros['@codrsnso'] = $this->input->post('codrsnso');
        $parametros['@codprod'] = $this->input->post('codprod');
        
        $resultado = $this->mbusctramdigesa->getbuscartramite($parametros);
        echo json_encode($resultado);
    }
    
    public function getbuscartramiteHist(){
        $parametros['@csumario'] = $this->input->post('csumario');
        
        $resultado = $this->mbusctramdigesa->getbuscartramiteHist($parametros);
        echo json_encode($resultado);
    }
    public function getdocum_aarr(){
        $parametros['@casuntoregula'] = $this->input->post('casuntoregula');
        $parametros['@centidad'] = $this->input->post('centidad');
        $parametros['@ctramite'] = $this->input->post('ctramite');
        $parametros['@csumario'] = $this->input->post('csumario');
        
        $resultado = $this->mbusctramdigesa->getdocum_aarr($parametros);

        if ($resultado && !empty($resultado)) {
        	foreach ($resultado as $key => $value) {
				$archivos = [];
				if (!empty($value->DUBICACIONFILESERVER)) {
					$archivos[] = $value->DUBICACIONFILESERVER;
				}
        		$otrosArchivos = $this->mbusctramdigesa->getArchivos($value->casuntoregulatorio, $value->centidadregula, $value->ctramite, $value->cdocumento);
				$archivos = array_merge($archivos, $otrosArchivos);
        		$resultado[$key]->archivos = $archivos;
			}
		}

        echo json_encode($resultado);
    }	

    public function setregproducto() {	// Visualizar Inspectores en CBO
		$varnull = '';
		
		$cproductofs    = $this->input->post('mhdncproductofs');	
		$codigoprod     = $this->input->post('mhdnmantCodigoprod');	
		$codformula     = null;	
		$nombprod       = $this->input->post('mhdnmantNombprod');	
        $modeloprod     = $this->input->post('mhdnmantModeloprod');
        $cboestado     = $this->input->post('cboestado');
        $cboestado = (empty($cboestado)) ? 'A' : $cboestado;
        
        $parametros = array(
            '@cproductofs'  => $cproductofs,
            '@codigoprod'   => $codigoprod,
            '@codformula'   => $codformula,
            '@nombprod'     => $nombprod,
            '@modeloprod'   => $modeloprod,
            '@cboestado'   => $cboestado,
        );
		$resultado = $this->mbusctramdigesa->setregproducto($parametros);

		echo json_encode($resultado);
    }

    public function sethistorico() {	// Visualizar Inspectores en CBO
        $varnull = '';
        
        $Femi       = $this->input->post('mtxtFemi');
        $Fvenci     = $this->input->post('mtxtFvenci');
		
		$CSUMARIO               = $this->input->post('mhdcsumario');	
		$FVENCIMIENTOREGISTRO   = ($this->input->post('mtxtFemi') == '%') ? NULL : substr($Femi, 6, 4).'-'.substr($Femi,3 , 2).'-'.substr($Femi, 0, 2); 	
		$FEMISIONREGISTRO       = ($this->input->post('mtxtFvenci') == '%') ? NULL : substr($Fvenci, 6, 4).'-'.substr($Fvenci,3 , 2).'-'.substr($Fvenci, 0, 2);  
		$DNUMEROREGISTRO        = $this->input->post('mtxtnrors');	
		$DNUMEROEXPEDIENTE      = $this->input->post('mtxtnroexpe');
        
        $parametros = array(
            'CSUMARIO'             => $CSUMARIO,
            'FVENCIMIENTOREGISTRO' => $FVENCIMIENTOREGISTRO,
            'FEMISIONREGISTRO'     => $FEMISIONREGISTRO,
            'DNUMEROREGISTRO'      => $DNUMEROREGISTRO,
            'DNUMEROEXPEDIENTE'    => $DNUMEROEXPEDIENTE,
        );
		$resultado = $this->mbusctramdigesa->sethistorico($parametros);

		echo json_encode($resultado);
    }

    public function getpaises(){
        
        $resultado = $this->mglobales->getpaises();
        echo json_encode($resultado);
    }

    public function archivo_historico() {	// Subir Acrhivo detalle propuesta
        $CCLIENTE = $this->input->post('mhdcclie');
        $RUTAARCH       = 'historico/'.$CCLIENTE.'/';

        !is_dir($RUTAARCH) && @mkdir($RUTAARCH, 0777, true);
		
		$data = array();
		$cpt = count($_FILES['mtxtDetArchivohist']['name']);

		for($i=0; $i<$cpt; $i++)
		{
			$_FILES['userFile']['name']= $_FILES['mtxtDetArchivohist']['name'][$i];
			$_FILES['userFile']['type']= $_FILES['mtxtDetArchivohist']['type'][$i];
			$_FILES['userFile']['tmp_name']= $_FILES['mtxtDetArchivohist']['tmp_name'][$i];
			$_FILES['userFile']['error']= $_FILES['mtxtDetArchivohist']['error'][$i];
			$_FILES['userFile']['size']= $_FILES['mtxtDetArchivohist']['size'][$i]; 

			//RUTA DONDE SE GUARDAN LOS FICHEROS
			$config['upload_path']      = $RUTAARCH;
			$config['allowed_types']    = 'pdf|xlsx|docx|xls|doc';
			$config['max_size']         = '60048';
			$config['remove_spaces']    = FALSE;
            $config['overwrite'] 		= TRUE;
            $config['file_name']        = $_FILES['userFile']['name'];
			
			$this->load->library('upload',$config);
			$this->upload->initialize($config);

			if ($this->upload->do_upload('userFile')) {
				$fileData =  $this->upload->data(); 
				$data = array("upload_data" => $this->upload->data());
				$datos = array(
                    "csumario" => $_POST['mhdcsumario'],
					"ruta" => $RUTAARCH,
					"nombarchivo" => $data['upload_data']['file_name']
				);

				if ($this->mbusctramdigesa->guardar_multiarchivohist($datos)) {
					//echo "Registro guardado";
					$info[$i] = array(
						"archivo" => $data['upload_data']['file_name'],
						"mensaje" => "Archivo subido y guardado"
					);
				}
				else{
					//echo "Error al intentar guardar la informacion";
					$info[$i] = array(
						"archivo" => $data['upload_data']['file_name'],
						"mensaje" => "Archivo subido pero no guardado "
					);
				}				
			}else{
				$info[$i] = array(
					"archivo" => $_FILES['archivo']['name'],
					"mensaje" => "Archivo no subido ni guardado"
				);
			}	
		}

		$envio = "";
		foreach ($info as $key) {
			$envio .= "Archivo : ".$key['archivo']." - ".$key["mensaje"]."\n";
		}
		echo json_encode($envio);
	}
    
    

}
?>

