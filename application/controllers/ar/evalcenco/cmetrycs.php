<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class cmetrycs
 *
 * @property cmetrycs cmetrycs
 */
class cmetrycs extends FS_Controller
{
	/**
	 * cmetrycs constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('ar/evalcenco/mmetrycs', 'mmetrycs');
	}

    public function getDistri()
    {
        $anio = $this->input->post('anio');
        $mes = $this->input->post('mes');

        $resultadoA = $this->mmetrycs->getDistriAprobados($anio,$mes);
        $resultadoO = $this->mmetrycs->getDistriObservados($anio,$mes);
        $resultadoT = $this->mmetrycs->getDistriTruncos($anio,$mes);
        $resultado = ['Aprobados'=>$resultadoA,'Observados'=>$resultadoO,'Truncos'=>$resultadoT];
        echo json_encode($resultado);
    }

    public function getSoli()
    {
        $anio = $this->input->post('anio');

        $resultado = $this->mmetrycs->getSoliTotal($anio);
        //$resultado = ['Aprobados'=>$resultadoA,'Observados'=>$resultadoO,'Truncos'=>$resultadoT];
        echo json_encode($resultado);
    }

}