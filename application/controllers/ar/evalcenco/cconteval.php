<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class cconteval
 *
 * @property cconteval cconteval
 */
class cconteval extends FS_Controller
{
	/**
	 * cconteval constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('ar/evalcenco/mceneval', 'mceneval');
	}

    public function buscar_evaluaciones()
    {
        if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {
            $codigo_eval = $this->input->post('filtro_codigo_eval');
            $codigo_rs = $this->input->post('filtro_codigo_rs');
            $estado_eval = $this->input->post('filtro_estado_eval');

            $listadoEval = $this->mceneval->buscarEvaluaciones($codigo_eval,$codigo_rs,$estado_eval);
            
        	$this->result['status'] = 200;
			//$this->result['message'] = "Producto fue creado correctamente.";
            $this->result['data'] = [
				'listadoEval' => $listadoEval
			];
		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
    }

    public function guardarProducto()
    {
        if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {
            $ideval =$this->input->post('id_eval');
			$codigoProducto = (string) $this->input->post('codigo_producto');
			$categoria = $this->input->post('categoria_id');
			$regsanitario = (string) $this->input->post('producto_rs');
			$fechaemision = (string) $this->input->post('producto_fecha_inicio');
            $fechavencimiento = (string) $this->input->post('producto_fecha_vencimiento');
            $descripcion = (string) $this->input->post('producto_descripcion');
            $nombreProducto = (string) $this->input->post('producto_nombre');
            $marca = $this->input->post('marca_id');
            $presentacion = (string) $this->input->post('producto_presentacion');
            $fabricante = $this->input->post('fabricante_id');
            $pais = $this->input->post('producto_pais');
            $direccionfabricante = (string) $this->input->post('direccion_fabricante');
            $vidautil = (string) $this->input->post('vida_util');
            $estado = $this->input->post('producto_estado');

			$dataProducto = $this->mceneval->crearProducto(
                $ideval,
                $codigoProducto,
                $categoria,
                $regsanitario,
                $fechaemision,
                $fechavencimiento,
                $descripcion,
                $nombreProducto,
                $marca,
                $presentacion,
                $fabricante,
                $pais,
                $direccionfabricante,
                $vidautil,
                $estado
                );
			$this->result['status'] = 200;
			$this->result['message'] = "Producto fue creado correctamente.";
            $this->result['data'] = [
				'dataProducto' => $dataProducto
			];
		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
    }

    public function guardarEval()
    {
        if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {
			$fecha_inicio = (string) $this->input->post('fecha_inicio');
			$estado_eval = $this->input->post('estado_eval');
            $codigo_eval = $this->input->post('codigo_eval');
            $cerrar = $this->input->post('cerrar');

            if(!empty($codigo_eval)){
                $dataEval = $this->mceneval->actualizarEval(
                    $fecha_inicio,
                    $estado_eval,
                    $codigo_eval,
                    $cerrar
                    );
                $this->result['message'] = "Evaluación fue actualizado correctamente.";
            }else{
                $dataEval = $this->mceneval->crearEval(
                    $fecha_inicio,
                    $estado_eval
                    );
                $this->result['message'] = "Evaluación fue creado correctamente.";
            }

			$this->result['status'] = 200;
			$this->result['data'] = [
				'dataEval' => $dataEval
			];
		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
    }

    public function cargar_Evaluacion()
    {
        if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {
            $codigoeval = $_POST['codigoEval'];
            $queryEval = $this->mceneval->buscarEvalxID($codigoeval);
            $dataEval = $queryEval[0];
            $queryProducto = $this->mceneval->buscarProductoxID($dataEval->IDPRODUCTO);
            $dataProducto = $queryProducto[0];
            $queryDocumentos = $this->mceneval->buscarDocumentosxProducto($dataProducto->IDPRODUCTO);
            $data_ad = $this->mceneval->datos_adicionales($dataProducto->CATEGORIA,$dataProducto->MARCA,$dataProducto->FABRICANTE,$dataProducto->PAISFABRICANTE);
            $data = [
                'FECHAINICIOEVAL' => $dataEval->FECHAINICIO,
                'FECHACIERREEVAL' => $dataEval->FECHACIERRE,
                'IDEVAL' => $dataEval->IDEVAL,
                'ESTADOEVAL' => $dataEval->ESTADO,
                'IDPRODUCTO' =>$dataProducto->IDPRODUCTO,
                'CODIGOPRODUCTO' =>$dataProducto->CODIGOPRODUCTO,
                'CATEGORIA' => $dataProducto->CATEGORIA,
                'REGSANITARIO' => $dataProducto->REGSANITARIO,
                'FECHAEMISION' => $dataProducto->FECHAEMISION,
                'FECHAVENCIMIENTO' => $dataProducto->FECHAVENCIMIENTO,
                'DESCRIPCION' => $dataProducto->DESCRIPCION,
                'NOMBREPRODUCTO' => $dataProducto->NOMBREPRODUCTO,
                'MARCA' => $dataProducto->MARCA,
                'PRESENTACION' => $dataProducto->PRESENTACION,
                'DIRECCIONFABRICANTE' => $dataProducto->DIRECCIONFABRICANTE,
                'FABRICANTE' => $dataProducto->FABRICANTE,
                'PAISFABRICANTE' => $dataProducto->PAISFABRICANTE,
                'VIDAUTIL' => $dataProducto->VIDAUTIL,
                'ESTADOPRODUCTO' => $dataProducto->ESTADO,
                'NOMBRECATEGORIA' =>  $data_ad['NOMBRECATEGORIA'],
                'NOMBREMARCA' => $data_ad['NOMBREMARCA'],
                'NOMBREFABRICANTE' => $data_ad['NOMBREFABRICANTE'],
                'NOMBREPAIS' => $data_ad['NOMBREPAIS']
            ];
            $this->result['status'] = 200;
			//$this->result['message'] = "Archivo fue subido correctamente.";
			$this->result['data'] = [
				'dataEval' => $data,
                'dataDocumentos' => $queryDocumentos
			];
        } catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
    }

    public function cargar_archivo(){
		try {
            $ideval = $_POST['id_eval_modal'];
            $idproducto = $_POST['id_producto_modal'];
            $idtramite = $_POST['id_tramite'];
            $estado_documento = $_POST['estado_documento'];
            $rutaCarpeta = "/FTPfileserver/Archivos/DocumentosCencosud/";
            if(!file_exists(".".$rutaCarpeta.$ideval)){
                mkdir(".".$rutaCarpeta.$ideval,0777,true);
            }
            $rutaCompleta = $rutaCarpeta.$ideval."/";
            $config = [
                'upload_path'=>".".$rutaCompleta,
                'allowed_types'=>"doc|docx|pdf|xls|xlsx|jpg|jpeg|png"
            ];
            $this->load->library('upload',$config);
            $this->upload->initialize($config);
            if($this->upload->do_upload('documento')){
                $data = array('upload_data' => $this->upload->data());
                $nombre = $this->upload->data('file_name');
                $ruta = $rutaCompleta.$nombre;
                if($estado_documento==0){
                    $estado_documento=1;
                }
                $dataDocumento = $this->mceneval->guardarDocumento($idtramite,$ideval,$idproducto,$nombre,$ruta,$estado_documento);
            }else{
                $error = array('error' => $this->upload->display_errors());
            }
            $this->result['status'] = 200;
			$this->result['message'] = "Archivo fue subido correctamente.";
			$this->result['data'] = [
				'dataDocumento' => $dataDocumento
			];
        } catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
    }

    public function cargarProducto(){
        if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {
            $idproducto = $this->input->post('filter_codigo');
            $rsproducto = $this->input->post('filter_producto');

            $dataProducto = $this->mceneval->cargarProducto($idproducto,$rsproducto);

            $this->result['status'] = 200;
			$this->result['message'] = "Producto fue cargado correctamente.";
            $this->result['data'] = [
				'dataProducto' => $dataProducto
			];
        } catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
    }

    public function opciones_documentos()
    {
        if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {
            $idtramite = (integer)$this->input->post('numeroTramite');
            $ideval = (integer)$this->input->post('ideval');

            $documentos = $this->mceneval->documentos_tramite($ideval,$idtramite);

            $this->result['status'] = 200;
			$this->result['message'] = "Documentos cargados correctamente.";
            $this->result['data'] = [
				'documentos' => $documentos
			];
        } catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
    }

    public function eliminar_documento()
    {
        if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {
            $idtramite = (integer)$this->input->post('numeroTramite');
            $ideval = (integer)$this->input->post('ideval');
            $numeroDoc = (integer)$this->input->post('numeroDocumento');

            $documentos = $this->mceneval->eliminar_documento($ideval,$idtramite,$numeroDoc);

            $this->result['status'] = 200;
			$this->result['message'] = "Documentos cargados correctamente.";
            $this->result['data'] = [
				'documentos' => $documentos
			];
        } catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
    }

    public function cambiar_estado()
    {
        if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {
            $idtramite = (integer)$this->input->post('numeroTramite');
            $ideval = (integer)$this->input->post('ideval');
            $estado = (integer)$this->input->post('estado');

            $documentos = $this->mceneval->cambiar_estado($ideval,$idtramite,$estado);

            $this->result['status'] = 200;
			$this->result['message'] = "Estado Actualizado.";
            $this->result['data'] = [
				'documentos' => $documentos
			];
        } catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
    }

    
}