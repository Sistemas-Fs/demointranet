<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class cconseval
 *
 * @property cconseval cconteval
 */
class cconseval extends FS_Controller
{
	/**
	 * cconteval constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('ar/evalcenco/mconseval', 'mconseval');
	}

    public function buscar(){
        if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {
            $codeval =$this->input->post('codeval');
			$txtnrors = (string) $this->input->post('txtnrors');
			$txtdescprodu = (string) $this->input->post('txtdescprodu');
            $estado_eval = $this->input->post('estado_eval');
			$slc_fabricante = (integer)$this->input->post('slc_fabricante');

			$dataResultados = $this->mconseval->consultarEvaluaciones(
                $codeval,
                $txtnrors,
                $txtdescprodu,
                $estado_eval,
				$slc_fabricante
                );
			$this->result['status'] = 200;
			//$this->result['message'] = "Producto fue creado correctamente.";
            $this->result['data'] = [
				'dataresultados' => $dataResultados
			];
		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
    }

	public function buscarDocumentos(){
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {
			$codigoeval = $_POST['codigoeval'];
			$dataResultado = $this->mconseval->buscarDocumentoxEval($codigoeval);
			$this->result['status'] = 200;
			//$this->result['message'] = "Producto fue creado correctamente.";
            $this->result['data'] = [
				'dataresultados' => $dataResultado
			];
		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

	public function cargarFabricantes(){
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {
			$dataFabricantes = $this->mconseval->autoload_fabricantes();
			$this->result['status'] = 200;
			//$this->result['message'] = "Producto fue creado correctamente.";
            $this->result['data'] = [
				'dataFabricantes' => $dataFabricantes
			];
		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

}