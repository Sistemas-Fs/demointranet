<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class cclieusuarios extends CI_Controller {
	function __construct() {
		parent:: __construct();	
		$this->load->model('pt/mclieusuarios');
		$this->load->model('mglobales');
		$this->load->library('encryption');
		$this->load->helper(array('form','url','download','html','file'));
		$this->load->library('form_validation');
    }    
    public function getbuscarclieusuario() {	// Busqueda de Contratos 	
		$varnull = '';

        $descripcion   = $this->input->post('descripcion');
        
        $parametros = array(
			'@descripcion'		=> ($this->input->post('descripcion') == '') ? '%' : '%'.$descripcion.'%',
        );
        
        $resultado = $this->mclieusuarios->getbuscarclieusuario($parametros);
        echo json_encode($resultado);
    } 
    
    public function setregclieusu() {	// Registrar Vacaciones		
		$varnull = 	'';	

		$id_usuario     = $this->input->post('mhdnidusuario');
		$id_administrado     = $this->input->post('mhdnidadministrado');
		$idtipodoc      = $this->input->post('mhdntipodoc');
		$nrodoc         = $this->input->post('mtxtnrodoc');
		$apepaterno     = $this->input->post('mtxtapepat');
		$apematerno     = $this->input->post('mtxtapemat');
		$nombres        = $this->input->post('mtxtnombres');
		$email          = $this->input->post('mtxtemail');
		$ccliente      = $this->input->post('mcboClie');
		$accion         = $this->input->post('mhdnAccion');

		$parametros = array(
			'@id_usuario'  =>  $id_usuario,
			'@id_administrado'  =>  $id_administrado,
			'@id_tipodoc'   =>  $idtipodoc,
			'@nrodoc'       =>  $nrodoc,
			'@ape_paterno'  =>  $apepaterno,
			'@ape_materno'  =>  $apematerno,
			'@nombres'      =>  $nombres,
			'@email'        =>  $email,
			'@ccliente'    =>  $ccliente,
			'@accion'    	=>  $accion,
		);				
		$respuesta = $this->mclieusuarios->setregclieusu($parametros);
		echo json_encode($respuesta);
	}
	public function setbloquear() {	// Registrar Vacaciones		
		$varnull = 	'';	
	
		$id_usuario     = $this->input->post('id_usuario');
			
		$respuesta = $this->mclieusuarios->setbloquear($id_usuario);
		echo json_encode($respuesta);
	}
	public function setdesbloquear() {	// Registrar Vacaciones		
		$varnull = 	'';	
	
		$id_usuario     = $this->input->post('id_usuario');
			
		$respuesta = $this->mclieusuarios->setdesbloquear($id_usuario);
		echo json_encode($respuesta);
	}
	public function delUsuario(){ 				
		$id_usuario = $this->input->post('id_usuario');	
		$respuesta = $this->mclieusuarios->delUsuario($id_usuario);
		echo json_encode($respuesta);

	}



}
?>