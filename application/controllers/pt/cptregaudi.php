<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cptregaudi extends CI_Controller {
	function __construct() {
		parent:: __construct();	
		$this->load->model('pt/mptregaudi');
		$this->load->model('mglobales');
		$this->load->library('encryption');
		$this->load->helper(array('form','url','download','html','file'));
		$this->load->library('form_validation');
    }
    
   /** CONTROL DE PROVEEDORES **/ 
    public function getbuscarauditoria() {	// Recupera Listado de Informes	
        
		$varnull 			= 	'';

		$fini       = $this->input->post('fdesde');
		$ffin       = $this->input->post('fhasta');
		$dnropropu    = $this->input->post('dnropropu');
		$dnroinfor    = $this->input->post('dnroinfor');
            
        $parametros = array(
			'@ccliente'     => ($this->input->post('ccliente') == $varnull) ? '0' : $this->input->post('ccliente'),
			'@fini'         => ($this->input->post('fdesde') == '%') ? NULL : substr($fini, 6, 4).'-'.substr($fini,3 , 2).'-'.substr($fini, 0, 2),
			'@ffin'         => ($this->input->post('fhasta') == '%') ? NULL : substr($ffin, 6, 4).'-'.substr($ffin,3 , 2).'-'.substr($ffin, 0, 2),
			'@dnropropu'    => ($this->input->post('dnropropu') == $varnull) ? '%' : "%".$dnropropu."%",
			'@dnroinfor'    => ($this->input->post('dnroinfor') == $varnull) ? '%' : "%".$dnroinfor."%",
		);		
		$resultado = $this->mptregaudi->getbuscarauditoria($parametros);
		echo json_encode($resultado);
	}
    public function getclientepropu() {	// Visualizar Clientes con propuestas en CBO	
        
		$resultado = $this->mptregaudi->getclientepropu();
		echo json_encode($resultado);
	}
    public function getpropuevaluar() {	// Obtener numero de propuesta	
        $ccliente = $this->input->post('ccliente');
		$resultado = $this->mptregaudi->getpropuevaluar($ccliente);
		echo json_encode($resultado);
	}
    public function getcboprovxclie() {	// Obtener numero de propuesta	
        $ccliente = $this->input->post('ccliente');
		$resultado = $this->mptregaudi->getcboprovxclie($ccliente);
		echo json_encode($resultado);
	}
    public function getcboregestable() {	// Obtener numero de propuesta	
        $ccliente = $this->input->post('ccliente');
        $cproveedor = $this->input->post('cproveedor');
		$resultado = $this->mptregaudi->getcboregestable($ccliente,$cproveedor);
		echo json_encode($resultado);
	}
    public function getdirestable() {	// Obtener numero de propuesta	
        $cestable = $this->input->post('cestablecimiento');
		$resultado = $this->mptregaudi->getdirestable($cestable);
		echo json_encode($resultado);
	}
	public function getServicioAudi() {	// Visualizar Servicios en CBO	        
	 	$resultado = $this->mptregaudi->getServicioAudi();
	 	echo json_encode($resultado);
	 } 
	 public function setregauditoria() { // Registrar informe PT
		$varnull = '';
		
		$idptevaluacion		= $this->input->post('mhdnIdpteval');
		$idptpropuesta		= $this->input->post('cboRegPropu');
		$idptinforme		= $this->input->post('mhdnIdInfor');
		$nro_informe 		= $this->input->post('mtxtNroinfor');
		$fecha_informe		= $this->input->post('mtxtFinfor');
		$idresponsable		= $this->input->post('mcboContacInfor');
		$idptregauditoria	= $this->input->post('hdnidauditoria');
		$idptservicio		= $this->input->post('cboservicios');
		$descripcion_equipo	= $this->input->post('txtEquipos');
		$descripcion_producto	= $this->input->post('txtProdLinea');
		$comentarios		= $this->input->post('mtxtregcomentario');
		$cproveedor			= $this->input->post('cboregprovclie');
		$cestablecimiento	= $this->input->post('cboregestable');
		$fauditoria			= $this->input->post('txtFAudi');
		$idptregistro		= $this->input->post('mhdnIdptregistro');
		$accion 			= $this->input->post('hdnAccionaudi');
        
        $parametros = array(
            '@idptevaluacion'		=>  $idptevaluacion,
            '@idptpropuesta'		=>  $idptpropuesta,
            '@idptinforme'			=>  $idptinforme,
            '@nro_informe'			=>  $nro_informe,
            '@fecha_informe'		=>  substr($fecha_informe, 6, 4).'-'.substr($fecha_informe,3 , 2).'-'.substr($fecha_informe, 0, 2),
            '@idresponsable'      	=>  $idresponsable,
            '@idptregauditoria'		=>  $idptregauditoria,
            '@idptservicio'			=>  $idptservicio,
            '@descripcion_equipo'	=>  $descripcion_equipo,
            '@descripcion_producto'	=>  $descripcion_producto,
            '@comentarios'			=>  $comentarios,
            '@cproveedor'      		=>  $cproveedor,
            '@cestablecimiento'		=>  $cestablecimiento,
            '@fauditoria'			=>  substr($fauditoria, 6, 4).'-'.substr($fauditoria,3 , 2).'-'.substr($fauditoria, 0, 2),
            '@idptregistro'		=>  $idptregistro,
            '@accion'           	=>  $accion
        );
        $retorna = $this->mptregaudi->setregauditoria($parametros);
        echo json_encode($retorna);		
	}
	public function subirArchivoAudi() {	// Subir Acrhivo 
        $IDINFOR    = $this->input->post('mhdnIdInfor');
        $ANIO       = substr($this->input->post('mtxtFinfor'),-4);
        $NOMBARCH 	= 'INF'.substr($this->input->post('mtxtNroinfor'),0,4).substr($this->input->post('mtxtNroinfor'),5,4).'PTFS';
        
        $RUTAARCH   = 'FTPfileserver/Archivos/10202/'.$ANIO.'/';

        !is_dir($RUTAARCH) && @mkdir($RUTAARCH, 0777, true);

		//RUTA DONDE SE GUARDAN LOS FICHEROS
		$config['upload_path'] 		= $RUTAARCH;
		$config['allowed_types'] 	= 'pdf|xlsx|docx|xls|doc';
		$config['max_size'] 		= '60048';
		$config['overwrite'] 		= TRUE;
		$config['file_name']        = $NOMBARCH;
		
		$this->load->library('upload',$config);
		$this->upload->initialize($config);
		
		if (!($this->upload->do_upload('mtxtArchivoaudi'))) {
			//si al subirse hay algun error 
			$data['uploadError'] = $this->upload->display_errors();
			$error = '';
			return $error;					
		}else{
			$data = $this->upload->data();
			$parametros = array(
                '@idptinforme'   	=>  $IDINFOR,
                '@archivo_informe'	=>  $data['file_name'],
                '@ruta_informe'		=>  $config['upload_path'],
                '@descripcion_archivo'	=>  $this->input->post('mtxtNomarchaudi'),
            );
            $retorna = $this->mptregaudi->subirArchivoAudi($parametros);
            echo json_encode($retorna);
		}	
	}

	public function delauditoria() {	// Eliminar de informe	
		$idptauditoria = $this->input->post('idptauditoria');	
		$idptinforme = $this->input->post('idptinforme');	
		$resultado = $this->mptregaudi->delauditoria($idptauditoria, $idptinforme);
		echo json_encode($resultado);
	}

}
?>