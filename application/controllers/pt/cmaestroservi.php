<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cmaestroservi extends CI_Controller {
	function __construct() {
		parent:: __construct();	
		$this->load->model('pt/mservcliente');
		$this->load->model('mglobales');
		$this->load->helper(array('form','url','download','html','file'));
		$this->load->library('form_validation');
    }
    
    public function viewmapter($ccliente) { // MAPEO TÉRMICO		
		
        $this->layout->js(array(public_url('script/pt/servcliente/servcliemapter.js')));
        
        $data['vista'] = 'Dpt';
        $data['ccliente'] = $ccliente;
		$data['content_for_layout'] = 'pt/servcliente/vptservcliemapter';
        $this->parser->parse('seguridad/vprincipal',$data);
    }    
    public function viewcalfrio($ccliente) { // LLENADO EN CALIENTE - FRIO		
		
		$this->layout->js(array(public_url('script/pt/servcliente/servcliecalfrio.js')));

        $data['vista'] = 'Dpt';
        $data['ccliente'] = $ccliente;
		$data['content_for_layout'] = 'pt/servcliente/vptservcliecalfrio';
        $this->parser->parse('seguridad/vprincipal',$data);
    }    
    public function viewcocsechor($ccliente) { // COCINADOR-SECADOR-HORNO		
		
		$this->layout->js(array(public_url('script/pt/servcliente/servcliecocsechor.js')));

        $data['vista'] = 'Dpt';
        $data['ccliente'] = $ccliente;
		$data['content_for_layout'] = 'pt/servcliente/vptservcliecocsechor';
        $this->parser->parse('seguridad/vprincipal',$data);
    }    
    public function viewevaldesvi($ccliente) { // EVALUACIÓN DE DESVIACIONES		
		
		$this->layout->js(array(public_url('script/pt/servcliente/servclieevaldesvi.js')));

        $data['vista'] = 'Dpt';
        $data['ccliente'] = $ccliente;
		$data['content_for_layout'] = 'pt/servcliente/vptservclieevaldesvi';
        $this->parser->parse('seguridad/vprincipal',$data);
    }    
    public function viewproacep($ccliente) { // PROCESAMIENTO ASÉPTICO		
		
		$this->layout->js(array(public_url('script/pt/servcliente/servclieproasep.js')));

        $data['vista'] = 'Dpt';
        $data['ccliente'] = $ccliente;
		$data['content_for_layout'] = 'pt/servcliente/vptservclieproacep';
        $this->parser->parse('seguridad/vprincipal',$data);
	}	
    public function viewproconv($ccliente) { // PROCESAMIENTO CONVENCIONAL		
		
		$this->layout->js(array(public_url('script/pt/servcliente/servclieproconv.js')));

        $data['vista'] = 'Dpt';
        $data['ccliente'] = $ccliente;
		$data['content_for_layout'] = 'pt/servcliente/vptservclieproconv';
        $this->parser->parse('seguridad/vprincipal',$data);
    }

    public function getmapterequipo() {	// Obtener equipo mapeo termico	
		
		$parametros = array(
            '@ccliente'   => $this->input->post('ccliente')
        );
		$resultado = $this->mservcliente->getmapterequipo($parametros);
		echo json_encode($resultado);
	}
    public function getmapterrecinto() {	// Obtener recinto mapeo termico	
		
		$parametros = array(
            '@ccliente'   => $this->input->post('ccliente')
        );
		$resultado = $this->mservcliente->getmapterrecinto($parametros);
		echo json_encode($resultado);
	}
    public function getmapterestudio() {	// Obtener estudio mapeo termico	
		
		$parametros = array(
            '@ccliente'   => $this->input->post('ccliente')
        );
		$resultado = $this->mservcliente->getmapterestudio($parametros);
		echo json_encode($resultado);
	}
    public function getmapterproducto() {	// Obtener producto mapeo termico	
		
		$parametros = array(
            '@ccliente'   => $this->input->post('ccliente')
        );
		$resultado = $this->mservcliente->getmapterproducto($parametros);
		echo json_encode($resultado);
	}

    public function getproconvequipo() {	// Obtener numero de propuesta	
		
		$parametros = array(
            '@ccliente'   => $this->input->post('ccliente')
        );
		$resultado = $this->mservcliente->getproconvequipo($parametros);
		echo json_encode($resultado);
	}
    public function getproconvproducto() {	// Obtener numero de propuesta	
		
		$parametros = array(
            '@CCLIENTE'   => $this->input->post('ccliente'),
        );
		$resultado = $this->mservcliente->getproconvproducto($parametros);
		echo json_encode($resultado);
	}

    public function getcalfrioproducto() {	// Obtener numero de propuesta	
		
		$parametros = array(
            '@ccliente'   => $this->input->post('ccliente')
        );
		$resultado = $this->mservcliente->getcalfrioproducto($parametros);
		echo json_encode($resultado);
	}
    public function getcalfrioequipo() {	// Obtener numero de propuesta	
		
		$parametros = array(
            '@ccliente'   => $this->input->post('ccliente')
        );
		$resultado = $this->mservcliente->getcalfrioequipo($parametros);
		echo json_encode($resultado);
	}
	
    public function getproasepproducto() {	// Obtener numero de propuesta	
		
		$parametros = array(
            '@ccliente'   => $this->input->post('ccliente')
        );
		$resultado = $this->mservcliente->getproasepproducto($parametros);
		echo json_encode($resultado);
	}
	
    public function getevaldesviestudio() {	// Obtener estudio mapeo termico	
		
		$parametros = array(
            '@ccliente'   => $this->input->post('ccliente')
        );
		$resultado = $this->mservcliente->getevaldesviestudio($parametros);
		echo json_encode($resultado);
	}
	
    public function getcocsechorequipo() {	// Obtener estudio mapeo termico	
		
		$parametros = array(
            '@ccliente'   => $this->input->post('ccliente')
        );
		$resultado = $this->mservcliente->getcocsechorequipo($parametros);
		echo json_encode($resultado);
	}
	
    public function getcocsechorrecinto() {	// Obtener estudio mapeo termico	
		
		$parametros = array(
            '@ccliente'   => $this->input->post('ccliente')
        );
		$resultado = $this->mservcliente->getcocsechorrecinto($parametros);
		echo json_encode($resultado);
	}
}
?>