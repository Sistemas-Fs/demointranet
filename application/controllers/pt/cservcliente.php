<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Style\Conditional;
use PhpOffice\PhpSpreadsheet\IOFactory;

class Cservcliente extends CI_Controller {
	function __construct() {
		parent:: __construct();	
		$this->load->model('pt/mservcliente');
		$this->load->model('mglobales');
		$this->load->helper(array('form','url','download','html','file'));
		$this->load->library('form_validation');
    }
    
    public function viewmapter() { // MAPEO TÉRMICO		
		
		$this->layout->js(array(public_url('script/pt/servcliente/servcliemapter.js')));

		$data['content_for_layout'] = 'pt/servcliente/vservcliemapter';
        $this->parser->parse('seguridad/vprincipalClie',$data);
    }    
    public function viewcalfrio() { // LLENADO EN CALIENTE - FRIO		
		
		$this->layout->js(array(public_url('script/pt/servcliente/servcliecalfrio.js')));

		$data['content_for_layout'] = 'pt/servcliente/vservcliecalfrio';
        $this->parser->parse('seguridad/vprincipalClie',$data);
    }    
    public function viewcocsechor() { // COCINADOR-SECADOR-HORNO		
		
		$this->layout->js(array(public_url('script/pt/servcliente/servcliecocsechor.js')));

		$data['content_for_layout'] = 'pt/servcliente/vservcliecocsechor';
        $this->parser->parse('seguridad/vprincipalClie',$data);
    }    
    public function viewevaldesvi() { // EVALUACIÓN DE DESVIACIONES		
		
		$this->layout->js(array(public_url('script/pt/servcliente/servclieevaldesvi.js')));

		$data['content_for_layout'] = 'pt/servcliente/vservclieevaldesvi';
        $this->parser->parse('seguridad/vprincipalClie',$data);
    }    
    public function viewproacep() { // PROCESAMIENTO ASÉPTICO		
		
		$this->layout->js(array(public_url('script/pt/servcliente/servclieproasep.js')));

		$data['content_for_layout'] = 'pt/servcliente/vservclieproacep';
        $this->parser->parse('seguridad/vprincipalClie',$data);
	}	
    public function viewproconv() { // PROCESAMIENTO CONVENCIONAL		
		
		$this->layout->js(array(public_url('script/pt/servcliente/servclieproconv.js')));

		$data['content_for_layout'] = 'pt/servcliente/vservclieproconv';
        $this->parser->parse('seguridad/vprincipalClie',$data);
    }

    public function getmapterequipo() {	// Obtener equipo mapeo termico	
		
		$parametros = array(
            '@ccliente'   => $this->input->post('ccliente')
        );
		$resultado = $this->mservcliente->getmapterequipo($parametros);
		echo json_encode($resultado);
	}
    public function getmapterrecinto() {	// Obtener recinto mapeo termico	
		
		$parametros = array(
            '@ccliente'   => $this->input->post('ccliente')
        );
		$resultado = $this->mservcliente->getmapterrecinto($parametros);
		echo json_encode($resultado);
	}
    public function getmapterestudio() {	// Obtener estudio mapeo termico	
		
		$parametros = array(
            '@ccliente'   => $this->input->post('ccliente')
        );
		$resultado = $this->mservcliente->getmapterestudio($parametros);
		echo json_encode($resultado);
	}
    public function getmapterproducto() {	// Obtener producto mapeo termico	
		
		$parametros = array(
            '@ccliente'   => $this->input->post('ccliente')
        );
		$resultado = $this->mservcliente->getmapterproducto($parametros);
		echo json_encode($resultado);
	}

    public function getproconvequipo() {	// Obtener numero de propuesta	
		
		$parametros = array(
            '@ccliente'   => $this->input->post('ccliente')
        );
		$resultado = $this->mservcliente->getproconvequipo($parametros);
		echo json_encode($resultado);
	}
    public function getproconvproducto() {	// Obtener numero de propuesta	
		
		$parametros = array(
            '@CCLIENTE'   => $this->input->post('ccliente'),
        );
		$resultado = $this->mservcliente->getproconvproducto($parametros);
		echo json_encode($resultado);
	}

    public function getcalfrioproducto() {	// Obtener numero de propuesta	
		
		$parametros = array(
            '@ccliente'   => $this->input->post('ccliente')
        );
		$resultado = $this->mservcliente->getcalfrioproducto($parametros);
		echo json_encode($resultado);
	}
    public function getcalfrioequipo() {	// Obtener numero de propuesta	
		
		$parametros = array(
            '@ccliente'   => $this->input->post('ccliente')
        );
		$resultado = $this->mservcliente->getcalfrioequipo($parametros);
		echo json_encode($resultado);
	}
	
    public function getproasepproducto() {	// Obtener numero de propuesta	
		
		$parametros = array(
            '@ccliente'   => $this->input->post('ccliente')
        );
		$resultado = $this->mservcliente->getproasepproducto($parametros);
		echo json_encode($resultado);
	}
	
    public function getevaldesviestudio() {	// Obtener estudio mapeo termico	
		
		$parametros = array(
            '@ccliente'   => $this->input->post('ccliente')
        );
		$resultado = $this->mservcliente->getevaldesviestudio($parametros);
		echo json_encode($resultado);
	}
	
    public function getcocsechorequipo() {	// Obtener estudio mapeo termico	
		
		$parametros = array(
            '@ccliente'   => $this->input->post('ccliente')
        );
		$resultado = $this->mservcliente->getcocsechorequipo($parametros);
		echo json_encode($resultado);
	}
	
    public function getcocsechorrecinto() {	// Obtener estudio mapeo termico	
		
		$parametros = array(
            '@ccliente'   => $this->input->post('ccliente')
        );
		$resultado = $this->mservcliente->getcocsechorrecinto($parametros);
		echo json_encode($resultado);
	}
	
	public function excelproconvequipo() {
	   /*Estilos */
		$titulo = [
			'font'	=> [
				'name' => 'Arial',
				'size' =>12,
				'color' => array('rgb' => 'FFFFFF'),
				'bold' => true,
			], 
			'fill'	=>[
				'fillType' => Fill::FILL_SOLID,
				'startColor' => [
					'rgb' => '29B037'
				]
			],
			'borders'	=>[
				'allBorders' => [
					'borderStyle' => Border::BORDER_THIN,
					'color' => [ 
						'rgb' => '000000'
					]
				]
			],
			'alignment' => [
				'horizontal' => Alignment::HORIZONTAL_CENTER,
				'vertical' => Alignment::VERTICAL_CENTER,
				'wrapText' => true,
			],
		];
        $cabecera = [
            'font'	=> [
                'name' => 'Arial',
                'size' =>10,
                'color' => array('rgb' => 'FFFFFF'),
                'bold' => true,
            ], 
            'fill'	=>[
                'fillType' => Fill::FILL_SOLID,
                'startColor' => [
                    'rgb' => '29B037'
                ]
            ],
            'borders'	=>[
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => [ 
                        'rgb' => '000000'
                    ]
                ]
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ];
        $celdastexto = [
            'borders'	=>[
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => [ 
                        'rgb' => '000000'
                    ]
                ]
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_LEFT,
                'vertical' => Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ];
        $celdasnumero = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_RIGHT,
                'vertical' => Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ];
        $celdascentro = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ];
	   /*Estilos */	
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setTitle('Listado - Equipo');

        $spreadsheet->getDefaultStyle()
            ->getFont()
            ->setName('Arial')
            ->setSize(9);
		
		$sheet->setCellValue('A1', 'Listado de Equipos - Proceso Convencional')
			->mergeCells('A1:I1')
			->setCellValue('A3', '#')
			->setCellValue('B3', 'Nro Informe')
			->setCellValue('C3', 'Medio de Calentamiento')
			->setCellValue('D3', 'Fabricante')
			->setCellValue('E3', 'Envase')
			->setCellValue('F3', 'Dimensiones')
			->setCellValue('G3', 'ID Estudio')
			->setCellValue('H3', 'Nro Equipos')
			->setCellValue('I3', 'Nro Canastillas');

		$sheet->getStyle('A1:I1')->applyFromArray($titulo);
        $sheet->getStyle('A3:I3')->applyFromArray($cabecera);

		$sheet->getColumnDimension('A')->setAutoSize(false)->setWidth(4.10);
		$sheet->getColumnDimension('B')->setAutoSize(false)->setWidth(20.10);
		$sheet->getColumnDimension('C')->setAutoSize(false)->setWidth(14.10);
		$sheet->getColumnDimension('D')->setAutoSize(false)->setWidth(41.10);
		$sheet->getColumnDimension('E')->setAutoSize(false)->setWidth(34.10);
		$sheet->getColumnDimension('F')->setAutoSize(false)->setWidth(45.10);
		$sheet->getColumnDimension('G')->setAutoSize(false)->setWidth(45.10);
		$sheet->getColumnDimension('H')->setAutoSize(false)->setWidth(20.10);
		$sheet->getColumnDimension('I')->setAutoSize(false)->setWidth(20.10);
		

		$varnull 			= 	'';

		$ccliente   = $this->input->post('hdnccliente');
            
        $parametros = array(
			'@ccliente'     => $ccliente,
		);		
		$rpt = $this->mservcliente->getproconvequipo($parametros);
		$i = 1;
		$irow = 4;
        if ($rpt){
        	foreach($rpt as $row){
            
				$NROINFOR = $row->NROINFOR;
				$MEDIOCAL = $row->MEDIOCAL;
				$FABRI = $row->FABRI;
				$ENVASE = $row->ENVASE;
				$DIMENSION = $row->DIMENSION;
				$IDENTIF = $row->IDENTIF;
				$NROEQUIPO = $row->NROEQUIPO;
				$NROCANAS = $row->NROCANAS;

				$sheet->setCellValue('A'.$irow,$i);
				$sheet->setCellValue('B'.$irow,$NROINFOR);
				$sheet->setCellValue('C'.$irow,$MEDIOCAL);
				$sheet->setCellValue('D'.$irow,$FABRI);
				$sheet->setCellValue('E'.$irow,$ENVASE);
				$sheet->setCellValue('F'.$irow,$DIMENSION);
				$sheet->setCellValue('G'.$irow,$IDENTIF);
				$sheet->setCellValue('H'.$irow,$NROEQUIPO);
				$sheet->setCellValue('I'.$irow,$NROCANAS);

				$i++;
				$irow++;
			}
		}
		$pos = $irow - 1;
		$sheet->getStyle('A4:I'.$pos)->applyFromArray($celdastexto);
		//$sheet->getStyle('A4:A'.$pos)->applyFromArray($celdasnumero);
		//$sheet->getStyle('H4:H'.$pos)->applyFromArray($celdasnumero);
		//$sheet->getStyle('C4:C'.$pos)->applyFromArray($celdascentro);
		//$sheet->getStyle('E4:E'.$pos)->applyFromArray($celdascentro);

		$sheet->setAutoFilter('B3:I'.$pos);
		
                   
		$writer = new Xlsx($spreadsheet);
		$filename = 'listEquipoProcConve-'.time().'.xlsx';
		ob_end_clean();
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
		header('Cache-Control: max-age=0');

		$writer->save('php://output');
	}
	public function excelproconvproducto() {
	   /*Estilos */
		$titulo = [
			'font'	=> [
				'name' => 'Arial',
				'size' =>12,
				'color' => array('rgb' => 'FFFFFF'),
				'bold' => true,
			], 
			'fill'	=>[
				'fillType' => Fill::FILL_SOLID,
				'startColor' => [
					'rgb' => '29B037'
				]
			],
			'borders'	=>[
				'allBorders' => [
					'borderStyle' => Border::BORDER_THIN,
					'color' => [ 
						'rgb' => '000000'
					]
				]
			],
			'alignment' => [
				'horizontal' => Alignment::HORIZONTAL_CENTER,
				'vertical' => Alignment::VERTICAL_CENTER,
				'wrapText' => true,
			],
		];
        $cabecera = [
            'font'	=> [
                'name' => 'Arial',
                'size' =>10,
                'color' => array('rgb' => 'FFFFFF'),
                'bold' => true,
            ], 
            'fill'	=>[
                'fillType' => Fill::FILL_SOLID,
                'startColor' => [
                    'rgb' => '29B037'
                ]
            ],
            'borders'	=>[
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => [ 
                        'rgb' => '000000'
                    ]
                ]
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ];
        $celdastexto = [
            'borders'	=>[
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => [ 
                        'rgb' => '000000'
                    ]
                ]
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_LEFT,
                'vertical' => Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ];
        $celdasnumero = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_RIGHT,
                'vertical' => Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ];
        $celdascentro = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ];
	   /*Estilos */	
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setTitle('Listado - Producto');

        $spreadsheet->getDefaultStyle()
            ->getFont()
            ->setName('Arial')
            ->setSize(9);
		
		$sheet->setCellValue('A1', 'Listado de Productos - Proceso Convencional')
			->mergeCells('A1:J1')
			->setCellValue('A3', '#')
			->setCellValue('B3', 'Nro Informe')
			->setCellValue('C3', 'Nombre')
			->setCellValue('D3', 'Envase')
			->setCellValue('E3', 'Nombre Comun')
			->setCellValue('F3', 'Dimensiones')
			->setCellValue('G3', 'Tipo de Equipo')
			->setCellValue('H3', 'Fabricante del Equipo')
			->setCellValue('I3', 'ID Equipo')
			->setCellValue('J3', 'PROCAL');

		$sheet->getStyle('A1:J1')->applyFromArray($titulo);
        $sheet->getStyle('A3:J3')->applyFromArray($cabecera);

		$sheet->getColumnDimension('A')->setAutoSize(false)->setWidth(4.10);
		$sheet->getColumnDimension('B')->setAutoSize(false)->setWidth(20.10);
		$sheet->getColumnDimension('C')->setAutoSize(false)->setWidth(14.10);
		$sheet->getColumnDimension('D')->setAutoSize(false)->setWidth(41.10);
		$sheet->getColumnDimension('E')->setAutoSize(false)->setWidth(34.10);
		$sheet->getColumnDimension('F')->setAutoSize(false)->setWidth(45.10);
		$sheet->getColumnDimension('G')->setAutoSize(false)->setWidth(45.10);
		$sheet->getColumnDimension('H')->setAutoSize(false)->setWidth(20.10);
		$sheet->getColumnDimension('I')->setAutoSize(false)->setWidth(20.10);
		$sheet->getColumnDimension('J')->setAutoSize(false)->setWidth(20.10);
		

		$varnull 			= 	'';

		$ccliente   = $this->input->post('hdnccliente_a');
            
        $parametros = array(
			'@ccliente'     => $ccliente,
		);		
		$rpt = $this->mservcliente->getproconvproducto($parametros);
		$i = 1;
		$irow = 4;
        if ($rpt){
        	foreach($rpt as $row){
            
				$NROINFOR = $row->NROINFOR;
				$PRODUCTO = $row->PRODUCTO;
				$ENVASE = $row->ENVASE;
				$NOMCOMUN = $row->NOMCOMUN;
				$DIMENSION = $row->DIMENSION;
				$TIPOEQUIPO = $row->TIPOEQUIPO;
				$FABRIEQUIPO = $row->FABRIEQUIPO;
				$IDENEQUIPO = $row->IDENEQUIPO;
				$NROPROCAL = $row->NROPROCAL;

				$sheet->setCellValue('A'.$irow,$i);
				$sheet->setCellValue('B'.$irow,$NROINFOR);
				$sheet->setCellValue('C'.$irow,$PRODUCTO);
				$sheet->setCellValue('D'.$irow,$ENVASE);
				$sheet->setCellValue('E'.$irow,$NOMCOMUN);
				$sheet->setCellValue('F'.$irow,$DIMENSION);
				$sheet->setCellValue('G'.$irow,$TIPOEQUIPO);
				$sheet->setCellValue('H'.$irow,$FABRIEQUIPO);
				$sheet->setCellValue('I'.$irow,$IDENEQUIPO);
				$sheet->setCellValue('J'.$irow,$NROPROCAL);

				$i++;
				$irow++;
			}
		}
		$pos = $irow - 1;
		$sheet->getStyle('A4:J'.$pos)->applyFromArray($celdastexto);
		//$sheet->getStyle('A4:A'.$pos)->applyFromArray($celdasnumero);
		//$sheet->getStyle('H4:H'.$pos)->applyFromArray($celdasnumero);
		//$sheet->getStyle('C4:C'.$pos)->applyFromArray($celdascentro);
		//$sheet->getStyle('E4:E'.$pos)->applyFromArray($celdascentro);

		$sheet->setAutoFilter('B3:J'.$pos);
		
                   
		$writer = new Xlsx($spreadsheet);
		$filename = 'listProductoProcConve-'.time().'.xlsx';
		ob_end_clean();
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
		header('Cache-Control: max-age=0');

		$writer->save('php://output');
	}
	
	public function excelcocsechorrecinto() {
		/*Estilos */
		 $titulo = [
			 'font'	=> [
				 'name' => 'Arial',
				 'size' =>12,
				 'color' => array('rgb' => 'FFFFFF'),
				 'bold' => true,
			 ], 
			 'fill'	=>[
				 'fillType' => Fill::FILL_SOLID,
				 'startColor' => [
					 'rgb' => '29B037'
				 ]
			 ],
			 'borders'	=>[
				 'allBorders' => [
					 'borderStyle' => Border::BORDER_THIN,
					 'color' => [ 
						 'rgb' => '000000'
					 ]
				 ]
			 ],
			 'alignment' => [
				 'horizontal' => Alignment::HORIZONTAL_CENTER,
				 'vertical' => Alignment::VERTICAL_CENTER,
				 'wrapText' => true,
			 ],
		 ];
		 $cabecera = [
			 'font'	=> [
				 'name' => 'Arial',
				 'size' =>10,
				 'color' => array('rgb' => 'FFFFFF'),
				 'bold' => true,
			 ], 
			 'fill'	=>[
				 'fillType' => Fill::FILL_SOLID,
				 'startColor' => [
					 'rgb' => '29B037'
				 ]
			 ],
			 'borders'	=>[
				 'allBorders' => [
					 'borderStyle' => Border::BORDER_THIN,
					 'color' => [ 
						 'rgb' => '000000'
					 ]
				 ]
			 ],
			 'alignment' => [
				 'horizontal' => Alignment::HORIZONTAL_CENTER,
				 'vertical' => Alignment::VERTICAL_CENTER,
				 'wrapText' => true,
			 ],
		 ];
		 $celdastexto = [
			 'borders'	=>[
				 'allBorders' => [
					 'borderStyle' => Border::BORDER_THIN,
					 'color' => [ 
						 'rgb' => '000000'
					 ]
				 ]
			 ],
			 'alignment' => [
				 'horizontal' => Alignment::HORIZONTAL_LEFT,
				 'vertical' => Alignment::VERTICAL_CENTER,
				 'wrapText' => true,
			 ],
		 ];
		 $celdasnumero = [
			 'alignment' => [
				 'horizontal' => Alignment::HORIZONTAL_RIGHT,
				 'vertical' => Alignment::VERTICAL_CENTER,
				 'wrapText' => true,
			 ],
		 ];
		 $celdascentro = [
			 'alignment' => [
				 'horizontal' => Alignment::HORIZONTAL_CENTER,
				 'vertical' => Alignment::VERTICAL_CENTER,
				 'wrapText' => true,
			 ],
		 ];
		/*Estilos */	
		 $spreadsheet = new Spreadsheet();
		 $sheet = $spreadsheet->getActiveSheet();
		 $sheet->setTitle('Listado - Recinto');
 
		 $spreadsheet->getDefaultStyle()
			 ->getFont()
			 ->setName('Arial')
			 ->setSize(9);
		 
		 $sheet->setCellValue('A1', 'Listado de Recintos - Cocinador Secador Horno')
			 ->mergeCells('A1:I1')
			 ->setCellValue('A3', '#')
			 ->setCellValue('B3', 'Nro Informe')
			 ->setCellValue('C3', 'Área evaluada')
			 ->setCellValue('D3', 'Medio Calentamiento')
			 ->setCellValue('E3', 'Nro Recinto')
			 ->setCellValue('F3', 'Nro Coches / Parrillas')
			 ->setCellValue('G3', 'ID Recintos')
			 ->setCellValue('H3', 'Producto')
			 ->setCellValue('I3', 'Presentacion');
 
		 $sheet->getStyle('A1:I1')->applyFromArray($titulo);
		 $sheet->getStyle('A3:I3')->applyFromArray($cabecera);
 
		 $sheet->getColumnDimension('A')->setAutoSize(false)->setWidth(4.10);
		 $sheet->getColumnDimension('B')->setAutoSize(false)->setWidth(20.10);
		 $sheet->getColumnDimension('C')->setAutoSize(false)->setWidth(14.10);
		 $sheet->getColumnDimension('D')->setAutoSize(false)->setWidth(41.10);
		 $sheet->getColumnDimension('E')->setAutoSize(false)->setWidth(34.10);
		 $sheet->getColumnDimension('F')->setAutoSize(false)->setWidth(45.10);
		 $sheet->getColumnDimension('G')->setAutoSize(false)->setWidth(45.10);
		 $sheet->getColumnDimension('H')->setAutoSize(false)->setWidth(20.10);
		 $sheet->getColumnDimension('I')->setAutoSize(false)->setWidth(20.10);
		 
 
		 $varnull 			= 	'';
 
		 $ccliente   = $this->input->post('hdnccliente');
			 
		 $parametros = array(
			 '@ccliente'     => $ccliente,
		 );		
		 $rpt = $this->mservcliente->getcocsechorrecinto($parametros);
		 $i = 1;
		 $irow = 4;
		 if ($rpt){
			 foreach($rpt as $row){
			 
				 $NROINFOR = $row->NROINFOR;
				 $AREAEVAL = $row->AREAEVAL;
				 $MEDIOCAL = $row->MEDIOCAL;
				 $NRORECINTOS = $row->NRORECINTOS;
				 $NROCOCHES = $row->NROCOCHES;
				 $IDENTIF = $row->IDENTIF;
				 $PRODUCTO = $row->PRODUCTO;
				 $PRESENTA = $row->PRESENTA;

				 $sheet->setCellValue('A'.$irow,$i);
				 $sheet->setCellValue('B'.$irow,$NROINFOR);
				 $sheet->setCellValue('C'.$irow,$AREAEVAL);
				 $sheet->setCellValue('D'.$irow,$MEDIOCAL);
				 $sheet->setCellValue('E'.$irow,$NRORECINTOS);
				 $sheet->setCellValue('F'.$irow,$NROCOCHES);
				 $sheet->setCellValue('G'.$irow,$IDENTIF);
				 $sheet->setCellValue('H'.$irow,$PRODUCTO);
				 $sheet->setCellValue('I'.$irow,$PRESENTA);
 
				 $i++;
				 $irow++;
			 }
		 }
		 $pos = $irow - 1;
		 $sheet->getStyle('A4:I'.$pos)->applyFromArray($celdastexto);
		 //$sheet->getStyle('A4:A'.$pos)->applyFromArray($celdasnumero);
		 //$sheet->getStyle('H4:H'.$pos)->applyFromArray($celdasnumero);
		 //$sheet->getStyle('C4:C'.$pos)->applyFromArray($celdascentro);
		 //$sheet->getStyle('E4:E'.$pos)->applyFromArray($celdascentro);
 
		 $sheet->setAutoFilter('B3:I'.$pos);
		 
					
		 $writer = new Xlsx($spreadsheet);
		 $filename = 'listRecintococsechor-'.time().'.xlsx';
		 ob_end_clean();
		 header('Content-Type: application/vnd.ms-excel');
		 header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
		 header('Cache-Control: max-age=0');
 
		 $writer->save('php://output');
	}
	public function excelcocsechorequipo() {
	   /*Estilos */
		$titulo = [
			'font'	=> [
				'name' => 'Arial',
				'size' =>12,
				'color' => array('rgb' => 'FFFFFF'),
				'bold' => true,
			], 
			'fill'	=>[
				'fillType' => Fill::FILL_SOLID,
				'startColor' => [
					'rgb' => '29B037'
				]
			],
			'borders'	=>[
				'allBorders' => [
					'borderStyle' => Border::BORDER_THIN,
					'color' => [ 
						'rgb' => '000000'
					]
				]
			],
			'alignment' => [
				'horizontal' => Alignment::HORIZONTAL_CENTER,
				'vertical' => Alignment::VERTICAL_CENTER,
				'wrapText' => true,
			],
		];
        $cabecera = [
            'font'	=> [
                'name' => 'Arial',
                'size' =>10,
                'color' => array('rgb' => 'FFFFFF'),
                'bold' => true,
            ], 
            'fill'	=>[
                'fillType' => Fill::FILL_SOLID,
                'startColor' => [
                    'rgb' => '29B037'
                ]
            ],
            'borders'	=>[
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => [ 
                        'rgb' => '000000'
                    ]
                ]
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ];
        $celdastexto = [
            'borders'	=>[
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => [ 
                        'rgb' => '000000'
                    ]
                ]
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_LEFT,
                'vertical' => Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ];
        $celdasnumero = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_RIGHT,
                'vertical' => Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ];
        $celdascentro = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ];
	   /*Estilos */	
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setTitle('Listado - Equipo');

        $spreadsheet->getDefaultStyle()
            ->getFont()
            ->setName('Arial')
            ->setSize(9);
		
		$sheet->setCellValue('A1', 'Listado de Equipos - Cocinador Secador Horno')
			->mergeCells('A1:J1')
			->setCellValue('A3', '#')
			->setCellValue('B3', 'Nro Informe')
			->setCellValue('C3', 'Fabricante')
			->setCellValue('D3', 'Capacidad')
			->setCellValue('E3', 'Medio de Calentamiento')
			->setCellValue('F3', 'Nro Equipos')
			->setCellValue('G3', 'Nro Coches / Parrillas')
			->setCellValue('H3', 'ID Equipos')
			->setCellValue('I3', 'Producto')
			->setCellValue('J3', 'Presentacion');

		$sheet->getStyle('A1:J1')->applyFromArray($titulo);
        $sheet->getStyle('A3:J3')->applyFromArray($cabecera);

		$sheet->getColumnDimension('A')->setAutoSize(false)->setWidth(4.10);
		$sheet->getColumnDimension('B')->setAutoSize(false)->setWidth(20.10);
		$sheet->getColumnDimension('C')->setAutoSize(false)->setWidth(14.10);
		$sheet->getColumnDimension('D')->setAutoSize(false)->setWidth(41.10);
		$sheet->getColumnDimension('E')->setAutoSize(false)->setWidth(34.10);
		$sheet->getColumnDimension('F')->setAutoSize(false)->setWidth(45.10);
		$sheet->getColumnDimension('G')->setAutoSize(false)->setWidth(45.10);
		$sheet->getColumnDimension('H')->setAutoSize(false)->setWidth(20.10);
		$sheet->getColumnDimension('I')->setAutoSize(false)->setWidth(20.10);
		$sheet->getColumnDimension('J')->setAutoSize(false)->setWidth(20.10);
		

		$varnull 			= 	'';

		$ccliente   = $this->input->post('hdnccliente_a');
            
        $parametros = array(
			'@ccliente'     => $ccliente,
		);		
		$rpt = $this->mservcliente->getcocsechorequipo($parametros);
		$i = 1;
		$irow = 4;
        if ($rpt){
        	foreach($rpt as $row){
            
				$NROINFOR = $row->NROINFOR;
				$FABRI = $row->FABRI;
				$CAPA = $row->CAPA;
				$MEDIOCAL = $row->MEDIOCAL;
				$NROEQUI = $row->NROEQUI;
				$NROCANAST = $row->NROCANAST;
				$IDENTIF = $row->IDENTIF;
				$PRODUCTO = $row->PRODUCTO;
				$PRESENTA = $row->PRESENTA;

				$sheet->setCellValue('A'.$irow,$i);
				$sheet->setCellValue('B'.$irow,$NROINFOR);
				$sheet->setCellValue('C'.$irow,$FABRI);
				$sheet->setCellValue('D'.$irow,$CAPA);
				$sheet->setCellValue('E'.$irow,$MEDIOCAL);
				$sheet->setCellValue('F'.$irow,$NROEQUI);
				$sheet->setCellValue('G'.$irow,$NROCANAST);
				$sheet->setCellValue('H'.$irow,$IDENTIF);
				$sheet->setCellValue('I'.$irow,$PRODUCTO);
				$sheet->setCellValue('J'.$irow,$PRESENTA);

				$i++;
				$irow++;
			}
		}
		$pos = $irow - 1;
		$sheet->getStyle('A4:J'.$pos)->applyFromArray($celdastexto);
		//$sheet->getStyle('A4:A'.$pos)->applyFromArray($celdasnumero);
		//$sheet->getStyle('H4:H'.$pos)->applyFromArray($celdasnumero);
		//$sheet->getStyle('C4:C'.$pos)->applyFromArray($celdascentro);
		//$sheet->getStyle('E4:E'.$pos)->applyFromArray($celdascentro);

		$sheet->setAutoFilter('B3:J'.$pos);
		
                   
		$writer = new Xlsx($spreadsheet);
		$filename = 'listEquipococsechor-'.time().'.xlsx';
		ob_end_clean();
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
		header('Cache-Control: max-age=0');

		$writer->save('php://output');
	}
	
	public function excelcalfrioproducto() {
		/*Estilos */
		 $titulo = [
			 'font'	=> [
				 'name' => 'Arial',
				 'size' =>12,
				 'color' => array('rgb' => 'FFFFFF'),
				 'bold' => true,
			 ], 
			 'fill'	=>[
				 'fillType' => Fill::FILL_SOLID,
				 'startColor' => [
					 'rgb' => '29B037'
				 ]
			 ],
			 'borders'	=>[
				 'allBorders' => [
					 'borderStyle' => Border::BORDER_THIN,
					 'color' => [ 
						 'rgb' => '000000'
					 ]
				 ]
			 ],
			 'alignment' => [
				 'horizontal' => Alignment::HORIZONTAL_CENTER,
				 'vertical' => Alignment::VERTICAL_CENTER,
				 'wrapText' => true,
			 ],
		 ];
		 $cabecera = [
			 'font'	=> [
				 'name' => 'Arial',
				 'size' =>10,
				 'color' => array('rgb' => 'FFFFFF'),
				 'bold' => true,
			 ], 
			 'fill'	=>[
				 'fillType' => Fill::FILL_SOLID,
				 'startColor' => [
					 'rgb' => '29B037'
				 ]
			 ],
			 'borders'	=>[
				 'allBorders' => [
					 'borderStyle' => Border::BORDER_THIN,
					 'color' => [ 
						 'rgb' => '000000'
					 ]
				 ]
			 ],
			 'alignment' => [
				 'horizontal' => Alignment::HORIZONTAL_CENTER,
				 'vertical' => Alignment::VERTICAL_CENTER,
				 'wrapText' => true,
			 ],
		 ];
		 $celdastexto = [
			 'borders'	=>[
				 'allBorders' => [
					 'borderStyle' => Border::BORDER_THIN,
					 'color' => [ 
						 'rgb' => '000000'
					 ]
				 ]
			 ],
			 'alignment' => [
				 'horizontal' => Alignment::HORIZONTAL_LEFT,
				 'vertical' => Alignment::VERTICAL_CENTER,
				 'wrapText' => true,
			 ],
		 ];
		 $celdasnumero = [
			 'alignment' => [
				 'horizontal' => Alignment::HORIZONTAL_RIGHT,
				 'vertical' => Alignment::VERTICAL_CENTER,
				 'wrapText' => true,
			 ],
		 ];
		 $celdascentro = [
			 'alignment' => [
				 'horizontal' => Alignment::HORIZONTAL_CENTER,
				 'vertical' => Alignment::VERTICAL_CENTER,
				 'wrapText' => true,
			 ],
		 ];
		/*Estilos */	
		 $spreadsheet = new Spreadsheet();
		 $sheet = $spreadsheet->getActiveSheet();
		 $sheet->setTitle('Listado - Producto');
 
		 $spreadsheet->getDefaultStyle()
			 ->getFont()
			 ->setName('Arial')
			 ->setSize(9);
		 
		 $sheet->setCellValue('A1', 'Listado de Productos - Llenado en Caliente - Frio')
			 ->mergeCells('A1:E1')
			 ->setCellValue('A3', '#')
			 ->setCellValue('B3', 'Nro Informe')
			 ->setCellValue('C3', 'Nombre')
			 ->setCellValue('D3', 'Envase')
			 ->setCellValue('E3', 'Dimensiones');
 
		 $sheet->getStyle('A1:E1')->applyFromArray($titulo);
		 $sheet->getStyle('A3:E3')->applyFromArray($cabecera);
 
		 $sheet->getColumnDimension('A')->setAutoSize(false)->setWidth(4.10);
		 $sheet->getColumnDimension('B')->setAutoSize(false)->setWidth(20.10);
		 $sheet->getColumnDimension('C')->setAutoSize(false)->setWidth(14.10);
		 $sheet->getColumnDimension('D')->setAutoSize(false)->setWidth(41.10);
		 $sheet->getColumnDimension('E')->setAutoSize(false)->setWidth(34.10);
		 
 
		 $varnull 			= 	'';
 
		 $ccliente   = $this->input->post('hdnccliente');
			 
		 $parametros = array(
			 '@ccliente'     => $ccliente,
		 );		
		 $rpt = $this->mservcliente->getcalfrioproducto($parametros);
		 $i = 1;
		 $irow = 4;
		 if ($rpt){
			 foreach($rpt as $row){
			 
				 $NROINFOR = $row->NROINFOR;
				 $PRODUCTO = $row->PRODUCTO;
				 $ENVASE = $row->ENVASE;
				 $DIMENSION = $row->DIMENSION;

				 $sheet->setCellValue('A'.$irow,$i);
				 $sheet->setCellValue('B'.$irow,$NROINFOR);
				 $sheet->setCellValue('C'.$irow,$PRODUCTO);
				 $sheet->setCellValue('D'.$irow,$ENVASE);
				 $sheet->setCellValue('E'.$irow,$DIMENSION);
 
				 $i++;
				 $irow++;
			 }
		 }
		 $pos = $irow - 1;
		 $sheet->getStyle('A4:E'.$pos)->applyFromArray($celdastexto);
		 //$sheet->getStyle('A4:A'.$pos)->applyFromArray($celdasnumero);
		 //$sheet->getStyle('H4:H'.$pos)->applyFromArray($celdasnumero);
		 //$sheet->getStyle('C4:C'.$pos)->applyFromArray($celdascentro);
		 //$sheet->getStyle('E4:E'.$pos)->applyFromArray($celdascentro);
 
		 $sheet->setAutoFilter('B3:E'.$pos);
		 
					
		 $writer = new Xlsx($spreadsheet);
		 $filename = 'listProductocalfrio-'.time().'.xlsx';
		 ob_end_clean();
		 header('Content-Type: application/vnd.ms-excel');
		 header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
		 header('Cache-Control: max-age=0');
 
		 $writer->save('php://output');
	}
	
	public function excelprocasepproducto() {
		/*Estilos */
		 $titulo = [
			 'font'	=> [
				 'name' => 'Arial',
				 'size' =>12,
				 'color' => array('rgb' => 'FFFFFF'),
				 'bold' => true,
			 ], 
			 'fill'	=>[
				 'fillType' => Fill::FILL_SOLID,
				 'startColor' => [
					 'rgb' => '29B037'
				 ]
			 ],
			 'borders'	=>[
				 'allBorders' => [
					 'borderStyle' => Border::BORDER_THIN,
					 'color' => [ 
						 'rgb' => '000000'
					 ]
				 ]
			 ],
			 'alignment' => [
				 'horizontal' => Alignment::HORIZONTAL_CENTER,
				 'vertical' => Alignment::VERTICAL_CENTER,
				 'wrapText' => true,
			 ],
		 ];
		 $cabecera = [
			 'font'	=> [
				 'name' => 'Arial',
				 'size' =>10,
				 'color' => array('rgb' => 'FFFFFF'),
				 'bold' => true,
			 ], 
			 'fill'	=>[
				 'fillType' => Fill::FILL_SOLID,
				 'startColor' => [
					 'rgb' => '29B037'
				 ]
			 ],
			 'borders'	=>[
				 'allBorders' => [
					 'borderStyle' => Border::BORDER_THIN,
					 'color' => [ 
						 'rgb' => '000000'
					 ]
				 ]
			 ],
			 'alignment' => [
				 'horizontal' => Alignment::HORIZONTAL_CENTER,
				 'vertical' => Alignment::VERTICAL_CENTER,
				 'wrapText' => true,
			 ],
		 ];
		 $celdastexto = [
			 'borders'	=>[
				 'allBorders' => [
					 'borderStyle' => Border::BORDER_THIN,
					 'color' => [ 
						 'rgb' => '000000'
					 ]
				 ]
			 ],
			 'alignment' => [
				 'horizontal' => Alignment::HORIZONTAL_LEFT,
				 'vertical' => Alignment::VERTICAL_CENTER,
				 'wrapText' => true,
			 ],
		 ];
		 $celdasnumero = [
			 'alignment' => [
				 'horizontal' => Alignment::HORIZONTAL_RIGHT,
				 'vertical' => Alignment::VERTICAL_CENTER,
				 'wrapText' => true,
			 ],
		 ];
		 $celdascentro = [
			 'alignment' => [
				 'horizontal' => Alignment::HORIZONTAL_CENTER,
				 'vertical' => Alignment::VERTICAL_CENTER,
				 'wrapText' => true,
			 ],
		 ];
		/*Estilos */	
		 $spreadsheet = new Spreadsheet();
		 $sheet = $spreadsheet->getActiveSheet();
		 $sheet->setTitle('Listado - Producto');
 
		 $spreadsheet->getDefaultStyle()
			 ->getFont()
			 ->setName('Arial')
			 ->setSize(9);
		 
		 $sheet->setCellValue('A1', 'Listado de Producto - Procesamiento Aséptico')
			 ->mergeCells('A1:F1')
			 ->setCellValue('A3', '#')
			 ->setCellValue('B3', 'Nro Informe')
			 ->setCellValue('C3', 'Nombre Producto')
			 ->setCellValue('D3', 'pH Materia Prima')
			 ->setCellValue('E3', 'pH Producto Final')
			 ->setCellValue('F3', 'Particula');
 
		 $sheet->getStyle('A1:F1')->applyFromArray($titulo);
		 $sheet->getStyle('A3:F3')->applyFromArray($cabecera);
 
		 $sheet->getColumnDimension('A')->setAutoSize(false)->setWidth(4.10);
		 $sheet->getColumnDimension('B')->setAutoSize(false)->setWidth(20.10);
		 $sheet->getColumnDimension('C')->setAutoSize(false)->setWidth(14.10);
		 $sheet->getColumnDimension('D')->setAutoSize(false)->setWidth(41.10);
		 $sheet->getColumnDimension('E')->setAutoSize(false)->setWidth(34.10);
		 $sheet->getColumnDimension('F')->setAutoSize(false)->setWidth(45.10);
		 
 
		 $varnull 			= 	'';
 
		 $ccliente   = $this->input->post('hdnccliente');
			 
		 $parametros = array(
			 '@ccliente'     => $ccliente,
		 );		
		 $rpt = $this->mservcliente->getproasepproducto($parametros);
		 $i = 1;
		 $irow = 4;
		 if ($rpt){
			 foreach($rpt as $row){
			 
				 $NROINFOR = $row->NROINFOR;
				 $NOMPROD = $row->NOMPROD;
				 $ph_materia_prima = $row->ph_materia_prima;
				 $ph_producto_final = $row->ph_producto_final;
				 $PARTICULA = $row->PARTICULA;

				 $sheet->setCellValue('A'.$irow,$i);
				 $sheet->setCellValue('B'.$irow,$NROINFOR);
				 $sheet->setCellValue('C'.$irow,$NOMPROD);
				 $sheet->setCellValue('D'.$irow,$ph_materia_prima);
				 $sheet->setCellValue('E'.$irow,$ph_producto_final);
				 $sheet->setCellValue('F'.$irow,$PARTICULA);
 
				 $i++;
				 $irow++;
			 }
		 }
		 $pos = $irow - 1;
		 $sheet->getStyle('A4:F'.$pos)->applyFromArray($celdastexto);
		 //$sheet->getStyle('A4:A'.$pos)->applyFromArray($celdasnumero);
		 //$sheet->getStyle('H4:H'.$pos)->applyFromArray($celdasnumero);
		 //$sheet->getStyle('C4:C'.$pos)->applyFromArray($celdascentro);
		 //$sheet->getStyle('E4:E'.$pos)->applyFromArray($celdascentro);
 
		 $sheet->setAutoFilter('B3:F'.$pos);
		 
					
		 $writer = new Xlsx($spreadsheet);
		 $filename = 'listProductoprocasep-'.time().'.xlsx';
		 ob_end_clean();
		 header('Content-Type: application/vnd.ms-excel');
		 header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
		 header('Cache-Control: max-age=0');
 
		 $writer->save('php://output');
	}
	
	public function excelmapterequipo() {
	   /*Estilos */
		$titulo = [
			'font'	=> [
				'name' => 'Arial',
				'size' =>12,
				'color' => array('rgb' => 'FFFFFF'),
				'bold' => true,
			], 
			'fill'	=>[
				'fillType' => Fill::FILL_SOLID,
				'startColor' => [
					'rgb' => '29B037'
				]
			],
			'borders'	=>[
				'allBorders' => [
					'borderStyle' => Border::BORDER_THIN,
					'color' => [ 
						'rgb' => '000000'
					]
				]
			],
			'alignment' => [
				'horizontal' => Alignment::HORIZONTAL_CENTER,
				'vertical' => Alignment::VERTICAL_CENTER,
				'wrapText' => true,
			],
		];
        $cabecera = [
            'font'	=> [
                'name' => 'Arial',
                'size' =>10,
                'color' => array('rgb' => 'FFFFFF'),
                'bold' => true,
            ], 
            'fill'	=>[
                'fillType' => Fill::FILL_SOLID,
                'startColor' => [
                    'rgb' => '29B037'
                ]
            ],
            'borders'	=>[
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => [ 
                        'rgb' => '000000'
                    ]
                ]
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ];
        $celdastexto = [
            'borders'	=>[
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => [ 
                        'rgb' => '000000'
                    ]
                ]
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_LEFT,
                'vertical' => Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ];
        $celdasnumero = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_RIGHT,
                'vertical' => Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ];
        $celdascentro = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ];
	   /*Estilos */	
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setTitle('Listado - Equipo');

        $spreadsheet->getDefaultStyle()
            ->getFont()
            ->setName('Arial')
            ->setSize(9);
		
		$sheet->setCellValue('A1', 'Listado de Equipos - Mapeo Termico')
			->mergeCells('A1:I1')
			->setCellValue('A3', '#')
			->setCellValue('B3', 'Nro Informe')
			->setCellValue('C3', 'Nro Equipos')
			->setCellValue('D3', 'Area Evaluada m2')
			->setCellValue('E3', 'Nro Posiciones')
			->setCellValue('F3', 'Vol. Enfria. m3')
			->setCellValue('G3', 'Producto')
			->setCellValue('H3', 'Forma de Producto')
			->setCellValue('I3', 'Envase');


		$sheet->getStyle('A1:I1')->applyFromArray($titulo);
        $sheet->getStyle('A3:I3')->applyFromArray($cabecera);

		$sheet->getColumnDimension('A')->setAutoSize(false)->setWidth(4.10);
		$sheet->getColumnDimension('B')->setAutoSize(false)->setWidth(20.10);
		$sheet->getColumnDimension('C')->setAutoSize(false)->setWidth(14.10);
		$sheet->getColumnDimension('D')->setAutoSize(false)->setWidth(41.10);
		$sheet->getColumnDimension('E')->setAutoSize(false)->setWidth(34.10);
		$sheet->getColumnDimension('F')->setAutoSize(false)->setWidth(45.10);
		$sheet->getColumnDimension('G')->setAutoSize(false)->setWidth(45.10);
		$sheet->getColumnDimension('H')->setAutoSize(false)->setWidth(20.10);
		$sheet->getColumnDimension('I')->setAutoSize(false)->setWidth(20.10);
		

		$varnull 			= 	'';

		$ccliente   = $this->input->post('hdnccliente');
            
        $parametros = array(
			'@ccliente'     => $ccliente,
		);		
		$rpt = $this->mservcliente->getmapterequipo($parametros);
		$i = 1;
		$irow = 4;
        if ($rpt){
        	foreach($rpt as $row){
            
				$NROINFOR = $row->NROINFOR;
				$NROEQUIPOS = $row->NROEQUIPOS;
				$AREAEVAL = $row->AREAEVAL;
				$NROPOS = $row->NROPOS;
				$VOLALMA = $row->VOLALMA;
				$PRODUCTO = $row->PRODUCTO;
				$FORMAPRODUCTO = $row->FORMAPRODUCTO;
				$ENVASE = $row->ENVASE;

				$sheet->setCellValue('A'.$irow,$i);
				$sheet->setCellValue('B'.$irow,$NROINFOR);
				$sheet->setCellValue('C'.$irow,$NROEQUIPOS);
				$sheet->setCellValue('D'.$irow,$AREAEVAL);
				$sheet->setCellValue('E'.$irow,$NROPOS);
				$sheet->setCellValue('F'.$irow,$VOLALMA);
				$sheet->setCellValue('G'.$irow,$PRODUCTO);
				$sheet->setCellValue('H'.$irow,$FORMAPRODUCTO);
				$sheet->setCellValue('I'.$irow,$ENVASE);

				$i++;
				$irow++;
			}
		}
		$pos = $irow - 1;
		$sheet->getStyle('A4:I'.$pos)->applyFromArray($celdastexto);
		//$sheet->getStyle('A4:A'.$pos)->applyFromArray($celdasnumero);
		//$sheet->getStyle('H4:H'.$pos)->applyFromArray($celdasnumero);
		//$sheet->getStyle('C4:C'.$pos)->applyFromArray($celdascentro);
		//$sheet->getStyle('E4:E'.$pos)->applyFromArray($celdascentro);

		$sheet->setAutoFilter('B3:I'.$pos);
		
                   
		$writer = new Xlsx($spreadsheet);
		$filename = 'listEquipoMapTer-'.time().'.xlsx';
		ob_end_clean();
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
		header('Cache-Control: max-age=0');

		$writer->save('php://output');
	}
	public function excelmapterrecinto() {
	   /*Estilos */
		$titulo = [
			'font'	=> [
				'name' => 'Arial',
				'size' =>12,
				'color' => array('rgb' => 'FFFFFF'),
				'bold' => true,
			], 
			'fill'	=>[
				'fillType' => Fill::FILL_SOLID,
				'startColor' => [
					'rgb' => '29B037'
				]
			],
			'borders'	=>[
				'allBorders' => [
					'borderStyle' => Border::BORDER_THIN,
					'color' => [ 
						'rgb' => '000000'
					]
				]
			],
			'alignment' => [
				'horizontal' => Alignment::HORIZONTAL_CENTER,
				'vertical' => Alignment::VERTICAL_CENTER,
				'wrapText' => true,
			],
		];
        $cabecera = [
            'font'	=> [
                'name' => 'Arial',
                'size' =>10,
                'color' => array('rgb' => 'FFFFFF'),
                'bold' => true,
            ], 
            'fill'	=>[
                'fillType' => Fill::FILL_SOLID,
                'startColor' => [
                    'rgb' => '29B037'
                ]
            ],
            'borders'	=>[
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => [ 
                        'rgb' => '000000'
                    ]
                ]
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ];
        $celdastexto = [
            'borders'	=>[
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => [ 
                        'rgb' => '000000'
                    ]
                ]
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_LEFT,
                'vertical' => Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ];
        $celdasnumero = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_RIGHT,
                'vertical' => Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ];
        $celdascentro = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ];
	   /*Estilos */	
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setTitle('Listado - Equipo');

        $spreadsheet->getDefaultStyle()
            ->getFont()
            ->setName('Arial')
            ->setSize(9);
		
		$sheet->setCellValue('A1', 'Listado de Recintos - Mapeo Termico')
			->mergeCells('A1:G1')
			->setCellValue('A3', '#')
			->setCellValue('B3', 'Nro Informe')
			->setCellValue('C3', 'Evaluación')
			->setCellValue('D3', 'Nro. Recintos')
			->setCellValue('E3', 'Área Evaluada m2')
			->setCellValue('F3', 'Nro. Posiciones')
			->setCellValue('G3', 'Vol. Alma. m3');

		$sheet->getStyle('A1:G1')->applyFromArray($titulo);
        $sheet->getStyle('A3:G3')->applyFromArray($cabecera);

		$sheet->getColumnDimension('A')->setAutoSize(false)->setWidth(4.10);
		$sheet->getColumnDimension('B')->setAutoSize(false)->setWidth(20.10);
		$sheet->getColumnDimension('C')->setAutoSize(false)->setWidth(14.10);
		$sheet->getColumnDimension('D')->setAutoSize(false)->setWidth(41.10);
		$sheet->getColumnDimension('E')->setAutoSize(false)->setWidth(34.10);
		$sheet->getColumnDimension('F')->setAutoSize(false)->setWidth(45.10);
		$sheet->getColumnDimension('G')->setAutoSize(false)->setWidth(45.10);
		

		$varnull 			= 	'';

		$ccliente   = $this->input->post('hdnccliente_a');
            
        $parametros = array(
			'@ccliente'     => $ccliente,
		);		
		$rpt = $this->mservcliente->getmapterrecinto($parametros);
		$i = 1;
		$irow = 4;
        if ($rpt){
        	foreach($rpt as $row){
            
				$NROINFOR = $row->NROINFOR;
				$EVALUACION = $row->EVALUACION;
				$NRORECINTOS = $row->NRORECINTOS;
				$AREAEVAL = $row->AREAEVAL;
				$NROPOS = $row->NROPOS;
				$VOLENFRIA = $row->VOLENFRIA;

				$sheet->setCellValue('A'.$irow,$i);
				$sheet->setCellValue('B'.$irow,$NROINFOR);
				$sheet->setCellValue('C'.$irow,$EVALUACION);
				$sheet->setCellValue('D'.$irow,$NRORECINTOS);
				$sheet->setCellValue('E'.$irow,$AREAEVAL);
				$sheet->setCellValue('F'.$irow,$NROPOS);
				$sheet->setCellValue('G'.$irow,$VOLENFRIA);

				$i++;
				$irow++;
			}
		}
		$pos = $irow - 1;
		$sheet->getStyle('A4:G'.$pos)->applyFromArray($celdastexto);
		//$sheet->getStyle('A4:A'.$pos)->applyFromArray($celdasnumero);
		//$sheet->getStyle('H4:H'.$pos)->applyFromArray($celdasnumero);
		//$sheet->getStyle('C4:C'.$pos)->applyFromArray($celdascentro);
		//$sheet->getStyle('E4:E'.$pos)->applyFromArray($celdascentro);

		$sheet->setAutoFilter('B3:G'.$pos);
		
                   
		$writer = new Xlsx($spreadsheet);
		$filename = 'listRecintoMapTer-'.time().'.xlsx';
		ob_end_clean();
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
		header('Cache-Control: max-age=0');

		$writer->save('php://output');
	}
	public function excelmapterestudio() {
	   /*Estilos */
		$titulo = [
			'font'	=> [
				'name' => 'Arial',
				'size' =>12,
				'color' => array('rgb' => 'FFFFFF'),
				'bold' => true,
			], 
			'fill'	=>[
				'fillType' => Fill::FILL_SOLID,
				'startColor' => [
					'rgb' => '29B037'
				]
			],
			'borders'	=>[
				'allBorders' => [
					'borderStyle' => Border::BORDER_THIN,
					'color' => [ 
						'rgb' => '000000'
					]
				]
			],
			'alignment' => [
				'horizontal' => Alignment::HORIZONTAL_CENTER,
				'vertical' => Alignment::VERTICAL_CENTER,
				'wrapText' => true,
			],
		];
        $cabecera = [
            'font'	=> [
                'name' => 'Arial',
                'size' =>10,
                'color' => array('rgb' => 'FFFFFF'),
                'bold' => true,
            ], 
            'fill'	=>[
                'fillType' => Fill::FILL_SOLID,
                'startColor' => [
                    'rgb' => '29B037'
                ]
            ],
            'borders'	=>[
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => [ 
                        'rgb' => '000000'
                    ]
                ]
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ];
        $celdastexto = [
            'borders'	=>[
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => [ 
                        'rgb' => '000000'
                    ]
                ]
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_LEFT,
                'vertical' => Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ];
        $celdasnumero = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_RIGHT,
                'vertical' => Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ];
        $celdascentro = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ];
	   /*Estilos */	
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setTitle('Listado - Equipo');

        $spreadsheet->getDefaultStyle()
            ->getFont()
            ->setName('Arial')
            ->setSize(9);
		
		$sheet->setCellValue('A1', 'Listado de Estudios - Mapeo Termico')
			->mergeCells('A1:F1')
			->setCellValue('A3', '#')
			->setCellValue('B3', 'Nro Informe')
			->setCellValue('C3', 'Nro. ID')
			->setCellValue('D3', 'Producto')
			->setCellValue('E3', 'Forma de Producto')
			->setCellValue('F3', 'Envase');
			

		$sheet->getStyle('A1:F1')->applyFromArray($titulo);
        $sheet->getStyle('A3:F3')->applyFromArray($cabecera);

		$sheet->getColumnDimension('A')->setAutoSize(false)->setWidth(4.10);
		$sheet->getColumnDimension('B')->setAutoSize(false)->setWidth(20.10);
		$sheet->getColumnDimension('C')->setAutoSize(false)->setWidth(14.10);
		$sheet->getColumnDimension('D')->setAutoSize(false)->setWidth(41.10);
		$sheet->getColumnDimension('E')->setAutoSize(false)->setWidth(34.10);
		$sheet->getColumnDimension('F')->setAutoSize(false)->setWidth(45.10);
		

		$varnull 			= 	'';

		$ccliente   = $this->input->post('hdnccliente_b');
            
        $parametros = array(
			'@ccliente'     => $ccliente,
		);		
		$rpt = $this->mservcliente->getmapterestudio($parametros);
		$i = 1;
		$irow = 4;
        if ($rpt){
        	foreach($rpt as $row){
            
				$NROINFOR = $row->NROINFOR;
				$ID = $row->ID;
				$PRODUCTO = $row->PRODUCTO;
				$FORMAPRODUCTO = $row->FORMAPRODUCTO;
				$ENVASE = $row->ENVASE;

				$sheet->setCellValue('A'.$irow,$i);
				$sheet->setCellValue('B'.$irow,$NROINFOR);
				$sheet->setCellValue('C'.$irow,$ID);
				$sheet->setCellValue('D'.$irow,$PRODUCTO);
				$sheet->setCellValue('E'.$irow,$FORMAPRODUCTO);
				$sheet->setCellValue('F'.$irow,$ENVASE);

				$i++;
				$irow++;
			}
		}
		$pos = $irow - 1;
		$sheet->getStyle('A4:F'.$pos)->applyFromArray($celdastexto);
		//$sheet->getStyle('A4:A'.$pos)->applyFromArray($celdasnumero);
		//$sheet->getStyle('H4:H'.$pos)->applyFromArray($celdasnumero);
		//$sheet->getStyle('C4:C'.$pos)->applyFromArray($celdascentro);
		//$sheet->getStyle('E4:E'.$pos)->applyFromArray($celdascentro);

		$sheet->setAutoFilter('B3:F'.$pos);
		
                   
		$writer = new Xlsx($spreadsheet);
		$filename = 'listEstudioMapTer-'.time().'.xlsx';
		ob_end_clean();
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
		header('Cache-Control: max-age=0');

		$writer->save('php://output');
	}
}
?>