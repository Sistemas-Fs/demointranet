<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Style\Conditional;
use PhpOffice\PhpSpreadsheet\IOFactory;

class Ccartas extends CI_Controller {
	function __construct() {
		parent:: __construct();	
		$this->load->model('pt/mcartas');
		$this->load->model('mglobales');
		$this->load->library('encryption');
		$this->load->helper(array('form','url','download','html','file'));
		$this->load->library('form_validation');
    }
    
   /** CARTAS **/ 	
    public function getclientecarta() {	// Visualizar Clientes con propuestas en CBO	
        
		$resultado = $this->mcartas->getclientecarta();
		echo json_encode($resultado);
    }
    public function getbuscarcarta() { // Tabla de busqueda de clientes	
        
		$varnull 			= 	'';

		$ccliente   = $this->input->post('ccliente');
		$fini       = $this->input->post('fdesde');
		$ffin       = $this->input->post('fhasta');
		$dnrodet    = $this->input->post('dnrodet');
        $parametros = array(
			'@ccliente'     => ($this->input->post('ccliente') == '') ? '0' : $ccliente,
			'@fini'         => ($this->input->post('fdesde') == '%') ? NULL : substr($fini, 6, 4).'-'.substr($fini,3 , 2).'-'.substr($fini, 0, 2),
			'@ffin'         => ($this->input->post('fhasta') == '%') ? NULL : substr($ffin, 6, 4).'-'.substr($ffin,3 , 2).'-'.substr($ffin, 0, 2),
			'@dnrodet'      => ($this->input->post('dnrodet') == $varnull) ? '%' : "%".$dnrodet."%",
		);		

		$resultado = $this->mcartas->getbuscarcarta($parametros);
		echo json_encode($resultado);
	}

    public function getbuscarCliente() { // Tabla de busqueda de clientes	
        
		$resultado = $this->mcartas->getbuscarCliente();
		echo json_encode($resultado);
	}
    
    public function getnrocarta() {	// Obtener numero de propuesta	
		
		$parametros = array(
            '@yearCarta'   => $this->input->post('yearCarta')
        );
		$resultado = $this->mcartas->getnrocarta($parametros);
		echo json_encode($resultado);
	}

	public function subirArchivo() {	// Subir Acrhivo 
        $IDCARTA   = $this->input->post('mhdnIdCarta');
        $ANIO       = substr($this->input->post('mtxtFcarta'),-4);
        $NOMBARCH 	= 'CARTA'.substr($this->input->post('mtxtNrocarta'),0,4).substr($this->input->post('mtxtNrocarta'),5,4).'PTFS';
        
        $RUTAARCH   = 'FTPfileserver/Archivos/10204/'.$ANIO.'/';

        !is_dir($RUTAARCH) && @mkdir($RUTAARCH, 0777, true);

		//RUTA DONDE SE GUARDAN LOS FICHEROS
		$config['upload_path'] 		= $RUTAARCH;
		$config['allowed_types'] 	= 'pdf|xlsx|docx|xls|doc';
		$config['max_size'] 		= '60048';
		$config['overwrite'] 		= TRUE;
		$config['file_name']        = $NOMBARCH;
		
		$this->load->library('upload',$config);
		$this->upload->initialize($config);
		
		if (!($this->upload->do_upload('mtxtArchivocarta'))) {
			//si al subirse hay algun error 
			$data['uploadError'] = $this->upload->display_errors();
			$error = '';
			return $error;					
		}else{
			$data = $this->upload->data();
			$parametros = array(
                '@idptcarta'   	=>  $IDCARTA,
                '@archivo_carta'    =>  $data['file_name'],
                '@ruta_carta'       =>  $config['upload_path'],
                '@descripcion_archivo'   =>  $this->input->post('mtxtNomarchcarta'),
            );
            $retorna = $this->mcartas->subirArchivo($parametros);
            echo json_encode($retorna);
		}	
	}

    public function setcarta() { // Registrar informe PT
		$varnull = '';
		
		$idptcarta 			= $this->input->post('mhdnIdCarta');
		$idptpropuesta		= $this->input->post('cboRegPropu');
		$ccliente 			= $this->input->post('mcboClie');
		$nro_carta 			= $this->input->post('mtxtNrocarta');
		$fecha_carta		= $this->input->post('mtxtFcarta');
		$idresponsable 		= $this->input->post('mcboRespon');
		$descripcion 		= $this->input->post('mtxtDetaCarta');
		$accion 			= $this->input->post('mhdnAccionCarta');
        
        $parametros = array(
            '@idptcarta'		=>  $idptcarta,
            '@idptpropuesta'	=>  $idptpropuesta,
            '@ccliente'			=>  $ccliente,
			'@nro_carta'		=>  $nro_carta,
            '@fecha_carta'		=>  substr($fecha_carta, 6, 4).'-'.substr($fecha_carta,3 , 2).'-'.substr($fecha_carta, 0, 2),
			'@idresponsable'	=>  $idresponsable,
			'@descripcion'		=>  $descripcion,
            '@accion'			=>  $accion
        );
        $retorna = $this->mcartas->setcarta($parametros);
        echo json_encode($retorna);		
	}

    public function getpropucarta() {	// Visualizar NRO propuestas a evaluar en CBO	
		
		$parametros = array(
            '@IDCLIE'   => $this->input->post('ccliente')
        );
		$resultado = $this->mcartas->getcartaevaluar($parametros);
		echo json_encode($resultado);
	}
	        
	public function getbuscardetacarta() {	// Recupera Listado de Documentos Propuestas		

		$parametros = array(
			'@idptcarta' =>  $this->input->post('idptcarta')
		);		
		$resultado = $this->mcartas->getbuscardetacarta($parametros);
		echo json_encode($resultado);
	}
	public function archivo_detcarta() {	// Subir Acrhivo detalle propuesta
        $ANIO           = substr($this->input->post('mtxtfechadetacarta'),-4); 
        $NROCARTA       = $this->input->post('mtxtnrodetacarta');
        $CANTDET        = $this->input->post('mtxtcantdetacarta');
        $NOMBARCH 	    = substr($NROCARTA,0,4).substr($NROCARTA,5,4).'PTFS';
        $RUTAARCH       = 'FTPfileserver/Archivos/10204/VARIOS/'.$ANIO.'/';

        !is_dir($RUTAARCH) && @mkdir($RUTAARCH, 0777, true);
		
		$data = array();
		$cpt = count($_FILES['mtxtDetArchivocarta']['name']);

		for($i=0; $i<$cpt; $i++)
		{
			$_FILES['userFile']['name']= $_FILES['mtxtDetArchivocarta']['name'][$i];
			$_FILES['userFile']['type']= $_FILES['mtxtDetArchivocarta']['type'][$i];
			$_FILES['userFile']['tmp_name']= $_FILES['mtxtDetArchivocarta']['tmp_name'][$i];
			$_FILES['userFile']['error']= $_FILES['mtxtDetArchivocarta']['error'][$i];
			$_FILES['userFile']['size']= $_FILES['mtxtDetArchivocarta']['size'][$i]; 

			//RUTA DONDE SE GUARDAN LOS FICHEROS
			$config['upload_path']      = $RUTAARCH;
			$config['allowed_types']    = 'pdf|xlsx|docx|xls|doc';
			$config['max_size']         = '60048';
			$config['remove_spaces']    = FALSE;
            $config['overwrite'] 		= TRUE;
            $config['file_name']        = 'CARTA-VAR'.$NOMBARCH.'-'.substr('000'.($CANTDET+$i), -3);
			
			$this->load->library('upload',$config);
			$this->upload->initialize($config);

			if ($this->upload->do_upload('userFile')) {
				$fileData =  $this->upload->data(); 
				$data = array("upload_data" => $this->upload->data());
				$datos = array(
					"idptcarta" => $_POST['mtxtiddetacarta'],
					"ruta" => $RUTAARCH,
					"nombarchivo" => $data['upload_data']['file_name'],
					"descarchivo" => $data['upload_data']['client_name']
				);

				if ($this->mcartas->guardar_multiarchivocarta($datos)) {
					//echo "Registro guardado";
					$info[$i] = array(
						"archivo" => $data['upload_data']['file_name'],
						"mensaje" => "Archivo subido y guardado"
					);
				}
				else{
					//echo "Error al intentar guardar la informacion";
					$info[$i] = array(
						"archivo" => $data['upload_data']['file_name'],
						"mensaje" => "Archivo subido pero no guardado "
					);
				}				
			}else{
				$info[$i] = array(
					"archivo" => $_FILES['archivo']['name'],
					"mensaje" => "Archivo no subido ni guardado"
				);
			}	
		}

		$envio = "";
		foreach ($info as $key) {
			$envio .= "Archivo : ".$key['archivo']." - ".$key["mensaje"]."\n";
		}
		echo json_encode($envio);
	}
	public function deldetcarta(){ // Eliminar detalle propuesta				
		$item = $this->input->post('item');
		$respuesta = $this->mcartas->deldetcarta($item);
		echo json_encode($respuesta);									
	}
	

}
?>