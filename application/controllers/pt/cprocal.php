<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Style\Conditional;
use PhpOffice\PhpSpreadsheet\IOFactory;

class Cprocal extends CI_Controller {
	function __construct() {
		parent:: __construct();	
		$this->load->model('pt/mprocal');
		$this->load->model('mglobales');
		$this->load->library('encryption');
		$this->load->helper(array('form','url','download','html','file'));
		$this->load->library('form_validation');
    }
    
   /** CARTAS **/ 	
    public function setprocal() {	// Visualizar Clientes con propuestas en CBO	
        $varnull = '';
		
		$idptprocal 		= $this->input->post('mhdnidprocal');
		$idptprocaldet		= $this->input->post('mhdnidprocaldet');
		$ccliente 			= $this->input->post('cboRegClie');
		$idptpropuesta		= $this->input->post('cboRegPropu');
		$nro_procal 		= $this->input->post('txtmnumeroprocal');
		$fecha_procal 		= $this->input->post('mtxtFprocal');
		$contacto_procal	= $this->input->post('mcboContacprocal');
		$descripcion_procal		= $this->input->post('mtxtDetaprocal');
		$jh 				= $this->input->post('txtmparjh');
		$fh 				= $this->input->post('txtmparfh');
		$f2 				= $this->input->post('txtmparf2');
		$xbh 				= $this->input->post('txtmparxbh');
		$jc 				= $this->input->post('txtmparjc');
		$fc 				= $this->input->post('txtmparfc');
		$f 					= $this->input->post('txtmparf');
		$mg 				= $this->input->post('txtmparmg');
		$accion 			= $this->input->post('mhdnAccionprocal');
        
        $parametros = array(
            '@idptprocal'   		=>  $idptprocal,
            '@idptprocaldet'   		=>  $idptprocaldet,
            '@ccliente'      		=>  $ccliente,
			'@idptpropuesta'    	=>  $idptpropuesta,
			'@nro_procal'    		=>  $nro_procal,
            '@fecha_procal'    		=>  substr($fecha_procal, 6, 4).'-'.substr($fecha_procal,3 , 2).'-'.substr($fecha_procal, 0, 2),
			'@contacto_procal'    	=>  $contacto_procal,
			'@descripcion_procal'   =>  $descripcion_procal,
			'@jh'   =>  $jh,
			'@fh'   =>  $fh,
			'@f2'   =>  $f2,
			'@xbh'  =>  $xbh,
			'@jc'   =>  $jc,
			'@fc'   =>  $fc,
            '@f'    =>  $f,
            '@mg'   =>  $mg,
            '@accion'           	=>  $accion
		);
		
		$resultado = $this->mprocal->setprocal();
		echo json_encode($resultado);
    }
    









    
}
?>