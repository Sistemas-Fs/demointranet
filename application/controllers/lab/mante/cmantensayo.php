<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cmantensayo extends CI_Controller {
	function __construct() {
		parent:: __construct();	
		$this->load->model('lab/mante/mmantensayo');
		$this->load->model('mglobales');
		$this->load->helper(array('form','url','download','html','file'));
		$this->load->library('form_validation');
    }
    
    public function getbuscarensayo() { // Lista de consultas de Ensayos	
        $parametros = array(
            '@ENSAYO' =>  "%".$this->input->post('ensayo')."%"
        );		
        $resultado = $this->mmantensayo->getbuscarensayo($parametros);
        echo json_encode($resultado);
    }
    public function getcboztipoensayo() {	// Visualizar Servicios en CBO	
        $resultado = $this->mmantensayo->getcboztipoensayo();
        echo json_encode($resultado);
    } 
    public function setensayo() { // Guardar Cliente
        $parametros = array(
            '@censayo' 		    =>  $this->input->post('hdnIdcensayo'),
            '@censayofs' 	    =>  $this->input->post('txtCodigofs'),
            '@stipoensayo' 	    =>  $this->input->post('cboSTipoensayo'),
            '@sversion' 	    =>  $this->input->post('cboSVersion'),
            '@zctipoensayo' 	=>  $this->input->post('cboZTipoensayo'),
            '@sacnoac' 	        =>  $this->input->post('cbosacnoac'),
            '@densayo' 	        =>  $this->input->post('txtDensayo'),
            '@unidad_original' 	=>  $this->input->post('txtUM'),
            '@valorvr' 	        =>  $this->input->post('txtValorvr'),
            '@dtitulonorma' 	=>  $this->input->post('txtTituloNorma'),
            '@naniopublicacion' =>  $this->input->post('txtAniopubli'),
            '@dnorma' 	        =>  $this->input->post('txtDnorma'),
            '@accion' 		    =>  $this->input->post('hdnAccregensayo'),
        );
        $retorna = $this->mmantensayo->setensayo($parametros);		
        echo json_encode($retorna);
    }
    public function getcboreglab() {	// Visualizar Servicios en CBO	
        $resultado = $this->mmantensayo->getcboreglab();
        echo json_encode($resultado);
    } 

    public function getlistlaboratorio() { // Lista de consultas de Ensayos	
        $censayo = $this->input->post('censayo');	
        $resultado = $this->mmantensayo->getlistlaboratorio($censayo);
        echo json_encode($resultado);
    }
    public function setlaboratorio() { // Guardar Cliente
        $parametros = array(
            '@claboratorio' =>  $this->input->post('cboreglab'),
            '@censayo' 	    =>  $this->input->post('hdnlabccensayo'),
            '@icosto' 	    =>  $this->input->post('txtlabcosto'),
            '@accion' 	    =>  'N',
        );
        $retorna = $this->mmantensayo->setlaboratorio($parametros);		
        echo json_encode($retorna);
    }

    public function getlistmatriz() { // Lista de consultas de Ensayos	
        $censayo = $this->input->post('censayo');	
        $resultado = $this->mmantensayo->getlistmatriz($censayo);
        echo json_encode($resultado);
    }
    public function setmatriz() { // Guardar Cliente
        $parametros = array(
            '@censayo'  =>  $this->input->post('hdnmatrizccensayo'),
            '@cdensayo' =>  $this->input->post('hdnmatrizcdensayo'),
            '@densayo' 	=>  $this->input->post('txtdescmatriz'),
            '@accion' 	=>  'N',
        );
        $retorna = $this->mmantensayo->setmatriz($parametros);		
        echo json_encode($retorna);
    }
	
	public function setreglab() {	// Registrar Vacaciones		

		$CLABORATORIO	= $this->input->post('mhdnidlaboratorio');
		$DLABORATORIO	= $this->input->post('txtdeslaboratorio');
		$accion		= $this->input->post('mhdnAccionlaboratorio');

		$parametros = array(
			'@CLABORATORIO'	=>  $CLABORATORIO,
			'@DLABORATORIO'	=>  $DLABORATORIO,
			'@accion'	=>  $accion,
		);				
		$respuesta = $this->mmantensayo->setreglab($parametros);
		echo json_encode($respuesta);
    }
    
   
}
?>