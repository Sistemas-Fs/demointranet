<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ccotizacion extends CI_Controller {
	function __construct() {
		parent:: __construct();	
		$this->load->model('lab/coti/mcotizacion');
		$this->load->model('mglobales');
		$this->load->library('encryption');
		$this->load->helper(array('form','url','download','html','file'));
		$this->load->library('form_validation');
    }
    
   /** COTIZACION **/
    public function getcboclieserv() {	// Visualizar Clientes del servicio en CBO	
        $resultado = $this->mcotizacion->getcboclieserv();
        echo json_encode($resultado);
    } 

    public function getbuscarcotizacion() { // Buscar Cotizacion
		$varnull = '';

		$ccliente   = $this->input->post('ccliente');
		$fini       = $this->input->post('fini');
		$ffin       = $this->input->post('ffin');
		$descr      = $this->input->post('descr');
		$estado      = $this->input->post('estado');
		$tieneot      = $this->input->post('tieneot');
		$nroot      = $this->input->post('nroot');
		$nroinf      = $this->input->post('nroinf');
		$activo      = $this->input->post('activo');
        
        $parametros = array(
			'@CCIA'         => '2',
			'@CCLIENTE'     => ($this->input->post('ccliente') == '') ? '0' : $ccliente,
			'@FINI'         => ($this->input->post('fini') == '%') ? NULL : substr($fini, 6, 4).'-'.substr($fini,3 , 2).'-'.substr($fini, 0, 2),
			'@FFIN'         => ($this->input->post('ffin') == '%') ? NULL : substr($ffin, 6, 4).'-'.substr($ffin,3 , 2).'-'.substr($ffin, 0, 2),
			'@DESCR'		=> ($this->input->post('descr') == '') ? '%' : '%'.$descr.'%',
			'@ESTADO'		=> ($this->input->post('estado') == '%') ? '%' : $estado,
			'@TIENEOT'		=> ($this->input->post('tieneot') == '%') ? '%' : $tieneot,
			'@NORDENSERVICIO' => ($this->input->post('nroot') == '') ? '%' : '%'.$nroot.'%',
			'@NROINFORME' 	=> ($this->input->post('nroinf') == '') ? '%' : '%'.$nroinf.'%',
			'@ACTIVO'       => $activo,
        );
        $retorna = $this->mcotizacion->getbuscarcotizacion($parametros);
        echo json_encode($retorna);		
    }
    
    public function getcboregserv() {	// Visualizar Servicios en CBO	
        $resultado = $this->mcotizacion->getcboregserv();
        echo json_encode($resultado);
    } 
    
    public function getcliente() {	// Visualizar Servicios en CBO	
        $resultado = $this->mcotizacion->getcliente();
        echo json_encode($resultado);
    } 
    
    public function getprovcliente() {	// Visualizar Servicios en CBO
        $ccliente   = $this->input->post('ccliente');
        $resultado = $this->mcotizacion->getprovcliente($ccliente);
        echo json_encode($resultado);
    } 
    
    public function getcontaccliente() {	// Visualizar Servicios en CBO
        $ccliente   = $this->input->post('ccliente');
        $resultado = $this->mcotizacion->getcontaccliente($ccliente);
        echo json_encode($resultado);
    } 

    public function setcotizacion() { // Registrar informe PT
		$varnull = '';
		
		$cinternocotizacion 	= $this->input->post('mtxtidcotizacion');
		$nversioncotizacion 	= $this->input->post('mtxtnroversion');
		$fcotizacion 			= $this->input->post('mtxtFcotizacion');
		$dcotizacion 			= $this->input->post('mtxtregnumcoti');
		$scotizacion 		    = $this->input->post('hdnregestado');
		$nvigencia 		        = $this->input->post('mtxtregvigen');
		$csubservicio 		    = $this->input->post('cboregserv');
		$ccliente 		        = $this->input->post('cboregclie');
		$cproveedorcliente 	    = ($this->input->post('cboregprov') == '%') ? '' : $this->input->post('cboregprov');
		$ccontacto 	            = ($this->input->post('cboregcontacto') == '%') ? '' : $this->input->post('cboregcontacto');
		$npermanenciamuestra 	= $this->input->post('mtxtcontramuestra');
		$ntiempoentregainforme 	= $this->input->post('mtxtregentregainf');
		$stiempoentregainforme 	= $this->input->post('txtregtipodias');
		$dobservacion 		    = $this->input->post('mtxtobserv');
		$zctipoformapago 		= $this->input->post('txtregformapagos');
		$dotraformapago 	    = $this->input->post('mtxtregpagotro');
		$ctipocambio 	        = $this->input->post('mtxtregtipopagos');
		$dtipocambio 	        = $this->input->post('mtxtregtipocambio');
		$smuestreo 	            = ($this->input->post('chksmuestreo') == '') ? 'N' : 'S';
		$imuestreo 	            = $this->input->post('txtmontmuestreo');
		$srecojo 	            = ($this->input->post('chksrecojo') == '') ? 'N' : 'S';
		$irecojo 	            = $this->input->post('txtmontrecojo');
		$sgastoadm 	            = ($this->input->post('chksgastoadm') == '') ? 'N' : 'S';
		$igastoadm 	            = $this->input->post('txtmontgastoadm');
		$isubtotal 	            = $this->input->post('txtmontsubtotal');
		$pdescuento 	        = $this->input->post('txtporcdescuento');
		$pigv 	                = 18;
		$digv 	                = $this->input->post('txtporctigv');
		$itotal 	            = $this->input->post('txtmonttotal');
        $smostrarprecios 	    = ($this->input->post('chkregverpago') == '') ? 'N' : 'S';
		$cusuario 			    = $this->input->post('mtxtcusuario');
		$zpermanenciamuestra 	= $this->input->post('mtxtregpermane');
		$tipoinforme 	        = $this->input->post('mcbotipoinforme');
		$tipocertificado 	    = $this->input->post('mcbotipocertificado');
        $concc 	                = ($this->input->post('chkcertifcali') == '') ? 'N' : 'S';
		$accion 			    = $this->input->post('hdnAccionregcoti');
        
        $parametros = array(
            '@cinternocotizacion'   	=>  $cinternocotizacion,
            '@nversioncotizacion'   	=>  $nversioncotizacion,
            '@fcotizacion'      		=>  substr($fcotizacion, 6, 4).'-'.substr($fcotizacion,3 , 2).'-'.substr($fcotizacion, 0, 2),
            '@dcotizacion'    		    =>  $dcotizacion,
			'@scotizacion'    		    =>  $scotizacion,
			'@nvigencia'    	        =>  $nvigencia,
			'@csubservicio'    		    =>  $csubservicio,
			'@ccliente'    		        =>  $ccliente,
			'@cproveedorcliente'   	    =>  $cproveedorcliente,
			'@ccontacto'                =>  $ccontacto,
			'@npermanenciamuestra'    	=>  $npermanenciamuestra,
			'@ntiempoentregainforme'    =>  $ntiempoentregainforme,
			'@stiempoentregainforme'    =>  $stiempoentregainforme,
			'@dobservacion'    		    =>  $dobservacion,
            '@zctipoformapago'      	=>  $zctipoformapago,
            '@dotraformapago'           =>  $dotraformapago,
            '@ctipocambio'              =>  $ctipocambio,
            '@dtipocambio'              =>  $dtipocambio,
            '@smuestreo'                =>  $smuestreo,
            '@imuestreo'                =>  $imuestreo,
            '@srecojo'                  =>  $srecojo,
            '@irecojo'                  =>  $irecojo,
            '@sgastoadm'                =>  $sgastoadm,
            '@igastoadm'                =>  $igastoadm,
            '@isubtotal'                =>  $isubtotal,
            '@pdescuento'               =>  $pdescuento,
            '@pigv'                     =>  $pigv,
            '@digv'                     =>  $digv,
            '@itotal'                   =>  $itotal,
            '@smostrarprecios'          =>  $smostrarprecios,
            '@cusuario'                 =>  $cusuario,
			'@zpermanenciamuestra'    	=>  $zpermanenciamuestra,
			'@zctipoinforme'    	    =>  $tipoinforme,
			'@zctipocerticalidad'    	=>  $tipocertificado,
			'@concc'    	            =>  $concc,
            '@accion'           	    =>  $accion 
        );
        $retorna = $this->mcotizacion->setcotizacion($parametros);
        echo json_encode($retorna);		
	}
    
    public function getlistarproducto() {	// Visualizar Servicios en CBO	
        $idcoti     = $this->input->post('idcoti');
        $nversion   = $this->input->post('nversion');
        $resultado = $this->mcotizacion->getlistarproducto($idcoti,$nversion);
        echo json_encode($resultado);
    } 
    
    public function getcboregLocalclie() {	// Visualizar Servicios en CBO
        $ccliente   = $this->input->post('ccliente');
        $resultado = $this->mcotizacion->getcboregLocalclie($ccliente);
        echo json_encode($resultado);
    }
    
    public function getcboregcondi() {	// Visualizar Servicios en CBO
        $resultado = $this->mcotizacion->getcboregcondi();
        echo json_encode($resultado);
    }  
    
    public function getcboregprocede() {	// Visualizar Servicios en CBO
        $resultado = $this->mcotizacion->getcboregprocede();
        echo json_encode($resultado);
    }  

    public function setproductoxcotizacion() { // Registrar informe PT
		$varnull = '';
		
		$cinternocotizacion 	= $this->input->post('mhdnidcotizacion');
		$nversioncotizacion 	= $this->input->post('mhdnnroversion');
		$nordenproducto 		= $this->input->post('mhdnIdProduc');
		$clocalcliente 			= $this->input->post('mcboregLocalclie');
		$dproducto 		        = $this->input->post('mtxtregProducto');
		$zctipocondicionpdto 	= $this->input->post('mcboregcondicion');
		$nmuestra 		        = $this->input->post('mtxtregmuestra');
		$zctipoprocedencia 		= $this->input->post('mcboregprocedencia');
		$dcantidadminima 	    = $this->input->post('mtxtregcantimin');
		$ctipoproducto 	        = $this->input->post('mcboregoctogono');
		$setiquetanutri 	    = $this->input->post('mcboregetiquetado');
		$ntamanoporcion 	    = $this->input->post('mtxtregtamporci');
		$umporcion 	            = $this->input->post('mcboregumeti');
		$cusuario 			    = $this->input->post('mhdncusuario');
		$accion 			    = $this->input->post('mhdnAccionProduc');
        
        $parametros = array(
            '@cinternocotizacion'   =>  $cinternocotizacion,
            '@nversioncotizacion'   =>  $nversioncotizacion,
            '@nordenproducto'   	=>  $nordenproducto,
            '@clocalcliente'    	=>  $clocalcliente,
			'@dproducto'    		=>  $dproducto,
			'@zctipocondicionpdto'  =>  $zctipocondicionpdto,
			'@nmuestra'    		    =>  $nmuestra,
			'@zctipoprocedencia'    =>  $zctipoprocedencia,
			'@dcantidadminima'   	=>  $dcantidadminima,
			'@ctipoproducto'        =>  $ctipoproducto,
			'@setiquetanutri'    	=>  $setiquetanutri,
			'@ntamanoporcion'       =>  $ntamanoporcion,
			'@umporcion'            =>  $umporcion,
            '@cusuario'             =>  $cusuario,
            '@accion'           	=>  $accion
        );
        $retorna = $this->mcotizacion->setproductoxcotizacion($parametros);
        echo json_encode($retorna);		
	}
    
    public function getlistarensayo() {	// Visualizar Servicios en CBO	
        $idcoti     = $this->input->post('idcoti');
        $nversion   = $this->input->post('nversion');
        $idproduc   = $this->input->post('idproduc');
        $resultado = $this->mcotizacion->getlistarensayo($idcoti,$nversion,$idproduc);
        echo json_encode($resultado);
    } 

    public function setduplicarprodxcoti() { // Registrar informe PT
		$varnull = '';
		
		$cinternocotizacion 	= $this->input->post('idcotizacion');
		$nversioncotizacion 	= $this->input->post('nversion');
		$nordenproducto 		= $this->input->post('idcotiproducto');
        
        $parametros = array(
            '@cinternocotizacion'   =>  $cinternocotizacion,
            '@nversioncotizacion'   =>  $nversioncotizacion,
            '@nordenproducto'   	=>  $nordenproducto
        );
        $retorna = $this->mcotizacion->setduplicarprodxcoti($parametros);
        echo json_encode($retorna);		
	}

    public function precioxcoti() { // Registrar informe PT
		
		$cinternocotizacion 	= $this->input->post('idcotizacion');
		$nversioncotizacion 	= $this->input->post('nversion');
		$smostrarprecios 	= $this->input->post('smostrarprecios');
        
        $retorna = $this->mcotizacion->precioxcoti($cinternocotizacion,$nversioncotizacion,$smostrarprecios);
        echo json_encode($retorna);		
	}

    public function deleteprodxcoti() { // Registrar informe PT
		$varnull = '';
		
		$cinternocotizacion 	= $this->input->post('idcotizacion');
		$nversioncotizacion 	= $this->input->post('nversion');
		$nordenproducto 		= $this->input->post('idcotiproducto');
        
        $parametros = array(
            '@cinternocotizacion'   =>  $cinternocotizacion,
            '@nversioncotizacion'   =>  $nversioncotizacion,
            '@nordenproducto'   	=>  $nordenproducto
        );
        $retorna = $this->mcotizacion->deleteprodxcoti($parametros);
        echo json_encode($retorna);		
	}

    public function getbuscarensayos() { // Buscar Cotizacion
		$varnull = '';

		$descripcion   = $this->input->post('descripcion');
		$sacnoac       = $this->input->post('sacnoac');
		$tipoensayo       = $this->input->post('tipoensayo');
        
        $parametros = array(
			'@descripcion'		=> ($this->input->post('descripcion') == '') ? '%' : '%'.$descripcion.'%',
			'@sacnoac'          => ($this->input->post('sacnoac') == '') ? '%' : $sacnoac,
			'@tipoensayo'       => $tipoensayo,
        );
        $retorna = $this->mcotizacion->getbuscarensayos($parametros);
        echo json_encode($retorna);		
    }

    public function setregensayoxprod() { // Registrar informe PT
		$varnull = '';
		
		$cinternocotizacion 	= $this->input->post('hdnmIdcoti');
		$nversioncotizacion 	= $this->input->post('hdnmNvers');
		$nordenproducto 		= $this->input->post('hdnmIdprod');
		$censayo 		        = $this->input->post('mhdnmcensayo');
		$claboratorio 		    = $this->input->post('mtxtmCLab');
		$nvias 		            = $this->input->post('mtxtmvias');
		$icostoclienteparcial 	= $this->input->post('mtxtmCosto');
		$censayoedit 	        = $this->input->post('mhdnmcensayoedit');
		$editclase 	            = $this->input->post('mhdnmeditclase');
		$accion 		        = $this->input->post('hdnmAccion');
        
        $parametros = array(
            '@cinternocotizacion'   =>  $cinternocotizacion,
            '@nversioncotizacion'   =>  $nversioncotizacion,
            '@nordenproducto'   	=>  $nordenproducto,
            '@censayo'   	        =>  $censayo,
            '@claboratorio'   	    =>  $claboratorio,
            '@nvias'   	            =>  $nvias,
            '@icostoclienteparcial' =>  $icostoclienteparcial,
            '@censayoedit'          =>  $censayoedit,
            '@editclase'            =>  $editclase,
            '@accion'   	        =>  $accion
        );
        $retorna = $this->mcotizacion->setregensayoxprod($parametros);
        echo json_encode($retorna);		
	}

    public function seteditensayoxprod_coti() { // Registrar informe PT
		$varnull = '';
		$id_ser = $this->input->post('ID');
        $id_array = preg_split("/;/",$id_ser);

        $cinternocotizacion     = $id_array[0];
		$nordenproducto 	    = $id_array[1];
        $censayo 	            = $id_array[2];
        $accion                 = $this->input->post('action');
        $icostoclienteparcial   = $this->input->post('CONSTOENSAYO');
        $nvias 	                = $this->input->post('NVIAS');

        if ($accion == 'edit') {
            if(isset($icostoclienteparcial)) {
                $parametros = array(
                    'cinternocotizacion'     =>  $cinternocotizacion,
                    'nordenproducto'      		=>  $nordenproducto,
                    'censayo'    		        =>  $censayo,
                    'icostoclienteparcial'    	=>  $icostoclienteparcial,
                );
                
                $retorna = $this->mcotizacion->seteditensayoxprod_costo($parametros);
            } else if(isset($nvias)) {
                $parametros = array(
                    'cinternocotizacion'     =>  $cinternocotizacion,
                    'nordenproducto'      		=>  $nordenproducto,
                    'censayo'    		        =>  $censayo,
                    'nvias'                     =>  $nvias,
                );
                
                $retorna = $this->mcotizacion->seteditensayoxprod_via($parametros);
            }
        }
       
        echo json_encode($retorna);		
	}

    public function seteditensayoxprod() { // Registrar informe PT
		$varnull = '';
		
		$cinternocotizacion 	= $this->input->post('ehdnmIdcoti');
		$nversioncotizacion 	= $this->input->post('ehdnmNvers');
		$nordenproducto 		= $this->input->post('ehdnmIdprod');
		$censayo 		        = $this->input->post('ehdnmcensayo');
		$claboratorio 		    = $this->input->post('etxtmCLab');
		$nvias 		            = $this->input->post('etxtmvias');
		$icostoclienteparcial 	= $this->input->post('etxtmCosto');
		$accion 		        = $this->input->post('ehdnmAccion');
        
        $parametros = array(
            '@cinternocotizacion'   =>  $cinternocotizacion,
            '@nversioncotizacion'   =>  $nversioncotizacion,
            '@nordenproducto'   	=>  $nordenproducto,
            '@censayo'   	        =>  $censayo,
            '@claboratorio'   	    =>  $claboratorio,
            '@nvias'   	            =>  $nvias,
            '@icostoclienteparcial' =>  $icostoclienteparcial,
            '@accion'   	        =>  $accion
        );
        $retorna = $this->mcotizacion->setregensayoxprod($parametros);
        echo json_encode($retorna);		
	}

    public function deleteensayoxprod() { // Registrar informe PT
		$varnull = '';
		
		$cinternocotizacion 	= $this->input->post('idcotizacion');
		$nversioncotizacion 	= $this->input->post('nversion');
		$nordenproducto 		= $this->input->post('idcotiproducto');
		$censayo 		        = $this->input->post('censayo');
        
        $parametros = array(
            '@cinternocotizacion'   =>  $cinternocotizacion,
            '@nversioncotizacion'   =>  $nversioncotizacion,
            '@nordenproducto'   	=>  $nordenproducto,
            '@censayo'   	        =>  $censayo
        );
        $retorna = $this->mcotizacion->deleteensayoxprod($parametros);
        echo json_encode($retorna);		
	}

    public function cerrarcotizacion() { // Registrar informe PT
		$varnull = '';
		
		$cinternocotizacion 	= $this->input->post('idcotizacion');
		$nversioncotizacion 	= $this->input->post('nversion');
        
        $parametros = array(
            '@cinternocotizacion'   =>  $cinternocotizacion,
            '@nversioncotizacion'   =>  $nversioncotizacion
        );
        $retorna = $this->mcotizacion->cerrarcotizacion($parametros);
        echo json_encode($retorna);		
	}

    public function abrircotizacion() { // Registrar informe PT
		$varnull = '';
		
		$cinternocotizacion 	= $this->input->post('idcotizacion');
		$nversioncotizacion 	= $this->input->post('nversion');
        
        $parametros = array(
            '@cinternocotizacion'   =>  $cinternocotizacion,
            '@nversioncotizacion'   =>  $nversioncotizacion
        );
        $retorna = $this->mcotizacion->abrircotizacion($parametros);
        echo json_encode($retorna);		
	}
    
    public function getmcbobustipoensayo() {	// Visualizar Servicios en CBO
        $resultado = $this->mcotizacion->getmcbobustipoensayo();
        echo json_encode($resultado);
    }  

    public function exportexcellistcoti(){
		/*Estilos */
		   $titulo = [
			   'font'	=> [
				   'name' => 'Arial',
				   'size' =>12,
				   'color' => array('rgb' => 'FFFFFF'),
				   'bold' => true,
			   ], 
			   'fill'	=>[
				   'fillType' => Fill::FILL_SOLID,
				   'startColor' => [
					   'rgb' => '042C5C'
				   ]
			   ],
			   'borders'	=>[
				   'allBorders' => [
					   'borderStyle' => Border::BORDER_THIN,
					   'color' => [ 
						   'rgb' => '000000'
					   ]
				   ]
			   ],
			   'alignment' => [
				   'horizontal' => Alignment::HORIZONTAL_CENTER,
				   'vertical' => Alignment::VERTICAL_CENTER,
				   'wrapText' => true,
			   ],
		   ];
		   $cabecera = [
			   'font'	=> [
				   'name' => 'Arial',
				   'size' =>10,
				   'color' => array('rgb' => 'FFFFFF'),
				   'bold' => true,
			   ], 
			   'fill'	=>[
				   'fillType' => Fill::FILL_SOLID,
				   'startColor' => [
					   'rgb' => '042C5C'
				   ]
			   ],
			   'borders'	=>[
				   'allBorders' => [
					   'borderStyle' => Border::BORDER_THIN,
					   'color' => [ 
						   'rgb' => '000000'
					   ]
				   ]
			   ],
			   'alignment' => [
				   'horizontal' => Alignment::HORIZONTAL_CENTER,
				   'vertical' => Alignment::VERTICAL_CENTER,
				   'wrapText' => true,
			   ],
		   ];
		   $celdastexto = [
			   'borders'	=>[
				   'allBorders' => [
					   'borderStyle' => Border::BORDER_THIN,
					   'color' => [ 
						   'rgb' => '000000'
					   ]
				   ]
			   ],
			   'alignment' => [
				   'horizontal' => Alignment::HORIZONTAL_LEFT,
				   'vertical' => Alignment::VERTICAL_CENTER,
				   'wrapText' => true,
			   ],
		   ];
           $celdastextocentro = [
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                    'vertical' => Alignment::VERTICAL_CENTER,
                    'wrapText' => true,
                ],
           ];			
           $celdasnumerodec = [
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_RIGHT,
                    'vertical' => Alignment::VERTICAL_CENTER,
                    'wrapText' => true,
                ],
                'numberFormat' => [
                    'formatCode' => '#,##0.0',
                ],
           ];		
           $celdasnumero = [
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_RIGHT,
                    'vertical' => Alignment::VERTICAL_CENTER,
                    'wrapText' => true,
                ],
                'numberFormat' => [
                    'formatCode' => '#,##0',
                ],
           ];
        /*Estilos */	
        
        $varnull = '';

		$ccliente   = $this->input->post('cboclieserv');
		$chkFreg       = $this->input->post('chkFreg');
		$fini       = $this->input->post('txtFIni');
		$ffin       = $this->input->post('txtFFin');
		$descr      = $this->input->post('txtdescri');
		$estado      = $this->input->post('cboestado');
		$tieneot      = $this->input->post('cbotieneot');
		$activo      = $this->input->post('swVigencia');
        
        $parametros = array(
			'@CCIA'         => '2',
			'@CCLIENTE'     => ($this->input->post('cboclieserv') == '') ? '0' : $ccliente,
			'@FINI'         => ($this->input->post('chkFreg') == NULL) ? NULL : substr($fini, 6, 4).'-'.substr($fini,3 , 2).'-'.substr($fini, 0, 2),
			'@FFIN'         => ($this->input->post('chkFreg') == NULL) ? NULL : substr($ffin, 6, 4).'-'.substr($ffin,3 , 2).'-'.substr($ffin, 0, 2),
			'@DESCR'		=> ($this->input->post('txtdescri') == '') ? '%' : '%'.$descr.'%',
			'@ESTADO'		=> ($this->input->post('cboestado') == '%') ? '%' : $estado,
			'@TIENEOT'		=> ($this->input->post('cbotieneot') == '%') ? '%' : $tieneot,
			'@ACTIVO'       => ($this->input->post('swVigencia') == NULL) ? 'I' : 'A',
        );

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $spreadsheet->getDefaultStyle()
            ->getFont()
            ->setName('Arial')
            ->setSize(9);
        
        $sheet->setCellValue('A1', 'LISTADO DE COTIZACIONES')
			->mergeCells('A1:S1')
			->setCellValue('A3', 'Fecha Cotización')
			->setCellValue('B3', 'Nro Cotización')
			->setCellValue('C3', 'Estado Coti.')
			->setCellValue('D3', 'Cliente')
			->setCellValue('E3', 'Proveedor')
			->setCellValue('F3', 'Contacto')
			->setCellValue('G3', 'Elaborado por')
			->setCellValue('H3', 'Tipo de Pago')
			->setCellValue('I3', 'Moneda')
			->setCellValue('J3', 'Muestreo')
			->setCellValue('K3', 'Sub Total')
			->setCellValue('L3', 'Descuento %')
			->setCellValue('M3', 'Descuento')
			->setCellValue('N3', 'Monto sin IGV')
			->setCellValue('O3', 'IGV')
			->setCellValue('P3', 'Monto Total')
			->setCellValue('Q3', 'Nro OT')
			->setCellValue('R3', 'Fecha OT')
			->setCellValue('S3', 'Observación');

        $sheet->getStyle('A1:S1')->applyFromArray($titulo);
        $sheet->getStyle('A3:S3')->applyFromArray($cabecera);
		
		$sheet->getColumnDimension('A')->setAutoSize(false)->setWidth(12.10);
		$sheet->getColumnDimension('B')->setAutoSize(false)->setWidth(19.10);
		$sheet->getColumnDimension('C')->setAutoSize(false)->setWidth(10.10);
		$sheet->getColumnDimension('D')->setAutoSize(false)->setWidth(54.10);
		$sheet->getColumnDimension('E')->setAutoSize(false)->setWidth(54.10);
		$sheet->getColumnDimension('F')->setAutoSize(false)->setWidth(35.10);
		$sheet->getColumnDimension('G')->setAutoSize(false)->setWidth(35.10);
		$sheet->getColumnDimension('H')->setAutoSize(false)->setWidth(17.10);
		$sheet->getColumnDimension('I')->setAutoSize(false)->setWidth(9.10);
		$sheet->getColumnDimension('J')->setAutoSize(false)->setWidth(10.10);
		$sheet->getColumnDimension('K')->setAutoSize(false)->setWidth(12.10);
		$sheet->getColumnDimension('L')->setAutoSize(false)->setWidth(11.10);
		$sheet->getColumnDimension('M')->setAutoSize(false)->setWidth(11.10);
		$sheet->getColumnDimension('N')->setAutoSize(false)->setWidth(12.10);
		$sheet->getColumnDimension('O')->setAutoSize(false)->setWidth(11.10);
		$sheet->getColumnDimension('P')->setAutoSize(false)->setWidth(12.10);
		$sheet->getColumnDimension('Q')->setAutoSize(false)->setWidth(19.10);
		$sheet->getColumnDimension('R')->setAutoSize(false)->setWidth(12.10);
        $sheet->getColumnDimension('S')->setAutoSize(false)->setWidth(72.10);
        
        $sheet->getStyle('Q')->getAlignment()->setWrapText(true);
        $sheet->getStyle('S')->getAlignment()->setWrapText(true);

		$rpt = $this->mcotizacion->getexcellistcoti($parametros);
		$irow = 4;
        if ($rpt){
        	foreach($rpt as $row){
                $DFECHA     = $row->DFECHA;
                $NROCOTI = $row->NROCOTI;
                $DESTADO = $row->DESTADO;
                $DCLIENTE = $row->DCLIENTE;
                $CPROVEEDOR = $row->CPROVEEDOR;
                $CONTACTO = $row->CONTACTO;
                $ELABORADO = $row->ELABORADO;
                $TIPOPAGO = $row->TIPOPAGO;
                $MONEDA = $row->MONEDA;
                $IMUESTREO = $row->IMUESTREO;
                $ISUBTOTAL = $row->ISUBTOTAL;
                $PDESCUENTO = $row->PDESCUENTO;
                $DDESCUENTO = $row->DDESCUENTO;
                $MONTOSINIGV = $row->MONTOSINIGV;
                $DIGV = $row->DIGV;
                $ITOTAL = $row->ITOTAL;
                $NROOT = $row->NROOT;
                $FOT = $row->FOT;
                $OBSERVA = $row->OBSERVA;

                $sheet->setCellValue('A'.$irow,$DFECHA);
                $sheet->setCellValue('B'.$irow,$NROCOTI);
                $sheet->setCellValue('C'.$irow,$DESTADO);
                $sheet->setCellValue('D'.$irow,$DCLIENTE);
                $sheet->setCellValue('E'.$irow,$CPROVEEDOR);
                $sheet->setCellValue('F'.$irow,$CONTACTO);
                $sheet->setCellValue('G'.$irow,$ELABORADO);
                $sheet->setCellValue('H'.$irow,$TIPOPAGO);
                $sheet->setCellValue('I'.$irow,$MONEDA);
                $sheet->setCellValue('J'.$irow,$IMUESTREO);
                $sheet->setCellValue('K'.$irow,$ISUBTOTAL);
                $sheet->setCellValue('L'.$irow,$PDESCUENTO);
                $sheet->setCellValue('M'.$irow,$DDESCUENTO);
                $sheet->setCellValue('N'.$irow,$MONTOSINIGV);
                $sheet->setCellValue('O'.$irow,$DIGV);
                $sheet->setCellValue('P'.$irow,$ITOTAL);
                $sheet->setCellValue('Q'.$irow,$NROOT);
                $sheet->setCellValue('R'.$irow,$FOT);
                $sheet->setCellValue('S'.$irow,$OBSERVA);

				$irow++;
			}
        }
        $posfin = $irow - 1;

        $sheet->getStyle('A4:S'.$posfin)->applyFromArray($celdastexto);
        $sheet->getStyle('A4:A'.$posfin)->applyFromArray($celdastextocentro);
        $sheet->getStyle('J4:K'.$posfin)->applyFromArray($celdasnumerodec);
        $sheet->getStyle('L4:L'.$posfin)->applyFromArray($celdasnumero);
        $sheet->getStyle('M4:P'.$posfin)->applyFromArray($celdasnumerodec);
        $sheet->getStyle('I4:I'.$posfin)->applyFromArray($celdastextocentro);
        
		$sheet->setTitle('Listado - Cotización');            
		$writer = new Xlsx($spreadsheet);
		$filename = 'listCotizacion-'.time();
		ob_end_clean();
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
		header('Cache-Control: max-age=0');

		$writer->save('php://output');
    }

    public function setduplicarcoti() { // Registrar informe PT
		$varnull = '';
		
		$cinternocotizacion 	= $this->input->post('idcotizacion');
		$nversioncotizacion 	= $this->input->post('nversion');
		$cusuariocrea 	        = $this->input->post('cusuario');
        
        $parametros = array(
            '@cinternocotizacion'   =>  $cinternocotizacion,
            '@nversioncotizacion'   =>  $nversioncotizacion,
            '@cusuariocrea'         =>  $cusuariocrea
        );
        $retorna = $this->mcotizacion->setduplicarcoti($parametros);
        echo json_encode($retorna);		
	}
    
    public function getlistarot() {	// Visualizar Servicios en CBO	
        $idcoti     = $this->input->post('idcoti');
        $resultado = $this->mcotizacion->getlistarot($idcoti);
        echo json_encode($resultado);
    } 
    
    public function getlistarinf() {	// Visualizar Servicios en CBO	
        $idcoti     = $this->input->post('idcoti');
        $tipoinf     = $this->input->post('tipoinf');
        $resultado = $this->mcotizacion->getlistarinf($idcoti,$tipoinf);
        echo json_encode($resultado);
    } 
}
?>