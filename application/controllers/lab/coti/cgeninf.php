<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cgeninf extends CI_Controller {
	function __construct() {
		parent:: __construct();	
		$this->load->model('lab/coti/mgeninf');
		$this->load->model('mglobales');
		$this->load->library('encryption');
		$this->load->helper(array('form','url','download','html','file'));
		$this->load->library('form_validation');
    }
    
    public function generarformatos() { // Buscar Cotizacion
		$varnull = '';

		$ccliente       = $this->input->post('ccliente');
		$descripcion    = $this->input->post('descripcion');
		$tipobuscar     = $this->input->post('tipobuscar');
        
        $parametros = array(
			'@ccliente'         => $ccliente,
			'@descripcion'		=> ($this->input->post('descripcion') == '') ? '%' : '%'.$descripcion.'%',
			'@tipobuscar'       => $tipobuscar,
        );
        $retorna = $this->mgeninf->generarformatos($parametros);
        echo json_encode($retorna);		
    }
    
    public function detgenerarformatos() { // Buscar Cotizacion
		$varnull = '';

		$cinternoordenservicio = $this->input->post('cinternoordenservicio');
		$tipoinforme        = $this->input->post('zctipoinforme');
        
        $parametros = array(
			'@cinternoordenservicio'   => $cinternoordenservicio,
			'@tipoinforme'          => $tipoinforme,
        );
        $retorna = $this->mgeninf->detgenerarformatos($parametros);
        echo json_encode($retorna);		
    }

    public function setgeninforme() {	// Visualizar Maquilador por proveedor en CBO	
        
		$cinternocotizacion 	= $this->input->post('cinternocotizacion');
		$cinternoordenservicio	= $this->input->post('cinternoordenservicio');
		$zctipoinforme        	= $this->input->post('zctipoinforme');
        
        $parametros = array(
			'@cinternocotizacion'   => $cinternocotizacion,
			'@cinternoordenservicio'          => $cinternoordenservicio,
			'@zctipoinforme'          => $zctipoinforme,
        );
		$resultado = $this->mgeninf->setgeninforme($parametros);
		echo json_encode($resultado);
	}

    public function setgeninformepersonalizado() {	// Visualizar Maquilador por proveedor en CBO	
        
		$idlabinformes 	= $this->input->post('idlabinformes');
		$cinternoordenservicio	= $this->input->post('cinternoordenservicio');
		$concc	= $this->input->post('concc');
        
        $parametros = array(
			'@idlabinformes'   => $idlabinformes,
			'@cinternoordenservicio'          => $cinternoordenservicio,
			'@concc'          => $concc,
        );
		$resultado = $this->mgeninf->setgeninformepersonalizado($parametros);
		echo json_encode($resultado);
	}

    public function setgencertificado() {	// Visualizar Maquilador por proveedor en CBO	
        
		$cinternocotizacion 	= $this->input->post('cinternocotizacion');
		$cinternoordenservicio	= $this->input->post('cinternoordenservicio');
		$zctipocertificado        	= $this->input->post('zctipocertificado');
        
        $parametros = array(
			'@cinternocotizacion'   => $cinternocotizacion,
			'@cinternoordenservicio'          => $cinternoordenservicio,
			'@zctipocertificado'          => $zctipocertificado,
        );
		$resultado = $this->mgeninf->setgencertificado($parametros);
		echo json_encode($resultado);
	}

	public function cambiartipoinf() {	// Actualizar estado de propuesta
		$parametros = array(
			'CINTERNOCOTIZACION' =>  $this->input->post('idcoti'),
			'ZCTIPOINFORME' =>  $this->input->post('idtipo')
		);	
		$resultado =  $this->mgeninf->cambiartipoinf($parametros);
		echo json_encode($resultado);

	}
    
    public function getlistensayosperso() { // Buscar Cotizacion
		$varnull = '';

		$cinternoordenservicio	= $this->input->post('cinternoordenservicio');
		$cmuestra	= $this->input->post('cmuestra');
        
        $parametros = array(
			'@cinternoordenservicio'         => $cinternoordenservicio,
			'@cmuestra'         => $cmuestra,
        );
        $retorna = $this->mgeninf->getlistensayosperso($parametros);
        echo json_encode($retorna);		
	}
	
    public function setidinforme() {	// Visualizar Maquilador por proveedor en CBO	
        
		$cinternoordenservicio	= $this->input->post('cinternoordenservicio');
        
        $parametros = array(
			'@cinternoordenservicio' => $cinternoordenservicio,
        );
		$resultado = $this->mgeninf->setidinforme($parametros);
		echo json_encode($resultado);
	}

    public function geninfoseleccionados() {	// Visualizar Maquilador por proveedor en CBO	
        
		$cinternoordenservicio	= $this->input->post('cinternoordenservicio');
		$cinternocotizacion		= $this->input->post('cinternocotizacion');
		$nordenproducto			= $this->input->post('nordenproducto');
		$cmuestra        		= $this->input->post('cmuestra');
		$censayo        		= $this->input->post('censayo');
		$nviausado        		= $this->input->post('nviausado');
		$idlabinformes        	= $this->input->post('idlabinformes');
        
        $parametros = array(
			'@cinternoordenservicio'	=> $cinternoordenservicio,
			'@cinternocotizacion'		=> $cinternocotizacion,
			'@nordenproducto'			=> $nordenproducto,
			'@cmuestra'   				=> $cmuestra,
			'@censayo'          		=> $censayo,
			'@nviausado'          		=> $nviausado,
			'@idlabinformes'          	=> $idlabinformes,
        );
		$resultado = $this->mgeninf->geninfoseleccionados($parametros);
		echo json_encode($resultado);
	}
    
	public function cbogenmuestras() {	// Visualizar Servicios en CBO
		$cordenservicio   = $this->input->post('cordenservicio');
        $resultado = $this->mgeninf->cbogenmuestras($cordenservicio);
        echo json_encode($resultado);
	} 
	
	public function getcbomuestrasborrador() {
		$cinternoordenservicio   = $this->input->post('cinternoordenservicio');
        $resultado = $this->mgeninf->getcbomuestrasborrador($cinternoordenservicio);
        echo json_encode($resultado);
	} 
}
?>