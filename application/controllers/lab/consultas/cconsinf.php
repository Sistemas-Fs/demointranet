<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cconsinf extends CI_Controller {
	function __construct() {
		parent:: __construct();	
		$this->load->model('lab/consultas/mconsinf');
		$this->load->model('mglobales');
		$this->load->library('encryption');
		$this->load->helper(array('form','url','download','html','file'));
		$this->load->library('form_validation');
    }
    
    public function consultaformatos() { // Buscar Cotizacion
		$varnull = '';

		$ccliente       = $this->input->post('ccliente');
		$tipobuscar     = $this->input->post('tipobuscar');
		$descripcion    = $this->input->post('descripcion');
		$fechabuscar	= $this->input->post('fechabuscar');
		$fini       	= $this->input->post('fini');
		$ffin       	= $this->input->post('ffin');
        
        $parametros = array(
			'@ccliente'		=> $ccliente,
			'@tipobuscar'	=> $tipobuscar,
			'@descripcion'	=> ($this->input->post('descripcion') == '') ? '%' : '%'.$descripcion.'%',
			'@fechabuscar'	=> $fechabuscar,
			'@FINI'         => ($this->input->post('fini') == '%') ? NULL : substr($fini, 6, 4).'-'.substr($fini,3 , 2).'-'.substr($fini, 0, 2),
			'@FFIN'         => ($this->input->post('ffin') == '%') ? NULL : substr($ffin, 6, 4).'-'.substr($ffin,3 , 2).'-'.substr($ffin, 0, 2),
        );
        $retorna = $this->mconsinf->consultaformatos($parametros);
        echo json_encode($retorna);		
    }
    
    public function detconsultaformatos() { // Buscar Cotizacion
		$varnull = '';

		$cinternoordenservicio = $this->input->post('cinternoordenservicio');
		$tipoinforme        = $this->input->post('zctipoinforme');
        
        $parametros = array(
			'@cinternoordenservicio'   => $cinternoordenservicio,
			'@tipoinforme'          => $tipoinforme,
        );
        $retorna = $this->mconsinf->detconsultaformatos($parametros);
        echo json_encode($retorna);		
    }
    
}
?>