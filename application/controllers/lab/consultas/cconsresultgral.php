<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Style\Conditional;
use PhpOffice\PhpSpreadsheet\Writer\IWriter;
use PhpOffice\PhpSpreadsheet\IOFactory;

class Cconsresultgral extends CI_Controller {
	function __construct() {
		parent:: __construct();	
		$this->load->model('lab/consultas/mconsresultgral');
		$this->load->model('mglobales');
		$this->load->library('encryption');
		$this->load->helper(array('form','url','download','html','file'));
		$this->load->library('form_validation');
    }
    
    public function resultadosgeneral() { // Buscar Cotizacion
		$varnull = '';

		$ccliente       = $this->input->post('ccliente');
		$dcotizacion    = $this->input->post('dcotizacion');
		$fini       	= $this->input->post('fini');
		$ffin       	= $this->input->post('ffin');
        
        $parametros = array(
			'@ccliente'		=> $ccliente,
			'@dcotizacion'	=> ($this->input->post('dcotizacion') == '') ? '%' : '%'.$dcotizacion.'%',
			'@fini'         => ($this->input->post('fini') == '%') ? NULL : substr($fini, 6, 4).'-'.substr($fini,3 , 2).'-'.substr($fini, 0, 2),
			'@ffin'         => ($this->input->post('ffin') == '%') ? NULL : substr($ffin, 6, 4).'-'.substr($ffin,3 , 2).'-'.substr($ffin, 0, 2),
        );
        $retorna = $this->mconsresultgral->resultadosgeneral($parametros);
        echo json_encode($retorna);		
	}
	
	public function getlistelementos() { // 

        $varnull = '';
		
        $cinternoordenservicio = $this->input->post('cinternoordenservicio');
        $cinternocotizacion = $this->input->post('cinternocotizacion');
        $nordenproducto = $this->input->post('nordenproducto');
        $cmuestra = $this->input->post('cmuestra');
        $censayo = $this->input->post('censayo');
        $nviausado = $this->input->post('nviausado');
        
        $parametros = array(
			'@cinternoordenservicio'    => $cinternoordenservicio,
			'@cinternocotizacion'    => $cinternocotizacion,
			'@nordenproducto'    => $nordenproducto,
			'@cmuestra'    => $cmuestra,
			'@censayo'    => $censayo,
			'@nviausado'    => $nviausado,
        );
        $retorna = $this->mconsresultgral->getlistelementos($parametros);
        echo json_encode($retorna);	
    }
    

    public function excelresultgral(){
		/*Estilos */
		   $titulo = [
			   'font'	=> [
				   'name' => 'Arial',
				   'size' =>12,
				   'color' => array('rgb' => 'FFFFFF'),
				   'bold' => true,
			   ], 
			   'fill'	=>[
				   'fillType' => Fill::FILL_SOLID,
				   'startColor' => [
					   'rgb' => '042C5C'
				   ]
			   ],
			   'borders'	=>[
				   'allBorders' => [
					   'borderStyle' => Border::BORDER_THIN,
					   'color' => [ 
						   'rgb' => '000000'
					   ]
				   ]
			   ],
			   'alignment' => [
				   'horizontal' => Alignment::HORIZONTAL_CENTER,
				   'vertical' => Alignment::VERTICAL_CENTER,
				   'wrapText' => true,
			   ],
		   ];
		   $cabecera = [
			   'font'	=> [
				   'name' => 'Arial',
				   'size' =>10,
				   'color' => array('rgb' => 'FFFFFF'),
				   'bold' => true,
			   ], 
			   'fill'	=>[
				   'fillType' => Fill::FILL_SOLID,
				   'startColor' => [
					   'rgb' => '042C5C'
				   ]
			   ],
			   'borders'	=>[
				   'allBorders' => [
					   'borderStyle' => Border::BORDER_THIN,
					   'color' => [ 
						   'rgb' => '000000'
					   ]
				   ]
			   ],
			   'alignment' => [
				   'horizontal' => Alignment::HORIZONTAL_CENTER,
				   'vertical' => Alignment::VERTICAL_CENTER,
				   'wrapText' => true,
			   ],
		   ];
		   $celdastexto = [
			   'borders'	=>[
				   'allBorders' => [
					   'borderStyle' => Border::BORDER_THIN,
					   'color' => [ 
						   'rgb' => '000000'
					   ]
				   ]
			   ],
			   'alignment' => [
				   'horizontal' => Alignment::HORIZONTAL_LEFT,
				   'vertical' => Alignment::VERTICAL_CENTER,
				   'wrapText' => true,
			   ],
		   ];
           $celdastextocentro = [
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                    'vertical' => Alignment::VERTICAL_CENTER,
                    'wrapText' => true,
                ],
           ];
           $celdastextoderecha = [
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_RIGHT,
                    'vertical' => Alignment::VERTICAL_CENTER,
                    'wrapText' => true,
                ],
           ];			
           $celdasnumerodec = [
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_RIGHT,
                    'vertical' => Alignment::VERTICAL_CENTER,
                    'wrapText' => true,
                ],
                'numberFormat' => [
                    'formatCode' => '#,##0.0',
                ],
           ];		
           $celdasnumero = [
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_RIGHT,
                    'vertical' => Alignment::VERTICAL_CENTER,
                    'wrapText' => true,
                ],
                'numberFormat' => [
                    'formatCode' => '#,##0',
                ],
           ];		
           $celdasexponente = [
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_LEFT,
                    'vertical' => Alignment::VERTICAL_TOP,
                    'wrapText' => true,
                ],
                'numberFormat' => [
                    'formatCode' => '##0',
                ],
				'font'	=> [
					'name' => 'Arial',
					'size' =>9,
				], 
           ];
        /*Estilos */	
        
        $varnull = '';

		$ccliente      = $this->input->post('cboclieserv');
		$dcotizacion   = $this->input->post('txtbuscar');
		$fini       = $this->input->post('txtFIni');
		$ffin       = $this->input->post('txtFFin');
                 
        $parametros = array(
			'@ccliente'		=> $ccliente,
			'@dcotizacion'	=> ($this->input->post('dcotizacion') == '') ? '%' : '%'.$dcotizacion.'%',
			'@fini'         => ($this->input->post('fini') == '%') ? NULL : substr($fini, 6, 4).'-'.substr($fini,3 , 2).'-'.substr($fini, 0, 2),
			'@ffin'         => ($this->input->post('ffin') == '%') ? NULL : substr($ffin, 6, 4).'-'.substr($ffin,3 , 2).'-'.substr($ffin, 0, 2),
        );

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $spreadsheet->getDefaultStyle()
            ->getFont()
            ->setName('Arial')
            ->setSize(9);
        
        $sheet->setCellValue('A1', 'RESULTADOS GENERALES')
			->mergeCells('A1:K1')
			->setCellValue('A3', '#')
			->setCellValue('B3', 'COTIZACION')
			->setCellValue('C3', 'FECHA DE RECEPCION')
			->setCellValue('D3', 'NOMBRE PRODUCTO')
			->setCellValue('E3', 'PRESENTACION')
			->setCellValue('F3', 'CONDICION')
			->setCellValue('G3', 'ENSAYO')
			->setCellValue('H3', 'ELEMENTO')
			->setCellValue('I3', 'VIA')
			->setCellValue('J3', 'RESULTADO')
			->setCellValue('K3', 'EXPONENTE')
			->setCellValue('L3', 'UNIDAD')
			->setCellValue('M3', 'ESTADO');

        $sheet->getStyle('A1:M1')->applyFromArray($titulo);
        $sheet->getStyle('A3:M3')->applyFromArray($cabecera);
		
		$sheet->getColumnDimension('A')->setAutoSize(false)->setWidth(7.10);
		$sheet->getColumnDimension('B')->setAutoSize(false)->setWidth(20.10);
		$sheet->getColumnDimension('C')->setAutoSize(false)->setWidth(18.10);
		$sheet->getColumnDimension('D')->setAutoSize(false)->setWidth(50.10);
		$sheet->getColumnDimension('E')->setAutoSize(false)->setWidth(50.10);
		$sheet->getColumnDimension('F')->setAutoSize(false)->setWidth(20.10);
		$sheet->getColumnDimension('G')->setAutoSize(false)->setWidth(40.10);
		$sheet->getColumnDimension('H')->setAutoSize(false)->setWidth(30.10);
		$sheet->getColumnDimension('I')->setAutoSize(false)->setWidth(10.10);
		$sheet->getColumnDimension('J')->setAutoSize(false)->setWidth(24.10);
		$sheet->getColumnDimension('K')->setAutoSize(false)->setWidth(3.10);
		$sheet->getColumnDimension('L')->setAutoSize(false)->setWidth(25.10);
		$sheet->getColumnDimension('M')->setAutoSize(false)->setWidth(20.10);
        
		$rpt = $this->mconsresultgral->resultadosgeneral($parametros);
        $irow = 4;
        $i = 0;
        if ($rpt){
        	foreach($rpt as $row){
                $DCOTIZACION        = $row->DCOTIZACION;
                $FEC_RECEP          = $row->FEC_RECEP;
                $DREALPRODUCTO      = $row->DREALPRODUCTO;
                $DPRESENTACION      = $row->DPRESENTACION;
                $DCONDICION      	= $row->DCONDICION;
                $DENSAYO      		= $row->DENSAYO;
                $NVIAUSADO      	= $row->NVIAUSADO;
                $DRESULT      		= $row->DRESULT;
                $EXP10RES      		= $row->EXP10RES;
                $UNIDAD      		= $row->UNIDAD;
				$ESTADO      		= $row->ESTADO;
				$ZCTIPOENSAYO		= $row->ZCTIPOENSAYO;
                $i++;
				
				if($ZCTIPOENSAYO == "068" || $ZCTIPOENSAYO == "069" || $ZCTIPOENSAYO == "A61" || $ZCTIPOENSAYO == "070" || $ZCTIPOENSAYO == "736"){ // normal 
					$sheet->setCellValue('A'.$irow,$i);
					$sheet->setCellValue('B'.$irow,$DCOTIZACION);
					$sheet->setCellValue('C'.$irow,$FEC_RECEP);
					$sheet->setCellValue('D'.$irow,$DREALPRODUCTO);
					$sheet->setCellValue('E'.$irow,$DPRESENTACION);
					$sheet->setCellValue('F'.$irow,$DCONDICION);
					$sheet->setCellValue('G'.$irow,$DENSAYO);
					$sheet->setCellValue('H'.$irow,'');
					$sheet->setCellValue('I'.$irow,$NVIAUSADO);
					$sheet->setCellValue('J'.$irow,$DRESULT);
					$sheet->setCellValue('K'.$irow,$EXP10RES);
					$sheet->setCellValue('L'.$irow,$UNIDAD);
					$sheet->setCellValue('M'.$irow,$ESTADO);

					$irow++;
				}else{		
				
					$cinternoordenservicio	= $row->CINTERNOORDENSERVICIO;
					$cinternocotizacion		= $row->CINTERNOCOTIZACION;
					$nordenproducto      	= $row->NORDENPRODUCTO;
					$cmuestra      			= $row->CMUESTRA;
					$censayo      			= $row->CENSAYO;
					$nviausado      		= $row->NVIAUSADO;

					$parametrosele = array(
						'@cinternoordenservicio'    => $cinternoordenservicio,
						'@cinternocotizacion'    => $cinternocotizacion,
						'@nordenproducto'    => $nordenproducto,
						'@cmuestra'    => $cmuestra,
						'@censayo'    => $censayo,
						'@nviausado'    => $nviausado,
					);

					$rptele = $this->mconsresultgral->getlistelementos($parametrosele);
					if ($rptele){
						foreach($rptele as $rowele){
							$delemento        = $rowele->delemento;
							$resultado        = $rowele->RESULTADO;
							$dunidad        = $rowele->dunidad;
							$sconclusion        = $rowele->sconclusion;
							$sheet->setCellValue('B'.$irow,$DCOTIZACION);
							$sheet->setCellValue('C'.$irow,$FEC_RECEP);
							$sheet->setCellValue('D'.$irow,$DREALPRODUCTO);
							$sheet->setCellValue('E'.$irow,$DPRESENTACION);
							$sheet->setCellValue('F'.$irow,$DCONDICION);
							$sheet->setCellValue('G'.$irow,$DENSAYO);
							$sheet->setCellValue('H'.$irow,$delemento);
							$sheet->setCellValue('I'.$irow,$NVIAUSADO);
							$sheet->setCellValue('J'.$irow,$resultado);
							$sheet->setCellValue('L'.$irow,$dunidad);
							$sheet->setCellValue('M'.$irow,$sconclusion);

							$irow++;
						}
					}
				}

			}
        }
        $posfin = $irow - 1;

        $sheet->getStyle('A4:M'.$posfin)->applyFromArray($celdastexto);
        $sheet->getStyle('A4:A'.$posfin)->applyFromArray($celdastextocentro);
        $sheet->getStyle('C4:C'.$posfin)->applyFromArray($celdastextocentro);        
        $sheet->getStyle('J4:J'.$posfin)->applyFromArray($celdastextoderecha);
        $sheet->getStyle('K4:K'.$posfin)->applyFromArray($celdasexponente);
        $sheet->getStyle('L4:L'.$posfin)->applyFromArray($celdastextocentro);
        $sheet->getStyle('M4:M'.$posfin)->applyFromArray($celdastextocentro);
        
		$sheet->setTitle('Listado - Resultados Generales');  
        $sheet->setAutoFilter('A3:M'.$posfin);          
		$writer = new Xlsx($spreadsheet);
		$filename = 'listresultgral-'.time();
		ob_end_clean();
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
		header('Cache-Control: max-age=0');

		$writer->save('php://output');
    }
    
}
?>