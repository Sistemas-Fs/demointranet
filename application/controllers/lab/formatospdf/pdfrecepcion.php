<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Style\Conditional;
use PhpOffice\PhpSpreadsheet\Writer\IWriter;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Dompdf\Dompdf;

class pdfrecepcion extends CI_Controller {
	function __construct() {
		parent:: __construct();	
		$this->load->model('lab/recepcion/mrecepcion');
		$this->load->model('mglobales');
		$this->load->library('encryption');
		$this->load->helper(array('form','url','download','html','file'));
		$this->load->library('form_validation');
    }
    
   /** RECEPCION DE MUESTRA **/
	public function pdfOrderServ($cinternoordenservicio) { // recupera los cPTIZACION
        $this->load->library('pdfgenerator');

        $date = getdate();
        $fechaactual = date("d") . "/" . date("m") . "/" . date("Y");

        $html = '<html>
                <head>
                    <title>Orden de Trabajo</title>
                    <style>
                        @page {
                             margin: 0.3in 0.3in 0.3in 0.3in;
                        }
                        .teacherPage {
                            page: teacher;
                            page-break-after: always;
                        }
                        body{
                            font-family: Arial, Helvetica, sans-serif;
                            font-size: 9pt;
                            margin-top: 2cm;
                            margin-left: 0cm;
                            margin-right: 0cm;
                            margin-bottom: 0cm;
                        }  
                        header {
                            position: fixed;
                            top: 0cm;
                            left: 0cm;
                            right: 0cm;
                            height: 3cm;
                        }
                        .cuerpo {
                            text-align: justify;
                        }
                        img.izquierda {
                            float: left;
                        }
                        img.derecha {
                            float: right;
                        }
                        div.page_break {
                            page-break-before: always;
                        }
                        .page-number {
                          text-align: right;
                        }                        
                        .page-number:before {
                          content: counter(page);
                        }
                        th { 
                            text-align: center; 
                            border: 1px solid black;
                        }
                    </style>
                </head>
                <body>
                
                <header>
                    <table  width="700px" align="center" cellspacing="0" cellpadding="2" style="border: 1px solid black;">
                        <tr>
                            <td width="20%" rowspan="4">
                                <img src="'.public_url_ftp().'Imagenes/formatos/2/logoFSC.jpg" width="100" height="60" />    
                            </td>
                            <td width="60%" align="center" rowspan="4">
                                <h2>ORDEN DE TRABAJO</h2>
                            </td>
                            <td width="20%" align="center" colspan="2">
                                FSC-F-LAB-09
                            </td>
                        </tr>
                        <tr>
                            <td>Versión</td>
                            <td align="right">01</td>
                        </tr>
                        <tr>
                            <td>Fecha</td>
                            <td align="right">05/11/2012</td>
                        </tr>
                        <tr>
                            <td>Página</td>
                            <td align="right"><div class="page-number"></div></td>
                        </tr>
                    </table>
                </header>';

        			
        $res = $this->mrecepcion->getpdfdatosot($cinternoordenservicio);
        if ($res){
            foreach($res as $row){
				$nordentrabajo      = $row->nordentrabajo;
				$fordentrabajo      = $row->fordentrabajo;
				$cinternocotizacion = $row->cinternocotizacion;
                $nversioncotizacion = $row->nversioncotizacion;
                $OBSERVACION        = $row->OBSERVACION;
                $fentregainf        = $row->fentregainf;
                $XMICRO             = $row->XMICRO;
                $XFQ                = $row->XFQ;
                $XINSTRU            = $row->XINSTRU;
			}
		}
                
        $html .= '
            <main>
                <table width="700px" align="center" cellspacing="0" cellpadding="2" >
                    <tr>
                        <td colspan="9" style="height:10px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="30%" colspan="3" >&nbsp;</td>
                        <td width="35%" align="right" colspan="3" >N° '.$nordentrabajo.'</td>
                        <td width="35%" align="right" colspan="3" >Fecha '.$fordentrabajo.'</td>
                    </tr>
                    <tr>
                        <td colspan="3" >&nbsp;</td>
                        <td align="right" colspan="6">Fecha de entrega de informe '.$fentregainf.'</td>
                    </tr>
                    <tr>
                        <td colspan="9" ><b>I AREA</b></td>
                    </tr>
                    <tr>
                        <td width="50%">MICROBIOLOGÍA</td><td align="center" style="border: black 1px solid;" width="10%">'.$XMICRO.'</td><td width="40%"></td>
                        <td width="50%">FISICOQUÍMICA</td><td align="center" style="border: black 1px solid;" width="10%">'.$XFQ.'</td><td width="40%"></td>
                        <td width="50%">INSTRUMENTAL</td><td align="center" style="border: black 1px solid;" width="10%">'.$XINSTRU.'</td><td width="40%"></td>
                    </tr>
                    <tr>
                        <td colspan="9" style="height:10px;">&nbsp;</td>
                    </tr>
                </table>
                <table width="700px" align="center" cellspacing="0" cellpadding="2">
                    <tr>
                        <td><b>II PRODUCTOS / PRESENTACIÓN / CANTIDAD</b></td>
                    </tr>
                </table>
                <table width="700px" align="center" cellspacing="1" cellpadding="0" FRAME="void" RULES="rows">
                    <tr>
                        <th width="10%" align="center">CÓDIGO DE MUESTRA</th>
                        <th width="38%" align="center">PRODUCTO</th>
                        <th width="38%" align="center">PRESENTACIÓN / CANTIDAD</th>
                        <th width="14%" align="center">CONDICIONES(*)</th>
                    </tr>';
                    $resprod = $this->mrecepcion->getpdfdatosotprod($cinternoordenservicio);
                    if ($resprod){
                        foreach($resprod as $rowprod){
                            $cmuestra       = $rowprod->cmuestra;
                            $drealproducto  = $rowprod->drealproducto;
                            $DPRESENTACION  = $rowprod->DPRESENTACION;
                            $dtemperatura = $rowprod->dtemperatura;
                            $html .= '<tr>
                                <td>&nbsp;'.$cmuestra.'</td>
                                <td>&nbsp;'.$drealproducto.'</td>
                                <td>&nbsp;'.$DPRESENTACION.'</td>
                                <td>&nbsp;'.$dtemperatura.'</td>
                            </tr>';
                        }
                    }        
                $html .= '</table>
                <table width="700px" align="center" cellspacing="0" cellpadding="2">
                    <tr>
                        <td style="height:10px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td><b>III SOLICITUD DE ENSAYO</b></td>
                    </tr>
                </table>
                <table width="100%" align="center" cellspacing="1" cellpadding="0" FRAME="void" RULES="rows">
                    <tr>
                        <th width="12%" align="center">PRODUCTO(S)</th>
                        <th width="8%" align="center">Código Metodo</th>
                        <th width="30%" align="center">METODO DE ENSAYO</th>
                        <th width="37%" align="center">NORMA / REFERENCIA</th>
                        <th width="5%" align="center">Vías</th>
                        <th width="5%" align="center">AC/ NOAC</th>
                    </tr>';
                    $resensayo = $this->mrecepcion->getpdfdatosotensayo($cinternoordenservicio);
                    if ($resensayo){
                        foreach($resensayo as $rowensayo){
                            $CMUESTRA   = $rowensayo->CMUESTRA;
                            $CENSAYOFS  = $rowensayo->CENSAYOFS;
                            $DENSAYO    = $rowensayo->DENSAYO;
                            $DNORMA     = $rowensayo->DNORMA;
                            $SACNOAC    = $rowensayo->SACNOAC;
                            $CANTIDAD   = $rowensayo->CANTIDAD;
                            $html .= '<tr>
                                <td width="10%">&nbsp;'.$CMUESTRA.'</td>
                                <td width="8%">&nbsp;'.$CENSAYOFS.'</td>
                                <td width="30%">&nbsp;'.$DENSAYO.'</td>
                                <td width="39%">&nbsp;'.$DNORMA.'</td>
                                <td width="5%">&nbsp;'.$CANTIDAD.'</td>
                                <td width="5%">&nbsp;'.$SACNOAC.'</td>
                            </tr>';
                        }
                    } 
                    
                           
                $html .= '<tr>
                        <td colspan="6" style="height:10px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2">Observaciones:</td>
                        <td colspan="4">'.$OBSERVACION.'</td>
                    </tr>
                    <tr>
                        <td colspan="6" style="height:10px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="6" style="height:10px;">&nbsp;</td>
                    </tr>
                </table>
                
                <table width="700px" align="center" cellspacing="0" cellpadding="2">
                    <tr>
                        <td colspan="3" style="height:10px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <th width="15%" align="center">BK</th>
                        <th width="30%" align="center" style="border: black 1px solid;">LOTE</th>
                        <th width="55%" align="center" style="border: white 1px solid;">&nbsp;</th>
                    </tr>';
                    $resensayo = $this->mrecepcion->getpdfdatosotblancov($cinternocotizacion);
                    if ($resensayo){
                        foreach($resensayo as $rowensayo){
                            $bk     = $rowensayo->bk;
                            $lote   = $rowensayo->lote;
                            $html .= '<tr>
                                <td width="15%">&nbsp;'.$bk.'</td>
                                <td width="30%">&nbsp;'.$lote.'</td>
                                <td width="55%">&nbsp;</td>
                            </tr>';
                        }
                    } 
                $html .= '
                <tr>
                    <td colspan="3" style="height:10px;">&nbsp;</td>
                </tr>/table>
                <br>
                <br>
                <table width="700px" align="center" cellspacing="0" cellpadding="2">
                    <tr>
                        <td width="65%" align="center">&nbsp;</td>
                        <td width="15%" align="right">Recibido por :</td>
                        <td width="20%" align="center" style="border-bottom: black 1px solid;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="65%" align="center">&nbsp;</td>
                        <td width="15%" align="right">Fecha :</td>
                        <td width="20%" align="center" style="border-bottom: black 1px solid;">&nbsp;</td>
                    </tr>
                /table>
            </main>';

        $html .= '</body></html>';
		$filename = 'OrdenTrabajo-';//.$namefile;
		$this->pdfgenerator->generate($html, $filename, TRUE, 'A4', 'portrait');
        //echo $html;
    }

	public function pdfCargoOT($cidconst) { // recupera los cPTIZACION
        $this->load->library('pdfgenerator');

        $date = getdate();
        $fechaactual = date("d") . "/" . date("m") . "/" . date("Y");

        $html = '<html>
                <head>
                    <title>Constancia</title>
                    <style>
                        @page {
                             margin: 0.3in 0.3in 0.3in 0.3in;
                        }
                        .teacherPage {
                            page: teacher;
                            page-break-after: always;
                        }
                        body{
                            font-family: Arial, Helvetica, sans-serif;
                            font-size: 9pt;
                            margin-top: 1.7cm;
                            margin-left: 0cm;
                            margin-right: 0cm;
                            margin-bottom: 0cm;
                        }  
                        header {
                            position: fixed;
                            top: 0cm;
                            left: 0cm;
                            right: 0cm;
                            height: 0cm;
                        }
                        main {                            
                            margin-top: 0.4cm;
                            text-align: justify;
                        }
                        footer {
                            position: fixed; 
                            bottom: 5px; 
                            left: 0px; 
                            right: 0px;
                            height: 0px; 
                        }
                        img.izquierda {
                            float: left;
                        }
                        img.derecha {
                            float: right;
                        }
                        div.page_break {
                            page-break-before: always;
                        }
                        .page-number {
                          text-align: right;
                        }                        
                        .page-number:before {
                          content: counter(page);
                        }
                        th { 
                            font-size: 8pt;
                            text-align: center; 
                            border: 1px solid black;
                        }
                        td { 
                            text-align: left; 
                            border: 1px solid black;
                        }
                    </style>
                </head>
                <body>';

                $res = $this->mrecepcion->getpdfdatosconst($cidconst);
                if ($res){
                    foreach($res as $row){
                        $fingreso       = $row->fingreso;
                        $hingreso       = $row->hingreso;
                        $nro_constancia = $row->nro_constancia;
                        $dcotizacion    = $row->dcotizacion;
                        $nombcliente    = $row->nombcliente;
                        $nombcontacto   = $row->nombcontacto;
                        $dmail          = $row->dmail;
                        $dtelefono      = $row->dtelefono;
                        $dequipo        = $row->DEQUIPOS;
                    }
                }

            $version = '03';
            $fec_cabecera = '09/03/2017';
            $colspan = '5';
            if ($fingreso >= '2022-05-16') {
                $version = '04';
                $fec_cabecera = '2022-05-16';
                $colspan = '3';
            }
            
        $html .='    
                <header>
                    <table  width="1050px" align="center" cellspacing="0" cellpadding="2">
                        <tr>
                            <td width="20%" style="text-align: center;" rowspan="4">
                                <img src="'.public_url_ftp().'Imagenes/formatos/2/logoFSC.jpg" width="100" height="60" />    
                            </td>
                            <td width="65%" style="text-align: center;" rowspan="4">
                                <h2>CONSTANCIA DE RECEPCION DE MUESTRAS</h2>
                            </td>
                            <td width="15%" style="text-align: center;" colspan="2">
                                FSC-F-LAB-08
                            </td>
                        </tr>
                        <tr>
                            <td>Versión</td>
                            <td align="right">'.$version.'</td>
                        </tr>
                        <tr>
                            <td>Fecha</td>
                            <td align="right">'.$fec_cabecera.'</td>
                        </tr>
                        <tr>
                            <td>Página</td>
                            <td align="right"><div class="page-number"></div></td>
                        </tr>
                    </table>
                </header>';
       			
        
                
        $html .= '
            <main>
                <table width="1050px" align="center" cellspacing="0" cellpadding="2" >
                    <tr>
                        <td width="20%">FECHA INGRESO:</td>
                        <td width="15%">'.$fingreso.'</td>
                        <td width="10%">HORA: </td>
                        <td width="20%">'.$hingreso.'</td>
                        <td width="15%">N° CONSTANCIA:</td>
                        <td width="20%">'.$nro_constancia.'</td>
                    </tr>
                    <tr>
                        <td>CLIENTE:</td>
                        <td colspan="3">'.$nombcliente.'</td>
                        <td>N° COTIZ</td>
                        <td>'.$dcotizacion.'</td>
                    </tr>
                    <tr>
                        <td>CONTACTO:</td>
                        <td colspan="3">'.$nombcontacto.'</td>
                        <td>TELEFONO:</td>
                        <td>'.$dtelefono.'</td>
                    </tr>
                    <tr>
                        <td>EMAIL:</td>
                        <td colspan="'.$colspan.'">'.$dmail.'</td>
                        ';
                        if ($fingreso > '2022-05-16') {
                           $html.= '<td>EQUIPO:</td>
                            <td >'.$dequipo.'</td>';
                         }
                 $html .='</tr>
                </table>
                <table width="1050px" align="center" cellspacing="0" cellpadding="2" >
                    <tr>
                        <th width="5%">N° MUESTRA</th>
                        <th width="25%">DESCRIPCION DE LA MUESTRA</th>
                        <th width="7%">UNIDADES POR MUESTRA</th>
                        <th width="30%">CONDICIONES DE RECEPCIÓN DE LA MUESTRA (Temperatura, Peso, FP, FV, Lote, etc.)</th>
                        <th width="15%">OBSERVACIONES</th>
                    </tr>';
                    $resprod = $this->mrecepcion->getpdfdetalleconst($cidconst);
                    $i = 0;
                    if ($resprod){
                        foreach($resprod as $rowprod){
                            $drealproducto   = $rowprod->drealproducto;
                            $dpresentacion  = $rowprod->dpresentacion;
                            $dobservacionesconst      = $rowprod->dobservacionesconst;
                            $dprecinto      = $rowprod->dprecinto;
                            $dcantidad      = $rowprod->dcantidad;
                            $i++;

                            $html .= '<tr>
                                <td>&nbsp;'.$i.'</td>
                                <td>&nbsp;'.$drealproducto.'</td>
                                <td>&nbsp;'.$dcantidad.'</td>
                                <td>&nbsp;'.$dpresentacion.'</td>
                                <td>&nbsp;'.$dobservacionesconst.'</td>
                            </tr>';
                        }
                    }       
                $html .= '</table>
                <table width="1050px" align="center" cellspacing="0" cellpadding="2" >
                    <tr>
                        <td style="height:80px; border: 0;">&nbsp;</td>
                        <td style="border: 0;"></td>
                        <td style="border: 0;">&nbsp;</td>
                        <td style="border: 0; text-align: center;"><img src="'.public_url_ftp().'Imagenes/firmas/FS000150.png" width="150" height="100" /> </td>
                        <td style="border: 0;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width:10%; border: 0;">&nbsp;</td>
                        <td style="width:35%; text-align: center; border: 0; border-top: 1px;">Firma Cliente</td>
                        <td style="width:10%; border: 0;">&nbsp;</td>
                        <td style="width:35%; text-align: center; border: 0; border-top: 1px;">Firma FSC</td>
                        <td style="width:10%; border: 0;">&nbsp;</td>
                    </tr>
                </table>
            </main>           

            <footer>
                <table  width="1050px" align="center">
                    <tr>
                        <td style="border: 0;">NOTA: Este documento es la constancia del recibo de muestras según las condiciones descritas en el mismo.</td>
                    </tr>
                </table> 
            </footer>';

        $html .= '</body></html>';
		$filename = 'Constancia-'.$nro_constancia;
		$this->pdfgenerator->generate($html, $filename, TRUE, 'A4', 'landscape');
        //echo $html;
    }

    public function pdfEtiquetagen(){
        
        $len = $this->input->post('len');
        $variable  = $this->input->post('id');
        $lon = strlen($variable) - 1;
        $id = substr($variable, 0, $lon);
        $copias = substr($variable, -1);
        $result = $this->mrecepcion->getpdfdatosot($id);
		if ($result){
			foreach($result as $row){
                $nordentrabajo      = $row->nordentrabajo;
            }
        }
        $this->load->library('pdf');
        $html = '<html>
                <head>
                    <title>Etiqueta</title>
                    <style>
                        @page {
                             margin: 0in 0in 0in 0in;
                        }
                        .teacherPage {
                            page: teacher;
                            page-break-after: always;
                        }
                        body{
                            font-family: Arial, Helvetica, sans-serif;
                            font-size: 8pt;
                            margin-top: 0.10cm;
                            margin-left: 0cm;
                            margin-right: 0cm;
                            margin-bottom: 0cm;
                        }  
                        header {
                            position: fixed;
                            top: 0cm;
                            left: 0cm;
                            right: 0cm;
                            height: 0cm;
                        }
                        .cuerpo {
                            text-align: justify;
                        }
                        img.izquierda {
                            float: left;
                        }
                        img.derecha {
                            float: right;
                        }
                        div.page_break {
                            page-break-before: always;
                        }
                        .page-number {
                          text-align: right;
                        }                        
                        .page-number:before {
                          content: counter(page);
                        }
                        td {  
                            border: 1px solid black;
                        }
                    </style>
                </head>
                <body>';
                for ($i = 0; $i < $len; $i++) {
                    $muestra = $this->input->post('0['.$i.']');                    
                    $nvia = 0;
                    for ($ci = 0; $ci < $copias; $ci++) {
                        $nvia++;
                        $html .= '<div class="teacherPage">				
                        <page>	
                            <table width="220px" align="center" style="border-collapse: collapse;">
                                <tr>
                                    <td width="30%" style="height:30px;">Nro de OT</td>
                                    <td width="70%" align="left">&nbsp;'.$nordentrabajo.'</td>
                                </tr>
                                <tr>
                                    <td style="height:30px;">Nro de M</td>
                                    <td align="left">&nbsp;'.$muestra.'</td>
                                </tr>
                                <tr>
                                    <td style="height:30px;">Nro de vía</td>
                                    <td align="center">'.$nvia.'</td>
                                </tr>
                            </table>
                        </page>
                        </div>';
                    }
                }

        $html .= '</body></html>';
        $this->pdf->load_view($html);
        //echo $html;
    }

}
?>