<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Style\Conditional;
use PhpOffice\PhpSpreadsheet\Writer\IWriter;
use PhpOffice\PhpSpreadsheet\IOFactory;

class pdfcotizacion extends CI_Controller {
	function __construct() {
		parent:: __construct();	
		$this->load->model('lab/coti/mcotizacion');
		$this->load->model('mglobales');
		$this->load->library('encryption');
		$this->load->helper(array('form','url','download','html','file'));
		$this->load->library('form_validation');
    }
    
   /** COTIZACION **/
	public function pdfCoti($idcoti,$nversion) { // recupera los cPTIZACION
        $this->load->library('pdfgenerator');

        $date = getdate();
        $fechaactual = date("d") . "/" . date("m") . "/" . date("Y");

        $html = '<html>
                <head>
                    <title>Cotización</title>
                    <style>
                        @page {
                             margin: 0.3in 0.3in 0.3in 0.3in;
                        } 
                        .teacherPage {
                            page: teacher;
                            page-break-after: always;
                        }
                        body{
                            font-family: Arial, Helvetica, sans-serif;
                            font-size: 9pt;
                            margin-top: 2cm;
                            margin-left: 0cm;
                            margin-right: 0cm;
                            margin-bottom: 0cm;
                        }  
                        header {
                            position: fixed;
                            top: 0cm;
                            left: 0cm;
                            right: 0cm;
                            height: 3cm;
                        }
                        .cuerpo {
                            text-align: justify;
                        }
                        img.izquierda {
                            float: left;
                        }
                        img.derecha {
                            float: right;
                        }
                        div.page_break {
                            page-break-before: always;
                        }
                        .page-number {
                          text-align: right;
                        }
                        
                        .page-number:before {
                          content: counter(page);
                        }
                        th { 
                            text-align: center; 
                            border: 1px solid black;
                        }
                    </style>
                </head>
                <body>
                
                <header>
                    <table  width="700px" align="center" cellspacing="0" cellpadding="2" style="border: 1px solid black;">
                        <tr>
                            <td width="20%" rowspan="4">
                                <img src="'.public_url_ftp().'Imagenes/formatos/2/logoFSC.jpg" width="100" height="60" />    
                            </td>
                            <td width="60%" align="center" rowspan="4">
                                <h2>COTIZACION DE SERVICIO DE ENSAYO</h2>
                            </td>
                            <td width="20%" align="center" colspan="2">
                                FSC-F-LAB-07
                            </td>
                        </tr>
                        <tr>
                            <td>Versión</td>
                            <td align="right">03</td>
                        </tr>
                        <tr>
                            <td>Fecha</td>
                            <td align="right">05/04/2022</td>
                        </tr>
                        <tr>
                            <td>Página</td>
                            <td align="right"><div class="page-number"></div></td>
                        </tr>
                    </table>
                </header>';
        			
        $res = $this->mcotizacion->getpdfdatoscoti($idcoti,$nversion);
        if ($res){
            foreach($res as $row){
				$dcotizacion        = $row->dcotizacion;
				$drazonsocial       = $row->drazonsocial;
				$nruc               = ($row->nruc === '') ? '&nbsp;' : $row->nruc;
				$ddireccioncliente  = $row->ddireccioncliente;
				$dtelefono          = $row->dtelefono;
				$dcontacto          = $row->dcontacto;
				$dmail              = $row->dmail;
                $fcotizacion        = $row->fcotizacion;
                $smuestreo          = $row->smuestreo;
                $imuestreo          = $row->imuestreo;
                $srecojo            = $row->srecojo;
                $irecojo            = $row->irecojo;
                $sgastoadm          = $row->sgastoadm;
                $igastoadm          = $row->igastoadm;
                $isubtotal          = $row->isubtotal;
                $subtotalimuestra   = $row->subtotalimuestra;
                $pigv               = $row->pigv;
                $pdescuento         = $row->pdescuento;
                $itotal             = $row->itotal;
				$cantprod           = $row->cantprod;
				$summuestra         = $row->summuestra;
				$cforma_pago        = $row->cforma_pago;
				$banco              = $row->banco;
				$detraccion         = $row->detraccion;
                $entrega            = $row->entrega;
                $dcantidadminima    = $row->dcantidadminima;
                $diaspermanecia     = $row->diaspermanecia;
                $diascoti           = $row->diascoti;
                $dobservacion       = $row->dobservacion;
                $usuariocrea        = $row->usuariocrea;
                $verprecios         = $row->verprecios;
                $digv               = $row->digv;
                $ddescuento         = $row->ddescuento;
                $condescuento       = $row->condescuento;
                $namefile           = $row->namefile;
                $ctipocambio        = $row->ctipocambio;
                $dtipocambio        = $row->dtipocambio;              
                
                
                if ($ctipocambio  == 'S') :
                    $var_moneda = 'S/.';
                else:
                    $var_moneda = '$';
                endif;
			}
		}
                
        $html .= '
            <main>
                <table width="700px" align="center" cellspacing="0" cellpadding="2" style="border: 1px solid black;">
                    <tr>
                        <td colspan="4"><b>N°'.$dcotizacion.'</b></td>
                    </tr>
                    <tr>
                        <td colspan="4" style="height:1px;"></td>
                    </tr>
                    <tr>
                        <td colspan="4" ><b>I CLIENTE</b></td>
                    </tr>
                    <tr>
                        <td width="80px">Razón Social:</td>
                        <td width="360px">'.$drazonsocial.'</td>
                        <td width="50px">RUC:</td>
                        <td width="190px">'.$nruc.'</td>
                    </tr>
                    <tr>
                        <td>Dirección:</td>
                        <td>'.$ddireccioncliente.'</td>
                        <td>Teléfono:</td>
                        <td>'.$dtelefono.'</td>
                    </tr>
                    <tr>
                        <td>Contacto:</td>
                        <td>'.$dcontacto.'</td>
                        <td>E-mail:</td>
                        <td>'.$dmail.'</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>Fecha:</td>
                        <td>'.$fcotizacion.'</td>
                    </tr>
                </table>
                <table width="700px" align="center" cellspacing="0" cellpadding="2" style="border: 1px solid black;">
                    <tr>
                        <td><b>II DETALLES</b></td>
                    </tr>
                </table>
                <table width="700px" align="center" cellspacing="1" cellpadding="0" FRAME="void" RULES="rows">
                    <tr>
                        <th width="30%" align="center">LOCAL</th>
                        <th width="35%" align="center">PRODUCTO</th>
                        <th width="20%" align="center">CONDICIONES DE LA MUESTRA</th>
                        <th width="15%" align="center">CANTIDAD MUESTRA MINIMA</th>
                    </tr>';
                    $resprod = $this->mcotizacion->getpdfdatosprod($idcoti,$nversion);
                    if ($resprod){
                        foreach($resprod as $rowprod){
                            $destablecimiento = $rowprod->destablecimiento;
                            $dproducto = $rowprod->dproducto;
                            $condicion = $rowprod->condicion;
                            $procedencia = $rowprod->procedencia;
                            $dcantidadminima = $rowprod->dcantidadminima;
                            $html .= '<tr>
                                <td width="30%">&nbsp;'.$destablecimiento.' <br> &nbsp;'.$procedencia.'</td>
                                <td width="35%">&nbsp;'.$dproducto.'</td>
                                <td width="20%">&nbsp;'.$condicion.'</td>
                                <td width="15%">&nbsp;'.$dcantidadminima.'</td>
                            </tr>';
                        }
                    }        
                $html .= '</table>';
                $html .= '<table width="350px" style="margin-left: 15px; height:12px;">
                    <tr>
                        <td>Cantidad de Productos:</td>
                        <td>'.$cantprod.'</td>
                        <td>Suma de Muestras:</td>
                        <td>'.$summuestra.'</td>
                    </tr>
                    <tr>
                        <td colspan="4" style="height:10px;"></td>
                    </tr>
                </table>
                <table width="auto" align="center" cellspacing="1" cellpadding="0" FRAME="void" RULES="rows">
                    <tr >
                        <th width="8%" align="center">Codigo Metodo</th>
                        <th width="28%" align="center">METODO DE ENSAYO</th>
                        <th width="7%" align="center">AC / NOAC</th>
                        <th width="37%" align="center">NORMA / REFERENCIA</th>
                        <th width="5%" align="center">Cant.</th>
                        <th width="7%" align="center">P.UNI '.$var_moneda.'</th>
                        <th width="8%" align="center">Precio Total '.$var_moneda.'</th>
                    </tr>';
                    $resproddet = $this->mcotizacion->getpdfdatosprod($idcoti,$nversion);
                    if ($resproddet){
                        foreach($resproddet as $rowproddet){
                            $dproductodet = strtoupper($rowproddet->dproducto);
                            $idproduc = $rowproddet->nordenproducto;
                            $subtotal = $rowproddet->subtotal;
                            
                            if ($var_moneda  == 'S/.') :
                                $var_subtotal = $subtotal;
                            else:
                                $var_subtotal = null;
                            endif;
                            
                            $html .= '<tr>
                                <td colspan="6" ><h4>'.$dproductodet.'</h4>
                                </td>
                                <td align="right"><h4>'.$var_subtotal.'</h4>
                                </td>
                            </tr>';
                                $resensadet = $this->mcotizacion->getlistarensayo($idcoti,$nversion,$idproduc);
                                if ($resensadet){
                                    foreach($resensadet as $rowensadet){
                                        $codigo = $rowensadet->CODIGO;
                                        $densayo = $rowensadet->DENSAYO;
                                        $acre = $rowensadet->ACRE;
                                        $norma = $rowensadet->NORMA;
                                        $cantidad = $rowensadet->CANTIDAD;
                                        $costoensa = $rowensadet->CONSTOENSAYO;
                                        $costo = $rowensadet->COSTO;
                                        
                                        if ($verprecios  == 'S') :
                                            $var_costoensa = $costoensa;
                                        else:
                                            $var_costoensa = null;
                                        endif;
                                        if ($verprecios  == 'S') :
                                            $var_costo = $costo;
                                        else:
                                            $var_costo = null;
                                        endif;

                                        $html .= '<tr>
                                        <td width="10%" align="center">
                                        '.$codigo.'
                                        </td>
                                        <td width="25%">
                                        '.$densayo.'
                                        </td>
                                        <td width="5%" align="center">
                                        '.$acre.'
                                        </td>
                                        <td width="39%">
                                        '.$norma.'
                                        </td>
                                        <td width="3%" align="center">
                                        '.$cantidad.'
                                        </td>
                                        <td width="10%" align="right">
                                        '.$var_costoensa.'
                                        </td>
                                        <td width="8%" align="right">
                                        '.$var_costo.'
                                        </td>
                                        </tr>';
                                    }
                                }
                                $html .= '';
                        }
                    }
                    $html .= '</table>                 
                    <table width="698px" align="center">';
                    
                if ($ctipocambio  == 'S') :
                    if($smuestreo == 'S' && $imuestreo > 0){
                    $html .= '<tr>
                            <td width="470px" >AC: Método Acreditado<br>NO AC: Método No Acreditado</td>
                            <td width="130px" > Muestreo</td>
                            <td width="80px" align="right">'.$imuestreo.'</td>
                        </tr>';
                    }else{
                        $html .= '<tr>
                                <td width="470px" >AC: Método Acreditado<br>NO AC: Método No Acreditado</td>
                                <td width="130px" >&nbsp; </td>
                                <td width="80px" align="right">&nbsp; </td>
                            </tr>';
                    }
                    if($srecojo == 'S' && $irecojo > 0){
                    $html .= '<tr>
                            <td width="470px" ></td>
                            <td width="130px" > Recojo</td>
                            <td width="80px" align="right">'.$irecojo.'</td>
                        </tr>';
                    }
                    if($sgastoadm == 'S' && $igastoadm > 0){
                    $html .= '<tr>
                            <td width="470px" ></td>
                            <td width="130px" > Gastos Administrativos</td>
                            <td width="80px" align="right">'.$igastoadm.'</td>
                        </tr>';
                    }
                    $html .= '<tr>
                            <td></td>
                            <td> SUBTOTAL</td>
                            <td align="right">'.$subtotalimuestra.'</td> 
                        </tr>';
                    if($pdescuento > 0){
                    $html .= '<tr>
                                <td></td>
                                <td> DESCUENTOS '.$pdescuento.'% </td>
                                <td align="right">('.$ddescuento.')</td>
                            </tr>
                            <tr>                
                                <td></td>
                                <td> </td>
                                <td align="right">'.$condescuento.'</td>
                            </tr>';
                    }
                    $html .= '<tr>                
                            <td></td>
                            <td> IGV 18%</td>
                            <td align="right">'.$digv.'</td>
                        </tr>
                        <tr>                
                            <td></td>
                            <td> TOTAL</td>
                            <td align="right">'.$itotal.'</td>
                        </tr>
                        <tr>
                            <td style="height:5px;">
                            </td>
                        </tr>';
                else:
                    if($smuestreo == 'S' && $imuestreo > 0){
                    $html .= '<tr>
                            <td width="470px" >AC: Método Acreditado<br>NO AC: Método No Acreditado</td>
                            <td width="130px" > Muestreo</td>
                            <td width="80px" align="right">'.$imuestreo.'</td>
                        </tr>';
                    }else{
                        $html .= '<tr>
                                <td width="470px" >AC: Método Acreditado<br>NO AC: Método No Acreditado</td>
                                <td width="130px" >&nbsp; </td>
                                <td width="80px" align="right">&nbsp; </td>
                            </tr>';
                    }
                    if($srecojo == 'S' && $irecojo > 0){
                    $html .= '<tr>
                            <td width="470px" ></td>
                            <td width="130px" > Recojo</td>
                            <td width="80px" align="right">'.$irecojo.'</td>
                        </tr>';
                    }
                    if($sgastoadm == 'S' && $igastoadm > 0){
                    $html .= '<tr>
                            <td width="470px" ></td>
                            <td width="130px" > Gastos Administrativos</td>
                            <td width="80px" align="right">'.$igastoadm.'</td>
                        </tr>';
                    }
                    $html .= '<tr>
                            <td></td>
                            <td> SUBTOTAL</td>
                            <td align="right">'.$isubtotal.'</td> 
                        </tr>';
                    $html .= '<tr>
                        <td width="470px" ></td>
                        <td width="130px"> TOTAL $</td>
                        <td width="80px" align="right">'.$itotal.'</td>
                    </tr>
                    <tr>
                        <td style="height:10px;">
                        </td>
                    </tr>';
                endif;                    

        $html .= '</table>';

        $html .= '<table width="700px" align="center">
            <tr>
                <td><b>III</b></td>
                <td><b>FORMA DE PAGO:</b></td>
                <td align="left">'.$cforma_pago.'</td>
                
            </tr>       
            <tr>
                <td colspan="4" style="height:10px;">
                </td>
            </tr>

            <tr>
                <td><b>IV</b></td>
                <td colspan="3"><b>CUENTAS BANCARIAS:</b></td>
            </tr>
            <tr>
                <td colspan="1"></td>
                <td colspan="3">'.$banco.' CCI: 00219400156160801692</td>                
            </tr>
            <tr>
                <td colspan="1"></td>
                <td colspan="3">'.$detraccion.'</td>
            </tr>
            <tr>
                <td colspan="1"></td>
                <td colspan="3">CTA CORRIENTE EN DOLARES BCP: 194-2425691-1-38</td>
            </tr>     
            <tr>
                <td colspan="1"></td>
                <td align="left">Dirección de Banco</td>
                <td colspan="2">Calle Centenario 156, La Molina, Lima 13 – Perú</td>
            </tr>
            <tr>
                <td colspan="1"></td>
                <td align="left">Teléfono del Banco</td>
                <td colspan="2">511-3119898</td>
            </tr>
            <tr>
                <td colspan="1"></td>
                <td align="left"># Swift</td>
                <td colspan="2">BCPLPEPL</td>
            </tr>
            <tr>
                <td colspan="1"></td>
                <td align="left">Razón social</td>
                <td colspan="2">FS CERTIFICACIONES S.A.C.</td>
            </tr>
            <tr>
                <td colspan="1"></td>
                <td align="left">RUC</td>
                <td colspan="2">20514553719</td>
            </tr>
            <tr>
                <td colspan="4" style="height:10px;">
                </td>
            </tr>

            <tr>
                <td><b>V</b></td>
                <td colspan="2"><b>TIEMPO DE ENTREGA DEL INFORME:</b></td>
                <td style="text-align: justify;">'.$entrega.'</td>
            </tr>
            <tr>
                <td colspan="4" style="height:10px;">
                </td>
            </tr>
            <tr>
                <td VALIGN=top><b>VI</b></td>
                <td VALIGN=top style="text-align: justify;"><b>RECEPCIÓN DE MUESTRAS:</b></td>
                <td colspan="3" style="text-align: justify;">Dirección de Laboratorio: Pasaje Monte azul 120-180. Oficina 203-204. Chacarilla. Surco</td>
            </tr>   
            <tr>
                <td></td>
                <td></td>
                <td colspan="3" style="text-align: justify;"><b></b>Horario: Lunes a viernes 9h a 13h y de 14h a 17h. Sábados de 9h a 12h</td>
            </tr>          
            <tr>
                <td colspan="4" style="height:10px;">
                </td>
            </tr>
            <tr>
                <td VALIGN=top ><b>VII</b></td>
                <td colspan="3"><b>CONTACTO:</b>Para cualquier consulta adicional póngase en contacto con su Ejecutivo o Asistente comercial:</td>
            </tr>
            <tr>
                <td VALIGN=top ></td>
                <td colspan="3" align="left">'.$usuariocrea.'</td>
            </tr>
            <tr>
                <td colspan="4" style="height:10px;"></td>
            </tr>
            <tr>
                <td VALIGN=top><b>VIII</b></td>
                <td colspan="4" style="text-align: justify;"><b>PERMANENCIA DE LA CONTRA MUESTRA EN EL LABORATORIO:</b>  En caso de que el servicio considere contramuestras, éstas se conservarán en el laboratorio por el período acordado con el cliente, 
                luego de lo cual serán eliminadas de acuerdo a nuestros procedimientos  internos. En caso el cliente requiera la devolución de la contramuestra, esta deberá ser solicitada antes de la finalización del Tiempo de Custodia.</td>
            </tr>            
            <tr>
                <td colspan="4" style="height:10px;">
                </td>
            </tr>
        </table>';
        
        $html .= '<table width="700px" align="center">
            <tr>
                <td VALIGN=top ><b>IX</b></td>
                <td ><b>VIGENCIA DE COTIZACION:</b></td>
                <td align="left">'.$diascoti.'</td>
            </tr>
            <tr>
                <td colspan="3" style="height:10px;"></td>
            </tr>
            <tr>
                <td VALIGN=top><b>X</b></td>
                <td colspan="2"><b>ACEPTACION DE LA COTIZACION:</b></td>
            </tr>
            <tr>
                <td VALIGN=top>-</td>
                <td colspan="2" style="text-align: justify;">En caso de Aceptar la Cotización, favor de enviarla firmada o declaranado su aceptación mediante un correo electronico. Cualquier cambio en la cotización enviada deberá solicitarse antes de iniciado el servicio.</td>
            </tr>
            <tr>
                <td VALIGN=top>-</td>
                <td colspan="2" style="text-align: justify;">Al dar su visto bueno, el cliente acepta esta cotización con carácter de contrato.</td>
            </tr>
            <tr>
                <td VALIGN=top>-</td>
                <td colspan="2" style="text-align: justify;">Toda la información derivada de las actividades del laboratorio obtenidas a través del servicio brindado se conservará de modo confidencial, excepto por la información que el cliente pone a disposición del público, o cuando lo acuerdan el laboratorio y el cliente.</td>
            </tr>
            <tr>
                <td VALIGN=top>-</td>
                <td colspan="2" style="text-align: justify;">Cuando a FS Certificaciones se le solicite por ley o disposiciones contractuales divulgar información confidencial, notificará al cliente salvo que esté prohibido por ley.</td>
            </tr>
            <tr>
                <td VALIGN=top>-</td>
                <td colspan="2" style="text-align: justify;">La información acerca del cliente, obtenida de fuentes diferentes del cliente se tratará como información confidencial.</td>
            </tr>
            <tr>
                <td VALIGN=top>-</td>
                <td colspan="2" style="text-align: justify;">La emisión de los Informes de Ensayo se realizará en formato digital (PDF), los cuales serán enviados por correo electrónico al cliente. El envío de los informes físicos tiene un costo adicional que será informado al cliente.</td>
            </tr>
            <tr>
                <td VALIGN=top>-</td>
                <td colspan="2" style="text-align: justify;">Copias adicionales de los informes y las traducciones de los mismos tendrán un costo adicional.</td>
            </tr>
            <tr>
                <td VALIGN=top>-</td>
                <td colspan="2" style="text-align: justify;">En FS Certificaciones estamos comprometidos a prevenir actos de soborno conforme a lo establecido en nuestra política de calidad. En base a ello rechazamos cualquier tipo de ofrecimientos, pago, solicitud o aceptación de soborno de cualquier tipo. FS Certificaciones procederá a realizar la denuncia de este tipo de actos conforme a los mecanismos correspondientes.</td>
            </tr>
            <tr>
                <td VALIGN=top>-</td>
                <td colspan="2" style="text-align: justify;">Modificaciones solicitadas posterior a la emisión de los Informes de Ensayo, luego de que el cliente revisó los previos y dio conformidad, tendrán un costo adicional que le serán comunicados oportunamente.</td>
            </tr>
            <tr>
                <td VALIGN=top>-</td>
                <td colspan="2" style="text-align: justify;">En caso se determine que no se cumplen las cantidades mínimas o las condiciones, la muestra se considerará muestra no idónea y sólo con autorización del cliente será ensayada y se reportará como Informe No Acreditado.</td>
            </tr>
            <tr>
                <td VALIGN=top>-</td>
                <td colspan="2" style="text-align: justify;">En caso el cliente tenga una queja sobre el servicio brindado enviar un correo a adiaz@fscertificaciones.com para recibir una descripción del proceso de tratamiento de quejas.</td>
            </tr>
            <tr>
                <td colspan="3" style="height:10px;"></td>
            </tr>
            <tr>
                <td VALIGN=top width="10px"><b>XI</b></td>
                <td width="180px"><b>OBSERVACIONES:</b></td>
                <td>'.$dobservacion.'</td>
            </tr>
            <tr>
                <td colspan="3" style="height:10px;"></td>
            </tr>
        </table>';
        
        $html .= '<table width="700px" align="center">
            <tr>
                <td colspan="4" style="height:80px;"></td>
            </tr>
            <tr>
                <td></td>
                <td align="center" style="border-top: 1px solid black;">CLIENTE</td>
                <td></td>
                <td align="center" >FSC LABORATORIO</td>
            </tr>
            <tr>
                <td colspan="4" style="height:10px;"></td>
            </tr>
        </table>';

        $html .= '</main></body></html>';
		$filename = 'COT '.$namefile.' '.$drazonsocial;
        $this->pdfgenerator->generate($html, $filename, TRUE, 'A4', 'portrait');
        //+echo $html;
        
	}

	public function pdfCotisolodet($idcoti,$nversion) { // recupera los cPTIZACION
        $this->load->library('pdfgenerator');

        $date = getdate();
        $fechaactual = date("d") . "/" . date("m") . "/" . date("Y");

        $html = '<html>
                <head>
                    <title>Cotización solo detalle</title>
                    <style>
                        @page {
                             margin: 0.3in 0.3in 0.3in 0.3in;
                        }
                        .teacherPage {
                            page: teacher;
                            page-break-after: always;
                        }
                        body{
                            font-family: Arial, Helvetica, sans-serif;
                            font-size: 9pt;
                            margin-top: 2cm;
                            margin-left: 0cm;
                            margin-right: 0cm;
                            margin-bottom: 0cm;
                        }  
                        header {
                            position: fixed;
                            top: 0cm;
                            left: 0cm;
                            right: 0cm;
                            height: 3cm;
                        }
                        .cuerpo {
                            text-align: justify;
                        }
                        img.izquierda {
                            float: left;
                        }
                        img.derecha {
                            float: right;
                        }
                        div.page_break {
                            page-break-before: always;
                        }
                        .page-number {
                          text-align: right;
                        }
                        
                        .page-number:before {
                          content: counter(page);
                        }
                        th { 
                            text-align: center; 
                            border: 1px solid black;
                        }
                    </style>
                </head>
                <body>
                
                <header>
                    <table  width="700px" align="center" cellspacing="0" cellpadding="2" style="border: 1px solid black;">
                        <tr>
                            <td width="20%" rowspan="4">
                                <img src="'.public_url_ftp().'Imagenes/formatos/2/logoFSC.jpg" width="100" height="60" />    
                            </td>
                            <td width="60%" align="center" rowspan="4">
                                <h2>COTIZACION DE SERVICIO DE ENSAYO</h2>
                            </td>
                            <td width="20%" align="center" colspan="2">
                                FSC-F-LAB-07
                            </td>
                        </tr>
                        <tr>
                            <td>Versión</td>
                            <td align="right">03</td>
                        </tr>
                        <tr>
                            <td>Fecha</td>
                            <td align="right">05/04/2022</td>
                        </tr>
                        <tr>
                            <td>Página</td>
                            <td align="right"><div class="page-number"></div></td>
                        </tr>
                    </table>
                </header>';

        			
        $res = $this->mcotizacion->getpdfdatoscoti($idcoti,$nversion);
        if ($res){
            foreach($res as $row){
				$dcotizacion        = $row->dcotizacion;
				$drazonsocial       = $row->drazonsocial;
				$nruc               = $row->nruc;
				$ddireccioncliente  = $row->ddireccioncliente;
				$dtelefono          = $row->dtelefono;
				$dcontacto          = $row->dcontacto;
				$dmail              = $row->dmail;
                $fcotizacion        = $row->fcotizacion;
                $imuestreo          = $row->imuestreo;
                $irecojo            = $row->irecojo;
                $isubtotal          = $row->isubtotal;
                $subtotalimuestra   = $row->subtotalimuestra;
                $pigv               = $row->pigv;
                $pdescuento         = $row->pdescuento;
                $itotal             = $row->itotal;
				$cantprod           = $row->cantprod;
				$summuestra         = $row->summuestra;
				$cforma_pago        = $row->cforma_pago;
				$banco              = $row->banco;
				$detraccion         = $row->detraccion;
                $entrega            = $row->entrega;
                $dcantidadminima    = $row->dcantidadminima;
                $diaspermanecia     = $row->diaspermanecia;
                $diascoti           = $row->diascoti;
                $dobservacion       = $row->dobservacion;
                $usuariocrea        = $row->usuariocrea;
                $verprecios         = $row->verprecios;
                $digv               = $row->digv;
                $ddescuento         = $row->ddescuento;
                $condescuento       = $row->condescuento;
                $namefile           = $row->namefile;
                $ctipocambio        = $row->ctipocambio;
                $dtipocambio        = $row->dtipocambio;              
                
                
                if ($ctipocambio  == 'S') :
                    $var_moneda = 'S/.';
                else:
                    $var_moneda = '$';
                endif;
			}
		}
                
        $html .= '
            <main>
                <table width="700px" align="center" cellspacing="0" cellpadding="2" style="border: 1px solid black;">
                    <tr>
                        <td colspan="4"><b>N°'.$dcotizacion.'</b></td>
                    </tr>
                    <tr>
                        <td colspan="4" style="height:2px;"></td>
                    </tr>
                    <tr>
                        <td colspan="4" ><b>I CLIENTE</b></td>
                    </tr>
                    <tr>
                        <td width="80px">Razón Social:</td>
                        <td width="360px">'.$drazonsocial.'</td>
                        <td width="50px">RUC:</td>
                        <td width="190px">'.$nruc.'</td>
                    </tr>
                    <tr>
                        <td>Dirección:</td>
                        <td>'.$ddireccioncliente.'</td>
                        <td>Teléfono:</td>
                        <td>'.$dtelefono.'</td>
                    </tr>
                    <tr>
                        <td>Contacto:</td>
                        <td>'.$dcontacto.'</td>
                        <td>E-mail:</td>
                        <td>'.$dmail.'</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>Fecha:</td>
                        <td>'.$fcotizacion.'</td>
                    </tr>
                </table>
                <table width="700px" align="center" cellspacing="0" cellpadding="2" style="border: 1px solid black;">
                    <tr>
                        <td><b>II DETALLES</b></td>
                    </tr>
                </table>
                <table width="700px" align="center" cellspacing="1" cellpadding="0" FRAME="void" RULES="rows">
                    <tr>
                        <th width="30%" align="center">LOCAL</th>
                        <th width="35%" align="center">PRODUCTO</th>
                        <th width="20%" align="center">CONDICIONES DE LA MUESTRA</th>
                        <th width="15%" align="center">CANTIDAD MUESTRA MINIMA</th>
                    </tr>';
                    $resprod = $this->mcotizacion->getpdfdatosprod($idcoti,$nversion);
                    if ($resprod){
                        foreach($resprod as $rowprod){
                            $destablecimiento = $rowprod->destablecimiento;
                            $dproducto = $rowprod->dproducto;
                            $condicion = $rowprod->condicion;
                            $procedencia = $rowprod->procedencia;
                            $dcantidadminima = $rowprod->dcantidadminima;
                            $html .= '<tr>
                                <td width="30%">&nbsp;'.$destablecimiento.' <br> &nbsp;'.$procedencia.'</td>
                                <td width="35%">&nbsp;'.$dproducto.'</td>
                                <td width="20%">&nbsp;'.$condicion.'</td>
                                <td width="15%">&nbsp;'.$dcantidadminima.'</td>
                            </tr>';
                        }
                    }        
                $html .= '</table>';
                $html .= '<table width="350px" style="margin-left: 15px; height:12px;">
                    <tr>
                        <td>Cantidad de Productos:</td>
                        <td>'.$cantprod.'</td>
                        <td>Suma de Muestras:</td>
                        <td>'.$summuestra.'</td>
                    </tr>
                    <tr>
                        <td colspan="4" style="height:10px;"></td>
                    </tr>
                </table>
                <table width="700px" align="center" cellspacing="1" cellpadding="0" FRAME="void" RULES="rows">
                    <tr >
                        <th width="10%" align="center">Codigo Metodo</th>
                        <th width="35%" align="center">METODO DE ENSAYO</th>
                        <th width="10%" align="center">AC / NOAC</th>
                        <th width="25%" align="center">NORMA / REFERENCIA</th>
                        <th width="10%" align="center" colspan="3">Cant.</th>
                    </tr>';
                    $resproddet = $this->mcotizacion->getpdfdatosprod($idcoti,$nversion);
                    if ($resproddet){
                        foreach($resproddet as $rowproddet){
                            $dproductodet = strtoupper($rowproddet->dproducto);
                            $idproduc = $rowproddet->nordenproducto;
                            $subtotal = $rowproddet->subtotal;
                            
                            if ($verprecios  == 'S') :
                                $var_subtotal = $subtotal;
                            else:
                                $var_subtotal = null;
                            endif;
                            
                            $html .= '<tr>
                                <td colspan="6" ><h3>'.$dproductodet.'</h3>
                                </td>
                                <td align="right"><h3></h3>
                                </td>
                            </tr>';
                                $resensadet = $this->mcotizacion->getlistarensayo($idcoti,$nversion,$idproduc);
                                if ($resensadet){
                                    foreach($resensadet as $rowensadet){
                                        $codigo = $rowensadet->CODIGO;
                                        $densayo = $rowensadet->DENSAYO;
                                        $acre = $rowensadet->ACRE;
                                        $norma = $rowensadet->NORMA;
                                        $cantidad = $rowensadet->CANTIDAD;
                                        $costoensa = $rowensadet->CONSTOENSAYO;
                                        $costo = $rowensadet->COSTO;
                                        
                                        if ($verprecios  == 'S') :
                                            $var_costoensa = $costoensa;
                                        else:
                                            $var_costoensa = null;
                                        endif;
                                        if ($verprecios  == 'S') :
                                            $var_costo = $costo;
                                        else:
                                            $var_costo = null;
                                        endif;

                                        $html .= '<tr>
                                        <td width="10%" align="center">
                                        '.$codigo.'
                                        </td>
                                        <td width="30%">
                                        '.$densayo.'
                                        </td>
                                        <td width="5%" align="center">
                                        '.$acre.'
                                        </td>
                                        <td width="40%">
                                        '.$norma.'
                                        </td>
                                        <td width="15%" align="center" colspan="3">
                                        '.$cantidad.'
                                        </td>
                                        </tr>';
                                    }
                                }
                                $html .= '';
                        }
                    }
                    $html .= '</table>                    
                    <table width="698px" align="center">';
                    
                if ($ctipocambio  == 'S') :
                    $html .= '<tr>
                            <td width="470px" >AC: Método Acreditado<br>NO AC: Método No Acreditado</td>
                            <td width="130px" > </td>
                            <td width="80px"></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td> </td>
                            <td></td> 
                        </tr>';
                    if($pdescuento > 0){
                    $html .= '<tr>
                                <td></td>
                                <td> </td>
                                <td </td>
                            </tr>
                            <tr>                
                                <td></td>
                                <td> </td>
                                <td</td>
                            </tr>';
                    }
                    $html .= '<tr>                
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>                
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td style="height:10px;">
                            </td>
                        </tr>';
                else:
                    $html .= '<tr>
                        <td width="530px" >AC: Método Acreditado<br>NO AC: Método No Acreditado</td>
                        <td width="70px"></td>
                        <td width="80px" align="right"></td>
                    </tr>
                    <tr>
                        <td style="height:10px;">
                        </td>
                    </tr>';
                endif;                    

                    $html .= '</table>';

                    $html .= '<table width="700px" align="center">
                    <tr>
                        <td><b>III</b></td>
                        <td><b>FORMA DE PAGO:</b></td>
                        <td align="left">'.$cforma_pago.'</td>
                        
                    </tr>       
                    <tr>
                        <td colspan="4" style="height:10px;">
                        </td>
                    </tr>
        
                    <tr>
                        <td><b>IV</b></td>
                        <td colspan="3"><b>CUENTAS BANCARIAS:</b></td>
                    </tr>
                    <tr>
                        <td colspan="1"></td>
                        <td colspan="3">'.$banco.' CCI: 00219400156160801692</td>                
                    </tr>
                    <tr>
                        <td colspan="1"></td>
                        <td colspan="3">'.$detraccion.'</td>
                    </tr>
                    <tr>
                        <td colspan="1"></td>
                        <td colspan="3">CTA CORRIENTE EN DOLARES BCP: 194-2425691-1-38</td>
                    </tr>     
                    <tr>
                        <td colspan="1"></td>
                        <td align="left">Dirección de Banco</td>
                        <td colspan="2">Calle Centenario 156, La Molina, Lima 13 – Perú</td>
                    </tr>
                    <tr>
                        <td colspan="1"></td>
                        <td align="left">Teléfono del Banco</td>
                        <td colspan="2">511-3119898</td>
                    </tr>
                    <tr>
                        <td colspan="1"></td>
                        <td align="left"># Swift</td>
                        <td colspan="2">BCPLPEPL</td>
                    </tr>
                    <tr>
                        <td colspan="1"></td>
                        <td align="left">Razón social</td>
                        <td colspan="2">FS CERTIFICACIONES S.A.C.</td>
                    </tr>
                    <tr>
                        <td colspan="1"></td>
                        <td align="left">RUC</td>
                        <td colspan="2">20514553719</td>
                    </tr>
                    <tr>
                        <td colspan="4" style="height:10px;">
                        </td>
                    </tr>
        
                    <tr>
                        <td><b>V</b></td>
                        <td colspan="2"><b>TIEMPO DE ENTREGA DEL INFORME:</b></td>
                        <td style="text-align: justify;">'.$entrega.'</td>
                    </tr>
                    <tr>
                        <td colspan="4" style="height:10px;">
                        </td>
                    </tr>
                    <tr>
                        <td VALIGN=top><b>VI</b></td>
                        <td VALIGN=top style="text-align: justify;"><b>RECEPCIÓN DE MUESTRAS:</b></td>
                        <td colspan="3" style="text-align: justify;">Dirección de Laboratorio: Pasaje Monte azul 120-180. Oficina 203-204. Chacarilla. Surco</td>
                    </tr>   
                    <tr>
                        <td></td>
                        <td></td>
                        <td colspan="3" style="text-align: justify;"><b></b>Horario: Lunes a viernes 9h a 13h y de 14h a 17h. Sábados de 9h a 12h</td>
                    </tr>          
                    <tr>
                        <td colspan="4" style="height:10px;">
                        </td>
                    </tr>
                    <tr>
                        <td VALIGN=top ><b>VII</b></td>
                        <td colspan="3"><b>CONTACTO:</b>Para cualquier consulta adicional póngase en contacto con su Ejecutivo o Asistente comercial:</td>
                    </tr>
                    <tr>
                        <td VALIGN=top ></td>
                        <td colspan="3" align="left">'.$usuariocrea.'</td>
                    </tr>
                    <tr>
                        <td colspan="4" style="height:10px;"></td>
                    </tr>
                    <tr>
                        <td VALIGN=top><b>VIII</b></td>
                        <td colspan="4" style="text-align: justify;"><b>PERMANENCIA DE LA CONTRA MUESTRA EN EL LABORATORIO:</b>  En caso de que el servicio considere contramuestras, éstas se conservarán en el laboratorio por el período acordado con el cliente, 
                        luego de lo cual serán eliminadas de acuerdo a nuestros procedimientos  internos. En caso el cliente requiera la devolución de la contramuestra, esta deberá ser solicitada antes de la finalización del Tiempo de Custodia.</td>
                    </tr>            
                    <tr>
                        <td colspan="4" style="height:10px;">
                        </td>
                    </tr>
                </table>';
                
                $html .= '<table width="700px" align="center">
                    <tr>
                        <td VALIGN=top ><b>IX</b></td>
                        <td ><b>VIGENCIA DE COTIZACION:</b></td>
                        <td align="left">'.$diascoti.'</td>
                    </tr>
                    <tr>
                        <td VALIGN=top><b>X</b></td>
                        <td colspan="2"><b>ACEPTACION DE LA COTIZACION:</b></td>
                    </tr>
                    <tr>  
                        <td VALIGN=top>-</td>
                        <td colspan="2" style="text-align: justify;">En caso de Aceptar la Cotización, favor de enviarla firmada o declaranado su aceptación mediante un correo electronico. Cualquier cambio en la cotización enviada deberá solicitarse antes de iniciado el servicio.</td>
                    </tr>
                    <tr>
                        <td VALIGN=top>-</td>
                        <td colspan="2" style="text-align: justify;">Al dar su visto bueno, el cliente acepta esta cotización con carácter de contrato.</td>
                    </tr>
                    <tr>
                        <td VALIGN=top>-</td>
                        <td colspan="2" style="text-align: justify;">Toda la información derivada de las actividades del laboratorio obtenidas a través del servicio brindado se conservará de modo confidencial, excepto por la información que el cliente pone a disposición del público, o cuando lo acuerdan el laboratorio y el cliente.</td>
                    </tr>
                    <tr>
                        <td VALIGN=top>-</td>
                        <td colspan="2" style="text-align: justify;">Cuando a FS Certificaciones se le solicite por ley o disposiciones contractuales divulgar información confidencial, notificará al cliente salvo que esté prohibido por ley.</td>
                    </tr>
                    <tr>
                        <td VALIGN=top>-</td>
                        <td colspan="2" style="text-align: justify;">La información acerca del cliente, obtenida de fuentes diferentes del cliente se tratará como información confidencial.</td>
                    </tr>
                    <tr>
                        <td VALIGN=top>-</td>
                        <td colspan="2" style="text-align: justify;">La emisión de los Informes de Ensayo se realizará en formato digital (PDF), los cuales serán enviados por correo electrónico al cliente. El envío de los informes físicos tiene un costo adicional que será informado al cliente.</td>
                    </tr>
                    <tr>
                        <td VALIGN=top>-</td>
                        <td colspan="2" style="text-align: justify;">Copias adicionales de los informes y las traducciones de los mismos tendrán un costo adicional.</td>
                    </tr>
                    <tr>
                        <td VALIGN=top>-</td>
                        <td colspan="2" style="text-align: justify;">En FS Certificaciones estamos comprometidos a prevenir actos de soborno conforme a lo establecido en nuestra política de calidad. En base a ello rechazamos cualquier tipo de ofrecimientos, pago, solicitud o aceptación de soborno de cualquier tipo. FS Certificaciones procederá a realizar la denuncia de este tipo de actos conforme a los mecanismos correspondientes.</td>
                    </tr>
                    <tr>
                        <td VALIGN=top>-</td>
                        <td colspan="2" style="text-align: justify;">Modificaciones solicitadas posterior a la emisión de los Informes de Ensayo, luego de que el cliente revisó los previos y dio conformidad, tendrán un costo adicional que le serán comunicados oportunamente.</td>
                    </tr>
                    <tr>
                        <td VALIGN=top>-</td>
                        <td colspan="2" style="text-align: justify;">En caso se determine que no se cumplen las cantidades mínimas o las condiciones, la muestra se considerará muestra no idónea y sólo con autorización del cliente será ensayada y se reportará como Informe No Acreditado.</td>
                    </tr>
                    <tr>
                        <td VALIGN=top>-</td>
                        <td colspan="2" style="text-align: justify;">En caso el cliente tenga una queja sobre el servicio brindado enviar un correo a adiaz@fscertificaciones.com para recibir una descripción del proceso de tratamiento de quejas.</td>
                    </tr>
                    <tr>
                        <td colspan="3" style="height:10px;"></td>
                    </tr>
                    <tr>
                        <td VALIGN=top width="10px"><b>XI</b></td>
                        <td width="180px"><b>OBSERVACIONES:</b></td>
                        <td>'.$dobservacion.'</td>
                    </tr>
                    <tr>
                        <td colspan="3" style="height:10px;"></td>
                    </tr>
                </table>';
                
                $html .= '<table width="700px" align="center">
                    <tr>
                        <td colspan="4" style="height:80px;"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td align="center" style="border-top: 1px solid black;">CLIENTE</td>
                        <td></td>
                        <td align="center">FSC LABORATORIO</td>
                    </tr>
                    <tr>
                        <td colspan="4" style="height:10px;"></td>
                    </tr>
                </table>';

        $html .= '</main></body></html>';
		$filename = 'COT '.$namefile.' '.$drazonsocial;
        $this->pdfgenerator->generate($html, $filename, TRUE, 'A4', 'portrait');
        
    }
}
?>