<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Style\Conditional;
use PhpOffice\PhpSpreadsheet\Writer\IWriter;
use PhpOffice\PhpSpreadsheet\IOFactory;

class pdfinformes extends CI_Controller {
	function __construct() {
		parent:: __construct();	
		$this->load->model('lab/resultados/minformes');
		$this->load->model('mglobales');
		$this->load->library('encryption');
		$this->load->helper(array('form','url','download','html','file'));
		$this->load->library('form_validation');
    }
    
   /** INFORMES POR MUESTRAS **/  
    
   /** INFORMES PREVIO POR MUESTRAS **/                
	public function pdfInfensayoBorrador($cinternoordenservicio,$vcmuestra) { // recupera 
        $this->load->library('pdfgenerator');

        $html = '<html>
                <head>
                    <title>Informe Borrador</title>
                    <style type="text/css">
                        @page{
                             margin: 0.3in 0.3in 0.3in 0.3in;
                        }
                        body{
                            font-family: Arial, Helvetica, sans-serif;
                            margin-top: 0cm;
                            margin-left: 1cm;
                            margin-right: 1cm;
                            margin-bottom: 3.5cm;
                        }
                        #header,
                        #footer {
                            position: fixed;
                            left: 0;
                            right: 0;
                            color: #000000;
                            font-size: 0.9em;
                        }                        
                        #header {
                            top: 0;
                            height: 1cm;
                        }                      
                        #footer {
                            bottom: 0;
                            border-top: 0.1pt solid #aaa;
                            height: 3.5cm;
                        }                        
                        #header table,
                        #footer table{
                            width: 100%;
                            border-collapse: collapse;
                            border: none;
                        }                        
                        #header td,
                        #footer td{
                            padding: 0;
                        }
                        .page-number{
                            text-align: right;
                        }
                        .page-number:before{
                            content: "Pagina " counter(page);
                        }    
                        #caratula td{
                            font-size:0.8em;        
                        }
                        #tableborder{
                            border-collapse: collapse;
                            width: 100%;
                            font-size:0.8em;
                            border: 0.5pt solid black;
                        }
                        #tableborder td{
                            border: 0.5pt solid black;
                            padding-left: 5px;
                        }
                        #tableborder th{
                            text-align: center;
                            border: 0.5pt solid black;
                            background-color: #DBD7D7;
                        }
                        .list-unstyled{
                            padding-left: 0;
                            list-style: none;
                        }
                    </style>
                </head>
                <body>  
                    <div id="footer">
                        <table width="100%" style="font-size:0.8em; margin-top: 0.3cm; text-align: justify">
                        <tr>
                            <td width="100%" align="left" colspan="2">
                                Los resultados de los ensayos pertenecen sólo a las muestras ensayadas y no deben ser utilizados como una certificación de conformidad con normas del producto o como certificado del sistema de calidad de la entidad que lo produce. Queda prohibida la reproducción total o parcial de este informe, sin la autorización escrita de FS Certificaciones S.A.C.                            
                            </td>
                        </tr>
                        <tr>
                            <td width="100%" align="left" colspan="2"> &nbsp; </td>
                        </tr>
                        <tr>
                            <td width="50%" align="left"> 
                                FSC-F-LAB-011/V.03                           
                            </td>
                            <td width="50%" align="right" >
                                <div class="page-number"></div>
                            </td>
                        </tr>
                        <tr>
                            <td width="100%" align="center" colspan="2">
                                <ul class="list-unstyled">
                                    <li>Jr. Monterrey N° 221 Of. 201-202, Urb. Chacarilla del Estanque. Santiago de Surco, Lima - Perú</li>
                                    <li>Teléfonos: (51-1) 480 0561 - Anexo: Lab 111</li>
                                    <li>www.fscertificaciones.com</li>
                                </ul>
                            </td>
                        </tr>
                        </table>                    
                    </div>'; 
                   $parametroscabe = array(
                        '@cinternoordenservicio'         => $cinternoordenservicio,
                        '@cmuestra'       => $vcmuestra,
                    );
                    $res = $this->minformes->getinfxmuestras_caratula($parametroscabe);
                    $pos = 0;
                    
                    if ($res){
                        foreach($res as $row){ 
                            $NROINFORME     = $row->NROINFORME;
                            $CLIENTE        = $row->CLIENTE;
                            $DIRECCION      = $row->DIRECCION;
                            $NROORDEN       = $row->NROORDEN;
                            $PROCEDENCIA    = $row->PROCEDENCIA;
                            $FMUESTRA       = $row->FMUESTRA;
                            $FRECEPCION     = $row->FRECEPCION;
                            $FANALISIS      = $row->FANALISIS;
                            $LUGARMUESTRA   = $row->LUGARMUESTRA ;
                            $CMUESTRA       = $row->CMUESTRA;
                            $DPRODUCTO      = $row->DPRODUCTO;
                            $DTEMPERATURA   = $row->DTEMPERATURA;
                            $DLCLAB         = $row->DLCLAB;
                            $OBSERVACION    = $row->OBSERVACION;
                            $SOBSERV        = $row->SOBSERV;
                            $ACNAC          = $row->ACNAC;
                            $nporcion       = $row->nporcion;
                                                       
                            $parametros = array(
                                '@cinternoordenservicio'    => $cinternoordenservicio,
                                '@cmuestra'                 => $CMUESTRA,
                            ); 

                            if($pos > 0){
                                $html .= '<h1 style="page-break-before: always;" size="0"/>';             
                            }
                                            
        $html .= '<div id="main">
                    <table width="100%" align="center">
                    <tr>
                        <td colspan="3" style="font-size:1.1em;" align="center">
                            <b>INFORME BORRADOR '.$CMUESTRA.'</b>
                        </td>
                    </tr>
                    </table>                           
                    <table id="caratula" width="100%" align="center" cellspacing="0" cellpadding="2" >                       
                            
                        <tr>
                            <td align="left">
                                <b>Nombre del Cliente</b>   
                            </td>
                            <td align="left" colspan="2">
                                : '.$CLIENTE.'   
                            </td>
                        </tr>
                        <tr style="vertical-align:top">
                            <td align="left">
                                <b>Dirección del Cliente</b>  
                            </td>  
                            <td align="left" colspan="2">
                                : '.$DIRECCION.'   
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <b>N° Orden de Trabajo</b>   
                            </td>
                            <td align="left" colspan="2">
                                : '.$NROORDEN.'   
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <b>Procedencia de la Muestra</b>  
                            </td>
                            <td align="left" colspan="2">
                                : '.$PROCEDENCIA.'   
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <b>Fecha de Muestreo</b>    
                            </td>
                            <td align="left" colspan="2">
                                : '.$FMUESTRA.'   
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <b>Fecha de Recepción</b>    
                            </td>
                            <td align="left" colspan="2">
                                : '.$FRECEPCION.'   
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <b>Fecha de Análisis</b>    
                            </td>
                            <td align="left" colspan="2">
                                : '.$FANALISIS.'  
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <b>Lugar de Muestreo</b>   
                            </td>
                            <td align="left" colspan="2">
                                : '.$LUGARMUESTRA.'  
                            </td>
                        </tr>
                        <tr style="vertical-align:top">
                            <td width="27%" align="left">
                                <b>Muestra / Descripción</b>    
                            </td>
                            <td width="7%" align="left">
                                : '.$CMUESTRA.'   
                            </td>
                            <td width="66%" align="left">
                                '.$DPRODUCTO.'   
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <b>Temperatura de Recepción</b>    
                            </td>
                            <td align="left" colspan="2">
                                : '.$DTEMPERATURA.'   
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="3">
                            &nbsp;   
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="3" style="height:30px; border-top:solid 3px #000000">
                                <b><u>RESULTADOS DE ENSAYO</u></b>   
                            </td>
                        </tr>
                        
                    </table>';

                    ///RESULTADOS MICROBIOLOGIA//
                     $resmicro = $this->minformes->getinfxmuestras_resmicro($parametros);
                     if ($resmicro){
                         $html .= '<table id="tableborder">
                         <thead>
                         <tr>
                             <th> <b>Ensayo</b> </th>
                             <th> <b>Unidades</b> </th>
                             <th> <b>Via</b> </th>
                             <th> <b>Resultado</b> </th>
                         </tr>
                         </thead>
                         <tbody>';
                         $DENSAYOmicro = '';
                         foreach($resmicro as $rowmicro){
                             $DENSAYO = $rowmicro->DENSAYO;
                             $UNIDADMEDIDA = $rowmicro->UNIDADMEDIDA;
                             $VIA = $rowmicro->VIA;
                             $RESULT_FINAL = (strlen($rowmicro->EXP10RES) > 0) ? $rowmicro->RESULTADO : $rowmicro->RESULT_FINAL;
                             $EXP10RES = $rowmicro->EXP10RES;

                             $html .= '<tr>';
                             if ($DENSAYO == $DENSAYOmicro){
                                $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                             }else{
                                $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                             }
                             
                             $html .= '<td width="130px" align="center">'.$UNIDADMEDIDA.'</td>
                                 <td width="50px" align="center">'.$VIA.'</td>
                                 <td width="130px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'<sup>'.$EXP10RES.'</sup></td>
                             </tr>';
                             $DENSAYOmicro = $DENSAYO;
                         }
                         $html .= '</tbody></table><br>';
                     }  
                    ///RESULTADOS FISICOQUIMICO///
                     $resfq = $this->minformes->getinfxmuestras_resfq($parametros);
                     if ($resfq){
                         $html .= '<table id="tableborder">
                         <thead>
                         <tr>
                             <th> <b>Ensayo</b> </th>
                             <th> <b>Unidades</b> </th>
                             <th> <b>Via</b> </th>
                             <th> <b>Resultado</b> </th>
                         </tr>
                         </thead>
                         <tbody>';
                         $DENSAYOfq = '';
                         foreach($resfq as $rowfq){
                             $DENSAYO = $rowfq->DENSAYO;
                             $UNIDADMEDIDA = $rowfq->UNIDADMEDIDA;
                             $VIA = $rowfq->VIA;
                             $RESULT_FINAL = $rowfq->RESULT_FINAL;
                             $EXP10RES = $rowfq->EXP10RES;

                             $html .= '<tr>';
                             if ($DENSAYO == $DENSAYOfq){
                                $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                             }else{
                                $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                             }

                             $html .= '<td width="130px" align="center">'.$UNIDADMEDIDA.'</td>
                                 <td width="50px" align="center">'.$VIA.'</td>
                                 <td width="130px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'<sup>'.$EXP10RES.'</sup></td>
                             </tr>';
                             $DENSAYOfq = $DENSAYO;
                         }
                         $html .= '</tbody></table><br>';
                     }
                    ///RESULTADOS INSTRUMENTAL TERCERO///
                      $resins = $this->minformes->getinfxmuestras_resinstru($parametros);
                      if ($resins){
                          $html .= '<table id="tableborder">
                          <thead>
                          <tr>
                              <th> <b>Ensayo</b> </th>
                              <th> <b>Unidades</b> </th>
                              <th> <b>Via</b> </th>
                              <th> <b>Resultado</b> </th>
                          </tr>
                          </thead>
                          <tbody>';
                          $DENSAYOins = '';
                          foreach($resins as $rowins){
                              $DENSAYO = $rowins->DENSAYO;
                              $UNIDADMEDIDA = $rowins->UNIDADMEDIDA;
                              $VIA = $rowins->VIA;
                              $RESULT_FINAL = $rowins->RESULT_FINAL;
                              $EXP10RES = $rowins->EXP10RES;
 
                              $html .= '<tr>';
                              if ($DENSAYO == $DENSAYOins){
                                 $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                              }else{
                                 $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                              }
 
                              $html .= '<td width="130px" align="center">'.$UNIDADMEDIDA.'</td>
                                  <td width="50px" align="center">'.$VIA.'</td>
                                  <td width="130px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'<sup>'.$EXP10RES.'</sup></td>
                              </tr>';
                              $DENSAYOins = $DENSAYO;
                          }
                          $html .= '</tbody></table><br>';
                      }
                    ///RESULTADOS SENSORIAL///
                     $ressen = $this->minformes->getinfxmuestras_ressenso($parametros);
                     if ($ressen){
                         $html .= '<table id="tableborder" style="page-break-inside: avoid;">
                         <thead>
                         <tr>
                             <th> <b>Ensayo</b> </th>
                             <th> <b>Via</b> </th>
                             <th> <b>Atributo</b> </th>
                             <th> <b>Resultado</b> </th>
                         </tr>
                         </thead>
                         <tbody>';
                         $DENSAYOsen = '';
                         foreach($ressen as $rowsen){
                             $DENSAYO = $rowsen->DENSAYO;
                             $DNOMBREESCALA = $rowsen->DNOMBREESCALA;
                             $NVIAUSADO = $rowsen->NVIAUSADO;
                             $RESULT_FINAL = $rowsen->RESULT_FINAL;
                             $html .= '<tr style="vertical-align:top">';
                             if ($DENSAYO == $DENSAYOsen){
                                $html .= '';
                             }else{
                                $html .= '<td rowspan="5" style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                             }

                             $html .= '<td width="30px" align="center">'.$NVIAUSADO.'</td>
                                 <td width="100px">'.$DNOMBREESCALA.'</td>
                                 <td width="210px">'.str_replace("<", "&lt;", $RESULT_FINAL).'</td>
                             </tr>';
                             $DENSAYOsen = $DENSAYO;
                         }
                         $html .= '</tbody></table><br>';
                     }
                   
                    ///RESULTADOS CON ELEMENTOS///
                     $resele = $this->minformes->getinfxmuestras_reselementos($parametros);
                     if ($resele){
                        $html .= '<table id="tableborder">
                        <thead>
                        <tr>
                            <th> <b>Ensayo</b> </th>
                            <th> <b>Via</b> </th>
                            <th> <b>Elemento</b> </th>
                            <th> <b>Unidades</b> </th>
                            <th> <b>Resultado</b> </th>
                        </tr>
                        </thead>
                        <tbody>';
                        $DENSAYOele = '';
                        foreach($resele as $rowele){
                            $DENSAYO = $rowele->DENSAYO;
                            $ELEMENTO = $rowele->ELEMENTO;
                            $UNIDADMEDIDA = $rowele->UNIDADMEDIDA;
                            $VIA = $rowele->VIA;
                            $RESULT_FINAL = $rowele->RESULT_FINAL;
                            $html .= '<tr>';
                            if ($DENSAYO == $DENSAYOele){
                               $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                            }else{
                               $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                            }

                            $html .= '<td width="30px" align="center">'.$VIA.'</td>
                                <td>'.$ELEMENTO.'</td>
                                <td width="100px" align="center">'.$UNIDADMEDIDA.'</td>
                                <td width="130px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'</td>
                            </tr>';
                            $DENSAYOele = $DENSAYO;
                        }
                        $html .= '</tbody></table><br>';
                     }
                    ///RESULTADOS ESTERILIDAD BAJA///
                     $parametrosEsterili = array(
                        '@cinternoordenservicio' => $cinternoordenservicio,
                        '@cmuestra' => $CMUESTRA,
                        '@stipoacidez' => 'B'
                     );
                     $resesteb = $this->minformes->getinfxmuestras_resesteri($parametrosEsterili);
                     if ($resesteb){
                        $html .= '<table id="tableborder">
                        <tr>
                            <td colspan="7"> Baja acidez (pH>4.6)</td>
                        </tr>
                        <tr>
                            <th width="30%" rowspan="2"> <b>Preincubación 35°C/14 días</b> </th>
                            <th width="5%" rowspan="2"> <b>pH</b> </th>
                            <th width="25%" colspan="2"> <b>Mesófilos (35°C)</b> </th>
                            <th width="15%" colspan="2"> <b>Termófilos (55°C)</b> </th>
                            <th width="25%" rowspan="2"> <b>Resultado</b> </th>
                        </tr>
                        <tr>
                            <th> <b>Caldo púrpura de bromocreasol 120h</b> </th>
                            <th> <b>Caldo carne cocida 120h</b> </th>
                            <th> <b>Caldo púrpura de bromocreasol 48h</b> </th>
                            <th> <b>Caldo carne cocida 72h</b> </th>
                        </tr>';
                        foreach($resesteb as $rowesteb){
                            $preincuba = $rowesteb->preincuba;
                            $dph = $rowesteb->dph;
                            $dbames35cp = $rowesteb->dbames35cp;
                            $dbames35cc = $rowesteb->dbames35cc;
                            $dbater55cp = $rowesteb->dbater55cp;
                            $dbater55cc = $rowesteb->dbater55cc;
                            $result_final = $rowesteb->result_final;
                            $html .= '<tr>
                                <td>'.$preincuba.'</td>
                                <td align="center">'.$dph.'</td>
                                <td align="center">'.$dbames35cp.'</td>
                                <td align="center">'.$dbames35cc.'</td>
                                <td align="center">'.$dbater55cp.'</td>
                                <td align="center">'.$dbater55cc.'</td>
                                <td align="center">'.$result_final.'</td>
                            </tr>';
                        }
                        $html .= '</table><br>';
                     }
                    ///RESULTADOS ESTERILIDAD ALTA///
                     $parametrosEsterili = array(
                        '@cinternoordenservicio' => $cinternoordenservicio,
                        '@cmuestra' => $CMUESTRA,
                        '@stipoacidez' => 'A'
                     );
                     $resestea = $this->minformes->getinfxmuestras_resesteri($parametrosEsterili);
                     if ($resestea){
                        $html .= '<table id="tableborder">
                        <tr>
                            <td colspan="7"> Alta acidez ('.str_replace("<", "&lt;", "pH<4.6").')</td>
                        </tr>
                        <tr>
                            <th width="30%" rowspan="2"> <b>Preincubación 35°C/14 días</b> </th>
                            <th width="5%" rowspan="2"> <b>pH</b> </th>
                            <th width="25%" colspan="2"> <b>Mesófilos (35°C)</b> </th>
                            <th width="15%" colspan="2"> <b>Termófilos (55°C)</b> </th>
                            <th width="25%" rowspan="2"> <b>Resultado</b> </th>
                        </tr>
                        <tr>
                            <th> <b>Caldo ácido 96h</b> </th>
                            <th> <b>Caldo extracto de malta 96h</b> </th>
                            <th colspan="2"> <b>Caldo ácido 48h</b> </th>
                        </tr>';
                        foreach($resestea as $rowestea){
                            $preincuba = $rowestea->preincuba;
                            $dph = $rowestea->dph;
                            $dacmes30ca = $rowestea->dacmes30ca;
                            $dacmes30cm = $rowestea->dacmes30cm;
                            $dacter55ca = $rowestea->dacter55ca;
                            $result_final = $rowestea->result_final;
                            $html .= '<tr>
                                <td>'.$preincuba.'</td>
                                <td align="center">'.$dph.'</td>
                                <td align="center">'.$dacmes30ca.'</td>
                                <td align="center">'.$dacmes30cm.'</td>
                                <td align="center" colspan="2">'.$dacter55ca.'</td>
                                <td align="center">'.$result_final.'</td>
                            </tr>';
                        }
                        $html .= '</table><br>';
                     }

                    ///RESULTADOS ETIQUETADO NUTRICIONAL///
                      $parametrosEtiquetado = array(
                          '@cinternoordenservicio' => $cinternoordenservicio,
                          '@cmuestra' => $CMUESTRA
                      );
                      $resetiqueta = $this->minformes->getinfxmuestras_resetiquetado($parametrosEtiquetado);
                      if ($resetiqueta){
                        $html .= '<table id="tableborder">
                          <tr>
                              <td colspan="7"> <b>INFORMACION NUTRICIONAL</b> </td>
                          </tr>
                          <tr>
                              <td width="40%" colspan="4"> Tamaño de Porción :</td>
                              <td colspan="3">'.$nporcion.'</td>
                          </tr>
                          <tr>
                              <th width="25%" colspan="3"> <b>Ensayo</b> </th>
                              <th width="15%"> <b>Unidad</b> </th>
                              <th width="20%"> <b>100g</b> </th>
                              <th width="20%"> <b>Porción</b> </th>
                              <th width="20%"> <b>% VDR(*)</b> </th>
                          </tr>';
                          foreach($resetiqueta as $rowetiqueta){
                              $DENSAYO = $rowetiqueta->DENSAYO;
                              $UNIDADMEDIDA = $rowetiqueta->UNIDADMEDIDA;
                              $DRESULTADO = $rowetiqueta->DRESULTADO;
                              $NPORCION = $rowetiqueta->NPORCION;
                              $VALORVR = $rowetiqueta->VALORVR;
                              $html .= '<tr>
                                  <td colspan="3">'.$DENSAYO.'</td>
                                  <td align="center">'.$UNIDADMEDIDA.'</td>
                                  <td align="center">'.$DRESULTADO.'</td>
                                  <td align="center">'.$NPORCION.'</td>
                                  <td align="center">'.$VALORVR.'</td>
                              </tr>';
                          }
                          $html .= '<tr><td colspan="7">(*) % Valor Diario (VD) con base a una dieta de 2000 kcal u 8370 kJ.  según Codex Alimentarius FAO/OMS. Sus valores diarios pueden ser mayores o menores dependiendo de sus necesidades energéticas.</tr>
                                     </table><br>';
                      }
                    ///NOTAS ADICIONALES///
                     $resNOTA1 = $this->minformes->getinfxmuestras_nota01($parametros);
                        if ($resNOTA1){
                            foreach($resNOTA1 as $rowNOTA1){
                                $NOTA01        = $rowNOTA1->NOTA01;
                            }
                        }
                        $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" >                        
                                    <tr>
                                        <td width="100%" align="left" style="font-size: 0.8em;">
                                            '.$DLCLAB.'   
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td width="100%" align="center" style="font-size: 0.8em;">
                                            '.$NOTA01.'   
                                        </td>
                                    </tr> 
                        </table> <br>';
                        
                        $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" >
                                    <tr>
                                        <td width="100%" align="center" colspan="3" style="height:30px; font-size: 0.8em;">
                                            <b><u>METODOS DE ENSAYO</u></b>   
                                        </td>
                                    </tr>
                        </table>';
                    
                    ///METODOS DE ENSAYO///    
                     $resmetensa = $this->minformes->getmetodosensayos($parametros);
                     if ($resmetensa){
                        $html .= '<table id="tableborder">
                        <thead>
                            <tr>
                                <th>
                                    <b>Ensayo</b>   
                                </th>
                                <th>
                                    <b>Norma o Referencia</b>    
                                </td>
                            </tr>
                            </thead>
                            <tbody>';
                            foreach($resmetensa as $rowmetensa){
                                $METDENSAYO = $rowmetensa->DENSAYO;
                                $METDNORMA = $rowmetensa->DNORMA;
                                $html .= '<tr>
                                <td width="180px">'.$METDENSAYO.'</td>
                                    <td>'.$METDNORMA.'</td>
                                </tr>';
                            }
                        $html .= '</tbody></table><br>';
                     }  
                    /// OBSERVACIONES ///
                     if($SOBSERV > 0){
                        $html .= '<table id="tableborder" >
                          <tr>
                            <td width="100%" align="left" >
                                <b>Observaciones :</b>
                            </td>
                          </tr> 
                          <tr>
                            <td width="100%" align="center" >
                                <div style="text-align: justify;">'.$OBSERVACION.'</div>   
                            </td>
                          </tr> 
                        </table> <br>';
                     }
                   

        $html .= '</div>';
                        $pos++;
                   }}
        $html .= '</body>
                </html>';
		$filename = 'INF-BORRADOR';
        $this->pdfgenerator->generate($html, $filename, TRUE, 'A4', 'portrait');
        //echo $html;
    }               
	public function pdfInfensayoMuestra($cinternoordenservicio,$vcmuestra) { // recupera 
        $this->load->library('pdfgenerator');

       try{
        $html = '<html>
        <head>
            <title>Informe</title>
            <style type="text/css">
                @page{
                     margin: 0.3in 0.3in 0.3in 0.3in;
                }
                body{
                    font-family: Arial, Helvetica, sans-serif;
                    margin-top: 3.7cm;
                    margin-left: 1cm;
                    margin-right: 1cm;
                    margin-bottom: 3.5cm;
                }
                #header,
                #footer,
                #nroinf {
                    position: fixed;
                    left: 0;
                    right: 0;
                    color: #000000;
                    font-size: 0.9em;
                }                        
                #header {
                    top: 0;
                    height: 3.7cm;
                }    
                #nroinf {
                    top: 0cm;
                    height: 3.7cm;
                }                     
                #footer {
                    bottom: 0;
                    border-top: 0.1pt solid #aaa;
                    height: 3.5cm;
                }                        
                #header table,
                #footer table,
                #nroinf table {
                    width: 100%;
                    border-collapse: collapse;
                    border: none;
                }                        
                #header td,
                #footer td,
                #nroinf td {
                    padding: 0;
                }
                .page-number{
                    text-align: right;
                }
                .page-number:before{
                    content: "Pagina " counter(page);
                }  
                #salto hr{
                    page-break-before: always;
                    color: #ffffff;
                    margin: -2px;
                }     
                #caratula td{
                    font-size:0.8em;        
                }
                #tableborder{
                    border-collapse: collapse;
                    width: 100%;
                    font-size:0.8em;
                    border: 0.5pt solid black;
                }
                #tableborder td{
                    border: 0.5pt solid black;
                    padding-left: 5px;
                }
                #tableborder th{
                    text-align: center;
                    border: 0.5pt solid black;
                    background-color: #DBD7D7;
                }
                .list-unstyled{
                    padding-left: 0;
                    list-style: none;
                }
            </style>
        </head>
        <body>

            <div id="header">
                <table width="100%" align="center">
                <tr>
                    <td width="25%" align="center">                            
                        <img src="'.public_url_ftp().'Imagenes/formatos/2/logoFSC.jpg" width="170" height="80" />  
                    </td>
                    <td width="50%" align="center" style="font-size:0.85em;"> 
                        &nbsp;
                    </td>
                    <td width="25%" align="center">                        
                        &nbsp;
                    </td>
                </tr>
                </table>
            </div>  

            <div id="footer">
                <table width="100%" style="font-size:0.8em; margin-top: 0.3cm; text-align: justify">
                <tr>
                    <td width="100%" align="left" colspan="2">
                        Los resultados de los ensayos pertenecen sólo a las muestras ensayadas y no deben ser utilizados como una certificación de conformidad con normas del producto o como certificado del sistema de calidad de la entidad que lo produce. Queda prohibida la reproducción total o parcial de este informe, sin la autorización escrita de FS Certificaciones S.A.C.                            
                    </td>
                </tr>
                <tr>
                    <td width="100%" align="left" colspan="2"> &nbsp; </td>
                </tr>
                <tr>
                    <td width="50%" align="left"> 
                        FSC-F-LAB-011/V.03                           
                    </td>
                    <td width="50%" align="right" >
                        <div class="page-number"></div>
                    </td>
                </tr>
                <tr>
                    <td width="100%" align="center" colspan="2">
                        <ul class="list-unstyled">
                            <li>Jr. Monterrey N° 221 Of. 201-202, Urb. Chacarilla del Estanque. Santiago de Surco, Lima - Perú</li>
                            <li>Teléfonos: (51-1) 480 0561 - Anexo: Lab 111</li>
                            <li>www.fscertificaciones.com</li>
                        </ul>
                    </td>
                </tr>
                </table>                    
            </div>'; 
           $parametroscabe = array(
                '@cinternoordenservicio'         => $cinternoordenservicio,
                '@cmuestra'       => $vcmuestra,
            );
            $res = $this->minformes->getinfxmuestras_caratula($parametroscabe);
            $pos = 0;
            if ($res){
                foreach($res as $row){ 
                    $NROINFORME     = $row->NROINFORME;
                    $CLIENTE        = $row->CLIENTE;
                    $DIRECCION      = $row->DIRECCION;
                    $NROORDEN       = $row->NROORDEN;
                    $PROCEDENCIA    = $row->PROCEDENCIA;
                    $FMUESTRA       = $row->FMUESTRA;
                    $FRECEPCION     = $row->FRECEPCION;
                    $FANALISIS      = $row->FANALISIS;
                    $LUGARMUESTRA   = $row->LUGARMUESTRA;
                    $CMUESTRA       = $row->CMUESTRA;
                    $DPRODUCTO      = $row->DPRODUCTO;
                    $DTEMPERATURA   = $row->DTEMPERATURA;
                    $DLCLAB         = $row->DLCLAB;
                    $OBSERVACION    = $row->OBSERVACION;
                    $SOBSERV        = $row->SOBSERV;
                    $ACNAC          = $row->ACNAC;
                    $nporcion       = $row->nporcion;


                    /*if($pos > 0){
                        $html .= '<hr style="page-break-before: always;" size="0"/>
                        <table width="100%" align="center">
                        <tr>
                            <td colspan="3" style="font-size:1.1em;" align="center">
                                <b>INFORME DE ENSAYO BORRADOR </b>
                            </td>
                        </tr>
                        </table>';                      
                    }else{
                        $html .= '<hr size="0"/>
                        <table width="100%" align="center">
                        <tr>
                            <td colspan="3" style="font-size:1.1em;" align="center">
                                <b>INFORME DE ENSAYO BORRADOR </b>
                            </td>
                        </tr>
                        </table>';
                    }*/
                    
                    if($pos > 0){
                        $html .= '<hr style="page-break-before: always;" size="0" noshade="noshade" />';                          
                    }  
                    
                    $parametros = array(
                        '@cinternoordenservicio'    => $cinternoordenservicio,
                        '@cmuestra'                 => $CMUESTRA,
                    ); 
                                     
                    $html .= '   <div id="header">
                                <table width="100%" align="center">
                                <tr>
                                    <td width="25%" align="center">                            
                                        <img src="'.public_url_ftp().'Imagenes/formatos/2/logoFSC.jpg" width="170" height="80" />  
                                    </td>
                                    <td width="50%" align="center" style="font-size:0.85em;">';                        
                                        if($ACNAC == "1"){
                                        $html .= '<ul class="list-unstyled">
                                            <li>LABORATORIO DE ENSAYO ACREDITADO POR EL</li>
                                            <li>ORGANISMO PERUANO DE ACREDITACIÓN INACAL - DA</li>
                                            <li>CON EL REGISTRO N° LE-073</li>
                                        </ul>';
                                        }                    
                                        $html .= '</td>
                                    <td width="25%" align="center">';                        
                                        if($ACNAC == "1"){
                                            $html .= '<img src="'.public_url_ftp().'Imagenes/formatos/2/inacal.jpg" width="150" height="80" /> ';
                                        }                            
                                    $html .= '</td>
                                </tr>
                                <tr>
                                    <td colspan="3"  style="height:40px; font-size:1.1em;" align="center">
                                        <b>INFORME DE ENSAYO N° '.$NROINFORME.' </b>
                                    </td>
                                </tr>
                                </table>
                            </div>';

                    $html .= '<div id="main">
                   
            <table id="caratula" width="100%" align="center" cellspacing="0" cellpadding="2" >                       
                    
                <tr>
                    <td align="left">
                        <b>Nombre del Cliente</b>   
                    </td>
                    <td align="left" colspan="2">
                        : '.$CLIENTE.'   
                    </td>
                </tr>
                <tr style="vertical-align:top">
                    <td align="left">
                        <b>Dirección del Cliente</b>  
                    </td>  
                    <td align="left" colspan="2">
                        : '.$DIRECCION.'   
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <b>N° Orden de Trabajo</b>   
                    </td>
                    <td align="left" colspan="2">
                        : '.$NROORDEN.'   
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <b>Procedencia de la Muestra</b>  
                    </td>
                    <td align="left" colspan="2">
                        : '.$PROCEDENCIA.'   
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <b>Fecha de Muestreo</b>    
                    </td>
                    <td align="left" colspan="2">
                        : '.$FMUESTRA.'   
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <b>Fecha de Recepción</b>    
                    </td>
                    <td align="left" colspan="2">
                        : '.$FRECEPCION.'   
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <b>Fecha de Análisis</b>    
                    </td>
                    <td align="left" colspan="2">
                        : '.$FANALISIS.'  
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <b>Lugar de Muestreo</b>   
                    </td>
                    <td align="left" colspan="2">
                        : '.$LUGARMUESTRA.'  
                    </td>
                </tr>
                <tr style="vertical-align:top">
                    <td width="27%" align="left">
                        <b>Muestra / Descripción</b>    
                    </td>
                    <td width="7%" align="left">
                        : '.$CMUESTRA.'   
                    </td>
                    <td width="66%" align="left">
                        '.$DPRODUCTO.'   
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <b>Temperatura de Recepción</b>    
                    </td>
                    <td align="left" colspan="2">
                        : '.$DTEMPERATURA.'   
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="3">
                    &nbsp;   
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="3" style="height:30px; border-top:solid 3px #000000">
                        <b><u>RESULTADOS DE ENSAYO</u></b>   
                    </td>
                </tr>
                
            </table>';

            ///RESULTADOS MICROBIOLOGIA
             $resmicro = $this->minformes->getinfxmuestras_resmicro($parametros);
                if($resmicro != ''){
                $html .= '<table id="tableborder">
                <thead>
                <tr>
                    <th> <b>Ensayo</b> </th>
                    <th> <b>Unidades</b> </th>
                    <th> <b>Via</b> </th>
                    <th> <b>Resultado</b> </th>
                </tr>
                </thead>
                <tbody>';
                $DENSAYOmicro = '';
                foreach($resmicro as $rowmicro){
                    $DENSAYO = $rowmicro->DENSAYO;
                    $UNIDADMEDIDA = $rowmicro->UNIDADMEDIDA;
                    $VIA = $rowmicro->VIA;
                    $RESULT_FINAL = (strlen($rowmicro->EXP10RES) > 0) ? $rowmicro->RESULTADO : $rowmicro->RESULT_FINAL;
                    $EXP10RES = $rowmicro->EXP10RES;
                    $html .= '<tr>';
                    if ($DENSAYO == $DENSAYOmicro){
                       $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                    }else{
                       $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                    }
                    
                    $html .= '<td width="130px" align="center">'.$UNIDADMEDIDA.'</td>
                        <td width="50px" align="center">'.$VIA.'</td>
                        <td width="130px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'<sup>'.$EXP10RES.'</sup></td>
                    </tr>';
                    $DENSAYOmicro = $DENSAYO;
                }
                $html .= '</tbody></table><br>'; 
            }
            ///RESULTADOS FISICOQUIMICO///
             $resfq = $this->minformes->getinfxmuestras_resfq($parametros);
             if ($resfq != ''){
                 $html .= '<table id="tableborder">
                 <thead>
                 <tr>
                     <th> <b>Ensayo</b> </th>
                     <th> <b>Unidades</b> </th>
                     <th> <b>Via</b> </th>
                     <th> <b>Resultado</b> </th>
                 </tr>
                 </thead>
                 <tbody>';
                 $DENSAYOfq = '';
                 foreach($resfq as $rowfq){
                     $DENSAYO = $rowfq->DENSAYO;
                     $UNIDADMEDIDA = $rowfq->UNIDADMEDIDA;
                     $VIA = $rowfq->VIA;
                     $RESULT_FINAL = $rowfq->RESULT_FINAL;
                     $EXP10RES = $rowfq->EXP10RES;

                     $html .= '<tr>';
                     if ($DENSAYO == $DENSAYOfq){
                        $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                     }else{
                        $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                     }

                     $html .= '<td width="130px" align="center">'.$UNIDADMEDIDA.'</td>
                         <td width="50px" align="center">'.$VIA.'</td>
                         <td width="130px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'<sup>'.$EXP10RES.'</sup></td>
                     </tr>';
                     $DENSAYOfq = $DENSAYO;
                 }
                 $html .= '</tbody></table><br>';
             }
            ///RESULTADOS INSTRUMENTAL TERCERO///
              $resins = $this->minformes->getinfxmuestras_resinstru($parametros);
              if ($resins){
                  $html .= '<table id="tableborder">
                  <thead>
                  <tr>
                      <th> <b>Ensayo</b> </th>
                      <th> <b>Unidades</b> </th>
                      <th> <b>Via</b> </th>
                      <th> <b>Resultado</b> </th>
                  </tr>
                  </thead>
                  <tbody>';
                  $DENSAYOins = '';
                  foreach($resins as $rowins){
                      $DENSAYO = $rowins->DENSAYO;
                      $UNIDADMEDIDA = $rowins->UNIDADMEDIDA;
                      $VIA = $rowins->VIA;
                      $RESULT_FINAL = $rowins->RESULT_FINAL;
                      $EXP10RES = $rowins->EXP10RES;

                      $html .= '<tr>';
                      if ($DENSAYO == $DENSAYOins){
                         $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                      }else{
                         $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                      }

                      $html .= '<td width="130px" align="center">'.$UNIDADMEDIDA.'</td>
                          <td width="50px" align="center">'.$VIA.'</td>
                          <td width="130px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'<sup>'.$EXP10RES.'</sup></td>
                      </tr>';
                      $DENSAYOins = $DENSAYO;
                  }
                  $html .= '</tbody></table><br>';
                  if ($cinternoordenservicio == "15510") {
                    $html .='<TABLE id="tableborder">
                    
                    <TR>	
                        <Th>Ensayo</Th>
                        <TH colspan=10 ROWSPAN=1>Resultados</TH>
                    </TR>
                    <TR>	
                        <TH ROWSPAN=5>
                            Evaluación de cierre doble
                            
                        </TH>
                            <TH ROWSPAN=2>Defecto Visual</TH>
                            <TH>Arrugas (expresado como planchado)</TH>
                            <TH>Altura</TH>
                            <TH>Espesor</TH>
                            <TH>Gancho de Tapa</TH>
                            <TH>Gancho de cuerpo</TH>
                            <TH>Traslape</TH>
                            <TH>Compacidad</TH>
                            <TH>Traslape</TH>
                            <TH>Penetracion gancho de cuerpo</TH>
                    </TR>
                    <TR>	
                        <TH>%</TH>
                        <TH>mm</TH>
                        <TH>mm</TH>
                        <TH>mm</TH>
                        <TH>mm</TH>
                        <TH>mm</TH>
                        <TH>%</TH>
                        <TH>%</TH>
                        <TH>%</TH>
                    </TR>
                    <TBODY>
                        <TR>
                            <TD>NO</TD>
                            <TD>> 90</TD>
                            <TD>2.87</TD>
                            <TD>1.25</TD>
                            <TD>1.90</TD>
                            <TD>2.06</TD>
                            <TD>1.34</TD>
                            <TD>84.00</TD>
                            <TD>62.00</TD>
                            <TD>85.96</TD>
                        </TR>
                        <TR>
                            <TD>NO</TD>
                            <TD>> 90</TD>
                            <TD>2.88</TD>
                            <TD>1.24</TD>
                            <TD>1.97</TD>
                            <TD>2.05</TD>
                            <TD>1.39</TD>
                            <TD>84.68</TD>
                            <TD>64.02</TD>
                            <TD>85.11</TD>
                        </TR>
                        <TR>
                            <TD>NO</TD>
                            <TD>> 90</TD>
                            <TD>2.81</TD>
                            <TD>1.22</TD>
                            <TD>1.86</TD>
                            <TD>1.93</TD>
                            <TD>1.23</TD>
                            <TD>86.07</TD>
                            <TD>58.55</TD>
                            <TD>82.24</TD>
                        </TR>
                    </TBODY>
                </TABLE>';
                  }

              }
            ///RESULTADOS SENSORIAL///
             $ressen = $this->minformes->getinfxmuestras_ressenso($parametros);
             if ($ressen){
                 $html .= '<table id="tableborder" style="page-break-inside: avoid; width:100%">
                 <thead>
                 <tr>
                     <th> <b>Ensayo</b> </th>
                     <th> <b>Via</b> </th>
                     <th> <b>Atributo</b> </th>
                     <th> <b>Resultado</b> </th>
                 </tr>
                 </thead>
                 <tbody>';
                 $DENSAYOsen = '';
                 foreach($ressen as $rowsen){
                     $DENSAYO = $rowsen->DENSAYO;
                     $DNOMBREESCALA = $rowsen->DNOMBREESCALA;
                     $NVIAUSADO = $rowsen->NVIAUSADO;
                     $RESULT_FINAL = $rowsen->RESULT_FINAL;
                     $html .= '<tr style="vertical-align:top">';
                     if ($DENSAYO == $DENSAYOsen){
                        $html .= '';
                     }else{
                        $html .= '<td rowspan="5" style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                     }

                     $html .= '<td  align="center">'.$NVIAUSADO.'</td>
                         <td >'.$DNOMBREESCALA.'</td>
                         <td >'.str_replace("<", "&lt;", $RESULT_FINAL).'</td>
                     </tr>';
                     $DENSAYOsen = $DENSAYO;
                 }
                 $html .= '</tbody></table><br>';
             }
             
            ///RESULTADOS CON ELEMENTOS///
             $resele = $this->minformes->getinfxmuestras_reselementos($parametros);
             if ($resele){
                $html .= '<table id="tableborder">
                <thead>
                <tr>
                    <th> <b>Ensayo</b> </th>
                    <th> <b>Via</b> </th>
                    <th> <b>Elemento</b> </th>
                    <th> <b>Unidades</b> </th>
                    <th> <b>Resultado</b> </th>
                </tr>
                </thead>
                <tbody>';
                $DENSAYOele = '';
                foreach($resele as $rowele){
                    $DENSAYO = $rowele->DENSAYO;
                    $ELEMENTO = $rowele->ELEMENTO;
                    $UNIDADMEDIDA = $rowele->UNIDADMEDIDA;
                    $VIA = $rowele->VIA;
                    $RESULT_FINAL = $rowele->RESULT_FINAL;
                    $html .= '<tr>';
                    if ($DENSAYO == $DENSAYOele){
                       $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                    }else{
                       $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                    }

                    $html .= '<td width="30px" align="center">'.$VIA.'</td>
                        <td>'.$ELEMENTO.'</td>
                        <td width="100px" align="center">'.$UNIDADMEDIDA.'</td>
                        <td width="130px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'</td>
                    </tr>';
                    $DENSAYOele = $DENSAYO;
                }
                $html .= '</tbody></table><br>';
             }
            ///RESULTADOS ESTERILIDAD BAJA///
             $parametrosEsterili = array(
                '@cinternoordenservicio' => $cinternoordenservicio,
                '@cmuestra' => $CMUESTRA,
                '@stipoacidez' => 'B'
             );
             $resesteb = $this->minformes->getinfxmuestras_resesteri($parametrosEsterili);
             if ($resesteb){
                $html .= '<table id="tableborder">
                <tr>
                    <td colspan="7"> Baja acidez (pH>4.6)</td>
                </tr>
                <tr>
                    <th width="30%" rowspan="2"> <b>Preincubación 35°C/14 días</b> </th>
                    <th width="5%" rowspan="2"> <b>pH</b> </th>
                    <th width="25%" colspan="2"> <b>Mesófilos (35°C)</b> </th>
                    <th width="15%" colspan="2"> <b>Termófilos (55°C)</b> </th>
                    <th width="25%" rowspan="2"> <b>Resultado</b> </th>
                </tr>
                <tr>
                    <th> <b>Caldo púrpura de bromocreasol 120h</b> </th>
                    <th> <b>Caldo carne cocida 120h</b> </th>
                    <th> <b>Caldo púrpura de bromocreasol 48h</b> </th>
                    <th> <b>Caldo carne cocida 72h</b> </th>
                </tr>';
                foreach($resesteb as $rowesteb){
                    $preincuba = $rowesteb->preincuba;
                    $dph = $rowesteb->dph;
                    $dbames35cp = $rowesteb->dbames35cp;
                    $dbames35cc = $rowesteb->dbames35cc;
                    $dbater55cp = $rowesteb->dbater55cp;
                    $dbater55cc = $rowesteb->dbater55cc;
                    $result_final = $rowesteb->result_final;
                    $html .= '<tr>
                        <td>'.$preincuba.'</td>
                        <td align="center">'.$dph.'</td>
                        <td align="center">'.$dbames35cp.'</td>
                        <td align="center">'.$dbames35cc.'</td>
                        <td align="center">'.$dbater55cp.'</td>
                        <td align="center">'.$dbater55cc.'</td>
                        <td align="center">'.$result_final.'</td>
                    </tr>';
                }
                $html .= '</table><br>';
             }
            ///RESULTADOS ESTERILIDAD ALTA///
             $parametrosEsterili = array(
                '@cinternoordenservicio' => $cinternoordenservicio,
                '@cmuestra' => $CMUESTRA,
                '@stipoacidez' => 'A'
             );
             $resestea = $this->minformes->getinfxmuestras_resesteri($parametrosEsterili);
             if ($resestea){
                $html .= '<table id="tableborder">
                <tr>
                    <td colspan="7"> Alta acidez ('.str_replace("<", "&lt;", "pH<4.6").')</td>
                </tr>
                <tr>
                    <th width="30%" rowspan="2"> <b>Preincubación 35°C/14 días</b> </th>
                    <th width="5%" rowspan="2"> <b>pH</b> </th>
                    <th width="25%" colspan="2"> <b>Mesófilos (35°C)</b> </th>
                    <th width="15%" colspan="2"> <b>Termófilos (55°C)</b> </th>
                    <th width="25%" rowspan="2"> <b>Resultado</b> </th>
                </tr>
                <tr>
                    <th> <b>Caldo ácido 96h</b> </th>
                    <th> <b>Caldo extracto de malta 96h</b> </th>
                    <th colspan="2"> <b>Caldo ácido 48h</b> </th>
                </tr>';
                foreach($resestea as $rowestea){
                    $preincuba = $rowestea->preincuba;
                    $dph = $rowestea->dph;
                    $dacmes30ca = $rowestea->dacmes30ca;
                    $dacmes30cm = $rowestea->dacmes30cm;
                    $dacter55ca = $rowestea->dacter55ca;
                    $result_final = $rowestea->result_final;
                    $html .= '<tr>
                        <td>'.$preincuba.'</td>
                        <td align="center">'.$dph.'</td>
                        <td align="center">'.$dacmes30ca.'</td>
                        <td align="center">'.$dacmes30cm.'</td>
                        <td align="center" colspan="2">'.$dacter55ca.'</td>
                        <td align="center">'.$result_final.'</td>
                    </tr>';
                }
                $html .= '</table><br>';
             }

            ///RESULTADOS ETIQUETADO NUTRICIONAL///
              $parametrosEtiquetado = array(
                  '@cinternoordenservicio' => $cinternoordenservicio,
                  '@cmuestra' => $CMUESTRA
              );
              $resetiqueta = $this->minformes->getinfxmuestras_resetiquetado($parametrosEtiquetado);
              if ($resetiqueta){
                $html .= '<table id="tableborder">
                  <tr>
                      <td colspan="7"> <b>INFORMACION NUTRICIONAL</b> </td>
                  </tr>
                  <tr>
                      <td width="40%" colspan="4"> Tamaño de Porción :</td>
                      <td colspan="3">'.$nporcion.'</td>
                  </tr>
                  <tr>
                      <th width="25%" colspan="3"> <b>Ensayo</b> </th>
                      <th width="15%"> <b>Unidad</b> </th>
                      <th width="20%"> <b>100g</b> </th>
                      <th width="20%"> <b>Porción</b> </th>
                      <th width="20%"> <b>% VDR(*)</b> </th>
                  </tr>';
                  foreach($resetiqueta as $rowetiqueta){
                      $DENSAYO = $rowetiqueta->DENSAYO;
                      $UNIDADMEDIDA = $rowetiqueta->UNIDADMEDIDA;
                      $DRESULTADO = $rowetiqueta->DRESULTADO;
                      $NPORCION = $rowetiqueta->NPORCION;
                      $VALORVR = $rowetiqueta->VALORVR;
                      $html .= '<tr>
                          <td colspan="3">'.$DENSAYO.'</td>
                          <td align="center">'.$UNIDADMEDIDA.'</td>
                          <td align="center">'.$DRESULTADO.'</td>
                          <td align="center">'.$NPORCION.'</td>
                          <td align="center">'.$VALORVR.'</td>
                      </tr>';
                  }
                  $html .= '<tr><td colspan="7">(*) % Valor Diario (VD) con base a una dieta de 2000 kcal u 8370 kJ.  según Codex Alimentarius FAO/OMS. Sus valores diarios pueden ser mayores o menores dependiendo de sus necesidades energéticas.</tr>
                             </table><br>';
              }
            /*NOTAS ADICIONALES*/
             $resNOTA1 = $this->minformes->getinfxmuestras_nota01($parametros);
                if ($resNOTA1){
                    foreach($resNOTA1 as $rowNOTA1){
                        $NOTA01        = $rowNOTA1->NOTA01;
                    }
                    
                    $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" > 
                                <tr>
                                    <td width="100%" align="left" style="font-size: 0.8em;">
                                        '.$DLCLAB.'   
                                    </td>
                                </tr>             
                                <tr>
                                    <td width="100%" align="center" style="font-size: 0.8em;">
                                        '.$NOTA01.'   
                                    </td>
                                </tr> 
                    </table> <br>';
                    
                }
                
                
                $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" >
                            <tr>
                                <td width="100%" align="center" colspan="3" style="height:30px; font-size: 0.8em;">
                                    <b><u>METODOS DE ENSAYO</u></b>   
                                </td>
                            </tr>
                </table>';
            
            /*METODOS DE ENSAYO*/    
             $resmetensa = $this->minformes->getmetodosensayos($parametros);
             if ($resmetensa){
                $html .= '<table id="tableborder">
                <thead>
                    <tr>
                        <th>
                            <b>Ensayo</b>   
                        </th>
                        <th>
                            <b>Norma o Referencia</b>    
                        </td>
                    </tr>
                    </thead>
                    <tbody>';
                    foreach($resmetensa as $rowmetensa){
                        $METDENSAYO = $rowmetensa->DENSAYO;
                        $METDNORMA = $rowmetensa->DNORMA;
                        $html .= '<tr>
                        <td width="180px">'.$METDENSAYO.'</td>
                            <td>'.$METDNORMA.'</td>
                        </tr>';
                    }
                    if ($cinternoordenservicio == "15510") {
                        $html .= 
                        '<tr>
                            <td width="180px">Evaluación de cierre doble </td>
                            <td>NCh 2701. Sección 5, excluyendo el parrafo 5.2.2 Of 2022.</td>
                        </tr>';
                    }
                $html .= '</tbody></table><br>';
             }  
            /* OBSERVACIONES */
             if($SOBSERV > 0){
                $html .= '<table id="tableborder" >
                  <tr>
                    <td width="100%" align="left" >
                        <b>Observaciones :</b>
                    </td>
                  </tr> 
                  <tr>
                    <td width="100%" align="center" >
                        <div style="text-align: justify;">'.$OBSERVACION.'</div>   
                    </td>
                  </tr> 
                </table> <br>';
             }else{
                $html .= '';
             }
            /* FECHA Y FIRMAS*/ 
             $resFECHAFIRMA = $this->minformes->getinfxmuestras_fechafirma($parametros);
             if ($resFECHAFIRMA ){
                 foreach($resFECHAFIRMA as $rowFECHAFIRMA){
                     $FECHA        = $rowFECHAFIRMA->FECHA;

                     $SFQ        = $rowFECHAFIRMA->SFQ;
                     $SMICRO        = $rowFECHAFIRMA->SMICRO;
                     $SINSTRU        = ($rowFECHAFIRMA->SINSTRU === '0' && $cinternoordenservicio=== '16313'|| $cinternoordenservicio=== '16457' || $cinternoordenservicio=== '16459' || $cinternoordenservicio=== '16071' ||   $cinternoordenservicio=== '16011'||   $cinternoordenservicio=== '16312' ||   $cinternoordenservicio=== '15537') ? '1' : $rowFECHAFIRMA->SINSTRU;

                     $FIRMAFQS        = $rowFECHAFIRMA->FIRMAFQS;
                     $NOMBREFQS        = $rowFECHAFIRMA->NOMBREFQS;
                     $CARGOFQS        = $rowFECHAFIRMA->CARGOFQS;
                     $CODIGOFQS        = $rowFECHAFIRMA->CODIGOFQS;

                     $FIRMAMICRO        = $rowFECHAFIRMA->FIRMAMICRO;
                     $NOMBREMICRO        = $rowFECHAFIRMA->NOMBREMICRO;
                     $CARGOMICRO        = $rowFECHAFIRMA->CARGOMICRO;
                     $CODIGOMICRO        = $rowFECHAFIRMA->CODIGOMICRO;

                     $FIRMAINSTRU        = $rowFECHAFIRMA->FIRMAINSTRU;
                     $NOMBREINSTRU        = $rowFECHAFIRMA->NOMBREINSTRU;
                     $CARGOINSTRU        = $rowFECHAFIRMA->CARGOINSTRU;
                     $CODIGOINSTRU        = $rowFECHAFIRMA->CODIGOINSTRU;
                 }
             }

             if($SFQ == '1' and $SMICRO == '1' and $SINSTRU == '1'){
                $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">                     
                        <tr>
                            <td width="100%" align="left" style="font-size: 0.8em;" colspan="2">
                            Lima, '.$FECHA.'   
                            </td>
                        </tr> 
                        <tr>
                            <td width="100%" align="center">
                            <img src="'.public_url_ftp().'Imagenes/firmas/FS000052.jpg" width="190" height="80" />    
                            </td>
                            <td width="100%" align="center">
                            <img src="'.public_url_ftp().'Imagenes/firmas/FS000046.jpg" width="190" height="80" />   
                            </td>
                        </tr>
                        <tr>
                            <td width="100%" align="center" style="font-size: 0.8em;">
                            <hr width=150>
                            '.$NOMBREFQS.'    
                            </td>
                            <td width="100%" align="center" style="font-size: 0.8em;">
                            <hr width=150>
                            '.$NOMBREMICRO.'  
                            </td>
                        </tr>
                        <tr>
                            <td width="100%" align="center" style="font-size: 0.8em;">
                            '.$CARGOFQS.'
                            </td>
                            <td width="100%" align="center" style="font-size: 0.8em;">
                            '.$CARGOMICRO.'
                            </td>
                        </tr>
                        <tr>
                            <td width="100%" align="center" style="font-size: 0.8em;">
                            '.$CODIGOFQS.'
                            </td>
                            <td width="100%" align="center" style="font-size: 0.8em;">
                            '.$CODIGOMICRO.'
                            </td>
                        </tr>

                        <tr>
                            <td width="100%" align="center" colspan="2">
                            <br>   
                            <br>  
                            </td>
                        </tr>

                        <tr>
                            <td width="100%" align="center" colspan="2">
                            <img src="'.public_url_ftp().'Imagenes/firmas/FS000120.jpg" width="190" height="80" />     
                            </td>
                        </tr>
                        <tr>
                            <td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                            <hr width=150>
                            '.$NOMBREINSTRU.'     
                            </td>
                        </tr>
                        <tr>
                            <td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                            '.$CARGOINSTRU.'    
                            </td>
                        </tr>
                        <tr>
                            <td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                            '.$CODIGOINSTRU.'    
                            </td>
                        </tr>
                    </tr>
                </table>';
             }else if($SFQ == '1' and $SMICRO == '0' and $SINSTRU == '1'){
                $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">              
                        <tr>
                            <td align="left" style="font-size: 0.8em;">
                                Lima, '.$FECHA.'   
                            </td>
                        </tr> 
                        <tr>';
                        if($SFQ == '1'){
                            $html .= '<td align="center">
                                <img src="'.public_url_ftp().'Imagenes/firmas/FS000052.jpg" width="190" height="80" />    
                                </td>';
                        }
                        if($SINSTRU == '1'){
                            $html .= '<td align="center">
                                <img src="'.public_url_ftp().'Imagenes/firmas/FS000120.jpg" width="190" height="80" />     
                                </td>';
                        }
                $html .= '</tr>
                        <tr>';
                        if($SFQ == '1'){
                            $html .= '<td align="center" style="font-size: 0.8em;">
                            <hr width=150>
                            '.$NOMBREFQS.'    
                            </td>';
                        }
                        if($SINSTRU == '1'){
                            $html .= '<td align="center" style="font-size: 0.8em;">
                            <hr width=150>
                            '.$NOMBREINSTRU.'     
                            </td>';
                        }
                $html .= '</tr>
                        <tr>';
                        if($SFQ == '1'){
                            $html .= '<td align="center" style="font-size: 0.8em;">
                            '.$CARGOFQS.'
                            </td>';
                        }
                        if($SINSTRU == '1'){
                            $html .= '<td align="center" style="font-size: 0.8em;">
                            '.$CARGOINSTRU.'    
                            </td>';
                        }
                $html .= '</tr>
                        <tr>';
                        if($SFQ == '1'){
                            $html .= '<td align="center" style="font-size: 0.8em;">
                            '.$CODIGOFQS.'
                            </td>';
                        }
                        if($SINSTRU == '1'){
                            $html .= '<td align="center" style="font-size: 0.8em;">
                            '.$CODIGOINSTRU.'    
                            </td>';
                        }
                $html .= '</tr>
                </table>';
             }else if($SFQ == '0' and $SMICRO == '1' and $SINSTRU == '1'){  
                $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">              
                        <tr>
                            <td align="left" style="font-size: 0.8em;">
                                Lima, '.$FECHA.'   
                            </td>
                        </tr> 
                        <tr>';
                        if($SMICRO == '1'){
                            $html .= '<td align="center">
                                <img src="'.public_url_ftp().'Imagenes/firmas/FS000046.jpg" width="190" height="80" />   
                                </td>';
                        }
                        if($SINSTRU == '1'){
                            $html .= '<td align="center" colspan="2">
                                <img src="'.public_url_ftp().'Imagenes/firmas/FS000120.jpg" width="190" height="80" />     
                                </td>';
                        }
                $html .= '</tr>
                        <tr>';
                        if($SMICRO == '1'){
                            $html .= '<td align="center" style="font-size: 0.8em;">
                            <hr width=150>
                            '.$NOMBREMICRO.'  
                            </td>';
                        }
                        if($SINSTRU == '1'){
                            $html .= '<td align="center" colspan="2" style="font-size: 0.8em;">
                            <hr width=150>
                            '.$NOMBREINSTRU.'     
                            </td>';
                        }
                $html .= '</tr>
                        <tr>';
                        if($SMICRO == '1'){
                            $html .= '<td align="center" style="font-size: 0.8em;">
                            '.$CARGOMICRO.'
                            </td>';
                        }
                        if($SINSTRU == '1'){
                            $html .= '<td align="center" colspan="2" style="font-size: 0.8em;">
                            '.$CARGOINSTRU.'    
                            </td>';
                        }
                $html .= '</tr>
                        <tr>';
                        if($SMICRO == '1'){
                            $html .= '<td width="50%" align="center" style="font-size: 0.8em;">
                            '.$CODIGOMICRO.'
                            </td>';
                        }
                        if($SINSTRU == '1'){
                            $html .= '<td width="50%" align="center" colspan="2" style="font-size: 0.8em;">
                            '.$CODIGOINSTRU.'    
                            </td>';
                        }
                $html .= '</tr>
                </table>';   
             }else if($SFQ == '1' and $SMICRO == '1' and $SINSTRU == '0'){
                $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">              
                        <tr>
                            <td width="100%" align="left" style="font-size: 0.8em;">
                                Lima, '.$FECHA.'   
                            </td>
                        </tr> 
                        <tr>';
                        if($SFQ == '1'){
                            $html .= '<td width="50%" align="center">
                                <img src="'.public_url_ftp().'Imagenes/firmas/FS000052.jpg" width="190" height="80" />    
                                </td>';
                        }
                        if($SMICRO == '1'){
                            $html .= '<td width="50%" align="center">
                                <img src="'.public_url_ftp().'Imagenes/firmas/FS000046.jpg" width="190" height="80" />   
                                </td>';
                        }
                $html .= '</tr>
                        <tr>';
                        if($SFQ == '1'){
                            $html .= '<td width="50%" align="center" style="font-size: 0.8em;">
                            <hr width=150>
                            '.$NOMBREFQS.'    
                            </td>';
                        }
                        if($SMICRO == '1'){
                            $html .= '<td width="50%" align="center" style="font-size: 0.8em;">
                            <hr width=150>
                            '.$NOMBREMICRO.'  
                            </td>';
                        }
                $html .= '</tr>
                        <tr>';
                        if($SFQ == '1'){
                            $html .= '<td width="50%" align="center" style="font-size: 0.8em;">
                            '.$CARGOFQS.'
                            </td>';
                        }
                        if($SMICRO == '1'){
                            $html .= '<td width="50%" align="center" style="font-size: 0.8em;">
                            '.$CARGOMICRO.'
                            </td>';
                        }
                $html .= '</tr>
                        <tr>';
                        if($SFQ == '1'){
                            $html .= '<td width="50%" align="center" style="font-size: 0.8em;">
                            '.$CODIGOFQS.'
                            </td>';
                        }
                        if($SMICRO == '1'){
                            $html .= '<td width="50%" align="center" style="font-size: 0.8em;">
                            '.$CODIGOMICRO.'
                            </td>';
                        }
                $html .= '</tr>
                </table>';
             }else{
                $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">              
                        <tr>
                            <td width="100%" align="left" style="font-size: 0.8em;">
                                Lima, '.$FECHA.'   
                            </td>
                        </tr> 
                        <tr>';
                        if($SFQ == '1'){
                            $html .= '<td width="100%" align="center">
                                <img src="'.public_url_ftp().'Imagenes/firmas/FS000052.jpg" width="190" height="80" />    
                                </td>';
                        }
                        if($SMICRO == '1'){
                            $html .= '<td width="100%" align="center">
                                <img src="'.public_url_ftp().'Imagenes/firmas/FS000046.jpg" width="190" height="80" />   
                                </td>';
                        }
                        if($SINSTRU == '1'  ){
                            $html .= '<td width="100%" align="center" colspan="2">
                                <img src="'.public_url_ftp().'Imagenes/firmas/FS000120.jpg" width="190" height="80" />     
                                </td>';
                        }
                        
                $html .= '</tr>
                        <tr>';
                        if($SFQ == '1'){
                            $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                            <hr width=150>
                            '.$NOMBREFQS.'    
                            </td>';
                        }
                        if($SMICRO == '1'){
                            $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                            <hr width=150>
                            '.$NOMBREMICRO.'  
                            </td>';
                        }
                        if($SINSTRU == '1'){
                            $html .= '<td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                            <hr width=150>
                            '.$NOMBREINSTRU.'     
                            </td>';
                        }
                        
                $html .= '</tr>
                        <tr>';
                        if($SFQ == '1'){
                            $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                            '.$CARGOFQS.'
                            </td>';
                        }
                        if($SMICRO == '1'){
                            $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                            '.$CARGOMICRO.'
                            </td>';
                        }
                        if($SINSTRU == '1'){
                            $html .= '<td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                            '.$CARGOINSTRU.'    
                            </td>';
                        }
                        
                $html .= '</tr>
                        <tr>';
                        if($SFQ == '1'){
                            $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                            '.$CODIGOFQS.'
                            </td>';
                        }
                        if($SMICRO == '1'){
                            $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                            '.$CODIGOMICRO.'
                            </td>';
                        }
                        if($SINSTRU == '1'){
                            $html .= '<td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                            '.$CODIGOINSTRU.'    
                            </td>';
                        }
                        
                $html .= '</tr>
                </table>';
             }  
             

                $html .= '</div>';
                                $pos++;
                        }}
                $html .= '</body>
                        </html>';
                $filename = 'IE '.substr($NROINFORME, 0, 9).' '.$CLIENTE.' '.$CMUESTRA;
                $this->pdfgenerator->generate($html, $filename, TRUE, 'A4', 'portrait');
                //echo $html;
       }catch(Exception $e){
        echo 'Excepción capturada: ',  $e->getMessage(), "\n";
       }
    }  
    public function pdfInfensayoMuestraSinLogo($cinternoordenservicio,$vcmuestra,$firma) {
        
        $this->load->library('pdfgenerator');

        $html = '<html>
                <head>
                    <title>Informe</title>
                    <style type="text/css">
                        @page{
                             margin: 0.3in 0.3in 0.3in 0.3in;
                        }
                        body{
                            font-family: Arial, Helvetica, sans-serif;
                            margin-top: 3.7cm;
                            margin-left: 1cm;
                            margin-right: 1cm;
                            margin-bottom: 3.5cm;
                        }
                        #header,
                        #footer,
                        #nroinf {
                            position: fixed;
                            left: 0;
                            right: 0;
                            color: #000000;
                            font-size: 0.9em;
                        }                        
                        #header {
                            top: 0;
                            height: 3.7cm;
                        }    
                        #nroinf {
                            top: 0cm;
                            height: 3.7cm;
                        }                     
                        #footer {
                            bottom: 0;
                            border-top: 0.1pt solid #aaa;
                            height: 3.5cm;
                        }                        
                        #header table,
                        #footer table,
                        #nroinf table {
                            width: 100%;
                            border-collapse: collapse;
                            border: none;
                        }                        
                        #header td,
                        #footer td,
                        #nroinf td {
                            padding: 0;
                        }
                        .page-number{
                            text-align: right;
                        }
                        .page-number:before{
                            content: "Pagina " counter(page);
                        }  
                        #salto hr{
                            page-break-before: always;
                            color: #ffffff;
                            margin: -2px;
                        }     
                        #caratula td{
                            font-size:0.8em;        
                        }
                        #tableborder{
                            border-collapse: collapse;
                            width: 100%;
                            font-size:0.8em;
                            border: 0.5pt solid black;
                        }
                        #tableborder td{
                            border: 0.5pt solid black;
                            padding-left: 5px;
                        }
                        #tableborder th{
                            text-align: center;
                            border: 0.5pt solid black;
                            background-color: #DBD7D7;
                        }
                        .list-unstyled{
                            padding-left: 0;
                            list-style: none;
                        }
                    </style>
                </head>
                <body>

                    <div id="header">
                        <table width="100%" align="center">
                        <tr>
                            <td width="25%" align="center">                            
                                <img src="'.public_url_ftp().'Imagenes/formatos/2/BLANCO.jpg" width="170" height="80" />
                            </td>
                            <td width="50%" align="center" style="font-size:0.85em;"> 
                                &nbsp;
                            </td>
                            <td width="25%" align="center">                        
                                &nbsp;
                            </td>
                        </tr>
                        </table>
                    </div>  

                    <div id="footer">
                        <table width="100%" style="font-size:0.8em; margin-top: 0.3cm; text-align: justify">
                        <tr>
                            <td width="100%" align="left" colspan="2">
                                Los resultados de los ensayos pertenecen sólo a las muestras ensayadas y no deben ser utilizados como una certificación de conformidad con normas del producto o como certificado del sistema de calidad de la entidad que lo produce. Queda prohibida la reproducción total o parcial de este informe, sin la autorización escrita de FS Certificaciones S.A.C.                            
                            </td>
                        </tr>
                        <tr>
                            <td width="100%" align="left" colspan="2"> &nbsp; </td>
                        </tr>
                        <tr>
                            <td width="50%" align="left"> 
                                FSC-F-LAB-011/V.03                           
                            </td>
                            <td width="50%" align="right" >
                                <div class="page-number"></div>
                            </td>
                        </tr>
                        <tr>
                            <td width="100%" align="center" colspan="2">
                                <ul class="list-unstyled">
                                    <li>&nbsp;</li>
                                    <li>&nbsp;</li>
                                    <li>&nbsp;</li>
                                </ul>
                            </td>
                        </tr>
                        </table>                    
                    </div>'; 
                   $parametroscabe = array(
                        '@cinternoordenservicio'         => $cinternoordenservicio,
                        '@cmuestra'       => $vcmuestra,
                    );
                    $res = $this->minformes->getinfxmuestras_caratula($parametroscabe);
                    $pos = 0;
                    if ($res){
                        foreach($res as $row){ 
                            $NROINFORME     = $row->NROINFORME;
                            $CLIENTE        = $row->CLIENTE;
                            $DIRECCION      = $row->DIRECCION;
                            $NROORDEN       = $row->NROORDEN;
                            $PROCEDENCIA    = $row->PROCEDENCIA;
                            $FMUESTRA       = $row->FMUESTRA;
                            $FRECEPCION     = $row->FRECEPCION;
                            $FANALISIS      = $row->FANALISIS;
                            $LUGARMUESTRA   = $row->LUGARMUESTRA;
                            $CMUESTRA       = $row->CMUESTRA;
                            $DPRODUCTO      = $row->DPRODUCTO;
                            $DTEMPERATURA   = $row->DTEMPERATURA;
                            $DLCLAB         = $row->DLCLAB;
                            $OBSERVACION    = $row->OBSERVACION;
                            $SOBSERV        = $row->SOBSERV;
                            $ACNAC          = $row->ACNAC;
                            $nporcion       = $row->nporcion;


                            /*if($pos > 0){
                                $html .= '<hr style="page-break-before: always;" size="0"/>
                                <table width="100%" align="center">
                                <tr>
                                    <td colspan="3" style="font-size:1.1em;" align="center">
                                        <b>INFORME DE ENSAYO BORRADOR </b>
                                    </td>
                                </tr>
                                </table>';                      
                            }else{
                                $html .= '<hr size="0"/>
                                <table width="100%" align="center">
                                <tr>
                                    <td colspan="3" style="font-size:1.1em;" align="center">
                                        <b>INFORME DE ENSAYO BORRADOR </b>
                                    </td>
                                </tr>
                                </table>';
                            }*/
                            
                            if($pos > 0){
                                $html .= '<hr style="page-break-before: always;" size="0" noshade="noshade"/>';                          
                                $html .= '<div style="height:5%"></div>';                          
                            }  
                            
                            $parametros = array(
                                '@cinternoordenservicio'    => $cinternoordenservicio,
                                '@cmuestra'                 => $CMUESTRA,
                            ); 
                                             
        $html .= '   <div id="header" >
                    <table width="100%" align="center">
                    <tr>
                        <td width="25%" align="center">                            
                        <img src="'.public_url_ftp().'Imagenes/formatos/2/BLANCO.jpg" width="170" height="80" />  
                        </td>
                        <td width="50%" align="center" style="font-size:0.85em;">';                        
                            if($ACNAC == "1"){
                            $html .= '<ul class="list-unstyled">
                            <li>&nbsp;</li>
                            <li>&nbsp;</li>
                            <li>&nbsp;</li>
                            </ul>';
                            }                    
                            $html .= '</td>
                        <td width="25%" align="center">';                        
                            if($ACNAC == "1"){
                                $html .= '<img src="'.public_url_ftp().'Imagenes/formatos/2/BLANCO.jpg" width="150" height="80" /> ';
                            }                            
                        $html .= '</td>
                    </tr>
                    <tr>
                        <td colspan="3" style="height:40px; font-size:1.1em;" align="center">
                            <b>INFORME DE ENSAYO N° '.$NROINFORME.' </b>
                        </td>
                    </tr>
                    </table>
                </div>';

        $html .= '<div id="main">
                           
                    <table id="caratula" width="100%" align="center" cellspacing="0" cellpadding="2" >                       
                            
                        <tr>
                            <td align="left">
                                <b>Nombre del Cliente</b>   
                            </td>
                            <td align="left" colspan="2">
                                : '.$CLIENTE.'   
                            </td>
                        </tr>
                        <tr style="vertical-align:top">
                            <td align="left">
                                <b>Dirección del Cliente</b>  
                            </td>  
                            <td align="left" colspan="2">
                                : '.$DIRECCION.'   
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <b>N° Orden de Trabajo</b>   
                            </td>
                            <td align="left" colspan="2">
                                : '.$NROORDEN.'   
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <b>Procedencia de la Muestra</b>  
                            </td>
                            <td align="left" colspan="2">
                                : '.$PROCEDENCIA.'   
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <b>Fecha de Muestreo</b>    
                            </td>
                            <td align="left" colspan="2">
                                : '.$FMUESTRA.'   
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <b>Fecha de Recepción</b>    
                            </td>
                            <td align="left" colspan="2">
                                : '.$FRECEPCION.'<   
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <b>Fecha de Análisis</b>    
                            </td>
                            <td align="left" colspan="2">
                                : '.$FANALISIS.'  
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <b>Lugar de Muestreo</b>   
                            </td>
                            <td align="left" colspan="2">
                                : '.$LUGARMUESTRA.'  
                            </td>
                        </tr>
                        <tr style="vertical-align:top">
                            <td width="27%" align="left">
                                <b>Muestra / Descripción</b>    
                            </td>
                            <td width="7%" align="left">
                                : '.$CMUESTRA.'   
                            </td>
                            <td width="66%" align="left">
                                '.$DPRODUCTO.'   
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <b>Temperatura de Recepción</b>    
                            </td>
                            <td align="left" colspan="2">
                                : '.$DTEMPERATURA.'   
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="3">
                            &nbsp;   
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="3" style="height:30px; border-top:solid 3px #000000">
                                <b><u>RESULTADOS DE ENSAYO</u></b>   
                            </td>
                        </tr>
                        
                    </table>';

                    ///RESULTADOS MICROBIOLOGIA//
                     $resmicro = $this->minformes->getinfxmuestras_resmicro($parametros);
                     if ($resmicro){
                         $html .= '<table id="tableborder">
                         <thead>
                         <tr>
                             <th> <b>Ensayo</b> </th>
                             <th> <b>Unidades</b> </th>
                             <th> <b>Via</b> </th>
                             <th> <b>Resultado</b> </th>
                         </tr>
                         </thead>
                         <tbody>';
                         $DENSAYOmicro = '';
                         foreach($resmicro as $rowmicro){
                             $DENSAYO = $rowmicro->DENSAYO;
                             $UNIDADMEDIDA = $rowmicro->UNIDADMEDIDA;
                             $VIA = $rowmicro->VIA;
                             $RESULT_FINAL = $rowmicro->RESULT_FINAL;
                             $EXP10RES = $rowmicro->EXP10RES;

                             $html .= '<tr>';
                             if ($DENSAYO == $DENSAYOmicro){
                                $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                             }else{
                                $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                             }
                             
                             $html .= '<td width="130px" align="center">'.$UNIDADMEDIDA.'</td>
                                 <td width="50px" align="center">'.$VIA.'</td>
                                 <td width="130px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'<sup>'.$EXP10RES.'</sup></td>
                             </tr>';
                             $DENSAYOmicro = $DENSAYO;
                         }
                         $html .= '</tbody></table><br>';
                     }  
                    ///RESULTADOS FISICOQUIMICO///
                     $resfq = $this->minformes->getinfxmuestras_resfq($parametros);
                     if ($resfq){
                         $html .= '<table id="tableborder">
                         <thead>
                         <tr>
                             <th> <b>Ensayo</b> </th>
                             <th> <b>Unidades</b> </th>
                             <th> <b>Via</b> </th>
                             <th> <b>Resultado</b> </th>
                         </tr>
                         </thead>
                         <tbody>';
                         $DENSAYOfq = '';
                         foreach($resfq as $rowfq){
                             $DENSAYO = $rowfq->DENSAYO;
                             $UNIDADMEDIDA = $rowfq->UNIDADMEDIDA;
                             $VIA = $rowfq->VIA;
                             $RESULT_FINAL = $rowfq->RESULT_FINAL;
                             $EXP10RES = $rowfq->EXP10RES;

                             $html .= '<tr>';
                             if ($DENSAYO == $DENSAYOfq){
                                $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                             }else{
                                $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                             }

                             $html .= '<td width="130px" align="center">'.$UNIDADMEDIDA.'</td>
                                 <td width="50px" align="center">'.$VIA.'</td>
                                 <td width="130px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'<sup>'.$EXP10RES.'</sup></td>
                             </tr>';
                             $DENSAYOfq = $DENSAYO;
                         }
                         $html .= '</tbody></table><br>';
                     }
                    ///RESULTADOS INSTRUMENTAL TERCERO///
                      $resins = $this->minformes->getinfxmuestras_resinstru($parametros);
                      if ($resins){
                          $html .= '<table id="tableborder">
                          <thead>
                          <tr>
                              <th> <b>Ensayo</b> </th>
                              <th> <b>Unidades</b> </th>
                              <th> <b>Via</b> </th>
                              <th> <b>Resultado</b> </th>
                          </tr>
                          </thead>
                          <tbody>';
                          $DENSAYOins = '';
                          foreach($resins as $rowins){
                              $DENSAYO = $rowins->DENSAYO;
                              $UNIDADMEDIDA = $rowins->UNIDADMEDIDA;
                              $VIA = $rowins->VIA;
                              $RESULT_FINAL = $rowins->RESULT_FINAL;
                              $EXP10RES = $rowins->EXP10RES;
 
                              $html .= '<tr>';
                              if ($DENSAYO == $DENSAYOins){
                                 $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                              }else{
                                 $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                              }
 
                              $html .= '<td width="130px" align="center">'.$UNIDADMEDIDA.'</td>
                                  <td width="50px" align="center">'.$VIA.'</td>
                                  <td width="130px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'<sup>'.$EXP10RES.'</sup></td>
                              </tr>';
                              $DENSAYOins = $DENSAYO;
                          }
                          $html .= '</tbody></table><br>';
                      }
                    ///RESULTADOS SENSORIAL///
                     $ressen = $this->minformes->getinfxmuestras_ressenso($parametros);
                     if ($ressen){
                         $html .= '<table id="tableborder" style="page-break-inside: avoid;">
                         <thead>
                         <tr>
                             <th> <b>Ensayo</b> </th>
                             <th> <b>Via</b> </th>
                             <th> <b>Atributo</b> </th>
                             <th> <b>Resultado</b> </th>
                         </tr>
                         </thead>
                         <tbody>';
                         $DENSAYOsen = '';
                         foreach($ressen as $rowsen){
                             $DENSAYO = $rowsen->DENSAYO;
                             $DNOMBREESCALA = $rowsen->DNOMBREESCALA;
                             $NVIAUSADO = $rowsen->NVIAUSADO;
                             $RESULT_FINAL = $rowsen->RESULT_FINAL;
                             $html .= '<tr style="vertical-align:top">';
                             if ($DENSAYO == $DENSAYOsen){
                                $html .= '';
                             }else{
                                $html .= '<td rowspan="5" style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                             }

                             $html .= '<td width="30px" align="center">'.$NVIAUSADO.'</td>
                                 <td width="100px">'.$DNOMBREESCALA.'</td>
                                 <td width="210px">'.str_replace("<", "&lt;", $RESULT_FINAL).'</td>
                             </tr>';
                             $DENSAYOsen = $DENSAYO;
                         }
                         $html .= '</tbody></table><br>';
                     }
                     
                    ///RESULTADOS CON ELEMENTOS///
                     $resele = $this->minformes->getinfxmuestras_reselementos($parametros);
                     if ($resele){
                        $html .= '<table id="tableborder">
                        <thead>
                        <tr>
                            <th> <b>Ensayo</b> </th>
                            <th> <b>Via</b> </th>
                            <th> <b>Elemento</b> </th>
                            <th> <b>Unidades</b> </th>
                            <th> <b>Resultado</b> </th>
                        </tr>
                        </thead>
                        <tbody>';
                        $DENSAYOele = '';
                        foreach($resele as $rowele){
                            $DENSAYO = $rowele->DENSAYO;
                            $ELEMENTO = $rowele->ELEMENTO;
                            $UNIDADMEDIDA = $rowele->UNIDADMEDIDA;
                            $VIA = $rowele->VIA;
                            $RESULT_FINAL = $rowele->RESULT_FINAL;
                            $html .= '<tr>';
                            if ($DENSAYO == $DENSAYOele){
                               $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                            }else{
                               $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                            }

                            $html .= '<td width="30px" align="center">'.$VIA.'</td>
                                <td>'.$ELEMENTO.'</td>
                                <td width="100px" align="center">'.$UNIDADMEDIDA.'</td>
                                <td width="130px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'</td>
                            </tr>';
                            $DENSAYOele = $DENSAYO;
                        }
                        $html .= '</tbody></table><br>';
                     }
                    ///RESULTADOS ESTERILIDAD BAJA///
                     $parametrosEsterili = array(
                        '@cinternoordenservicio' => $cinternoordenservicio,
                        '@cmuestra' => $CMUESTRA,
                        '@stipoacidez' => 'B'
                     );
                     $resesteb = $this->minformes->getinfxmuestras_resesteri($parametrosEsterili);
                     if ($resesteb){
                        $html .= '<table id="tableborder">
                        <tr>
                            <td colspan="7"> Baja acidez (pH>4.6)</td>
                        </tr>
                        <tr>
                            <th width="30%" rowspan="2"> <b>Preincubación 35°C/14 días</b> </th>
                            <th width="5%" rowspan="2"> <b>pH</b> </th>
                            <th width="25%" colspan="2"> <b>Mesófilos (35°C)</b> </th>
                            <th width="15%" colspan="2"> <b>Termófilos (55°C)</b> </th>
                            <th width="25%" rowspan="2"> <b>Resultado</b> </th>
                        </tr>
                        <tr>
                            <th> <b>Caldo púrpura de bromocreasol 120h</b> </th>
                            <th> <b>Caldo carne cocida 120h</b> </th>
                            <th> <b>Caldo púrpura de bromocreasol 48h</b> </th>
                            <th> <b>Caldo carne cocida 72h</b> </th>
                        </tr>';
                        foreach($resesteb as $rowesteb){
                            $preincuba = $rowesteb->preincuba;
                            $dph = $rowesteb->dph;
                            $dbames35cp = $rowesteb->dbames35cp;
                            $dbames35cc = $rowesteb->dbames35cc;
                            $dbater55cp = $rowesteb->dbater55cp;
                            $dbater55cc = $rowesteb->dbater55cc;
                            $result_final = $rowesteb->result_final;
                            $html .= '<tr>
                                <td>'.$preincuba.'</td>
                                <td align="center">'.$dph.'</td>
                                <td align="center">'.$dbames35cp.'</td>
                                <td align="center">'.$dbames35cc.'</td>
                                <td align="center">'.$dbater55cp.'</td>
                                <td align="center">'.$dbater55cc.'</td>
                                <td align="center">'.$result_final.'</td>
                            </tr>';
                        }
                        $html .= '</table><br>';
                     }
                    ///RESULTADOS ESTERILIDAD ALTA///
                     $parametrosEsterili = array(
                        '@cinternoordenservicio' => $cinternoordenservicio,
                        '@cmuestra' => $CMUESTRA,
                        '@stipoacidez' => 'A'
                     );
                     $resestea = $this->minformes->getinfxmuestras_resesteri($parametrosEsterili);
                     if ($resestea){
                        $html .= '<table id="tableborder">
                        <tr>
                            <td colspan="7"> Alta acidez ('.str_replace("<", "&lt;", "pH<4.6").')</td>
                        </tr>
                        <tr>
                            <th width="30%" rowspan="2"> <b>Preincubación 35°C/14 días</b> </th>
                            <th width="5%" rowspan="2"> <b>pH</b> </th>
                            <th width="25%" colspan="2"> <b>Mesófilos (35°C)</b> </th>
                            <th width="15%" colspan="2"> <b>Termófilos (55°C)</b> </th>
                            <th width="25%" rowspan="2"> <b>Resultado</b> </th>
                        </tr>
                        <tr>
                            <th> <b>Caldo ácido 96h</b> </th>
                            <th> <b>Caldo extracto de malta 96h</b> </th>
                            <th colspan="2"> <b>Caldo ácido 48h</b> </th>
                        </tr>';
                        foreach($resestea as $rowestea){
                            $preincuba = $rowestea->preincuba;
                            $dph = $rowestea->dph;
                            $dacmes30ca = $rowestea->dacmes30ca;
                            $dacmes30cm = $rowestea->dacmes30cm;
                            $dacter55ca = $rowestea->dacter55ca;
                            $result_final = $rowestea->result_final;
                            $html .= '<tr>
                                <td>'.$preincuba.'</td>
                                <td align="center">'.$dph.'</td>
                                <td align="center">'.$dacmes30ca.'</td>
                                <td align="center">'.$dacmes30cm.'</td>
                                <td align="center" colspan="2">'.$dacter55ca.'</td>
                                <td align="center">'.$result_final.'</td>
                            </tr>';
                        }
                        $html .= '</table><br>';
                     }

                    ///RESULTADOS ETIQUETADO NUTRICIONAL///
                      $parametrosEtiquetado = array(
                          '@cinternoordenservicio' => $cinternoordenservicio,
                          '@cmuestra' => $CMUESTRA
                      );
                      $resetiqueta = $this->minformes->getinfxmuestras_resetiquetado($parametrosEtiquetado);
                      if ($resetiqueta){
                        $html .= '<table id="tableborder">
                          <tr>
                              <td colspan="7"> <b>INFORMACION NUTRICIONAL</b> </td>
                          </tr>
                          <tr>
                              <td width="40%" colspan="4"> Tamaño de Porción :</td>
                              <td colspan="3">'.$nporcion.'</td>
                          </tr>
                          <tr>
                              <th width="25%" colspan="3"> <b>Ensayo</b> </th>
                              <th width="15%"> <b>Unidad</b> </th>
                              <th width="20%"> <b>100g</b> </th>
                              <th width="20%"> <b>Porción</b> </th>
                              <th width="20%"> <b>% VDR(*)</b> </th>
                          </tr>';
                          foreach($resetiqueta as $rowetiqueta){
                              $DENSAYO = $rowetiqueta->DENSAYO;
                              $UNIDADMEDIDA = $rowetiqueta->UNIDADMEDIDA;
                              $DRESULTADO = $rowetiqueta->DRESULTADO;
                              $NPORCION = $rowetiqueta->NPORCION;
                              $VALORVR = $rowetiqueta->VALORVR;
                              $html .= '<tr>
                                  <td colspan="3">'.$DENSAYO.'</td>
                                  <td align="center">'.$UNIDADMEDIDA.'</td>
                                  <td align="center">'.$DRESULTADO.'</td>
                                  <td align="center">'.$NPORCION.'</td>
                                  <td align="center">'.$VALORVR.'</td>
                              </tr>';
                          }
                          $html .= '<tr><td colspan="7">(*) % Valor Diario (VD) con base a una dieta de 2000 kcal u 8370 kJ.  según Codex Alimentarius FAO/OMS. Sus valores diarios pueden ser mayores o menores dependiendo de sus necesidades energéticas.</tr>
                                     </table><br>';
                      }
                    /*NOTAS ADICIONALES*/
                     $resNOTA1 = $this->minformes->getinfxmuestras_nota01($parametros);
                     //var_dump($resNOTA1);
                        if ($resNOTA1){
                            foreach($resNOTA1 as $rowNOTA1){
                                $NOTA01        = $rowNOTA1->NOTA01;
                            }
                        }
                        $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" >                        
                                    <tr>
                                        <td width="100%" align="left" style="font-size: 0.8em;">
                                            '.$DLCLAB.'   
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td width="100%" align="center" style="font-size: 0.8em;">
                                            '.$NOTA01.'   
                                        </td>
                                    </tr> 
                        </table> <br>';
                        
                        $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" >
                                    <tr>
                                        <td width="100%" align="center" colspan="3" style="height:30px; font-size: 0.8em;">
                                            <b><u>METODOS DE ENSAYO</u></b>   
                                        </td>
                                    </tr>
                        </table>';
                    
                    /*METODOS DE ENSAYO*/    
                     $resmetensa = $this->minformes->getmetodosensayos($parametros);
                     if ($resmetensa){
                        $html .= '<table id="tableborder">
                        <thead>
                            <tr>
                                <th>
                                    <b>Ensayo</b>   
                                </th>
                                <th>
                                    <b>Norma o Referencia</b>    
                                </td>
                            </tr>
                            </thead>
                            <tbody>';
                            foreach($resmetensa as $rowmetensa){
                                $METDENSAYO = $rowmetensa->DENSAYO;
                                $METDNORMA = $rowmetensa->DNORMA;
                                $html .= '<tr>
                                <td width="180px">'.$METDENSAYO.'</td>
                                    <td>'.$METDNORMA.'</td>
                                </tr>';
                            }
                        $html .= '</tbody></table><br>';
                     }  
                    /* OBSERVACIONES */
                     if($SOBSERV > 0){
                        $html .= '<table id="tableborder" >
                          <tr>
                            <td width="100%" align="left" >
                                <b>Observaciones :</b>
                            </td>
                          </tr> 
                          <tr>
                            <td width="100%" align="center" >
                                <div style="text-align: justify;">'.$OBSERVACION.'</div>   
                            </td>
                          </tr> 
                        </table> <br>';
                     }else{
                        $html .= '';
                     }
                    /* FECHA Y FIRMAS*/ 
                     $resFECHAFIRMA = $this->minformes->getinfxmuestras_fechafirma($parametros);
                     if ($resFECHAFIRMA){
                         foreach($resFECHAFIRMA as $rowFECHAFIRMA){
                             $FECHA        = $rowFECHAFIRMA->FECHA;

                             $SFQ        = $rowFECHAFIRMA->SFQ;
                             $SMICRO        = $rowFECHAFIRMA->SMICRO;
                             $SINSTRU        = $rowFECHAFIRMA->SINSTRU;

                             $FIRMAFQS        = $rowFECHAFIRMA->FIRMAFQS;
                             $NOMBREFQS        = $rowFECHAFIRMA->NOMBREFQS;
                             $CARGOFQS        = $rowFECHAFIRMA->CARGOFQS;
                             $CODIGOFQS        = $rowFECHAFIRMA->CODIGOFQS;

                             $FIRMAMICRO        = $rowFECHAFIRMA->FIRMAMICRO;
                             $NOMBREMICRO        = $rowFECHAFIRMA->NOMBREMICRO;
                             $CARGOMICRO        = $rowFECHAFIRMA->CARGOMICRO;
                             $CODIGOMICRO        = $rowFECHAFIRMA->CODIGOMICRO;

                             $FIRMAINSTRU        = $rowFECHAFIRMA->FIRMAINSTRU;
                             $NOMBREINSTRU        = $rowFECHAFIRMA->NOMBREINSTRU;
                             $CARGOINSTRU        = $rowFECHAFIRMA->CARGOINSTRU;
                             $CODIGOINSTRU        = $rowFECHAFIRMA->CODIGOINSTRU;
                         }
                     }

                     
                     if ($firma == 'N'){
                        if($SFQ == '1' and $SMICRO == '1' and $SINSTRU == '1'){
                            $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">                     
                                    <tr>
                                        <td width="100%" align="left" style="font-size: 0.8em;" colspan="2">
                                        Lima, '.$FECHA.'   
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td width="100%" align="center">
                                        <img src="'.public_url_ftp().'Imagenes/firmas/FIRMABLANCO.jpg" width="190" height="80" />    
                                        </td>
                                        <td width="100%" align="center">
                                        <img src="'.public_url_ftp().'Imagenes/firmas/FIRMABLANCO.jpg" width="190" height="80" />  
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%" align="center" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREFQS.'    
                                        </td>
                                        <td width="100%" align="center" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREMICRO.'  
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%" align="center" style="font-size: 0.8em;">
                                        '.$CARGOFQS.'
                                        </td>
                                        <td width="100%" align="center" style="font-size: 0.8em;">
                                        '.$CARGOMICRO.'
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%" align="center" style="font-size: 0.8em;">
                                        '.$CODIGOFQS.'
                                        </td>
                                        <td width="100%" align="center" style="font-size: 0.8em;">
                                        '.$CODIGOMICRO.'
                                        </td>
                                    </tr>

                                    <tr>
                                        <td width="100%" align="center" colspan="2">
                                        <br>   
                                        <br>  
                                        </td>
                                    </tr>

                                    <tr>
                                        <td width="100%" align="center" colspan="2">
                                        <img src="'.public_url_ftp().'Imagenes/firmas/FIRMABLANCO.jpg" width="190" height="80" />     
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREINSTRU.'     
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                        '.$CARGOINSTRU.'    
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                        '.$CODIGOINSTRU.'    
                                        </td>
                                    </tr>
                                </tr>
                            </table>';
                        }else if($SFQ == '1' and $SMICRO == '0' and $SINSTRU == '1'){
                            $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">              
                                    <tr>
                                        <td align="left" style="font-size: 0.8em;">
                                            Lima, '.$FECHA.'   
                                        </td>
                                    </tr> 
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td align="center">
                                            <img src="'.public_url_ftp().'Imagenes/firmas/FIRMABLANCO.jpg" width="190" height="80" />
                                            </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td align="center">
                                            <img src="'.public_url_ftp().'Imagenes/firmas/FIRMABLANCO.jpg" width="190" height="80" />
                                            </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td align="center" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREFQS.'    
                                        </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td align="center" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREINSTRU.'     
                                        </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td align="center" style="font-size: 0.8em;">
                                        '.$CARGOFQS.'
                                        </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td align="center" style="font-size: 0.8em;">
                                        '.$CARGOINSTRU.'    
                                        </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td align="center" style="font-size: 0.8em;">
                                        '.$CODIGOFQS.'
                                        </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td align="center" style="font-size: 0.8em;">
                                        '.$CODIGOINSTRU.'    
                                        </td>';
                                    }
                            $html .= '</tr>
                            </table>';
                        }else if($SFQ == '0' and $SMICRO == '1' and $SINSTRU == '1'){  
                            $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">              
                                    <tr>
                                        <td align="left" style="font-size: 0.8em;">
                                            Lima, '.$FECHA.'   
                                        </td>
                                    </tr> 
                                    <tr>';
                                    if($SMICRO == '1'){
                                        $html .= '<td align="center">
                                            <img src="'.public_url_ftp().'Imagenes/firmas/FIRMABLANCO.jpg" width="190" height="80" />
                                            </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td align="center" colspan="2">
                                            <img src="'.public_url_ftp().'Imagenes/firmas/FIRMABLANCO.jpg" width="190" height="80" />
                                            </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SMICRO == '1'){
                                        $html .= '<td align="center" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREMICRO.'  
                                        </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td align="center" colspan="2" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREINSTRU.'     
                                        </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SMICRO == '1'){
                                        $html .= '<td align="center" style="font-size: 0.8em;">
                                        '.$CARGOMICRO.'
                                        </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td align="center" colspan="2" style="font-size: 0.8em;">
                                        '.$CARGOINSTRU.'    
                                        </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SMICRO == '1'){
                                        $html .= '<td width="50%" align="center" style="font-size: 0.8em;">
                                        '.$CODIGOMICRO.'
                                        </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td width="50%" align="center" colspan="2" style="font-size: 0.8em;">
                                        '.$CODIGOINSTRU.'    
                                        </td>';
                                    }
                            $html .= '</tr>
                            </table>';   
                        }else if($SFQ == '1' and $SMICRO == '1' and $SINSTRU == '0'){
                            $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">              
                                    <tr>
                                        <td width="100%" align="left" style="font-size: 0.8em;">
                                            Lima, '.$FECHA.'   
                                        </td>
                                    </tr> 
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td width="50%" align="center">
                                            <img src="'.public_url_ftp().'Imagenes/firmas/FIRMABLANCO.jpg" width="190" height="80" />
                                            </td>';
                                    }
                                    if($SMICRO == '1'){
                                        $html .= '<td width="50%" align="center">
                                            <img src="'.public_url_ftp().'Imagenes/firmas/FIRMABLANCO.jpg" width="190" height="80" />
                                            </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td width="50%" align="center" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREFQS.'    
                                        </td>';
                                    }
                                    if($SMICRO == '1'){
                                        $html .= '<td width="50%" align="center" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREMICRO.'  
                                        </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td width="50%" align="center" style="font-size: 0.8em;">
                                        '.$CARGOFQS.'
                                        </td>';
                                    }
                                    if($SMICRO == '1'){
                                        $html .= '<td width="50%" align="center" style="font-size: 0.8em;">
                                        '.$CARGOMICRO.'
                                        </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td width="50%" align="center" style="font-size: 0.8em;">
                                        '.$CODIGOFQS.'
                                        </td>';
                                    }
                                    if($SMICRO == '1'){
                                        $html .= '<td width="50%" align="center" style="font-size: 0.8em;">
                                        '.$CODIGOMICRO.'
                                        </td>';
                                    }
                            $html .= '</tr>
                            </table>';
                        }else{
                            $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">              
                                    <tr>
                                        <td width="100%" align="left" style="font-size: 0.8em;">
                                            Lima, '.$FECHA.'   
                                        </td>
                                    </tr> 
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td width="100%" align="center">
                                            <img src="'.public_url_ftp().'Imagenes/firmas/FIRMABLANCO.jpg" width="190" height="80" />    
                                            </td>';
                                    }
                                    if($SMICRO == '1'){
                                        $html .= '<td width="100%" align="center">
                                            <img src="'.public_url_ftp().'Imagenes/firmas/FIRMABLANCO.jpg" width="190" height="80" />   
                                            </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td width="100%" align="center" colspan="2">
                                            <img src="'.public_url_ftp().'Imagenes/firmas/FIRMABLANCO.jpg" width="190" height="80" />     
                                            </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREFQS.'    
                                        </td>';
                                    }
                                    if($SMICRO == '1'){
                                        $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREMICRO.'  
                                        </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREINSTRU.'     
                                        </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                        '.$CARGOFQS.'
                                        </td>';
                                    }
                                    if($SMICRO == '1'){
                                        $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                        '.$CARGOMICRO.'
                                        </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                        '.$CARGOINSTRU.'    
                                        </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                        '.$CODIGOFQS.'
                                        </td>';
                                    }
                                    if($SMICRO == '1'){
                                        $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                        '.$CODIGOMICRO.'
                                        </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                        '.$CODIGOINSTRU.'    
                                        </td>';
                                    }
                            $html .= '</tr>
                            </table>';
                        }  
                     }else{                         
                        if($SFQ == '1' and $SMICRO == '1' and $SINSTRU == '1'){
                            $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">                     
                                    <tr>
                                        <td width="100%" align="left" style="font-size: 0.8em;" colspan="2">
                                        Lima, '.$FECHA.'   
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td width="100%" align="center">
                                        <img src="'.public_url_ftp().'Imagenes/firmas/FS000052.jpg" width="190" height="80" />    
                                        </td>
                                        <td width="100%" align="center">
                                        <img src="'.public_url_ftp().'Imagenes/firmas/FS000046.jpg" width="190" height="80" />   
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%" align="center" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREFQS.'    
                                        </td>
                                        <td width="100%" align="center" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREMICRO.'  
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%" align="center" style="font-size: 0.8em;">
                                        '.$CARGOFQS.'
                                        </td>
                                        <td width="100%" align="center" style="font-size: 0.8em;">
                                        '.$CARGOMICRO.'
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%" align="center" style="font-size: 0.8em;">
                                        '.$CODIGOFQS.'
                                        </td>
                                        <td width="100%" align="center" style="font-size: 0.8em;">
                                        '.$CODIGOMICRO.'
                                        </td>
                                    </tr>

                                    <tr>
                                        <td width="100%" align="center" colspan="2">
                                        <br>   
                                        <br>  
                                        </td>
                                    </tr>

                                    <tr>
                                        <td width="100%" align="center" colspan="2">
                                        <img src="'.public_url_ftp().'Imagenes/firmas/FS000120.jpg" width="190" height="80" />     
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREINSTRU.'     
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                        '.$CARGOINSTRU.'    
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                        '.$CODIGOINSTRU.'    
                                        </td>
                                    </tr>
                                </tr>
                            </table>';
                        }else if($SFQ == '1' and $SMICRO == '0' and $SINSTRU == '1'){
                            $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">              
                                    <tr>
                                        <td align="left" style="font-size: 0.8em;">
                                            Lima, '.$FECHA.'   
                                        </td>
                                    </tr> 
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td align="center">
                                            <img src="'.public_url_ftp().'Imagenes/firmas/FS000052.jpg" width="190" height="80" />    
                                            </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td align="center">
                                            <img src="'.public_url_ftp().'Imagenes/firmas/FS000120.jpg" width="190" height="80" />     
                                            </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td align="center" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREFQS.'    
                                        </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td align="center" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREINSTRU.'     
                                        </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td align="center" style="font-size: 0.8em;">
                                        '.$CARGOFQS.'
                                        </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td align="center" style="font-size: 0.8em;">
                                        '.$CARGOINSTRU.'    
                                        </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td align="center" style="font-size: 0.8em;">
                                        '.$CODIGOFQS.'
                                        </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td align="center" style="font-size: 0.8em;">
                                        '.$CODIGOINSTRU.'    
                                        </td>';
                                    }
                            $html .= '</tr>
                            </table>';
                        }else if($SFQ == '0' and $SMICRO == '1' and $SINSTRU == '1'){  
                            $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">              
                                    <tr>
                                        <td align="left" style="font-size: 0.8em;">
                                            Lima, '.$FECHA.'   
                                        </td>
                                    </tr> 
                                    <tr>';
                                    if($SMICRO == '1'){
                                        $html .= '<td align="center">
                                            <img src="'.public_url_ftp().'Imagenes/firmas/FS000046.jpg" width="190" height="80" />   
                                            </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td align="center" colspan="2">
                                            <img src="'.public_url_ftp().'Imagenes/firmas/FS000120.jpg" width="190" height="80" />     
                                            </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SMICRO == '1'){
                                        $html .= '<td align="center" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREMICRO.'  
                                        </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td align="center" colspan="2" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREINSTRU.'     
                                        </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SMICRO == '1'){
                                        $html .= '<td align="center" style="font-size: 0.8em;">
                                        '.$CARGOMICRO.'
                                        </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td align="center" colspan="2" style="font-size: 0.8em;">
                                        '.$CARGOINSTRU.'    
                                        </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SMICRO == '1'){
                                        $html .= '<td width="50%" align="center" style="font-size: 0.8em;">
                                        '.$CODIGOMICRO.'
                                        </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td width="50%" align="center" colspan="2" style="font-size: 0.8em;">
                                        '.$CODIGOINSTRU.'    
                                        </td>';
                                    }
                            $html .= '</tr>
                            </table>';   
                        }else if($SFQ == '1' and $SMICRO == '1' and $SINSTRU == '0'){
                            $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">              
                                    <tr>
                                        <td width="100%" align="left" style="font-size: 0.8em;">
                                            Lima, '.$FECHA.'   
                                        </td>
                                    </tr> 
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td width="50%" align="center">
                                            <img src="'.public_url_ftp().'Imagenes/firmas/FS000052.jpg" width="190" height="80" />    
                                            </td>';
                                    }
                                    if($SMICRO == '1'){
                                        $html .= '<td width="50%" align="center">
                                            <img src="'.public_url_ftp().'Imagenes/firmas/FS000046.jpg" width="190" height="80" />   
                                            </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td width="50%" align="center" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREFQS.'    
                                        </td>';
                                    }
                                    if($SMICRO == '1'){
                                        $html .= '<td width="50%" align="center" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREMICRO.'  
                                        </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td width="50%" align="center" style="font-size: 0.8em;">
                                        '.$CARGOFQS.'
                                        </td>';
                                    }
                                    if($SMICRO == '1'){
                                        $html .= '<td width="50%" align="center" style="font-size: 0.8em;">
                                        '.$CARGOMICRO.'
                                        </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td width="50%" align="center" style="font-size: 0.8em;">
                                        '.$CODIGOFQS.'
                                        </td>';
                                    }
                                    if($SMICRO == '1'){
                                        $html .= '<td width="50%" align="center" style="font-size: 0.8em;">
                                        '.$CODIGOMICRO.'
                                        </td>';
                                    }
                            $html .= '</tr>
                            </table>';
                        }else{
                            $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">              
                                    <tr>
                                        <td width="100%" align="left" style="font-size: 0.8em;">
                                            Lima, '.$FECHA.'   
                                        </td>
                                    </tr> 
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td width="100%" align="center">
                                            <img src="'.public_url_ftp().'Imagenes/firmas/FS000052.jpg" width="190" height="80" />    
                                            </td>';
                                    }
                                    if($SMICRO == '1'){
                                        $html .= '<td width="100%" align="center">
                                            <img src="'.public_url_ftp().'Imagenes/firmas/FS000046.jpg" width="190" height="80" />   
                                            </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td width="100%" align="center" colspan="2">
                                            <img src="'.public_url_ftp().'Imagenes/firmas/FS000120.jpg" width="190" height="80" />     
                                            </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREFQS.'    
                                        </td>';
                                    }
                                    if($SMICRO == '1'){
                                        $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREMICRO.'  
                                        </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREINSTRU.'     
                                        </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                        '.$CARGOFQS.'
                                        </td>';
                                    }
                                    if($SMICRO == '1'){
                                        $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                        '.$CARGOMICRO.'
                                        </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                        '.$CARGOINSTRU.'    
                                        </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                        '.$CODIGOFQS.'
                                        </td>';
                                    }
                                    if($SMICRO == '1'){
                                        $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                        '.$CODIGOMICRO.'
                                        </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                        '.$CODIGOINSTRU.'    
                                        </td>';
                                    }
                            $html .= '</tr>
                            </table>';
                        }  
                     }                  

        $html .= '</div>';
                        $pos++;
                   }}
        $html .= '</body>
                </html>';
		$filename = 'IE '.substr($NROINFORME, 0, 9).' '.$CLIENTE;
        $this->pdfgenerator->generate($html, $filename, TRUE, 'A4', 'portrait');
    }
	public function pdfInfensayoMuestraAll($cinternoordenservicio,$vcmuestra) { // recupera 
        $this->load->library('pdfgenerator');

        $html = '<html>
                <head>
                    <title>Informe</title>
                    <style type="text/css">
                        @page{
                             margin: 0.3in 0.3in 0.3in 0.3in;
                        }
                        body{
                            font-family: Arial, Helvetica, sans-serif;
                            margin-top: 3.7cm;
                            margin-left: 1cm;
                            margin-right: 1cm;
                            margin-bottom: 3.5cm;
                        }
                        #header,
                        #footer,
                        #nroinf {
                            position: fixed;
                            left: 0;
                            right: 0;
                            color: #000000;
                            font-size: 0.9em;
                        }                        
                        #header {
                            top: 0;
                            height: 3.7cm;
                        }    
                        #nroinf {
                            top: 0cm;
                            height: 3.7cm;
                        }                     
                        #footer {
                            bottom: 0;
                            border-top: 0.1pt solid #aaa;
                            height: 3.5cm;
                        }                        
                        #header table,
                        #footer table,
                        #nroinf table {
                            width: 100%;
                            border-collapse: collapse;
                            border: none;
                        }                        
                        #header td,
                        #footer td,
                        #nroinf td {
                            padding: 0;
                        }
                        .page-number{
                            text-align: right;
                        }
                        .page-number:before{
                            content: "Pagina " counter(page);
                        }  
                        #salto hr{
                            page-break-before: always;
                            color: #ffffff;
                            margin: -2px;
                        }     
                        #caratula td{
                            font-size:0.8em;        
                        }
                        #tableborder{
                            border-collapse: collapse;
                            width: 100%;
                            font-size:0.8em;
                            border: 0.5pt solid black;
                        }
                        #tableborder td{
                            border: 0.5pt solid black;
                            padding-left: 5px;
                        }
                        #tableborder th{
                            text-align: center;
                            border: 0.5pt solid black;
                            background-color: #DBD7D7;
                        }
                        .list-unstyled{
                            padding-left: 0;
                            list-style: none;
                        }
                    </style>
                </head>
                <body> 

                    <div id="footer">
                        <table width="100%" style="font-size:0.8em; margin-top: 0.3cm; text-align: justify">
                        <tr>
                            <td width="100%" align="left" colspan="2">
                                Los resultados de los ensayos pertenecen sólo a las muestras ensayadas y no deben ser utilizados como una certificación de conformidad con normas del producto o como certificado del sistema de calidad de la entidad que lo produce. Queda prohibida la reproducción total o parcial de este informe, sin la autorización escrita de FS Certificaciones S.A.C.                            
                            </td>
                        </tr>
                        <tr>
                            <td width="100%" align="left" colspan="2"> &nbsp; </td>
                        </tr>
                        <tr>
                            <td width="50%" align="left"> 
                                FSC-F-LAB-011/V.03                           
                            </td>
                            <td width="50%" align="right" >
                                <div class="page-number"></div>
                            </td>
                        </tr>
                        <tr>
                            <td width="100%" align="center" colspan="2">
                                <ul class="list-unstyled">
                                    <li>Jr. Monterrey N° 221 Of. 201-202, Urb. Chacarilla del Estanque. Santiago de Surco, Lima - Perú</li>
                                    <li>Teléfonos: (51-1) 480 0561 - Anexo: Lab 111</li>
                                    <li>www.fscertificaciones.com</li>
                                </ul>
                            </td>
                        </tr>
                        </table>                    
                    </div>'; 
                   $parametroscabe = array(
                        '@cinternoordenservicio'         => $cinternoordenservicio,
                        '@cmuestra'       => $vcmuestra,
                    );
                    $res = $this->minformes->getinfxmuestras_caratula($parametroscabe);
                    $pos = 0;
                    if ($res){
                        foreach($res as $row){ 
                            $NROINFORME     = $row->NROINFORME;
                            $CLIENTE        = $row->CLIENTE;
                            $DIRECCION      = $row->DIRECCION;
                            $NROORDEN       = $row->NROORDEN;
                            $PROCEDENCIA    = $row->PROCEDENCIA;
                            $FMUESTRA       = $row->FMUESTRA;
                            $FRECEPCION     = $row->FRECEPCION;
                            $FANALISIS      = $row->FANALISIS;
                            $LUGARMUESTRA   = $row->LUGARMUESTRA;
                            $CMUESTRA       = $row->CMUESTRA;
                            $DPRODUCTO      = $row->DPRODUCTO;
                            $DTEMPERATURA   = $row->DTEMPERATURA;
                            $DLCLAB         = $row->DLCLAB;
                            $OBSERVACION    = $row->OBSERVACION;
                            $SOBSERV        = $row->SOBSERV;
                            $ACNAC          = $row->ACNAC;
                            $nporcion       = $row->nporcion;

                           
                            if($pos > 0){
                                $html .= '<h1 style="page-break-before: always;" size="0" noshade="noshade" />';                          
                            }  
                            
                            $parametros = array(
                                '@cinternoordenservicio'    => $cinternoordenservicio,
                                '@cmuestra'                 => $CMUESTRA,
                            ); 

        $html .= '<table style="page-break-inside: avoid;"><thead><tr><td>VER</td></tr></thead> 
                  <tbody><tr><td>';
                   

                  $html .= '<div id="main">
                           
                  <table id="caratula" width="100%" align="center" cellspacing="0" cellpadding="2" >                       
                          
                      <tr>
                          <td align="left">
                              <b>Nombre del Cliente</b>   
                          </td>
                          <td align="left" colspan="2">
                              : '.$CLIENTE.'   
                          </td>
                      </tr>
                      <tr style="vertical-align:top">
                          <td align="left">
                              <b>Dirección del Cliente</b>  
                          </td>  
                          <td align="left" colspan="2">
                              : '.$DIRECCION.'   
                          </td>
                      </tr>
                      <tr>
                          <td align="left">
                              <b>N° Orden de Trabajo</b>   
                          </td>
                          <td align="left" colspan="2">
                              : '.$NROORDEN.'   
                          </td>
                      </tr>
                      <tr>
                          <td align="left">
                              <b>Procedencia de la Muestra</b>  
                          </td>
                          <td align="left" colspan="2">
                              : '.$PROCEDENCIA.'   
                          </td>
                      </tr>
                      <tr>
                          <td align="left">
                              <b>Fecha de Muestreo</b>    
                          </td>
                          <td align="left" colspan="2">
                              : '.$FMUESTRA.'   
                          </td>
                      </tr>
                      <tr>
                          <td align="left">
                              <b>Fecha de Recepción</b>    
                          </td>
                          <td align="left" colspan="2">
                              : '.$FRECEPCION.'<   
                          </td>
                      </tr>
                      <tr>
                          <td align="left">
                              <b>Fecha de Análisis</b>    
                          </td>
                          <td align="left" colspan="2">
                              : '.$FANALISIS.'  
                          </td>
                      </tr>
                      <tr>
                          <td align="left">
                              <b>Lugar de Muestreo</b>   
                          </td>
                          <td align="left" colspan="2">
                              : '.$LUGARMUESTRA.'  
                          </td>
                      </tr>
                      <tr style="vertical-align:top">
                          <td width="27%" align="left">
                              <b>Muestra / Descripción</b>    
                          </td>
                          <td width="7%" align="left">
                              : '.$CMUESTRA.'   
                          </td>
                          <td width="66%" align="left">
                              '.$DPRODUCTO.'   
                          </td>
                      </tr>
                      <tr>
                          <td align="left">
                              <b>Temperatura de Recepción</b>    
                          </td>
                          <td align="left" colspan="2">
                              : '.$DTEMPERATURA.'   
                          </td>
                      </tr>
                      <tr>
                          <td align="center" colspan="3">
                          &nbsp;   
                          </td>
                      </tr>
                      <tr>
                          <td align="center" colspan="3" style="height:30px; border-top:solid 3px #000000">
                              <b><u>RESULTADOS DE ENSAYO</u></b>   
                          </td>
                      </tr>
                      
                  </table>';

                  ///RESULTADOS MICROBIOLOGIA//
                   $resmicro = $this->minformes->getinfxmuestras_resmicro($parametros);
                   if ($resmicro){
                       $html .= '<table id="tableborder">
                       <thead>
                       <tr>
                           <th> <b>Ensayo</b> </th>
                           <th> <b>Unidades</b> </th>
                           <th> <b>Via</b> </th>
                           <th> <b>Resultado</b> </th>
                       </tr>
                       </thead>
                       <tbody>';
                       $DENSAYOmicro = '';
                       foreach($resmicro as $rowmicro){
                           $DENSAYO = $rowmicro->DENSAYO;
                           $UNIDADMEDIDA = $rowmicro->UNIDADMEDIDA;
                           $VIA = $rowmicro->VIA;
                           $RESULT_FINAL = $rowmicro->RESULT_FINAL;
                           $EXP10RES = $rowmicro->EXP10RES;

                           $html .= '<tr>';
                           if ($DENSAYO == $DENSAYOmicro){
                              $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                           }else{
                              $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                           }
                           
                           $html .= '<td width="130px" align="center">'.$UNIDADMEDIDA.'</td>
                               <td width="50px" align="center">'.$VIA.'</td>
                               <td width="130px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'<sup>'.$EXP10RES.'</sup></td>
                           </tr>';
                           $DENSAYOmicro = $DENSAYO;
                       }
                       $html .= '</tbody></table><br>';
                   }  
                  ///RESULTADOS FISICOQUIMICO///
                   $resfq = $this->minformes->getinfxmuestras_resfq($parametros);
                   if ($resfq){
                       $html .= '<table id="tableborder">
                       <thead>
                       <tr>
                           <th> <b>Ensayo</b> </th>
                           <th> <b>Unidades</b> </th>
                           <th> <b>Via</b> </th>
                           <th> <b>Resultado</b> </th>
                       </tr>
                       </thead>
                       <tbody>';
                       $DENSAYOfq = '';
                       foreach($resfq as $rowfq){
                           $DENSAYO = $rowfq->DENSAYO;
                           $UNIDADMEDIDA = $rowfq->UNIDADMEDIDA;
                           $VIA = $rowfq->VIA;
                           $RESULT_FINAL = $rowfq->RESULT_FINAL;
                           $EXP10RES = $rowfq->EXP10RES;

                           $html .= '<tr>';
                           if ($DENSAYO == $DENSAYOfq){
                              $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                           }else{
                              $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                           }

                           $html .= '<td width="130px" align="center">'.$UNIDADMEDIDA.'</td>
                               <td width="50px" align="center">'.$VIA.'</td>
                               <td width="130px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'<sup>'.$EXP10RES.'</sup></td>
                           </tr>';
                           $DENSAYOfq = $DENSAYO;
                       }
                       $html .= '</tbody></table><br>';
                   }
                  ///RESULTADOS INSTRUMENTAL TERCERO///
                    $resins = $this->minformes->getinfxmuestras_resinstru($parametros);
                    if ($resins){
                        $html .= '<table id="tableborder">
                        <thead>
                        <tr>
                            <th> <b>Ensayo</b> </th>
                            <th> <b>Unidades</b> </th>
                            <th> <b>Via</b> </th>
                            <th> <b>Resultado</b> </th>
                        </tr>
                        </thead>
                        <tbody>';
                        $DENSAYOins = '';
                        foreach($resins as $rowins){
                            $DENSAYO = $rowins->DENSAYO;
                            $UNIDADMEDIDA = $rowins->UNIDADMEDIDA;
                            $VIA = $rowins->VIA;
                            $RESULT_FINAL = $rowins->RESULT_FINAL;
                            $EXP10RES = $rowins->EXP10RES;

                            $html .= '<tr>';
                            if ($DENSAYO == $DENSAYOins){
                               $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                            }else{
                               $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                            }

                            $html .= '<td width="130px" align="center">'.$UNIDADMEDIDA.'</td>
                                <td width="50px" align="center">'.$VIA.'</td>
                                <td width="130px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'<sup>'.$EXP10RES.'</sup></td>
                            </tr>';
                            $DENSAYOins = $DENSAYO;
                        }
                        $html .= '</tbody></table><br>';
                    }
                  ///RESULTADOS SENSORIAL///
                   $ressen = $this->minformes->getinfxmuestras_ressenso($parametros);
                   if ($ressen){
                       $html .= '<table id="tableborder" style="page-break-inside: avoid;">
                       <thead>
                       <tr>
                           <th> <b>Ensayo</b> </th>
                           <th> <b>Via</b> </th>
                           <th> <b>Atributo</b> </th>
                           <th> <b>Resultado</b> </th>
                       </tr>
                       </thead>
                       <tbody>';
                       $DENSAYOsen = '';
                       foreach($ressen as $rowsen){
                           $DENSAYO = $rowsen->DENSAYO;
                           $DNOMBREESCALA = $rowsen->DNOMBREESCALA;
                           $NVIAUSADO = $rowsen->NVIAUSADO;
                           $RESULT_FINAL = $rowsen->RESULT_FINAL;
                           $html .= '<tr style="vertical-align:top">';
                           if ($DENSAYO == $DENSAYOsen){
                              $html .= '';
                           }else{
                              $html .= '<td rowspan="5" style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                           }

                           $html .= '<td width="30px" align="center">'.$NVIAUSADO.'</td>
                               <td width="100px">'.$DNOMBREESCALA.'</td>
                               <td width="210px">'.str_replace("<", "&lt;", $RESULT_FINAL).'</td>
                           </tr>';
                           $DENSAYOsen = $DENSAYO;
                       }
                       $html .= '</tbody></table><br>';
                   }
                   
                  ///RESULTADOS CON ELEMENTOS///
                   $resele = $this->minformes->getinfxmuestras_reselementos($parametros);
                   if ($resele){
                      $html .= '<table id="tableborder">
                      <thead>
                      <tr>
                          <th> <b>Ensayo</b> </th>
                          <th> <b>Via</b> </th>
                          <th> <b>Elemento</b> </th>
                          <th> <b>Unidades</b> </th>
                          <th> <b>Resultado</b> </th>
                      </tr>
                      </thead>
                      <tbody>';
                      $DENSAYOele = '';
                      foreach($resele as $rowele){
                          $DENSAYO = $rowele->DENSAYO;
                          $ELEMENTO = $rowele->ELEMENTO;
                          $UNIDADMEDIDA = $rowele->UNIDADMEDIDA;
                          $VIA = $rowele->VIA;
                          $RESULT_FINAL = $rowele->RESULT_FINAL;
                          $html .= '<tr>';
                          if ($DENSAYO == $DENSAYOele){
                             $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                          }else{
                             $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                          }

                          $html .= '<td width="30px" align="center">'.$VIA.'</td>
                              <td>'.$ELEMENTO.'</td>
                              <td width="100px" align="center">'.$UNIDADMEDIDA.'</td>
                              <td width="130px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'</td>
                          </tr>';
                          $DENSAYOele = $DENSAYO;
                      }
                      $html .= '</tbody></table><br>';
                   }
                  ///RESULTADOS ESTERILIDAD BAJA///
                   $parametrosEsterili = array(
                      '@cinternoordenservicio' => $cinternoordenservicio,
                      '@cmuestra' => $CMUESTRA,
                      '@stipoacidez' => 'B'
                   );
                   $resesteb = $this->minformes->getinfxmuestras_resesteri($parametrosEsterili);
                   if ($resesteb){
                      $html .= '<table id="tableborder">
                      <tr>
                          <td colspan="7"> Baja acidez (pH>4.6)</td>
                      </tr>
                      <tr>
                          <th width="30%" rowspan="2"> <b>Preincubación 35°C/14 días</b> </th>
                          <th width="5%" rowspan="2"> <b>pH</b> </th>
                          <th width="25%" colspan="2"> <b>Mesófilos (35°C)</b> </th>
                          <th width="15%" colspan="2"> <b>Termófilos (55°C)</b> </th>
                          <th width="25%" rowspan="2"> <b>Resultado</b> </th>
                      </tr>
                      <tr>
                          <th> <b>Caldo púrpura de bromocreasol 120h</b> </th>
                          <th> <b>Caldo carne cocida 120h</b> </th>
                          <th> <b>Caldo púrpura de bromocreasol 48h</b> </th>
                          <th> <b>Caldo carne cocida 72h</b> </th>
                      </tr>';
                      foreach($resesteb as $rowesteb){
                          $preincuba = $rowesteb->preincuba;
                          $dph = $rowesteb->dph;
                          $dbames35cp = $rowesteb->dbames35cp;
                          $dbames35cc = $rowesteb->dbames35cc;
                          $dbater55cp = $rowesteb->dbater55cp;
                          $dbater55cc = $rowesteb->dbater55cc;
                          $result_final = $rowesteb->result_final;
                          $html .= '<tr>
                              <td>'.$preincuba.'</td>
                              <td align="center">'.$dph.'</td>
                              <td align="center">'.$dbames35cp.'</td>
                              <td align="center">'.$dbames35cc.'</td>
                              <td align="center">'.$dbater55cp.'</td>
                              <td align="center">'.$dbater55cc.'</td>
                              <td align="center">'.$result_final.'</td>
                          </tr>';
                      }
                      $html .= '</table><br>';
                   }
                  ///RESULTADOS ESTERILIDAD ALTA///
                   $parametrosEsterili = array(
                      '@cinternoordenservicio' => $cinternoordenservicio,
                      '@cmuestra' => $CMUESTRA,
                      '@stipoacidez' => 'A'
                   );
                   $resestea = $this->minformes->getinfxmuestras_resesteri($parametrosEsterili);
                   if ($resestea){
                      $html .= '<table id="tableborder">
                      <tr>
                          <td colspan="7"> Alta acidez ('.str_replace("<", "&lt;", "pH<4.6").')</td>
                      </tr>
                      <tr>
                          <th width="30%" rowspan="2"> <b>Preincubación 35°C/14 días</b> </th>
                          <th width="5%" rowspan="2"> <b>pH</b> </th>
                          <th width="25%" colspan="2"> <b>Mesófilos (35°C)</b> </th>
                          <th width="15%" colspan="2"> <b>Termófilos (55°C)</b> </th>
                          <th width="25%" rowspan="2"> <b>Resultado</b> </th>
                      </tr>
                      <tr>
                          <th> <b>Caldo ácido 96h</b> </th>
                          <th> <b>Caldo extracto de malta 96h</b> </th>
                          <th colspan="2"> <b>Caldo ácido 48h</b> </th>
                      </tr>';
                      foreach($resestea as $rowestea){
                          $preincuba = $rowestea->preincuba;
                          $dph = $rowestea->dph;
                          $dacmes30ca = $rowestea->dacmes30ca;
                          $dacmes30cm = $rowestea->dacmes30cm;
                          $dacter55ca = $rowestea->dacter55ca;
                          $result_final = $rowestea->result_final;
                          $html .= '<tr>
                              <td>'.$preincuba.'</td>
                              <td align="center">'.$dph.'</td>
                              <td align="center">'.$dacmes30ca.'</td>
                              <td align="center">'.$dacmes30cm.'</td>
                              <td align="center" colspan="2">'.$dacter55ca.'</td>
                              <td align="center">'.$result_final.'</td>
                          </tr>';
                      }
                      $html .= '</table><br>';
                   }

                  ///RESULTADOS ETIQUETADO NUTRICIONAL///
                    $parametrosEtiquetado = array(
                        '@cinternoordenservicio' => $cinternoordenservicio,
                        '@cmuestra' => $CMUESTRA
                    );
                    $resetiqueta = $this->minformes->getinfxmuestras_resetiquetado($parametrosEtiquetado);
                    if ($resetiqueta){
                      $html .= '<table id="tableborder">
                        <tr>
                            <td colspan="7"> <b>INFORMACION NUTRICIONAL</b> </td>
                        </tr>
                        <tr>
                            <td width="40%" colspan="4"> Tamaño de Porción :</td>
                            <td colspan="3">'.$nporcion.'</td>
                        </tr>
                        <tr>
                            <th width="25%" colspan="3"> <b>Ensayo</b> </th>
                            <th width="15%"> <b>Unidad</b> </th>
                            <th width="20%"> <b>100g</b> </th>
                            <th width="20%"> <b>Porción</b> </th>
                            <th width="20%"> <b>% VDR(*)</b> </th>
                        </tr>';
                        foreach($resetiqueta as $rowetiqueta){
                            $DENSAYO = $rowetiqueta->DENSAYO;
                            $UNIDADMEDIDA = $rowetiqueta->UNIDADMEDIDA;
                            $DRESULTADO = $rowetiqueta->DRESULTADO;
                            $NPORCION = $rowetiqueta->NPORCION;
                            $VALORVR = $rowetiqueta->VALORVR;
                            $html .= '<tr>
                                <td colspan="3">'.$DENSAYO.'</td>
                                <td align="center">'.$UNIDADMEDIDA.'</td>
                                <td align="center">'.$DRESULTADO.'</td>
                                <td align="center">'.$NPORCION.'</td>
                                <td align="center">'.$VALORVR.'</td>
                            </tr>';
                        }
                        $html .= '<tr><td colspan="7">(*) % Valor Diario (VD) con base a una dieta de 2000 kcal u 8370 kJ.  según Codex Alimentarius FAO/OMS. Sus valores diarios pueden ser mayores o menores dependiendo de sus necesidades energéticas.</tr>
                                   </table><br>';
                    }
                  /*NOTAS ADICIONALES*/
                   $resNOTA1 = $this->minformes->getinfxmuestras_nota01($parametros);
                      if ($resNOTA1){
                          foreach($resNOTA1 as $rowNOTA1){
                              $NOTA01        = $rowNOTA1->NOTA01;
                          }
                      }
                      $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" >                        
                                  <tr>
                                      <td width="100%" align="left" style="font-size: 0.8em;">
                                          '.$DLCLAB.'   
                                      </td>
                                  </tr> 
                                  <tr>
                                      <td width="100%" align="center" style="font-size: 0.8em;">
                                          '.$NOTA01.'   
                                      </td>
                                  </tr> 
                      </table> <br>';
                      
                      $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" >
                                  <tr>
                                      <td width="100%" align="center" colspan="3" style="height:30px; font-size: 0.8em;">
                                          <b><u>METODOS DE ENSAYO</u></b>   
                                      </td>
                                  </tr>
                      </table>';
                  
                  /*METODOS DE ENSAYO*/    
                   $resmetensa = $this->minformes->getmetodosensayos($parametros);
                   if ($resmetensa){
                      $html .= '<table id="tableborder">
                      <thead>
                          <tr>
                              <th>
                                  <b>Ensayo</b>   
                              </th>
                              <th>
                                  <b>Norma o Referencia</b>    
                              </td>
                          </tr>
                          </thead>
                          <tbody>';
                          foreach($resmetensa as $rowmetensa){
                              $METDENSAYO = $rowmetensa->DENSAYO;
                              $METDNORMA = $rowmetensa->DNORMA;
                              $html .= '<tr>
                              <td width="180px">'.$METDENSAYO.'</td>
                                  <td>'.$METDNORMA.'</td>
                              </tr>';
                          }
                      $html .= '</tbody></table><br>';
                   }  
                  /* OBSERVACIONES */
                   if($SOBSERV > 0){
                      $html .= '<table id="tableborder" >
                        <tr>
                          <td width="100%" align="left" >
                              <b>Observaciones :</b>
                          </td>
                        </tr> 
                        <tr>
                          <td width="100%" align="center" >
                              <div style="text-align: justify;">'.$OBSERVACION.'</div>   
                          </td>
                        </tr> 
                      </table> <br>';
                   }else{
                      $html .= '';
                   }
                  /* FECHA Y FIRMAS*/ 
                   $resFECHAFIRMA = $this->minformes->getinfxmuestras_fechafirma($parametros);
                   if ($resFECHAFIRMA){
                       foreach($resFECHAFIRMA as $rowFECHAFIRMA){
                           $FECHA        = $rowFECHAFIRMA->FECHA;

                           $SFQ        = $rowFECHAFIRMA->SFQ;
                           $SMICRO        = $rowFECHAFIRMA->SMICRO;
                           $SINSTRU        = $rowFECHAFIRMA->SINSTRU;

                           $FIRMAFQS        = $rowFECHAFIRMA->FIRMAFQS;
                           $NOMBREFQS        = $rowFECHAFIRMA->NOMBREFQS;
                           $CARGOFQS        = $rowFECHAFIRMA->CARGOFQS;
                           $CODIGOFQS        = $rowFECHAFIRMA->CODIGOFQS;

                           $FIRMAMICRO        = $rowFECHAFIRMA->FIRMAMICRO;
                           $NOMBREMICRO        = $rowFECHAFIRMA->NOMBREMICRO;
                           $CARGOMICRO        = $rowFECHAFIRMA->CARGOMICRO;
                           $CODIGOMICRO        = $rowFECHAFIRMA->CODIGOMICRO;

                           $FIRMAINSTRU        = $rowFECHAFIRMA->FIRMAINSTRU;
                           $NOMBREINSTRU        = $rowFECHAFIRMA->NOMBREINSTRU;
                           $CARGOINSTRU        = $rowFECHAFIRMA->CARGOINSTRU;
                           $CODIGOINSTRU        = $rowFECHAFIRMA->CODIGOINSTRU;
                       }
                   }
                   //$html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" >   
                   //</table>';
                   
                   if($SFQ == '1' and $SMICRO == '1' and $SINSTRU == '1'){
                      $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">                     
                              <tr>
                                  <td width="100%" align="left" style="font-size: 0.8em;" colspan="2">
                                  Lima, '.$FECHA.'   
                                  </td>
                              </tr> 
                              <tr>
                                  <td width="100%" align="center">
                                  <img src="'.public_url_ftp().'Imagenes/firmas/FS000052.jpg" width="190" height="80" />    
                                  </td>
                                  <td width="100%" align="center">
                                  <img src="'.public_url_ftp().'Imagenes/firmas/FS000046.jpg" width="190" height="80" />   
                                  </td>
                              </tr>
                              <tr>
                                  <td width="100%" align="center" style="font-size: 0.8em;">
                                  <hr width=150>
                                  '.$NOMBREFQS.'    
                                  </td>
                                  <td width="100%" align="center" style="font-size: 0.8em;">
                                  <hr width=150>
                                  '.$NOMBREMICRO.'  
                                  </td>
                              </tr>
                              <tr>
                                  <td width="100%" align="center" style="font-size: 0.8em;">
                                  '.$CARGOFQS.'
                                  </td>
                                  <td width="100%" align="center" style="font-size: 0.8em;">
                                  '.$CARGOMICRO.'
                                  </td>
                              </tr>
                              <tr>
                                  <td width="100%" align="center" style="font-size: 0.8em;">
                                  '.$CODIGOFQS.'
                                  </td>
                                  <td width="100%" align="center" style="font-size: 0.8em;">
                                  '.$CODIGOMICRO.'
                                  </td>
                              </tr>

                              <tr>
                                  <td width="100%" align="center" colspan="2">
                                  <br>   
                                  <br>  
                                  </td>
                              </tr>

                              <tr>
                                  <td width="100%" align="center" colspan="2">
                                  <img src="'.public_url_ftp().'Imagenes/firmas/FS000120.jpg" width="190" height="80" />     
                                  </td>
                              </tr>
                              <tr>
                                  <td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                  <hr width=150>
                                  '.$NOMBREINSTRU.'     
                                  </td>
                              </tr>
                              <tr>
                                  <td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                  '.$CARGOINSTRU.'    
                                  </td>
                              </tr>
                              <tr>
                                  <td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                  '.$CODIGOINSTRU.'    
                                  </td>
                              </tr>
                          </tr>
                      </table>';
                   }else{
                      $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">              
                              <tr>
                                  <td width="100%" align="left" style="font-size: 0.8em;">
                                      Lima, '.$FECHA.'   
                                  </td>
                              </tr> 
                              <tr>';
                              if($SFQ == '1'){
                                  $html .= '<td width="100%" align="center">
                                      <img src="'.public_url_ftp().'Imagenes/firmas/FS000052.jpg" width="190" height="80" />    
                                      </td>';
                              }
                              if($SMICRO == '1'){
                                  $html .= '<td width="100%" align="center">
                                      <img src="'.public_url_ftp().'Imagenes/firmas/FS000046.jpg" width="190" height="80" />   
                                      </td>';
                              }
                              if($SINSTRU == '1'){
                                  $html .= '<td width="100%" align="center" colspan="2">
                                      <img src="'.public_url_ftp().'Imagenes/firmas/FS000120.jpg" width="190" height="80" />     
                                      </td>';
                              }
                      $html .= '</tr>
                              <tr>';
                              if($SFQ == '1'){
                                  $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                  <hr width=150>
                                  '.$NOMBREFQS.'    
                                  </td>';
                              }
                              if($SMICRO == '1'){
                                  $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                  <hr width=150>
                                  '.$NOMBREMICRO.'  
                                  </td>';
                              }
                              if($SINSTRU == '1'){
                                  $html .= '<td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                  <hr width=150>
                                  '.$NOMBREINSTRU.'     
                                  </td>';
                              }
                      $html .= '</tr>
                              <tr>';
                              if($SFQ == '1'){
                                  $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                  '.$CARGOFQS.'
                                  </td>';
                              }
                              if($SMICRO == '1'){
                                  $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                  '.$CARGOMICRO.'
                                  </td>';
                              }
                              if($SINSTRU == '1'){
                                  $html .= '<td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                  '.$CARGOINSTRU.'    
                                  </td>';
                              }
                      $html .= '</tr>
                              <tr>';
                              if($SFQ == '1'){
                                  $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                  '.$CODIGOFQS.'
                                  </td>';
                              }
                              if($SMICRO == '1'){
                                  $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                  '.$CODIGOMICRO.'
                                  </td>';
                              }
                              if($SINSTRU == '1'){
                                  $html .= '<td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                  '.$CODIGOINSTRU.'    
                                  </td>';
                              }
                      $html .= '</tr>
                      </table>';
                   }
                   
        $html .= '</div></td></tr></tbody></table>';
                        $pos++;
                   }}
        $html .= '</body>
                </html>';
		$filename = 'IE '.substr($NROINFORME, 0, 9).' '.$CLIENTE;
        $this->pdfgenerator->generate($html, $filename, TRUE, 'A4', 'portrait');
        //echo $html;
    } 
    
   /** INFORMES CERTIFICADO PERSONALIZADO **/                
	public function pdfInfensayoPersonalizado($cinternoordenservicio,$vcmuestra,$vcinforme) { // recupera 
        $this->load->library('pdfgenerator');

        try{
            $html = '<html>
                <head>
                    <title>Informe</title>
                    <style type="text/css">
                        @page{
                             margin: 0.3in 0.3in 0.3in 0.3in;
                        }
                        body{
                            font-family: Arial, Helvetica, sans-serif;
                            margin-top: 3.7cm;
                            margin-left: 1cm;
                            margin-right: 1cm;
                            margin-bottom: 3.5cm;
                        }
                        #header,
                        #footer,
                        #nroinf {
                            position: fixed;
                            left: 0;
                            right: 0;
                            color: #000000;
                            font-size: 0.9em;
                        }                        
                        #header {
                            top: 0;
                            height: 3.7cm;
                        }    
                        #nroinf {
                            top: 0cm;
                            height: 3.7cm;
                        }                     
                        #footer {
                            bottom: 0;
                            border-top: 0.1pt solid #aaa;
                            height: 3.5cm;
                        }                        
                        #header table,
                        #footer table,
                        #nroinf table {
                            width: 100%;
                            border-collapse: collapse;
                            border: none;
                        }                        
                        #header td,
                        #footer td,
                        #nroinf td {
                            padding: 0;
                        }
                        .page-number{
                            text-align: right;
                        }
                        .page-number:before{
                            content: "Pagina " counter(page);
                        }
                        #salto hr{
                            page-break-before: always;
                            color: #ffffff;
                            margin: -2px;
                        }     
                        #caratula td{
                            font-size:0.8em;        
                        }
                        #tableborder{
                            border-collapse: collapse;
                            width: 100%;
                            font-size:0.8em;
                            border: 0.5pt solid black;
                        }
                        #tableborder td{
                            border: 0.5pt solid black;
                            padding-left: 5px;
                        }
                        #tableborder th{
                            text-align: center;
                            border: 0.5pt solid black;
                            background-color: #DBD7D7;
                        }
                        .list-unstyled{
                            padding-left: 0;
                            list-style: none;
                        }
                    </style>
                </head>
                <body>

                    <div id="header">
                        <table width="100%" align="center">
                        <tr>
                            <td width="25%" align="center">                            
                                <img src="'.public_url_ftp().'Imagenes/formatos/2/logoFSC.jpg" width="170" height="80" />  
                            </td>
                            <td width="50%" align="center" style="font-size:0.85em;"> 
                                &nbsp;
                            </td>
                            <td width="25%" align="center">                        
                                &nbsp;
                            </td>
                        </tr>
                        </table>
                    </div>  

                    <div id="footer">
                        <table width="100%" style="font-size:0.8em; margin-top: 0.3cm; text-align: justify">
                        <tr>
                            <td width="100%" align="left" colspan="2">
                                Los resultados de los ensayos pertenecen sólo a las muestras ensayadas y no deben ser utilizados como una certificación de conformidad con normas del producto o como certificado del sistema de calidad de la entidad que lo produce. Queda prohibida la reproducción total o parcial de este informe, sin la autorización escrita de FS Certificaciones S.A.C.                            
                            </td>
                        </tr>
                        <tr>
                            <td width="100%" align="left" colspan="2"> &nbsp; </td>
                        </tr>
                        <tr>
                            <td width="50%" align="left"> 
                                FSC-F-LAB-011/V.03                           
                            </td>
                            <td width="50%" align="right" >
                                <div class="page-number"></div>
                            </td>
                        </tr>
                        <tr>
                            <td width="100%" align="center" colspan="2">
                                <ul class="list-unstyled">
                                    <li>Jr. Monterrey N° 221 Of. 201-202, Urb. Chacarilla del Estanque. Santiago de Surco, Lima - Perú</li>
                                    <li>Teléfonos: (51-1) 480 0561 - Anexo: Lab 111</li>
                                    <li>www.fscertificaciones.com</li>
                                </ul>
                            </td>
                        </tr>
                        </table>                    
                    </div>'; 
                   $parametroscabe = array(
                        '@cinternoordenservicio'         => $cinternoordenservicio,
                        '@cmuestra'                 => $vcmuestra,
                        '@idlabinformes'       => $vcinforme,
                    );
                    $res = $this->minformes->getinfpersonalizado_caratula($parametroscabe);
                    $pos = 0;
                    if ($res){
                        foreach($res as $row){ 
                            $NROINFORME     = $row->NROINFORME;
                            $CLIENTE        = $row->CLIENTE;
                            $DIRECCION      = $row->DIRECCION;
                            $NROORDEN       = $row->NROORDEN;
                            $PROCEDENCIA    = $row->PROCEDENCIA;
                            $FMUESTRA       = $row->FMUESTRA;
                            $FRECEPCION     = $row->FRECEPCION;
                            $FANALISIS      = $row->FANALISIS;
                            $LUGARMUESTRA   = $row->LUGARMUESTRA;
                            $CMUESTRA       = $row->CMUESTRA;
                            $DPRODUCTO      = $row->DPRODUCTO;
                            $DTEMPERATURA   = $row->DTEMPERATURA;
                            $DLCLAB         = $row->DLCLAB;
                            $OBSERVACION    = $row->OBSERVACION;
                            $SOBSERV        = $row->SOBSERV;
                            $ACNAC          = $row->ACNAC;
                            $nporcion       = $row->nporcion;
                            $idlabinformes       = $row->idlabinformes;
                            $NOMPRODUCTO = $row->NOMPRODUCTO;

                            /*if($pos > 0){
                                $html .= '<hr style="page-break-before: always;" size="0"/>
                                <table width="100%" align="center">
                                <tr>
                                    <td colspan="3" style="font-size:1.1em;" align="center">
                                        <b>INFORME DE ENSAYO BORRADOR </b>
                                    </td>
                                </tr>
                                </table>';                      
                            }else{
                                $html .= '<hr size="0"/>
                                <table width="100%" align="center">
                                <tr>
                                    <td colspan="3" style="font-size:1.1em;" align="center">
                                        <b>INFORME DE ENSAYO BORRADOR </b>
                                    </td>
                                </tr>
                                </table>';
                            }*/

                            if($pos > 0){
                                $html .= '<hr style="page-break-before: always;" size="0" noshade="noshade" />';                          
                            }  
                            
                            $parametros = array(
                                '@cinternoordenservicio'    => $cinternoordenservicio,
                                '@cmuestra'                 => $CMUESTRA,
                                '@idlabinformes'                 => $idlabinformes,
                            ); 
                                             
        $html .= '   <div id="header">
                    <table width="100%" align="center">
                    <tr>
                        <td width="25%" align="center">                            
                            <img src="'.public_url_ftp().'Imagenes/formatos/2/logoFSC.jpg" width="170" height="80" />  
                        </td>
                        <td width="50%" align="center" style="font-size:0.85em;">';                        
                            if($ACNAC == "1"){
                            $html .= '<ul class="list-unstyled">
                                <li>LABORATORIO DE ENSAYO ACREDITADO POR EL</li>
                                <li>ORGANISMO PERUANO DE ACREDITAción INACAL - DA</li>
                                <li>CON EL REGISTRO N° LE-073</li>
                            </ul>';
                            }                    
                            $html .= '</td>
                        <td width="25%" align="center">';                        
                            if($ACNAC == "1"){
                                $html .= '<img src="'.public_url_ftp().'Imagenes/formatos/2/inacal.jpg" width="150" height="80" /> ';
                            }                            
                        $html .= '</td>
                    </tr>
                    <tr>
                        <td colspan="3" style="height:40px; font-size:1.1em;" align="center">
                            <b>INFORME DE ENSAYO N° '.$NROINFORME.' </b>
                        </td>
                    </tr>
                    </table>
                </div>';

        $html .= '<div id="main">
                           
                    <table id="caratula" width="100%" align="center" cellspacing="0" cellpadding="2" >                       
                            
                        <tr>
                            <td align="left">
                                <b>Nombre del Cliente</b>   
                            </td>
                            <td align="left" colspan="2">
                                : '.$CLIENTE.'   
                            </td>
                        </tr>
                        <tr style="vertical-align:top">
                            <td align="left">
                                <b>Dirección del Cliente</b>  
                            </td>  
                            <td align="left" colspan="2">
                                : '.$DIRECCION.'   
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <b>N° Orden de Trabajo</b>   
                            </td>
                            <td align="left" colspan="2">
                                : '.$NROORDEN.'   
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <b>Procedencia de la Muestra</b>  
                            </td>
                            <td align="left" colspan="2">
                                : '.$PROCEDENCIA.'   
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <b>Fecha de Muestreo</b>    
                            </td>
                            <td align="left" colspan="2">
                                : '.$FMUESTRA.'   
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <b>Fecha de Recepción</b>    
                            </td>
                            <td align="left" colspan="2">
                                : '.$FRECEPCION.'<   
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <b>Fecha de Análisis</b>    
                            </td>
                            <td align="left" colspan="2">
                                : '.$FANALISIS.'  
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <b>Lugar de Muestreo</b>   
                            </td>
                            <td align="left" colspan="2">
                                : '.$LUGARMUESTRA.'  
                            </td>
                        </tr>
                        <tr style="vertical-align:top">
                            <td width="27%" align="left">
                                <b>Muestra / Descripción</b>    
                            </td>
                            <td width="7%" align="left">
                                : '.$CMUESTRA.'   
                            </td>
                            <td width="66%" align="left">
                                '.$DPRODUCTO.'   
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <b>Temperatura de Recepción</b>    
                            </td>
                            <td align="left" colspan="2">
                                : '.$DTEMPERATURA.'   
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="3">
                            &nbsp;   
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="3" style="height:30px; border-top:solid 3px #000000">
                                <b><u>RESULTADOS DE ENSAYO</u></b>   
                            </td>
                        </tr>
                        
                    </table>';
                    

                   /// RESULTADOS MICROBIOLOGIA ///
                    $resmicro = $this->minformes->getinfpersonalizado_resmicro($parametros);
                    if ($resmicro){
                        $html .= '<table id="tableborder">
                        <thead>
                        <tr>
                            <th> <b>Ensayo</b> </th>
                            <th> <b>Unidades</b> </th>
                            <th> <b>Via</b> </th>
                            <th> <b>Resultado</b> </th>
                        </tr>
                        </thead>
                        <tbody>';
                        $DENSAYOmicro = '';
                        foreach($resmicro as $rowmicro){
                             $DENSAYO = $rowmicro->DENSAYO;
                             $UNIDADMEDIDA = $rowmicro->UNIDADMEDIDA;
                             $VIA = $rowmicro->VIA;
                             $RESULT_FINAL = $rowmicro->RESULT_FINAL;
                             $EXP10RES = $rowmicro->EXP10RES;

                             $html .= '<tr>';
                             if ($DENSAYO == $DENSAYOmicro){
                                $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                             }else{
                                $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                             }
                             
                             $html .= '<td width="130px" align="center">'.$UNIDADMEDIDA.'</td>
                                 <td width="50px" align="center">'.$VIA.'</td>
                                 <td width="130px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'<sup>'.$EXP10RES.'</sup></td>
                             </tr>';
                             $DENSAYOmicro = $DENSAYO;
                        }
                        $html .= '</tbody></table><br>';
                    }  
                   /// RESULTADOS FISICOQUIMICO ///
                    $resfq = $this->minformes->getinfpersonalizado_resfq($parametros);
                    if ($resfq){
                        $html .= '<table id="tableborder">
                        <thead>
                        <tr>
                            <th> <b>Ensayo</b> </th>
                            <th> <b>Unidades</b> </th>
                            <th> <b>Via</b> </th>
                            <th> <b>Resultado</b> </th>
                        </tr>
                        </thead>
                        <tbody>';
                        $DENSAYOfq = '';
                        foreach($resfq as $rowfq){
                            $DENSAYO = $rowfq->DENSAYO;
                            $UNIDADMEDIDA = $rowfq->UNIDADMEDIDA;
                            $VIA = $rowfq->VIA;
                            $RESULT_FINAL = $rowfq->RESULT_FINAL;
                            $EXP10RES = $rowfq->EXP10RES;

                            $html .= '<tr>';
                            if ($DENSAYO == $DENSAYOfq){
                               $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                            }else{
                               $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                            }

                            $html .= '<td width="130px" align="center">'.$UNIDADMEDIDA.'</td>
                                <td width="50px" align="center">'.$VIA.'</td>
                                <td width="130px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'<sup>'.$EXP10RES.'</sup></td>
                            </tr>';
                            $DENSAYOfq = $DENSAYO;
                        }
                        $html .= '</tbody></table><br>';
                    }
                   ///RESULTADOS INSTRUMENTAL TERCERO///
                    $resins = $this->minformes->getinfpersonalizado_resinstru($parametros);
                    if ($resins){
                        $html .= '<table id="tableborder">
                        <thead>
                        <tr>
                            <th> <b>Ensayo</b> </th>
                            <th> <b>Unidades</b> </th>
                            <th> <b>Via</b> </th>
                            <th> <b>Resultado</b> </th>
                        </tr>
                        </thead>
                        <tbody>';
                        $DENSAYOins = '';
                        foreach($resins as $rowins){
                            $DENSAYO = $rowins->DENSAYO;
                            $UNIDADMEDIDA = $rowins->UNIDADMEDIDA;
                            $VIA = $rowins->VIA;
                            $RESULT_FINAL = $rowins->RESULT_FINAL;
                            $EXP10RES = $rowins->EXP10RES;

                            $html .= '<tr>';
                            if ($DENSAYO == $DENSAYOins){
                               $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                            }else{
                               $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                            }

                            $html .= '<td width="130px" align="center">'.$UNIDADMEDIDA.'</td>
                                <td width="50px" align="center">'.$VIA.'</td>
                                <td width="130px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'<sup>'.$EXP10RES.'</sup></td>
                            </tr>';
                            $DENSAYOins = $DENSAYO;
                        }
                        $html .= '</tbody></table><br>';
                    }
                   /// RESULTADOS SENSORIAL///
                    $ressen = $this->minformes->getinfpersonalizado_ressenso($parametros);
                    if ($ressen){
                        $html .= '<table id="tableborder" style="page-break-inside: avoid;">
                        <thead>
                        <tr>
                            <th> <b>Ensayo</b> </th>
                            <th> <b>Via</b> </th>
                            <th> <b>Atributo</b> </th>
                            <th> <b>Resultado</b> </th>
                        </tr>
                        </thead>
                        <tbody>';
                        $DENSAYOsen = '';
                        foreach($ressen as $rowsen){
                            $DENSAYO = $rowsen->DENSAYO;
                            $DNOMBREESCALA = $rowsen->DNOMBREESCALA;
                            $NVIAUSADO = $rowsen->NVIAUSADO;
                            $RESULT_FINAL = $rowsen->RESULT_FINAL;
                            $html .= '<tr style="vertical-align:top">';
                            if ($DENSAYO == $DENSAYOsen){
                               $html .= '';
                            }else{
                               $html .= '<td rowspan="5" style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                            }

                            $html .= '<td width="30px" align="center">'.$NVIAUSADO.'</td>
                                <td width="100px">'.$DNOMBREESCALA.'</td>
                                <td width="210px">'.str_replace("<", "&lt;", $RESULT_FINAL).'</td>
                            </tr>';
                            $DENSAYOsen = $DENSAYO;
                        }
                        $html .= '</tbody></table><br>';
                    }
                    
                   /// RESULTADOS CON ELEMENTOS ///
                    $resele = $this->minformes->getinfpersonalizado_reselementos($parametros);
                    if ($resele){
                       $html .= '<table id="tableborder">
                       <thead>
                       <tr>
                           <th> <b>Ensayo</b> </th>
                           <th> <b>Via</b> </th>
                           <th> <b>Elemento</b> </th>
                           <th> <b>Unidades</b> </th>
                           <th> <b>Resultado</b> </th>
                       </tr>
                       </thead>
                       <tbody>';
                       $DENSAYOele = '';
                       foreach($resele as $rowele){
                        $DENSAYO = $rowele->DENSAYO;
                        $ELEMENTO = $rowele->ELEMENTO;
                        $UNIDADMEDIDA = $rowele->UNIDADMEDIDA;
                        $VIA = $rowele->VIA;
                        $RESULT_FINAL = $rowele->RESULT_FINAL;
                        $html .= '<tr>';
                        if ($DENSAYO == $DENSAYOele){
                           $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                        }else{
                           $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                        }

                        $html .= '<td width="30px" align="center">'.$VIA.'</td>
                            <td>'.$ELEMENTO.'</td>
                            <td width="100px" align="center">'.$UNIDADMEDIDA.'</td>
                            <td width="130px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'</td>
                        </tr>';
                        $DENSAYOele = $DENSAYO;
                       }
                       $html .= '</tbody></table><br>';
                    }
                   /// RESULTADOS ESTERILIDAD BAJA ///
                    $parametrosEsterili = array(
                       '@cinternoordenservicio' => $cinternoordenservicio,
                       '@cmuestra' => $CMUESTRA,
                       '@idlabinformes' => $idlabinformes,
                       '@stipoacidez' => 'B'
                    );
                    $resesteb = $this->minformes->getinfpersonalizado_resesteri($parametrosEsterili);
                    if ($resesteb){
                       $html .= '<table id="tableborder">
                       <tr>
                           <td colspan="7"> Baja acidez (pH>4.6)</td>
                       </tr>
                       <tr>
                           <th width="30%" rowspan="2"> <b>Preincubación 35°C/14 días</b> </th>
                           <th width="5%" rowspan="2"> <b>pH</b> </th>
                           <th width="25%" colspan="2"> <b>Mesófilos (35°C)</b> </th>
                           <th width="15%" colspan="2"> <b>Termófilos (55°C)</b> </th>
                           <th width="25%" rowspan="2"> <b>Resultado</b> </th>
                       </tr>
                       <tr>
                           <th> <b>Caldo púrpura de bromocreasol 120h</b> </th>
                           <th> <b>Caldo carne cocida 120h</b> </th>
                           <th> <b>Caldo púrpura de bromocreasol 48h</b> </th>
                           <th> <b>Caldo carne cocida 72h</b> </th>
                       </tr>';
                       foreach($resesteb as $rowesteb){
                            $preincuba = $rowesteb->preincuba;
                            $dph = $rowesteb->dph;
                            $dbames35cp = $rowesteb->dbames35cp;
                            $dbames35cc = $rowesteb->dbames35cc;
                            $dbater55cp = $rowesteb->dbater55cp;
                            $dbater55cc = $rowesteb->dbater55cc;
                            $result_final = $rowesteb->result_final;
                            $html .= '<tr>
                                <td>'.$preincuba.'</td>
                                <td align="center">'.$dph.'</td>
                                <td align="center">'.$dbames35cp.'</td>
                                <td align="center">'.$dbames35cc.'</td>
                                <td align="center">'.$dbater55cp.'</td>
                                <td align="center">'.$dbater55cc.'</td>
                                <td align="center">'.$result_final.'</td>
                            </tr>';
                       }
                       $html .= '</table><br>';
                    }
                   /// RESULTADOS ESTERILIDAD ALTA ///
                    $parametrosEsterili = array(
                       '@cinternoordenservicio' => $cinternoordenservicio,
                       '@cmuestra' => $CMUESTRA,
                       '@idlabinformes' => $idlabinformes,
                       '@stipoacidez' => 'A'
                    );
                    $resestea = $this->minformes->getinfpersonalizado_resesteri($parametrosEsterili);
                    if ($resestea){
                       $html .= '<table id="tableborder">
                       <tr>
                            <td colspan="7"> Alta acidez ('.str_replace("<", "&lt;", "pH<4.6").')</td>
                       </tr>
                       <tr>
                           <th width="30%" rowspan="2"> <b>Preincubación 35°C/14 días</b> </th>
                           <th width="5%" rowspan="2"> <b>pH</b> </th>
                           <th width="25%" colspan="2"> <b>Mesófilos (35°C)</b> </th>
                           <th width="15%" colspan="2"> <b>Termófilos (55°C)</b> </th>
                           <th width="25%" rowspan="2"> <b>Resultado</b> </th>
                       </tr>
                       <tr>
                           <th> <b>Caldo ácido 96h</b> </th>
                           <th> <b>Caldo extracto de malta 96h</b> </th>
                           <th colspan="2"> <b>Caldo ácido 48h</b> </th>
                       </tr>';
                       foreach($resestea as $rowestea){
                            $preincuba = $rowestea->preincuba;
                            $dph = $rowestea->dph;
                            $dacmes30ca = $rowestea->dacmes30ca;
                            $dacmes30cm = $rowestea->dacmes30cm;
                            $dacter55ca = $rowestea->dacter55ca;
                            $result_final = $rowestea->result_final;
                            $html .= '<tr>
                                <td>'.$preincuba.'</td>
                                <td align="center">'.$dph.'</td>
                                <td align="center">'.$dacmes30ca.'</td>
                                <td align="center">'.$dacmes30cm.'</td>
                                <td align="center" colspan="2">'.$dacter55ca.'</td>
                                <td align="center">'.$result_final.'</td>
                            </tr>';
                        }
                        $html .= '</table><br>';
                    }                   

                   ///RESULTADOS ETIQUETADO NUTRICIONAL///
                    $parametrosEtiquetado = array(
                        '@cinternoordenservicio' => $cinternoordenservicio,
                        '@cmuestra' => $CMUESTRA,
                        '@idlabinformes' => $idlabinformes
                    );
                    $resetiqueta = $this->minformes->getinfpersonalizado_resetiquetado($parametrosEtiquetado);
                    if ($resetiqueta){
                      $html .= '<table id="tableborder">
                        <tr>
                            <td colspan="7"> <b>INFORMACION NUTRICIONAL</b> </td>
                        </tr>
                        <tr>
                            <td width="40%" colspan="4"> Tamaño de Porción :</td>
                            <td colspan="3">'.$nporcion.'</td>
                        </tr>
                        <tr>
                            <th width="25%" colspan="3"> <b>Ensayo</b> </th>
                            <th width="15%"> <b>Unidad</b> </th>
                            <th width="20%"> <b>100g</b> </th>
                            <th width="20%"> <b>Porción</b> </th>
                            <th width="20%"> <b>% VDR(*)</b> </th>
                        </tr>';
                        foreach($resetiqueta as $rowetiqueta){
                            $DENSAYO = $rowetiqueta->DENSAYO;
                            $UNIDADMEDIDA = $rowetiqueta->UNIDADMEDIDA;
                            $DRESULTADO = $rowetiqueta->DRESULTADO;
                            $NPORCION = $rowetiqueta->NPORCION;
                            $VALORVR = $rowetiqueta->VALORVR;
                            $html .= '<tr>
                                <td colspan="3">'.$DENSAYO.'</td>
                                <td align="center">'.$UNIDADMEDIDA.'</td>
                                <td align="center">'.$DRESULTADO.'</td>
                                <td align="center">'.$NPORCION.'</td>
                                <td align="center">'.$VALORVR.'</td>
                            </tr>';
                        }
                        $html .= '<tr><td colspan="7">(*) % Valor Diario (VD) con base a una dieta de 2000 kcal u 8370 kJ.  según Codex Alimentarius FAO/OMS. Sus valores diarios pueden ser mayores o menores dependiendo de sus necesidades energéticas.</tr>
                                   </table><br>';
                    }
                    
                   /// NOTAS ADICIONALES /
                    if($cinternoordenservicio !== '15734' && $vcmuestra !== 'M03' && $vcinforme !== '99'){
                        $resNOTA1 = $this->minformes->getinfpersonalizado_nota01($parametros);
                        if ($resNOTA1){
                            foreach($resNOTA1 as $rowNOTA1){
                                $NOTA01        = $rowNOTA1->NOTA01;
                            }
                        }
                        $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" >                        
                                    <tr>
                                        <td width="100%" align="left" style="font-size: 0.8em;">
                                            '.$DLCLAB.'   
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td width="100%" align="center" style="font-size: 0.8em;">
                                            '.$NOTA01.'   
                                        </td>
                                    </tr> 
                        </table> <br>';
                        
                       
                    }
                     $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" >
                                    <tr>
                                        <td width="100%" align="center" colspan="3" style="height:30px; font-size: 0.8em;">
                                            <b><u>METODOS DE ENSAYO</u></b>   
                                        </td>
                                    </tr>
                        </table>';
                   /// METODOS DE ENSAYO ///    
                     $resmetensa = $this->minformes->getmetodosensayospersonalizado($parametros);
                     if ($resmetensa){
                        $html .= '<table id="tableborder">
                        <thead>
                            <tr>
                                <th>
                                    <b>Ensayo</b>   
                                </th>
                                <th>
                                    <b>Norma o Referencia</b>    
                                </td>
                            </tr>
                            </thead>
                            <tbody>';
                            foreach($resmetensa as $rowmetensa){
                                $METDENSAYO = $rowmetensa->DENSAYO;
                                $METDNORMA = $rowmetensa->DNORMA;
                                $html .= '<tr>
                                <td width="180px">'.$METDENSAYO.'</td>
                                    <td>'.$METDNORMA.'</td>
                                </tr>';
                            }
                        $html .= '</tbody></table><br>';
                     }  
 
                   /// OBSERVACIONES /
                    if($SOBSERV > 0){
                        $html .= '<table id="tableborder" >
                          <tr>
                            <td width="100%" align="left" >
                                <b>Observaciones :</b>
                            </td>
                          </tr> 
                          <tr>
                            <td width="100%" align="center" >
                                <div style="text-align: justify;">'.$OBSERVACION.'</div>   
                            </td>
                          </tr> 
                        </table> <br>';
                     }
                   /* FECHA Y FIRMAS*/ 
                     $resFECHAFIRMA = $this->minformes->getinfpersonalizado_fechafirma($parametros);
                     if ($resFECHAFIRMA){
                         foreach($resFECHAFIRMA as $rowFECHAFIRMA){
                             $FECHA        = $rowFECHAFIRMA->FECHA;

                             $SFQ        = $rowFECHAFIRMA->SFQ;
                             $SMICRO        = $rowFECHAFIRMA->SMICRO;
                             $SINSTRU        = $rowFECHAFIRMA->SINSTRU;

                             $FIRMAFQS        = $rowFECHAFIRMA->FIRMAFQS;
                             $NOMBREFQS        = $rowFECHAFIRMA->NOMBREFQS;
                             $CARGOFQS        = $rowFECHAFIRMA->CARGOFQS;
                             $CODIGOFQS        = $rowFECHAFIRMA->CODIGOFQS;

                             $FIRMAMICRO        = $rowFECHAFIRMA->FIRMAMICRO;
                             $NOMBREMICRO        = $rowFECHAFIRMA->NOMBREMICRO;
                             $CARGOMICRO        = $rowFECHAFIRMA->CARGOMICRO;
                             $CODIGOMICRO        = $rowFECHAFIRMA->CODIGOMICRO;

                             $FIRMAINSTRU        = $rowFECHAFIRMA->FIRMAINSTRU;
                             $NOMBREINSTRU        = $rowFECHAFIRMA->NOMBREINSTRU;
                             $CARGOINSTRU        = $rowFECHAFIRMA->CARGOINSTRU;
                             $CODIGOINSTRU        = $rowFECHAFIRMA->CODIGOINSTRU;
                         }
                     }
                     //$html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" >   
                     //</table>';
                     
                     if($SFQ == '1' and $SMICRO == '1' and $SINSTRU == '1'){
                        $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">                     
                                <tr>
                                    <td width="100%" align="left" style="font-size: 0.8em;" colspan="2">
                                    Lima, '.$FECHA.'   
                                    </td>
                                </tr> 
                                <tr>
                                    <td width="100%" align="center">
                                    <img src="'.public_url_ftp().'Imagenes/firmas/FS000052.jpg" width="190" height="80" />    
                                    </td>
                                    <td width="100%" align="center">
                                    <img src="'.public_url_ftp().'Imagenes/firmas/FS000046.jpg" width="190" height="80" />   
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100%" align="center" style="font-size: 0.8em;">
                                    <hr width=150>
                                    '.$NOMBREFQS.'    
                                    </td>
                                    <td width="100%" align="center" style="font-size: 0.8em;">
                                    <hr width=150>
                                    '.$NOMBREMICRO.'  
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100%" align="center" style="font-size: 0.8em;">
                                    '.$CARGOFQS.'
                                    </td>
                                    <td width="100%" align="center" style="font-size: 0.8em;">
                                    '.$CARGOMICRO.'
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100%" align="center" style="font-size: 0.8em;">
                                    '.$CODIGOFQS.'
                                    </td>
                                    <td width="100%" align="center" style="font-size: 0.8em;">
                                    '.$CODIGOMICRO.'
                                    </td>
                                </tr>

                                <tr>
                                    <td width="100%" align="center" colspan="2">
                                    <br>   
                                    <br>  
                                    </td>
                                </tr>

                                <tr>
                                    <td width="100%" align="center" colspan="2">
                                    <img src="'.public_url_ftp().'Imagenes/firmas/FS000120.jpg" width="190" height="80" />     
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                    <hr width=150>
                                    '.$NOMBREINSTRU.'     
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                    '.$CARGOINSTRU.'    
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                    '.$CODIGOINSTRU.'    
                                    </td>
                                </tr>
                            </tr>
                        </table>';
                     }else if($SFQ == '0' and $SMICRO == '0' and $SINSTRU == '0'){
                        $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">                     
                                <tr>
                                    <td width="100%" align="left" style="font-size: 0.8em;" colspan="2">
                                    Lima, '.$FECHA.'   
                                    </td>
                                </tr> 
                                <tr>
                                    <td width="100%" align="center">
                                    <img src="'.public_url_ftp().'Imagenes/firmas/FS000052.jpg" width="190" height="80" />    
                                    </td>
                                    <td width="100%" align="center">
                                    <img src="'.public_url_ftp().'Imagenes/firmas/FS000046.jpg" width="190" height="80" />   
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100%" align="center" style="font-size: 0.8em;">
                                    <hr width=150>
                                    '.$NOMBREFQS.'    
                                    </td>
                                    <td width="100%" align="center" style="font-size: 0.8em;">
                                    <hr width=150>
                                    '.$NOMBREMICRO.'  
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100%" align="center" style="font-size: 0.8em;">
                                    '.$CARGOFQS.'
                                    </td>
                                    <td width="100%" align="center" style="font-size: 0.8em;">
                                    '.$CARGOMICRO.'
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100%" align="center" style="font-size: 0.8em;">
                                    '.$CODIGOFQS.'
                                    </td>
                                    <td width="100%" align="center" style="font-size: 0.8em;">
                                    '.$CODIGOMICRO.'
                                    </td>
                                </tr>

                                <tr>
                                    <td width="100%" align="center" colspan="2">
                                    <br>   
                                    <br>  
                                    </td>
                                </tr>

                                <tr>
                                    <td width="100%" align="center" colspan="2">
                                    <img src="'.public_url_ftp().'Imagenes/firmas/FS000120.jpg" width="190" height="80" />     
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                    <hr width=150>
                                    '.$NOMBREINSTRU.'     
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                    '.$CARGOINSTRU.'    
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                    '.$CODIGOINSTRU.'    
                                    </td>
                                </tr>
                            </tr>
                        </table>';
                     }
                     else{
                        $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">              
                                <tr>
                                    <td width="100%" align="left" style="font-size: 0.8em;">
                                        Lima, '.$FECHA.'   
                                    </td>
                                </tr> 
                                <tr>';
                               
                                if($SFQ == '1'){
                                    $html .= '<td width="100%" align="center">
                                        <img src="'.public_url_ftp().'Imagenes/firmas/FS000052.jpg" width="190" height="80" />    
                                        </td>';
                                }
                                 if($SMICRO == '1' ){
                                    $html .= '<td width="100%" align="center">
                                        <img src="'.public_url_ftp().'Imagenes/firmas/FS000046.jpg" width="190" height="80" />   
                                        </td>';
                                }
                                if($SINSTRU == '1'){
                                    $html .= '<td width="100%" align="center" colspan="2">
                                        <img src="'.public_url_ftp().'Imagenes/firmas/FS000120.jpg" width="190" height="80" />     
                                        </td>';
                                }
                        $html .= '</tr>
                                <tr>';
                               
                                if($SFQ == '1'){
                                    $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                    <hr width=150>
                                    '.$NOMBREFQS.'    
                                    </td>';
                                }
                                 if($SMICRO == '1' ){
                                    $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                    <hr width=150>
                                    '.$NOMBREMICRO.'  
                                    </td>';
                                }
                                if($SINSTRU == '1'){
                                    $html .= '<td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                    <hr width=150>
                                    '.$NOMBREINSTRU.'     
                                    </td>';
                                }
                        $html .= '</tr>
                                <tr>';
                               
                                if($SFQ == '1'){
                                    $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                    '.$CARGOFQS.'
                                    </td>';
                                }
                                 if($SMICRO == '1'){
                                    $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                    '.$CARGOMICRO.'
                                    </td>';
                                }
                                if($SINSTRU == '1'){
                                    $html .= '<td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                    '.$CARGOINSTRU.'    
                                    </td>';
                                }
                        $html .= '</tr>
                                <tr>';
                               
                                if($SFQ == '1'){
                                    $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                    '.$CODIGOFQS.'
                                    </td>';
                                }
                                 if($SMICRO == '1'){
                                    $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                    '.$CODIGOMICRO.'
                                    </td>';
                                }
                                if($SINSTRU == '1'){
                                    $html .= '<td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                    '.$CODIGOINSTRU.'    
                                    </td>';
                                }
                        $html .= '</tr>
                        </table>';
                     }
                     
                    
            $html .= '</div>';
                            $pos++;
                    }}
            $html .= '</body>
                    </html>';
            $filename = 'IE '.substr($NROINFORME, 0, 9).' '.$CLIENTE.' '.$CMUESTRA;
             $this->pdfgenerator->generate($html, $filename, TRUE, 'A4', 'portrait');
            //echo $html;
        }catch(Exception $e){
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        }
        //echo $html;
    }         
	public function pdfInfensayoPersonalizadoSinLogo($cinternoordenservicio,$vcmuestra,$vcinforme,$firma) { // recupera 
        $this->load->library('pdfgenerator');

        try{
            $html = '<html>
                <head>
                    <title>Informe</title>
                    <style type="text/css">
                        @page{
                             margin: 0.3in 0.3in 0.3in 0.3in;
                        }
                        body{
                            font-family: Arial, Helvetica, sans-serif;
                            margin-top: 3.7cm;
                            margin-left: 1cm;
                            margin-right: 1cm;
                            margin-bottom: 3.5cm;
                        }
                        #header,
                        #footer,
                        #nroinf {
                            position: fixed;
                            left: 0;
                            right: 0;
                            color: #000000;
                            font-size: 0.9em;
                        }                        
                        #header {
                            top: 1.5cm;
                            height: 1.5cm;
                        }    
                        #nroinf {
                            top: 0cm;
                            height: 3.7cm;
                        }                     
                        #footer {
                            bottom: 0;
                            border-top: 0.1pt solid #aaa;
                            height: 3.5cm;
                        }                        
                        #header table,
                        #footer table,
                        #nroinf table {
                            width: 100%;
                            border-collapse: collapse;
                            border: none;
                        }                        
                        #header td,
                        #footer td,
                        #nroinf td {
                            padding: 0;
                        }
                        .page-number{
                            text-align: right;
                        }
                        .page-number:before{
                            content: "Pagina " counter(page);
                        }
                        #salto hr{
                            page-break-before: always;
                            color: #ffffff;
                            margin: -2px;
                        }     
                        #caratula td{
                            font-size:0.8em;        
                        }
                        #tableborder{
                            border-collapse: collapse;
                            width: 100%;
                            font-size:0.8em;
                            border: 0.5pt solid black;
                        }
                        #tableborder td{
                            border: 0.5pt solid black;
                            padding-left: 5px;
                        }
                        #tableborder th{
                            text-align: center;
                            border: 0.5pt solid black;
                            background-color: #DBD7D7;
                        }
                        .list-unstyled{
                            padding-left: 0;
                            list-style: none;
                        }
                    </style>
                </head>
                <body>

                    <div id="header">
                        <table width="100%" align="center">
                        <tr>
                            <td width="25%" align="center">                            
                                &nbsp;
                            </td>
                            <td width="50%" align="center" style="font-size:0.85em;"> 
                                &nbsp;
                            </td>
                            <td width="25%" align="center">                        
                                &nbsp;
                            </td>
                        </tr>
                        </table>
                    </div>  

                    <div id="footer">
                        <table width="100%" style="font-size:0.8em; margin-top: 0.3cm; text-align: justify">
                        <tr>
                            <td width="100%" align="left" colspan="2">
                                Los resultados de los ensayos pertenecen sólo a las muestras ensayadas y no deben ser utilizados como una certificación de conformidad con normas del producto o como certificado del sistema de calidad de la entidad que lo produce. Queda prohibida la reproducción total o parcial de este informe, sin la autorización escrita de FS Certificaciones S.A.C.                            
                            </td>
                        </tr>
                        <tr>
                            <td width="100%" align="left" colspan="2"> &nbsp; </td>
                        </tr>
                        <tr>
                            <td width="50%" align="left"> 
                                FSC-F-LAB-011/V.03                           
                            </td>
                            <td width="50%" align="right" >
                                <div class="page-number"></div>
                            </td>
                        </tr>
                        <tr>
                            <td width="100%" align="center" colspan="2">
                                <ul class="list-unstyled">
                                    <li>&nbsp;</li>
                                    <li>&nbsp;</li>
                                    <li>&nbsp;</li>
                                </ul>
                            </td>
                        </tr>
                        </table>                    
                    </div>'; 
                   $parametroscabe = array(
                        '@cinternoordenservicio'         => $cinternoordenservicio,
                        '@cmuestra'                 => $vcmuestra,
                        '@idlabinformes'       => $vcinforme,
                    );
                    $res = $this->minformes->getinfpersonalizado_caratula($parametroscabe);
                    $pos = 0;
                    if ($res){
                        foreach($res as $row){ 
                            $NROINFORME     = $row->NROINFORME;
                            $CLIENTE        = $row->CLIENTE;
                            $DIRECCION      = $row->DIRECCION;
                            $NROORDEN       = $row->NROORDEN;
                            $PROCEDENCIA    = $row->PROCEDENCIA;
                            $FMUESTRA       = $row->FMUESTRA;
                            $FRECEPCION     = $row->FRECEPCION;
                            $FANALISIS      = $row->FANALISIS;
                            $LUGARMUESTRA   = $row->LUGARMUESTRA;
                            $CMUESTRA       = $row->CMUESTRA;
                            $DPRODUCTO      = $row->DPRODUCTO;
                            $DTEMPERATURA   = $row->DTEMPERATURA;
                            $DLCLAB         = $row->DLCLAB;
                            $OBSERVACION    = $row->OBSERVACION;
                            $SOBSERV        = $row->SOBSERV;
                            $ACNAC          = $row->ACNAC;
                            $nporcion       = $row->nporcion;
                            $idlabinformes       = $row->idlabinformes;
                            $NOMPRODUCTO = $row->NOMPRODUCTO;

                            /*if($pos > 0){
                                $html .= '<hr style="page-break-before: always;" size="0"/>
                                <table width="100%" align="center">
                                <tr>
                                    <td colspan="3" style="font-size:1.1em;" align="center">
                                        <b>INFORME DE ENSAYO BORRADOR </b>
                                    </td>
                                </tr>
                                </table>';                      
                            }else{
                                $html .= '<hr size="0"/>
                                <table width="100%" align="center">
                                <tr>
                                    <td colspan="3" style="font-size:1.1em;" align="center">
                                        <b>INFORME DE ENSAYO BORRADOR </b>
                                    </td>
                                </tr>
                                </table>';
                            }*/

                            if($pos > 0){
                                $html .= '<hr style="page-break-before: always;" size="0" noshade="noshade" />';                          
                            }  
                            
                            $parametros = array(
                                '@cinternoordenservicio'    => $cinternoordenservicio,
                                '@cmuestra'                 => $CMUESTRA,
                                '@idlabinformes'                 => $idlabinformes,
                            ); 
                                             
        $html .= '   <div id="header">
                    <table width="100%" align="center">
                    <tr>
                        <td width="25%" align="center">                            
                            &nbsp; 
                        </td>
                        <td width="50%" align="center" style="font-size:0.85em;">';                        
                            if($ACNAC == "1"){
                            $html .= '<ul class="list-unstyled">
                            <li>&nbsp;</li>
                            <li>&nbsp;</li>
                            <li>&nbsp;</li>
                            </ul>';
                            }                    
                            $html .= '</td>
                        <td width="25%" align="center">';                        
                            if($ACNAC == "1"){
                                $html .= '&nbsp; ';
                            }                            
                        $html .= '</td>
                    </tr>
                    <tr>
                        <td colspan="3" style="height:40px; font-size:1.1em;" align="center">
                            <b>INFORME DE ENSAYO N° '.$NROINFORME.' </b>
                        </td>
                    </tr>
                    </table>
                </div>';

        $html .= '<div id="main">
                           
                    <table id="caratula" width="100%" align="center" cellspacing="0" cellpadding="2" >                       
                            
                        <tr>
                            <td align="left">
                                <b>Nombre del Cliente</b>   
                            </td>
                            <td align="left" colspan="2">
                                : '.$CLIENTE.'   
                            </td>
                        </tr>
                        <tr style="vertical-align:top">
                            <td align="left">
                                <b>Dirección del Cliente</b>  
                            </td>  
                            <td align="left" colspan="2">
                                : '.$DIRECCION.'   
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <b>N° Orden de Trabajo</b>   
                            </td>
                            <td align="left" colspan="2">
                                : '.$NROORDEN.'   
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <b>Procedencia de la Muestra</b>  
                            </td>
                            <td align="left" colspan="2">
                                : '.$PROCEDENCIA.'   
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <b>Fecha de Muestreo</b>    
                            </td>
                            <td align="left" colspan="2">
                                : '.$FMUESTRA.'   
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <b>Fecha de Recepción</b>    
                            </td>
                            <td align="left" colspan="2">
                                : '.$FRECEPCION.'<   
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <b>Fecha de Análisis</b>    
                            </td>
                            <td align="left" colspan="2">
                                : '.$FANALISIS.'  
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <b>Lugar de Muestreo</b>   
                            </td>
                            <td align="left" colspan="2">
                                : '.$LUGARMUESTRA.'  
                            </td>
                        </tr>
                        <tr style="vertical-align:top">
                            <td width="27%" align="left">
                                <b>Muestra / Descripción</b>    
                            </td>
                            <td width="7%" align="left">
                                : '.$CMUESTRA.'   
                            </td>
                            <td width="66%" align="left">
                                '.$DPRODUCTO.'   
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <b>Temperatura de Recepción</b>    
                            </td>
                            <td align="left" colspan="2">
                                : '.$DTEMPERATURA.'   
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="3">
                            &nbsp;   
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="3" style="height:30px; border-top:solid 3px #000000">
                                <b><u>RESULTADOS DE ENSAYO</u></b>   
                            </td>
                        </tr>
                        
                    </table>';
                    

                   /// RESULTADOS MICROBIOLOGIA ///
                    $resmicro = $this->minformes->getinfpersonalizado_resmicro($parametros);
                    if ($resmicro){
                        $html .= '<table id="tableborder">
                        <thead>
                        <tr>
                            <th> <b>Ensayo</b> </th>
                            <th> <b>Unidades</b> </th>
                            <th> <b>Via</b> </th>
                            <th> <b>Resultado</b> </th>
                        </tr>
                        </thead>
                        <tbody>';
                        $DENSAYOmicro = '';
                        foreach($resmicro as $rowmicro){
                             $DENSAYO = $rowmicro->DENSAYO;
                             $UNIDADMEDIDA = $rowmicro->UNIDADMEDIDA;
                             $VIA = $rowmicro->VIA;
                             $RESULT_FINAL = $rowmicro->RESULT_FINAL;
                             $EXP10RES = $rowmicro->EXP10RES;

                             $html .= '<tr>';
                             if ($DENSAYO == $DENSAYOmicro){
                                $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                             }else{
                                $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                             }
                             
                             $html .= '<td width="130px" align="center">'.$UNIDADMEDIDA.'</td>
                                 <td width="50px" align="center">'.$VIA.'</td>
                                 <td width="130px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'<sup>'.$EXP10RES.'</sup></td>
                             </tr>';
                             $DENSAYOmicro = $DENSAYO;
                        }
                        $html .= '</tbody></table><br>';
                    }  
                   /// RESULTADOS FISICOQUIMICO ///
                    $resfq = $this->minformes->getinfpersonalizado_resfq($parametros);
                    if ($resfq){
                        $html .= '<table id="tableborder">
                        <thead>
                        <tr>
                            <th> <b>Ensayo</b> </th>
                            <th> <b>Unidades</b> </th>
                            <th> <b>Via</b> </th>
                            <th> <b>Resultado</b> </th>
                        </tr>
                        </thead>
                        <tbody>';
                        $DENSAYOfq = '';
                        foreach($resfq as $rowfq){
                            $DENSAYO = $rowfq->DENSAYO;
                            $UNIDADMEDIDA = $rowfq->UNIDADMEDIDA;
                            $VIA = $rowfq->VIA;
                            $RESULT_FINAL = $rowfq->RESULT_FINAL;
                            $EXP10RES = $rowfq->EXP10RES;

                            $html .= '<tr>';
                            if ($DENSAYO == $DENSAYOfq){
                               $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                            }else{
                               $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                            }

                            $html .= '<td width="130px" align="center">'.$UNIDADMEDIDA.'</td>
                                <td width="50px" align="center">'.$VIA.'</td>
                                <td width="130px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'<sup>'.$EXP10RES.'</sup></td>
                            </tr>';
                            $DENSAYOfq = $DENSAYO;
                        }
                        $html .= '</tbody></table><br>';
                    }
                   ///RESULTADOS INSTRUMENTAL TERCERO///
                    $resins = $this->minformes->getinfpersonalizado_resinstru($parametros);
                    if ($resins){
                        $html .= '<table id="tableborder">
                        <thead>
                        <tr>
                            <th> <b>Ensayo</b> </th>
                            <th> <b>Unidades</b> </th>
                            <th> <b>Via</b> </th>
                            <th> <b>Resultado</b> </th>
                        </tr>
                        </thead>
                        <tbody>';
                        $DENSAYOins = '';
                        foreach($resins as $rowins){
                            $DENSAYO = $rowins->DENSAYO;
                            $UNIDADMEDIDA = $rowins->UNIDADMEDIDA;
                            $VIA = $rowins->VIA;
                            $RESULT_FINAL = $rowins->RESULT_FINAL;
                            $EXP10RES = $rowins->EXP10RES;

                            $html .= '<tr>';
                            if ($DENSAYO == $DENSAYOins){
                               $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                            }else{
                               $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                            }

                            $html .= '<td width="130px" align="center">'.$UNIDADMEDIDA.'</td>
                                <td width="50px" align="center">'.$VIA.'</td>
                                <td width="130px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'<sup>'.$EXP10RES.'</sup></td>
                            </tr>';
                            $DENSAYOins = $DENSAYO;
                        }
                        $html .= '</tbody></table><br>';
                    }
                   /// RESULTADOS SENSORIAL///
                    $ressen = $this->minformes->getinfpersonalizado_ressenso($parametros);
                    if ($ressen){
                        $html .= '<table id="tableborder" style="page-break-inside: avoid;">
                        <thead>
                        <tr>
                            <th> <b>Ensayo</b> </th>
                            <th> <b>Via</b> </th>
                            <th> <b>Atributo</b> </th>
                            <th> <b>Resultado</b> </th>
                        </tr>
                        </thead>
                        <tbody>';
                        $DENSAYOsen = '';
                        foreach($ressen as $rowsen){
                            $DENSAYO = $rowsen->DENSAYO;
                            $DNOMBREESCALA = $rowsen->DNOMBREESCALA;
                            $NVIAUSADO = $rowsen->NVIAUSADO;
                            $RESULT_FINAL = $rowsen->RESULT_FINAL;
                            $html .= '<tr style="vertical-align:top">';
                            if ($DENSAYO == $DENSAYOsen){
                               $html .= '';
                            }else{
                               $html .= '<td rowspan="5" style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                            }

                            $html .= '<td width="30px" align="center">'.$NVIAUSADO.'</td>
                                <td width="100px">'.$DNOMBREESCALA.'</td>
                                <td width="210px">'.str_replace("<", "&lt;", $RESULT_FINAL).'</td>
                            </tr>';
                            $DENSAYOsen = $DENSAYO;
                        }
                        $html .= '</tbody></table><br>';
                    }
                    
                   /// RESULTADOS CON ELEMENTOS ///
                    $resele = $this->minformes->getinfpersonalizado_reselementos($parametros);
                    if ($resele){
                       $html .= '<table id="tableborder">
                       <thead>
                       <tr>
                           <th> <b>Ensayo</b> </th>
                           <th> <b>Via</b> </th>
                           <th> <b>Elemento</b> </th>
                           <th> <b>Unidades</b> </th>
                           <th> <b>Resultado</b> </th>
                       </tr>
                       </thead>
                       <tbody>';
                       $DENSAYOele = '';
                       foreach($resele as $rowele){
                        $DENSAYO = $rowele->DENSAYO;
                        $ELEMENTO = $rowele->ELEMENTO;
                        $UNIDADMEDIDA = $rowele->UNIDADMEDIDA;
                        $VIA = $rowele->VIA;
                        $RESULT_FINAL = $rowele->RESULT_FINAL;
                        $html .= '<tr>';
                        if ($DENSAYO == $DENSAYOele){
                           $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                        }else{
                           $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                        }

                        $html .= '<td width="30px" align="center">'.$VIA.'</td>
                            <td>'.$ELEMENTO.'</td>
                            <td width="100px" align="center">'.$UNIDADMEDIDA.'</td>
                            <td width="130px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'</td>
                        </tr>';
                        $DENSAYOele = $DENSAYO;
                       }
                       $html .= '</tbody></table><br>';
                    }
                   /// RESULTADOS ESTERILIDAD BAJA ///
                    $parametrosEsterili = array(
                       '@cinternoordenservicio' => $cinternoordenservicio,
                       '@cmuestra' => $CMUESTRA,
                       '@idlabinformes' => $idlabinformes,
                       '@stipoacidez' => 'B'
                    );
                    $resesteb = $this->minformes->getinfpersonalizado_resesteri($parametrosEsterili);
                    if ($resesteb){
                       $html .= '<table id="tableborder">
                       <tr>
                           <td colspan="7"> Baja acidez (pH>4.6)</td>
                       </tr>
                       <tr>
                           <th width="30%" rowspan="2"> <b>Preincubación 35°C/14 días</b> </th>
                           <th width="5%" rowspan="2"> <b>pH</b> </th>
                           <th width="25%" colspan="2"> <b>Mesófilos (35°C)</b> </th>
                           <th width="15%" colspan="2"> <b>Termófilos (55°C)</b> </th>
                           <th width="25%" rowspan="2"> <b>Resultado</b> </th>
                       </tr>
                       <tr>
                           <th> <b>Caldo púrpura de bromocreasol 120h</b> </th>
                           <th> <b>Caldo carne cocida 120h</b> </th>
                           <th> <b>Caldo púrpura de bromocreasol 48h</b> </th>
                           <th> <b>Caldo carne cocida 72h</b> </th>
                       </tr>';
                       foreach($resesteb as $rowesteb){
                            $preincuba = $rowesteb->preincuba;
                            $dph = $rowesteb->dph;
                            $dbames35cp = $rowesteb->dbames35cp;
                            $dbames35cc = $rowesteb->dbames35cc;
                            $dbater55cp = $rowesteb->dbater55cp;
                            $dbater55cc = $rowesteb->dbater55cc;
                            $result_final = $rowesteb->result_final;
                            $html .= '<tr>
                                <td>'.$preincuba.'</td>
                                <td align="center">'.$dph.'</td>
                                <td align="center">'.$dbames35cp.'</td>
                                <td align="center">'.$dbames35cc.'</td>
                                <td align="center">'.$dbater55cp.'</td>
                                <td align="center">'.$dbater55cc.'</td>
                                <td align="center">'.$result_final.'</td>
                            </tr>';
                       }
                       $html .= '</table><br>';
                    }
                   /// RESULTADOS ESTERILIDAD ALTA ///
                    $parametrosEsterili = array(
                       '@cinternoordenservicio' => $cinternoordenservicio,
                       '@cmuestra' => $CMUESTRA,
                       '@idlabinformes' => $idlabinformes,
                       '@stipoacidez' => 'A'
                    );
                    $resestea = $this->minformes->getinfpersonalizado_resesteri($parametrosEsterili);
                    if ($resestea){
                       $html .= '<table id="tableborder">
                       <tr>
                            <td colspan="7"> Alta acidez ('.str_replace("<", "&lt;", "pH<4.6").')</td>
                       </tr>
                       <tr>
                           <th width="30%" rowspan="2"> <b>Preincubación 35°C/14 días</b> </th>
                           <th width="5%" rowspan="2"> <b>pH</b> </th>
                           <th width="25%" colspan="2"> <b>Mesófilos (35°C)</b> </th>
                           <th width="15%" colspan="2"> <b>Termófilos (55°C)</b> </th>
                           <th width="25%" rowspan="2"> <b>Resultado</b> </th>
                       </tr>
                       <tr>
                           <th> <b>Caldo ácido 96h</b> </th>
                           <th> <b>Caldo extracto de malta 96h</b> </th>
                           <th colspan="2"> <b>Caldo ácido 48h</b> </th>
                       </tr>';
                       foreach($resestea as $rowestea){
                            $preincuba = $rowestea->preincuba;
                            $dph = $rowestea->dph;
                            $dacmes30ca = $rowestea->dacmes30ca;
                            $dacmes30cm = $rowestea->dacmes30cm;
                            $dacter55ca = $rowestea->dacter55ca;
                            $result_final = $rowestea->result_final;
                            $html .= '<tr>
                                <td>'.$preincuba.'</td>
                                <td align="center">'.$dph.'</td>
                                <td align="center">'.$dacmes30ca.'</td>
                                <td align="center">'.$dacmes30cm.'</td>
                                <td align="center" colspan="2">'.$dacter55ca.'</td>
                                <td align="center">'.$result_final.'</td>
                            </tr>';
                        }
                        $html .= '</table><br>';
                    }                   

                   ///RESULTADOS ETIQUETADO NUTRICIONAL///
                    $parametrosEtiquetado = array(
                        '@cinternoordenservicio' => $cinternoordenservicio,
                        '@cmuestra' => $CMUESTRA,
                        '@idlabinformes' => $idlabinformes
                    );
                    $resetiqueta = $this->minformes->getinfpersonalizado_resetiquetado($parametrosEtiquetado);
                    if ($resetiqueta){
                      $html .= '<table id="tableborder">
                        <tr>
                            <td colspan="7"> <b>INFORMACION NUTRICIONAL</b> </td>
                        </tr>
                        <tr>
                            <td width="40%" colspan="4"> Tamaño de Porción :</td>
                            <td colspan="3">'.$nporcion.'</td>
                        </tr>
                        <tr>
                            <th width="25%" colspan="3"> <b>Ensayo</b> </th>
                            <th width="15%"> <b>Unidad</b> </th>
                            <th width="20%"> <b>100g</b> </th>
                            <th width="20%"> <b>Porción</b> </th>
                            <th width="20%"> <b>% VDR(*)</b> </th>
                        </tr>';
                        foreach($resetiqueta as $rowetiqueta){
                            $DENSAYO = $rowetiqueta->DENSAYO;
                            $UNIDADMEDIDA = $rowetiqueta->UNIDADMEDIDA;
                            $DRESULTADO = $rowetiqueta->DRESULTADO;
                            $NPORCION = $rowetiqueta->NPORCION;
                            $VALORVR = $rowetiqueta->VALORVR;
                            $html .= '<tr>
                                <td colspan="3">'.$DENSAYO.'</td>
                                <td align="center">'.$UNIDADMEDIDA.'</td>
                                <td align="center">'.$DRESULTADO.'</td>
                                <td align="center">'.$NPORCION.'</td>
                                <td align="center">'.$VALORVR.'</td>
                            </tr>';
                        }
                        $html .= '<tr><td colspan="7">(*) % Valor Diario (VD) con base a una dieta de 2000 kcal u 8370 kJ.  según Codex Alimentarius FAO/OMS. Sus valores diarios pueden ser mayores o menores dependiendo de sus necesidades energéticas.</tr>
                                   </table><br>';
                    }
                    
                   /// NOTAS ADICIONALES /
                    if($cinternoordenservicio !== '15734' && $vcmuestra !== 'M03' && $vcinforme !== '99'){
                        $resNOTA1 = $this->minformes->getinfpersonalizado_nota01($parametros);
                        if ($resNOTA1){
                            foreach($resNOTA1 as $rowNOTA1){
                                $NOTA01        = $rowNOTA1->NOTA01;
                            }
                        }
                        $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" >                        
                                    <tr>
                                        <td width="100%" align="left" style="font-size: 0.8em;">
                                            '.$DLCLAB.'   
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td width="100%" align="center" style="font-size: 0.8em;">
                                            '.$NOTA01.'   
                                        </td>
                                    </tr> 
                        </table> <br>';
                        
                       
                    }
                     $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" >
                                    <tr>
                                        <td width="100%" align="center" colspan="3" style="height:30px; font-size: 0.8em;">
                                            <b><u>METODOS DE ENSAYO</u></b>   
                                        </td>
                                    </tr>
                        </table>';
                   /// METODOS DE ENSAYO ///    
                     $resmetensa = $this->minformes->getmetodosensayospersonalizado($parametros);
                     if ($resmetensa){
                        $html .= '<table id="tableborder">
                        <thead>
                            <tr>
                                <th>
                                    <b>Ensayo</b>   
                                </th>
                                <th>
                                    <b>Norma o Referencia</b>    
                                </td>
                            </tr>
                            </thead>
                            <tbody>';
                            foreach($resmetensa as $rowmetensa){
                                $METDENSAYO = $rowmetensa->DENSAYO;
                                $METDNORMA = $rowmetensa->DNORMA;
                                $html .= '<tr>
                                <td width="180px">'.$METDENSAYO.'</td>
                                    <td>'.$METDNORMA.'</td>
                                </tr>';
                            }
                        $html .= '</tbody></table><br>';
                     }  
 
                   /// OBSERVACIONES /
                    if($SOBSERV > 0){
                        $html .= '<table id="tableborder" >
                          <tr>
                            <td width="100%" align="left" >
                                <b>Observaciones :</b>
                            </td>
                          </tr> 
                          <tr>
                            <td width="100%" align="center" >
                                <div style="text-align: justify;">'.$OBSERVACION.'</div>   
                            </td>
                          </tr> 
                        </table> <br>';
                     }
                   /* FECHA Y FIRMAS*/ 
                     $resFECHAFIRMA = $this->minformes->getinfpersonalizado_fechafirma($parametros);
                     if ($resFECHAFIRMA){
                         foreach($resFECHAFIRMA as $rowFECHAFIRMA){
                             $FECHA        = $rowFECHAFIRMA->FECHA;

                             $SFQ        = $rowFECHAFIRMA->SFQ;
                             $SMICRO        = $rowFECHAFIRMA->SMICRO;
                             $SINSTRU        = $rowFECHAFIRMA->SINSTRU;

                             $FIRMAFQS        = $rowFECHAFIRMA->FIRMAFQS;
                             $NOMBREFQS        = $rowFECHAFIRMA->NOMBREFQS;
                             $CARGOFQS        = $rowFECHAFIRMA->CARGOFQS;
                             $CODIGOFQS        = $rowFECHAFIRMA->CODIGOFQS;

                             $FIRMAMICRO        = $rowFECHAFIRMA->FIRMAMICRO;
                             $NOMBREMICRO        = $rowFECHAFIRMA->NOMBREMICRO;
                             $CARGOMICRO        = $rowFECHAFIRMA->CARGOMICRO;
                             $CODIGOMICRO        = $rowFECHAFIRMA->CODIGOMICRO;

                             $FIRMAINSTRU        = $rowFECHAFIRMA->FIRMAINSTRU;
                             $NOMBREINSTRU        = $rowFECHAFIRMA->NOMBREINSTRU;
                             $CARGOINSTRU        = $rowFECHAFIRMA->CARGOINSTRU;
                             $CODIGOINSTRU        = $rowFECHAFIRMA->CODIGOINSTRU;
                         }
                     }
                     if ($firma == 'N'){
                        if($SFQ == '1' and $SMICRO == '1' and $SINSTRU == '1'){
                            $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">                     
                                    <tr>
                                        <td width="100%" align="left" style="font-size: 0.8em;" colspan="2">
                                        Lima, '.$FECHA.'   
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td width="100%" align="center">
                                        <img src="'.public_url_ftp().'Imagenes/firmas/FIRMABLANCO.jpg" width="190" height="80" />    
                                        </td>
                                        <td width="100%" align="center">
                                        <img src="'.public_url_ftp().'Imagenes/firmas/FIRMABLANCO.jpg" width="190" height="80" />  
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%" align="center" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREFQS.'    
                                        </td>
                                        <td width="100%" align="center" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREMICRO.'  
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%" align="center" style="font-size: 0.8em;">
                                        '.$CARGOFQS.'
                                        </td>
                                        <td width="100%" align="center" style="font-size: 0.8em;">
                                        '.$CARGOMICRO.'
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%" align="center" style="font-size: 0.8em;">
                                        '.$CODIGOFQS.'
                                        </td>
                                        <td width="100%" align="center" style="font-size: 0.8em;">
                                        '.$CODIGOMICRO.'
                                        </td>
                                    </tr>

                                    <tr>
                                        <td width="100%" align="center" colspan="2">
                                        <br>   
                                        <br>  
                                        </td>
                                    </tr>

                                    <tr>
                                        <td width="100%" align="center" colspan="2">
                                        <img src="'.public_url_ftp().'Imagenes/firmas/FIRMABLANCO.jpg" width="190" height="80" />     
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREINSTRU.'     
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                        '.$CARGOINSTRU.'    
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                        '.$CODIGOINSTRU.'    
                                        </td>
                                    </tr>
                                </tr>
                            </table>';
                        }else if($SFQ == '1' and $SMICRO == '0' and $SINSTRU == '1'){
                            $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">              
                                    <tr>
                                        <td align="left" style="font-size: 0.8em;">
                                            Lima, '.$FECHA.'   
                                        </td>
                                    </tr> 
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td align="center">
                                            <img src="'.public_url_ftp().'Imagenes/firmas/FIRMABLANCO.jpg" width="190" height="80" />
                                            </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td align="center">
                                            <img src="'.public_url_ftp().'Imagenes/firmas/FIRMABLANCO.jpg" width="190" height="80" />
                                            </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td align="center" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREFQS.'    
                                        </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td align="center" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREINSTRU.'     
                                        </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td align="center" style="font-size: 0.8em;">
                                        '.$CARGOFQS.'
                                        </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td align="center" style="font-size: 0.8em;">
                                        '.$CARGOINSTRU.'    
                                        </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td align="center" style="font-size: 0.8em;">
                                        '.$CODIGOFQS.'
                                        </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td align="center" style="font-size: 0.8em;">
                                        '.$CODIGOINSTRU.'    
                                        </td>';
                                    }
                            $html .= '</tr>
                            </table>';
                        }else if($SFQ == '0' and $SMICRO == '1' and $SINSTRU == '1'){  
                            $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">              
                                    <tr>
                                        <td align="left" style="font-size: 0.8em;">
                                            Lima, '.$FECHA.'   
                                        </td>
                                    </tr> 
                                    <tr>';
                                    if($SMICRO == '1'){
                                        $html .= '<td align="center">
                                            <img src="'.public_url_ftp().'Imagenes/firmas/FIRMABLANCO.jpg" width="190" height="80" />
                                            </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td align="center" colspan="2">
                                            <img src="'.public_url_ftp().'Imagenes/firmas/FIRMABLANCO.jpg" width="190" height="80" />
                                            </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SMICRO == '1'){
                                        $html .= '<td align="center" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREMICRO.'  
                                        </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td align="center" colspan="2" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREINSTRU.'     
                                        </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SMICRO == '1'){
                                        $html .= '<td align="center" style="font-size: 0.8em;">
                                        '.$CARGOMICRO.'
                                        </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td align="center" colspan="2" style="font-size: 0.8em;">
                                        '.$CARGOINSTRU.'    
                                        </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SMICRO == '1'){
                                        $html .= '<td width="50%" align="center" style="font-size: 0.8em;">
                                        '.$CODIGOMICRO.'
                                        </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td width="50%" align="center" colspan="2" style="font-size: 0.8em;">
                                        '.$CODIGOINSTRU.'    
                                        </td>';
                                    }
                            $html .= '</tr>
                            </table>';   
                        }else if($SFQ == '1' and $SMICRO == '1' and $SINSTRU == '0'){
                            $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">              
                                    <tr>
                                        <td width="100%" align="left" style="font-size: 0.8em;">
                                            Lima, '.$FECHA.'   
                                        </td>
                                    </tr> 
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td width="50%" align="center">
                                            <img src="'.public_url_ftp().'Imagenes/firmas/FIRMABLANCO.jpg" width="190" height="80" />
                                            </td>';
                                    }
                                    if($SMICRO == '1'){
                                        $html .= '<td width="50%" align="center">
                                            <img src="'.public_url_ftp().'Imagenes/firmas/FIRMABLANCO.jpg" width="190" height="80" />
                                            </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td width="50%" align="center" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREFQS.'    
                                        </td>';
                                    }
                                    if($SMICRO == '1'){
                                        $html .= '<td width="50%" align="center" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREMICRO.'  
                                        </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td width="50%" align="center" style="font-size: 0.8em;">
                                        '.$CARGOFQS.'
                                        </td>';
                                    }
                                    if($SMICRO == '1'){
                                        $html .= '<td width="50%" align="center" style="font-size: 0.8em;">
                                        '.$CARGOMICRO.'
                                        </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td width="50%" align="center" style="font-size: 0.8em;">
                                        '.$CODIGOFQS.'
                                        </td>';
                                    }
                                    if($SMICRO == '1'){
                                        $html .= '<td width="50%" align="center" style="font-size: 0.8em;">
                                        '.$CODIGOMICRO.'
                                        </td>';
                                    }
                            $html .= '</tr>
                            </table>';
                        }else{
                            $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">              
                                    <tr>
                                        <td width="100%" align="left" style="font-size: 0.8em;">
                                            Lima, '.$FECHA.'   
                                        </td>
                                    </tr> 
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td width="100%" align="center">
                                            <img src="'.public_url_ftp().'Imagenes/firmas/FIRMABLANCO.jpg" width="190" height="80" />    
                                            </td>';
                                    }
                                    if($SMICRO == '1'){
                                        $html .= '<td width="100%" align="center">
                                            <img src="'.public_url_ftp().'Imagenes/firmas/FIRMABLANCO.jpg" width="190" height="80" />   
                                            </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td width="100%" align="center" colspan="2">
                                            <img src="'.public_url_ftp().'Imagenes/firmas/FIRMABLANCO.jpg" width="190" height="80" />     
                                            </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREFQS.'    
                                        </td>';
                                    }
                                    if($SMICRO == '1'){
                                        $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREMICRO.'  
                                        </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREINSTRU.'     
                                        </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                        '.$CARGOFQS.'
                                        </td>';
                                    }
                                    if($SMICRO == '1'){
                                        $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                        '.$CARGOMICRO.'
                                        </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                        '.$CARGOINSTRU.'    
                                        </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                        '.$CODIGOFQS.'
                                        </td>';
                                    }
                                    if($SMICRO == '1'){
                                        $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                        '.$CODIGOMICRO.'
                                        </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                        '.$CODIGOINSTRU.'    
                                        </td>';
                                    }
                            $html .= '</tr>
                            </table>';
                        }  
                     }else{                         
                        if($SFQ == '1' and $SMICRO == '1' and $SINSTRU == '1'){
                            $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">                     
                                    <tr>
                                        <td width="100%" align="left" style="font-size: 0.8em;" colspan="2">
                                        Lima, '.$FECHA.'   
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td width="100%" align="center">
                                        <img src="'.public_url_ftp().'Imagenes/firmas/FS000052.jpg" width="190" height="80" />    
                                        </td>
                                        <td width="100%" align="center">
                                        <img src="'.public_url_ftp().'Imagenes/firmas/FS000046.jpg" width="190" height="80" />   
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%" align="center" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREFQS.'    
                                        </td>
                                        <td width="100%" align="center" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREMICRO.'  
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%" align="center" style="font-size: 0.8em;">
                                        '.$CARGOFQS.'
                                        </td>
                                        <td width="100%" align="center" style="font-size: 0.8em;">
                                        '.$CARGOMICRO.'
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%" align="center" style="font-size: 0.8em;">
                                        '.$CODIGOFQS.'
                                        </td>
                                        <td width="100%" align="center" style="font-size: 0.8em;">
                                        '.$CODIGOMICRO.'
                                        </td>
                                    </tr>

                                    <tr>
                                        <td width="100%" align="center" colspan="2">
                                        <br>   
                                        <br>  
                                        </td>
                                    </tr>

                                    <tr>
                                        <td width="100%" align="center" colspan="2">
                                        <img src="'.public_url_ftp().'Imagenes/firmas/FS000120.jpg" width="190" height="80" />     
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREINSTRU.'     
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                        '.$CARGOINSTRU.'    
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                        '.$CODIGOINSTRU.'    
                                        </td>
                                    </tr>
                                </tr>
                            </table>';
                        }else if($SFQ == '1' and $SMICRO == '0' and $SINSTRU == '1'){
                            $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">              
                                    <tr>
                                        <td align="left" style="font-size: 0.8em;">
                                            Lima, '.$FECHA.'   
                                        </td>
                                    </tr> 
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td align="center">
                                            <img src="'.public_url_ftp().'Imagenes/firmas/FS000052.jpg" width="190" height="80" />    
                                            </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td align="center">
                                            <img src="'.public_url_ftp().'Imagenes/firmas/FS000120.jpg" width="190" height="80" />     
                                            </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td align="center" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREFQS.'    
                                        </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td align="center" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREINSTRU.'     
                                        </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td align="center" style="font-size: 0.8em;">
                                        '.$CARGOFQS.'
                                        </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td align="center" style="font-size: 0.8em;">
                                        '.$CARGOINSTRU.'    
                                        </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td align="center" style="font-size: 0.8em;">
                                        '.$CODIGOFQS.'
                                        </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td align="center" style="font-size: 0.8em;">
                                        '.$CODIGOINSTRU.'    
                                        </td>';
                                    }
                            $html .= '</tr>
                            </table>';
                        }else if($SFQ == '0' and $SMICRO == '1' and $SINSTRU == '1'){  
                            $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">              
                                    <tr>
                                        <td align="left" style="font-size: 0.8em;">
                                            Lima, '.$FECHA.'   
                                        </td>
                                    </tr> 
                                    <tr>';
                                    if($SMICRO == '1'){
                                        $html .= '<td align="center">
                                            <img src="'.public_url_ftp().'Imagenes/firmas/FS000046.jpg" width="190" height="80" />   
                                            </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td align="center" colspan="2">
                                            <img src="'.public_url_ftp().'Imagenes/firmas/FS000120.jpg" width="190" height="80" />     
                                            </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SMICRO == '1'){
                                        $html .= '<td align="center" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREMICRO.'  
                                        </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td align="center" colspan="2" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREINSTRU.'     
                                        </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SMICRO == '1'){
                                        $html .= '<td align="center" style="font-size: 0.8em;">
                                        '.$CARGOMICRO.'
                                        </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td align="center" colspan="2" style="font-size: 0.8em;">
                                        '.$CARGOINSTRU.'    
                                        </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SMICRO == '1'){
                                        $html .= '<td width="50%" align="center" style="font-size: 0.8em;">
                                        '.$CODIGOMICRO.'
                                        </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td width="50%" align="center" colspan="2" style="font-size: 0.8em;">
                                        '.$CODIGOINSTRU.'    
                                        </td>';
                                    }
                            $html .= '</tr>
                            </table>';   
                        }else if($SFQ == '1' and $SMICRO == '1' and $SINSTRU == '0'){
                            $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">              
                                    <tr>
                                        <td width="100%" align="left" style="font-size: 0.8em;">
                                            Lima, '.$FECHA.'   
                                        </td>
                                    </tr> 
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td width="50%" align="center">
                                            <img src="'.public_url_ftp().'Imagenes/firmas/FS000052.jpg" width="190" height="80" />    
                                            </td>';
                                    }
                                    if($SMICRO == '1'){
                                        $html .= '<td width="50%" align="center">
                                            <img src="'.public_url_ftp().'Imagenes/firmas/FS000046.jpg" width="190" height="80" />   
                                            </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td width="50%" align="center" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREFQS.'    
                                        </td>';
                                    }
                                    if($SMICRO == '1'){
                                        $html .= '<td width="50%" align="center" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREMICRO.'  
                                        </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td width="50%" align="center" style="font-size: 0.8em;">
                                        '.$CARGOFQS.'
                                        </td>';
                                    }
                                    if($SMICRO == '1'){
                                        $html .= '<td width="50%" align="center" style="font-size: 0.8em;">
                                        '.$CARGOMICRO.'
                                        </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td width="50%" align="center" style="font-size: 0.8em;">
                                        '.$CODIGOFQS.'
                                        </td>';
                                    }
                                    if($SMICRO == '1'){
                                        $html .= '<td width="50%" align="center" style="font-size: 0.8em;">
                                        '.$CODIGOMICRO.'
                                        </td>';
                                    }
                            $html .= '</tr>
                            </table>';
                        }else{
                            $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">              
                                    <tr>
                                        <td width="100%" align="left" style="font-size: 0.8em;">
                                            Lima, '.$FECHA.'   
                                        </td>
                                    </tr> 
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td width="100%" align="center">
                                            <img src="'.public_url_ftp().'Imagenes/firmas/FS000052.jpg" width="190" height="80" />    
                                            </td>';
                                    }
                                    if($SMICRO == '1'){
                                        $html .= '<td width="100%" align="center">
                                            <img src="'.public_url_ftp().'Imagenes/firmas/FS000046.jpg" width="190" height="80" />   
                                            </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td width="100%" align="center" colspan="2">
                                            <img src="'.public_url_ftp().'Imagenes/firmas/FS000120.jpg" width="190" height="80" />     
                                            </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREFQS.'    
                                        </td>';
                                    }
                                    if($SMICRO == '1'){
                                        $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREMICRO.'  
                                        </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                        <hr width=150>
                                        '.$NOMBREINSTRU.'     
                                        </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                        '.$CARGOFQS.'
                                        </td>';
                                    }
                                    if($SMICRO == '1'){
                                        $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                        '.$CARGOMICRO.'
                                        </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                        '.$CARGOINSTRU.'    
                                        </td>';
                                    }
                            $html .= '</tr>
                                    <tr>';
                                    if($SFQ == '1'){
                                        $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                        '.$CODIGOFQS.'
                                        </td>';
                                    }
                                    if($SMICRO == '1'){
                                        $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                        '.$CODIGOMICRO.'
                                        </td>';
                                    }
                                    if($SINSTRU == '1'){
                                        $html .= '<td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                        '.$CODIGOINSTRU.'    
                                        </td>';
                                    }
                            $html .= '</tr>
                            </table>';
                        }  
                     }
                     
                    
            $html .= '</div>';
                            $pos++;
                    }}
            $html .= '</body>
                    </html>';
            $filename = 'IE '.substr($NROINFORME, 0, 9).' '.$CLIENTE.' '.$CMUESTRA;
             $this->pdfgenerator->generate($html, $filename, TRUE, 'A4', 'portrait');
            //echo $html;
        }catch(Exception $e){
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        }
        //echo $html;
    }
    public function pdfCertificadoPersonalizado($cinternoordenservicio,$vcmuestra,$vcinforme) { // recupera 
        $this->load->library('pdfgenerator');

        $html = '<html>
                <head>
                    <title>Certificado</title>
                    <style type="text/css">
                        @page{
                             margin: 0.3in 0.3in 0.3in 0.3in;
                        }
                        body{
                            font-family: Arial, Helvetica, sans-serif;
                            margin-top: 3.5cm;
                            margin-left: 1cm;
                            margin-right: 1cm;
                            margin-bottom: 2cm;
                        }
                        #header,
                        #footer,
                        #nroinf {
                            position: fixed;
                            left: 0;
                            right: 0;
                            color: #000000;
                            font-size: 0.9em;
                        }                        
                        #header {
                            top: 0;
                            height: 4cm;
                        }    
                        #nroinf {
                            top: 2.5cm;
                            height: 2cm;
                        }                     
                        #footer {
                            bottom: 0;
                            border-top: 0.1pt solid #aaa;
                            height: 1cm;
                        }                        
                        #header table,
                        #footer table,
                        #nroinf table {
                            width: 100%;
                            border-collapse: collapse;
                            border: none;
                        }                        
                        #header td,
                        #footer td,
                        #nroinf td {
                            padding: 0;
                        }
                        .page-number{
                            text-align: right;
                        }
                        .page-number:before{
                            content: "Pagina " counter(page);
                        }
                        hr1{
                            page-break-before: always;
                            color: #ffffff;
                            margin: -2px;
                        }     
                        #caratula td{
                            font-size:0.8em;        
                        }
                        #tableborder{
                            border-collapse: collapse;
                            width: 100%;
                            font-size:0.8em;
                            border: 0.5pt solid black;
                        }
                        #tableborder td{
                            border: 0.5pt solid black;
                            padding-left: 5px;
                        }
                        #tableborder th{
                            text-align: center;
                            border: 0.5pt solid black;
                            background-color: #DBD7D7;
                        }
                        .list-unstyled{
                            padding-left: 0;
                            list-style: none;
                        }
                    </style>
                </head>
                <body> 
                <div id="header">
                     <table width="100%" align="center">
                         <tr>
                             <td width="25%" align="center">                            
                                 <img src="'.public_url_ftp().'Imagenes/formatos/2/logoFSC.jpg" width="180" height="95" />  
                             </td>   
                             <td width="100%" align="right" colspan="2" style="font-size: 0.8em;">
                                 <ul class="list-unstyled">
                                     <li>Jr. Monterrey N° 221 Of. 201-202</li>
                                     <li>Urb. Chacarilla del Estanque. Santiago de Surco, Lima - Perú</li>
                                     <li>Teléfonos: (51-1) 480 0561 - Anexo: Lab 111</li>
                                     <li>www.fscertificaciones.com</li>
                                 </ul>
                             </td>
                         </tr>
                     </table>
                </div>   
                <div id="footer">
                     <table width="100%" style="font-size:0.8em; margin-top: 0.3cm; text-align: justify">
                     <tr>
                         <td width="100%" align="right" >
                             <div class="page-number"></div>
                         </td>
                     </tr>
                     </table>                    
                </div>'; 
                $parametroscabe = array(
                    '@cinternoordenservicio'         => $cinternoordenservicio,
                    '@cmuestra'       => $vcmuestra,
                    '@idlabinformes'       => $vcinforme,
                );
                $res = $this->minformes->getcertipersonalizado_caratula($parametroscabe);
                $pos = 0;
                if ($res){
                    foreach($res as $row){ 
                        $NROINFORME     = $row->NROINFORME;
                        $NROCERTI       = $row->NROCERTI;
                        $CLIENTE        = $row->CLIENTE;
                        $DIRECCION      = $row->DIRECCION;
                        $NROORDEN       = $row->NROORDEN;
                        $PROCEDENCIA    = $row->PROCEDENCIA;
                        $FMUESTRA       = $row->FMUESTRA;
                        $FRECEPCION     = $row->FRECEPCION;
                        $FANALISIS      = $row->FANALISIS;
                        $LUGARMUESTRA   = $row->LUGARMUESTRA;
                        $CMUESTRA       = $row->CMUESTRA;
                        $DPRODUCTO      = $row->DPRODUCTO;
                        $DTEMPERATURA   = $row->DTEMPERATURA;
                        $DLCLAB         = $row->DLCLAB;
                        $OBSERVACION    = $row->OBSERVACION;
                        $SOBSERV        = $row->SOBSERV;
                        $ACNAC          = $row->ACNAC;
                        $DDATOSADICICONCLU = $row->DDATOSADICICONCLU;
                        $DREALPRODUCTO = $row->DREALPRODUCTO;
                        $nporcion       = $row->nporcion;
                        $idlabinformes       = $row->idlabinformes;

                        /*if($pos > 0){
                            $html .= '<hr style="page-break-before: always;" size="0"/>
                            <table width="100%" align="center">
                            <tr>
                                <td colspan="3" style="font-size:1.1em;" align="center">
                                    <b>INFORME DE ENSAYO BORRADOR </b>
                                </td>
                            </tr>
                            </table>';                      
                        }else{
                            $html .= '<hr size="0"/>
                            <table width="100%" align="center">
                            <tr>
                                <td colspan="3" style="font-size:1.1em;" align="center">
                                    <b>INFORME DE ENSAYO BORRADOR </b>
                                </td>
                            </tr>
                            </table>';
                        }*/

                        if($pos > 0){
                            $html .= '<hr style="page-break-before: always;" size="0" noshade="noshade" />';                          
                        }  
                        
                        $parametros = array(
                            '@cinternoordenservicio'    => $cinternoordenservicio,
                            '@cmuestra'                 => $CMUESTRA,
                            '@idlabinformes'                 => $idlabinformes,
                        );
                                             
                        $html .= '   <div id="header">
                                    <table width="100%" align="center">   
                                    <tr>
                                        <td width="25%" align="center">                            
                                            &nbsp;  
                                        </td>   
                                        <td width="100%" align="right" colspan="2" style="font-size: 0.8em;">
                                            <ul class="list-unstyled">
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                            </ul>
                                        </td>
                                    </tr>                                 
                                    <tr>
                                        <td colspan="3" style="height:40px; font-size:1.1em;" align="center">
                                            <b>CERTIFICADO DE CALIDAD N° '.$NROCERTI.'  </b>
                                        </td>
                                    </tr>
                                    </table>
                                </div>';
                       
                        $html .= '<div id="main">
                                           
                                    <table id="caratula" width="100%" align="center" cellspacing="0" cellpadding="2">  
                                        <tr>
                                            <td> 
                                                <b>1.- </b>  
                                            </td>
                                            <td colspan="3" align="left">
                                                <b> DATOS GENERALES</b>   
                                            </td>
                                        </tr>                       
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>Nombre del Cliente</b>   
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$CLIENTE.'   
                                            </td>
                                        </tr>
                                        <tr style="vertical-align:top">
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>Dirección del Cliente</b>  
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$DIRECCION.'   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>N° Orden de Trabajo</b>   
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$NROORDEN.'   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>Procedencia de la Muestra</b>  
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$PROCEDENCIA.'   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>Fecha de Muestreo</b>    
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$FMUESTRA.'   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>Fecha de Recepción</b>    
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$FRECEPCION.'<   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>Fecha de Análisis</b>    
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$FANALISIS.'  
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>Lugar de Muestreo</b>   
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$LUGARMUESTRA.'  
                                            </td>
                                        </tr>
                                        <tr style="vertical-align:top">
                                            <td width="3%" align="left">   
                                            </td>
                                            <td width="27%" align="left">
                                                <b>Muestra / Descripción</b>    
                                            </td>
                                            <td width="7%" align="left">
                                                : '.$CMUESTRA.'   
                                            </td>
                                            <td width="63%" align="left">
                                                '.$DPRODUCTO.'   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>Temperatura de Recepción</b>    
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$DTEMPERATURA.'   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>N° Informe de Ensayo</b>   
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$NROINFORME.'   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="4">
                                            &nbsp;   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td> 
                                                <b>2.- </b>  
                                            </td>
                                            <td align="left" colspan="3" style="height:30px;">
                                                <b>DOCUMENTOS NORMATIVOS</b>   
                                            </td>
                                        </tr>';
                                        $resdocnorma = $this->minformes->getdocnormativo($cinternoordenservicio,$CMUESTRA);
                                        $idocnorma = 1;
                                        if ($resdocnorma){
                                            foreach($resdocnorma as $rowdocnorma){
                                                $cnormalab = $rowdocnorma->cnormalab;
                                                $cgruponormalab = $rowdocnorma->cgruponormalab;
                                                $dnormalab = $rowdocnorma->dnormalab;
                                                $html .= '<tr style="vertical-align:top">
                                                   <td>2.'.$idocnorma.'</td>
                                                   <td colspan="3">'.$dnormalab.'</td>
                                                </tr>';
                                                $idocnorma ++ ;
                                                $resdocnorma_det = $this->minformes->getdocnormativo_det($cnormalab,$cgruponormalab);
                                                if ($resdocnorma_det){
                                                    foreach($resdocnorma_det as $rowdocnorma_det){
                                                        $dnumerogrupo = $rowdocnorma_det->dnumerogrupo;
                                                        $dnombregrupo = $rowdocnorma_det->dnombregrupo;
                                                        if($dnumerogrupo <> '0'){
                                                            $html .= '<tr>
                                                            <td></td>
                                                            <td colspan="3">'.$dnumerogrupo.' &nbsp; &nbsp; '.$dnombregrupo.'</td>
                                                            </tr>';
                                                        }
                                                    }
                                                } 
                                            }
                                        } 
                                        $html .= '
                                        <tr>
                                           <td > 
                                               <b>3.- </b>  
                                           </td>
                                            <td align="left" colspan="3" style="height:50px;">
                                                <b>METODOS DE ENSAYO</b>   
                                            </td>
                                        </tr>
                                    </table>';
                                     
                                    /*RESULTADOS METODOS DE ENSAYO*/    
                                     $resmetensa = $this->minformes->getmetodosensayospersonalizado($parametros);
                                      if ($resmetensa){
                                        $html .= '<table id="tableborder" style="padding-left: 15px;">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <b>Ensayo</b>   
                                                </th>
                                                <th>
                                                    <b>Norma o Referencia</b>    
                                                </td>
                                            </tr>
                                            </thead>
                                            <tbody>';
                                            foreach($resmetensa as $rowmetensa){
                                                $METDENSAYO = $rowmetensa->DENSAYO;
                                                $METDNORMA = $rowmetensa->DNORMA;
                                                $html .= '<tr>
                                                   <td width="180px">'.$METDENSAYO.'</td>
                                                   <td style="vertical-align:top">'.$METDNORMA.'</td>
                                                </tr>';
                                            }
                                        $html .= '</tbody></table><br>';
                                     }  
                
                                     $html .= '<table id="caratula" width="713px" align="center" cellspacing="0" cellpadding="2" >
                                       <tr>
                                           <td width="3%"> 
                                               <b>4.- </b>  
                                           </td>
                                           <td width="97%" align="left" colspan="3" style="height:50px;">
                                               <b>RESULTADOS E INTERPRETACIONES</b>   
                                           </td>
                                       </tr>
                                     </table>'; 
                
                                    /*RESULTADOS MICROBIOLOGIA*/
                                     $resmicro = $this->minformes->getinfpersonalizado_resmicro($parametros);
                                     if ($resmicro){
                                         $html .= '<table id="tableborder">
                                         <thead>
                                         <tr>
                                             <th> <b>Ensayo</b> </th>
                                             <th> <b>Unidades</b> </th>
                                             <th> <b>Limite permisible</b> </th>
                                             <th> <b>Resultado</b> </th>
                                             <th> <b>Conclusión</b> </th>
                                         </tr>
                                         </thead>
                                         <tbody>';
                                         $DENSAYOmicro = '';
                                         foreach($resmicro as $rowmicro){
                                             $DENSAYO = $rowmicro->NOMBENSAYO;
                                             $UNIDADMEDIDA = $rowmicro->UNIDADMEDIDA;
                                             $ESP_FINAL = $rowmicro->ESP_FINAL;
                                             $RESULT_FINAL = $rowmicro->RESULT_FINAL;
                                             $VALRESULTADO = $rowmicro->VALRESULTADO;
                                             $EXP10RES = $rowmicro->EXP10RES;
                                             $EXP10ESP = $rowmicro->EXP10ESP;

                                             $html .= '<tr>';
                                             if ($DENSAYO == $DENSAYOmicro){
                                                $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                                             }else{
                                                $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                                             }
                                             
                                             $html .= '<td width="100px" align="center">'.$UNIDADMEDIDA.'</td>
                                                 <td width="100px" align="center">'.str_replace("<", "&lt;", $ESP_FINAL).'<sup>'.$EXP10ESP.'</sup></td>
                                                 <td width="140px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'<sup>'.$EXP10RES.'</sup></td>
                                                 <td width="100px" align="center">'.$VALRESULTADO.'</td>
                                             </tr>';
                                             $DENSAYOmicro = $DENSAYO;
                                         }
                                         $html .= '</tbody></table><br>';
                                     }  
                                    /*RESULTADOS FISICOQUIMICO*/
                                     $resfq = $this->minformes->getinfpersonalizado_resfq($parametros);
                                     if ($resfq){
                                         $html .= '<table id="tableborder">
                                         <thead>
                                         <tr>
                                             <th> <b>Ensayo</b> </th>
                                             <th> <b>Unidades</b> </th>
                                             <th> <b>Limite permisible</b> </th>
                                             <th> <b>Resultado</b> </th>
                                             <th> <b>Conclusión</b> </th>
                                         </tr>
                                         </thead>
                                         <tbody>';
                                         $DENSAYOfq = '';
                                         foreach($resfq as $rowfq){
                                             $DENSAYO = $rowfq->NOMBENSAYO;
                                             $UNIDADMEDIDA = $rowfq->UNIDADMEDIDA;
                                             $ESP_FINAL = $rowfq->ESP_FINAL;
                                             $RESULT_FINAL = $rowfq->RESULT_FINAL;
                                             $VALRESULTADO = $rowfq->VALRESULTADO;
                                             $EXP10ESP = $rowfq->EXP10ESP;
                                             $EXP10RES = $rowfq->EXP10RES;

                                             $html .= '<tr>';
                                             if ($DENSAYO == $DENSAYOfq){
                                                $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                                             }else{
                                                $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                                             }
                
                                             $html .= '<td width="100px" align="center">'.$UNIDADMEDIDA.'</td>
                                                 <td width="100px" align="center">'.str_replace("<", "&lt;", $ESP_FINAL).'<sup>'.$EXP10ESP.'</sup></td>
                                                 <td width="140px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'<sup>'.$EXP10RES.'</sup></td>
                                                 <td width="100px" align="center">'.$VALRESULTADO.'</td>
                                             </tr>';
                                             $DENSAYOfq = $DENSAYO;
                                         }
                                         $html .= '</tbody></table><br>';
                                     }
                                    ///RESULTADOS INSTRUMENTAL TERCERO///
                                       $resins = $this->minformes->getinfpersonalizado_resinstru($parametros);
                                       if ($resins){
                                           $html .= '<table id="tableborder">
                                           <thead>
                                           <tr>
                                                <th> <b>Ensayo</b> </th>
                                                <th> <b>Unidades</b> </th>
                                                <th> <b>Limite permisible</b> </th>
                                                <th> <b>Resultado</b> </th>
                                                <th> <b>Conclusión</b> </th>
                                           </tr>
                                           </thead>
                                           <tbody>';
                                           $DENSAYOins = '';
                                           foreach($resins as $rowins){
                                               $DENSAYO = $rowins->DENSAYO;
                                               $UNIDADMEDIDA = $rowins->UNIDADMEDIDA;
                                               $ESP_FINAL = $rowins->ESP_FINAL;
                                               $RESULT_FINAL = $rowins->RESULT_FINAL;
                                               $VALRESULTADO = $rowins->VALRESULTADO;
                                               $EXP10ESP = $rowins->EXP10ESP;
                                               $EXP10RES = $rowins->EXP10RES;
                                               $TIPOPRODUCTO = $rowins->TIPOPRODUCTO;
                  
                                               $nota01 = '';                                               
                                               if ($TIPOPRODUCTO == '1'){
                                                $nota01 = '<small style="font-size: 10px;">(*)Fuente: Art. 4 , Decreto Supremo N°017-2017-SA Decreto Supremo N°012-2018 -SA. "Manual de Advertencias Publicitarias en el marco de los establecido en la Ley N° 30021, Ley de Promoción de la alimentación saludable para niños, niñas y adolescentes, y su Reglamento aprobado por Decreto Supremo N°017-2017-SA, Decreto Supremo N°033-2016 SA. "Reglamento que establece el proceso reducción gradual hasta le eliminación de las grasas trans en los alimentos y bebidas no alcohólicas procesados industrialmente.</small>';
                                               }                                               

                                               $html .= '<tr>';
                                               if ($DENSAYO == $DENSAYOins){
                                                  $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                                               }else{
                                                  $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                                               }
                  
                                               $html .= '<td width="90px" align="center">'.$UNIDADMEDIDA.'</td>
                                                   <td width="100px" align="center">'.str_replace("<", "&lt;", $ESP_FINAL).'<sup>'.$EXP10ESP.'</sup></td>
                                                   <td width="120px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'<sup>'.$EXP10RES.'</sup></td>
                                                   <td width="140px" align="center">'.$VALRESULTADO.'</td>
                                               </tr>';
                                               $DENSAYOins = $DENSAYO;
                                           }
                                           $html .= '</tbody></table><br>'.$nota01;
                                       }
                                    /*RESULTADOS SENSORIAL*/
                                     $ressen = $this->minformes->getinfpersonalizado_ressenso($parametros);
                                     if ($ressen){
                                         $html .= '<table id="tableborder" style="page-break-inside: avoid;">
                                         <thead>
                                         <tr>
                                             <th> <b>Ensayo</b> </th>
                                             <th> <b>Atributo</b> </th>
                                             <th> <b>Limite permisible</b> </th>
                                             <th> <b>Resultado</b> </th>
                                             <th> <b>Conclusión</b> </th>
                                         </tr>
                                         </thead>
                                         <tbody>';
                                         $DENSAYOsen = '';
                                         foreach($ressen as $rowsen){
                                             $DENSAYO = $rowsen->DENSAYO;
                                             $DNOMBREESCALA = $rowsen->DNOMBREESCALA;
                                             $DLIMITE = $rowsen->DLIMITE;
                                             $RESULT_FINAL = $rowsen->RESULT_FINAL;
                                             $SCONCLUSION = $rowsen->SCONCLUSION;
                                             $html .= '<tr style="vertical-align:top">';
                                             if ($DENSAYO == $DENSAYOsen){
                                                $html .= '';
                                             }else{
                                                $html .= '<td rowspan="5" style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                                             }
                
                                             $html .= '<td width="70px">'.$DNOMBREESCALA.'</td>
                                                 <td width="160px" align="center">'.$DLIMITE.'</td>
                                                 <td width="170px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'</td>
                                                 <td width="100px" align="center">'.$SCONCLUSION.'</td>
                                             </tr>';
                                             $DENSAYOsen = $DENSAYO;
                                         }
                                         $html .= '</tbody></table><br>';
                                     }
                                     
                                    /*RESULTADOS CON ELEMENTOS*/
                                     $resele = $this->minformes->getinfpersonalizado_reselementos($parametros);
                                     if ($resele){
                                        $html .= '<table id="tableborder">
                                        <thead>
                                        <tr>
                                            <th> <b>Ensayo</b> </th>
                                            <th> <b>Elemento</b> </th>
                                            <th> <b>Unidades</b> </th>
                                            <th> <b>Limite permisible</b> </th>
                                            <th> <b>Resultado</b> </th>
                                            <th> <b>Conclusión</b> </th>
                                        </tr>
                                        </thead>
                                        <tbody>';
                                        $DENSAYOele = '';
                                        foreach($resele as $rowele){
                                            $DENSAYO = $rowele->NOMBENSAYO;
                                            $ELEMENTO = $rowele->ELEMENTO;
                                            $UNIDADMEDIDA = $rowele->UNIDADMEDIDA;
                                            $LIMITE_FINAL = $rowele->LIMITE_FINAL;
                                            $RESULT_FINAL = $rowele->RESULT_FINAL;
                                            $VALRESULTADO = $rowele->VALRESULTADO;
                                            $html .= '<tr>';
                                            if ($DENSAYO == $DENSAYOele){
                                               $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                                            }else{
                                               $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                                            }
                
                                            $html .= '<td>'.$ELEMENTO.'</td>
                                                <td width="100px" align="center">'.$UNIDADMEDIDA.'</td>
                                                <td width="120px" align="center">'.str_replace("<", "&lt;", $LIMITE_FINAL).'</td>
                                                <td width="110px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'</td>
                                                <td width="80px" align="center">'.$VALRESULTADO.'</td>
                                            </tr>';
                                            $DENSAYOele = $DENSAYO;
                                        }
                                        $html .= '</tbody></table><br>';
                                     }
                                    /*RESULTADOS ESTERILIDAD BAJA*/
                                     $parametrosEsterili = array(
                                        '@cinternoordenservicio' => $cinternoordenservicio,
                                        '@cmuestra' => $CMUESTRA,
                                        '@idlabinformes' => $idlabinformes,
                                        '@stipoacidez' => 'B'
                                     );
                                     $resesteb = $this->minformes->getinfpersonalizado_resesteri($parametrosEsterili);
                                     if ($resesteb){
                                        $html .= '<table id="tableborder">
                                        <tr>
                                            <td colspan="7"> Baja acidez (pH>4.6)</td>
                                        </tr>
                                        <tr>
                                            <th width="30%" rowspan="2"> <b>Preincubación 35°C/14 días</b> </th>
                                            <th width="5%" rowspan="2"> <b>pH</b> </th>
                                            <th width="25%" colspan="2"> <b>Mesófilos (35°C)</b> </th>
                                            <th width="15%" colspan="2"> <b>Termófilos (55°C)</b> </th>
                                            <th width="25%" rowspan="2"> <b>Resultado</b> </th>
                                        </tr>
                                        <tr>
                                            <th> <b>Caldo púrpura de bromocreasol 120h</b> </th>
                                            <th> <b>Caldo carne cocida 120h</b> </th>
                                            <th> <b>Caldo púrpura de bromocreasol 48h</b> </th>
                                            <th> <b>Caldo carne cocida 72h</b> </th>
                                        </tr>';
                                        foreach($resesteb as $rowesteb){
                                            $preincuba = $rowesteb->preincuba;
                                            $dph = $rowesteb->dph;
                                            $dbames35cp = $rowesteb->dbames35cp;
                                            $dbames35cc = $rowesteb->dbames35cc;
                                            $dbater55cp = $rowesteb->dbater55cp;
                                            $dbater55cc = $rowesteb->dbater55cc;
                                            $result_final = $rowesteb->result_final;
                                            $html .= '<tr>
                                                <td>'.$preincuba.'</td>
                                                <td align="center">'.$dph.'</td>
                                                <td align="center">'.$dbames35cp.'</td>
                                                <td align="center">'.$dbames35cc.'</td>
                                                <td align="center">'.$dbater55cp.'</td>
                                                <td align="center">'.$dbater55cc.'</td>
                                                <td align="center">'.$result_final.'</td>
                                            </tr>';
                                        }
                                        $html .= '</table><br>';
                                     }
                                    /*RESULTADOS ESTERILIDAD ALTA*/
                                     $parametrosEsterili = array(
                                        '@cinternoordenservicio' => $cinternoordenservicio,
                                        '@cmuestra' => $CMUESTRA,
                                        '@idlabinformes' => $idlabinformes,
                                        '@stipoacidez' => 'A'
                                     );
                                     $resestea = $this->minformes->getinfpersonalizado_resesteri($parametrosEsterili);
                                     if ($resestea){
                                        $html .= '<table id="tableborder">
                                        <tr>
                                            <td colspan="7"> Alta acidez ('.str_replace("<", "&lt;", "pH<4.6").')</td>
                                        </tr>
                                        <tr>
                                            <th width="30%" rowspan="2"> <b>Preincubación 35°C/14 días</b> </th>
                                            <th width="5%" rowspan="2"> <b>pH</b> </th>
                                            <th width="25%" colspan="2"> <b>Mesófilos (35°C)</b> </th>
                                            <th width="15%" colspan="2"> <b>Termófilos (55°C)</b> </th>
                                            <th width="25%" rowspan="2"> <b>Resultado</b> </th>
                                        </tr>
                                        <tr>
                                            <th> <b>Caldo ácido 96h</b> </th>
                                            <th> <b>Caldo extracto de malta 96h</b> </th>
                                            <th colspan="2"> <b>Caldo ácido 48h</b> </th>
                                        </tr>';
                                        foreach($resestea as $rowestea){
                                            $preincuba = $rowestea->preincuba;
                                            $dph = $rowestea->dph;
                                            $dacmes30ca = $rowestea->dacmes30ca;
                                            $dacmes30cm = $rowestea->dacmes30cm;
                                            $dacter55ca = $rowestea->dacter55ca;
                                            $result_final = $rowestea->result_final;
                                            $html .= '<tr>
                                                <td>'.$preincuba.'</td>
                                                <td align="center">'.$dph.'</td>
                                                <td align="center">'.$dacmes30ca.'</td>
                                                <td align="center">'.$dacmes30cm.'</td>
                                                <td align="center" colspan="2">'.$dacter55ca.'</td>
                                                <td align="center">'.$result_final.'</td>
                                            </tr>';
                                        }
                                        $html .= '</table><br>';
                                     }

                                    ///RESULTADOS ETIQUETADO NUTRICIONAL///
                                       $parametrosEtiquetado = array(
                                           '@cinternoordenservicio' => $cinternoordenservicio,
                                           '@cmuestra' => $CMUESTRA,
                                           '@idlabinformes' => $idlabinformes,
                                       );
                                       $resetiqueta = $this->minformes->getinfpersonalizado_resetiquetado($parametrosEtiquetado);
                                       if ($resetiqueta){
                                         $html .= '<table id="tableborder">
                                           <tr>
                                               <td colspan="7"> <b>INFORMACION NUTRICIONAL</b> </td>
                                           </tr>
                                           <tr>
                                               <td width="40%" colspan="4"> Tamaño de Porción :</td>
                                               <td colspan="3">14g</td>
                                           </tr>
                                           <tr>
                                               <th width="25%" colspan="3"> <b>Ensayo</b> </th>
                                               <th width="15%"> <b>Unidad</b> </th>
                                               <th width="20%"> <b>100g</b> </th>
                                               <th width="20%"> <b>Porción</b> </th>
                                               <th width="20%"> <b>% VDR(*)</b> </th>
                                           </tr>';
                                           foreach($resetiqueta as $rowetiqueta){
                                               $DENSAYO = $rowetiqueta->DENSAYO;
                                               $UNIDADMEDIDA = $rowetiqueta->UNIDADMEDIDA;
                                               $DRESULTADO = $rowetiqueta->DRESULTADO;
                                               $NPORCION = $rowetiqueta->NPORCION;
                                               $VALORVR = $rowetiqueta->VALORVR;
                                               $html .= '<tr>
                                                   <td colspan="3">'.$DENSAYO.'</td>
                                                   <td align="center">'.$UNIDADMEDIDA.'</td>
                                                   <td align="center">'.$DRESULTADO.'</td>
                                                   <td align="center">'.number_format($NPORCION, 3, '.', '').'</td>
                                                   <td align="center">'.number_format($VALORVR, 3, '.', '').'</td>
                                               </tr>';
                                           }
                                           $html .= '<tr><td colspan="7">(*) % Valor Diario (VD) con base a una dieta de 2000 kcal u 8370 kJ.  según Codex Alimentarius FAO/OMS. Sus valores diarios pueden ser mayores o menores dependiendo de sus necesidades energéticas.</tr>
                                                      </table><br>';
                                       }
                                    
                                 /*CONCLUSIONES */     
                                  $html .= '<table id="caratula" width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">
                                     <tr>
                                         <td width="3%"> 
                                             <b>5.- </b>  
                                         </td>
                                         <td width="97%" align="left" colspan="3" style="height:30px;">
                                             <b>CONCLUSIONES</b>   
                                         </td>                         
                                     </tr>';
                                      
                                     $parametrosconclu = array(
                                        '@cinternoordenservicio'         => $cinternoordenservicio,
                                        '@cmuestra'       => $CMUESTRA,
                                        '@DDATOSADICICONCLU'       => $DDATOSADICICONCLU,
                                        '@DREALPRODUCTO'       => $DREALPRODUCTO,
                                     );
                                     $resconclu = $this->minformes->getcertixmuestras_conclusion($parametrosconclu);
                                     if ($resconclu){
                                         foreach($resconclu as $rowconclu){
                                             $DCONCLUSION = $rowconclu->DCONCLUSION;
                                             $html .= '<tr>
                                                <td></td>
                                                <td colspan="3">'.$DCONCLUSION.'</td>
                                             </tr>';
                                         }
                                    }
                                    
                                  $html .= '</table><br>'; 
                                /* FECHA Y FIRMAS*/ 
                                $resFECHAFIRMA = $this->minformes->getinfpersonalizado_fechafirma($parametros);
                                if ($resFECHAFIRMA){
                                    foreach($resFECHAFIRMA as $rowFECHAFIRMA){
                                        $FECHA        = $rowFECHAFIRMA->FECHA;

                                        $SFQ        = $rowFECHAFIRMA->SFQ;
                                        $SMICRO        = $rowFECHAFIRMA->SMICRO;
                                        $SINSTRU        = $rowFECHAFIRMA->SINSTRU;

                                        $FIRMAFQS        = $rowFECHAFIRMA->FIRMAFQS;
                                        $NOMBREFQS        = $rowFECHAFIRMA->NOMBREFQS;
                                        $CARGOFQS        = $rowFECHAFIRMA->CARGOFQS;
                                        $CODIGOFQS        = $rowFECHAFIRMA->CODIGOFQS;

                                        $FIRMAMICRO        = $rowFECHAFIRMA->FIRMAMICRO;
                                        $NOMBREMICRO        = $rowFECHAFIRMA->NOMBREMICRO;
                                        $CARGOMICRO        = $rowFECHAFIRMA->CARGOMICRO;
                                        $CODIGOMICRO        = $rowFECHAFIRMA->CODIGOMICRO;

                                        $FIRMAINSTRU        = $rowFECHAFIRMA->FIRMAINSTRU;
                                        $NOMBREINSTRU        = $rowFECHAFIRMA->NOMBREINSTRU;
                                        $CARGOINSTRU        = $rowFECHAFIRMA->CARGOINSTRU;
                                        $CODIGOINSTRU        = $rowFECHAFIRMA->CODIGOINSTRU;
                                    }
                                }
                                
                                if($SFQ == '1' and $SMICRO == '1' and $SINSTRU == '1'){
                                    $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">                     
                                            <tr>
                                                <td width="100%" align="left" style="font-size: 0.8em;" colspan="2">
                                                Lima, '.$FECHA.'   
                                                </td>
                                            </tr> 
                                            <tr>
                                                <td width="100%" align="center">
                                                <img src="'.public_url_ftp().'Imagenes/firmas/FS000052.jpg" width="190" height="80" />    
                                                </td>
                                                <td width="100%" align="center">
                                                <img src="'.public_url_ftp().'Imagenes/firmas/FS000046.jpg" width="190" height="80" />   
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" align="center" style="font-size: 0.8em;">
                                                <hr width=150>
                                                '.$NOMBREFQS.'    
                                                </td>
                                                <td width="100%" align="center" style="font-size: 0.8em;">
                                                <hr width=150>
                                                '.$NOMBREMICRO.'  
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" align="center" style="font-size: 0.8em;">
                                                '.$CARGOFQS.'
                                                </td>
                                                <td width="100%" align="center" style="font-size: 0.8em;">
                                                '.$CARGOMICRO.'
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" align="center" style="font-size: 0.8em;">
                                                '.$CODIGOFQS.'
                                                </td>
                                                <td width="100%" align="center" style="font-size: 0.8em;">
                                                '.$CODIGOMICRO.'
                                                </td>
                                            </tr>

                                            <tr>
                                                <td width="100%" align="center" colspan="2">
                                                <br>   
                                                <br>  
                                                </td>
                                            </tr>

                                            <tr>
                                                <td width="100%" align="center" colspan="2">
                                                <img src="'.public_url_ftp().'Imagenes/firmas/FS000120.jpg" width="190" height="80" />     
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                                <hr width=150>
                                                '.$NOMBREINSTRU.'     
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                                '.$CARGOINSTRU.'    
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                                '.$CODIGOINSTRU.'    
                                                </td>
                                            </tr>
                                        </tr>
                                    </table>';
                                }else{
                                    $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">              
                                            <tr>
                                                <td width="100%" align="left" style="font-size: 0.8em;">
                                                    Lima, '.$FECHA.'   
                                                </td>
                                            </tr> 
                                            <tr>';
                                            if($SFQ == '1'){
                                                $html .= '<td width="100%" align="center">
                                                    <img src="'.public_url_ftp().'Imagenes/firmas/FS000052.jpg" width="190" height="80" />    
                                                    </td>';
                                            }
                                            if($SMICRO == '1'){
                                                $html .= '<td width="100%" align="center">
                                                    <img src="'.public_url_ftp().'Imagenes/firmas/FS000046.jpg" width="190" height="80" />   
                                                    </td>';
                                            }
                                            if($SINSTRU == '1'){
                                                $html .= '<td width="100%" align="center" colspan="2">
                                                    <img src="'.public_url_ftp().'Imagenes/firmas/FS000120.jpg" width="190" height="80" />     
                                                    </td>';
                                            }
                                    $html .= '</tr>
                                            <tr>';
                                            if($SFQ == '1'){
                                                $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                                <hr width=150>
                                                '.$NOMBREFQS.'    
                                                </td>';
                                            }
                                            if($SMICRO == '1'){
                                                $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                                <hr width=150>
                                                '.$NOMBREMICRO.'  
                                                </td>';
                                            }
                                            if($SINSTRU == '1'){
                                                $html .= '<td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                                <hr width=150>
                                                '.$NOMBREINSTRU.'     
                                                </td>';
                                            }
                                    $html .= '</tr>
                                            <tr>';
                                            if($SFQ == '1'){
                                                $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                                '.$CARGOFQS.'
                                                </td>';
                                            }
                                            if($SMICRO == '1'){
                                                $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                                '.$CARGOMICRO.'
                                                </td>';
                                            }
                                            if($SINSTRU == '1'){
                                                $html .= '<td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                                '.$CARGOINSTRU.'    
                                                </td>';
                                            }
                                    $html .= '</tr>
                                            <tr>';
                                            if($SFQ == '1'){
                                                $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                                '.$CODIGOFQS.'
                                                </td>';
                                            }
                                            if($SMICRO == '1'){
                                                $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                                '.$CODIGOMICRO.'
                                                </td>';
                                            }
                                            if($SINSTRU == '1'){
                                                $html .= '<td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                                '.$CODIGOINSTRU.'    
                                                </td>';
                                            }
                                    $html .= '</tr>
                                    </table>';
                                }   
        
                                  $html .= '</div>';
                                                  $pos++;
                                             }}
                                  $html .= '</body>
                                          </html>';
		$filename = 'CC '.substr($NROCERTI, 0, 9).' '.$CLIENTE.' '.$CMUESTRA;
        $this->pdfgenerator->generate($html, $filename, TRUE, 'A4', 'portrait');
        //echo $html;
    }  
    
   /** CERTIFICADO POR MUESTRAS **/                
    public function pdfCertificadoBorrador($cinternoordenservicio,$vcmuestra) { // recupera 
        $this->load->library('pdfgenerator');

        $html = '<html>
                <head>
                    <title>Certificado Borrador</title>
                    <style type="text/css">
                        @page{
                             margin: 0.3in 0.3in 0.3in 0.3in;
                        }
                        body{
                            font-family: Arial, Helvetica, sans-serif;
                            margin-top: 2.5cm;
                            margin-left: 1cm;
                            margin-right: 1cm;
                            margin-bottom: 2cm;
                        }
                        #header,
                        #footer,
                        #nroinf {
                            position: fixed;
                            left: 0;
                            right: 0;
                            color: #000000;
                            font-size: 0.9em;
                        }                        
                        #header {
                            top: 0;
                            height: 4cm;
                        }    
                        #nroinf {
                            top: 2.5cm;
                            height: 2cm;
                        }                     
                        #footer {
                            bottom: 0;
                            border-top: 0.1pt solid #aaa;
                            height: 1cm;
                        }                        
                        #header table,
                        #footer table,
                        #nroinf table {
                            width: 100%;
                            border-collapse: collapse;
                            border: none;
                        }                        
                        #header td,
                        #footer td,
                        #nroinf td {
                            padding: 0;
                        }
                        .page-number{
                            text-align: right;
                        }
                        .page-number:before{
                            content: "Pagina " counter(page);
                        }
                        hr1{
                            page-break-before: always;
                            color: #ffffff;
                            margin: -2px;
                        }     
                        #caratula td{
                            font-size:0.8em;        
                        }
                        #tableborder{
                            border-collapse: collapse;
                            width: 100%;
                            font-size:0.8em;
                            border: 0.5pt solid black;
                        }
                        #tableborder td{
                            border: 0.5pt solid black;
                            padding-left: 5px;
                        }
                        #tableborder th{
                            text-align: center;
                            border: 0.5pt solid black;
                            background-color: #DBD7D7;
                        }
                        .list-unstyled{
                            padding-left: 0;
                            list-style: none;
                        }
                    </style>
                </head>
                <body> 
                <div id="header">
                     <table width="100%" align="center">
                         <tr>
                             <td width="25%" align="center">                            
                                 <img src="'.public_url_ftp().'Imagenes/formatos/2/logoFSC.jpg" width="180" height="95" />  
                             </td>   
                             <td width="100%" align="right" colspan="2" style="font-size: 0.8em;">
                                 <ul class="list-unstyled">
                                     <li>Jr. Monterrey N° 221 Of. 201-202</li>
                                     <li>Urb. Chacarilla del Estanque. Santiago de Surco, Lima - Perú</li>
                                     <li>Teléfonos: (51-1) 480 0561 - Anexo: Lab 111</li>
                                     <li>www.fscertificaciones.com</li>
                                 </ul>
                             </td>
                         </tr>
                     </table>
                </div>   
                <div id="footer">
                     <table width="100%" style="font-size:0.8em; margin-top: 0.3cm; text-align: justify">
                     <tr>
                         <td width="100%" align="right" >
                             <div class="page-number"></div>
                         </td>
                     </tr>
                     </table>                    
                </div>'; 
                $parametroscabe = array(
                    '@cinternoordenservicio'         => $cinternoordenservicio,
                    '@cmuestra'       => $vcmuestra,
                );
                $res = $this->minformes->getcertixmuestras_caratula($parametroscabe);
                $pos = 0;
                if ($res){
                    foreach($res as $row){ 
                        $NROINFORME     = $row->NROINFORME;
                        $NROCERTI       = $row->NROCERTI;
                        $CLIENTE        = $row->CLIENTE;
                        $DIRECCION      = trim($row->DIRECCION);
                        $NROORDEN       = $row->NROORDEN;
                        $PROCEDENCIA    = $row->PROCEDENCIA;
                        $FMUESTRA       = $row->FMUESTRA;
                        $FRECEPCION     = $row->FRECEPCION;
                        $FANALISIS      = $row->FANALISIS;
                        $LUGARMUESTRA   = trim($row->LUGARMUESTRA);
                        $CMUESTRA       = trim($row->CMUESTRA);
                        $DPRODUCTO      = trim($row->DPRODUCTO);
                        $DTEMPERATURA   = $row->DTEMPERATURA;
                        $DLCLAB         = $row->DLCLAB;
                        $OBSERVACION    = $row->OBSERVACION;
                        $SOBSERV        = $row->SOBSERV;
                        $ACNAC          = $row->ACNAC;
                        $DDATOSADICICONCLU = $row->DDATOSADICICONCLU;
                        $DREALPRODUCTO = $row->DREALPRODUCTO;

                        if($pos > 0){
                            $html .= '
                            <table width="100%" align="center">
                            <tr>
                                <td colspan="3" style="font-size:1.1em;" align="center">
                                    <b>CERTIFICADO DE CALIDAD BORRADOR </b>
                                </td>
                            </tr>
                            </table>';                      
                        }else{
                            $html .= '
                            <table width="100%" align="center">
                            <tr>
                                <td colspan="3" style="font-size:1.1em;" align="center">
                                    <b>CERTIFICADO DE CALIDAD BORRADOR </b>
                                </td>
                            </tr>
                            </table>';
                        }
                        
                        $parametros = array(
                            '@cinternoordenservicio'    => $cinternoordenservicio,
                            '@cmuestra'                 => $CMUESTRA,
                        );
                       
                        $html .= '<div id="main">
                                           
                                    <table id="caratula" width="100%" align="center" cellspacing="0" cellpadding="2">  
                                        <tr>
                                            <td> 
                                                <b>1.- </b>  
                                            </td>
                                            <td colspan="3" align="left">
                                                <b> DATOS GENERALES</b>   
                                            </td>
                                        </tr>                       
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>Nombre del Cliente</b>   
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$CLIENTE.'   
                                            </td>
                                        </tr>
                                        <tr style="vertical-align:top">
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>Dirección del Cliente</b>  
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$DIRECCION.'   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>N° Orden de Trabajo</b>   
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$NROORDEN.'   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>Procedencia de la Muestra</b>  
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$PROCEDENCIA.'   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>Fecha de Muestreo</b>    
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$FMUESTRA.'   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>Fecha de Recepción</b>    
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$FRECEPCION.'   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>Fecha de Análisis</b>    
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$FANALISIS.'  
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>Lugar de Muestreo</b>   
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$LUGARMUESTRA.'  
                                            </td>
                                        </tr>
                                        <tr style="vertical-align:top">
                                            <td align="left">   
                                            </td>
                                            <td width="10%" align="left">
                                                <b>Muestra / Descripción</b>    
                                            </td>
                                            <td width="7%" align="left">
                                                : '.$CMUESTRA.'   
                                            </td>
                                            <td width="80%" align="left">
                                                '.$DPRODUCTO.'   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>Temperatura de Recepción</b>    
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$DTEMPERATURA.'   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>N° Informe de Ensayo</b>   
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$NROINFORME.'   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="4">
                                            &nbsp;   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td> 
                                                <b>2.- </b>  
                                            </td>
                                            <td align="left" colspan="3" style="height:30px;">
                                                <b>DOCUMENTOS NORMATIVOS</b>   
                                            </td>
                                        </tr>';
                                        $resdocnorma = $this->minformes->getdocnormativo($cinternoordenservicio,$CMUESTRA);
                                        $idocnorma = 1;
                                        if ($resdocnorma){
                                            foreach($resdocnorma as $rowdocnorma){
                                                $cnormalab = $rowdocnorma->cnormalab;
                                                $cgruponormalab = $rowdocnorma->cgruponormalab;
                                                $dnormalab = $rowdocnorma->dnormalab;
                                                $html .= '<tr style="vertical-align:top">
                                                   <td>2.'.$idocnorma.'</td>
                                                   <td colspan="3">'.$dnormalab.'</td>
                                                </tr>';
                                                $idocnorma ++ ;
                                                $resdocnorma_det = $this->minformes->getdocnormativo_det($cnormalab,$cgruponormalab);
                                                if ($resdocnorma_det){
                                                    foreach($resdocnorma_det as $rowdocnorma_det){
                                                        $dnumerogrupo = $rowdocnorma_det->dnumerogrupo;
                                                        $dnombregrupo = $rowdocnorma_det->dnombregrupo;
                                                        if($dnumerogrupo <> '0'){
                                                            $html .= '<tr>
                                                            <td></td>
                                                            <td colspan="3">'.$dnumerogrupo.' &nbsp; &nbsp; '.$dnombregrupo.'</td>
                                                            </tr>';
                                                        }
                                                    }
                                                } 
                                            }
                                        } 
                                        $html .= '
                                        <tr>
                                           <td > 
                                               <b>3.- </b>  
                                           </td>
                                            <td align="left" colspan="3" style="height:50px;">
                                                <b>METODOS DE ENSAYO</b>   
                                            </td>
                                        </tr>
                                    </table>';
                                     
                                    /*RESULTADOS METODOS DE ENSAYO*/    
                                     $resmetensa = $this->minformes->getmetodosensayos($parametros);
                                      if ($resmetensa){
                                        $html .= '<table id="tableborder" style="padding-left: 15px;">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <b>Ensayo</b>   
                                                </th>
                                                <th>
                                                    <b>Norma o Referencia</b>    
                                                </td>
                                            </tr>
                                            </thead>
                                            <tbody>';
                                            foreach($resmetensa as $rowmetensa){
                                                $METDENSAYO = $rowmetensa->DENSAYO;
                                                $METDNORMA = $rowmetensa->DNORMA;
                                                $html .= '<tr>
                                                   <td width="180px">'.$METDENSAYO.'</td>
                                                   <td style="vertical-align:top">'.$METDNORMA.'</td>
                                                </tr>';
                                            }
                                        $html .= '</tbody></table><br>';
                                     }  
                
                                     $html .= '<table id="caratula" width="713px" align="center" cellspacing="0" cellpadding="2" >
                                       <tr>
                                           <td width="3%"> 
                                               <b>4.- </b>  
                                           </td>
                                           <td width="97%" align="left" colspan="3" style="height:50px;">
                                               <b>RESULTADOS E INTERPRETACIONES</b>   
                                           </td>
                                       </tr>
                                     </table>'; 
                
                                    /*RESULTADOS MICROBIOLOGIA*/
                                     $resmicro = $this->minformes->getinfxmuestras_resmicro($parametros);
                                     if ($resmicro){
                                         $html .= '<table id="tableborder">
                                         <thead>
                                         <tr>
                                             <th> <b>Ensayo</b> </th>
                                             <th> <b>Unidades</b> </th>
                                             <th> <b>Limite permisible</b> </th>
                                             <th> <b>Resultado</b> </th>
                                             <th> <b>Conclusión</b> </th>
                                         </tr>
                                         </thead>
                                         <tbody>';
                                         $DENSAYOmicro = '';
                                         foreach($resmicro as $rowmicro){
                                             $DENSAYO = $rowmicro->NOMBENSAYO;
                                             $UNIDADMEDIDA = $rowmicro->UNIDADMEDIDA;
                                             $ESP_FINAL = $rowmicro->ESP_FINAL;
                                             $RESULT_FINAL = $rowmicro->RESULT_FINAL;
                                             $VALRESULTADO = $rowmicro->VALRESULTADO;
                                             $EXP10RES = $rowmicro->EXP10RES;
                                             $EXP10ESP = $rowmicro->EXP10ESP;

                                             $html .= '<tr>';
                                             if ($DENSAYO == $DENSAYOmicro){
                                                $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                                             }else{
                                                $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                                             }
                                             
                                             $html .= '<td width="100px" align="center">'.$UNIDADMEDIDA.'</td>
                                                 <td width="100px" align="center">'.str_replace("<", "&lt;", $ESP_FINAL).'<sup>'.$EXP10ESP.'</sup></td>
                                                 <td width="140px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'<sup>'.$EXP10RES.'</sup></td>
                                                 <td width="100px" align="center">'.$VALRESULTADO.'</td>
                                             </tr>';
                                             $DENSAYOmicro = $DENSAYO;
                                         }
                                         $html .= '</tbody></table><br>';
                                     }  
                                    /*RESULTADOS FISICOQUIMICO*/
                                     $resfq = $this->minformes->getinfxmuestras_resfq($parametros);
                                     if ($resfq){
                                         $html .= '<table id="tableborder">
                                         <thead>
                                         <tr>
                                             <th> <b>Ensayo</b> </th>
                                             <th> <b>Unidades</b> </th>
                                             <th> <b>Limite permisible</b> </th>
                                             <th> <b>Resultado</b> </th>
                                             <th> <b>Conclusión</b> </th>
                                         </tr>
                                         </thead>
                                         <tbody>';
                                         $DENSAYOfq = '';
                                         foreach($resfq as $rowfq){
                                             $DENSAYO = $rowfq->NOMBENSAYO;
                                             $UNIDADMEDIDA = $rowfq->UNIDADMEDIDA;
                                             $ESP_FINAL = $rowfq->ESP_FINAL;
                                             $RESULT_FINAL = $rowfq->RESULT_FINAL;
                                             $VALRESULTADO = $rowfq->VALRESULTADO;
                                             $EXP10ESP = $rowfq->EXP10ESP;
                                             $EXP10RES = $rowfq->EXP10RES;

                                             $html .= '<tr>';
                                             if ($DENSAYO == $DENSAYOfq){
                                                $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                                             }else{
                                                $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                                             }
                
                                             $html .= '<td width="100px" align="center">'.$UNIDADMEDIDA.'</td>
                                                 <td width="100px" align="center">'.str_replace("<", "&lt;", $ESP_FINAL).'<sup>'.$EXP10ESP.'</sup></td>
                                                 <td width="140px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'<sup>'.$EXP10RES.'</sup></td>
                                                 <td width="100px" align="center">'.$VALRESULTADO.'</td>
                                             </tr>';
                                             $DENSAYOfq = $DENSAYO;
                                         }
                                         $html .= '</tbody></table><br>';
                                     }
                                    ///RESULTADOS INSTRUMENTAL TERCERO///
                                       $resins = $this->minformes->getinfxmuestras_resinstru($parametros);
                                       if ($resins){
                                           $html .= '<table id="tableborder">
                                           <thead>
                                           <tr>
                                                <th> <b>Ensayo</b> </th>
                                                <th> <b>Unidades</b> </th>
                                                <th> <b>Limite permisible</b> </th>
                                                <th> <b>Resultado</b> </th>
                                                <th> <b>Conclusión</b> </th>
                                           </tr>
                                           </thead>
                                           <tbody>';
                                           $DENSAYOins = '';
                                           foreach($resins as $rowins){
                                               $DENSAYO = $rowins->DENSAYO;
                                               $UNIDADMEDIDA = $rowins->UNIDADMEDIDA;
                                               $ESP_FINAL = $rowins->ESP_FINAL;
                                               $RESULT_FINAL = $rowins->RESULT_FINAL;
                                               $VALRESULTADO = $rowins->VALRESULTADO;
                                               $EXP10ESP = $rowins->EXP10ESP;
                                               $EXP10RES = $rowins->EXP10RES;
                                               $TIPOPRODUCTO = $rowins->TIPOPRODUCTO;
                  
                                               $nota01 = '';                                               
                                               if ($TIPOPRODUCTO == '1'){
                                                $nota01 = '<small style="font-size: 10px;">(*)Fuente: Art. 4 , Decreto Supremo N°017-2017-SA Decreto Supremo N°012-2018 -SA. "Manual de Advertencias Publicitarias en el marco de los establecido en la Ley N° 30021, Ley de Promoción de la alimentación saludable para niños, niñas y adolescentes, y su Reglamento aprobado por Decreto Supremo N°017-2017-SA, Decreto Supremo N°033-2016 SA. "Reglamento que establece el proceso reducción gradual hasta le eliminación de las grasas trans en los alimentos y bebidas no alcohólicas procesados industrialmente.</small>';
                                               }                                               

                                               $html .= '<tr>';
                                               if ($DENSAYO == $DENSAYOins){
                                                  $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                                               }else{
                                                  $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                                               }
                  
                                               $html .= '<td width="100px" align="center">'.$UNIDADMEDIDA.'</td>
                                                   <td width="100px" align="center">'.str_replace("<", "&lt;", $ESP_FINAL).'<sup>'.$EXP10ESP.'</sup></td>
                                                   <td width="140px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'<sup>'.$EXP10RES.'</sup></td>
                                                   <td width="100px" align="center">'.$VALRESULTADO.'</td>
                                               </tr>';
                                               $DENSAYOins = $DENSAYO;
                                           }
                                           $html .= '</tbody></table><br>'.$nota01.'&nbsp;<br>&nbsp;<br>&nbsp;';
                                       }
                                    /*RESULTADOS SENSORIAL*/
                                     $ressen = $this->minformes->getinfxmuestras_ressenso($parametros);
                                     if ($ressen){
                                         $html .= '<table id="tableborder" style="page-break-inside: avoid;">
                                         <thead>
                                         <tr>
                                             <th> <b>Ensayo</b> </th>
                                             <th> <b>Atributo</b> </th>
                                             <th> <b>Limite permisible</b> </th>
                                             <th> <b>Resultado</b> </th>
                                             <th> <b>Conclusión</b> </th>
                                         </tr>
                                         </thead>
                                         <tbody>';
                                         $DENSAYOsen = '';
                                         foreach($ressen as $rowsen){
                                             $DENSAYO = $rowsen->DENSAYO;
                                             $DNOMBREESCALA = $rowsen->DNOMBREESCALA;
                                             $DLIMITE = $rowsen->DLIMITE;
                                             $RESULT_FINAL = $rowsen->RESULT_FINAL;
                                             $SCONCLUSION = $rowsen->SCONCLUSION;
                                             $html .= '<tr style="vertical-align:top">';
                                             if ($DENSAYO == $DENSAYOsen){
                                                $html .= '';
                                             }else{
                                                $html .= '<td rowspan="5" style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                                             }
                
                                             $html .= '<td width="70px">'.$DNOMBREESCALA.'</td>
                                                 <td width="160px" align="center">'.$DLIMITE.'</td>
                                                 <td width="170px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'</td>
                                                 <td width="100px" align="center">'.$SCONCLUSION.'</td>
                                             </tr>';
                                             $DENSAYOsen = $DENSAYO;
                                         }
                                         $html .= '</tbody></table><br>';
                                     }
                                     
                                    /*RESULTADOS CON ELEMENTOS*/
                                     $resele = $this->minformes->getinfxmuestras_reselementos($parametros);
                                     if ($resele){
                                        $html .= '<table id="tableborder">
                                        <thead>
                                        <tr>
                                            <th> <b>Ensayo</b> </th>
                                            <th> <b>Elemento</b> </th>
                                            <th> <b>Unidades</b> </th>
                                            <th> <b>Limite permisible</b> </th>
                                            <th> <b>Resultado</b> </th>
                                            <th> <b>Conclusión</b> </th>
                                        </tr>
                                        </thead>
                                        <tbody>';
                                        $DENSAYOele = '';
                                        foreach($resele as $rowele){
                                            $DENSAYO = $rowele->NOMBENSAYO;
                                            $ELEMENTO = $rowele->ELEMENTO;
                                            $UNIDADMEDIDA = $rowele->UNIDADMEDIDA;
                                            $LIMITE_FINAL = $rowele->LIMITE_FINAL;
                                            $RESULT_FINAL = $rowele->RESULT_FINAL;
                                            $VALRESULTADO = $rowele->VALRESULTADO;
                                            $html .= '<tr>';
                                            if ($DENSAYO == $DENSAYOele){
                                               $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                                            }else{
                                               $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                                            }
                
                                            $html .= '<td>'.$ELEMENTO.'</td>
                                                <td width="85px" align="center">'.$UNIDADMEDIDA.'</td>
                                                <td width="100px" align="center">'.str_replace("<", "&lt;", $LIMITE_FINAL).'</td>
                                                <td width="100px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'</td>
                                                <td width="100px" align="center">'.$VALRESULTADO.'</td>
                                            </tr>';
                                            $DENSAYOele = $DENSAYO;
                                        }
                                        $html .= '</tbody></table><br>';
                                     }
                                    /*RESULTADOS ESTERILIDAD BAJA*/
                                     $parametrosEsterili = array(
                                        '@cinternoordenservicio' => $cinternoordenservicio,
                                        '@cmuestra' => $CMUESTRA,
                                        '@stipoacidez' => 'B'
                                     );
                                     $resesteb = $this->minformes->getinfxmuestras_resesteri($parametrosEsterili);
                                     if ($resesteb){
                                        $html .= '<table id="tableborder">
                                        <tr>
                                            <td colspan="7"> Baja acidez (pH>4.6)</td>
                                        </tr>
                                        <tr>
                                            <th width="30%" rowspan="2"> <b>Preincubación 35°C/14 días</b> </th>
                                            <th width="5%" rowspan="2"> <b>pH</b> </th>
                                            <th width="25%" colspan="2"> <b>Mesófilos (35°C)</b> </th>
                                            <th width="15%" colspan="2"> <b>Termófilos (55°C)</b> </th>
                                            <th width="25%" rowspan="2"> <b>Resultado</b> </th>
                                        </tr>
                                        <tr>
                                            <th> <b>Caldo púrpura de bromocreasol 120h</b> </th>
                                            <th> <b>Caldo carne cocida 120h</b> </th>
                                            <th> <b>Caldo púrpura de bromocreasol 48h</b> </th>
                                            <th> <b>Caldo carne cocida 72h</b> </th>
                                        </tr>';
                                        foreach($resesteb as $rowesteb){
                                            $preincuba = $rowesteb->preincuba;
                                            $dph = $rowesteb->dph;
                                            $dbames35cp = $rowesteb->dbames35cp;
                                            $dbames35cc = $rowesteb->dbames35cc;
                                            $dbater55cp = $rowesteb->dbater55cp;
                                            $dbater55cc = $rowesteb->dbater55cc;
                                            $result_final = $rowesteb->result_final;
                                            $html .= '<tr>
                                                <td>'.$preincuba.'</td>
                                                <td align="center">'.$dph.'</td>
                                                <td align="center">'.$dbames35cp.'</td>
                                                <td align="center">'.$dbames35cc.'</td>
                                                <td align="center">'.$dbater55cp.'</td>
                                                <td align="center">'.$dbater55cc.'</td>
                                                <td align="center">'.$result_final.'</td>
                                            </tr>';
                                        }
                                        $html .= '</table><br>';
                                     }
                                    /*RESULTADOS ESTERILIDAD ALTA*/
                                     $parametrosEsterili = array(
                                        '@cinternoordenservicio' => $cinternoordenservicio,
                                        '@cmuestra' => $CMUESTRA,
                                        '@stipoacidez' => 'A'
                                     );
                                     $resestea = $this->minformes->getinfxmuestras_resesteri($parametrosEsterili);
                                     if ($resestea){
                                        $html .= '<table id="tableborder">
                                        <tr>
                                            <td colspan="7"> Alta acidez ('.str_replace("<", "&lt;", "pH<4.6").')</td>
                                        </tr>
                                        <tr>
                                            <th width="30%" rowspan="2"> <b>Preincubación 35°C/14 días</b> </th>
                                            <th width="5%" rowspan="2"> <b>pH</b> </th>
                                            <th width="25%" colspan="2"> <b>Mesófilos (35°C)</b> </th>
                                            <th width="15%" colspan="2"> <b>Termófilos (55°C)</b> </th>
                                            <th width="25%" rowspan="2"> <b>Resultado</b> </th>
                                        </tr>
                                        <tr>
                                            <th> <b>Caldo ácido 96h</b> </th>
                                            <th> <b>Caldo extracto de malta 96h</b> </th>
                                            <th colspan="2"> <b>Caldo ácido 48h</b> </th>
                                        </tr>';
                                        foreach($resestea as $rowestea){
                                            $preincuba = $rowestea->preincuba;
                                            $dph = $rowestea->dph;
                                            $dacmes30ca = $rowestea->dacmes30ca;
                                            $dacmes30cm = $rowestea->dacmes30cm;
                                            $dacter55ca = $rowestea->dacter55ca;
                                            $result_final = $rowestea->result_final;
                                            $html .= '<tr>
                                                <td>'.$preincuba.'</td>
                                                <td align="center">'.$dph.'</td>
                                                <td align="center">'.$dacmes30ca.'</td>
                                                <td align="center">'.$dacmes30cm.'</td>
                                                <td align="center" colspan="2">'.$dacter55ca.'</td>
                                                <td align="center">'.$result_final.'</td>
                                            </tr>';
                                        }
                                        $html .= '</table><br>';
                                     }

                                     ///RESULTADOS ETIQUETADO NUTRICIONAL///
                                       $parametrosEtiquetado = array(
                                           '@cinternoordenservicio' => $cinternoordenservicio,
                                           '@cmuestra' => $CMUESTRA
                                       );
                                       $resetiqueta = $this->minformes->getinfxmuestras_resetiquetado($parametrosEtiquetado);
                                       if ($resetiqueta){
                                         $html .= '<table id="tableborder">
                                           <tr>
                                               <td colspan="7"> <b>INFORMACION NUTRICIONAL</b> </td>
                                           </tr>
                                           <tr>
                                               <td width="40%" colspan="4"> Tamaño de Porción :</td>
                                               <td colspan="3">14g</td>
                                           </tr>
                                           <tr>
                                               <th width="25%" colspan="3"> <b>Ensayo</b> </th>
                                               <th width="15%"> <b>Unidad</b> </th>
                                               <th width="20%"> <b>100g</b> </th>
                                               <th width="20%"> <b>Porción</b> </th>
                                               <th width="20%"> <b>% VDR(*)</b> </th>
                                           </tr>';
                                           foreach($resetiqueta as $rowetiqueta){
                                               $DENSAYO = $rowetiqueta->DENSAYO;
                                               $UNIDADMEDIDA = $rowetiqueta->UNIDADMEDIDA;
                                               $DRESULTADO = $rowetiqueta->DRESULTADO;
                                               $NPORCION = $rowetiqueta->NPORCION;
                                               $VALORVR = $rowetiqueta->VALORVR;
                                               $html .= '<tr>
                                                   <td colspan="3">'.$DENSAYO.'</td>
                                                   <td align="center">'.$UNIDADMEDIDA.'</td>
                                                   <td align="center">'.$DRESULTADO.'</td>
                                                   <td align="center">'.number_format($NPORCION, 3, '.', '').'</td>
                                                   <td align="center">'.number_format($VALORVR, 3, '.', '').'</td>
                                               </tr>';
                                           }
                                           $html .= '<tr><td colspan="7">(*) % Valor Diario (VD) con base a una dieta de 2000 kcal u 8370 kJ.  según Codex Alimentarius FAO/OMS. Sus valores diarios pueden ser mayores o menores dependiendo de sus necesidades energéticas.</tr>
                                                      </table><br>';
                                       }
                                    
                                  /*CONCLUSIONES */     
                                   $html .= '<table id="caratula" width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">
                                     <tr>
                                         <td width="3%"> 
                                             <b>5.- </b>  
                                         </td>
                                         <td width="97%" align="left" colspan="3" style="height:30px;">
                                             <b>CONCLUSIONES</b>   
                                         </td>                         
                                     </tr>';
                                      
                                     $parametrosconclu = array(
                                        '@cinternoordenservicio'         => $cinternoordenservicio,
                                        '@cmuestra'       => $CMUESTRA,
                                        '@DDATOSADICICONCLU'       => $DDATOSADICICONCLU,
                                        '@DREALPRODUCTO'       => $DREALPRODUCTO,
                                     );
                                     $resconclu = $this->minformes->getcertixmuestras_conclusion($parametrosconclu);
                                     if ($resconclu){
                                         foreach($resconclu as $rowconclu){
                                             $DCONCLUSION = $rowconclu->DCONCLUSION;
                                             $html .= '<tr>
                                                <td></td>
                                                <td colspan="3">'.$DCONCLUSION.'</td>
                                             </tr>';
                                         }
                                    }
                                    
                                   $html .= '</table><br>'; 
                                    
        
                                  $html .= '</div>';
                                                  $pos++;
                                             }}
                                  $html .= '</body>
                                          </html>';
		
        $filename = 'CERTI-BORRADOR';
        $this->pdfgenerator->generate($html, $filename, TRUE, 'A4', 'portrait');
        //echo $html;
    } 
    public function pdfCertificadoIndivConLogo2($cinternoordenservicio,$vcmuestra) { // recupera 
        $this->load->library('pdfgenerator');

        $html = '<html>
                <head>
                    <title>Certificado</title>
                    <style type="text/css">
                        @page{
                             margin: 0.3in 0.3in 0.3in 0.3in;
                        }
                        body{
                            font-family: Arial, Helvetica, sans-serif;
                            margin-top: 3.5cm;
                            margin-left: 1cm;
                            margin-right: 1cm;
                            margin-bottom: 2cm;
                        }
                        #header,
                        #footer,
                        #nroinf {
                            position: fixed;
                            left: 0;
                            right: 0;
                            color: #000000;
                            font-size: 0.9em;
                        }                        
                        #header {
                            top: 0;
                            height: 4cm;
                        }    
                        #nroinf {
                            top: 2.5cm;
                            height: 2cm;
                        }                     
                        #footer {
                            bottom: 0;
                            border-top: 0.1pt solid #aaa;
                            height: 1cm;
                        }                        
                        #header table,
                        #footer table,
                        #nroinf table {
                            width: 100%;
                            border-collapse: collapse;
                            border: none;
                        }                        
                        #header td,
                        #footer td,
                        #nroinf td {
                            padding: 0;
                        }
                        .page-number{
                            text-align: right;
                        }
                        .page-number:before{
                            content: "Pagina " counter(page);
                        }
                        hr1{
                            page-break-before: always;
                            color: #ffffff;
                            margin: -2px;
                        }     
                        #caratula td{
                            font-size:0.8em;        
                        }
                        #tableborder{
                            border-collapse: collapse;
                            width: 100%;
                            font-size:0.8em;
                            border: 0.5pt solid black;
                        }
                        #tableborder td{
                            border: 0.5pt solid black;
                            padding-left: 5px;
                        }
                        #tableborder th{
                            text-align: center;
                            border: 0.5pt solid black;
                            background-color: #DBD7D7;
                        }
                        .list-unstyled{
                            padding-left: 0;
                            list-style: none;
                        }
                    </style>
                </head>
                <body> 
                <div id="header">
                     
                </div>   
                <div id="footer">
                     <table width="100%" style="font-size:0.8em; margin-top: 0.3cm; text-align: justify">
                     <tr>
                         <td width="100%" align="right" >
                             <div class="page-number"></div>
                         </td>
                     </tr>
                     </table>                    
                </div>'; 
                $parametroscabe = array(
                    '@cinternoordenservicio'         => $cinternoordenservicio,
                    '@cmuestra'       => $vcmuestra,
                );
                $res = $this->minformes->getcertixmuestras_caratula($parametroscabe);
                $pos = 0;
                if ($res){
                    foreach($res as $row){ 
                        $NROINFORME     = $row->NROINFORME;
                        $NROCERTI       = $row->NROCERTI;
                        $CLIENTE        = $row->CLIENTE;
                        $DIRECCION      = $row->DIRECCION;
                        $NROORDEN       = $row->NROORDEN;
                        $PROCEDENCIA    = $row->PROCEDENCIA;
                        $FMUESTRA       = $row->FMUESTRA;
                        $FRECEPCION     = $row->FRECEPCION;
                        $FANALISIS      = $row->FANALISIS;
                        $LUGARMUESTRA   = $row->LUGARMUESTRA;
                        $CMUESTRA       = $row->CMUESTRA;
                        $DPRODUCTO      = $row->DPRODUCTO;
                        $DTEMPERATURA   = $row->DTEMPERATURA;
                        $DLCLAB         = $row->DLCLAB;
                        $OBSERVACION    = $row->OBSERVACION;
                        $SOBSERV        = $row->SOBSERV;
                        $ACNAC          = $row->ACNAC;
                        $DDATOSADICICONCLU = $row->DDATOSADICICONCLU;
                        $DREALPRODUCTO = $row->DREALPRODUCTO;

                        if($pos > 0){
                            $html .= '<hr style="page-break-before: always;" size="0" noshade="noshade" />';                          
                        }  
                        
                        $parametros = array(
                            '@cinternoordenservicio'    => $cinternoordenservicio,
                            '@cmuestra'                 => $CMUESTRA,
                        );
                                             
                        $html .= '   <div id="header">
                                    <table width="100%" align="center">   
                                    <tr>
                                        <td width="25%" align="center">                            
                                            &nbsp;  
                                        </td>   
                                        <td width="100%" align="right" colspan="2" style="font-size: 0.8em;">
                                            <ul class="list-unstyled">
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                            </ul>
                                        </td>
                                    </tr>                                 
                                    <tr>
                                        <td colspan="3" style="height:40px; font-size:1.1em;" align="center">
                                            <b>CERTIFICADO DE CALIDAD N° '.$NROCERTI.'  </b>
                                        </td>
                                    </tr>
                                    </table>
                                </div>';
                       
                        $html .= '<div id="main">
                                           
                                    <table id="caratula" width="100%" align="center" cellspacing="0" cellpadding="2">  
                                        <tr>
                                            <td> 
                                                <b>1.- </b>  
                                            </td>
                                            <td colspan="3" align="left">
                                                <b> DATOS GENERALES</b>   
                                            </td>
                                        </tr>                       
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>Nombre del Cliente</b>   
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$CLIENTE.'   
                                            </td>
                                        </tr>
                                        <tr style="vertical-align:top">
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>Dirección del Cliente</b>  
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$DIRECCION.'   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>N° Orden de Trabajo</b>   
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$NROORDEN.'   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>Procedencia de la Muestra</b>  
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$PROCEDENCIA.'   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>Fecha de Muestreo</b>    
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$FMUESTRA.'   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>Fecha de Recepción</b>    
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$FRECEPCION.'<   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>Fecha de Análisis</b>    
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$FANALISIS.'  
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>Lugar de Muestreo</b>   
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$LUGARMUESTRA.'  
                                            </td>
                                        </tr>
                                        <tr style="vertical-align:top">
                                            <td width="3%" align="left">   
                                            </td>
                                            <td width="27%" align="left">
                                                <b>Muestra / Descripción</b>    
                                            </td>
                                            <td width="7%" align="left">
                                                : '.$CMUESTRA.'   
                                            </td>
                                            <td width="63%" align="left">
                                                '.$DPRODUCTO.'   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>Temperatura de Recepción</b>    
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$DTEMPERATURA.'   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>N° Informe de Ensayo</b>   
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$NROINFORME.'   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="4">
                                            &nbsp;   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td> 
                                                <b>2.- </b>  
                                            </td>
                                            <td align="left" colspan="3" style="height:30px;">
                                                <b>DOCUMENTOS NORMATIVOS</b>   
                                            </td>
                                        </tr>';
                                        $resdocnorma = $this->minformes->getdocnormativo($cinternoordenservicio,$CMUESTRA);
                                        $idocnorma = 1;
                                        if ($resdocnorma){
                                            foreach($resdocnorma as $rowdocnorma){
                                                $cnormalab = $rowdocnorma->cnormalab;
                                                $cgruponormalab = $rowdocnorma->cgruponormalab;
                                                $dnormalab = $rowdocnorma->dnormalab;
                                                $html .= '<tr style="vertical-align:top">
                                                   <td>2.'.$idocnorma.'</td>
                                                   <td colspan="3">'.$dnormalab.'</td>
                                                </tr>';
                                                $idocnorma ++ ;
                                                $resdocnorma_det = $this->minformes->getdocnormativo_det($cnormalab,$cgruponormalab);
                                                if ($resdocnorma_det){
                                                    foreach($resdocnorma_det as $rowdocnorma_det){
                                                        $dnumerogrupo = $rowdocnorma_det->dnumerogrupo;
                                                        $dnombregrupo = $rowdocnorma_det->dnombregrupo;
                                                        if($dnumerogrupo <> '0'){
                                                            $html .= '<tr>
                                                            <td></td>
                                                            <td colspan="3">'.$dnumerogrupo.' &nbsp; &nbsp; '.$dnombregrupo.'</td>
                                                            </tr>';
                                                        }
                                                    }
                                                } 
                                            }
                                        } 
                                        $html .= '
                                        <tr>
                                           <td > 
                                               <b>3.- </b>  
                                           </td>
                                            <td align="left" colspan="3" style="height:50px;">
                                                <b>METODOS DE ENSAYO</b>   
                                            </td>
                                        </tr>
                                    </table>';
                                     
                                    /*RESULTADOS METODOS DE ENSAYO*/    
                                     $resmetensa = $this->minformes->getmetodosensayos($parametros);
                                      if ($resmetensa){
                                        $html .= '<table id="tableborder" style="padding-left: 15px;">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <b>Ensayo</b>   
                                                </th>
                                                <th>
                                                    <b>Norma o Referencia</b>    
                                                </td>
                                            </tr>
                                            </thead>
                                            <tbody>';
                                            foreach($resmetensa as $rowmetensa){
                                                $METDENSAYO = $rowmetensa->DENSAYO;
                                                $METDNORMA = $rowmetensa->DNORMA;
                                                $html .= '<tr>
                                                   <td width="180px">'.$METDENSAYO.'</td>
                                                   <td style="vertical-align:top">'.$METDNORMA.'</td>
                                                </tr>';
                                            }
                                        $html .= '</tbody></table><br>';
                                     }  
                
                                     $html .= '<table id="caratula" width="713px" align="center" cellspacing="0" cellpadding="2" >
                                       <tr>
                                           <td width="3%"> 
                                               <b>4.- </b>  
                                           </td>
                                           <td width="97%" align="left" colspan="3" style="height:50px;">
                                               <b>RESULTADOS E INTERPRETACIONES</b>   
                                           </td>
                                       </tr>
                                     </table>'; 
                
                                    /*RESULTADOS MICROBIOLOGIA*/
                                     $resmicro = $this->minformes->getinfxmuestras_resmicro($parametros);
                                     if ($resmicro){
                                         $html .= '<table id="tableborder">
                                         <thead>
                                         <tr>
                                             <th> <b>Ensayo</b> </th>
                                             <th> <b>Unidades</b> </th>
                                             <th> <b>Limite permisible</b> </th>
                                             <th> <b>Resultado</b> </th>
                                             <th> <b>Conclusión</b> </th>
                                         </tr>
                                         </thead>
                                         <tbody>';
                                         $DENSAYOmicro = '';
                                         foreach($resmicro as $rowmicro){
                                             $DENSAYO = $rowmicro->NOMBENSAYO;
                                             $UNIDADMEDIDA = $rowmicro->UNIDADMEDIDA;
                                             $ESP_FINAL = $rowmicro->ESP_FINAL;
                                             $RESULT_FINAL = $rowmicro->RESULT_FINAL;
                                             $VALRESULTADO = $rowmicro->VALRESULTADO;
                                             $EXP10RES = $rowmicro->EXP10RES;
                                             $EXP10ESP = $rowmicro->EXP10ESP;

                                             $html .= '<tr>';
                                             if ($DENSAYO == $DENSAYOmicro){
                                                $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                                             }else{
                                                $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                                             }
                                             
                                             $html .= '<td width="100px" align="center">'.$UNIDADMEDIDA.'</td>
                                                 <td width="100px" align="center">'.str_replace("<", "&lt;", $ESP_FINAL).'<sup>'.$EXP10ESP.'</sup></td>
                                                 <td width="140px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'<sup>'.$EXP10RES.'</sup></td>
                                                 <td width="100px" align="center">'.$VALRESULTADO.'</td>
                                             </tr>';
                                             $DENSAYOmicro = $DENSAYO;
                                         }
                                         $html .= '</tbody></table><br>';
                                     }  
                                    /*RESULTADOS FISICOQUIMICO*/
                                     $resfq = $this->minformes->getinfxmuestras_resfq($parametros);
                                     if ($resfq){
                                         $html .= '<table id="tableborder">
                                         <thead>
                                         <tr>
                                             <th> <b>Ensayo</b> </th>
                                             <th> <b>Unidades</b> </th>
                                             <th> <b>Limite permisible</b> </th>
                                             <th> <b>Resultado</b> </th>
                                             <th> <b>Conclusión</b> </th>
                                         </tr>
                                         </thead>
                                         <tbody>';
                                         $DENSAYOfq = '';
                                         foreach($resfq as $rowfq){
                                             $DENSAYO = $rowfq->NOMBENSAYO;
                                             $UNIDADMEDIDA = $rowfq->UNIDADMEDIDA;
                                             $ESP_FINAL = $rowfq->ESP_FINAL;
                                             $RESULT_FINAL = $rowfq->RESULT_FINAL;
                                             $VALRESULTADO = $rowfq->VALRESULTADO;
                                             $EXP10ESP = $rowfq->EXP10ESP;
                                             $EXP10RES = $rowfq->EXP10RES;

                                             $html .= '<tr>';
                                             if ($DENSAYO == $DENSAYOfq){
                                                $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                                             }else{
                                                $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                                             }
                
                                             $html .= '<td width="100px" align="center">'.$UNIDADMEDIDA.'</td>
                                                 <td width="100px" align="center">'.str_replace("<", "&lt;", $ESP_FINAL).'<sup>'.$EXP10ESP.'</sup></td>
                                                 <td width="140px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'<sup>'.$EXP10RES.'</sup></td>
                                                 <td width="100px" align="center">'.$VALRESULTADO.'</td>
                                             </tr>';
                                             $DENSAYOfq = $DENSAYO;
                                         }
                                         $html .= '</tbody></table><br>';
                                     }
                                    ///RESULTADOS INSTRUMENTAL TERCERO///
                                       $resins = $this->minformes->getinfxmuestras_resinstru($parametros);
                                       if ($resins){
                                           $html .= '<table id="tableborder">
                                           <thead>
                                           <tr>
                                                <th> <b>Ensayo</b> </th>
                                                <th> <b>Unidades</b> </th>
                                                <th> <b>Limite permisible</b> </th>
                                                <th> <b>Resultado</b> </th>
                                                <th> <b>Conclusión</b> </th>
                                           </tr>
                                           </thead>
                                           <tbody>';
                                           $DENSAYOins = '';
                                           foreach($resins as $rowins){
                                               $DENSAYO = $rowins->DENSAYO;
                                               $UNIDADMEDIDA = $rowins->UNIDADMEDIDA;
                                               $ESP_FINAL = $rowins->ESP_FINAL;
                                               $RESULT_FINAL = $rowins->RESULT_FINAL;
                                               $VALRESULTADO = $rowins->VALRESULTADO;
                                               $EXP10ESP = $rowins->EXP10ESP;
                                               $EXP10RES = $rowins->EXP10RES;
                                               $TIPOPRODUCTO = $rowins->TIPOPRODUCTO;
                  
                                               $nota01 = '';                                               
                                               if ($TIPOPRODUCTO == '1'){
                                                $nota01 = '<small style="font-size: 10px;">(*)Fuente: Art. 4 , Decreto Supremo N°017-2017-SA Decreto Supremo N°012-2018 -SA. "Manual de Advertencias Publicitarias en el marco de los establecido en la Ley N° 30021, Ley de Promoción de la alimentación saludable para niños, niñas y adolescentes, y su Reglamento aprobado por Decreto Supremo N°017-2017-SA, Decreto Supremo N°033-2016 SA. "Reglamento que establece el proceso reducción gradual hasta le eliminación de las grasas trans en los alimentos y bebidas no alcohólicas procesados industrialmente.</small>';
                                               }                                               

                                               $html .= '<tr>';
                                               if ($DENSAYO == $DENSAYOins){
                                                  $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                                               }else{
                                                  $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                                               }
                  
                                               $html .= '<td width="100px" align="center">'.$UNIDADMEDIDA.'</td>
                                                   <td width="100px" align="center">'.str_replace("<", "&lt;", $ESP_FINAL).'<sup>'.$EXP10ESP.'</sup></td>
                                                   <td width="140px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'<sup>'.$EXP10RES.'</sup></td>
                                                   <td width="100px" align="center">'.$VALRESULTADO.'</td>
                                               </tr>';
                                               $DENSAYOins = $DENSAYO;
                                           }
                                           $html .= '</tbody></table><br>'.$nota01;
                                       }
                                    /*RESULTADOS SENSORIAL*/
                                     $ressen = $this->minformes->getinfxmuestras_ressenso($parametros);
                                     if ($ressen){
                                         $html .= '<table id="tableborder" style="page-break-inside: avoid;">
                                         <thead>
                                         <tr>
                                             <th> <b>Ensayo</b> </th>
                                             <th> <b>Atributo</b> </th>
                                             <th> <b>Limite permisible</b> </th>
                                             <th> <b>Resultado</b> </th>
                                             <th> <b>Conclusión</b> </th>
                                         </tr>
                                         </thead>
                                         <tbody>';
                                         $DENSAYOsen = '';
                                         foreach($ressen as $rowsen){
                                             $DENSAYO = $rowsen->DENSAYO;
                                             $DNOMBREESCALA = $rowsen->DNOMBREESCALA;
                                             $DLIMITE = $rowsen->DLIMITE;
                                             $RESULT_FINAL = $rowsen->RESULT_FINAL;
                                             $SCONCLUSION = $rowsen->SCONCLUSION;
                                             $html .= '<tr style="vertical-align:top">';
                                             if ($DENSAYO == $DENSAYOsen){
                                                $html .= '';
                                             }else{
                                                $html .= '<td rowspan="5" style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                                             }
                
                                             $html .= '<td width="70px">'.$DNOMBREESCALA.'</td>
                                                 <td width="160px" align="center">'.$DLIMITE.'</td>
                                                 <td width="170px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'</td>
                                                 <td width="100px" align="center">'.$SCONCLUSION.'</td>
                                             </tr>';
                                             $DENSAYOsen = $DENSAYO;
                                         }
                                         $html .= '</tbody></table><br>';
                                     }
                                     
                                    /*RESULTADOS CON ELEMENTOS*/
                                     $resele = $this->minformes->getinfxmuestras_reselementos($parametros);
                                     if ($resele){
                                        $html .= '<table id="tableborder">
                                        <thead>
                                        <tr>
                                            <th> <b>Ensayo</b> </th>
                                            <th> <b>Elemento</b> </th>
                                            <th> <b>Unidades</b> </th>
                                            <th> <b>Limite permisible</b> </th>
                                            <th> <b>Resultado</b> </th>
                                            <th> <b>Conclusión</b> </th>
                                        </tr>
                                        </thead>
                                        <tbody>';
                                        $DENSAYOele = '';
                                        foreach($resele as $rowele){
                                            $DENSAYO = $rowele->NOMBENSAYO;
                                            $ELEMENTO = $rowele->ELEMENTO;
                                            $UNIDADMEDIDA = $rowele->UNIDADMEDIDA;
                                            $LIMITE_FINAL = $rowele->LIMITE_FINAL;
                                            $RESULT_FINAL = $rowele->RESULT_FINAL;
                                            $VALRESULTADO = $rowele->VALRESULTADO;
                                            $html .= '<tr>';
                                            if ($DENSAYO == $DENSAYOele){
                                               $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                                            }else{
                                               $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                                            }
                
                                            $html .= '<td>'.$ELEMENTO.'</td>
                                                <td width="85px" align="center">'.$UNIDADMEDIDA.'</td>
                                                <td width="100px" align="center">'.str_replace("<", "&lt;", $LIMITE_FINAL).'</td>
                                                <td width="100px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'</td>
                                                <td width="100px" align="center">'.$VALRESULTADO.'</td>
                                            </tr>';
                                            $DENSAYOele = $DENSAYO;
                                        }
                                        $html .= '</tbody></table><br>';
                                     }
                                    /*RESULTADOS ESTERILIDAD BAJA*/
                                     $parametrosEsterili = array(
                                        '@cinternoordenservicio' => $cinternoordenservicio,
                                        '@cmuestra' => $CMUESTRA,
                                        '@stipoacidez' => 'B'
                                     );
                                     $resesteb = $this->minformes->getinfxmuestras_resesteri($parametrosEsterili);
                                     if ($resesteb){
                                        $html .= '<table id="tableborder">
                                        <tr>
                                            <td colspan="7"> Baja acidez (pH>4.6)</td>
                                        </tr>
                                        <tr>
                                            <th width="30%" rowspan="2"> <b>Preincubación 35°C/14 días</b> </th>
                                            <th width="5%" rowspan="2"> <b>pH</b> </th>
                                            <th width="25%" colspan="2"> <b>Mesófilos (35°C)</b> </th>
                                            <th width="15%" colspan="2"> <b>Termófilos (55°C)</b> </th>
                                            <th width="25%" rowspan="2"> <b>Resultado</b> </th>
                                        </tr>
                                        <tr>
                                            <th> <b>Caldo púrpura de bromocreasol 120h</b> </th>
                                            <th> <b>Caldo carne cocida 120h</b> </th>
                                            <th> <b>Caldo púrpura de bromocreasol 48h</b> </th>
                                            <th> <b>Caldo carne cocida 72h</b> </th>
                                        </tr>';
                                        foreach($resesteb as $rowesteb){
                                            $preincuba = $rowesteb->preincuba;
                                            $dph = $rowesteb->dph;
                                            $dbames35cp = $rowesteb->dbames35cp;
                                            $dbames35cc = $rowesteb->dbames35cc;
                                            $dbater55cp = $rowesteb->dbater55cp;
                                            $dbater55cc = $rowesteb->dbater55cc;
                                            $result_final = $rowesteb->result_final;
                                            $html .= '<tr>
                                                <td>'.$preincuba.'</td>
                                                <td align="center">'.$dph.'</td>
                                                <td align="center">'.$dbames35cp.'</td>
                                                <td align="center">'.$dbames35cc.'</td>
                                                <td align="center">'.$dbater55cp.'</td>
                                                <td align="center">'.$dbater55cc.'</td>
                                                <td align="center">'.$result_final.'</td>
                                            </tr>';
                                        }
                                        $html .= '</table><br>';
                                     }
                                    /*RESULTADOS ESTERILIDAD ALTA*/
                                     $parametrosEsterili = array(
                                        '@cinternoordenservicio' => $cinternoordenservicio,
                                        '@cmuestra' => $CMUESTRA,
                                        '@stipoacidez' => 'A'
                                     );
                                     $resestea = $this->minformes->getinfxmuestras_resesteri($parametrosEsterili);
                                     if ($resestea){
                                        $html .= '<table id="tableborder">
                                        <tr>
                                            <td colspan="7"> Alta acidez ('.str_replace("<", "&lt;", "pH<4.6").')</td>
                                        </tr>
                                        <tr>
                                            <th width="30%" rowspan="2"> <b>Preincubación 35°C/14 días</b> </th>
                                            <th width="5%" rowspan="2"> <b>pH</b> </th>
                                            <th width="25%" colspan="2"> <b>Mesófilos (35°C)</b> </th>
                                            <th width="15%" colspan="2"> <b>Termófilos (55°C)</b> </th>
                                            <th width="25%" rowspan="2"> <b>Resultado</b> </th>
                                        </tr>
                                        <tr>
                                            <th> <b>Caldo ácido 96h</b> </th>
                                            <th> <b>Caldo extracto de malta 96h</b> </th>
                                            <th colspan="2"> <b>Caldo ácido 48h</b> </th>
                                        </tr>';
                                        foreach($resestea as $rowestea){
                                            $preincuba = $rowestea->preincuba;
                                            $dph = $rowestea->dph;
                                            $dacmes30ca = $rowestea->dacmes30ca;
                                            $dacmes30cm = $rowestea->dacmes30cm;
                                            $dacter55ca = $rowestea->dacter55ca;
                                            $result_final = $rowestea->result_final;
                                            $html .= '<tr>
                                                <td>'.$preincuba.'</td>
                                                <td align="center">'.$dph.'</td>
                                                <td align="center">'.$dacmes30ca.'</td>
                                                <td align="center">'.$dacmes30cm.'</td>
                                                <td align="center" colspan="2">'.$dacter55ca.'</td>
                                                <td align="center">'.$result_final.'</td>
                                            </tr>';
                                        }
                                        $html .= '</table><br>';
                                     }

                                     ///RESULTADOS ETIQUETADO NUTRICIONAL///
                                       $parametrosEtiquetado = array(
                                           '@cinternoordenservicio' => $cinternoordenservicio,
                                           '@cmuestra' => $CMUESTRA
                                       );
                                       $resetiqueta = $this->minformes->getinfxmuestras_resetiquetado($parametrosEtiquetado);
                                       if ($resetiqueta){
                                         $html .= '<table id="tableborder">
                                           <tr>
                                               <td colspan="7"> <b>INFORMACION NUTRICIONAL</b> </td>
                                           </tr>
                                           <tr>
                                               <td width="40%" colspan="4"> Tamaño de Porción :</td>
                                               <td colspan="3">14g</td>
                                           </tr>
                                           <tr>
                                               <th width="25%" colspan="3"> <b>Ensayo</b> </th>
                                               <th width="15%"> <b>Unidad</b> </th>
                                               <th width="20%"> <b>100g</b> </th>
                                               <th width="20%"> <b>Porción</b> </th>
                                               <th width="20%"> <b>% VDR(*)</b> </th>
                                           </tr>';
                                           foreach($resetiqueta as $rowetiqueta){
                                               $DENSAYO = $rowetiqueta->DENSAYO;
                                               $UNIDADMEDIDA = $rowetiqueta->UNIDADMEDIDA;
                                               $DRESULTADO = $rowetiqueta->DRESULTADO;
                                               $NPORCION = $rowetiqueta->NPORCION;
                                               $VALORVR = $rowetiqueta->VALORVR;
                                               $html .= '<tr>
                                                   <td colspan="3">'.$DENSAYO.'</td>
                                                   <td align="center">'.$UNIDADMEDIDA.'</td>
                                                   <td align="center">'.$DRESULTADO.'</td>
                                                   <td align="center">'.number_format($NPORCION, 3, '.', '').'</td>
                                                   <td align="center">'.number_format($VALORVR, 3, '.', '').'</td>
                                               </tr>';
                                           }
                                           $html .= '<tr><td colspan="7">(*) % Valor Diario (VD) con base a una dieta de 2000 kcal u 8370 kJ.  según Codex Alimentarius FAO/OMS. Sus valores diarios pueden ser mayores o menores dependiendo de sus necesidades energéticas.</tr>
                                                      </table><br>';
                                       }
                                    
                                 /*CONCLUSIONES */     
                                  $html .= '<table id="caratula" width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">
                                     <tr>
                                         <td width="3%"> 
                                             <b>5.- </b>  
                                         </td>
                                         <td width="97%" align="left" colspan="3" style="height:30px;">
                                             <b>CONCLUSIONES</b>   
                                         </td>                         
                                     </tr>';
                                      
                                     $parametrosconclu = array(
                                        '@cinternoordenservicio'         => $cinternoordenservicio,
                                        '@cmuestra'       => $CMUESTRA,
                                        '@DDATOSADICICONCLU'       => $DDATOSADICICONCLU,
                                        '@DREALPRODUCTO'       => $DREALPRODUCTO,
                                     );
                                     $resconclu = $this->minformes->getcertixmuestras_conclusion($parametrosconclu);
                                     if ($resconclu){
                                         foreach($resconclu as $rowconclu){
                                             $DCONCLUSION = $rowconclu->DCONCLUSION;
                                             $html .= '<tr>
                                                <td></td>
                                                <td colspan="3">'.$DCONCLUSION.'</td>
                                             </tr>';
                                         }
                                    }
                                    
                                  $html .= '</table><br>'; 
                                 
                                /* FECHA Y FIRMAS*/ 
                                 $resFECHAFIRMA = $this->minformes->getinfxmuestras_fechafirma($parametros);
                                 if ($resFECHAFIRMA){
                                    foreach($resFECHAFIRMA as $rowFECHAFIRMA){
                                        $FECHA        = $rowFECHAFIRMA->FECHA;

                                        $SFQ        = $rowFECHAFIRMA->SFQ;
                                        $SMICRO        = $rowFECHAFIRMA->SMICRO;
                                        $SINSTRU        = $rowFECHAFIRMA->SINSTRU;

                                        $FIRMAFQS        = $rowFECHAFIRMA->FIRMAFQS;
                                        $NOMBREFQS        = $rowFECHAFIRMA->NOMBREFQS;
                                        $CARGOFQS        = $rowFECHAFIRMA->CARGOFQS;
                                        $CODIGOFQS        = $rowFECHAFIRMA->CODIGOFQS;

                                        $FIRMAMICRO        = $rowFECHAFIRMA->FIRMAMICRO;
                                        $NOMBREMICRO        = $rowFECHAFIRMA->NOMBREMICRO;
                                        $CARGOMICRO        = $rowFECHAFIRMA->CARGOMICRO;
                                        $CODIGOMICRO        = $rowFECHAFIRMA->CODIGOMICRO;

                                        $FIRMAINSTRU        = $rowFECHAFIRMA->FIRMAINSTRU;
                                        $NOMBREINSTRU        = $rowFECHAFIRMA->NOMBREINSTRU;
                                        $CARGOINSTRU        = $rowFECHAFIRMA->CARGOINSTRU;
                                        $CODIGOINSTRU        = $rowFECHAFIRMA->CODIGOINSTRU;
                                    }
                                 }
                                
                                 if($SFQ == '1' and $SMICRO == '1' and $SINSTRU == '1'){
                                    $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">                     
                                            <tr>
                                                <td width="100%" align="left" style="font-size: 0.8em;" colspan="2">
                                                Lima, '.$FECHA.'   
                                                </td>
                                            </tr> 
                                            <tr>
                                                <td width="100%" align="center">
                                                <img src="'.public_url_ftp().'Imagenes/firmas/FS000052.jpg" width="190" height="80" />    
                                                </td>
                                                <td width="100%" align="center">
                                                <img src="'.public_url_ftp().'Imagenes/firmas/FS000046.jpg" width="190" height="80" />   
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" align="center" style="font-size: 0.8em;">
                                                <hr width=150>
                                                '.$NOMBREFQS.'    
                                                </td>
                                                <td width="100%" align="center" style="font-size: 0.8em;">
                                                <hr width=150>
                                                '.$NOMBREMICRO.'  
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" align="center" style="font-size: 0.8em;">
                                                '.$CARGOFQS.'
                                                </td>
                                                <td width="100%" align="center" style="font-size: 0.8em;">
                                                '.$CARGOMICRO.'
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" align="center" style="font-size: 0.8em;">
                                                '.$CODIGOFQS.'
                                                </td>
                                                <td width="100%" align="center" style="font-size: 0.8em;">
                                                '.$CODIGOMICRO.'
                                                </td>
                                            </tr>

                                            <tr>
                                                <td width="100%" align="center" colspan="2">
                                                <br>   
                                                <br>  
                                                </td>
                                            </tr>

                                            <tr>
                                                <td width="100%" align="center" colspan="2">
                                                <img src="'.public_url_ftp().'Imagenes/firmas/FS000120.jpg" width="190" height="80" />     
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                                <hr width=150>
                                                '.$NOMBREINSTRU.'     
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                                '.$CARGOINSTRU.'    
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                                '.$CODIGOINSTRU.'    
                                                </td>
                                            </tr>
                                        </tr>
                                    </table>';
                                 }else{
                                    $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">              
                                            <tr>
                                                <td width="100%" align="left" style="font-size: 0.8em;">
                                                    Lima, '.$FECHA.'   
                                                </td>
                                            </tr> 
                                            <tr>';
                                            if($SFQ == '1'){
                                                $html .= '<td width="100%" align="center">
                                                    <img src="'.public_url_ftp().'Imagenes/firmas/FS000052.jpg" width="190" height="80" />    
                                                    </td>';
                                            }
                                            if($SMICRO == '1'){
                                                $html .= '<td width="100%" align="center">
                                                    <img src="'.public_url_ftp().'Imagenes/firmas/FS000046.jpg" width="190" height="80" />   
                                                    </td>';
                                            }
                                            if($SINSTRU == '1'){
                                                $html .= '<td width="100%" align="center" colspan="2">
                                                    <img src="'.public_url_ftp().'Imagenes/firmas/FS000120.jpg" width="190" height="80" />     
                                                    </td>';
                                            }
                                    $html .= '</tr>
                                            <tr>';
                                            if($SFQ == '1'){
                                                $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                                <hr width=150>
                                                '.$NOMBREFQS.'    
                                                </td>';
                                            }
                                            if($SMICRO == '1'){
                                                $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                                <hr width=150>
                                                '.$NOMBREMICRO.'  
                                                </td>';
                                            }
                                            if($SINSTRU == '1'){
                                                $html .= '<td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                                <hr width=150>
                                                '.$NOMBREINSTRU.'     
                                                </td>';
                                            }
                                    $html .= '</tr>
                                            <tr>';
                                            if($SFQ == '1'){
                                                $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                                '.$CARGOFQS.'
                                                </td>';
                                            }
                                            if($SMICRO == '1'){
                                                $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                                '.$CARGOMICRO.'
                                                </td>';
                                            }
                                            if($SINSTRU == '1'){
                                                $html .= '<td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                                '.$CARGOINSTRU.'    
                                                </td>';
                                            }
                                    $html .= '</tr>
                                            <tr>';
                                            if($SFQ == '1'){
                                                $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                                '.$CODIGOFQS.'
                                                </td>';
                                            }
                                            if($SMICRO == '1'){
                                                $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                                '.$CODIGOMICRO.'
                                                </td>';
                                            }
                                            if($SINSTRU == '1'){
                                                $html .= '<td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                                '.$CODIGOINSTRU.'    
                                                </td>';
                                            }
                                    $html .= '</tr>
                                    </table>';
                                 }     
        
                                $html .= '</div>';
                                                  $pos++;
                                             }}
                                $html .= '</body>
                                          </html>';
		
        $filename = 'CC '.substr($NROCERTI, 0, 9).' '.$CLIENTE.' '.$CMUESTRA;
        $this->pdfgenerator->generate($html, $filename, TRUE, 'A4', 'portrait');
        //echo $html;
    }    
    
    public function pdfCertificadoIndiv($cinternoordenservicio,$vcmuestra) { // recupera 
        $this->load->library('pdfgenerator');

        $html = '<html>
                <head>
                    <title>Certificado</title>
                    <style type="text/css">
                        @page{
                             margin: 0.3in 0.3in 0.3in 0.3in;
                        }
                        body{
                            font-family: Arial, Helvetica, sans-serif;
                            margin-top: 3.5cm;
                            margin-left: 1cm;
                            margin-right: 1cm;
                            margin-bottom: 2cm;
                        }
                        #header,
                        #footer,
                        #nroinf {
                            position: fixed;
                            left: 0;
                            right: 0;
                            color: #000000;
                            font-size: 0.9em;
                        }                        
                        #header {
                            top: 0;
                            height: 4cm;
                        }    
                        #nroinf {
                            top: 2.5cm;
                            height: 2cm;
                        }                     
                        #footer {
                            bottom: 0;
                            border-top: 0.1pt solid #aaa;
                            height: 1cm;
                        }                        
                        #header table,
                        #footer table,
                        #nroinf table {
                            width: 100%;
                            border-collapse: collapse;
                            border: none;
                        }                        
                        #header td,
                        #footer td,
                        #nroinf td {
                            padding: 0;
                        }
                        .page-number{
                            text-align: right;
                        }
                        .page-number:before{
                            content: "Pagina " counter(page);
                        }
                        hr1{
                            page-break-before: always;
                            color: #ffffff;
                            margin: -2px;
                        }     
                        #caratula td{
                            font-size:0.8em;        
                        }
                        #tableborder{
                            border-collapse: collapse;
                            width: 100%;
                            font-size:0.8em;
                            border: 0.5pt solid black;
                        }
                        #tableborder td{
                            border: 0.5pt solid black;
                            padding-left: 5px;
                        }
                        #tableborder th{
                            text-align: center;
                            border: 0.5pt solid black;
                            background-color: #DBD7D7;
                        }
                        .list-unstyled{
                            padding-left: 0;
                            list-style: none;
                        }
                    </style>
                </head>
                <body> 
                <div id="header">
                     <table width="100%" align="center">
                         <tr>
                             <td width="25%" align="center">                            
                                 <img src="'.public_url_ftp().'Imagenes/formatos/2/logoFSC.jpg" width="180" height="95" />  
                             </td>   
                             <td width="100%" align="right" colspan="2" style="font-size: 0.8em;">
                                 <ul class="list-unstyled">
                                     <li>Jr. Monterrey N° 221 Of. 201-202</li>
                                     <li>Urb. Chacarilla del Estanque. Santiago de Surco, Lima - Perú</li>
                                     <li>Teléfonos: (51-1) 480 0561 - Anexo: Lab 111</li>
                                     <li>www.fscertificaciones.com</li>
                                 </ul>
                             </td>
                         </tr>
                     </table>
                </div>   
                <div id="footer">
                     <table width="100%" style="font-size:0.8em; margin-top: 0.3cm; text-align: justify">
                     <tr>
                         <td width="100%" align="right" >
                             <div class="page-number"></div>
                         </td>
                     </tr>
                     </table>                    
                </div>'; 
                $parametroscabe = array(
                    '@cinternoordenservicio'         => $cinternoordenservicio,
                    '@cmuestra'       => $vcmuestra,
                );
                $res = $this->minformes->getcertixmuestras_caratula($parametroscabe);
                $pos = 0;
                if ($res){
                    $NROCERTI       = $res[0]->NROCERTI;
                    foreach($res as $row){ 
                        $NROINFORME     = $row->NROINFORME;
                        
                        $CLIENTE        = $row->CLIENTE;
                        $DIRECCION      = $row->DIRECCION;
                        $NROORDEN       = $row->NROORDEN;
                        $PROCEDENCIA    = $row->PROCEDENCIA;
                        $FMUESTRA       = $row->FMUESTRA;
                        $FRECEPCION     = $row->FRECEPCION;
                        $FANALISIS      = $row->FANALISIS;
                        $LUGARMUESTRA   = $row->LUGARMUESTRA;
                        $CMUESTRA       = $row->CMUESTRA;
                        $DPRODUCTO      = $row->DPRODUCTO;
                        $DTEMPERATURA   = $row->DTEMPERATURA;
                        $DLCLAB         = $row->DLCLAB;
                        $OBSERVACION    = $row->OBSERVACION;
                        $SOBSERV        = $row->SOBSERV;
                        $ACNAC          = $row->ACNAC;
                        $DDATOSADICICONCLU = $row->DDATOSADICICONCLU;
                        $DREALPRODUCTO = $row->DREALPRODUCTO;

                        if($pos > 0){
                            $html .= '<hr style="page-break-before: always;" size="0" noshade="noshade" />';                          
                        }  
                        $html .= '   <div id="header">
                                    <table width="100%" align="center">   
                                    <tr>
                                        <td width="25%" align="center">                            
                                            &nbsp;  
                                        </td>   
                                        <td width="100%" align="right" colspan="2" style="font-size: 0.8em;">
                                            <ul class="list-unstyled">
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                            </ul>
                                        </td>
                                    </tr>                                 
                                    <tr>
                                        <td colspan="3" style="height:63px; font-size:1.1em;" align="center">
                                            <b>CERTIFICADO DE CALIDAD N°'.$NROCERTI.'  </b>
                                        </td>
                                    </tr>
                                    </table>
                                </div>';
                        $parametros = array(
                            '@cinternoordenservicio'    => $cinternoordenservicio,
                            '@cmuestra'                 => $CMUESTRA,
                        );
                                             
                        
                       
                        $html .= '<div id="main">
                                           
                                    <table id="caratula" width="100%" align="center" cellspacing="0" cellpadding="2" style="margin-top:20px">  
                                        <tr>
                                            <td> 
                                                <b>1.- </b>  
                                            </td>
                                            <td colspan="3" align="left">
                                                <b> DATOS GENERALES</b>   
                                            </td>
                                        </tr>                       
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>Nombre del Cliente</b>   
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$CLIENTE.'   
                                            </td>
                                        </tr>
                                        <tr style="vertical-align:top">
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>Dirección del Cliente</b>  
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$DIRECCION.'   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>N° Orden de Trabajo</b>   
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$NROORDEN.'   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>Procedencia de la Muestra</b>  
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$PROCEDENCIA.'   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>Fecha de Muestreo</b>    
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$FMUESTRA.'   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>Fecha de Recepción</b>    
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$FRECEPCION.'<   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>Fecha de Análisis</b>    
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$FANALISIS.'  
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>Lugar de Muestreo</b>   
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$LUGARMUESTRA.'  
                                            </td>
                                        </tr>
                                        <tr style="vertical-align:top">
                                            <td width="3%" align="left">   
                                            </td>
                                            <td width="27%" align="left">
                                                <b>Muestra / Descripción</b>    
                                            </td>
                                            <td width="7%" align="left">
                                                : '.$CMUESTRA.'   
                                            </td>
                                            <td width="63%" align="left">
                                                '.$DPRODUCTO.'   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>Temperatura de Recepción</b>    
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$DTEMPERATURA.'   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>N° Informe de Ensayo</b>   
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$NROINFORME.'   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="4">
                                            &nbsp;   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td> 
                                                <b>2.- </b>  
                                            </td>
                                            <td align="left" colspan="3" style="height:30px;">
                                                <b>DOCUMENTOS NORMATIVOS</b>   
                                            </td>
                                        </tr>';
                                        $resdocnorma = $this->minformes->getdocnormativo($cinternoordenservicio,$CMUESTRA);
                                        $idocnorma = 1;
                                        if ($resdocnorma){
                                            foreach($resdocnorma as $rowdocnorma){
                                                $cnormalab = $rowdocnorma->cnormalab;
                                                $cgruponormalab = $rowdocnorma->cgruponormalab;
                                                $dnormalab = $rowdocnorma->dnormalab;
                                                $html .= '<tr style="vertical-align:top">
                                                   <td>2.'.$idocnorma.'</td>
                                                   <td colspan="3">'.$dnormalab.'</td>
                                                </tr>';
                                                $idocnorma ++ ;
                                                $resdocnorma_det = $this->minformes->getdocnormativo_det($cnormalab,$cgruponormalab);
                                                if ($resdocnorma_det){
                                                    foreach($resdocnorma_det as $rowdocnorma_det){
                                                        $dnumerogrupo = $rowdocnorma_det->dnumerogrupo;
                                                        $dnombregrupo = $rowdocnorma_det->dnombregrupo;
                                                        if($dnumerogrupo <> '0'){
                                                            $html .= '<tr>
                                                            <td></td>
                                                            <td colspan="3">'.$dnumerogrupo.' &nbsp; &nbsp; '.$dnombregrupo.'</td>
                                                            </tr>';
                                                        }
                                                    }
                                                } 
                                            }
                                        } 
                                        $html .= '
                                        <tr>
                                           <td > 
                                               <b>3.- </b>  
                                           </td>
                                            <td align="left" colspan="3" style="height:50px;">
                                                <b>METODOS DE ENSAYO</b>   
                                            </td>
                                        </tr>
                                    </table>';
                                     
                                    /*RESULTADOS METODOS DE ENSAYO*/    
                                     $resmetensa = $this->minformes->getmetodosensayos($parametros);
                                      if ($resmetensa){
                                        $html .= '<table id="tableborder" style="padding-left: 15px;">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <b>Ensayo</b>   
                                                </th>
                                                <th>
                                                    <b>Norma o Referencia</b>    
                                                </td>
                                            </tr>
                                            </thead>
                                            <tbody>';
                                            foreach($resmetensa as $rowmetensa){
                                                $METDENSAYO = $rowmetensa->DENSAYO;
                                                $METDNORMA = $rowmetensa->DNORMA;
                                                $html .= '<tr>
                                                   <td width="180px">'.$METDENSAYO.'</td>
                                                   <td style="vertical-align:top">'.$METDNORMA.'</td>
                                                </tr>';
                                            }
                                        $html .= '</tbody></table><br>';
                                     }  
                
                                     $html .= '<table id="caratula" width="713px" align="center" cellspacing="0" cellpadding="2" >
                                       <tr>
                                           <td width="3%"> 
                                               <b>4.- </b>  
                                           </td>
                                           <td width="97%" align="left" colspan="3" style="height:50px;">
                                               <b>RESULTADOS E INTERPRETACIONES</b>   
                                           </td>
                                       </tr>
                                     </table>'; 
                
                                    /*RESULTADOS MICROBIOLOGIA*/
                                     $resmicro = $this->minformes->getinfxmuestras_resmicro($parametros);
                                     if ($resmicro){
                                         $html .= '<table id="tableborder">
                                         <thead>
                                         <tr>
                                             <th> <b>Ensayo</b> </th>
                                             <th> <b>Unidades</b> </th>
                                             <th> <b>Limite permisible</b> </th>
                                             <th> <b>Resultado</b> </th>
                                             <th> <b>Conclusión</b> </th>
                                         </tr>
                                         </thead>
                                         <tbody>';
                                         $DENSAYOmicro = '';
                                         foreach($resmicro as $rowmicro){
                                             $DENSAYO = $rowmicro->NOMBENSAYO;
                                             $UNIDADMEDIDA = $rowmicro->UNIDADMEDIDA;
                                             $ESP_FINAL = $rowmicro->ESP_FINAL;
                                             $RESULT_FINAL = $rowmicro->RESULT_FINAL;
                                             $VALRESULTADO = $rowmicro->VALRESULTADO;
                                             $EXP10RES = $rowmicro->EXP10RES;
                                             $EXP10ESP = $rowmicro->EXP10ESP;

                                             $html .= '<tr>';
                                             if ($DENSAYO == $DENSAYOmicro){
                                                $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                                             }else{
                                                $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                                             }
                                             
                                             $html .= '<td width="100px" align="center">'.$UNIDADMEDIDA.'</td>
                                                 <td width="100px" align="center">'.str_replace("<", "&lt;", $ESP_FINAL).'<sup>'.$EXP10ESP.'</sup></td>
                                                 <td width="140px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'<sup>'.$EXP10RES.'</sup></td>
                                                 <td width="100px" align="center">'.$VALRESULTADO.'</td>
                                             </tr>';
                                             $DENSAYOmicro = $DENSAYO;
                                         }
                                         $html .= '</tbody></table><br>';
                                     }  
                                    /*RESULTADOS FISICOQUIMICO*/
                                     $resfq = $this->minformes->getinfxmuestras_resfq($parametros);
                                     if ($resfq){
                                         $html .= '<table id="tableborder">
                                         <thead>
                                         <tr>
                                             <th> <b>Ensayo</b> </th>
                                             <th> <b>Unidades</b> </th>
                                             <th> <b>Limite permisible</b> </th>
                                             <th> <b>Resultado</b> </th>
                                             <th> <b>Conclusión</b> </th>
                                         </tr>
                                         </thead>
                                         <tbody>';
                                         $DENSAYOfq = '';
                                         foreach($resfq as $rowfq){
                                             $DENSAYO = $rowfq->NOMBENSAYO;
                                             $UNIDADMEDIDA = $rowfq->UNIDADMEDIDA;
                                             $ESP_FINAL = $rowfq->ESP_FINAL;
                                             $RESULT_FINAL = $rowfq->RESULT_FINAL;
                                             $VALRESULTADO = $rowfq->VALRESULTADO;
                                             $EXP10ESP = $rowfq->EXP10ESP;
                                             $EXP10RES = $rowfq->EXP10RES;

                                             $html .= '<tr>';
                                             if ($DENSAYO == $DENSAYOfq){
                                                $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                                             }else{
                                                $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                                             }
                
                                             $html .= '<td width="100px" align="center">'.$UNIDADMEDIDA.'</td>
                                                 <td width="100px" align="center">'.str_replace("<", "&lt;", $ESP_FINAL).'<sup>'.$EXP10ESP.'</sup></td>
                                                 <td width="140px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'<sup>'.$EXP10RES.'</sup></td>
                                                 <td width="100px" align="center">'.$VALRESULTADO.'</td>
                                             </tr>';
                                             $DENSAYOfq = $DENSAYO;
                                         }
                                         $html .= '</tbody></table><br>';
                                     }
                                    ///RESULTADOS INSTRUMENTAL TERCERO///
                                       $resins = $this->minformes->getinfxmuestras_resinstru($parametros);
                                       if ($resins){
                                           $html .= '<table id="tableborder">
                                           <thead>
                                           <tr>
                                                <th> <b>Ensayo</b> </th>
                                                <th> <b>Unidades</b> </th>
                                                <th> <b>Limite permisible</b> </th>
                                                <th> <b>Resultado</b> </th>
                                                <th> <b>Conclusión</b> </th>
                                           </tr>
                                           </thead>
                                           <tbody>';
                                           $DENSAYOins = '';
                                           foreach($resins as $rowins){
                                               $DENSAYO = $rowins->DENSAYO;
                                               $UNIDADMEDIDA = $rowins->UNIDADMEDIDA;
                                               $ESP_FINAL = $rowins->ESP_FINAL;
                                               $RESULT_FINAL = $rowins->RESULT_FINAL;
                                               $VALRESULTADO = $rowins->VALRESULTADO;
                                               $EXP10ESP = $rowins->EXP10ESP;
                                               $EXP10RES = $rowins->EXP10RES;
                                               $TIPOPRODUCTO = $rowins->TIPOPRODUCTO;
                  
                                               $nota01 = '';                                               
                                               if ($TIPOPRODUCTO == '1'){
                                                $nota01 = '<small style="font-size: 10px;">(*)Fuente: Art. 4 , Decreto Supremo N°017-2017-SA Decreto Supremo N°012-2018 -SA. "Manual de Advertencias Publicitarias en el marco de los establecido en la Ley N° 30021, Ley de Promoción de la alimentación saludable para niños, niñas y adolescentes, y su Reglamento aprobado por Decreto Supremo N°017-2017-SA, Decreto Supremo N°033-2016 SA. "Reglamento que establece el proceso reducción gradual hasta le eliminación de las grasas trans en los alimentos y bebidas no alcohólicas procesados industrialmente.</small>';
                                               }                                               

                                               $html .= '<tr>';
                                               if ($DENSAYO == $DENSAYOins){
                                                  $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                                               }else{
                                                  $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                                               }
                  
                                               $html .= '<td width="100px" align="center">'.$UNIDADMEDIDA.'</td>
                                                   <td width="100px" align="center">'.str_replace("<", "&lt;", $ESP_FINAL).'<sup>'.$EXP10ESP.'</sup></td>
                                                   <td width="140px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'<sup>'.$EXP10RES.'</sup></td>
                                                   <td width="100px" align="center">'.$VALRESULTADO.'</td>
                                               </tr>';
                                               $DENSAYOins = $DENSAYO;
                                           }
                                           $html .= '</tbody></table><br>'.$nota01;
                                       }
                                    /*RESULTADOS SENSORIAL*/
                                     $ressen = $this->minformes->getinfxmuestras_ressenso($parametros);
                                     if ($ressen){
                                         $html .= '<table id="tableborder" style="page-break-inside: avoid;">
                                         <thead>
                                         <tr>
                                             <th> <b>Ensayo</b> </th>
                                             <th> <b>Atributo</b> </th>
                                             <th> <b>Limite permisible</b> </th>
                                             <th> <b>Resultado</b> </th>
                                             <th> <b>Conclusión</b> </th>
                                         </tr>
                                         </thead>
                                         <tbody>';
                                         $DENSAYOsen = '';
                                         foreach($ressen as $rowsen){
                                             $DENSAYO = $rowsen->DENSAYO;
                                             $DNOMBREESCALA = $rowsen->DNOMBREESCALA;
                                             $DLIMITE = $rowsen->DLIMITE;
                                             $RESULT_FINAL = $rowsen->RESULT_FINAL;
                                             $SCONCLUSION = $rowsen->SCONCLUSION;
                                             $html .= '<tr style="vertical-align:top">';
                                             if ($DENSAYO == $DENSAYOsen){
                                                $html .= '';
                                             }else{
                                                $html .= '<td rowspan="5" style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                                             }
                
                                             $html .= '<td width="70px">'.$DNOMBREESCALA.'</td>
                                                 <td width="160px" align="center">'.$DLIMITE.'</td>
                                                 <td width="170px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'</td>
                                                 <td width="100px" align="center">'.$SCONCLUSION.'</td>
                                             </tr>';
                                             $DENSAYOsen = $DENSAYO;
                                         }
                                         $html .= '</tbody></table><br>';
                                     }
                                     
                                    /*RESULTADOS CON ELEMENTOS*/
                                     $resele = $this->minformes->getinfxmuestras_reselementos($parametros);
                                     if ($resele){
                                        $html .= '<table id="tableborder">
                                        <thead>
                                        <tr>
                                            <th> <b>Ensayo</b> </th>
                                            <th> <b>Elemento</b> </th>
                                            <th> <b>Unidades</b> </th>
                                            <th> <b>Limite permisible</b> </th>
                                            <th> <b>Resultado</b> </th>
                                            <th> <b>Conclusión</b> </th>
                                        </tr>
                                        </thead>
                                        <tbody>';
                                        $DENSAYOele = '';
                                        foreach($resele as $rowele){
                                            $DENSAYO = $rowele->NOMBENSAYO;
                                            $ELEMENTO = $rowele->ELEMENTO;
                                            $UNIDADMEDIDA = $rowele->UNIDADMEDIDA;
                                            $LIMITE_FINAL = $rowele->LIMITE_FINAL;
                                            $RESULT_FINAL = $rowele->RESULT_FINAL;
                                            $VALRESULTADO = $rowele->VALRESULTADO;
                                            $html .= '<tr>';
                                            if ($DENSAYO == $DENSAYOele){
                                               $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                                            }else{
                                               $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                                            }
                
                                            $html .= '<td>'.$ELEMENTO.'</td>
                                                <td width="85px" align="center">'.$UNIDADMEDIDA.'</td>
                                                <td width="100px" align="center">'.str_replace("<", "&lt;", $LIMITE_FINAL).'</td>
                                                <td width="100px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'</td>
                                                <td width="100px" align="center">'.$VALRESULTADO.'</td>
                                            </tr>';
                                            $DENSAYOele = $DENSAYO;
                                        }
                                        $html .= '</tbody></table><br>';
                                     }
                                    /*RESULTADOS ESTERILIDAD BAJA*/
                                     $parametrosEsterili = array(
                                        '@cinternoordenservicio' => $cinternoordenservicio,
                                        '@cmuestra' => $CMUESTRA,
                                        '@stipoacidez' => 'B'
                                     );
                                     $resesteb = $this->minformes->getinfxmuestras_resesteri($parametrosEsterili);
                                     if ($resesteb){
                                        $html .= '<table id="tableborder">
                                        <tr>
                                            <td colspan="7"> Baja acidez (pH>4.6)</td>
                                        </tr>
                                        <tr>
                                            <th width="30%" rowspan="2"> <b>Preincubación 35°C/14 días</b> </th>
                                            <th width="5%" rowspan="2"> <b>pH</b> </th>
                                            <th width="25%" colspan="2"> <b>Mesófilos (35°C)</b> </th>
                                            <th width="15%" colspan="2"> <b>Termófilos (55°C)</b> </th>
                                            <th width="25%" rowspan="2"> <b>Resultado</b> </th>
                                        </tr>
                                        <tr>
                                            <th> <b>Caldo púrpura de bromocreasol 120h</b> </th>
                                            <th> <b>Caldo carne cocida 120h</b> </th>
                                            <th> <b>Caldo púrpura de bromocreasol 48h</b> </th>
                                            <th> <b>Caldo carne cocida 72h</b> </th>
                                        </tr>';
                                        foreach($resesteb as $rowesteb){
                                            $preincuba = $rowesteb->preincuba;
                                            $dph = $rowesteb->dph;
                                            $dbames35cp = $rowesteb->dbames35cp;
                                            $dbames35cc = $rowesteb->dbames35cc;
                                            $dbater55cp = $rowesteb->dbater55cp;
                                            $dbater55cc = $rowesteb->dbater55cc;
                                            $result_final = $rowesteb->result_final;
                                            $html .= '<tr>
                                                <td>'.$preincuba.'</td>
                                                <td align="center">'.$dph.'</td>
                                                <td align="center">'.$dbames35cp.'</td>
                                                <td align="center">'.$dbames35cc.'</td>
                                                <td align="center">'.$dbater55cp.'</td>
                                                <td align="center">'.$dbater55cc.'</td>
                                                <td align="center">'.$result_final.'</td>
                                            </tr>';
                                        }
                                        $html .= '</table><br>';
                                     }
                                    /*RESULTADOS ESTERILIDAD ALTA*/
                                     $parametrosEsterili = array(
                                        '@cinternoordenservicio' => $cinternoordenservicio,
                                        '@cmuestra' => $CMUESTRA,
                                        '@stipoacidez' => 'A'
                                     );
                                     $resestea = $this->minformes->getinfxmuestras_resesteri($parametrosEsterili);
                                     if ($resestea){
                                        $html .= '<table id="tableborder">
                                        <tr>
                                            <td colspan="7"> Alta acidez ('.str_replace("<", "&lt;", "pH<4.6").')</td>
                                        </tr>
                                        <tr>
                                            <th width="30%" rowspan="2"> <b>Preincubación 35°C/14 días</b> </th>
                                            <th width="5%" rowspan="2"> <b>pH</b> </th>
                                            <th width="25%" colspan="2"> <b>Mesófilos (35°C)</b> </th>
                                            <th width="15%" colspan="2"> <b>Termófilos (55°C)</b> </th>
                                            <th width="25%" rowspan="2"> <b>Resultado</b> </th>
                                        </tr>
                                        <tr>
                                            <th> <b>Caldo ácido 96h</b> </th>
                                            <th> <b>Caldo extracto de malta 96h</b> </th>
                                            <th colspan="2"> <b>Caldo ácido 48h</b> </th>
                                        </tr>';
                                        foreach($resestea as $rowestea){
                                            $preincuba = $rowestea->preincuba;
                                            $dph = $rowestea->dph;
                                            $dacmes30ca = $rowestea->dacmes30ca;
                                            $dacmes30cm = $rowestea->dacmes30cm;
                                            $dacter55ca = $rowestea->dacter55ca;
                                            $result_final = $rowestea->result_final;
                                            $html .= '<tr>
                                                <td>'.$preincuba.'</td>
                                                <td align="center">'.$dph.'</td>
                                                <td align="center">'.$dacmes30ca.'</td>
                                                <td align="center">'.$dacmes30cm.'</td>
                                                <td align="center" colspan="2">'.$dacter55ca.'</td>
                                                <td align="center">'.$result_final.'</td>
                                            </tr>';
                                        }
                                        $html .= '</table><br>';
                                     }

                                     ///RESULTADOS ETIQUETADO NUTRICIONAL///
                                       $parametrosEtiquetado = array(
                                           '@cinternoordenservicio' => $cinternoordenservicio,
                                           '@cmuestra' => $CMUESTRA
                                       );
                                       $resetiqueta = $this->minformes->getinfxmuestras_resetiquetado($parametrosEtiquetado);
                                       if ($resetiqueta){
                                         $html .= '<table id="tableborder">
                                           <tr>
                                               <td colspan="7"> <b>INFORMACION NUTRICIONAL</b> </td>
                                           </tr>
                                           <tr>
                                               <td width="40%" colspan="4"> Tamaño de Porción :</td>
                                               <td colspan="3">14g</td>
                                           </tr>
                                           <tr>
                                               <th width="25%" colspan="3"> <b>Ensayo</b> </th>
                                               <th width="15%"> <b>Unidad</b> </th>
                                               <th width="20%"> <b>100g</b> </th>
                                               <th width="20%"> <b>Porción</b> </th>
                                               <th width="20%"> <b>% VDR(*)</b> </th>
                                           </tr>';
                                           foreach($resetiqueta as $rowetiqueta){
                                               $DENSAYO = $rowetiqueta->DENSAYO;
                                               $UNIDADMEDIDA = $rowetiqueta->UNIDADMEDIDA;
                                               $DRESULTADO = $rowetiqueta->DRESULTADO;
                                               $NPORCION = $rowetiqueta->NPORCION;
                                               $VALORVR = $rowetiqueta->VALORVR;
                                               $html .= '<tr>
                                                   <td colspan="3">'.$DENSAYO.'</td>
                                                   <td align="center">'.$UNIDADMEDIDA.'</td>
                                                   <td align="center">'.$DRESULTADO.'</td>
                                                   <td align="center">'.number_format($NPORCION, 3, '.', '').'</td>
                                                   <td align="center">'.number_format($VALORVR, 3, '.', '').'</td>
                                               </tr>';
                                           }
                                           $html .= '<tr><td colspan="7">(*) % Valor Diario (VD) con base a una dieta de 2000 kcal u 8370 kJ.  según Codex Alimentarius FAO/OMS. Sus valores diarios pueden ser mayores o menores dependiendo de sus necesidades energéticas.</tr>
                                                      </table><br>';
                                       }
                                    
                                 /*CONCLUSIONES */     
                                  $html .= '<table id="caratula" width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">
                                     <tr>
                                         <td width="3%"> 
                                             <b>5.- </b>  
                                         </td>
                                         <td width="97%" align="left" colspan="3" style="height:30px;">
                                             <b>CONCLUSIONES</b>   
                                         </td>                         
                                     </tr>';
                                      
                                     $parametrosconclu = array(
                                        '@cinternoordenservicio'         => $cinternoordenservicio,
                                        '@cmuestra'       => $CMUESTRA,
                                        '@DDATOSADICICONCLU'       => $DDATOSADICICONCLU,
                                        '@DREALPRODUCTO'       => $DREALPRODUCTO,
                                     );
                                     $resconclu = $this->minformes->getcertixmuestras_conclusion($parametrosconclu);
                                     if ($resconclu){
                                         foreach($resconclu as $rowconclu){
                                             $DCONCLUSION = $rowconclu->DCONCLUSION;
                                             $html .= '<tr>
                                                <td></td>
                                                <td colspan="3">'.$DCONCLUSION.'</td>
                                             </tr>';
                                         }
                                    }
                                    
                                  $html .= '</table><br>'; 
                                 
                                /* FECHA Y FIRMAS*/ 
                                 $resFECHAFIRMA = $this->minformes->getinfxmuestras_fechafirma($parametros);
                                 if ($resFECHAFIRMA){
                                    foreach($resFECHAFIRMA as $rowFECHAFIRMA){
                                        $FECHA        = $rowFECHAFIRMA->FECHA;

                                        $SFQ        = $rowFECHAFIRMA->SFQ;
                                        $SMICRO        = $rowFECHAFIRMA->SMICRO;
                                        $SINSTRU        = $rowFECHAFIRMA->SINSTRU;

                                        $FIRMAFQS        = $rowFECHAFIRMA->FIRMAFQS;
                                        $NOMBREFQS        = $rowFECHAFIRMA->NOMBREFQS;
                                        $CARGOFQS        = $rowFECHAFIRMA->CARGOFQS;
                                        $CODIGOFQS        = $rowFECHAFIRMA->CODIGOFQS;

                                        $FIRMAMICRO        = $rowFECHAFIRMA->FIRMAMICRO;
                                        $NOMBREMICRO        = $rowFECHAFIRMA->NOMBREMICRO;
                                        $CARGOMICRO        = $rowFECHAFIRMA->CARGOMICRO;
                                        $CODIGOMICRO        = $rowFECHAFIRMA->CODIGOMICRO;

                                        $FIRMAINSTRU        = $rowFECHAFIRMA->FIRMAINSTRU;
                                        $NOMBREINSTRU        = $rowFECHAFIRMA->NOMBREINSTRU;
                                        $CARGOINSTRU        = $rowFECHAFIRMA->CARGOINSTRU;
                                        $CODIGOINSTRU        = $rowFECHAFIRMA->CODIGOINSTRU;
                                    }
                                 }
                                
                                 if($SFQ == '1' and $SMICRO == '1' and $SINSTRU == '1'){
                                    $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">                     
                                            <tr>
                                                <td width="100%" align="left" style="font-size: 0.8em;" colspan="2">
                                                Lima, '.$FECHA.'   
                                                </td>
                                            </tr> 
                                            <tr>
                                                <td width="100%" align="center">
                                                <img src="'.public_url_ftp().'Imagenes/firmas/FS000052.jpg" width="190" height="80" />    
                                                </td>
                                                <td width="100%" align="center">
                                                <img src="'.public_url_ftp().'Imagenes/firmas/FS000046.jpg" width="190" height="80" />   
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" align="center" style="font-size: 0.8em;">
                                                <hr width=150>
                                                '.$NOMBREFQS.'    
                                                </td>
                                                <td width="100%" align="center" style="font-size: 0.8em;">
                                                <hr width=150>
                                                '.$NOMBREMICRO.'  
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" align="center" style="font-size: 0.8em;">
                                                '.$CARGOFQS.'
                                                </td>
                                                <td width="100%" align="center" style="font-size: 0.8em;">
                                                '.$CARGOMICRO.'
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" align="center" style="font-size: 0.8em;">
                                                '.$CODIGOFQS.'
                                                </td>
                                                <td width="100%" align="center" style="font-size: 0.8em;">
                                                '.$CODIGOMICRO.'
                                                </td>
                                            </tr>

                                            <tr>
                                                <td width="100%" align="center" colspan="2">
                                                <br>   
                                                <br>  
                                                </td>
                                            </tr>

                                            <tr>
                                                <td width="100%" align="center" colspan="2">
                                                <img src="'.public_url_ftp().'Imagenes/firmas/FS000120.jpg" width="190" height="80" />     
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                                <hr width=150>
                                                '.$NOMBREINSTRU.'     
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                                '.$CARGOINSTRU.'    
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                                '.$CODIGOINSTRU.'    
                                                </td>
                                            </tr>
                                        </tr>
                                    </table>';
                                 }else{
                                    $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">              
                                            <tr>
                                                <td width="100%" align="left" style="font-size: 0.8em;">
                                                    Lima, '.$FECHA.'   
                                                </td>
                                            </tr> 
                                            <tr>';
                                            if($SFQ == '1'){
                                                $html .= '<td width="100%" align="center">
                                                    <img src="'.public_url_ftp().'Imagenes/firmas/FS000052.jpg" width="190" height="80" />    
                                                    </td>';
                                            }
                                            if($SMICRO == '1'){
                                                $html .= '<td width="100%" align="center">
                                                    <img src="'.public_url_ftp().'Imagenes/firmas/FS000046.jpg" width="190" height="80" />   
                                                    </td>';
                                            }
                                            if($SINSTRU == '1'){
                                                $html .= '<td width="100%" align="center" colspan="2">
                                                    <img src="'.public_url_ftp().'Imagenes/firmas/FS000120.jpg" width="190" height="80" />     
                                                    </td>';
                                            }
                                    $html .= '</tr>
                                            <tr>';
                                            if($SFQ == '1'){
                                                $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                                <hr width=150>
                                                '.$NOMBREFQS.'    
                                                </td>';
                                            }
                                            if($SMICRO == '1'){
                                                $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                                <hr width=150>
                                                '.$NOMBREMICRO.'  
                                                </td>';
                                            }
                                            if($SINSTRU == '1'){
                                                $html .= '<td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                                <hr width=150>
                                                '.$NOMBREINSTRU.'     
                                                </td>';
                                            }
                                    $html .= '</tr>
                                            <tr>';
                                            if($SFQ == '1'){
                                                $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                                '.$CARGOFQS.'
                                                </td>';
                                            }
                                            if($SMICRO == '1'){
                                                $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                                '.$CARGOMICRO.'
                                                </td>';
                                            }
                                            if($SINSTRU == '1'){
                                                $html .= '<td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                                '.$CARGOINSTRU.'    
                                                </td>';
                                            }
                                    $html .= '</tr>
                                            <tr>';
                                            if($SFQ == '1'){
                                                $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                                '.$CODIGOFQS.'
                                                </td>';
                                            }
                                            if($SMICRO == '1'){
                                                $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                                '.$CODIGOMICRO.'
                                                </td>';
                                            }
                                            if($SINSTRU == '1'){
                                                $html .= '<td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                                '.$CODIGOINSTRU.'    
                                                </td>';
                                            }
                                    $html .= '</tr>
                                    </table>';
                                 }     
        
                                $html .= '</div>';
                                                  $pos++;
                                             }}
                                $html .= '</body>
                                          </html>';
		
        $filename = 'CC '.substr($NROCERTI, 0, 9).' '.$CLIENTE.' '.$CMUESTRA;
        $this->pdfgenerator->generate($html, $filename, TRUE, 'A4', 'portrait');
        //echo $html;
    }  
    
    public function pdfCertificadoUnSoloNro($cinternoordenservicio) { // recupera 
        $this->load->library('pdfgenerator');

        $html = '<html>
                <head>
                    <title>Certificado</title>
                    <style type="text/css">
                        @page{
                             margin: 0.3in 0.3in 0.3in 0.3in;
                        }
                        body{
                            font-family: Arial, Helvetica, sans-serif;
                            margin-top: 3.5cm;
                            margin-left: 1cm;
                            margin-right: 1cm;
                            margin-bottom: 2cm;
                        }
                        #header,
                        #footer,
                        #nroinf {
                            position: fixed;
                            left: 0;
                            right: 0;
                            color: #000000;
                            font-size: 0.9em;
                        }                        
                        #header {
                            top: 0;
                            height: 4cm;
                        }    
                        #nroinf {
                            top: 2.5cm;
                            height: 2cm;
                        }                     
                        #footer {
                            bottom: 0;
                            border-top: 0.1pt solid #aaa;
                            height: 1cm;
                        }                        
                        #header table,
                        #footer table,
                        #nroinf table {
                            width: 100%;
                            border-collapse: collapse;
                            border: none;
                        }                        
                        #header td,
                        #footer td,
                        #nroinf td {
                            padding: 0;
                        }
                        .page-number{
                            text-align: right;
                        }
                        .page-number:before{
                            content: "Pagina " counter(page);
                        }
                        hr1{
                            page-break-before: always;
                            color: #ffffff;
                            margin: -2px;
                        }     
                        #caratula td{
                            font-size:0.8em;        
                        }
                        #tableborder{
                            border-collapse: collapse;
                            width: 100%;
                            font-size:0.8em;
                            border: 0.5pt solid black;
                        }
                        #tableborder td{
                            border: 0.5pt solid black;
                            padding-left: 5px;
                        }
                        #tableborder th{
                            text-align: center;
                            border: 0.5pt solid black;
                            background-color: #DBD7D7;
                        }
                        .list-unstyled{
                            padding-left: 0;
                            list-style: none;
                        }
                    </style>
                </head>
                <body> ';
                
                $parametroscabe = array(
                    '@cinternoordenservicio'         => $cinternoordenservicio,
                    '@cmuestra'       => 'T',
                );

                $resnro = $this->minformes->getcertiunsolonumero($cinternoordenservicio);
                $posnro = 0;
                if ($resnro){
                    $NROCERTI       = $resnro[0]->nrocerti;
                }
                $html .= '<div id="header">
                    
                     <table width="100%" align="center">
                         <tr>
                             <td width="25%" align="center">                            
                                 <img src="'.public_url_ftp().'Imagenes/formatos/2/logoFSC.jpg" width="180" height="95" />  
                             </td>   
                             <td width="100%" align="right" colspan="2" style="font-size: 0.8em;">
                                 <ul class="list-unstyled">
                                     <li>Jr. Monterrey N° 221 Of. 201-202</li>
                                     <li>Urb. Chacarilla del Estanque. Santiago de Surco, Lima - Perú</li>
                                     <li>Teléfonos: (51-1) 480 0561 - Anexo: Lab 111</li>
                                     <li>www.fscertificaciones.com</li>
                                 </ul>
                             </td>
                         </tr>
                         <tr>
                             <td colspan="3" style="height:40px; font-size:1.1em;" align="center">
                             <b>CERTIFICADO DE CALIDAD N°'.$NROCERTI.'  </b>
                             </td>
                         </tr>
                     </table>

                </div>   
                <div id="footer">
                     <table width="100%" style="font-size:0.8em; margin-top: 0.3cm; text-align: justify">
                     <tr>
                         <td width="100%" align="right" >
                             <div class="page-number"></div>
                         </td>
                     </tr>
                     </table>                    
                </div>'; 

                $res = $this->minformes->getcertixmuestras_caratula($parametroscabe);
                $pos = 0;
                if ($res){
                    foreach($res as $row){ 
                        $NROINFORME     = $row->NROINFORME;
                        
                        $CLIENTE        = $row->CLIENTE;
                        $DIRECCION      = $row->DIRECCION;
                        $NROORDEN       = $row->NROORDEN;
                        $PROCEDENCIA    = $row->PROCEDENCIA;
                        $FMUESTRA       = $row->FMUESTRA;
                        $FRECEPCION     = $row->FRECEPCION;
                        $FANALISIS      = $row->FANALISIS;
                        $LUGARMUESTRA   = $row->LUGARMUESTRA;
                        $CMUESTRA       = $row->CMUESTRA;
                        $DPRODUCTO      = $row->DPRODUCTO;
                        $DTEMPERATURA   = $row->DTEMPERATURA;
                        $DLCLAB         = $row->DLCLAB;
                        $OBSERVACION    = $row->OBSERVACION;
                        $SOBSERV        = $row->SOBSERV;
                        $ACNAC          = $row->ACNAC;
                        $DDATOSADICICONCLU = $row->DDATOSADICICONCLU;
                        $DREALPRODUCTO = $row->DREALPRODUCTO;

                        if($pos > 0){
                            $html .= '<hr style="page-break-before: always;" size="0" noshade="noshade" />';                          
                        }  
                        $parametros = array(
                            '@cinternoordenservicio'    => $cinternoordenservicio,
                            '@cmuestra'                 => $CMUESTRA,
                        );
                                
                        $html .= '<div id="main">
                                           
                                    <table id="caratula" width="100%" align="center" cellspacing="0" cellpadding="2" style="margin-top:20px">  
                                        <tr>
                                            <td> 
                                                <b>1.- </b>  
                                            </td>
                                            <td colspan="3" align="left">
                                                <b> DATOS GENERALES</b>   
                                            </td>
                                        </tr>                       
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>Nombre del Cliente</b>   
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$CLIENTE.'   
                                            </td>
                                        </tr>
                                        <tr style="vertical-align:top">
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>Dirección del Cliente</b>  
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$DIRECCION.'   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>N° Orden de Trabajo</b>   
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$NROORDEN.'   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>Procedencia de la Muestra</b>  
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$PROCEDENCIA.'   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>Fecha de Muestreo</b>    
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$FMUESTRA.'   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>Fecha de Recepción</b>    
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$FRECEPCION.'<   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>Fecha de Análisis</b>    
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$FANALISIS.'  
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>Lugar de Muestreo</b>   
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$LUGARMUESTRA.'  
                                            </td>
                                        </tr>
                                        <tr style="vertical-align:top">
                                            <td width="3%" align="left">   
                                            </td>
                                            <td width="27%" align="left">
                                                <b>Muestra / Descripción</b>    
                                            </td>
                                            <td width="7%" align="left">
                                                : '.$CMUESTRA.'   
                                            </td>
                                            <td width="63%" align="left">
                                                '.$DPRODUCTO.'   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>Temperatura de Recepción</b>    
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$DTEMPERATURA.'   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">   
                                            </td>
                                            <td align="left">
                                                <b>N° Informe de Ensayo</b>   
                                            </td>
                                            <td align="left" colspan="2">
                                                : '.$NROINFORME.'   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="4">
                                            &nbsp;   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td> 
                                                <b>2.- </b>  
                                            </td>
                                            <td align="left" colspan="3" style="height:30px;">
                                                <b>DOCUMENTOS NORMATIVOS</b>   
                                            </td>
                                        </tr>';
                                        $resdocnorma = $this->minformes->getdocnormativo($cinternoordenservicio,$CMUESTRA);
                                        $idocnorma = 1;
                                        if ($resdocnorma){
                                            foreach($resdocnorma as $rowdocnorma){
                                                $cnormalab = $rowdocnorma->cnormalab;
                                                $cgruponormalab = $rowdocnorma->cgruponormalab;
                                                $dnormalab = $rowdocnorma->dnormalab;
                                                $html .= '<tr style="vertical-align:top">
                                                   <td>2.'.$idocnorma.'</td>
                                                   <td colspan="3">'.$dnormalab.'</td>
                                                </tr>';
                                                $idocnorma ++ ;
                                                $resdocnorma_det = $this->minformes->getdocnormativo_det($cnormalab,$cgruponormalab);
                                                if ($resdocnorma_det){
                                                    foreach($resdocnorma_det as $rowdocnorma_det){
                                                        $dnumerogrupo = $rowdocnorma_det->dnumerogrupo;
                                                        $dnombregrupo = $rowdocnorma_det->dnombregrupo;
                                                        if($dnumerogrupo <> '0'){
                                                            $html .= '<tr>
                                                            <td></td>
                                                            <td colspan="3">'.$dnumerogrupo.' &nbsp; &nbsp; '.$dnombregrupo.'</td>
                                                            </tr>';
                                                        }
                                                    }
                                                } 
                                            }
                                        } 
                                        $html .= '
                                        <tr>
                                           <td > 
                                               <b>3.- </b>  
                                           </td>
                                            <td align="left" colspan="3" style="height:50px;">
                                                <b>METODOS DE ENSAYO</b>   
                                            </td>
                                        </tr>
                                    </table>';
                                     
                                    /*RESULTADOS METODOS DE ENSAYO*/    
                                     $resmetensa = $this->minformes->getmetodosensayos($parametros);
                                      if ($resmetensa){
                                        $html .= '<table id="tableborder" style="padding-left: 15px;">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <b>Ensayo</b>   
                                                </th>
                                                <th>
                                                    <b>Norma o Referencia</b>    
                                                </td>
                                            </tr>
                                            </thead>
                                            <tbody>';
                                            foreach($resmetensa as $rowmetensa){
                                                $METDENSAYO = $rowmetensa->DENSAYO;
                                                $METDNORMA = $rowmetensa->DNORMA;
                                                $html .= '<tr>
                                                   <td width="180px">'.$METDENSAYO.'</td>
                                                   <td style="vertical-align:top">'.$METDNORMA.'</td>
                                                </tr>';
                                            }
                                        $html .= '</tbody></table><br>';
                                     }  
                
                                     $html .= '<table id="caratula" width="713px" align="center" cellspacing="0" cellpadding="2" >
                                       <tr>
                                           <td width="3%"> 
                                               <b>4.- </b>  
                                           </td>
                                           <td width="97%" align="left" colspan="3" style="height:50px;">
                                               <b>RESULTADOS E INTERPRETACIONES</b>   
                                           </td>
                                       </tr>
                                     </table>'; 
                
                                    /*RESULTADOS MICROBIOLOGIA*/
                                     $resmicro = $this->minformes->getinfxmuestras_resmicro($parametros);
                                     if ($resmicro){
                                         $html .= '<table id="tableborder">
                                         <thead>
                                         <tr>
                                             <th> <b>Ensayo</b> </th>
                                             <th> <b>Unidades</b> </th>
                                             <th> <b>Limite permisible</b> </th>
                                             <th> <b>Resultado</b> </th>
                                             <th> <b>Conclusión</b> </th>
                                         </tr>
                                         </thead>
                                         <tbody>';
                                         $DENSAYOmicro = '';
                                         foreach($resmicro as $rowmicro){
                                             $DENSAYO = $rowmicro->NOMBENSAYO;
                                             $UNIDADMEDIDA = $rowmicro->UNIDADMEDIDA;
                                             $ESP_FINAL = $rowmicro->ESP_FINAL;
                                             $RESULT_FINAL = $rowmicro->RESULT_FINAL;
                                             $VALRESULTADO = $rowmicro->VALRESULTADO;
                                             $EXP10RES = $rowmicro->EXP10RES;
                                             $EXP10ESP = $rowmicro->EXP10ESP;

                                             $html .= '<tr>';
                                             if ($DENSAYO == $DENSAYOmicro){
                                                $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                                             }else{
                                                $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                                             }
                                             
                                             $html .= '<td width="100px" align="center">'.$UNIDADMEDIDA.'</td>
                                                 <td width="100px" align="center">'.str_replace("<", "&lt;", $ESP_FINAL).'<sup>'.$EXP10ESP.'</sup></td>
                                                 <td width="140px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'<sup>'.$EXP10RES.'</sup></td>
                                                 <td width="100px" align="center">'.$VALRESULTADO.'</td>
                                             </tr>';
                                             $DENSAYOmicro = $DENSAYO;
                                         }
                                         $html .= '</tbody></table><br>';
                                     }  
                                    /*RESULTADOS FISICOQUIMICO*/
                                     $resfq = $this->minformes->getinfxmuestras_resfq($parametros);
                                     if ($resfq){
                                         $html .= '<table id="tableborder">
                                         <thead>
                                         <tr>
                                             <th> <b>Ensayo</b> </th>
                                             <th> <b>Unidades</b> </th>
                                             <th> <b>Limite permisible</b> </th>
                                             <th> <b>Resultado</b> </th>
                                             <th> <b>Conclusión</b> </th>
                                         </tr>
                                         </thead>
                                         <tbody>';
                                         $DENSAYOfq = '';
                                         foreach($resfq as $rowfq){
                                             $DENSAYO = $rowfq->NOMBENSAYO;
                                             $UNIDADMEDIDA = $rowfq->UNIDADMEDIDA;
                                             $ESP_FINAL = $rowfq->ESP_FINAL;
                                             $RESULT_FINAL = $rowfq->RESULT_FINAL;
                                             $VALRESULTADO = $rowfq->VALRESULTADO;
                                             $EXP10ESP = $rowfq->EXP10ESP;
                                             $EXP10RES = $rowfq->EXP10RES;

                                             $html .= '<tr>';
                                             if ($DENSAYO == $DENSAYOfq){
                                                $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                                             }else{
                                                $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                                             }
                
                                             $html .= '<td width="100px" align="center">'.$UNIDADMEDIDA.'</td>
                                                 <td width="100px" align="center">'.str_replace("<", "&lt;", $ESP_FINAL).'<sup>'.$EXP10ESP.'</sup></td>
                                                 <td width="140px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'<sup>'.$EXP10RES.'</sup></td>
                                                 <td width="100px" align="center">'.$VALRESULTADO.'</td>
                                             </tr>';
                                             $DENSAYOfq = $DENSAYO;
                                         }
                                         $html .= '</tbody></table><br>';
                                     }
                                    ///RESULTADOS INSTRUMENTAL TERCERO///
                                       $resins = $this->minformes->getinfxmuestras_resinstru($parametros);
                                       if ($resins){
                                           $html .= '<table id="tableborder">
                                           <thead>
                                           <tr>
                                                <th> <b>Ensayo</b> </th>
                                                <th> <b>Unidades</b> </th>
                                                <th> <b>Limite permisible</b> </th>
                                                <th> <b>Resultado</b> </th>
                                                <th> <b>Conclusión</b> </th>
                                           </tr>
                                           </thead>
                                           <tbody>';
                                           $DENSAYOins = '';
                                           foreach($resins as $rowins){
                                               $DENSAYO = $rowins->DENSAYO;
                                               $UNIDADMEDIDA = $rowins->UNIDADMEDIDA;
                                               $ESP_FINAL = $rowins->ESP_FINAL;
                                               $RESULT_FINAL = $rowins->RESULT_FINAL;
                                               $VALRESULTADO = $rowins->VALRESULTADO;
                                               $EXP10ESP = $rowins->EXP10ESP;
                                               $EXP10RES = $rowins->EXP10RES;
                                               $TIPOPRODUCTO = $rowins->TIPOPRODUCTO;
                  
                                               $nota01 = '';                                               
                                               if ($TIPOPRODUCTO == '1'){
                                                $nota01 = '<small style="font-size: 10px;">(*)Fuente: Art. 4 , Decreto Supremo N°017-2017-SA Decreto Supremo N°012-2018 -SA. "Manual de Advertencias Publicitarias en el marco de los establecido en la Ley N° 30021, Ley de Promoción de la alimentación saludable para niños, niñas y adolescentes, y su Reglamento aprobado por Decreto Supremo N°017-2017-SA, Decreto Supremo N°033-2016 SA. "Reglamento que establece el proceso reducción gradual hasta le eliminación de las grasas trans en los alimentos y bebidas no alcohólicas procesados industrialmente.</small>';
                                               }                                               

                                               $html .= '<tr>';
                                               if ($DENSAYO == $DENSAYOins){
                                                  $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                                               }else{
                                                  $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                                               }
                  
                                               $html .= '<td width="100px" align="center">'.$UNIDADMEDIDA.'</td>
                                                   <td width="100px" align="center">'.str_replace("<", "&lt;", $ESP_FINAL).'<sup>'.$EXP10ESP.'</sup></td>
                                                   <td width="140px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'<sup>'.$EXP10RES.'</sup></td>
                                                   <td width="100px" align="center">'.$VALRESULTADO.'</td>
                                               </tr>';
                                               $DENSAYOins = $DENSAYO;
                                           }
                                           $html .= '</tbody></table><br>'.$nota01;
                                       }
                                    /*RESULTADOS SENSORIAL*/
                                     $ressen = $this->minformes->getinfxmuestras_ressenso($parametros);
                                     if ($ressen){
                                         $html .= '<table id="tableborder" style="page-break-inside: avoid;">
                                         <thead>
                                         <tr>
                                             <th> <b>Ensayo</b> </th>
                                             <th> <b>Atributo</b> </th>
                                             <th> <b>Limite permisible</b> </th>
                                             <th> <b>Resultado</b> </th>
                                             <th> <b>Conclusión</b> </th>
                                         </tr>
                                         </thead>
                                         <tbody>';
                                         $DENSAYOsen = '';
                                         foreach($ressen as $rowsen){
                                             $DENSAYO = $rowsen->DENSAYO;
                                             $DNOMBREESCALA = $rowsen->DNOMBREESCALA;
                                             $DLIMITE = $rowsen->DLIMITE;
                                             $RESULT_FINAL = $rowsen->RESULT_FINAL;
                                             $SCONCLUSION = $rowsen->SCONCLUSION;
                                             $html .= '<tr style="vertical-align:top">';
                                             if ($DENSAYO == $DENSAYOsen){
                                                $html .= '';
                                             }else{
                                                $html .= '<td rowspan="5" style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                                             }
                
                                             $html .= '<td width="70px">'.$DNOMBREESCALA.'</td>
                                                 <td width="160px" align="center">'.$DLIMITE.'</td>
                                                 <td width="170px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'</td>
                                                 <td width="100px" align="center">'.$SCONCLUSION.'</td>
                                             </tr>';
                                             $DENSAYOsen = $DENSAYO;
                                         }
                                         $html .= '</tbody></table><br>';
                                     }
                                     
                                    /*RESULTADOS CON ELEMENTOS*/
                                     $resele = $this->minformes->getinfxmuestras_reselementos($parametros);
                                     if ($resele){
                                        $html .= '<table id="tableborder">
                                        <thead>
                                        <tr>
                                            <th> <b>Ensayo</b> </th>
                                            <th> <b>Elemento</b> </th>
                                            <th> <b>Unidades</b> </th>
                                            <th> <b>Limite permisible</b> </th>
                                            <th> <b>Resultado</b> </th>
                                            <th> <b>Conclusión</b> </th>
                                        </tr>
                                        </thead>
                                        <tbody>';
                                        $DENSAYOele = '';
                                        foreach($resele as $rowele){
                                            $DENSAYO = $rowele->NOMBENSAYO;
                                            $ELEMENTO = $rowele->ELEMENTO;
                                            $UNIDADMEDIDA = $rowele->UNIDADMEDIDA;
                                            $LIMITE_FINAL = $rowele->LIMITE_FINAL;
                                            $RESULT_FINAL = $rowele->RESULT_FINAL;
                                            $VALRESULTADO = $rowele->VALRESULTADO;
                                            $html .= '<tr>';
                                            if ($DENSAYO == $DENSAYOele){
                                               $html .= '<td style="border-top: 0px solid; border-bottom: 0px solid;"></td>';
                                            }else{
                                               $html .= '<td style="border-bottom: 0px solid;">'.$DENSAYO.'</td>';
                                            }
                
                                            $html .= '<td>'.$ELEMENTO.'</td>
                                                <td width="85px" align="center">'.$UNIDADMEDIDA.'</td>
                                                <td width="100px" align="center">'.str_replace("<", "&lt;", $LIMITE_FINAL).'</td>
                                                <td width="100px" align="center">'.str_replace("<", "&lt;", $RESULT_FINAL).'</td>
                                                <td width="100px" align="center">'.$VALRESULTADO.'</td>
                                            </tr>';
                                            $DENSAYOele = $DENSAYO;
                                        }
                                        $html .= '</tbody></table><br>';
                                     }
                                    /*RESULTADOS ESTERILIDAD BAJA*/
                                     $parametrosEsterili = array(
                                        '@cinternoordenservicio' => $cinternoordenservicio,
                                        '@cmuestra' => $CMUESTRA,
                                        '@stipoacidez' => 'B'
                                     );
                                     $resesteb = $this->minformes->getinfxmuestras_resesteri($parametrosEsterili);
                                     if ($resesteb){
                                        $html .= '<table id="tableborder">
                                        <tr>
                                            <td colspan="7"> Baja acidez (pH>4.6)</td>
                                        </tr>
                                        <tr>
                                            <th width="30%" rowspan="2"> <b>Preincubación 35°C/14 días</b> </th>
                                            <th width="5%" rowspan="2"> <b>pH</b> </th>
                                            <th width="25%" colspan="2"> <b>Mesófilos (35°C)</b> </th>
                                            <th width="15%" colspan="2"> <b>Termófilos (55°C)</b> </th>
                                            <th width="25%" rowspan="2"> <b>Resultado</b> </th>
                                        </tr>
                                        <tr>
                                            <th> <b>Caldo púrpura de bromocreasol 120h</b> </th>
                                            <th> <b>Caldo carne cocida 120h</b> </th>
                                            <th> <b>Caldo púrpura de bromocreasol 48h</b> </th>
                                            <th> <b>Caldo carne cocida 72h</b> </th>
                                        </tr>';
                                        foreach($resesteb as $rowesteb){
                                            $preincuba = $rowesteb->preincuba;
                                            $dph = $rowesteb->dph;
                                            $dbames35cp = $rowesteb->dbames35cp;
                                            $dbames35cc = $rowesteb->dbames35cc;
                                            $dbater55cp = $rowesteb->dbater55cp;
                                            $dbater55cc = $rowesteb->dbater55cc;
                                            $result_final = $rowesteb->result_final;
                                            $html .= '<tr>
                                                <td>'.$preincuba.'</td>
                                                <td align="center">'.$dph.'</td>
                                                <td align="center">'.$dbames35cp.'</td>
                                                <td align="center">'.$dbames35cc.'</td>
                                                <td align="center">'.$dbater55cp.'</td>
                                                <td align="center">'.$dbater55cc.'</td>
                                                <td align="center">'.$result_final.'</td>
                                            </tr>';
                                        }
                                        $html .= '</table><br>';
                                     }
                                    /*RESULTADOS ESTERILIDAD ALTA*/
                                     $parametrosEsterili = array(
                                        '@cinternoordenservicio' => $cinternoordenservicio,
                                        '@cmuestra' => $CMUESTRA,
                                        '@stipoacidez' => 'A'
                                     );
                                     $resestea = $this->minformes->getinfxmuestras_resesteri($parametrosEsterili);
                                     if ($resestea){
                                        $html .= '<table id="tableborder">
                                        <tr>
                                            <td colspan="7"> Alta acidez ('.str_replace("<", "&lt;", "pH<4.6").')</td>
                                        </tr>
                                        <tr>
                                            <th width="30%" rowspan="2"> <b>Preincubación 35°C/14 días</b> </th>
                                            <th width="5%" rowspan="2"> <b>pH</b> </th>
                                            <th width="25%" colspan="2"> <b>Mesófilos (35°C)</b> </th>
                                            <th width="15%" colspan="2"> <b>Termófilos (55°C)</b> </th>
                                            <th width="25%" rowspan="2"> <b>Resultado</b> </th>
                                        </tr>
                                        <tr>
                                            <th> <b>Caldo ácido 96h</b> </th>
                                            <th> <b>Caldo extracto de malta 96h</b> </th>
                                            <th colspan="2"> <b>Caldo ácido 48h</b> </th>
                                        </tr>';
                                        foreach($resestea as $rowestea){
                                            $preincuba = $rowestea->preincuba;
                                            $dph = $rowestea->dph;
                                            $dacmes30ca = $rowestea->dacmes30ca;
                                            $dacmes30cm = $rowestea->dacmes30cm;
                                            $dacter55ca = $rowestea->dacter55ca;
                                            $result_final = $rowestea->result_final;
                                            $html .= '<tr>
                                                <td>'.$preincuba.'</td>
                                                <td align="center">'.$dph.'</td>
                                                <td align="center">'.$dacmes30ca.'</td>
                                                <td align="center">'.$dacmes30cm.'</td>
                                                <td align="center" colspan="2">'.$dacter55ca.'</td>
                                                <td align="center">'.$result_final.'</td>
                                            </tr>';
                                        }
                                        $html .= '</table><br>';
                                     }

                                     ///RESULTADOS ETIQUETADO NUTRICIONAL///
                                       $parametrosEtiquetado = array(
                                           '@cinternoordenservicio' => $cinternoordenservicio,
                                           '@cmuestra' => $CMUESTRA
                                       );
                                       $resetiqueta = $this->minformes->getinfxmuestras_resetiquetado($parametrosEtiquetado);
                                       if ($resetiqueta){
                                         $html .= '<table id="tableborder">
                                           <tr>
                                               <td colspan="7"> <b>INFORMACION NUTRICIONAL</b> </td>
                                           </tr>
                                           <tr>
                                               <td width="40%" colspan="4"> Tamaño de Porción :</td>
                                               <td colspan="3">14g</td>
                                           </tr>
                                           <tr>
                                               <th width="25%" colspan="3"> <b>Ensayo</b> </th>
                                               <th width="15%"> <b>Unidad</b> </th>
                                               <th width="20%"> <b>100g</b> </th>
                                               <th width="20%"> <b>Porción</b> </th>
                                               <th width="20%"> <b>% VDR(*)</b> </th>
                                           </tr>';
                                           foreach($resetiqueta as $rowetiqueta){
                                               $DENSAYO = $rowetiqueta->DENSAYO;
                                               $UNIDADMEDIDA = $rowetiqueta->UNIDADMEDIDA;
                                               $DRESULTADO = $rowetiqueta->DRESULTADO;
                                               $NPORCION = $rowetiqueta->NPORCION;
                                               $VALORVR = $rowetiqueta->VALORVR;
                                               $html .= '<tr>
                                                   <td colspan="3">'.$DENSAYO.'</td>
                                                   <td align="center">'.$UNIDADMEDIDA.'</td>
                                                   <td align="center">'.$DRESULTADO.'</td>
                                                   <td align="center">'.number_format($NPORCION, 3, '.', '').'</td>
                                                   <td align="center">'.number_format($VALORVR, 3, '.', '').'</td>
                                               </tr>';
                                           }
                                           $html .= '<tr><td colspan="7">(*) % Valor Diario (VD) con base a una dieta de 2000 kcal u 8370 kJ.  según Codex Alimentarius FAO/OMS. Sus valores diarios pueden ser mayores o menores dependiendo de sus necesidades energéticas.</tr>
                                                      </table><br>';
                                       }
                                    
                                 /*CONCLUSIONES */     
                                  $html .= '<table id="caratula" width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">
                                     <tr>
                                         <td width="3%"> 
                                             <b>5.- </b>  
                                         </td>
                                         <td width="97%" align="left" colspan="3" style="height:30px;">
                                             <b>CONCLUSIONES</b>   
                                         </td>                         
                                     </tr>';
                                      
                                     $parametrosconclu = array(
                                        '@cinternoordenservicio'         => $cinternoordenservicio,
                                        '@cmuestra'       => $CMUESTRA,
                                        '@DDATOSADICICONCLU'       => $DDATOSADICICONCLU,
                                        '@DREALPRODUCTO'       => $DREALPRODUCTO,
                                     );
                                     $resconclu = $this->minformes->getcertixmuestras_conclusion($parametrosconclu);
                                     if ($resconclu){
                                         foreach($resconclu as $rowconclu){
                                             $DCONCLUSION = $rowconclu->DCONCLUSION;
                                             $html .= '<tr>
                                                <td></td>
                                                <td colspan="3">'.$DCONCLUSION.'</td>
                                             </tr>';
                                         }
                                    }
                                    
                                  $html .= '</table><br>'; 
                                 
                                /* FECHA Y FIRMAS*/ 
                                 $resFECHAFIRMA = $this->minformes->getinfxmuestras_fechafirma($parametros);
                                 if ($resFECHAFIRMA){
                                    foreach($resFECHAFIRMA as $rowFECHAFIRMA){
                                        $FECHA        = $rowFECHAFIRMA->FECHA;

                                        $SFQ        = $rowFECHAFIRMA->SFQ;
                                        $SMICRO        = $rowFECHAFIRMA->SMICRO;
                                        $SINSTRU        = $rowFECHAFIRMA->SINSTRU;

                                        $FIRMAFQS        = $rowFECHAFIRMA->FIRMAFQS;
                                        $NOMBREFQS        = $rowFECHAFIRMA->NOMBREFQS;
                                        $CARGOFQS        = $rowFECHAFIRMA->CARGOFQS;
                                        $CODIGOFQS        = $rowFECHAFIRMA->CODIGOFQS;

                                        $FIRMAMICRO        = $rowFECHAFIRMA->FIRMAMICRO;
                                        $NOMBREMICRO        = $rowFECHAFIRMA->NOMBREMICRO;
                                        $CARGOMICRO        = $rowFECHAFIRMA->CARGOMICRO;
                                        $CODIGOMICRO        = $rowFECHAFIRMA->CODIGOMICRO;

                                        $FIRMAINSTRU        = $rowFECHAFIRMA->FIRMAINSTRU;
                                        $NOMBREINSTRU        = $rowFECHAFIRMA->NOMBREINSTRU;
                                        $CARGOINSTRU        = $rowFECHAFIRMA->CARGOINSTRU;
                                        $CODIGOINSTRU        = $rowFECHAFIRMA->CODIGOINSTRU;
                                    }
                                 }
                                
                                 if($SFQ == '1' and $SMICRO == '1' and $SINSTRU == '1'){
                                    $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">                     
                                            <tr>
                                                <td width="100%" align="left" style="font-size: 0.8em;" colspan="2">
                                                Lima, '.$FECHA.'   
                                                </td>
                                            </tr> 
                                            <tr>
                                                <td width="100%" align="center">
                                                <img src="'.public_url_ftp().'Imagenes/firmas/FS000052.jpg" width="190" height="80" />    
                                                </td>
                                                <td width="100%" align="center">
                                                <img src="'.public_url_ftp().'Imagenes/firmas/FS000046.jpg" width="190" height="80" />   
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" align="center" style="font-size: 0.8em;">
                                                <hr width=150>
                                                '.$NOMBREFQS.'    
                                                </td>
                                                <td width="100%" align="center" style="font-size: 0.8em;">
                                                <hr width=150>
                                                '.$NOMBREMICRO.'  
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" align="center" style="font-size: 0.8em;">
                                                '.$CARGOFQS.'
                                                </td>
                                                <td width="100%" align="center" style="font-size: 0.8em;">
                                                '.$CARGOMICRO.'
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" align="center" style="font-size: 0.8em;">
                                                '.$CODIGOFQS.'
                                                </td>
                                                <td width="100%" align="center" style="font-size: 0.8em;">
                                                '.$CODIGOMICRO.'
                                                </td>
                                            </tr>

                                            <tr>
                                                <td width="100%" align="center" colspan="2">
                                                <br>   
                                                <br>  
                                                </td>
                                            </tr>

                                            <tr>
                                                <td width="100%" align="center" colspan="2">
                                                <img src="'.public_url_ftp().'Imagenes/firmas/FS000120.jpg" width="190" height="80" />     
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                                <hr width=150>
                                                '.$NOMBREINSTRU.'     
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                                '.$CARGOINSTRU.'    
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                                '.$CODIGOINSTRU.'    
                                                </td>
                                            </tr>
                                        </tr>
                                    </table>';
                                 }else{
                                    $html .= '<table width="100%" align="center" cellspacing="0" cellpadding="2" style="page-break-inside: avoid;">              
                                            <tr>
                                                <td width="100%" align="left" style="font-size: 0.8em;">
                                                    Lima, '.$FECHA.'   
                                                </td>
                                            </tr> 
                                            <tr>';
                                            if($SFQ == '1'){
                                                $html .= '<td width="100%" align="center">
                                                    <img src="'.public_url_ftp().'Imagenes/firmas/FS000052.jpg" width="190" height="80" />    
                                                    </td>';
                                            }
                                            if($SMICRO == '1'){
                                                $html .= '<td width="100%" align="center">
                                                    <img src="'.public_url_ftp().'Imagenes/firmas/FS000046.jpg" width="190" height="80" />   
                                                    </td>';
                                            }
                                            if($SINSTRU == '1'){
                                                $html .= '<td width="100%" align="center" colspan="2">
                                                    <img src="'.public_url_ftp().'Imagenes/firmas/FS000120.jpg" width="190" height="80" />     
                                                    </td>';
                                            }
                                    $html .= '</tr>
                                            <tr>';
                                            if($SFQ == '1'){
                                                $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                                <hr width=150>
                                                '.$NOMBREFQS.'    
                                                </td>';
                                            }
                                            if($SMICRO == '1'){
                                                $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                                <hr width=150>
                                                '.$NOMBREMICRO.'  
                                                </td>';
                                            }
                                            if($SINSTRU == '1'){
                                                $html .= '<td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                                <hr width=150>
                                                '.$NOMBREINSTRU.'     
                                                </td>';
                                            }
                                    $html .= '</tr>
                                            <tr>';
                                            if($SFQ == '1'){
                                                $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                                '.$CARGOFQS.'
                                                </td>';
                                            }
                                            if($SMICRO == '1'){
                                                $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                                '.$CARGOMICRO.'
                                                </td>';
                                            }
                                            if($SINSTRU == '1'){
                                                $html .= '<td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                                '.$CARGOINSTRU.'    
                                                </td>';
                                            }
                                    $html .= '</tr>
                                            <tr>';
                                            if($SFQ == '1'){
                                                $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                                '.$CODIGOFQS.'
                                                </td>';
                                            }
                                            if($SMICRO == '1'){
                                                $html .= '<td width="100%" align="center" style="font-size: 0.8em;">
                                                '.$CODIGOMICRO.'
                                                </td>';
                                            }
                                            if($SINSTRU == '1'){
                                                $html .= '<td width="100%" align="center" colspan="2" style="font-size: 0.8em;">
                                                '.$CODIGOINSTRU.'    
                                                </td>';
                                            }
                                    $html .= '</tr>
                                    </table>';
                                 }     
        
                                $html .= '</div>';
                                                  $pos++;
                                             }}
                                $html .= '</body>
                                          </html>';
		
        $filename = 'CC '.substr($NROCERTI, 0, 9).' '.$CLIENTE.' '.$CMUESTRA;
        $this->pdfgenerator->generate($html, $filename, TRUE, 'A4', 'portrait');
        //echo $html;
    } 
	 
}
?>