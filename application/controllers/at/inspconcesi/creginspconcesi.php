<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class Cregctrolprov
 * @property mreginspconcesi mregctrolprov
 */
class Creginspconcesi extends FS_Controller
{
	function __construct()
	{
		parent:: __construct();
		$this->load->model('at/inspconcesi/mreginspconcesi');
		$this->load->model('mglobales');
		$this->load->library('encryption');
		$this->load->helper(array('form', 'url', 'download', 'html', 'file'));
		$this->load->library('form_validation');
	}

	/** CONTROL DE PROVEEDORES **/
	public function getcboclieserv()
	{    // Visualizar Clientes del servicio en CBO

		$resultado = $this->mreginspconcesi->getcboclieserv();
		echo json_encode($resultado);
	}

	/**
	 * Visualizar Inspectores en CBO
	 */
	public function getcboinspector()
	{
		$sregistro = $this->input->post('sregistro');
		$sregistro = (empty($sregistro)) ? 'A' : $sregistro;
		$parametros = array(
			'@sregistro' => $sregistro
		);
		$resultado = $this->mreginspconcesi->getcboinspector($parametros);
		echo json_encode($resultado);
	}

	public function getcboestado()
	{    // Visualizar Estado en CBO

		$resultado = $this->mreginspconcesi->getcboestado();
		echo json_encode($resultado);
	}
	

	/**
	 * Lista de registro de inspeccion
	 */
	public function getbuscarinspconcesi()
	{    // Busca Control de Proveedores

		$varnull = '';

		$ccia = '1';
		$carea = '01';
		$cservicio = '12';
		$fini = $this->input->post('fdesde');
		$ffin = $this->input->post('fhasta');
		$ccliente = $this->input->post('ccliente');
		$dclientetienesta = $this->input->post('dclientetienesta');
		$inspector = $this->input->post('inspector');
		$cboestado = $this->input->post('cboestado');

		$parametros = array(
			'@ccia' => $ccia,
			'@carea' => $carea,
			'@cservicio' => $cservicio,
			'@fini' => ($fini == '%') ? null : substr($fini, 6, 4) . '-' . substr($fini, 3, 2) . '-' . substr($fini, 0, 2),
			'@ffin' => ($ffin == '%') ? null : substr($ffin, 6, 4) . '-' . substr($ffin, 3, 2) . '-' . substr($ffin, 0, 2),
			'@ccliente' => $ccliente,
			'@dclientetienesta' => (empty($dclientetienesta)) ? '%' : $dclientetienesta,
			'@inspector' => ($this->input->post('inspector') == '') ? '' : $inspector,
			'@cusuario' => '%',
			'@estado' => (empty($cboestado)) ? '%' : implode(',', $cboestado),
		);
		$resultado = $this->mreginspconcesi->getbuscarinspconcesi($parametros);
		echo json_encode($resultado);
	}

	public function getrecuperainsp()
	{    // Visualizar Maquilador por proveedor en CBO

		$parametros = array(
			'@idinspeccion' => $this->input->post('idinspeccion')
		);
		$resultado = $this->mregctrolprov->getrecuperainsp($parametros);
		echo json_encode($resultado);
	}

	public function getcboregestable()
	{    // Visualizar Maquilador por proveedor en CBO

		$parametros = array(
			'@ccliente' => $this->input->post('ccliente'),
			'@cproveedor' => $this->input->post('cproveedor'),
			'@cmaquilador' => $this->input->post('cmaquilador'),
			'@tipo' => $this->input->post('tipo')
		);
		$resultado = $this->mregctrolprov->getcboregestable($parametros);
		echo json_encode($resultado);
	}

	public function getdirestable()
	{    // Visualizar Proveedor por cliente en CBO

		$cestable = $this->input->post('cestablecimiento');
		$resultado = $this->mregctrolprov->getdirestable($cestable);
		echo json_encode($resultado);
	}

	public function getcboareaclie()
	{    // Visualizar Proveedor por cliente en CBO

		$ccliente = $this->input->post('ccliente');
		$resultado = $this->mregctrolprov->getcboareaclie($ccliente);
		echo json_encode($resultado);
	}

	public function getcbolineaproc()
	{    // Visualizar Proveedor por cliente en CBO

		$cestablecimiento = $this->input->post('cestablecimiento');
		$resultado = $this->mregctrolprov->getcbolineaproc($cestablecimiento);
		echo json_encode($resultado);
	}

	public function getcbocontacprinc()
	{    // Visualizar Proveedor por cliente en CBO

		$cproveedor = $this->input->post('cproveedor');
		$resultado = $this->mregctrolprov->getcbocontacprinc($cproveedor);
		echo json_encode($resultado);
	}

	public function getcbotipoestable()
	{    // Visualizar Proveedor por cliente en CBO

		$resultado = $this->mregctrolprov->getcbotipoestable();
		echo json_encode($resultado);
	}

	public function getmontotipoestable()
	{    // Visualizar Proveedor por cliente en CBO

		$ctipoestable = $this->input->post('ctipoestable');
		$resultado = $this->mregctrolprov->getmontotipoestable($ctipoestable);
		echo json_encode($resultado);
	}

	public function setregctrlprov()
	{ // Registrar inspeccion
		$varnull = '';

		$cauditoriainspeccion = $this->input->post('mhdnregIdinsp');
		$ccliente = $this->input->post('cboregClie');
		$cproveedor = $this->input->post('cboregprovclie');
		$cmaquilador = $this->input->post('cboregmaquiprov');
		$cestablecimiento = $this->input->post('cboregestable');
		$clineaprocesocliente = $this->input->post('cboreglineaproc');
		$careacliente = $this->input->post('cboregareaclie');
		$ccontacto = $this->input->post('cbocontacprinc');
		$ctipoestablecimiento = $this->input->post('cbotipoestable');
		$icostobase = $this->input->post('txtcostoestable');
		$dperiodo = $this->input->post('mtxtregPeriodo');
		$cusuario = $this->input->post('hdnregcusuario');
		$accion = $this->input->post('mhdnAccionctrlprov');

		$parametros = array(
			'@cauditoriainspeccion' => $cauditoriainspeccion,
			'@ccliente' => $ccliente,
			'@cproveedor' => $cproveedor,
			'@cmaquilador' => ($this->input->post('cboregmaquiprov') == '') ? '' : $cmaquilador,
			'@cestablecimiento' => $cestablecimiento,
			'@clineaprocesocliente' => $clineaprocesocliente,
			'@careacliente' => $careacliente,
			'@ccontacto' => $ccontacto,
			'@ctipoestablecimiento' => $ctipoestablecimiento,
			'@icostobase' => $icostobase,
			'@dperiodo' => $dperiodo,
			'@cusuario' => $cusuario,
			'@accion' => $accion,
		);
		$resultado = $this->mregctrolprov->setregctrlprov($parametros);
		echo json_encode($resultado);
	}

	public function getcbosistemaip()
	{    // Visualizar Calificacion en CBO

		$resultado = $this->mregctrolprov->getcbosistemaip();
		echo json_encode($resultado);
	}

	public function getcborubroip()
	{    // Visualizar Inspectores en CBO

		$parametros = array(
			'@idnorma' => $this->input->post('cnorma')
		);
		$resultado = $this->mregctrolprov->getcborubroip($parametros);
		echo json_encode($resultado);
	}

	public function getcbochecklist()
	{    // Visualizar Inspectores en CBO

		$parametros = array(
			'@idnorma' => $this->input->post('cnorma'),
			'@idsubnorma' => $this->input->post('csubnorma'),
			'@ccliente' => $this->input->post('ccliente')
		);
		$resultado = $this->mregctrolprov->getcbochecklist($parametros);
		echo json_encode($resultado);
	}

	public function getcbomodinforme()
	{    // Visualizar Calificacion en CBO

		$resultado = $this->mregctrolprov->getcbomodinforme();
		echo json_encode($resultado);
	}

	public function getcboinspvalconf()
	{    // Visualizar Calificacion en CBO

		$resultado = $this->mregctrolprov->getcboinspvalconf();
		echo json_encode($resultado);
	}

	public function getcboinspformula()
	{    // Visualizar Proveedor por cliente en CBO

		$cchecklist = $this->input->post('cchecklist');
		$resultado = $this->mregctrolprov->getcboinspformula($cchecklist);
		echo json_encode($resultado);
	}

	public function getcboinspcritresul()
	{    // Visualizar Calificacion en CBO

		$resultado = $this->mregctrolprov->getcboinspcritresul();
		echo json_encode($resultado);
	}

	public function setreginspeccion()
	{    // Visualizar Inspectores en CBO
		$varnull = '';

		$cauditoriainspeccion = $this->input->post('mtxtidinsp');
		$fservicio = $this->input->post('mfechainsp');
		$fechaservicio = $this->input->post('txtFInsp');
		$cusuarioconsultor = $this->input->post('cboinspinspector');
		$chkaacc = $this->input->post('chkaacc');
		if ($chkaacc == 'on') {
			$spermitircorrectivas = 'S';
		} else {
			$spermitircorrectivas = 'N';
		}
		$zctiposervicio = $this->input->post('cbotiposerv');
		$chkplaninsp = $this->input->post('chkplaninsp');
		if ($chkplaninsp == 'on') {
			$sincluyeplan = 'S';
		} else {
			$sincluyeplan = 'N';
		}
		$cnorma = $this->input->post('cboinspsistema');
		$csubnorma = $this->input->post('cboinsprubro');
		$cchecklist = $this->input->post('cboinspcchecklist');
		$cvalornoconformidad = $this->input->post('cboinspvalconf');
		$cformulaevaluacion = $this->input->post('cboinspformula');
		$dcometario = $this->input->post('mtxtinspcoment');
		$ccriterioresultado = $this->input->post('cboinspcritresul');
		$cmodeloinforme = $this->input->post('cboinspmodeloinfo');
		$periodo = $this->input->post('cboinspperiodo');
		$zctipoestado = $this->input->post('mhdnzctipoestado');
		$accion = $this->input->post('mhdnAccioninsp');

		$parametros = array(
			'@cauditoriainspeccion' => $cauditoriainspeccion,
			'@fservicio' => ($this->input->post('mfechainsp') == '') ? '1900-01-01' : substr($fservicio, 6, 4) . '-' . substr($fservicio, 3, 2) . '-' . substr($fservicio, 0, 2),
			'@fechaservicio' => ($this->input->post('txtFInsp') == 'Sin Fecha') ? '1900-01-01' : substr($fechaservicio, 6, 4) . '-' . substr($fechaservicio, 3, 2) . '-' . substr($fechaservicio, 0, 2),
			'@cusuarioconsultor' => ($this->input->post('cboinspinspector') == $varnull) ? null : $cusuarioconsultor,
			'@spermitircorrectivas' => $spermitircorrectivas,
			'@zctiposervicio' => $zctiposervicio,
			'@sincluyeplan' => $sincluyeplan,
			'@cnorma' => ($this->input->post('cboinspsistema') == $varnull) ? null : $cnorma,
			'@csubnorma' => ($this->input->post('cboinsprubro') == $varnull) ? null : $csubnorma,
			'@cchecklist' => ($this->input->post('cboinspcchecklist') == $varnull) ? null : $cchecklist,
			'@cvalornoconformidad' => ($this->input->post('cboinspvalconf') == $varnull) ? null : $cvalornoconformidad,
			'@cformulaevaluacion' => ($this->input->post('cboinspformula') == $varnull) ? null : $cformulaevaluacion,
			'@dcometario' => $dcometario,
			'@ccriterioresultado' => ($this->input->post('cboinspcritresul') == $varnull) ? null : $ccriterioresultado,
			'@cmodeloinforme' => ($this->input->post('cboinspmodeloinfo') == $varnull) ? null : $cmodeloinforme,
			'@periodo' => $periodo,
			'@zctipoestado' => $zctipoestado,
			'@accion' => $accion
		);
		$resultado = $this->mregctrolprov->setreginspeccion($parametros);
		echo json_encode($resultado);
	}

	public function getcbocierreTipo()
	{    // Visualizar Proveedor por cliente en CBO

		$resultado = $this->mregctrolprov->getcbocierreTipo();
		echo json_encode($resultado);
	}

	public function getcbocontacplanins()
	{    // Visualizar Proveedor por cliente en CBO
		$varnull = '';
		$ccliente = $this->input->post('ccliente');
		$cproveedorcliente = $this->input->post('cproveedor');
		$cmaquiladorcliente = ($this->input->post('cmaquilador') == $varnull) ? null : $this->input->post('cmaquilador');

		$resultado = $this->mregctrolprov->getcbocontacplanins($cproveedorcliente, $cmaquiladorcliente);
		echo json_encode($resultado);
	}

	public function setcierreespecial()
	{    // Visualizar Inspectores en CBO
		$varnull = '';

		$cauditoriainspeccion = $this->input->post('txtcierreidinsp');
		$fservicio = $this->input->post('mhdncierrefservicio');
		$fechacierre = $this->input->post('txtcierrefservicio');
		$zctipoestadoservicio = $this->input->post('cbocierreTipo');
		$valorestado = $this->input->post('mhdnfprog');
		$fechaprogramada = $this->input->post('txtcierreFProg');
		$itrunco = $this->input->post('txtcierremonto');
		$ngastogeneral = $this->input->post('txtcierreviatico');
		$dcomentario = $this->input->post('mtxtcierrecomentario');
		$cusuario = $this->input->post('hdncusuario');
		$accion = $this->input->post('mhdnAccioncierre');

		$parametros = array(
			'@cauditoriainspeccion' => $cauditoriainspeccion,
			'@fservicio' => ($this->input->post('mhdncierrefservicio') == '') ? '1900-01-01' : substr($fservicio, 6, 4) . '-' . substr($fservicio, 3, 2) . '-' . substr($fservicio, 0, 2),
			'@fechacierre' => ($this->input->post('txtcierrefservicio') == '') ? '1900-01-01' : substr($fechacierre, 6, 4) . '-' . substr($fechacierre, 3, 2) . '-' . substr($fechacierre, 0, 2),
			'@zctipoestadoservicio' => $zctipoestadoservicio,
			'@valorestado' => $valorestado,
			'@fechaprogramada' => ($this->input->post('txtcierreFProg') == '') ? '1900-01-01' : substr($fechaprogramada, 6, 4) . '-' . substr($fechaprogramada, 3, 2) . '-' . substr($fechaprogramada, 0, 2),
			'@itrunco' => $itrunco,
			'@ngastogeneral' => $ngastogeneral,
			'@dcomentario' => $dcomentario,
			'@cusuario' => $cusuario,
			'@accion' => $accion
		);
		$resultado = $this->mregctrolprov->setcierreespecial($parametros);
		echo json_encode($resultado);
	}

	public function setreaperturar()
	{    // Visualizar Inspectores en CBO
		$varnull = '';

		$cauditoriainspeccion = $this->input->post('cauditoriainspeccion');
		$fservicio = $this->input->post('fservicio');
		$cusuario = $this->input->post('cusuario');

		$parametros = array(
			'@cauditoriainspeccion' => $cauditoriainspeccion,
			'@fservicio' => ($this->input->post('fservicio') == '') ? '1900-01-01' : substr($fservicio, 6, 4) . '-' . substr($fservicio, 3, 2) . '-' . substr($fservicio, 0, 2),
			'@cusuario' => $cusuario,
		);
		$resultado = $this->mregctrolprov->setreaperturar($parametros);
		echo json_encode($resultado);
	}

	public function getcbocertificadora()
	{    // Visualizar Proveedor por cliente en CBO

		$resultado = $this->mregctrolprov->getcbocertificadora();
		echo json_encode($resultado);
	}

	public function setplaninsp()
	{    // Visualizar Inspectores en CBO
		$varnull = '';

		$cauditoriainspeccion = $this->input->post('mhdnidinspplaninsp');
		$fservicio = $this->input->post('mhdnfservicioplaninsp');
		$dobjetivoplan = $this->input->post('mtxtobjeplaninsp');
		$dalcanceplan = $this->input->post('mtxtalcanplaninsp');
		$fechaplan = $this->input->post('mtxtFplanins');
		$horaplan = $this->input->post('mtxtHplanins');
		$ccontacto = $this->input->post('cbocontacplanins');
		$drequerimiento = $this->input->post('mtxtrequeplaninsp');
		$tiposerv = $this->input->post('mhdntiposervplaninsp');
		$cusuario = $this->input->post('hdncusuarioplaninsp');
		$accion = $this->input->post('mhdnAccionplaninsp');

		$parametros = array(
			'@cauditoriainspeccion' => $cauditoriainspeccion,
			'@fservicio' => ($this->input->post('mhdnfservicioplaninsp') == '') ? '1900-01-01' : substr($fservicio, 6, 4) . '-' . substr($fservicio, 3, 2) . '-' . substr($fservicio, 0, 2),
			'@dobjetivoplan' => $dobjetivoplan,
			'@dalcanceplan' => $dalcanceplan,
			'@fechaplan' => ($this->input->post('mtxtFplanins') == '') ? '1900-01-01' : substr($fechaplan, 6, 4) . '-' . substr($fechaplan, 3, 2) . '-' . substr($fechaplan, 0, 2),
			'@horaplan' => $horaplan,
			'@ccontacto' => $ccontacto,
			'@drequerimiento' => $drequerimiento,
			'@tiposerv' => $tiposerv,
			'@cusuario' => $cusuario,
			'@accion' => $accion
		);
		$resultado = $this->mregctrolprov->setplaninsp($parametros);
		echo json_encode($resultado);
	}

	public function genpdfplaninsp($idinspeccion, $fservicio)
	{ // recupera los cPTIZACION
		$this->load->library('pdfgenerator');

		$date = getdate();
		$fechaactual = date("d") . "/" . date("m") . "/" . date("Y");

		$html = '<html>
                <head>
                    <title>Plan de Inspeccion</title>
                    <style>
                        @page {
                             margin: 0.3in 0.3in 0.3in 0.3in;
                        } 
                        .teacherPage {
                            page: teacher;
                            page-break-after: always;
                        }
                        body{
                            font-family: Arial, Helvetica, sans-serif;
                            font-size: 9pt;
                            margin-top: 3cm;
                            margin-left: 0cm;
                            margin-right: 0cm;
                            margin-bottom: 0cm;
                        }  
                        header {
                            position: fixed;
                            top: 0cm;
                            left: 0cm;
                            right: 0cm;
                            height: 3cm;
                        }
                        .cuerpo {
                            text-align: justify;
                        }
                        img.izquierda {
                            float: left;
                        }
                        img.derecha {
                            float: right;
                        }
                        div.page_break {
                            page-break-before: always;
                        }
                        .page-number {
                          text-align: right;
                        }
                        
                        .page-number:before {
                          content: counter(page);
                        }
                        th { 
                            text-align: center; 
                            border: 1px solid black;
                        }
                    </style>
                </head>
                <body>
                
                <header>
                    <table  width="700px" align="center" cellspacing="0" cellpadding="2" style="border: 0px solid black;">
                        <tr>
                            <td width="70%" rowspan="4">
								<img src="' . public_url_ftp() . 'Imagenes/formatos/1/logoFS.png" width="180" height="70" />   
                            </td>
                            <td width="30%" align="right">
								Av. Del Pinar 110 of. 405-407
                            </td>
                        </tr>
                        <tr>
                            <td align="right">Santiago de Surco, Lima - Perú</td>
                        </tr>
                        <tr>
                            <td align="right">(51-1)372-1734 / 372-8182</td>
                        </tr>
                        <tr>
                            <td align="right">www.grupofs.com </td>
                        </tr>
                        <tr>
							<td align="center" colspan="2">
								<b>PLAN DE INSPECCION</b>
							</td>
                        </tr>
                    </table>
				</header>';
		$res = $this->mregctrolprov->getpdfdatosplaninsp($idinspeccion, $fservicio);
		if ($res) {
			foreach ($res as $row) {
				$empresa = $row->empresa;
				$direccion = $row->direccion;
				$dobjetivoplan = $row->dobjetivoplan;
				$dalcanceplan = $row->dalcanceplan;
				$finspeccion = $row->finspeccion;
				$hinicio = $row->hinicio;
				$contacto = $row->contacto;
				$dtelefono = $row->dtelefono;
				$inspector = $row->inspector;
				$drequerimiento = $row->drequerimiento;
				$ccompania = $row->ccompania;
				$carea = $row->carea;
				$cservicio = $row->cservicio;
				$dcriterios = $row->dcriterios;
			}
		}
		$html .= '
				<main>
					<table width="700px" align="center">
						<tr>
							<td colspan="2" style="height:30px;"><b>EMPRESA : </b>' . $empresa . '</td>
						</tr>
						<tr>
							<td colspan="2" style="height:30px;"><b>DIRECCION : </b>' . $direccion . '</td>
						</tr>
						<tr>
							<td colspan="2"><b>OBJETIVO : </b></td>
						</tr>
						<tr>
							<td colspan="2" style="height:30px;">' . $dobjetivoplan . '</td>
						</tr>
						<tr>
							<td colspan="2" style="height:30px;"><b>ALCANCE : </b>' . $dalcanceplan . '</td>
						</tr>
						<tr>
							<td colspan="2" style="height:30px;"><b>FECHA DE INSPECCION : </b>' . $finspeccion . '</td>
						</tr>
						<tr>
							<td colspan="2" style="height:30px;"><b>HORA DE INICIO : </b>' . $hinicio . '</td>
						</tr>
						<tr>
							<td colspan="2" style="height:30px;"><b>PERSONA DE CONTACTO : </b>' . $contacto . '</td>
						</tr>
						<tr>
							<td colspan="2" style="height:30px;"><b>TELEFONO : </b>' . $dtelefono . '</td>
						</tr>
						<tr>
							<td colspan="2" style="height:30px;"><b>INSPECTOR : </b>' . $inspector . '</td>
						</tr>
						<tr>
							<td colspan="2" style="height:30px;"><b>REQUISITO : </b>' . $drequerimiento . '</td>
						</tr>
						<tr>
							<td colspan="2"><b>METODOLOGIA : </b></td>
						</tr>';
		$resmet = $this->mregctrolprov->getpdfmetodoplaninsp($ccompania, $carea, $cservicio);
		if ($resmet) {
			foreach ($resmet as $rowmet) {
				$dmetodologia = $rowmet->dmetodologia;
				$html .= '<tr>
										<td colspan="2" style="text-align: justify;">&nbsp;' . $dmetodologia . '</td>
									</tr>';
			}
		}
		$html .= '<tr>
							<td colspan="2" style="height:30px;"><b>CRITERIOS DE INSPECCION : </b></td>
						</tr>
						<tr>
							<td colspan="2" style="text-align: justify;">&nbsp;' . $dcriterios . '</td>
						</tr>
						<tr>
							<td colspan="2" style="height:30px;"><b>SUSPENSION DE LA INSPECCION : </b></td>
						</tr>
						<tr>
							<td colspan="2">De darse una inspección trunca por permitir el ingreso al inspector al establecimiento o cancelarla a ultima hora tendra un costo según se muestra en la siguiente tabla:</td>
						</tr>
						<tr>
							<td colspan="2" style="height:10px;"></td>
						</tr>
						<tr>
							<td align="center" style="border: 1px solid black;" width="30%"><b>UBICACIONES</b></td>
							<td align="center" style="border: 1px solid black;"width="70%"><b>COSTOS</b></td>
						</tr>
						<tr>
							<td style="border: 1px solid black;"><b>LIMA</b></td>
							<td style="border: 1px solid black; text-align: justify;">El costo por inspecáones canceladas con menos de 2 días útiles a la inspección,  incluidas las canceladas cuando el inspedor a llegado al establecimiento,  será del  30%  en base al costo de la inspección más IGV,  según tipo de establecimiento. </td>
						</tr>
						<tr>
							<td style="border: 1px solid black;"><b>A NIVEL NACIONAL (excepto Lima Metropolitana)</b></td>
							<td style="border: 1px solid black; text-align: justify;">El costo por inspecáones  canceladas con menos de 15 días útiles de anticipación,  incluidas las canceladas  cuando el inspedor a llegado al establecimiento,  será  del  30%  en base al costo de la inspección más IGV,  según tipo de establecimiento, asi como los gastos de viáticos Que incurra el lnspedor.</td>
						</tr>
					</table>';

		$html .= '</main></body></html>';
		$filename = 'Plan-Inspeccion';
		$this->pdfgenerator->generate($html, $filename, TRUE, 'A4', 'portrait');
		//echo $html;

	}

	public function getcbocertificacion()
	{    // Visualizar Proveedor por cliente en CBO

		$ccertificadora = $this->input->post('ccertificadora');
		$resultado = $this->mregctrolprov->getcbocertificacion($ccertificadora);
		echo json_encode($resultado);
	}

	public function calculocriterio()
	{    // Visualizar Proveedor por cliente en CBO

		$vresul = $this->input->post('resultado');
		$ccriterio = $this->input->post('ccriterio');
		$resultado = $this->mregctrolprov->calculocriterio($vresul, $ccriterio);
		echo json_encode($resultado);
	}

	/**
	 * @throws Exception
	 */
	public function setconvalidacion()
	{
		try {

			$cauditoriainspeccion = $this->input->post('txtconvaliidinsp');
			$fservicio = $this->input->post('txtconvalifservicio');
			$ccertificadora = $this->input->post('cbocertificadora');
			$fcertificaciontxt = $this->input->post('txtFConva');
			$presultadocertificadora = $this->input->post('txtconvaliresul');
			$ccriterioresultado = $this->input->post('mhdncritresultconvali');
			$cdetallecriterioresultado = $this->input->post('mhdncritdetresultconvali');
			$dobservacion = $this->input->post('mtxtconvalicomentario');
			$dnrodocumento = $this->input->post('txtconvalidocu');
			$nmes = $this->input->post('txtconvalimes');
			$ccertificacion = $this->input->post('cbocertificacion');
			$cusuario = $this->input->post('hdncusuarioconvali');
			$accion = $this->input->post('mhdnAccionconvali');
			$cboclieserv = $this->input->post('cboclieserv');

			if (empty($fservicio)) {
				throw new Exception('La Fecha de Servicio no puede estar vacía.');
			}
			if (empty($fcertificaciontxt)) {
				throw new Exception('La Fecha de Convalidacion no puede estar vacía.');
			}
			if (empty($ccertificadora)) {
				throw new Exception('Debes elegir una certificadora');
			}
			if (empty($_FILES['mtxtArchivoconvali']['name'])) {
				throw new Exception('Debes elegir un archivo.');
			}

			$fservicio = \Carbon\Carbon::createFromFormat('d/m/Y', $fservicio, 'America/Lima');
			$fcertificacion = \Carbon\Carbon::createFromFormat('d/m/Y', $fcertificaciontxt, 'America/Lima');
			$var_fperiodo = \Carbon\Carbon::createFromFormat('d/m/Y', $fcertificaciontxt, 'America/Lima')
				->addMonths($nmes)
				->setDay(1);

			$parametros = array(
				'@cauditoriainspeccion' => $cauditoriainspeccion,
				'@fservicio' => $fservicio->format('Y-m-d'),
				'@ccertificadora' => $ccertificadora,
				'@fcertificacion' => $fcertificacion->format('Y-m-d'),
				'@var_fperiodo' => $var_fperiodo->format('Y-m-d'),
				'@presultadocertificadora' => $presultadocertificadora,
				'@ccriterioresultado' => $ccriterioresultado,
				'@cdetallecriterioresultado' => $cdetallecriterioresultado,
				'@dobservacion' => $dobservacion,
				'@dnrodocumento' => $dnrodocumento,
				'@nmes' => $nmes,
				'@ccertificacion' => $ccertificacion,
				'@cusuario' => $cusuario,
				'@accion' => $accion,
			);

			$this->mregctrolprov->setconvalidacion($parametros);

			// Carga del archivo adjunto
			$nombreficha = 'CONV' . $cauditoriainspeccion . $fcertificacion->format('Ymd') . '.pdf';
			$rutaficha = RUTA_ARCHIVOS . '10102/' . $cboclieserv . '/' . $cauditoriainspeccion . '/';
			$rutaarchivo = $rutaficha . $nombreficha;
			!is_dir($rutaficha) && @mkdir($rutaficha, 0777, true);
			$config['upload_path']      = $rutaficha;
			$config['file_name']        = $nombreficha;
			$config['allowed_types']    = 'pdf';
			$config['max_size']         = '60048';
			$config['overwrite'] 		= TRUE;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if (!($this->upload->do_upload('mtxtArchivoconvali'))) {
				throw new Exception($this->upload->display_errors());
			}
			// Se actualiza la convalidación de la inspección
			$this->db->update('potracertificadora', ['dubicacionfileserver' => $rutaarchivo], [
				'cauditoriainspeccion' => $cauditoriainspeccion,
				'fservicio' => $fservicio->format('Y-m-d'),
			]);

			$this->result['status'] = 200;
			$this->result['message'] = 'Inspección convalidada correctamente.';

		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

	public function subirArchivoconval()
	{    // Subir Acrhivo
		$IDPROPU = $this->input->post('mhdnIdPropu');
		$ANIO = substr($this->input->post('mtxtFpropu'), -4);
		$NOMBARCH = 'PROP' . substr($this->input->post('mtxtNropropuesta'), 0, 4) . substr($this->input->post('mtxtNropropuesta'), 5, 4) . 'PTFS';

		$RUTAARCH = 'FTPfileserver/Archivos/10201/' . $ANIO . '/';

		!is_dir($RUTAARCH) && @mkdir($RUTAARCH, 0777, true);

		//RUTA DONDE SE GUARDAN LOS FICHEROS
		$config['upload_path'] = $RUTAARCH;
		$config['allowed_types'] = 'pdf|xlsx|docx|xls|doc';
		$config['max_size'] = '60048';
		$config['overwrite'] = TRUE;
		$config['file_name'] = $NOMBARCH;

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if (!($this->upload->do_upload('mtxtArchivopropu'))) {
			//si al subirse hay algun error 
			$data['uploadError'] = $this->upload->display_errors();
			$error = '';
			return $error;
		} else {
			$data = $this->upload->data();
			$parametros = array(
				'@cauditoriainspeccion' => $IDPROPU,
				'@fservicio' => $IDPROPU,
				'@conval_archivo' => $data['file_name'],
				'@dubicacionfileserver' => $config['upload_path'],
				'@conval_nombarch' => $this->input->post('mtxtNomarchpropu'),
			);
			$retorna = $this->mpropuesta->subirArchivo($parametros);
			echo json_encode($retorna);
		}
	}

}

?>
