<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class Cclientes
 *
 * @property matclientes mclientes
 */
class Cclientes extends FS_Controller
{
	function __construct()
	{
		parent:: __construct();
		$this->load->model('at/ctrlprov/mantenimiento/matclientes2', 'mclientes');
		$this->load->model('mglobales');
		$this->load->helper(array('form', 'url', 'download', 'html', 'file'));
		$this->load->library(array('form_validation', 'upload'));
	}

	public function getbuscarclientes()
	{ // Lista de consultas de Cliente
		$resultado = $this->mclientes->getbuscarclientes($this->input->post('cliente'));
		echo json_encode($resultado);
	}

	public function setptcliente()
	{ // Guardar Cliente
		$parametros = array(
			'@ccliente' => $this->input->post('hdnIdptclieprov'),
			'@cgrupoempresarial' => $this->input->post('cboGrupEmp'),
			'@nruc' => $this->input->post('txtnrodoc'),
			'@drazonsocial' => $this->input->post('txtrazonsocial'),
			'@cpais' => $this->input->post('cboPais'),
			'@dciudad' => $this->input->post('txtCiudad'),
			'@destado' => $this->input->post('txtEstado'),
			'@dzip' => $this->input->post('txtCodigopostal'),
			'@cubigeo' => $this->input->post('hdnidubigeo'),
			'@ddireccioncliente' => $this->input->post('txtDireccion'),
			'@dtelefono' => $this->input->post('txtTelefono'),
			'@dfax' => $this->input->post('txtFax'),
			'@dweb' => $this->input->post('txtWeb'),
			'@zctipotamanoempresa' => $this->input->post('cboSizeEmp'),
			'@ntrabajador' => 0,

			'@drepresentante' => $this->input->post('txtRepresentante'),
			'@dcargorepresentante' => $this->input->post('txtCargorep'),
			'@demailrepresentante' => $this->input->post('txtEmailrep'),
			'@accion' => $this->input->post('hdnAccionptclieProv'),
			'@druta' => $this->input->post('utxtlogo'),
			'@tipodoc' => $this->input->post('cboTipoDoc')
		);
		$retorna = $this->mclientes->setptcliente($parametros);
		echo json_encode($retorna);
	}

	/**
	 * Crea o Edita un cliente
	 * @throws Exception
	 */
	public function setcliente()
	{
		try {

			$ccliente = $this->input->post('hdnIdptclie');
			$txtnrodoc = $this->input->post('txtnrodoc');
			$txtrazonsocial = $this->input->post('txtrazonsocial');
			$cboPais = $this->input->post('cboPais');
			$txtCiudad = $this->input->post('txtCiudad');
			$txtEstado = $this->input->post('txtEstado');
			$dzip = '00000';
			$hdnidubigeo = $this->input->post('hdnidubigeo');
			$txtDireccion = $this->input->post('txtDireccion');
			$txtTelefono = $this->input->post('txtTelefono');
			$txtFax = $this->input->post('txtFax');
			$txtWeb = $this->input->post('txtWeb');
			$ntrabajador = 0;
			$txtRepresentante = $this->input->post('txtRepresentante');
			$txtCargorep = $this->input->post('txtCargorep');
			$txtEmailrep = $this->input->post('txtEmailrep');
			$hdnAccionptclie = $this->input->post('hdnAccionptclie');
			$utxtlogo = $this->input->post('utxtlogo');
			$cboTipoDoc = $this->input->post('cboTipoDoc');

			if (empty($txtnrodoc)) {
				throw new Exception('');
			}

			$retorna = $this->mclientes->guardar(
				$ccliente,
				null,
				null,
				null,
				$cboPais,
				$hdnidubigeo,
				$this->session->userdata('s_cusuario'),
				$txtCargorep,
				$txtCiudad,
				$txtDireccion,
				$txtEmailrep,
				$txtEstado,
				$txtFax,
				$txtrazonsocial,
				$txtRepresentante,
				null,
				$txtTelefono,
				$txtWeb,
				$dzip,
				null,
				$txtnrodoc,
				$ntrabajador,
				'A',
				$cboTipoDoc
			);
			echo json_encode($retorna);

		} catch (Exception $ex) {
			$this->result['status'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

	public function upload_image()
	{

		// $newfilename = $this->input->post('hdnCCliente');
		$path = 'FTPfileserver/Imagenes/clientes/';
		if (!file_exists($path)) {
			mkdir($path, 0777, true);
		}

		$config['upload_path'] = $path;
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size'] = '60048';
		$config['overwrite'] = FALSE;

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if (!$this->upload->do_upload('file-input')) {
			$data = $this->upload->display_errors();

			return $data;
		} else {
			$data = $this->upload->data();

			/*$path = array(
				$nombreArch =$data['file_name'],
			);*/

			$retorno = array(
				'nombreArch' => $data['file_name'],
			);
			echo json_encode($retorno);
		}
	}

	public function getbuscarestablecimiento()
	{ // Lista de consultas de Establecimiento
		$parametros = array(
			'@CCLIENTE' => $this->input->post('IDCLIENTE')
		);
		$resultado = $this->mclientes->getbuscarestablecimiento($parametros);
		echo json_encode($resultado);
	}

	public function mantgral_establecimiento()
	{ // Guardar Establecimiento
		$parametros = array(
			'@CCLIENTE' => $this->input->post('mhdnIdClie'),
			'@CESTABLECIMIENTO' => $this->input->post('mhdnIdEstable'),
			'@DESTABLECIMIENTO' => $this->input->post('txtestableCI'),
			'@DDIRECCION' => $this->input->post('txtestabledireccion'),
			'@DZIP' => '000000',
			'@ESTADO' => $this->input->post('cboestableEstado'),
			'@ACCION' => $this->input->post('mhdnAccionEstable'),
			'@TELEFONO' => $this->input->post('txtestablecelu'),
			'@cubigeo' => $this->input->post('hdnidubigeoEstable'),
			'@cpais' => $this->input->post('cboPaisEstable'),
			'@dciudad' => $this->input->post('txtCiudadEstable'),
			'@destado' => $this->input->post('txtEstadoEstable'),
			'@referencia' => $this->input->post('txtreferenciadir'),
		);
		$retorna = $this->mclientes->mantgral_establecimiento($parametros);
		echo json_encode($retorna);
	}


	public function getbuscarproveedor()
	{ // Lista de consultas de Cliente
		$parametros = array(
			'@CLIENTE' => $this->input->post('cliente')
		);
		$resultado = $this->mclientes->getbuscarproveedor($parametros);
		echo json_encode($resultado);
	}

	public function getaddproveedor()
	{ // Lista de consultas de Cliente
		$parametros = array(
			'@CLIENTE' => $this->input->post('cliente')
		);
		$resultado = $this->mclientes->getaddproveedor($parametros);
		echo json_encode($resultado);
	}

	public function insertproveedorcliente()
	{ // Lista de consultas de Cliente
		$parametros = array(
			'@cliente' => $this->input->post('ccliente'),
			'@proveedor' => $this->input->post('cproveedor'),
			'@usuario' => $this->session->userdata('s_cusuario')
		);
		$resultado = $this->mclientes->insertproveedorcliente($parametros);
		echo json_encode($resultado);
	}

	public function setptclientexproveedor()
	{ // Guardar nuevo proveedor
		$parametros = array(
			'@ccliente' => $this->input->post('hdnIdptclieprov'),
			'@nruc' => $this->input->post('txtnrodoc'),
			'@drazonsocial' => $this->input->post('txtrazonsocial'),
			'@cpais' => $this->input->post('cboPais'),
			'@dciudad' => $this->input->post('txtCiudad'),
			'@destado' => $this->input->post('txtEstado'),
			'@dzip' => '000000',
			'@cubigeo' => $this->input->post('hdnidubigeo'),
			'@ddireccioncliente' => $this->input->post('txtDireccion'),
			'@dtelefono' => $this->input->post('txtTelefono'),
			'@dfax' => $this->input->post('txtFax'),
			'@dweb' => $this->input->post('txtWeb'),
			'@zctipotamanoempresa' => 112,
			'@ntrabajador' => 0,
			'@drepresentante' => $this->input->post('txtRepresentante'),
			'@dcargorepresentante' => $this->input->post('txtCargorep'),
			'@demailrepresentante' => $this->input->post('txtEmailrep'),
			'@accion' => $this->input->post('hdnAccionptclieProv'),
			'@druta' => $this->input->post('utxtlogo'),
			'@tipodoc' => $this->input->post('cboTipoDoc'),
			'@usuario' => $this->session->userdata('s_cusuario')
		);
		$retorna = $this->mclientes->setptclientexproveedor($parametros);
		echo json_encode($retorna);
	}

	public function setproveedorxmaquilador()
	{ // Guardar nuevo proveedor
		$parametros = array(
			'@ccliente' => $this->input->post('hdnIdptcliemaq'),
			'@nruc' => $this->input->post('txtnrodoc'),
			'@drazonsocial' => $this->input->post('txtrazonsocial'),
			'@cpais' => $this->input->post('cboPais'),
			'@dciudad' => $this->input->post('txtCiudad'),
			'@destado' => $this->input->post('txtEstado'),
			'@dzip' => $this->input->post('txtCodigopostal'),
			'@cubigeo' => $this->input->post('hdnidubigeo'),
			'@ddireccioncliente' => $this->input->post('txtDireccion'),
			'@dtelefono' => $this->input->post('txtTelefono'),
			'@dfax' => $this->input->post('txtFax'),
			'@dweb' => $this->input->post('txtWeb'),
			'@zctipotamanoempresa' => 112,
			'@ntrabajador' => 0,
			'@drepresentante' => $this->input->post('txtRepresentante'),
			'@dcargorepresentante' => $this->input->post('txtCargorep'),
			'@demailrepresentante' => $this->input->post('txtEmailrep'),
			'@accion' => $this->input->post('hdnAccionptclieMaq'),
			'@druta' => $this->input->post('utxtlogo'),
			'@tipodoc' => $this->input->post('cboTipoDoc'),
			'@usuario' => $this->session->userdata('s_cusuario')
		);
		$retorna = $this->mclientes->setproveedorxmaquilador($parametros);
		echo json_encode($retorna);
	}

	public function getservicios()
	{
		$resultado = $this->mclientes->getservicios();
		echo json_encode($resultado);
	}

	public function getsubservicios()
	{
		$id = $this->input->post('id');
		$resultado = $this->mclientes->getsubservicios($id);
		echo json_encode($resultado);
	}

	public function getbuscarcontacto()
	{ // Lista de consultas de contactos

		$id_cliente = $this->input->post('cliente');

		$resultado = $this->mclientes->getbuscarcontacto($id_cliente);
		echo json_encode($resultado);
	}

	public function createcontacto()
	{ // Lista de consultas de contactos
		try {
			$this->form_validation->set_rules('txtNombresContacto', 'txtNombresContacto', 'required');
			$this->form_validation->set_rules('txtEmailCon', 'txtEmailCon', 'required');
			$this->form_validation->set_rules('mhdnIdContacto', 'mhdnIdContacto', 'required');

			if ($this->form_validation->run() == FALSE) {
				echo false;
			} else {
				$parametros = array(
					'@ACCION' => $this->input->post('mhdnAccionContacto'),
					'@CCONTACTO' => null,
					'@CCLIENTE' => $this->input->post('mhdnIdContacto'),
					'@CESTABLECIMIENTO' => ($this->input->post('txtestablecimiento') == '') ? null : $this->input->post('txtestablecimiento'),
					'@DAPEPAT' => $this->input->post('txtApePaterno'),
					'@DAPEMAT' => $this->input->post('txtApeMaterno'),
					'@DNOMBRES' => $this->input->post('txtNombresContacto'),
					'@CARGO' => $this->input->post('txtCargoEmp'),
					'@DEMAIL' => $this->input->post('txtEmailCon'),
					'@ESTADO' => 'A',
					'@DTELEFONO' => $this->input->post('txtTelefonoCon')
				);
				$resultado = $this->mclientes->createcontacto($parametros);
				echo json_encode($resultado);

			}
		} catch (Exception $ex) {
			$this->result['mensagge'] = $ex->getMessage();
		}

	}

	public function getbuscarcontactoxestable()
	{ // Lista de consultas de contactos

		$id_cliente = $this->input->post('cliente');
		$id_estable = $this->input->post('cod_estable');

		$resultado = $this->mclientes->getbuscarcontactoxestable($id_cliente, $id_estable);
		echo json_encode($resultado);
	}

	public function addcontactoxestablecimiento()
	{ // Lista de consultas de contactos

		$id_contacto = $this->input->post('ccontacto');
		$id_estable = $this->input->post('cod_estable');
		//echo $id_contacto;
		//obtener los datos del contacto
		$query = $this->db->get_where('MCONTACTO', array('CCONTACTO' => $id_contacto));
		$row = $query->row();
		//var_dump($row);
		$parametros = array(
			'@ACCION' => 'N',
			'@CCONTACTO' => null,
			'@CCLIENTE' => $row->CCLIENTE,
			'@CESTABLECIMIENTO' => $id_estable,
			'@DAPEPAT' => $row->DAPEPAT,
			'@DAPEMAT' => $row->DAPEMAT,
			'@DNOMBRES' => $row->DNOMBRE,
			'@CARGO' => $row->DCARGOCONTACTO,
			'@DEMAIL' => $row->DMAIL,
			'@ESTADO' => 'A',
			'@DTELEFONO' => $row->DTELEFONO
		);

		$resultado = $this->mclientes->createcontacto($parametros);
		echo json_encode($resultado);
	}

	public function getbuscaraddcontacto()
	{ // Lista de consultas de contactos

		$id_cliente = $this->input->post('cliente');
		$id_estable = $this->input->post('cod_estable');

		$resultado = $this->mclientes->getbuscarcontacto($id_cliente);
		echo json_encode($resultado);
	}

	public function getbuscarlineaxestablec()
	{ // Lista de consultas de contactos

		$id_cliente = $this->input->post('cliente');
		$id_estable = $this->input->post('cod_estable');

		$resultado = $this->mclientes->getbuscarlineaxestablec($id_cliente, $id_estable);
		echo json_encode($resultado);
	}

	public function createLineaProc()
	{

		try {
			$this->form_validation->set_rules('txtestablecimiento', 'txtestablecimiento', 'required');
			$this->form_validation->set_rules('txtLineaProc', 'txtLineaProc', 'required');
			$this->form_validation->set_rules('hdnIdptClieLinea', 'hdnIdptClieLinea', 'required');

			if ($this->form_validation->run() == FALSE) {
				echo false;
			} else {
				$parametros = array(
					'@ACCION' => $this->input->post('hdnAccionptclieLinea'),
					'@CLINEAPROCESOCLIENTE' => null,
					'@CCLIENTE' => $this->input->post('hdnIdptClieLinea'),
					'@CESTABLECIMIENTO' => $this->input->post('txtestablecimiento'),
					'@DLINEACLIENTEE' => $this->input->post('txtLineaProc')

				);
				$resultado = $this->mclientes->createLineaProc($parametros);
				echo json_encode($resultado);

			}
		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
	}

	public function getResponsablesOI()
	{
		$resultado = $this->mclientes->getResponsablesOI();
		echo json_encode($resultado);
	}

	public function createServicio()
	{
		$this->db->select('MAX(CINTERNOPTE) AS CINTERNOPTE', false);
		$query = $this->db->get('PCPTE');

		$id_ultimo = $query->row()->CINTERNOPTE;
		$id_ultimo_anio = substr($id_ultimo, 0, 4);

		$anio_actual = date("Y");

		if ($id_ultimo_anio === $anio_actual) {
			$codigo = $id_ultimo + 1;
		} else {
			$codigo = $anio_actual . '0000';
		}

		$str_numero_pdte = substr($codigo, 4, 4) . '-' . $anio_actual . '/AT' . '/FS';

		$parametros = array(
			'@CINTERNOPTE' => $codigo,
			'@DNUMEROPTE' => $str_numero_pdte,
			'@CCLIENTE' => $this->input->post('idCLienteServ'),
			'@CSERVICIO' => $this->input->post('cboServicio'),
			'@CSUBSERVICIO' => $this->input->post('cboSubServicio'),
			'@CRESPONSABLE' => $this->input->post('cboResponsableOI')

		);

		$resultado = $this->mclientes->createServicio($parametros);
		echo json_encode($resultado);
	}

	public function getserviciosxcliente()
	{
		$id = array('@ccliente' => $this->input->post('cliente'));
		$resultado = $this->mclientes->getserviciosxcliente($id);
		echo json_encode($resultado);
	}

	public function createarea()
	{
		$cliente = $this->input->post('mhdnIdClieArea');
		$this->db->select_max('CAREACLIENTE');
		$this->db->where('CCLIENTE', $cliente);
		$query = $this->db->get('mareacliente');

		$int_area_cliente = $query->row()->CAREACLIENTE;

		if ($int_area_cliente !== '') {
			$int_area_cliente = $int_area_cliente + 1;
		} else {
			$int_area_cliente = '001';
		}


		$parametros = array(
			'@ccliente' => $cliente,
			'@careacliente' => $int_area_cliente,
			'@dareacliente' => $this->input->post('str_descripcion_area'),
			'@estado' => $this->input->post('cboEstado'),
			'@concesionario' => $this->input->post('cboConsecionario'),
			'@ctipo' => $this->input->post('cboTipoArea')

		);

		$resultado = $this->mclientes->createarea($parametros);
		echo json_encode($resultado);
	}

	public function getareacliente()
	{
		$id = array('@cclientes' => $this->input->post('ccliente'));
		$resultado = $this->mclientes->getareacliente($id);
		echo json_encode($resultado);
	}
}

?>
