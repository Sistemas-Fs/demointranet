<?php

/**
 * Class cquejapeligros
 *
 * @property mquejapeligros mquejapeligros
 */
class cquejapeligros extends FS_Controller
{

	/**
	 * cquejapeligros constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('at/ctrlprov/inspctrolprov/mquejapeligros');
	}

	/**
	 * Archivo de peligro
	 */
	public function descargar_peligro()
	{
		$archivoPeligro = $this->mquejapeligros->obtenerArchivoPeligro();
		$archivo = null;
		$nombre = null;
		if (!empty($archivoPeligro)) {
			$archivo = base_url('FTPfileserver/Archivos/' . $archivoPeligro);
			$pos = (strrpos($archivoPeligro, '/') == false) ? strrpos($archivoPeligro, '\\') : strrpos($archivoPeligro, '/');
			$nombre = substr($archivoPeligro, $pos + 1, strlen($archivoPeligro));
		}
		echo json_encode([
			'nombre' => $nombre,
			'archivo' => $archivo,
		]);
	}

	/**
	 * Archivo de quejas
	 */
	public function descargar_queja()
	{
		$archivoQueja = $this->mquejapeligros->obtenerArchivoQuejas();
		$archivo = null;
		$nombre = null;
		if (!empty($archivoQueja)) {
			$archivo = base_url('FTPfileserver/Archivos/' . $archivoQueja);
			$pos = (strrpos($archivoQueja, '/') == false) ? strrpos($archivoQueja, '\\') : strrpos($archivoQueja, '/');
			$nombre = substr($archivoQueja, $pos + 1, strlen($archivoQueja));
		}
		echo json_encode([
			'nombre' => $nombre,
			'archivo' => $archivo,
		]);
	}

	/**
	 * Sube los archivos de queja y peligros
	 */
	public function cargar()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {

			if (empty($_FILES['file_quejas']['name']) && empty($_FILES['file_peligro']['name'])) {
				throw new Exception('Debes elegir un archivo.');
			}

			if (!empty($_FILES['file_quejas']['name'])) {
				$carpetas = 'ALIMENTOS/';
				$rutaficha = RUTA_ARCHIVOS . $carpetas;

				!is_dir($rutaficha) && @mkdir($rutaficha, 0777, true);

				$config['upload_path']      = $rutaficha;
				$config['allowed_types']    = '*';
				$config['max_size']         = 0;
				$config['overwrite'] 		= TRUE;

				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if (!($this->upload->do_upload('file_quejas'))) {
					throw new Exception($this->upload->display_errors());
				}

				$nombreArchivo = $this->upload->data('file_name');
				$rutaCompleta = $carpetas . $nombreArchivo;
				$this->db->update('TTABLA', ['DREGISTRO' => $rutaCompleta], ['CTIPO' => '568']);
			}

			if (!empty($_FILES['file_peligro']['name'])) {
				$carpetas = 'ALIMENTOS/';
				$rutaficha = RUTA_ARCHIVOS . $carpetas;

				!is_dir($rutaficha) && @mkdir($rutaficha, 0777, true);

				$config['upload_path']      = $rutaficha;
				$config['allowed_types']    = '*';
				$config['max_size']         = 0;
				$config['overwrite'] 		= TRUE;

				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if (!($this->upload->do_upload('file_peligro'))) {
					throw new Exception($this->upload->display_errors());
				}

				$nombreArchivo = $this->upload->data('file_name');
				$rutaCompleta = $carpetas . $nombreArchivo;
				$this->db->update('TTABLA', ['DREGISTRO' => $rutaCompleta], ['CTIPO' => '569']);
			}

			$this->result['status'] = 200;
			$this->result['message'] = 'Archivos cargados correctamente.';

		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

	public function eliminar()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {
			$id = $this->input->post('id');

			if ($id != '569' && $id != '568') {
				throw new Exception('Error en el codigo enviado.');
			}

			$archivo = $this->mquejapeligros->obtenerArchivo($id);
			if (empty($archivo)) {
				throw new Exception('EL archivo no pudo ser encontrado');
			}

			$rutaFicha = RUTA_ARCHIVOS . $archivo->DREGISTRO;
			if (file_exists($rutaFicha)) {
				unlink($rutaFicha);
			}

			$this->db->update('TTABLA', ['DREGISTRO' => ''], ['CTIPO' => $id]);

			$this->result['status'] = 200;
			$this->result['message'] = 'Eliminado';
		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

}
