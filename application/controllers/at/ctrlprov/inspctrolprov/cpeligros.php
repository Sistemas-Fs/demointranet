<?php

/**
 * Class cpeligros
 *
 * @property Mpeligros mpeligros
 */
class cpeligros extends FS_Controller
{

	/**
	 * cpeligros constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('at/ctrlprov/inspctrolprov/mpeligros');
	}

	/**
	 * Lista de peligros
	 */
	public function lista()
	{
		$cauditoria = $this->input->post('cauditoria');
		$fservicio = $this->input->post('fservicio');
		$resultado = $this->mpeligros->listar($cauditoria, $fservicio);
		echo json_encode($resultado);
	}

	/**
	 * Guarda el peligro
	 */
	public function guardar()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {

			$cauditoria = $this->input->post('cauditoria');
			$fservicio = $this->input->post('fservicio');
			$peligro_producto = $this->input->post('peligro_producto');
			$peligro_cliente = $this->input->post('peligro_cliente');
			$peligro_proveedor = $this->input->post('peligro_proveedor');
			$peligro_inspeccion = $this->input->post('peligro_inspeccion');
			$peligro_observacion = $this->input->post('peligro_observacion');

			$peligro_producto = trim($peligro_producto);
			$peligro_cliente = trim($peligro_cliente);
			$peligro_proveedor = trim($peligro_proveedor);
			$peligro_inspeccion = trim($peligro_inspeccion);
			$peligro_observacion = trim($peligro_observacion);

			if (strlen($peligro_producto) > 500) {
				throw new Exception('Campo producto solo permite hasta 500 caracteres.');
			}
			if (strlen($peligro_cliente) > 500) {
				throw new Exception('Campo cliete solo permite hasta 500 caracteres.');
			}
			if (strlen($peligro_proveedor) > 500) {
				throw new Exception('Campo proveedor solo permite hasta 500 caracteres.');
			}
			if (strlen($peligro_inspeccion) > 500) {
				throw new Exception('Campo inspeccion solo permite hasta 500 caracteres.');
			}
			if (strlen($peligro_observacion) > 800) {
				throw new Exception('Campo observación solo permite hasta 800 caracteres.');
			}

			$this->mpeligros->guardar(
				$cauditoria,
				$fservicio,
				$peligro_producto,
				$peligro_cliente,
				$peligro_proveedor,
				$peligro_inspeccion,
				$peligro_observacion,
				$this->session->userdata('s_cusuario'),
				'A'
			);

			$this->result['status'] = 200;
			$this->result['message'] = 'Peligro guardado correctamente.';

		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

	/**
	 * Elimina un peligro
	 */
	public function eliminar()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {

			$cauditoria = $this->input->post('cauditoria');
			$fservicio = $this->input->post('fservicio');
			$norden = $this->input->post('norden');

			$resp = $this->db->delete('ppeligro', [
				'CAUDITORIAINSPECCION' => $cauditoria,
				'FSERVICIO' => $fservicio,
				'NORDENPELIGRO' => $norden,
			]);

			if (!$resp) {
				throw new Exception('El peligro no pudo ser eliminado.');
			}

			$this->result['status'] = 200;
			$this->result['message'] = "Peligro eliminado correctamente.";

		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

}
