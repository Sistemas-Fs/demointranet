<?php

/**
 * Class cchecklist
 *
 * @property mchecklist mchecklist
 * @property maccion_correctiva maccion_correctiva
 * @property minspeccion minspeccion
 */
class cchecklist extends FS_Controller
{


	/**
	 * cchecklist constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('at/ctrlprov/inspctrolprov/mchecklist');
		$this->load->model('at/ctrlprov/inspctrolprov/maccion_correctiva');
		$this->load->model('at/ctrlprov/inspctrolprov/minspeccion');
	}

	/**
	 * Lista del checklist de la inspeccion
	 */
	public function lista()
	{
		$parametros = array(
			'@cauditoriainspeccion' => $this->input->post('cauditoriainspeccion'),
			'@fechaservicio' => $this->input->post('fservicio')
		);
		$resultado = $this->mchecklist->lista($parametros);
		echo json_encode($resultado);
	}

	/**
	 * Se obtiene los valores del checklist
	 */
	public function valores()
	{
		$resultado = $this->mchecklist->listaValores(['@int_valor' => $this->input->post('int_valor')]);
		echo json_encode($resultado);
	}

	/**
	 * Lista de criterios de hallazgos
	 */
	public function criterioHallazgo()
	{
		$cauditoria = $this->input->post('cauditoria');
		$fservicio = $this->input->post('fservicio');
		$resultado = $this->mchecklist->listaCriterioHallazgos($cauditoria, $fservicio);
		echo json_encode($resultado);
	}

	/**
	 * Guarda el checklist de la inspeccion
	 */
	public function guardar()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {

			$cmbPuntuacionChecklist = $this->input->post('cmbPuntuacionChecklist');
			$cmbCriterioHallazgo = $this->input->post('cmbCriterioHallazgo');
			$txtHallazgo = $this->input->post('txtHallazgo');
			$int_codigo_checklist = $this->input->post('int_codigo_checklist'); // cchecklist
			$int_requisito_checklist = $this->input->post('int_requisito_checklist'); // crequisitochecklist
			$int_auditoria_inspeccion = $this->input->post('int_auditoria_inspeccion');
			$cauditoria = $this->input->post('cauditoria');
			$fservicio = $this->input->post('fservicio');
			$criterio_hallazgo_valor = $this->input->post('criterio_hallazgo_valor');

			$inspeccion = $this->minspeccion->buscarDauditoria($cauditoria, $fservicio);
			if (empty($inspeccion)) {
				throw new Exception('La inspección no pudo ser encontrado.');
			}

			if (empty($int_codigo_checklist) || empty($int_requisito_checklist) || empty($int_auditoria_inspeccion)) {
				throw new Exception('Falta de parametros para continuar.');
			}
			if (empty($cmbPuntuacionChecklist)) {
				throw new Exception('Debes elegir una puntuación.');
			}
			if ($cmbPuntuacionChecklist == "001" || $cmbPuntuacionChecklist == "002" || $cmbPuntuacionChecklist == "003") {
				if (empty($cmbCriterioHallazgo)) {
					$cmbCriterioHallazgo = null;
				}
			}

			if (empty($this->session->userdata('s_cusuario'))) {
				throw new Exception('Debes volver a iniciar sesión.');
			}

			$txtHallazgo = trim($txtHallazgo);

			$this->db->trans_begin();

			$response = $this->mchecklist->guardar([
				'@cmbPuntuacionChecklist' => $cmbPuntuacionChecklist,
				'@cmbCriterioHallazgo' => $cmbCriterioHallazgo,
				'@txtHallazgo' => $txtHallazgo,
				'@int_codigo_checklist' => $int_codigo_checklist,
				'@int_requisito_checklist' => $int_requisito_checklist,
				'@int_auditoria_inspeccion' => $int_auditoria_inspeccion,
			]);

			if (!$response) {
				throw new Exception('Error en el proceso de ejecución.');
			}

			// Se crea la acción correctiva
			if ($criterio_hallazgo_valor) {
				$this->maccion_correctiva->guardar(
					$cauditoria,
					$fservicio,
					$int_codigo_checklist,
					$int_requisito_checklist,
					'',
					null,
					'N',
					null,
					$this->session->userdata('s_cusuario'),
					'A',
					null,
					date('Y-m-d h:i:s'),
					''
				);
			}

			// Actualizacion del resultado checklist
			$resultado = $this->mchecklist->lista([
				'@cauditoriainspeccion' => $cauditoria,
				'@fechaservicio' => $fservicio
			]);

			$totalValorMax = 0;
			$totalNoConformidad = 0;
			$totalExcluyentes = 0;
			if (!empty($resultado)) {
				foreach ($resultado as $key => $value) {
					if ($value->srequisito == 'H') {
						$nValorMax = (isset($value->nvalormaxrequisito)) ? floatval($value->nvalormaxrequisito) : null;
						$noconformidad = (isset($value->nvalorrequisito) && !empty($value->nvalorrequisito)) ? floatval($value->nvalorrequisito) : -1000;
						if (!empty($nValorMax)) {
							// Cuando es NA, NE o SC es cero
							if ($value->cdetallevalorrequisito === "004" || $value->cdetallevalorrequisito === "005" || $value->cdetallevalorrequisito === "006") {
								$nValorMax = 0;
							}
							$totalValorMax += $nValorMax;
							if ($noconformidad >= 0) {
								$totalNoConformidad += $noconformidad;
							}
						}
						// Solo si es un exluyente con monto cero se le considera para bajarle el nivel
						if (strtoupper($value->sexcluyente) == 'S' && $noconformidad == 0) {
							++$totalExcluyentes;
						}
					}
				}
			}

			$nvalorResultado = ($totalValorMax > 0) ? round(($totalNoConformidad * 100) / $totalValorMax, 2) : 0;
			$CDETALLECRITERIORESULTADO = null;
			$DDETALLECRITERIORESULTADO = '';
			$CDETALLECRITERIORESULTADO_MAYOR = 0;

			// Buscar detalle resultado
			$detalleResultado = $this->mchecklist->buscarDetalleResultado($inspeccion->CCRITERIORESULTADO);
			if (!empty($detalleResultado)) {
				foreach ($detalleResultado as $key => $resultado) {
					$nvalorInicial = floatval($resultado->NVALORINICIAL);
					$nvalorfinal = floatval($resultado->NVALORFINAL);
					// Solo si cumple con el valor
					if ($nvalorResultado >= $nvalorInicial && $nvalorResultado <= $nvalorfinal) {
						$CDETALLECRITERIORESULTADO = $resultado->CDETALLECRITERIORESULTADO;
						$DDETALLECRITERIORESULTADO = $resultado->DDETALLECRITERIORESULTADO;
					}
					// Se almacena el mayor ID
					if ($CDETALLECRITERIORESULTADO_MAYOR < $resultado->CDETALLECRITERIORESULTADO) {
						$CDETALLECRITERIORESULTADO_MAYOR = $resultado->CDETALLECRITERIORESULTADO;
					}
				}
			}

			// Se actualiza el criterio de resultado encontrado
			if (!empty($CDETALLECRITERIORESULTADO)) {
				// Se bajara de nivel en caso tenga excluyentes
				if ($totalExcluyentes > 0) {
					$CDETALLECRITERIORESULTADO = $CDETALLECRITERIORESULTADO + $totalExcluyentes;
					// Solo si supera el mayor rango se toma el rango mayor encontrado en el resultado
					if ($CDETALLECRITERIORESULTADO > $CDETALLECRITERIORESULTADO_MAYOR) {
						$CDETALLECRITERIORESULTADO = $CDETALLECRITERIORESULTADO_MAYOR;
					}
					// Se busca el criterio de resultado y se cambia a la minima expresion
					$DETALLECRITERIORESULTADO = $this->mchecklist->buscarDetalleResultado($inspeccion->CCRITERIORESULTADO, $CDETALLECRITERIORESULTADO);
					$nvalorResultado = $DETALLECRITERIORESULTADO[0]->NVALORFINAL;
					$DDETALLECRITERIORESULTADO = $DETALLECRITERIORESULTADO[0]->DDETALLECRITERIORESULTADO;
				}
				$this->db->update('PDAUDITORIAINSPECCION', [
					'NRESULTADOCHECKLIST' => $nvalorResultado,
					'PRESULTADOCHECKLIST' => $nvalorResultado,
					'CDETALLECRITERIORESULTADO' => $CDETALLECRITERIORESULTADO,
				], [
					'CAUDITORIAINSPECCION' => $cauditoria,
					'FSERVICIO' => $fservicio,
				]);
			}

			$this->db->query('CALL sp_formula_insprov_valmax_01(?,?,?)', [
				'@AS_CAUDI' => $cauditoria,
				'@AD_FECHA' => $fservicio,
				'@AS_CCHECKLIST' => $inspeccion->CCHECKLIST,
			]);

			$this->db->query('CALL sp_formula_insprov_valobtenido_01(?,?,?)', [
				'@AS_CAUDI' => $cauditoria,
				'@AD_FECHA' => $fservicio,
				'@AS_CCHECKLIST' => $inspeccion->CCHECKLIST,
			]);

			$this->db->trans_commit();

			$this->result['status'] = 200;
			$this->result['message'] = 'CheckList guardado correctamente.';
			$this->result['data']['resultado_porcentaje'] = $nvalorResultado;
			$this->result['data']['resultado'] = $DDETALLECRITERIORESULTADO;

		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
			$this->db->trans_rollback();
		}

		responseResult($this->result);
	}

}
