<?php

/**
 * Class cresumen_contacto
 *
 * @property minspeccion minspeccion
 * @property mresumen_contacto mresumen_contacto
 */
class cresumen_contacto extends FS_Controller
{

	/**
	 * cresumen_contacto constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('at/ctrlprov/inspctrolprov/minspeccion');
		$this->load->model('at/ctrlprov/inspctrolprov/mresumen_contacto');
	}

	/**
	 * Lista de contactos
	 */
	public function lista()
	{
		$cauditoria = $this->input->post('cauditoria');
		$fservicio = $this->input->post('fservicio');
		$resultado = $this->mresumen_contacto->lista($cauditoria, $fservicio);
		echo json_encode(['items' => $resultado]);
	}

	/**
	 * Guarda el contacto de la inspeccion
	 */
	public function guardar()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {

			$id = $this->input->post('id');
			$cauditoria = $this->input->post('cauditoria');
			$fservicio = $this->input->post('fservicio');
			$res_contact_apepat = $this->input->post('apepat');
			$res_contact_apemat = $this->input->post('apemat');
			$res_contact_nombres = $this->input->post('nombres');
//			$nombre = $this->input->post('res_contact_apepat');
			$cargo = $this->input->post('cargo');
			$email = $this->input->post('email');
			$telefonos = $this->input->post('telefonos');
			$tipo = $this->input->post('tipo');
			$ccliente = $this->input->post('ccliente');

			$inspeccion = $this->minspeccion->buscarCauditoria($cauditoria);
			if (empty($inspeccion)) {
				throw new Exception('La inspección no pudo ser encontrada.');
			}
			$idCliente = $inspeccion->CCLIENTE;
			if ($tipo == 1) {
				$cliente = $this->mresumen_contacto->buscarCliente($ccliente);
				if (empty($cliente)) {
					throw new Exception('El cliente no pudo ser encontrado.');
				}
				$idCliente = $cliente->CCLIENTE;
			}

			if ($tipo == 1) {
				$resp = $this->mresumen_contacto->guardar(
					$id,
					$inspeccion->CAUDITORIAINSPECCION,
					$fservicio,
					$idCliente,
					$inspeccion->CESTABLECIMIENTOPROV,
					trim($res_contact_apepat),
					trim($res_contact_apemat),
					trim($res_contact_nombres),
					$cargo,
					$email,
					$telefonos,
					$this->session->userdata('s_cusuario')
				);
			} else {
				$resp = $this->db->insert('presponsableaudinsp', [
					'CAUDITORIAINSPECCION' => $inspeccion->CAUDITORIAINSPECCION,
					'FSERVICIO' => $fservicio,
					'CCONTACTO' => $id,
					'CUSUARIOCREA' => $this->session->userdata('s_cusuario'),
					'TCREACION' => date('Y-m-d H:i:s'),
					'TMODIFICACION' => null,
					'CUSUARIOMODIFICA' => null,
					'SREGISTRO' => 'A',
				]);
			}

			if (!$resp) {
				throw new Exception('No se pudo guardar correctamente el contacto.');
			}

			$this->result['status'] = 200;
			$this->result['message'] = 'Contacto creado correctamente.';

		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

	/**
	 * Elimina un contacto
	 */
	public function eliminar()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {
			$cauditoria = $this->input->post('cauditoria');
			$fservicio = $this->input->post('fservicio');
			$contact_id = $this->input->post('contact_id');

			$responsable = $this->mresumen_contacto->buscarResponsable($cauditoria, $fservicio, $contact_id);
			if (empty($responsable)) {
				throw new Exception('El responsable no pudo ser encontrado.');
			}

			$resp = $this->db->delete('presponsableaudinsp', [
				'CAUDITORIAINSPECCION' => $responsable->CAUDITORIAINSPECCION,
				'FSERVICIO' => $responsable->FSERVICIO,
				'CCONTACTO' => $responsable->CCONTACTO,
			]);

			if ($resp) {
				$this->db->delete('MCONTACTO', ['CCONTACTO' => $responsable->CCONTACTO]);
			}

			$this->result['status'] = 200;
			$this->result['message'] = 'Responsable eliminado correctamente.';

		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

	public function autocompletado()
	{
		$cauditoria = $this->input->get('cauditoria');
		$search = $this->input->get('search');

		$inspeccion = $this->minspeccion->buscarCauditoria($cauditoria);
		if (empty($inspeccion)) {
			throw new Exception('La inspección no pudo ser encontrada.');
		}

		$items = $this->mresumen_contacto->autocompletado($search, $inspeccion->CCLIENTE, $inspeccion->CPROVEEDORCLIENTE, $inspeccion->CMAQUILADORCLIENTE);
		echo json_encode(['items' => $items]);
	}

	public function autocompletado_clientes()
	{
		$cauditoria = $this->input->post('cauditoria');
		$search = $this->input->get('search');

		$inspeccion = $this->minspeccion->buscarCauditoria($cauditoria);

		$items = $this->mresumen_contacto->listaClientes($search, $inspeccion->CCLIENTE, $inspeccion->CPROVEEDORCLIENTE, $inspeccion->CMAQUILADORCLIENTE);
		echo json_encode(['items' => $items]);
	}

}
