<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
define("DOMPDF_ENABLE_REMOTE", true);

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Style\Conditional;
use PhpOffice\PhpSpreadsheet\Writer\IWriter;
use PhpOffice\PhpSpreadsheet\IOFactory;

/**
 * Class ccons_insp
 * @property mcons_insp mcons_insp
 */
class ccons_insp extends FS_Controller
{
	/**
	 * CODIGO CIA
	 */
	const CIA = '1';

	/**
	 * CODIGO DE AREA
	 */
	const AREA = '01';

	/**
	 * CODIGO DE SERVICIO
	 */
	const SERVICIO = '02';

	/**
	 * ccons_insp constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('consinsp');
		$this->load->model('at/ctrlprov/mcons_insp', 'mcons_insp');
	}

	/**
	 * Busqueda de areas del cliente
	 */
	public function get_areas()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		$ccliente = $this->input->post('ccliente');
		$items = $this->mcons_insp->getAreaCliente([
			'@AS_CCLIENTE' => $ccliente,
		]);
		echo json_encode(['items' => $items]);
	}

	/**
	 * Devuelve las inspecciones
	 */
	public function search()
	{
		$ccia = $this->input->post('ccia');
		$afecha = (int)$this->input->post('afecha');
		$fini = $this->input->post('fini');
		$ffin = $this->input->post('ffin');
		$ccliente = $this->input->post('filtro_cliente');
		$cclienteprov = $this->input->post('filtro_proveedor');
		$cclientemaquila = $this->input->post('filtro_maquilador');
		$careacliente = $this->input->post('filtro_cliente_area');
		$tipoestado = $this->input->post('filtro_tipo_estado');
		$peligro = $this->input->post('filtro_peligro');
		$sevalprod = $this->input->post('sevalprod');
		$filtro_calificacion = $this->input->post('filtro_calificacion');
		$establecimientoMaqui = $this->input->post('filtro_establecimiento_maqui');
		$dirEstablecimientoMaqui = $this->input->post('filtro_dir_establecimiento_maqui');
		$nroInforme = $this->input->post('filtro_nro_informe');

		$inspecciones = '';
		if (!empty($ccliente)) {
			$area = self::AREA;
			$cservicio = self::SERVICIO;
			$cclienteprov = (empty($cclienteprov)) ? '%' : $cclienteprov;
			$cclientemaquila = (empty($cclientemaquila)) ? '%' : $cclientemaquila;
			$careacliente = (empty($careacliente)) ? [] : $careacliente;
			$establecimientoMaqui = is_null($establecimientoMaqui) ? '%' : "%{$establecimientoMaqui}%";
			$dirEstablecimientoMaqui = is_null($dirEstablecimientoMaqui) ? '%' : "%{$dirEstablecimientoMaqui}%";
			$nroInforme = is_null($nroInforme) ? '%' : "%{$nroInforme}%";
			$tipoestado = (empty($tipoestado)) ? [] : $tipoestado;
			$peligro = empty($peligro) ? '%' : $peligro;
			$sevalprod = is_null($sevalprod) ? '%' : $sevalprod;
			$filtro_calificacion = is_null($filtro_calificacion) ? [] : $filtro_calificacion;

			// Se verifica si filtrara con fecha o no
			$now = \Carbon\Carbon::now('America/Lima')->format('Y-m-d');
			if (!$afecha) {
				$fini = null;
				$ffin = null;
			} else {
				// En caso sea un formato invalido se tomara la fecha de hoy
				$fini = (validateDate($fini, 'd/m/Y'))
					? \Carbon\Carbon::createFromFormat('d/m/Y', $fini, 'America/Lima')->format('Y-m-d')
					: $now;
				$ffin = (validateDate($ffin, 'd/m/Y'))
					? \Carbon\Carbon::createFromFormat('d/m/Y', $ffin, 'America/Lima')->format('Y-m-d')
					: $now;
			}

			$calificacion = clearLabel($filtro_calificacion, false);
			$tipoestado = clearLabel($tipoestado, false);
			$careacliente = clearLabel($careacliente, false);

			$inspecciones = $this->mcons_insp->buscarInspecciones(
				$ccia,
				$area,
				$cservicio,
				$ccliente,
				$cclienteprov,
				$cclientemaquila,
				$careacliente,
				$establecimientoMaqui,
				$dirEstablecimientoMaqui,
				$nroInforme,
				$tipoestado,
				$fini,
				$ffin,
				$peligro,
				$sevalprod,
				$calificacion
			);
		}
		echo json_encode(['items' => $inspecciones]);
	}

	/**
	 * Impresión del PDF de la inspección tecnica
	 */
	public function pdf()
	{
		try {
			$codigo = $this->input->get('codigo');
			$fecha = $this->input->get('fecha');
			$html2pdf = $this->_pdf($codigo, $fecha);
//			$dompdf->stream("ficha_tenica.pdf", array("Attachment" => 0));
			$html2pdf->output('file.pdf', 'I');
		} catch (Exception $ex) {
			show_error($ex->getMessage(), 500, 'Error al realizar la carga de PDF');
		}
	}

	/**
	 * Recurso para obtener el PDF
	 * @param string $codigo
	 * @param string $pdf
	 */
	private function _pdf($codigo, $fecha)
	{
		$data = [
			'@CAUDI' => $codigo, // '00000303',
			'@FSERV' => $fecha, // '2020-10-09',
		];
		$caratula = $this->mcons_insp->pdfCaratula($data);
		if (empty($caratula)) {
			throw new Exception('El control de proveedor no pudo ser encontrado.');
		}
		$parrafo1Pt1 = $this->mcons_insp->pdfParrafo1Parte1($data);
		$parrafo1Pt2 = $this->mcons_insp->pdfParrafo1Parte2($data);
		$cuadro1 = $this->mcons_insp->pdfCuadro1($data);
		$parrafo2 = $this->mcons_insp->pdfParrafo2($data);
		$grafico1 = $this->mcons_insp->pdfGrafico1($data);
		$cuadro2 = $this->mcons_insp->pdfCuadro2($data);
		$grafico2 = $this->mcons_insp->pdfGrafico2($data);
		$imgGrafico1 = '';
		$unicaInspeccion = true;
		if (!empty($grafico1)) {
			$arrayLabel = [];
			$arrayData = [];
			$arrayColor = [];
			if (!empty($grafico1->TF1)) {
				$arrayLabel[] = date('d/m/Y', strtotime($grafico1->TF1)) . " " . $grafico1->CERT1;
				$arrayData[] = $grafico1->TR1;
				$arrayColor[] = getColorRgba($grafico1->TR1);
			} else {
				$arrayLabel[] = '';
				$arrayData[] = -1; // $grafico1->TR2;
				$arrayColor[] = "'rgba(255,255,255,1)'";
			}
			if (!empty($grafico1->TF2)) {
				$unicaInspeccion = false;
				$arrayLabel[] = date('d/m/Y', strtotime($grafico1->TF2)) . " " . $grafico1->CERT2;
				$arrayData[] = $grafico1->TR2;
				$arrayColor[] = getColorRgba($grafico1->TR2);
			} else {
				$arrayLabel[] = '';
				$arrayData[] = -1; // $grafico1->TR2;
				$arrayColor[] = "'rgba(255,255,255,1)'";
			}
			if (!empty($grafico1->TF3)) {
				$unicaInspeccion = false;
				$arrayLabel[] = date('d/m/Y', strtotime($grafico1->TF3)) . " " . $grafico1->CERT3;
				$arrayData[] = $grafico1->TR3;
				$arrayColor[] = getColorRgba($grafico1->TR3);
			} else {
				$arrayLabel[] = '';
				$arrayData[] = -1; // $grafico1->TR2;
				$arrayColor[] = "'rgba(255,255,255,1)'";
			}
			if (!empty($grafico1->TF4)) {
				$unicaInspeccion = false;
				$arrayLabel[] = date('d/m/Y', strtotime($grafico1->TF4)) . " " . $grafico1->CERT4;
				$arrayData[] = $grafico1->TR4;
				$arrayColor[] = getColorRgba($grafico1->TR4);
			} else {
				$arrayLabel[] = '';
				$arrayData[] = -1; // $grafico1->TR2;
				$arrayColor[] = "'rgba(255,255,255,1)'";
			}
			if (!empty($grafico1->TF5)) {
				$unicaInspeccion = false;
				$arrayLabel[] = date('d/m/Y', strtotime($grafico1->TF5)) . " " . $grafico1->CERT5;
				$arrayData[] = $grafico1->TR5;
				$arrayColor[] = getColorRgba($grafico1->TR5);
			} else {
				$arrayLabel[] = '';
				$arrayData[] = -1; // $grafico1->TR2;
				$arrayColor[] = "'rgba(255,255,255,1)'";
			}
			$imgGrafico1 = getLinkFormChartBar(
				$arrayLabel,
				$arrayData,
				$arrayColor
			);
		}
		$parrafoPrimeraInsp = '';
		if ($unicaInspeccion) {
			$parrafoPrimeraInsp = $this->mcons_insp->pdfPrimeraInsp($data);
		}
		$imgGrafico2 = '';
		if (!empty($grafico2)) {
			$lenRows = count($grafico2);
			$totalRows = ($lenRows / 2);
			$labels = [];
			$maxValue = 0;
			foreach ($grafico2 as $key => $value) {
				if (!in_array($value->dnumerador, $labels)) {
					$labels[] = $value->dnumerador;
				}
				if ($value->mayor_val > $maxValue) {
					$maxValue = $value->mayor_val;
				}
			}
			$maxValue += 30; // Se aumenta 10 para poder mostrar el texto hacia arriba
			$dataMaximo = implode(',', getValueGraphic2($grafico2, 'maximo'));
			$dataObtenido = implode(',', getValueGraphic2($grafico2, 'obtenido'));
			$backgroundColorMaximo = "'rgba(255,0,0,1)'";
			$backgroundColorObtenido = "'rgba(0,128,0,1)'";
			if ($lenRows == 4) {
				$backgroundColorMaximo = "'rgba(255,0,0,1)','rgba(255,0,0,1)'";
				$backgroundColorObtenido = "'rgba(0,128,0,1)','rgba(0,128,0,1)'";
			}
			if ($lenRows > 4) {
				$backgroundColorMaximo = "'rgba(255,0,0,1)','rgba(255,0,0,1)','rgba(255,0,0,1)'";
				$backgroundColorObtenido = "'rgba(0,128,0,1)','rgba(0,128,0,1)','rgba(0,128,0,1)'";
			}
			$datasets = "{label:'Maximo',data:[{$dataMaximo}],backgroundColor:[{$backgroundColorMaximo}]}";
			$datasets .= ",{label:'Obtenido',data:[{$dataObtenido}],backgroundColor:[{$backgroundColorObtenido}]}";
			$imgGrafico2 = getLinkFormChartBar2($labels, $datasets, $maxValue);
		}
		$cuadro3 = $this->mcons_insp->pdfCuadro3($data);
		$responsables = $this->mcons_insp->responsables($codigo, $fecha);
		$cuadro4 = $this->mcons_insp->pdfCuadro4($data);
		$criterioInspeccion = $this->mcons_insp->pdfCriteriosInspeccion($data);
		$criterioEvaluacion = $this->mcons_insp->pdfCriteriosEvaluacion($data);
		$conclucionesGenerales = $this->mcons_insp->pdfConclucionesGeneral($data);
		$conclucionesEspecificas = $this->mcons_insp->pdfConclucionesEspecificas($data);
		$conclucionesEspecificasConformidades = null;
		$conclucionesEspecificasObservaciones = null;
		$conclucionesEspecificasNoConformidades = null;
		$conclucionesEspecificasQuejas = null;
		$conclucionesEspecificasComentarioAdicional = null;
		if (!empty($conclucionesEspecificas)) {
			foreach ($conclucionesEspecificas as $key => $value) {
				switch ($value->zcinfoadicional) {
					case '105': // Conformidades
						$conclucionesEspecificasConformidades = $conclucionesEspecificas[$key];
						break;
					case '106': // Analisis y quejas
						$conclucionesEspecificasQuejas = $conclucionesEspecificas[$key];
						break;
					case '107': // No conformidades
						$conclucionesEspecificasNoConformidades = $conclucionesEspecificas[$key];
						break;
					case '108': // Observaciones
						$conclucionesEspecificasObservaciones = $conclucionesEspecificas[$key];
						break;
					case 'L08': // Comentario adicionales
						$conclucionesEspecificasComentarioAdicional = $conclucionesEspecificas[$key];
						break;
				}
			}
		}

		$planAccionParrafo1 = $this->mcons_insp->pdfPlanAccionParrafo1($data);
		$planAccionParrafo2 = $this->mcons_insp->pdfPlanAccionParrafo2($data);
		$planAccionParrafo3 = $this->mcons_insp->pdfPlanAccionParrafo3($data);
		$escalaValoracion = $this->mcons_insp->pdfEscalaValoracion($data);
		$parrafoExcluyentes = $this->mcons_insp->pdfParrafoRequisitos($data);
		$requisitosExcluyentes = $this->mcons_insp->pdfRequisitoExcluyentes($data);
		$inspector = $this->mcons_insp->getDatosInspector($data);
		$peligros = $this->mcons_insp->pdfPeligros($data);
		$excluyentes = $this->mcons_insp->pdfExcluyentes($data);
		$rutafirma = null;
		if (!empty($inspector)) {
			$rutafirma = 'http://plataforma.grupofs.com:82/intranet/FTPfileserver/Imagenes/internos/firmas/' . $inspector->rutafirma;
			if (file_exists($rutafirma)) {
				$rutafirma = base64ResourceConvert($rutafirma);
			}
		}
		// Contenedor de la ficha tecnica
		$content = $this->load->view('at/ctrlprov/vcons_insp_pdf', [
			'caratula' => $caratula,
			'parrafo1Pt1' => $parrafo1Pt1,
			'parrafo1Pt2' => $parrafo1Pt2,
			'parrafoPrimeraInsp' => $parrafoPrimeraInsp,
			'cuadro1' => $cuadro1,
			'parrafo2' => $parrafo2,
			'imgGrafico1' => $imgGrafico1,
			'cuadro2' => $cuadro2,
			'imgGrafico2' => $imgGrafico2,
			'cuadro3' => $cuadro3,
			'responsables' => $responsables,
			'cuadro4' => $cuadro4,
			'criterioInspeccion' => $criterioInspeccion,
			'criterioEvaluacion' => $criterioEvaluacion,
			'conclucionesGenerales' => $conclucionesGenerales,
			'conclucionesEspecificasConformidades' => $conclucionesEspecificasConformidades,
			'conclucionesEspecificasObservaciones' => $conclucionesEspecificasObservaciones,
			'conclucionesEspecificasNoConformidades' => $conclucionesEspecificasNoConformidades,
			'conclucionesEspecificasQuejas' => $conclucionesEspecificasQuejas,
			'conclucionesEspecificasComentarioAdicional' => $conclucionesEspecificasComentarioAdicional,
			'planAccionParrafo1' => $planAccionParrafo1,
			'planAccionParrafo2' => $planAccionParrafo2,
			'planAccionParrafo3' => $planAccionParrafo3,
			'escalaValoracion' => $escalaValoracion,
			'parrafoExcluyentes' => $parrafoExcluyentes,
			'requisitosExcluyentes' => $requisitosExcluyentes,
			'peligros' => $peligros,
			'rutafirma' => $rutafirma,
			'inspector' => $inspector,
			'excluyentes' => $excluyentes,
			'unicaInspeccion' => $unicaInspeccion,
		], TRUE);
		$html2pdf = new \Spipu\Html2Pdf\Html2Pdf();
//		$html2pdf->setDefaultFont('arial');
		$html2pdf->writeHTML($content);
		return $html2pdf;
	}

	/**
	 * Cierre de la inspeccion tecnica
	 */
	public function close_download()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {
			$codigo = $this->input->post('codigo');
			$fecha = $this->input->post('fecha');
			$inspecciondet = $this->mcons_insp->buscarInspccion($codigo, $fecha);
			if (empty($inspecciondet)) {
				throw new Exception('La inspección no pudo ser encontrada.');
			}
			$inspeccioncab = $this->mcons_insp->buscarInspccionCab($codigo);
			if (empty($inspeccioncab)) {
				throw new Exception('La inspección cabecera no pudo ser encontrada.');
			}

			$caratula = $this->mcons_insp->pdfCaratula([
				'@CAUDI' => $codigo, // '00000303',
				'@FSERV' => $fecha, // '2020-10-09',
			]);

			$informe = $caratula->dinforme;
			$dinforme = explode('/', $informe);

			// codigo | fecha .pdf
			$fileName = date('dmY', strtotime($fecha)) . '-' . $dinforme[0] . '.pdf';
			$dompdf = $this->_pdf($codigo, $fecha);

			// Separado por Archivos / cia|area|servicio / ccliente / caudi
			$rutaCarpeta = '1' . self::AREA . self::SERVICIO . '/' . $inspeccioncab->CCLIENTE . '/' . $inspeccioncab->CAUDITORIAINSPECCION;
			if (!file_exists(RUTA_ARCHIVOS . $rutaCarpeta)) {
				mkdir(RUTA_ARCHIVOS . $rutaCarpeta, '0777', true);
			}
			$filePath = $rutaCarpeta . '/' . $fileName;
			$dompdf->output(RUTA_ARCHIVOS . $filePath, 'F');

			$validate = $this->db->update('PDAUDITORIAINSPECCION',
				[
					'DUBICACIONFILESERVERPDF' => $filePath,
				],
				[
					'CAUDITORIAINSPECCION' => $inspecciondet->CAUDITORIAINSPECCION,
					'FSERVICIO' => $inspecciondet->FSERVICIO,
				]
			);
			if (!$validate) {
				throw new Exception('La inspección no pudo ser actualizada correctamente.');
			}
			$inspecciondet->DUBICACIONFILESERVERPDF = $filePath;

			$this->result['status'] = 200;
			$this->result['message'] = 'Descarga del archivo correctamente.';
			$this->result['data'] = $inspecciondet;
		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

	/**
	 * Realiza la descarga del archivo
	 */
	public function download()
	{
		$fileName = $this->input->get('filename');
		$this->load->helper('download');
		$pathFile = RUTA_ARCHIVOS . $fileName;
		if (!file_exists($pathFile)) {
			show_404();
		}
		force_download($pathFile, null, false);
	}

	/**
	 * Busqueda de las acciones correctiva
	 */
	public function get_accion_correctiva()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		$codigo = $this->input->post('codigo');
		$fecha = $this->input->post('fecha');
		$items = $this->mcons_insp->getAccionesCorrectiva([
			'@CAUDI' => $codigo,
			'@FSERV' => $fecha,
		]);
		echo json_encode(['items' => $items]);
	}

	/**
	 * Busqueda de las acciones correctiva
	 */
	public function get_proveedor()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		$caudi = $this->input->post('caudi');
		$id_proveedor = $this->input->post('proveedor');
		$proveedor = $this->mcons_insp->getProveedor([
			'@AS_CCLIENTE' => $id_proveedor,
		]);
		$establecimiento = $this->mcons_insp->getProveedorEstablecimiento([
			'@AS_CAUDITORIAINSPECCION' => $caudi,
		]);
		$linea = $this->mcons_insp->getProveedorLinea([
			'@AS_CCLIENTE' => $id_proveedor,
			'@AS_CCIA' => 1,
		]);
		$contactos = $this->mcons_insp->getProveedorContactos([
			'@AS_CCLIENTE' => $id_proveedor,
			'@AS_CESTABLECIMIENTO' => (!empty($establecimiento)) ? $establecimiento->cestablecimientoprov : '',
		]);
		echo json_encode([
			'proveedor' => $proveedor,
			'establecimiento' => (!empty($establecimiento)) ? $establecimiento->ESTABLECIMIENTO : '',
			'linea' => $linea,
			'contactos' => $contactos,
		]);
	}

	/**
	 * Metodo para exportar los resultados
	 */
	public function exportar()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {

			$ccia = $this->input->post('ccia');
			$afecha = (int)$this->input->post('afecha');
			$fini = $this->input->post('fini');
			$ffin = $this->input->post('ffin');
			$ccliente = $this->input->post('filtro_cliente');
			$cclienteprov = $this->input->post('filtro_proveedor');
			$cclientemaquila = $this->input->post('filtro_maquilador');
			$careacliente = $this->input->post('filtro_cliente_area');
			$tipoestado = $this->input->post('filtro_tipo_estado');
			$peligro = $this->input->post('filtro_peligro');
			$sevalprod = $this->input->post('sevalprod');
			$filtro_calificacion = $this->input->post('filtro_calificacion');
			$establecimientoMaqui = $this->input->post('filtro_establecimiento_maqui');
			$dirEstablecimientoMaqui = $this->input->post('filtro_dir_establecimiento_maqui');
			$nroInforme = $this->input->post('filtro_nro_informe');

			$inspecciones = '';
			if (empty($ccliente)) {
				throw new Exception('Debe elegir un cliente');
			}
			$area = self::AREA;
			$cservicio = self::SERVICIO;
			$cclienteprov = (empty($cclienteprov)) ? '%' : $cclienteprov;
			$cclientemaquila = (empty($cclientemaquila)) ? '%' : $cclientemaquila;
			$careacliente = (empty($careacliente)) ? [] : $careacliente;
			$establecimientoMaqui = is_null($establecimientoMaqui) ? '%' : "%{$establecimientoMaqui}%";
			$dirEstablecimientoMaqui = is_null($dirEstablecimientoMaqui) ? '%' : "%{$dirEstablecimientoMaqui}%";
			$nroInforme = is_null($nroInforme) ? '%' : "%{$nroInforme}%";
			$tipoestado = (empty($tipoestado)) ? [] : $tipoestado;
			$peligro = empty($peligro) ? '%' : $peligro;
			$sevalprod = is_null($sevalprod) ? '%' : $sevalprod;
			$filtro_calificacion = is_null($filtro_calificacion) ? [] : $filtro_calificacion;

			// Se verifica si filtrara con fecha o no
			$now = \Carbon\Carbon::now('America/Lima')->format('Y-m-d');
			if (!$afecha) {
				$fini = null;
				$ffin = null;
			} else {
				// En caso sea un formato invalido se tomara la fecha de hoy
				$fini = (validateDate($fini, 'd/m/Y'))
					? \Carbon\Carbon::createFromFormat('d/m/Y', $fini, 'America/Lima')->format('Y-m-d')
					: $now;
				$ffin = (validateDate($ffin, 'd/m/Y'))
					? \Carbon\Carbon::createFromFormat('d/m/Y', $ffin, 'America/Lima')->format('Y-m-d')
					: $now;
			}

			$calificacion = clearLabel($filtro_calificacion, false);
			$tipoestado = clearLabel($tipoestado, false);
			$careacliente = clearLabel($careacliente, false);

			$inspecciones = $this->mcons_insp->buscarInspecciones(
				$ccia,
				$area,
				$cservicio,
				$ccliente,
				$cclienteprov,
				$cclientemaquila,
				$careacliente,
				$establecimientoMaqui,
				$dirEstablecimientoMaqui,
				$nroInforme,
				$tipoestado,
				$fini,
				$ffin,
				$peligro,
				$sevalprod,
				$calificacion
			);

			$spreadsheet = new Spreadsheet();
			$sheet = $spreadsheet->getActiveSheet();
			$sheet->setTitle('Inspección de Proveedores');

			$spreadsheet->getDefaultStyle()
				->getFont()
				->setName('Arial')
				->setSize(10);

			$sheet->setCellValue('A1', 'INSPECCIÓN A PROVEEDORES')
				->mergeCells('A1:AD1');

			$sheet->setCellValue('A2', 'Código')
				->setCellValue('B2', 'Fecha Inspección')
				->setCellValue('C2', 'Fecha Creación')
				->setCellValue('D2', 'Cliente')
				->setCellValue('E2', 'RUC')
				->setCellValue('F2', 'Proveedor')
				->setCellValue('G2', 'Establecimiento del proveedor o Maquilador')
				->setCellValue('H2', 'Dirección Establecimiento o Maquilador')
				->setCellValue('I2', 'Área Cliente')
				->setCellValue('J2', 'Línea')
				->setCellValue('K2', 'Línea Inspeccionada')
				->setCellValue('L2', 'Marca Inspeccionada')
				->setCellValue('M2', 'Tipo Estado Servicio')
				->setCellValue('N2', 'Comentario')
				->setCellValue('O2', 'Nro de Informe')
				->setCellValue('P2', 'Calificación')
				->setCellValue('Q2', 'Acción Correctiva')
				->setCellValue('R2', 'Consultor')
				->setCellValue('S2', 'Certificación')
				->setCellValue('T2', 'Estado Certificación')
				->setCellValue('U2', 'Licencia de Funcionamiento')
				->setCellValue('V2', 'Empresa Inspectora')
				->setCellValue('W2', 'Eval. Prod.')
				->setCellValue('X2', 'Es Peligro')
				->setCellValue('Y2', 'C. Checklist')
				->setCellValue('Z2', 'CheckList')
				->setCellValue('AA2', 'Entidad')
				->setCellValue('AB2', 'Sistema')
				->setCellValue('AC2', 'Rubro')
				->setCellValue('AD2', 'Costo');

			$irow = 3;
			foreach ($inspecciones as $key => $inspeccion) {

				$sheet->setCellValue('A' . $irow, $inspeccion->CODIGO);
				$sheet->setCellValue('B' . $irow, date('d/m/Y', strtotime($inspeccion->FECHAINSPECCION)));
				$sheet->setCellValue('C' . $irow, date('d/m/Y', strtotime($inspeccion->FCREACION)));
				$sheet->setCellValue('D' . $irow, $inspeccion->CLIENTE);
				$sheet->setCellValue('E' . $irow, $inspeccion->nruc);
				$sheet->setCellValue('F' . $irow, $inspeccion->PROVEEDOR);
				$DIRECCIONPROV = (!empty($inspeccion->DIRECCIONPROV)) ? $inspeccion->DIRECCIONPROV : '';
				$ESTABLECIMIENTOPROV = (!empty($inspeccion->ESTABLECIMIENTOPROV)) ? $inspeccion->ESTABLECIMIENTOPROV : '';
				$MAQUILADOR = (!empty($inspeccion->MAQUILADOR)) ? $inspeccion->MAQUILADOR : '';
				$establecimiento = (!empty($DIRECCIONPROV)) ? $ESTABLECIMIENTOPROV : $ESTABLECIMIENTOPROV . ' - ' . $MAQUILADOR;
				$sheet->setCellValue('G' . $irow, $establecimiento);

				$DIRECCIONMAQUILA = (!empty($inspeccion->DIRECCIONMAQUILA)) ? $inspeccion->DIRECCIONMAQUILA : '';
				$UBIGEOMAQUILA = (!empty($inspeccion->UBIGEOMAQUILA)) ? $inspeccion->UBIGEOMAQUILA : '';
				$DIRECCIONPROV = (!empty($inspeccion->DIRECCIONPROV)) ? $inspeccion->DIRECCIONPROV : '';
				$UBIGEOPROV = (!empty($inspeccion->UBIGEOPROV)) ? $inspeccion->UBIGEOPROV : '';
				$direccion = (!empty($DIRECCIONPROV)) ? $DIRECCIONPROV . ' ' . $UBIGEOPROV : $DIRECCIONMAQUILA . ' ' . $UBIGEOMAQUILA;
				$sheet->setCellValue('H' . $irow, $direccion);
				$sheet->setCellValue('I' . $irow, $inspeccion->AREACLIENTE);
				$sheet->setCellValue('J' . $irow, $inspeccion->LINEA);
				$sheet->setCellValue('K' . $irow, $inspeccion->DALCANCEINFORME);
				$sheet->setCellValue('L' . $irow, $inspeccion->DMARCAINFORME);
				$sheet->setCellValue('M' . $irow, $inspeccion->TIPOESTADOSERVICIO);
				$sheet->setCellValue('N' . $irow, $inspeccion->COMENTARIO);
				$sheet->setCellValue('O' . $irow, $inspeccion->dinformefinal);
				$sheet->setCellValue('P' . $irow, $inspeccion->RESULTADOCHECKLIST . ' ' . $inspeccion->RESULTADOTEXTO);
				$sheet->setCellValue('Q' . $irow, $inspeccion->ACCIONCORRECTIVA);
				$sheet->setCellValue('R' . $irow, $inspeccion->CONSULTOR);
				$CERTIFICADORA = (!empty($inspeccion->CERTIFICADORA)) ? $inspeccion->CERTIFICADORA : '';
				$CERTIFICACION = (!empty($inspeccion->CERTIFICACION)) ? $inspeccion->CERTIFICACION : '';
				$cetificacion = '';
				if (!empty($CERTIFICADORA) || !empty($CERTIFICACION)) {
					$cetificacion = $CERTIFICADORA . ' ' . $CERTIFICACION;
				} else {
					$tipoEstado = trim($inspeccion->TIPOESTADOSERVICIO);
					$EMPRESAINSPECTORA = trim($inspeccion->EMPRESAINSPECTORA);
					if ($tipoEstado === 'convalidado' && (
							$EMPRESAINSPECTORA === 'senasa' ||
							$EMPRESAINSPECTORA === 'digesa' ||
							$EMPRESAINSPECTORA === 'sanipes'
						)) {
						$cetificacion = $inspeccion->EMPRESAINSPECTORACONV;
					}
				}
				$sheet->setCellValue('S' . $irow, $cetificacion);
				$SCERTIFICACION = (!empty($inspeccion->SCERTIFICACION)) ? $inspeccion->SCERTIFICACION : '';
				$EMPRESAINSPECTORA = trim($inspeccion->EMPRESAINSPECTORA);
				$tipoEstado = trim($inspeccion->TIPOESTADOSERVICIO);
				if ($tipoEstado === 'convalidado' && (
						$EMPRESAINSPECTORA === 'senasa' ||
						$EMPRESAINSPECTORA === 'digemid' ||
						$EMPRESAINSPECTORA === 'digesa' ||
						$EMPRESAINSPECTORA === 'sanipes'
					)) {
					$textScetificacion = 'SI TIENE';
				} else {
					$textScetificacion = $SCERTIFICACION;
				}
				$sheet->setCellValue('T' . $irow, $textScetificacion);
				$LICENCIADEFUNCIONAMIENTO = (!empty($inspeccion->LICENCIADEFUNCIONAMIENTO)) ? $inspeccion->LICENCIADEFUNCIONAMIENTO : '';
				$ESTADOLICENCIADEFUNCIONAMIENTO = (!empty($inspeccion->ESTADOLICENCIADEFUNCIONAMIENTO)) ? $inspeccion->ESTADOLICENCIADEFUNCIONAMIENTO : '';
				$sheet->setCellValue('U' . $irow, $ESTADOLICENCIADEFUNCIONAMIENTO . ' ' . $LICENCIADEFUNCIONAMIENTO);
				$sheet->setCellValue('V' . $irow, $inspeccion->EMPRESAINSPECTORA);
				$sheet->setCellValue('W' . $irow, $inspeccion->SEVALPROD);
				$sheet->setCellValue('X' . $irow, $inspeccion->espeligro);
				$sheet->setCellValue('Y' . $irow, $inspeccion->CCHECKLIST);
				$sheet->setCellValue('Z' . $irow, $inspeccion->CHECKLIST);
				$sheet->setCellValue('AA' . $irow, $inspeccion->CERTIFICADORA);
				$sheet->setCellValue('AB' . $irow, $inspeccion->DSISTEMA);
				$sheet->setCellValue('AC' . $irow, $inspeccion->DSUBNORMA);
				$sheet->setCellValue('AD' . $irow, $inspeccion->COSTOINSP);
				$irow++;
			}

			$titulo = [
				'font' => [
					'name' => 'Arial',
					'size' => 12,
					'color' => array('rgb' => 'FFFFFF'),
					'bold' => true,
				],
				'fill' => [
					'fillType' => Fill::FILL_SOLID,
					'startColor' => [
						'rgb' => '29B037'
					]
				],
				'borders' => [
					'allBorders' => [
						'borderStyle' => Border::BORDER_THIN,
						'color' => [
							'rgb' => '000000'
						]
					]
				],
				'alignment' => [
					'horizontal' => Alignment::HORIZONTAL_CENTER,
					'vertical' => Alignment::VERTICAL_CENTER,
					'wrapText' => true,
				],
			];
			$cabecera = [
				'font' => [
					'name' => 'Arial',
					'size' => 10,
					'color' => array('rgb' => 'FFFFFF'),
					'bold' => true,
				],
				'fill' => [
					'fillType' => Fill::FILL_SOLID,
					'startColor' => [
						'rgb' => '29B037'
					]
				],
				'borders' => [
					'allBorders' => [
						'borderStyle' => Border::BORDER_THIN,
						'color' => [
							'rgb' => '000000'
						]
					]
				],
				'alignment' => [
					'horizontal' => Alignment::HORIZONTAL_CENTER,
					'vertical' => Alignment::VERTICAL_CENTER,
					'wrapText' => true,
				],
			];
			$sheet->getStyle('A1:AD1')->applyFromArray($titulo);
			$sheet->getStyle('A2:AD2')->applyFromArray($cabecera);

			foreach (range('A', 'AB') as $column) {
				$sheet->getColumnDimension($column)->setAutoSize(true);
			}

			$writer = new Xlsx($spreadsheet);
			$filename = 'inspeccion_proveedor_' . date('Ymd') . '.xlsx';
			$path = RUTA_ARCHIVOS . '../../temp/' . $filename;
			$writer->save($path);

			$this->result['status'] = 200;
			$this->result['message'] = 'Se realizo la exportación correctamente.';
			$this->result['data'] = $filename;

		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

	public function buscar_contactos()
	{
		$ccia = $this->input->post('ccia');
		$afecha = (int)$this->input->post('afecha');
		$fini = $this->input->post('fini');
		$ffin = $this->input->post('ffin');
		$ccliente = $this->input->post('filtro_cliente');
		$cclienteprov = $this->input->post('filtro_proveedor');
		$cclientemaquila = $this->input->post('filtro_maquilador');
		$careacliente = $this->input->post('filtro_cliente_area');
		$tipoestado = $this->input->post('filtro_tipo_estado');
		$peligro = $this->input->post('filtro_peligro');
		$sevalprod = $this->input->post('sevalprod');
		$filtro_calificacion = $this->input->post('filtro_calificacion');
		$establecimientoMaqui = $this->input->post('filtro_establecimiento_maqui');
		$dirEstablecimientoMaqui = $this->input->post('filtro_dir_establecimiento_maqui');
		$nroInforme = $this->input->post('filtro_nro_informe');

		$inspecciones = '';
		if (!empty($ccliente)) {
			$area = self::AREA;
			$cservicio = self::SERVICIO;
			$cclienteprov = (empty($cclienteprov)) ? '%' : $cclienteprov;
			$cclientemaquila = (empty($cclientemaquila)) ? '%' : $cclientemaquila;
			$careacliente = (empty($careacliente)) ? [] : $careacliente;
			$establecimientoMaqui = is_null($establecimientoMaqui) ? '%' : "%{$establecimientoMaqui}%";
			$dirEstablecimientoMaqui = is_null($dirEstablecimientoMaqui) ? '%' : "%{$dirEstablecimientoMaqui}%";
			$nroInforme = is_null($nroInforme) ? '%' : "%{$nroInforme}%";
			$tipoestado = (empty($tipoestado)) ? [] : $tipoestado;
			$peligro = empty($peligro) ? '%' : $peligro;
			$sevalprod = is_null($sevalprod) ? '%' : $sevalprod;
			$filtro_calificacion = is_null($filtro_calificacion) ? [] : $filtro_calificacion;

			// Se verifica si filtrara con fecha o no
			$now = \Carbon\Carbon::now('America/Lima')->format('Y-m-d');
			if (!$afecha) {
				$fini = null;
				$ffin = null;
			} else {
				// En caso sea un formato invalido se tomara la fecha de hoy
				$fini = (validateDate($fini, 'd/m/Y'))
					? \Carbon\Carbon::createFromFormat('d/m/Y', $fini, 'America/Lima')->format('Y-m-d')
					: $now;
				$ffin = (validateDate($ffin, 'd/m/Y'))
					? \Carbon\Carbon::createFromFormat('d/m/Y', $ffin, 'America/Lima')->format('Y-m-d')
					: $now;
			}

			$calificacion = clearLabel($filtro_calificacion, false);
			$tipoestado = clearLabel($tipoestado, false);
			$careacliente = clearLabel($careacliente, false);

			$inspecciones = $this->mcons_insp->buscarContactos(
				$ccia,
				$area,
				$cservicio,
				$ccliente,
				$cclienteprov,
				$cclientemaquila,
				$careacliente,
				$establecimientoMaqui,
				$dirEstablecimientoMaqui,
				$nroInforme,
				$tipoestado,
				$fini,
				$ffin,
				$peligro,
				$sevalprod,
				$calificacion
			);
		}
		echo json_encode(['items' => $inspecciones]);
	}

	public function descargar_contactos()
	{
		try {
			$ccia = $this->input->post('ccia');
			$afecha = (int)$this->input->post('afecha');
			$fini = $this->input->post('fini');
			$ffin = $this->input->post('ffin');
			$ccliente = $this->input->post('filtro_cliente');
			$cclienteprov = $this->input->post('filtro_proveedor');
			$cclientemaquila = $this->input->post('filtro_maquilador');
			$careacliente = $this->input->post('filtro_cliente_area');
			$tipoestado = $this->input->post('filtro_tipo_estado');
			$peligro = $this->input->post('filtro_peligro');
			$sevalprod = $this->input->post('sevalprod');
			$filtro_calificacion = $this->input->post('filtro_calificacion');
			$establecimientoMaqui = $this->input->post('filtro_establecimiento_maqui');
			$dirEstablecimientoMaqui = $this->input->post('filtro_dir_establecimiento_maqui');
			$nroInforme = $this->input->post('filtro_nro_informe');

			$area = self::AREA;
			$cservicio = self::SERVICIO;
			$cclienteprov = (empty($cclienteprov)) ? '%' : $cclienteprov;
			$cclientemaquila = (empty($cclientemaquila)) ? '%' : $cclientemaquila;
			$careacliente = (empty($careacliente)) ? [] : $careacliente;
			$establecimientoMaqui = is_null($establecimientoMaqui) ? '%' : "%{$establecimientoMaqui}%";
			$dirEstablecimientoMaqui = is_null($dirEstablecimientoMaqui) ? '%' : "%{$dirEstablecimientoMaqui}%";
			$nroInforme = is_null($nroInforme) ? '%' : "%{$nroInforme}%";
			$tipoestado = (empty($tipoestado)) ? [] : $tipoestado;
			$peligro = empty($peligro) ? '%' : $peligro;
			$sevalprod = is_null($sevalprod) ? '%' : $sevalprod;
			$filtro_calificacion = is_null($filtro_calificacion) ? [] : $filtro_calificacion;

			// Se verifica si filtrara con fecha o no
			$now = \Carbon\Carbon::now('America/Lima')->format('Y-m-d');
			if (!$afecha) {
				$fini = null;
				$ffin = null;
			} else {
				// En caso sea un formato invalido se tomara la fecha de hoy
				$fini = (validateDate($fini, 'd/m/Y'))
					? \Carbon\Carbon::createFromFormat('d/m/Y', $fini, 'America/Lima')->format('Y-m-d')
					: $now;
				$ffin = (validateDate($ffin, 'd/m/Y'))
					? \Carbon\Carbon::createFromFormat('d/m/Y', $ffin, 'America/Lima')->format('Y-m-d')
					: $now;
			}

			$calificacion = clearLabel($filtro_calificacion, false);
			$tipoestado = clearLabel($tipoestado, false);
			$careacliente = clearLabel($careacliente, false);

			$inspecciones = $this->mcons_insp->buscarContactos(
				$ccia,
				$area,
				$cservicio,
				$ccliente,
				$cclienteprov,
				$cclientemaquila,
				$careacliente,
				$establecimientoMaqui,
				$dirEstablecimientoMaqui,
				$nroInforme,
				$tipoestado,
				$fini,
				$ffin,
				$peligro,
				$sevalprod,
				$calificacion
			);

			$spreadsheet = new Spreadsheet();
			$sheet = $spreadsheet->getActiveSheet();
			$sheet->setTitle('Inspección de Proveedores');

			$spreadsheet->getDefaultStyle()
				->getFont()
				->setName('Arial')
				->setSize(10);

			$sheet->setCellValue('A1', 'CONTACTOS')
				->mergeCells('A1:AD1');

			$sheet->setCellValue('A2', 'Fecha Servicio')
				->setCellValue('B2', 'Nro. Informe')
				->setCellValue('C2', 'Cliente')
				->setCellValue('D2', 'RUC')
				->setCellValue('E2', 'Proveedor')
				->setCellValue('F2', 'Establecimiento Prov')
				->setCellValue('G2', 'Direccion Prov')
				->setCellValue('H2', 'Ubigeo Prov')
				->setCellValue('I2', 'Maquilador')
				->setCellValue('J2', 'Establecimiento Maquila')
				->setCellValue('K2', 'Direccion Maquila')
				->setCellValue('L2', 'Ubigeo Maquila')
				->setCellValue('M2', 'Area Cliente')
				->setCellValue('N2', 'Linea')
				->setCellValue('O2', 'Resultado checklist')
				->setCellValue('P2', 'Resultado')
				->setCellValue('Q2', 'Tamaño Empresa Prov')
				->setCellValue('R2', 'Tipo EStado Servicio')
				->setCellValue('S2', 'Consultor')
				->setCellValue('T2', 'AACC')
				->setCellValue('U2', 'Certificadora')
				->setCellValue('V2', 'Marca')
				->setCellValue('W2', 'Certificacion')
				->setCellValue('X2', 'Costo')
				->setCellValue('Y2', 'Nombre Contacto')
				->setCellValue('Z2', 'Cargo Contacto')
				->setCellValue('AA2', 'Correo Contacto')
				->setCellValue('AB2', 'Telefono Contacto')
				->setCellValue('AC2', 'EVAL. PROD.')
				->setCellValue('AD2', 'PELIGRO');
			$irow = 3;
			foreach ($inspecciones as $key => $inspeccion) {
				$fecha = date('d/m/Y', strtotime($inspeccion->FSERVICIO));
				$sheet->setCellValue('A' . $irow, $fecha);
				$sheet->setCellValue('B' . $irow, $inspeccion->DINFORMEFINAL);
				$sheet->setCellValue('C' . $irow, $inspeccion->CLIENTE);
				$sheet->setCellValue('D' . $irow, $inspeccion->NRUC);
				$sheet->setCellValue('E' . $irow, $inspeccion->PROVEEDOR);
				$sheet->setCellValue('F' . $irow, $inspeccion->ESTABLECIMIENTOPROV);
				$sheet->setCellValue('G' . $irow, $inspeccion->DIRECCIONPROV);
				$sheet->setCellValue('H' . $irow, $inspeccion->UBIGEOPROV);
				$sheet->setCellValue('I' . $irow, $inspeccion->MAQUILADOR);
				$sheet->setCellValue('J' . $irow, $inspeccion->ESTABLECIMIENTOMAQUILA);
				$sheet->setCellValue('K' . $irow, $inspeccion->DIRECCIONMAQUILA);
				$sheet->setCellValue('L' . $irow, $inspeccion->UBIGEOMAQUILA);
				$sheet->setCellValue('M' . $irow, $inspeccion->AREACLIENTE);
				$sheet->setCellValue('N' . $irow, $inspeccion->LINEA);
				$sheet->setCellValue('O' . $irow, $inspeccion->RESULTADOCHECKLIST);
				$sheet->setCellValue('P' . $irow, $inspeccion->RESULTADOTEXTO);
				$sheet->setCellValue('Q' . $irow, $inspeccion->TAMANOEMPRESAPROV);
				$sheet->setCellValue('R' . $irow, $inspeccion->TIPOESTADOSERVICIO);
				$sheet->setCellValue('S' . $irow, $inspeccion->CONSULTOR);
				$sheet->setCellValue('T' . $irow, $inspeccion->ACCIONCORRECTIVA);
				$sheet->setCellValue('U' . $irow, $inspeccion->CERTIFICADORA);
				$sheet->setCellValue('V' . $irow, '');
				$sheet->setCellValue('W' . $irow, $inspeccion->CERTIFICACION);
				$sheet->setCellValue('X' . $irow, $inspeccion->COSTO);
				$sheet->setCellValue('Y' . $irow, $inspeccion->NOMBCONTACTO);
				$sheet->setCellValue('Z' . $irow, $inspeccion->CARCONTACTO);
				$sheet->setCellValue('AA' . $irow, $inspeccion->MAILCONTACTO);
				$sheet->setCellValue('AB' . $irow, $inspeccion->FONOCONTACTO);
				$sheet->setCellValue('AC' . $irow, $inspeccion->SEVALPROD);
				$sheet->setCellValue('AD' . $irow, $inspeccion->ESPELIGRO);
				$irow++;
			}

			$titulo = [
				'font' => [
					'name' => 'Arial',
					'size' => 12,
					'color' => array('rgb' => 'FFFFFF'),
					'bold' => true,
				],
				'fill' => [
					'fillType' => Fill::FILL_SOLID,
					'startColor' => [
						'rgb' => '29B037'
					]
				],
				'borders' => [
					'allBorders' => [
						'borderStyle' => Border::BORDER_THIN,
						'color' => [
							'rgb' => '000000'
						]
					]
				],
				'alignment' => [
					'horizontal' => Alignment::HORIZONTAL_CENTER,
					'vertical' => Alignment::VERTICAL_CENTER,
					'wrapText' => true,
				],
			];
			$cabecera = [
				'font' => [
					'name' => 'Arial',
					'size' => 10,
					'color' => array('rgb' => 'FFFFFF'),
					'bold' => true,
				],
				'fill' => [
					'fillType' => Fill::FILL_SOLID,
					'startColor' => [
						'rgb' => '29B037'
					]
				],
				'borders' => [
					'allBorders' => [
						'borderStyle' => Border::BORDER_THIN,
						'color' => [
							'rgb' => '000000'
						]
					]
				],
				'alignment' => [
					'horizontal' => Alignment::HORIZONTAL_CENTER,
					'vertical' => Alignment::VERTICAL_CENTER,
					'wrapText' => true,
				],
			];
			$sheet->getStyle('A1:AD1')->applyFromArray($titulo);
			$sheet->getStyle('A2:AD2')->applyFromArray($cabecera);

			foreach (range('A', 'AB') as $column) {
				$sheet->getColumnDimension($column)->setAutoSize(true);
			}

			$writer = new Xlsx($spreadsheet);
			$filename = 'inspeccion_contactos_' . date('Ymd') . '.xlsx';
			$path = RUTA_ARCHIVOS . '../../temp/' . $filename;
			$writer->save($path);

			$this->result['status'] = 200;
			$this->result['message'] = 'Se realizo la exportación correctamente.';
			$this->result['data'] = $filename;
		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

}
