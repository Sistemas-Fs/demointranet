<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Style\Conditional;
use PhpOffice\PhpSpreadsheet\Writer\IWriter;
use PhpOffice\PhpSpreadsheet\IOFactory;

class Ccartasprov extends FS_Controller
{
	function __construct()
	{
		parent:: __construct();
		$this->load->model('at/ctrlprov/mcartasprov');
		$this->load->model('mglobales');
		$this->load->library('encryption');
		$this->load->helper(array('form', 'url', 'download', 'html', 'file'));
		$this->load->library('form_validation');
	}

	public function getclientes()
	{ // recupera los clientes
		$resultado = $this->mcartasprov->getclientes();
		echo json_encode($resultado);
	}

	public function getbuscarcartas()
	{ // recupera los cartas a proveedores
		$sevalprod = $this->input->post('sevalprod');
		$sevalprod = empty($sevalprod) ? '%' : $sevalprod;
		$parametros = array(
			'@anio' => $this->input->post('anio'),
			'@mes' => $this->input->post('mes'),
			'@ccliente' => $this->input->post('ccliente'),
			'@sevalprod' => $sevalprod
		);
		$resultado = $this->mcartasprov->getbuscarcartas($parametros);
		echo json_encode($resultado);
	}

	public function genpdfcartasprov($anio, $mes, $ccliente)
	{ // recupera los cartas a proveedores
		$this->load->library('pdfgenerator');
		$filename = "Document_name";

		$html = "<html>
				<head>
					<style>
						@page {
							margin: 2cm 1.5cm;
							margin-top: 0.5cm;
							margin-bottom: 0.5cm;
						}
						.teacherPage {
								page: teacher;
								page-break-after: always;
						}
						table tbody tr td{
							font-family: 'Helvetica';
							font-size: 9pt;
						}
						.cuerpo {
							text-align: justify;
						}
					</style>
				</head>
				<body>";
		$parametros = array(
			'@CCLIENTE' => $ccliente,
			'@ANIO' => $anio,
			'@MES' => $mes,
			'@MESINSP' => $mes,
		);
		$resultado = $this->mcartasprov->getcartasprov($parametros);
		if ($resultado) {
			foreach ($resultado as $row) {
				$contacto = $row->contacto;
				$cargo_contacto = $row->cargo_contacto;
				$empresa_contacto = $row->empresa_contacto;
				$cliente_drazonsocial = $row->cliente_drazonsocial;
				$fecha = $row->fecha;
				$dir_contacto = $row->dir_contacto;
				$departamento = $row->departamento;
				$drazonsocial = $row->drazonsocial;
				$establecimiento = $row->establecimiento;
				$mes = $row->mes;
				$costo = $row->costo;
				$firmante = $row->firmante;
				$cargo_firmante = $row->cargo_firmante;


				$html .= '<div class="teacherPage">				
					<page>					
					<table >
					<tbody>
						<tr>
							<td ALIGN="right">
								<img src="' . public_url_ftp() . 'Imagenes/formatos/1/01/02/cartas/00005/logo_tottus.jpg" width="160" height="70" />
							</td>
						</tr>
						<tr>
							<td>
								<span>' . $fecha . ' </span>
								<p>&nbsp;</p>
								<span style="margin-top: 1mm;">Señor(a) </span><br>
								<span style="margin-top: 1mm;"><b>' . $contacto . '</b></span><br>
								<span style="margin-top: 1mm;">' . $cargo_contacto . '</span><br>
								<span style="margin-top: 1mm;"><b>' . $empresa_contacto . '</b> </span><br>
								<span style="margin-top: 1mm;">' . $dir_contacto . ' </span><br>
								<span style="margin-top: 1mm;"><u><b>' . $departamento . ' </b></u></span><br><br>
								<span style="margin-top: 6mm;">Estimado(a) Señor(a) </span><br><br>
							</td>
						</tr>
						<tr>
							<td class="cuerpo">
								<p>Me dirijo a usted para saludarle y en el marco del Programa de vigilancia y control de proveedores de ' . $cliente_drazonsocial . ', comunicarle que el establecimiento <b>' . $establecimiento . '</b> será inspeccionado en el transcurso del mes de <b>' . $mes . ' - ' . $anio . '</b>, por un inspector calificado de la empresa ' . $drazonsocial . '.</p>
								<p>En tal sentido, se le recuerda que el objeto de la inspección  es verificar el nivel de cumplimiento de los Principios Generales de Higiene y del Sistema HACCP (en caso aplique) en el proceso de sus productos, a fin de garantizar la calidad  sanitaria de los productos que comercializan en ' . $cliente_drazonsocial . ', siendo el costo de la inspección de  <b>S/.  ' . $costo . '</b>  más IGV, monto que será retenido de la facturación mensual. </p>
								<p>Adicionalmente, los proveedores ubicados a nivel nacional (excepto Lima Metropolitana) deberán asumir los gastos de pasajes, hospedaje y alimentación que incurra el inspector, los mismos que serán compartidos  entre los proveedores programados en la misma ciudad. Cabe señalar que este gasto puede ser retenido de la facturación mensual de Tottus o financiados directamente, previa coordinación con ' . $drazonsocial . '.</p>
								<p>Asi mismo, se hace mencion que de darse una inspección trunca por no permitir el ingreso del inspector al establecimiento tendrá un costo según se muestra en la siguiente tabla:</p>
							</td>
						</tr>
						<tr>
							<td>
								<table style="width: 100%; border-collapse: collapse; margin-top: 2mm; ">
									<thead>
										<tr>
											<th style="border: solid 1px #000; width: 110px; border-collapse: collapse; text-align:center;">UBICACIONES</th>
											<th style="border: solid 1px #000; width: 490px; border-collapse: collapse; text-align:center;">COSTOS</th>
										</tr>
									</thead>
									<tbody >
										<tr>
											<td style="border: solid 1px #000; width: 110px; border-collapse: collapse; text-align:center;">LIMA</td>
											<td style="border: solid 1px #000; width: 490px; border-collapse: collapse; text-align:justify; padding: 0mm 2mm 0mm 2mm;">El costo por inspecciones canceladas con menos de dos dias utiles a la inspección,incluidas las cancelas cuando el inspector ha llegado al establecimiento, será del 30% en base al costo de la inspección mas IGV .</td>
										</tr>
										<tr>
											<td style="border: solid 1px #000; width: 110px; border-collapse: collapse; text-align:center;">A NIVEL NACIONAL (excepto Lima Metropolitana)</td>
											<td style="border: solid 1px #000; width: 490px; border-collapse: collapse; text-align:justify; padding: 0mm 2mm 0mm 2mm;">El costo por inspecciones canceladas con menos de 15 dias útiles de anticipación, incluidas las canceladas cuando el inspector ha llegado al establecimiento, será del 30% en base al costo de la inspección más IGV, según tipo de establecimiento, así como los gastos de viáticos que incurra el inspector.</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>         
						<tr>
							<td class="cuerpo">
								<p>Asimismo, se hace de su conocimiento que la inspección a su establecimiento es un requisito para continuar como proveedor autorizado; por lo que, agradeceremos su cooperación a fin que la inspección se efectúe de la mejor manera y obtenga el mayor provecho de la asistencia técnica brindada por el inspector.</p>
								<p>Sin otro particular, agradeciendo de antemano la atención prestada, quedo de usted.</p>
								<p>Atentamente,</p><br>
							</td>
						</tr>         
						<tr>
						<td>
						<div>
							<table>
							<tr>
								<td style="text-align:center;">
									<img src="' . public_url_ftp() . 'Imagenes/formatos/1/01/02/cartas/00005/firma_PFano.jpg" width="180" height="120">
								</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td style="text-align:center;">------------------------------------------</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td style="text-align:center;">' . $firmante . '</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td style="text-align:center;">' . $cargo_firmante . '</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td style="text-align:center;">' . $cliente_drazonsocial . '</td>
								<td>&nbsp;</td>
							</tr>
							</table>
						</div>
						</td>
						</tr>  
					</tbody>
					</table>        
				 	</page>
				</div>';
			}
		}
		$html .= '</body></html>';
		//echo $html;
		$this->pdfgenerator->generate($html, $filename);
	}

	public function genpdfcartasprov_elegidos($fanio, $fmes)
	{
		$this->load->library('pdfgenerator');
		$filename = "Document_name";
		$html = "<html>
				<head>
					<style>
						@page {
							margin: 2cm 1.5cm;
							margin-top: 0.5cm;
							margin-bottom: 0.5cm;
						}
						.teacherPage {
								page: teacher;
								page-break-after: always;
						}
						table tbody tr td{
							font-family: 'Helvetica';
							font-size: 9pt;
						}
						.cuerpo {
							text-align: justify;
						}
					</style>
				</head>
				<body>";
		$sfecha = \Carbon\Carbon::now('America/Lima')->format('Y-m-d');
		$meses = ['', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'];
		$fmes = $meses[$fmes];
		if (isset($_POST) && !empty($_POST)) {
			foreach ($_POST as $key => $value) {
				// Se verifica que este activo
				if (!empty($value) && $value == "on") {
					$code = substr($key, 6);
					$inspeccion = substr($code, 0, 8);
					$fservicio = substr($code, 8);
					if (!empty($inspeccion)) {
						$row = (new mcartasprov())->getcartaprov($inspeccion, $fservicio);
						if (!empty($row)) {
							// Se actualiza el envio
							$this->db->update('PDAUDITORIAINSPECCION',
								['senviocarta' => 'S', 'fenviocarta' => $sfecha],
								[
									'CAUDITORIAINSPECCION' => $inspeccion,
									'FSERVICIO' => substr($fservicio, 0, 4) . '-' . substr($fservicio, 4, 2) . '-' . substr($fservicio, 6, 8)
								]
							);
							$contacto = $row->contacto;
							$cargo_contacto = $row->cargo_contacto;
							$empresa_contacto = $row->empresa_contacto;
							$cliente_drazonsocial = $row->cliente_drazonsocial;
							$fecha = $row->fecha;
							$dir_contacto = $row->dir_contacto;
							$departamento = $row->departamento;
							$drazonsocial = $row->drazonsocial;
							$establecimiento = $row->establecimiento;
							$mes = $row->mes;
							$costo = $row->costo;
							$firmante = $row->firmante;
							$cargo_firmante = $row->cargo_firmante;
							$anio = $row->anio;

							$html .= '<div class="teacherPage">				
								<page>					
								<table >
								<tbody>
									<tr>
										<td ALIGN="right">
											<img src="' . public_url_ftp() . 'Imagenes/formatos/1/01/02/cartas/00005/logo_tottus.jpg" width="160" height="70" />
										</td>
									</tr>
									<tr>
										<td>
											<span>' . $fecha . ' </span>
											<p>&nbsp;</p>
											<span style="margin-top: 1mm;">Señor(a) </span><br>
											<span style="margin-top: 1mm;"><b>' . $contacto . '</b></span><br>
											<span style="margin-top: 1mm;">' . $cargo_contacto . '</span><br>
											<span style="margin-top: 1mm;"><b>' . $empresa_contacto . '</b> </span><br>
											<span style="margin-top: 1mm;">' . $dir_contacto . ' </span><br>
											<span style="margin-top: 1mm;"><u><b>' . $departamento . ' </b></u></span><br><br>
											<span style="margin-top: 6mm;">Estimado(a) Señor(a) </span><br><br>
										</td>
									</tr>
									<tr>
										<td class="cuerpo">
											<p>Me dirijo a usted para saludarle y en el marco del Programa de vigilancia y control de proveedores de ' . $cliente_drazonsocial . ', comunicarle que el establecimiento <b>' . $establecimiento . '</b> será inspeccionado en el transcurso del mes de <b>' . $fmes . ' - ' . $fanio . '</b>, por un inspector calificado de la empresa ' . $drazonsocial . '.</p>
											<p>En tal sentido, se le recuerda que el objeto de la inspección  es verificar el nivel de cumplimiento de los Principios Generales de Higiene y del Sistema HACCP (en caso aplique) en el proceso de sus productos, a fin de garantizar la calidad  sanitaria de los productos que comercializan en ' . $cliente_drazonsocial . ', siendo el costo de la inspección de  <b>S/.  ' . $costo . '</b>  más IGV, monto que será retenido de la facturación mensual. </p>
											<p>Adicionalmente, los proveedores ubicados a nivel nacional (excepto Lima Metropolitana) deberán asumir los gastos de pasajes, hospedaje y alimentación que incurra el inspector, los mismos que serán compartidos  entre los proveedores programados en la misma ciudad. Cabe señalar que este gasto puede ser retenido de la facturación mensual de Tottus o financiados directamente, previa coordinación con ' . $drazonsocial . '.</p>
											<p>Asi mismo, se hace mencion que de darse una inspección trunca por no permitir el ingreso del inspector al establecimiento tendrá un costo según se muestra en la siguiente tabla:</p>
										</td>
									</tr>
									<tr>
										<td>
											<table style="width: 100%; border-collapse: collapse; margin-top: 2mm; ">
												<thead>
													<tr>
														<th style="border: solid 1px #000; width: 110px; border-collapse: collapse; text-align:center;">UBICACIONES</th>
														<th style="border: solid 1px #000; width: 490px; border-collapse: collapse; text-align:center;">COSTOS</th>
													</tr>
												</thead>
												<tbody >
													<tr>
														<td style="border: solid 1px #000; width: 110px; border-collapse: collapse; text-align:center;">LIMA</td>
														<td style="border: solid 1px #000; width: 490px; border-collapse: collapse; text-align:justify; padding: 0mm 2mm 0mm 2mm;">El costo por inspecciones canceladas con menos de dos dias utiles a la inspección,incluidas las cancelas cuando el inspector ha llegado al establecimiento, será del 30% en base al costo de la inspección mas IGV .</td>
													</tr>
													<tr>
														<td style="border: solid 1px #000; width: 110px; border-collapse: collapse; text-align:center;">A NIVEL NACIONAL (excepto Lima Metropolitana)</td>
														<td style="border: solid 1px #000; width: 490px; border-collapse: collapse; text-align:justify; padding: 0mm 2mm 0mm 2mm;">El costo por inspecciones canceladas con menos de 15 dias útiles de anticipación, incluidas las canceladas cuando el inspector ha llegado al establecimiento, será del 30% en base al costo de la inspección más IGV, según tipo de establecimiento, así como los gastos de viáticos que incurra el inspector.</td>
													</tr>
												</tbody>
											</table>
										</td>
									</tr>         
									<tr>
										<td class="cuerpo">
											<p>Asimismo, se hace de su conocimiento que la inspección a su establecimiento es un requisito para continuar como proveedor autorizado; por lo que, agradeceremos su cooperación a fin que la inspección se efectúe de la mejor manera y obtenga el mayor provecho de la asistencia técnica brindada por el inspector.</p>
											<p>Sin otro particular, agradeciendo de antemano la atención prestada, quedo de usted.</p>
											<p>Atentamente,</p><br>
										</td>
									</tr>         
									<tr>
									<td>
									<div>
										<table>
										<tr>
											<td style="text-align:center;">
												<img src="' . public_url_ftp() . 'Imagenes/formatos/1/01/02/cartas/00005/firma_PFano.jpg" width="180" height="120">
											</td>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td style="text-align:center;">------------------------------------------</td>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td style="text-align:center;">' . $firmante . '</td>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td style="text-align:center;">' . $cargo_firmante . '</td>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td style="text-align:center;">' . $cliente_drazonsocial . '</td>
											<td>&nbsp;</td>
										</tr>
										</table>
									</div>
									</td>
									</tr>  
								</tbody>
								</table>        
								</page>
							</div>';
						}
					}
				}
			}
		}
		$html .= '</body></html>';
		//echo $html;
		$this->pdfgenerator->generate($html, $filename);
	}

	public function buscar_contactos()
	{
		$sevalprod = $this->input->post('sevalprod');
		$sevalprod = empty($sevalprod) ? '%' : $sevalprod;
		$parametros = array(
			'@anio' => $this->input->post('anio'),
			'@mes' => $this->input->post('mes'),
			'@ccliente' => $this->input->post('ccliente'),
			'@sevalprod' => $sevalprod
		);
		$resultado = $this->mcartasprov->buscarcontactos($parametros);
		echo json_encode($resultado);
	}

	public function descargar_contactos()
	{
		try {
			$sevalprod = $this->input->post('sevalprod');
			$sevalprod = empty($sevalprod) ? '%' : $sevalprod;
			$parametros = array(
				'@anio' => $this->input->post('anio'),
				'@mes' => $this->input->post('mes'),
				'@ccliente' => $this->input->post('ccliente'),
				'@sevalprod' => $sevalprod
			);

			$resultado = $this->mcartasprov->buscarcontactos($parametros);

			$spreadsheet = new Spreadsheet();
			$sheet = $spreadsheet->getActiveSheet();
			$sheet->setTitle('Cartas Proveedores');

			$spreadsheet->getDefaultStyle()
				->getFont()
				->setName('Arial')
				->setSize(10);

			$sheet->setCellValue('A1', 'CONTACTOS')
				->mergeCells('A1:T1');

			$sheet->setCellValue('A2', 'Interno')
				->setCellValue('B2', 'Fecha')
				->setCellValue('C2', 'f. Creacion')
				->setCellValue('D2', 'RUC')
				->setCellValue('E2', 'Proveedor')
				->setCellValue('F2', 'Establecimiento/Maquilador')
				->setCellValue('G2', 'Linea Proceso')
				->setCellValue('H2', 'Checklist')
				->setCellValue('I2', 'Checklist')
				->setCellValue('J2', 'Enviado')
				->setCellValue('K2', 'Ubigeo')
				->setCellValue('L2', 'Tamaño de Empresa')
				->setCellValue('M2', 'Eval. Prod.')
				->setCellValue('N2', 'Costo')
				->setCellValue('O2', 'Area')
				->setCellValue('P2', 'Jerarquia')
				->setCellValue('Q2', 'Nombre Contacto')
				->setCellValue('R2', 'Cargo Contacto')
				->setCellValue('S2', 'Correo Contacto')
				->setCellValue('T2', 'Telefono Contacto');
			$irow = 3;
			foreach ($resultado as $key => $value) {
				$sheet->setCellValue('A' . $irow, $value->cauditoriainspeccion);
				$sheet->setCellValue('B' . $irow, date('d/m/Y', strtotime($value->fservicio)));
				$sheet->setCellValue('C' . $irow, date('d/m/Y', strtotime($value->fcreacion)));
				$sheet->setCellValue('D' . $irow, $value->nruc);
				$sheet->setCellValue('E' . $irow, $value->proveedor);
				$sheet->setCellValue('F' . $irow, $value->ESTMAQ);
				$sheet->setCellValue('G' . $irow, $value->dlinea);
				$sheet->setCellValue('H' . $irow, $value->sischecklist);
				$sheet->setCellValue('I' . $irow, $value->cchecklist . ' - ' . $value->dchecklist);
				$sheet->setCellValue('J' . $irow, ($value->senviocarta == 'S') ? 'Si' : 'No');
				$sheet->setCellValue('K' . $irow, $value->dubigeo);
				$sheet->setCellValue('L' . $irow, $value->tipoempresa);
				$sheet->setCellValue('M' . $irow, ($value->sevalprod == '1') ? 'Si' : 'No');
				$sheet->setCellValue('N' . $irow, $value->costo);
				$sheet->setCellValue('O' . $irow, $value->DAREACLIENTE);
				$sheet->setCellValue('P' . $irow, $value->DJERARQUIA);
				$sheet->setCellValue('Q' . $irow, $value->NOMBCONTACTO);
				$sheet->setCellValue('R' . $irow, $value->CARCONTACTO);
				$sheet->setCellValue('S' . $irow, $value->MAILCONTACTO);
				$sheet->setCellValue('T' . $irow, $value->FONOCONTACTO);
				$irow++;
			}

			$titulo = [
				'font' => [
					'name' => 'Arial',
					'size' => 12,
					'color' => array('rgb' => 'FFFFFF'),
					'bold' => true,
				],
				'fill' => [
					'fillType' => Fill::FILL_SOLID,
					'startColor' => [
						'rgb' => '29B037'
					]
				],
				'borders' => [
					'allBorders' => [
						'borderStyle' => Border::BORDER_THIN,
						'color' => [
							'rgb' => '000000'
						]
					]
				],
				'alignment' => [
					'horizontal' => Alignment::HORIZONTAL_CENTER,
					'vertical' => Alignment::VERTICAL_CENTER,
					'wrapText' => true,
				],
			];
			$cabecera = [
				'font' => [
					'name' => 'Arial',
					'size' => 10,
					'color' => array('rgb' => 'FFFFFF'),
					'bold' => true,
				],
				'fill' => [
					'fillType' => Fill::FILL_SOLID,
					'startColor' => [
						'rgb' => '29B037'
					]
				],
				'borders' => [
					'allBorders' => [
						'borderStyle' => Border::BORDER_THIN,
						'color' => [
							'rgb' => '000000'
						]
					]
				],
				'alignment' => [
					'horizontal' => Alignment::HORIZONTAL_CENTER,
					'vertical' => Alignment::VERTICAL_CENTER,
					'wrapText' => true,
				],
			];
			$sheet->getStyle('A1:T1')->applyFromArray($titulo);
			$sheet->getStyle('A2:T2')->applyFromArray($cabecera);

			foreach (range('A', 'T') as $column) {
				$sheet->getColumnDimension($column)->setAutoSize(true);
			}

			$writer = new Xlsx($spreadsheet);
			$filename = 'cartas_contactos_' . date('Ymd') . '.xlsx';
			$path = RUTA_ARCHIVOS . '../../temp/' . $filename;
			$writer->save($path);

			$this->result['status'] = 200;
			$this->result['message'] = 'Se realizo la exportación correctamente.';
			$this->result['data'] = $filename;
		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

	public function descargar_cartas()
	{
		try {
			$sevalprod = $this->input->post('sevalprod');
			$sevalprod = empty($sevalprod) ? '%' : $sevalprod;
			$parametros = array(
				'@anio' => $this->input->post('anio'),
				'@mes' => $this->input->post('mes'),
				'@ccliente' => $this->input->post('ccliente'),
				'@sevalprod' => $sevalprod
			);

			$resultado = $this->mcartasprov->getbuscarcartas($parametros);

			$spreadsheet = new Spreadsheet();
			$sheet = $spreadsheet->getActiveSheet();
			$sheet->setTitle('Cartas Proveedores');

			$spreadsheet->getDefaultStyle()
				->getFont()
				->setName('Arial')
				->setSize(10);

			$sheet->setCellValue('A1', 'CARTAS')
				->mergeCells('A1:P1');

			$sheet->setCellValue('A2', 'Interno')
				->setCellValue('B2', 'Fecha')
				->setCellValue('C2', 'f. Creacion')
				->setCellValue('D2', 'RUC')
				->setCellValue('E2', 'Proveedor')
				->setCellValue('F2', 'Establecimiento/Maquilador')
				->setCellValue('G2', 'Linea Proceso')
				->setCellValue('H2', 'Checklist')
				->setCellValue('I2', 'Checklist')
				->setCellValue('J2', 'Enviado')
				->setCellValue('K2', 'Ubigeo')
				->setCellValue('L2', 'Tamaño de Empresa')
				->setCellValue('M2', 'Eval. Prod.')
				->setCellValue('N2', 'Costo')
				->setCellValue('O2', 'Area')
				->setCellValue('P2', 'Jerarquia');
			$irow = 3;
			foreach ($resultado as $key => $value) {
				$sheet->setCellValue('A' . $irow, $value->cauditoriainspeccion);
				$sheet->setCellValue('B' . $irow, date('d/m/Y', strtotime($value->fservicio)));
				$sheet->setCellValue('C' . $irow, date('d/m/Y', strtotime($value->fcreacion)));
				$sheet->setCellValue('D' . $irow, $value->nruc);
				$sheet->setCellValue('E' . $irow, $value->proveedor);
				$sheet->setCellValue('F' . $irow, $value->ESTMAQ);
				$sheet->setCellValue('G' . $irow, $value->dlinea);
				$sheet->setCellValue('H' . $irow, $value->sischecklist);
				$sheet->setCellValue('I' . $irow, $value->cchecklist . ' - ' . $value->dchecklist);
				$sheet->setCellValue('J' . $irow, ($value->senviocarta == 'S') ? 'Si' : 'No');
				$sheet->setCellValue('K' . $irow, $value->dubigeo);
				$sheet->setCellValue('L' . $irow, $value->tipoempresa);
				$sheet->setCellValue('M' . $irow, ($value->sevalprod == '1') ? 'Si' : 'No');
				$sheet->setCellValue('N' . $irow, $value->costo);
				$sheet->setCellValue('O' . $irow, $value->DAREACLIENTE);
				$sheet->setCellValue('P' . $irow, $value->DJERARQUIA);
				$irow++;
			}

			$titulo = [
				'font' => [
					'name' => 'Arial',
					'size' => 12,
					'color' => array('rgb' => 'FFFFFF'),
					'bold' => true,
				],
				'fill' => [
					'fillType' => Fill::FILL_SOLID,
					'startColor' => [
						'rgb' => '29B037'
					]
				],
				'borders' => [
					'allBorders' => [
						'borderStyle' => Border::BORDER_THIN,
						'color' => [
							'rgb' => '000000'
						]
					]
				],
				'alignment' => [
					'horizontal' => Alignment::HORIZONTAL_CENTER,
					'vertical' => Alignment::VERTICAL_CENTER,
					'wrapText' => true,
				],
			];
			$cabecera = [
				'font' => [
					'name' => 'Arial',
					'size' => 10,
					'color' => array('rgb' => 'FFFFFF'),
					'bold' => true,
				],
				'fill' => [
					'fillType' => Fill::FILL_SOLID,
					'startColor' => [
						'rgb' => '29B037'
					]
				],
				'borders' => [
					'allBorders' => [
						'borderStyle' => Border::BORDER_THIN,
						'color' => [
							'rgb' => '000000'
						]
					]
				],
				'alignment' => [
					'horizontal' => Alignment::HORIZONTAL_CENTER,
					'vertical' => Alignment::VERTICAL_CENTER,
					'wrapText' => true,
				],
			];
			$sheet->getStyle('A1:P1')->applyFromArray($titulo);
			$sheet->getStyle('A2:P2')->applyFromArray($cabecera);

			foreach (range('A', 'P') as $column) {
				$sheet->getColumnDimension($column)->setAutoSize(true);
			}

			$writer = new Xlsx($spreadsheet);
			$filename = 'cartas_' . date('Ymd') . '.xlsx';
			$path = RUTA_ARCHIVOS . '../../temp/' . $filename;
			$writer->save($path);

			$this->result['status'] = 200;
			$this->result['message'] = 'Se realizo la exportación correctamente.';
			$this->result['data'] = $filename;
		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

}

?>
