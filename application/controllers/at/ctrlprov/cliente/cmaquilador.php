<?php

/**
 * Class cmaquilador
 *
 * @property matmaquilador mmaquilador
 */
class cmaquilador extends FS_Controller
{

	/**
	 * cmaquilador constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('at/ctrlprov/cliente/matmaquilador', 'mmaquilador');
	}

	public function lista()
	{
		$ccliente = $this->input->post('ccliente');
		$items = $this->mmaquilador->lista($ccliente);
		echo json_encode($items);
	}

	public function lista_prov()
	{
		$ccliente = $this->input->post('ccliente');
		$items = $this->mmaquilador->listaProv($ccliente);
		echo json_encode($items);
	}

	public function guardar()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {

			$cmaquilador = $this->input->post('maqhdnIdptprovmaq');
			$ccliente = $this->input->post('maqhdnIdptcliemaq');
			$nruc = $this->input->post('maqtxtnrodoc');
			$drazonsocial = $this->input->post('maqtxtrazonsocial');
			$cpais = $this->input->post('maqcboPais');
			$dciudad = $this->input->post('maqtxtCiudad');
			$destado = $this->input->post('maqtxtEstado');
			$dzip = $this->input->post('maqtxtCodigopostal');
			$cubigeo = $this->input->post('maqhdnidubigeo');
			$ddireccioncliente = $this->input->post('maqtxtDireccion');
			$dtelefono = $this->input->post('maqtxtTelefono');
			$dfax = $this->input->post('maqtxtFax');
			$dweb = $this->input->post('maqtxtWeb');
//			$zctipotamanoempresa = 112;
			$ntrabajador = 0;
			$drepresentante = $this->input->post('maqtxtRepresentante');
			$dcargorepresentante = $this->input->post('maqtxtCargorep');
			$demailrepresentante = $this->input->post('maqtxtEmailrep');
//			$accion = $this->input->post('maqhdnAccionptclieMaq');
			$druta = $this->input->post('maqutxtlogo');
			$tipodoc = $this->input->post('maqcboTipoDoc');

			if (empty($tipodoc)) {
				throw new Exception('Debes elegir el tipo de documento.');
			}
			if (empty($nruc)) {
				throw new Exception('Debes ingresar el nro de documento.');
			}
			if (empty($drazonsocial)) {
				throw new Exception('Debes ingresar la razón social.');
			}

			$save = $this->mmaquilador->guardar(
				$cmaquilador, $ccliente, null, null, null, $cpais,
				$cubigeo, $this->session->userdata('s_cusuario'), $dcargorepresentante, $dciudad, $ddireccioncliente,
				$demailrepresentante, $destado, $dfax,
				$drazonsocial, $drepresentante, $druta, $dtelefono, $dweb,
				$dzip, null, $nruc, $ntrabajador, 'A', $tipodoc
			);

			if (!$save) {
				throw new Exception('El maquilador no pudo ser registrado correctamente.');
			}
			$this->result['status'] = 200;
			$this->result['message'] = 'Registrado correctamente.';

		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

	/**
	 * Agregar nuevo maquilador
	 */
	public function agregar()
	{
		try {
			$ccliente = $this->input->post('ccliente');
			$cproveedor = $this->input->post('cproveedor');
			$res = $this->mmaquilador->agregar($ccliente, $cproveedor, $this->session->userdata('s_cusuario'));
			if (!$res) {
				throw new Exception('Error al registrar el maquilador.');
			}
			$this->result['status'] = 200;
			$this->result['message'] = 'Guardado correctamente.';
		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

}
