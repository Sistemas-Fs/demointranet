<?php

/**
 * Class cestablecimiento
 *
 * @property matestablecimiento mestablecimiento
 */
class cestablecimiento extends FS_Controller
{

	/**
	 * cestablecimiento constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('at/ctrlprov/cliente/matestablecimiento', 'mestablecimiento');
	}

	/**
	 * Lista de clientes
	 */
	public function lista()
	{
		$ccliente = $this->input->post('ccliente');
		$resultado = $this->mestablecimiento->lista($ccliente);
		echo json_encode($resultado);
	}

	/**
	 * Guarda el establecimiento
	 */
	public function guardar()
	{
		try {

		$ccliente = $this->input->post('mhdnIdClie');
		$cestablecimiento = $this->input->post('mhdnIdEstable');
		$destablecimiento = $this->input->post('txtestableCI');
		$ddireccion = $this->input->post('txtestabledireccion');
		$dzip = '000000';
		$estado = $this->input->post('cboestableEstado');
		$telefono = $this->input->post('txtestablecelu');
		$cubigeo = $this->input->post('hdnidubigeoEstable');
		$cpais = $this->input->post('cboPaisEstable');
		$dciudad = $this->input->post('txtCiudadEstable');
		$destado = $this->input->post('txtEstadoEstable');
		$referencia = $this->input->post('txtreferenciadir');

		$nuevoCcestablecimiento = $this->mestablecimiento->guardar($ccliente, $cestablecimiento, $destablecimiento, '000',
			$ddireccion, $dzip, $this->session->userdata('s_cusuario'), $estado, $telefono, $cubigeo, $cpais,
			$dciudad, $destado, $referencia);

			$this->result['status'] = 200;
			$this->result['message'] = 'Cliente guardado correctamente.';
			$this->result['data']['ccestablecimiento'] = $nuevoCcestablecimiento;

		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

}
