<?php

/**
 * Class ccliente
 *
 * @property matclientes mcliente
 */
class ccliente extends FS_Controller
{

	/**
	 * ccliente constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('at/ctrlprov/cliente/matclientes', 'mcliente');
	}

	/**
	 * Lista de clientes
	 */
	public function lista()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		$busquedaClie = $this->input->post('cliente');
		$busquedaClie = trim($busquedaClie);
		$items = $this->mcliente->lista($busquedaClie);
		echo json_encode($items);
	}

	public function getsizeempresa()
	{
		$resultado = $this->mcliente->getsizeempresa();
		echo json_encode($resultado);
	}

	public function getgrupoempresarial()
	{
		$resultado = $this->mcliente->getgrupoempresarial();
		echo json_encode($resultado);
	}

	/**
	 * Crea o Edita un cliente
	 * @throws Exception
	 */
	public function guardar()
	{
		try {

			$ccliente = $this->input->post('hdnIdptclie');
			$txtnrodoc = $this->input->post('txtnrodoc');
			$txtrazonsocial = $this->input->post('txtrazonsocial');
			$cboPais = $this->input->post('cboPais');
			$txtCiudad = $this->input->post('txtCiudad');
			$txtEstado = $this->input->post('txtEstado');
			$dzip = '00000';
			$hdnidubigeo = $this->input->post('hdnidubigeo');
			$txtDireccion = $this->input->post('txtDireccion');
			$txtTelefono = $this->input->post('txtTelefono');
			$txtFax = $this->input->post('txtFax');
			$txtWeb = $this->input->post('txtWeb');
			$ntrabajador = 0;
			$txtRepresentante = $this->input->post('txtRepresentante');
			$txtCargorep = $this->input->post('txtCargorep');
			$txtEmailrep = $this->input->post('txtEmailrep');
			$hdnAccionptclie = $this->input->post('hdnAccionptclie');
			$utxtlogo = $this->input->post('utxtlogo');
			$cboTipoDoc = $this->input->post('cboTipoDoc');

			if (empty($cboTipoDoc)) {
				throw new Exception('Debe elegir el tipo de documento.');
			}
			if (empty($txtnrodoc)) {
				throw new Exception('Debe ingresar el nro de documento.');
			}
			if (empty($txtrazonsocial)) {
				throw new Exception('Debe ingresar razón social o nombre completo.');
			}

			$nuevoCcliente = $this->mcliente->guardar(
				$ccliente,
				null,
				null,
				null,
				$cboPais,
				$hdnidubigeo,
				$this->session->userdata('s_cusuario'),
				$txtCargorep,
				$txtCiudad,
				$txtDireccion,
				$txtEmailrep,
				$txtEstado,
				$txtFax,
				$txtrazonsocial,
				$txtRepresentante,
				null,
				$txtTelefono,
				$txtWeb,
				$dzip,
				null,
				$txtnrodoc,
				$ntrabajador,
				'A',
				$cboTipoDoc
			);

			$this->result['status'] = 200;
			$this->result['message'] = 'Cliente guardado correctamente.';
			$this->result['data']['ccliente'] = $nuevoCcliente;

		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

	/**
	 * @param $ccliente
	 */
	public function buscar($ccliente)
	{
		$data = $this->mcliente->buscar($ccliente);
		echo json_encode(['data' => $data]);
	}

}
