<?php

/**
 * Class ccontacto
 *
 * @property matcontacto mcontacto
 */
class ccontacto extends FS_Controller
{

	/**
	 * ccontacto constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('at/ctrlprov/cliente/matcontacto', 'mcontacto');
	}

	/**
	 * Lista de contactos del cliente
	 */
	public function lista()
	{
		$ccliente = $this->input->post('ccliente');
		$cestable = $this->input->post('cestable');
		$items = $this->mcontacto->lista($ccliente, $cestable);
		echo json_encode($items);
	}

	/**
	 * Guardar contacto
	 */
	public function guardar()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {

			$ccontacto = $this->input->post('mhdnIdContacto');
			$ccliente = $this->input->post('id_cliente_contacto');
			$cestablecimiento = $this->input->post('id_cliente_establecimiento');
			$dnombres = $this->input->post('txtNombresContacto');
			$dapepat = $this->input->post('txtApePaterno');
			$dapemat = $this->input->post('txtApeMaterno');
			$dcargo = $this->input->post('txtCargoEmp');
			$demail = $this->input->post('txtEmailCon');
			$dtelefono = $this->input->post('txtTelefonoCon');

			if (empty($ccliente)) {
				throw new Exception('Falta parametros del cliente.');
			}

			$this->mcontacto->guardar($ccontacto, $ccliente, $cestablecimiento, $dapepat, $dapemat, $dnombres, $dcargo, $demail, $dtelefono, $this->session->userdata('s_cusuario'));

			$this->result['status'] = 200;
			$this->result['message'] = 'Contacto guardado correctamente!';

		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

	public function listacontactoxestablecimiento()
	{
		$id_cliente = $this->input->post('cliente');
		$resultado = $this->mcontacto->lista($id_cliente);
		echo json_encode($resultado);
	}

	public function addcontactoxestablecimiento() { // Lista de consultas de contactos
		try {
			$id_contacto = $this->input->post('ccontacto');
			$id_estable = $this->input->post('cod_estable');
			$ccliente = $this->input->post('ccliente');

			$mcontacto = $this->mcontacto->buscar($id_contacto);
			if (empty($mcontacto)) {
				throw new Exception('El contacto no pudo ser encontrado.');
			}
			if (empty($id_estable)) {
				throw new Exception('Falta datos del establecimiento');
			}

			$this->mcontacto->guardar(
				0,
				$ccliente,
				$id_estable,
				$mcontacto->DAPEPAT,
				$mcontacto->DAPEMAT,
				$mcontacto->DNOMBRE,
				$mcontacto->DCARGOCONTACTO,
				$mcontacto->DMAIL,
				$mcontacto->DTELEFONO,
				$this->session->userdata('s_cusuario')
			);

		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		echo json_encode($this->result);
	}

}
