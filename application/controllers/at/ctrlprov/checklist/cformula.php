<?php

/**
 * Class cformula
 * @property mformula mformula
 */
class cformula extends FS_Controller
{

	/**
	 * cformula constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('at/ctrlprov/checklist/mformula');
	}

	/**
	 * Busqueda de formulas
	 */
	public function autocompletado()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		$busqueda = $this->input->post('busqueda');
		$items = $this->mformula->autoCompletable($busqueda);
		echo json_encode(['items' => $items]);
	}

	/**
	 * Lista de clientes de un checklist
	 */
	public function listar()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {
			$idCheckList = $this->input->post('idCheckList');
			$items = $this->mformula->lista($idCheckList);
			echo json_encode(['items' => $items]);
		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
	}

	/**
	 * Guarda la formula de un checklist
	 */
	public function guardar()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {
			$idFormula = $this->input->post('idFormula');
			$idCheckList = $this->input->post('idCheckList');
			$estado = $this->input->post('estado');
			if (empty($idFormula)) {
				throw new Exception('Debes elegir un cliente.');
			}
			if (empty($idCheckList)) {
				throw new Exception('Se debe informar el CheckList asociado.');
			}

			$validar = $this->mformula->buscar($idCheckList, $idFormula);
			if (!empty($validar)) {
				throw new Exception('La Formula ya esta asociado al checklist.');
			}

			$s_cusuario = $this->session->userdata('s_idusuario');

			$datos = $this->mformula->guardar($idCheckList, $idFormula, $estado, $s_cusuario);

			$this->result['status'] = 200;
			$this->result['data'] = $datos;

		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

	/**
	 * Cambia el estado de la formula del checklist
	 */
	public function cambiar_estado()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {
			$idFormula = $this->input->post('idFormula');
			$idCheckList = $this->input->post('idCheckList');
			$estado = $this->input->post('estado');
			if (empty($idFormula)) {
				throw new Exception('Debes elegir un cliente.');
			}
			if (empty($idCheckList)) {
				throw new Exception('Se debe informar el CheckList asociado.');
			}

			$validar = $this->mformula->buscar($idCheckList, $idFormula);
			if (empty($validar)) {
				throw new Exception('La Formula no se encuentra asociado al checklist.');
			}

			$datos = $this->mformula->actualizar($idCheckList, $idFormula, [
				'SREGISTRO' => $estado
			]);

			$this->result['status'] = 200;
			$this->result['data'] = $datos;

		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

}
