<?php

/**
 * Class creqhomologacion
 *
 * @property mreqhomologacion mreqhomologacion
 */
class creqhomologacion extends FS_Controller
{

	/**
	 * creqhomologacion constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('oi/homologaciones/mreqhomologacion');
	}

	/**
	 * Lista de areas de homologacion
	 */
	public function lista_area()
	{
		$items = $this->mreqhomologacion->listaArea();
		echo json_encode($items);
	}

	/**
	 * Lista de areas de homologacion
	 */
	public function lista_tipo()
	{
		$idArea = $this->input->post('idArea');
		$items = $this->mreqhomologacion->listaTipo($idArea);
		echo json_encode($items);
	}

	public function lista()
	{
		$idTipo = $this->input->post('idTipo');
		$idMarca = $this->input->post('idMarca');
		$items = $this->mreqhomologacion->lista($idTipo, $idMarca);
		echo json_encode($items);
	}

}
