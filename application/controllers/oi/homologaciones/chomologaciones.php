<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
define("DOMPDF_ENABLE_REMOTE", true);

ini_set('max_execution_time', 1800);
ini_set("memory_limit", "500M");

use Dompdf\Dompdf;

/**
 * Class Chomologaciones
 *
 * @property mhomologaciones mhomologaciones
 */
class Chomologaciones extends FS_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('oi/homologaciones/mhomologaciones');
		$this->load->helper(array('form', 'url', 'download', 'html', 'file'));
		$this->load->library(array('form_validation', 'session', 'encryption'));
	}

	// Vista principal
	public function index()
	{
//		$this->load->view('oi/homologaciones/vhomologaciones');
	}

	public function getClientes()
	{
		$resultado = $this->mhomologaciones->getClientes();
		echo json_encode($resultado);
	}

	public function getEstadoExp()
	{
		$resultado = $this->mhomologaciones->getEstadoExp();
		echo json_encode($resultado);
	}

	public function getArea()
	{
		$cliente = $this->input->post('cliente');

		$parametros = array(
			'@cliente' => $cliente
		);

		$resultado = $this->mhomologaciones->getArea($parametros);
		echo json_encode($resultado);
	}

	public function get_areaxcliente()
	{
		$cliente = $this->input->post('cliente');
		$resultado = $this->mhomologaciones->getAreaxCliente($cliente);
		echo json_encode($resultado);
	}

	public function getbuscarexpediente()
	{

		$cliente = $this->input->post('cliente');
		$finicio = $this->input->post('fecinicio');
		$ffin = $this->input->post('fecfin');
		$estado = $this->input->post('estado');

		$parametros = array(
			'@cliente' => $cliente,
			'@fecinicio' => $finicio,
			'@fecfin' => $ffin,
			'@estado' => $estado
		);

		$resultado = $this->mhomologaciones->getbuscarexpediente($parametros);
		echo json_encode($resultado);

	}

	public function getbuscarproductoxespediente()
	{
		$expediente = $this->input->post('expediente');

		$parametros = array(
			'@expediente' => $expediente

		);

		$resultado = $this->mhomologaciones->getbuscarproductoxespediente($parametros);
		echo json_encode($resultado);
	}

	public function getClienteDetallado()
	{
		$expediente = $this->input->post("expediente");

		$parametros = array(
			'@expediente' => $expediente
		);

		$resultado = $this->mhomologaciones->getClienteDetallado($parametros);
		echo json_encode($resultado);
	}

	public function getContactoProveedor()
	{
		$idproveedor = $this->input->post('idproveedor');

		$parametros = array(
			'@idproveedor' => $idproveedor
		);

		$resultado = $this->mhomologaciones->getContactoProveedor($idproveedor);
		echo json_encode($resultado);
	}

	public function getProveedorxCliente()
	{
		$cliente = $this->input->post('expediente');

		$parametros = array(
			'@expediente' => $cliente
		);

		$resultado = $this->mhomologaciones->getProveedorxCliente($parametros);
		echo json_encode($resultado);
	}

	public function getbuscarequisitoxproducto()
	{
		$parametros = array(
			'@expediente' => $this->input->post('expediente'),
			'@idprod' => $this->input->post('idprod')
		);

		$resultado = $this->mhomologaciones->getbuscarequisitoxproducto($parametros);

		echo json_encode($resultado);
	}

	public function getbuscarobservacionxproducto()
	{
		$parametros = array(
			'@expediente' => $this->input->post('expediente'),
			'@idprod' => $this->input->post('idprod')
		);

		$resultado = $this->mhomologaciones->getbuscarobservacionxproducto($parametros);
		echo json_encode($resultado);
	}

	public function insertarProducto()
	{
		$this->form_validation->set_rules('txtProducto', 'Producto', 'required');
		$this->form_validation->set_rules('cboTipRequisito', 'Requisito', 'required');
		$this->form_validation->set_rules('cboTipoMarca', 'Tipo Marca', 'required');
		$this->form_validation->set_rules('cboOrigenProd', 'Origen', 'required');
		$this->form_validation->set_rules('txtEnvPrimario', 'Envase', 'required');


		if ($this->form_validation->run() == TRUE) {
			$idprod = $this->input->post('idProductoEdit');

			$parametros = array(
				'@idproductoexpediente' => $this->input->post('txtIdExpediente'),
				'@nombreproducto' => strtoupper($this->input->post('txtProducto')),
				'@requisito' => $this->input->post('cboTipRequisito'),
				'@tipomarca' => $this->input->post('cboTipoMarca'),
				'@origen_prod' => $this->input->post('cboOrigenProd'),
				'@dmarca' => $this->input->post('txtMarca'),
				'@cond_almacenaje' => $this->input->post('txtCondicionAlmacen'),
				'@vida_util' => $this->input->post('txtVidautil'),
				'@env_primario' => $this->input->post('txtEnvPrimario'),
				'@env_secundario' => $this->input->post('txtEnvSecundario'),
				'@fabricante' => $this->input->post('txtFabricante'),
				'@direc_fabricante' => $this->input->post('txtDirFabricante'),
				'@almacen' => $this->input->post('txtAlmacen'),
				'@direc_almacen' => $this->input->post('txtDirecAlmacen'),
				'@userreg' => $this->input->post('txtUserCreated'),
				'@dplantaorigen' => $this->input->post('txtplantaorigen'),
				'@dpaisorigen' => $this->input->post('txtpaisorigen'),
				'@dcodigosap' => $this->input->post('txtcodigosap')

			);

			//PROBLEMAS AL REGISTRAR Y ACTUALIZAR, DESPUES DE AÑADIR EL CAMPO VIDA UTIL

			if (!empty($idprod)) {
				$parametros['@idprod'] = $idprod; //añadir una variable idprod para poider editar
				$response = $this->mhomologaciones->update($parametros);

			} else {
				$response = $this->mhomologaciones->insert($parametros);
			}

			echo json_encode($response);
		} else {
			$response = 2;
			echo json_encode($response);

		}


	}

	public function deleteProducto()
	{
		$parametros = array(
			'@expediente' => $this->input->post('expediente'),
			'@idprod' => $this->input->post('idprod')

		);

		$response = $this->mhomologaciones->delete($parametros);
		echo json_encode($response);

		/* CAMMBIA EL ESTADO DEL PRODUCTO PARA NO VISUALIZARLO, MAS NO ELIMINA */
	}

	public function insertarObsRequisito()
	{

		$parametros = array(
			'@txtExpedienteObservacion' => $this->input->post('txtExpedienteObservacion'),
			'@txtIdProdObservacion' => $this->input->post('txtIdProdObservacion'),
			'@mtxtObservacion' => $this->input->post('mtxtObservacion'),
			'@mtxtAcuerdo' => $this->input->post('mtxtAcuerdo'),
			'@FechaRecepDoc' => $this->input->post('FechaRecepDoc'),
			'@tmpRespProv' => $this->input->post('tmpRespProv'),
			'@FecPrimEval' => $this->input->post('FecPrimEval'),
			'@tmpRespFsc' => $this->input->post('tmpRespFsc'),
			'@FechaLevObs' => $this->input->post('FechaLevObs'),
			// '@txtmpPrimEval'                => $this->input->post('txtmpPrimEval'),
			'@FechaTerminos' => $this->input->post('FechaTerminos'),
			'@tmpDuracion' => $this->input->post('tmpDuracion')

		);

		$response = $this->mhomologaciones->insertarObsRequisito($parametros);

		echo json_encode($response);


	}

	public function insertarProductoProveedor()
	{
		$this->form_validation->set_rules('cboPagoCliente', 'Pago Cliente', 'required');
		$fechaCobro = $this->input->post('FechaCobro');

		if ($this->form_validation->run() == TRUE) {

			$parametros = array(
				'@txtidProduc' => $this->input->post('txtidProduc'),
				'@txtExp' => $this->input->post('txtExp'),
				'@cboEstadoProducto' => $this->input->post('cboEstadoProducto'),
				'@txtProductoTab3' => $this->input->post('txtProductoTab3'),
				'@txtMonto' => $this->input->post('txtMonto'),
				'@cboPagoCliente' => $this->input->post('cboPagoCliente'),
				'@FechaCobro' => ($fechaCobro == '') ? NULL : $fechaCobro,
			);

			$response = $this->mhomologaciones->updateProductoProveedor($parametros);


			echo json_encode($response);

		} else {
			$response = 2;
			echo json_encode($response);
		}

	}

	public function getTipoRequisito()
	{
		$tipoProd = $this->input->post('tipoProd');

		$parametros = array(
			'@tipoProd' => $tipoProd
		);

		$resultado = $this->mhomologaciones->getTipoRequisito($parametros);
		echo json_encode($resultado);
	}

	public function insertarRequisitoProducto()
	{
		$this->form_validation->set_rules('txtExpRequisito', 'idExpediente', 'required');
		$this->form_validation->set_rules('txtIdProducto', 'idProducto', 'required');
		$this->form_validation->set_rules('cboRequisito', 'Requisito', 'required');
		$this->form_validation->set_rules('cboConformidad', 'Conformidad', 'required');
		$this->form_validation->set_rules('cboTipo', 'Tipo', 'required');

		if ($this->form_validation->run() == TRUE) {
			$accion = $this->input->post('txtAccion');

			$idCliente = $this->input->post('cboCliente');
			$ExpedienteReq = $this->input->post('txtExpRequisito');
			$idProdcutoFile = $this->input->post('txtIdProducto');
			$name_file = $this->input->post('txtFileRequisito');

			$varnull = '';
			$fechareg = $this->input->post('FecRegistroRequ');
			//RUTA DONDE SE GUARDAN LOS FICHEROS
			if (!empty($name_file)) {
				$ruta = '20106/' . $idCliente . '/' . $ExpedienteReq . '/' . $idProdcutoFile . '/' . $name_file;
			} else {
				$ruta = '';
			}


			$parametros = array(
				'@exp' => $this->input->post('txtExpRequisito'),
				'@idprod' => $this->input->post('txtIdProducto'),
				'@requisito' => $this->input->post('cboRequisito'),
				'@conformidad' => $this->input->post('cboConformidad'),
				'@nota' => $this->input->post('txtNotaReq'),
				'@fecha' => ($fechareg == $varnull) ? NULL : $fechareg,
				'@descripcion' => $this->input->post('txtDescripcionRequisito'),
				'@archivo' => $ruta,
				'@usereg' => $this->session->userdata('s_usuario')
			);

			if ($accion == 'I') {
				$response = $this->mhomologaciones->insertarRequisitoProducto($parametros);
			} else if ($accion == 'U') {
				$response = $this->mhomologaciones->updateRequisitoProducto($parametros);
			}

			$requisito = $this->db->select('*')
				->from('MREQUISITOEVALPRODUCTO')
				->where('CREQUISITOPDTO', $this->input->post('cboRequisito'))
				->get()
				->row();
			$requisitoPadre = $requisito->NCPADRE;

			$parametros = array(
				'@expediente' => $this->input->post('txtExpRequisito'),
				'@idprod' => $this->input->post('txtIdProducto')
			);
			$resultado = $this->mhomologaciones->getbuscarequisitoxproducto($parametros);
			if (!empty($resultado) && $this->input->post('cboConformidad') != 'P') {
				foreach ($resultado as $key => $value) {
					if ($value->NCPADRE == $requisitoPadre) {
						$this->db->update('PREQUISITOPDTOEVALUAR', ['SCUMPLIMIENTO' => $this->input->post('cboConformidad')], [
							'CEVALUACIONPRODUCTO' => $this->input->post('txtExpRequisito'),
							'CPRODUCTOFSEVALUAR' => $this->input->post('txtIdProducto'),
							'CREQUISITOPDTO' => $value->IDREQU,
						]);
					}
				}
			}

			echo json_encode($response);

		} else {
			$response = 2;
			echo json_encode($response);
		}

	}

	public function deleteRequisitoProd()
	{
		$parametros = array(
			'@exp' => $this->input->post('expediente'),
			'@idprod' => $this->input->post('idprod'),
			'@requisito' => $this->input->post('requisito')
		);

		$response = $this->mhomologaciones->deleteRequisitoProd($parametros);
		echo json_encode($response);
	}

	// Subir Archivos
	public function subirArchivo()
	{
		$idCliente = $this->input->post('cboCliente');
		$ExpedienteReq = $this->input->post('txtExpRequisito');
		$idProdcutoFile = $this->input->post('txtIdProducto');

		//RUTA DONDE SE GUARDAN LOS FICHEROS
		$ruta = 'FTPfileserver/Archivos/20106/' . $idCliente . '/' . $ExpedienteReq . '/' . $idProdcutoFile . '/';

		if (!file_exists($ruta)) {
			mkdir($ruta, 0777, true);
		}

		$config['upload_path'] = $ruta;
		$config['allowed_types'] = 'pdf|xlsx|docx|doc|xls|rar|zip';
		$config['max_size'] = '90048';
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		//if (!($this->upload->do_upload('mtxtArchivopropu') || $this->upload->do_upload('mtxtArchivo'))) {

		if (!($this->upload->do_upload('fileRequisito'))) {
			//si al subirse hay algun error
			$data['uploadError'] = $this->upload->display_errors();
			$error = '';
			return $error;

		} else {

			$data = $this->upload->data();

			$path = array(
				$prueba1 = $data['file_name'],
				$prueba2 = $config['upload_path'],
			);
			echo json_encode($path);
		}
	}

	/***
	 * Lista los grupos empresariales
	 */
	public function lista_grupo_empresarial()
	{
		$items = $this->mhomologaciones->listaGrupoEmpresarial();
		echo json_encode(['items' => $items]);
	}

	/***
	 * Lista los grupos empresariales
	 */
	public function lista_clientes()
	{
		$cgrupoemp = $this->input->get('cgrupoemp');
		$items = $this->mhomologaciones->listaClientes($cgrupoemp);
		echo json_encode(['items' => $items]);
	}

	public function lista_proveedores()
	{
		$ccliente = $this->input->get('cliente');
		$items = $this->mhomologaciones->listaProveedores($ccliente);
		echo json_encode(['items' => $items]);
	}

	public function guardar_evaluacion()
	{
		try {

			$evaluacion_id = $this->input->post('evaluacion_id');
			$cboCliente01 = $this->input->post('cboCliente01');
			$cboArea = $this->input->post('cboArea');
//			$cboTipoequipoReg05 = $this->input->post('cboTipoequipoReg05');
			$cboEstadoProveedor = $this->input->post('cboEstadoProveedor');
			$cboProveedor = $this->input->post('cboProveedor');
			$cboContacto1 = $this->input->post('cboContacto1');
			$cboContacto2 = $this->input->post('cboContacto2');
			$proveedor_nuevo = $this->input->post('proveedor_nuevo');
			$proveedor_nuevo = (!empty($proveedor_nuevo)) ? 'S' : 'N';

			if (empty($cboCliente01)) {
				throw new Exception('Debes elegir un cliente');
			}
//			if (empty($cboArea)) {
//				throw new Exception('Debes elegir una área');
//			}
			if (empty($cboProveedor)) {
				throw new Exception('Debes elegir un proveedor');
			}

			$cboContacto1 = (empty($cboContacto1) || $cboContacto1 == '%') ? null : $cboContacto1;
			$cboContacto2 = (empty($cboContacto2) || $cboContacto2 == '%') ? null : $cboContacto2;
			$cboArea = (empty($cboArea) || $cboArea == '%') ? null : $cboContacto2;

			$result = $this->mhomologaciones->guardarEval(
				$evaluacion_id,
				$cboCliente01,
				$cboArea,
				$cboEstadoProveedor,
				$cboProveedor,
				$cboContacto1,
				$cboContacto2,
				$proveedor_nuevo,
				$this->session->userdata('s_cusuario')
			);

			if (empty($result['resp'])) {
				throw new Exception('La evaluación no pudo ser guardado correctamente.');
			}

			$tipo = (empty($evaluacion_id)) ? 'creado' : 'actualizado';

			$this->result['status'] = 200;
			$this->result['message'] = "El expediente fue {$tipo} correctamente.";
			$this->result['data'] = $result['id'];

		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

	public function buscar_expediente()
	{
		try {

			$expediente = $this->input->post('expediente');
			$evalprod = $this->mhomologaciones->buscarExpdiente($expediente);
			if (empty($evalprod)) {
				throw new Exception('No pudo ser encontrado la evaluación');
			}

			$area = $this->db->select('*')->from('MAREACLIENTE')->where('CAREACLIENTE', $evalprod->CAREACLIENTE)->get()->row();
			$contactoprov1 = $this->db->select('*')->from('MCONTACTO')->where('CCONTACTO', $evalprod->CCONTACTOPROVEEDOR1)->get()->row();
			$contactoprov2 = $this->db->select('*')->from('MCONTACTO')->where('CCONTACTO', $evalprod->CCONTACTOPROVEEDOR2)->get()->row();

			$this->result['status'] = 200;
			$this->result['message'] = '';
			$this->result['data'] = [
				'evalprod' => $evalprod,
				'area' => $area,
				'contactoprov1' => $contactoprov1,
				'contactoprov2' => $contactoprov2,
			];

		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

	public function descargar_producto($id)
	{
		$evalprod = $this->mhomologaciones->buscarExpdiente($id);
		if (empty($evalprod)) {
			throw new Exception('No pudo ser encontrado la evaluación');
		}
		$productos = $this->mhomologaciones->pdfBuscarProductos($id);
		if (empty($productos)) {
			show_error('No cuenta con productos la inspeccion elegida');
		}

		$arrayRutas = [];

		foreach ($productos as $key => $producto) {
			$cproducto = $producto->CPRODUCTOFSEVALUAR;

			// Producto
			$producto = $this->mhomologaciones->pdfBuscarProdEval($id, $cproducto);
			// Requisitos
			$requisitos = $this->mhomologaciones->pdfBuscarProdRequisitos($id, $cproducto);
			// Requisitos observaciones
			$observaciones = $this->mhomologaciones->pdfBuscarProdRequisitoObs($id, $cproducto);

			$content = $this->load->view('oi/homologaciones/vresultado_eval_pdf', [
				'evalprod' => $evalprod,
				'producto' => $producto,
				'requisitos' => $requisitos,
				'observaciones' => $observaciones,
			], TRUE);
			$html2pdf = new \Spipu\Html2Pdf\Html2Pdf();
			$html2pdf->writeHTML($content);
//		$html2pdf->setDefaultFont('arial');
			$fileName = str_replace([' ', '@'], '_', $evalprod->DPROVEEDORCLIENTE) . '-'
				. str_replace([' ', '@'], '_', $producto->PRODUCTO) . '-' . date('Ymd', strtotime($evalprod->FINICIOSERVICIO)) . '.pdf';
			$ruta = RUTA_ARCHIVOS . 'OI/TEMP/' . $fileName;
			$arrayRutas[] = [
				'nombre' => $fileName,
				'ruta' => $ruta
			];
			$html2pdf->Output($ruta, 'F');
		}

		// Libreria Codeigniter
		$this->load->library('zip');
		$zipTODO = new CI_Zip();
		foreach ($arrayRutas as $key => $valueRuta) {
			if (file_exists($valueRuta['ruta'])) {
				$zipTODO->add_data($valueRuta['nombre'], file_get_contents($valueRuta['ruta']));
				unlink($valueRuta['ruta']);
			}
		}
		$fechaInicio = date('Ymd', strtotime($evalprod->FINICIOSERVICIO));
		$rutaDescarga = RUTA_ARCHIVOS . 'OI/TEMP/' . "{$id}-{$fechaInicio}.zip";
		$zipTODO->archive($rutaDescarga);
		$zipTODO->clear_data();

		$this->load->helper('download');
		force_download($rutaDescarga, null, false, true);
	}

	/**
	 * @param $id
	 * @param string $cproducto
	 */
	public function vista_previa($id, $cproducto = '')
	{
		try{

			$evalprod = $this->mhomologaciones->buscarExpdiente($id);
			if (empty($evalprod)) {
				throw new Exception('No pudo ser encontrado la evaluación');
			}
			// Producto
			$producto = $this->mhomologaciones->pdfBuscarProdEval($id, $cproducto);
			// Requisitos
			$requisitos = $this->mhomologaciones->pdfBuscarProdRequisitos($id, $cproducto);
			// Requisitos observaciones
			$observaciones = $this->mhomologaciones->pdfBuscarProdRequisitoObs($id, $cproducto);

			$content = $this->load->view('oi/homologaciones/vresultado_eval_pdf', [
				'evalprod' => $evalprod,
				'producto' => $producto,
				'requisitos' => $requisitos,
				'observaciones' => $observaciones,
			], TRUE);
			$html2pdf = new \Spipu\Html2Pdf\Html2Pdf();
			$html2pdf->writeHTML($content);
//		$html2pdf->setDefaultFont('arial');
			$fileName = str_replace([' ', '@'], '_', $evalprod->DPROVEEDORCLIENTE) . '-'
				. str_replace([' ', '@'], '_', $producto->PRODUCTO) . '-' . date('Ymd', strtotime($evalprod->FINICIOSERVICIO));
			$html2pdf->Output($fileName . '.pdf', 'I');
		} catch (Exception $ex) {
			show_error($ex->getMessage());
		}
	}

}
