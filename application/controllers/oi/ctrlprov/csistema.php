<?php

/**
 * Class csistema
 * @property msistema msistema
 */
class csistema extends FS_Controller
{

	/**
	 * csistema constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('oi/ctrlprov/msistema', 'msistema');
	}

	/**
	 * Recursos para crear o editar
	 */
	public function recursos()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		$areas = $this->msistema->lista_areas();
		$servicios = $this->msistema->lista_servicios();
		echo json_encode([
			'areas' => $areas,
			'servicios' => $servicios,
		]);
	}

	/**
	 * Busqueda de sistemas
	 */
	public function lista()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		$busqueda = $this->input->post('busqueda');
		$result = $this->msistema->lista(2, $busqueda);
		echo json_encode(['items' => $result]);
	}

	/**
	 * Se busca un sistema
	 */
	public function buscar()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		$codigo = $this->input->post('codigo');
		$sistema = $this->msistema->buscar($codigo);
		echo json_encode($sistema);
	}

	/**
	 *
	 */
	public function generar()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {

			$id = $this->input->post('sistema_id');
			$descripcion = $this->input->post('sistema_descripcion');
			$area_id = $this->input->post('sistema_area_id');
			$servicio_id = $this->input->post('sistema_servicio_id');
			$estado = $this->input->post('sistema_estado');
			if (empty($descripcion)) {
				throw new Exception('Debes ingresar la descripción.');
			}
			if (empty($area_id)) {
				throw new Exception('Debes elegir una área.');
			}
			if (empty($servicio_id)) {
				throw new Exception('Debes elegir un servicio.');
			}

			$s_cusuario = $this->session->userdata('s_idusuario');
			if (empty($s_cusuario)) {
				throw new Exception('Debe volver a iniciar sesión.');
			}

			$data = $this->msistema->guardar(
				$id,
				$descripcion,
				2,
				$area_id,
				$servicio_id,
				$s_cusuario,
				$estado
			);

			if (empty($data)) {
				throw new Exception('Error en el proceso del sistema.');
			}

			$this->result['status'] = 200;
			$textEStado = (empty($id)) ? 'creado' : 'actualizado';
			$this->result['message'] = "Sistema fue {$textEStado} correctamente.";
			$this->result['data'] = $data;

		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

	/**
	 * Elimina un sistema
	 */
	public function eliminar()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {

			$codigo = $this->input->post('codigo');
			$sistema = $this->msistema->buscar($codigo);
			if (empty($sistema)) {
				throw new Exception('El Sistema no pudo ser encontrado.');
			}
			$res = $this->msistema->eliminar($sistema->CSISTEMA);
			if (!$res) {
				throw new Exception('Error en el proceso de eliminación.');
			}

			$this->result['status'] = 200;
			$this->result['message'] = "El sistema fue eliminado eliminado correctamente.";

		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

}
