<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
define("DOMPDF_ENABLE_REMOTE", true);

/**
 * Class caccion_correctiva
 *
 * @property maccion_correctiva maccion_correctiva
 * @property minspctrolprov minspctrolprov
 */
class caccion_correctiva extends FS_Controller
{

	/**
	 * caccion_correctiva constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('oi/ctrlprov/aacc/maccion_correctiva', 'maccion_correctiva');
		$this->load->model('oi/ctrlprov/minspctrolprov', 'minspctrolprov');
	}

	/**
	 * Lista de acciones correctivas de las inspeccion de un cliente
	 */
	public function lista()
	{
		$ccliente = $this->input->post('ccliente');
		$busqueda = $this->input->post('busqueda');
		$items = $this->maccion_correctiva->listaInsp($ccliente, $busqueda);
		echo json_encode($items);
	}

	public function buscar()
	{
		$cauditoria = $this->input->post('cauditoria');
		$fservicio = $this->input->post('fservicio');
		$inspeccion = $this->minspctrolprov->buscar($cauditoria, $fservicio);
		echo json_encode($inspeccion);
	}

	public function buscar_aacc()
	{
		$cauditoria = $this->input->post('cauditoria');
		$fservicio = $this->input->post('fservicio');
		$fservicio = \Carbon\Carbon::createFromFormat('d/m/Y', $fservicio, 'America/Lima');
		$aacc = $this->maccion_correctiva->listaAACC($cauditoria, $fservicio->format('Y-m-d'));
		echo json_encode($aacc);
	}

}
