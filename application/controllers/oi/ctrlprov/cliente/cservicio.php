<?php

/**
 * Class cservicio
 *
 * @property matservicio mservicio
 */
class cservicio extends FS_Controller
{

	/**
	 * cservicio constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('at/ctrlprov/cliente/matservicio', 'mservicio');
	}

	/**
	 * Lista de servicios
	 */
	public function lista()
	{
		$ccliente = $this->input->post('ccliente');
		$items = $this->mservicio->lista($ccliente);
		echo json_encode($items);
	}

	public function get_servicios()
	{
		$items = $this->mservicio->getServicios();
		echo json_encode($items);
	}

	public function get_responsabled()
	{
		$items = $this->mservicio->getResponsables();
		echo json_encode($items);
	}

	public function get_subservicios()
	{
		$cservicio = $this->input->post('id');
		$items = $this->mservicio->getSubServicios($cservicio);
		echo json_encode($items);
	}

	/**
	 * Guarda el servicio
	 */
	public function guardar()
	{
		$this->db->select_max('CINTERNOPTE');
		$query = $this->db->get('PCPTE');
		$id_ultimo = $query->row()->CINTERNOPTE;
		$id_ultimo_anio = substr($id_ultimo, 0, 4);
		$anio_actual = date("Y");
		if ($id_ultimo_anio === $anio_actual) {
			$codigo = $id_ultimo + 1;
		} else {
			$codigo = $anio_actual . '0000';
		}
		$str_numero_pdte = substr($codigo, 4, 4) . '-' . $anio_actual . '/AT' . '/FS';
		$parametros = array(
			'@CINTERNOPTE' => $codigo,
			'@DNUMEROPTE' => $str_numero_pdte,
			'@CCLIENTE' => $this->input->post('idCLienteServ'),
			'@CSERVICIO' => $this->input->post('cboServicio'),
			'@CSUBSERVICIO' => $this->input->post('cboSubServicio'),
			'@CRESPONSABLE' => $this->input->post('cboResponsableOI')

		);
		$resultado = $this->mservicio->createServicio($parametros);
		echo json_encode($resultado);
	}

}
