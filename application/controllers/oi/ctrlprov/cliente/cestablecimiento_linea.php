<?php

/**
 * Class cestablecimiento_linea
 *
 * @property mestablecimiento_linea mlinea
 */
class cestablecimiento_linea extends FS_Controller
{

	/**
	 * cestablecimiento_linea constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('at/ctrlprov/cliente/mestablecimiento_linea', 'mlinea');
	}

	/**
	 * Lista de lineas por establecimiento
	 */
	public function lista()
	{
		$ccliente = $this->input->post('ccliente');
		$cestaclecimiento = $this->input->post('cestablecimiento');
		$items = $this->mlinea->lista($ccliente, $cestaclecimiento);
		echo json_encode($items);
	}

	public function guardar()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {
			$ccliente = $this->input->post('hdnIdptClieLinea');
			$cestablecimiento = $this->input->post('txtestablecimiento');
			$clineaprocesocliente = $this->input->post('hdnIdLinea');
			$txtLineaProc = $this->input->post('txtLineaProc');
			$chxPeligro = $this->input->post('chxPeligro');

			$chxPeligro = 'N'; // ($chxPeligro == '1') ? 'S' : 'N';

			$txtLineaProc = trim($txtLineaProc);
			if (empty($txtLineaProc)) {
				throw new Exception('Debes ingresar el nombre de la línea.');
			}

			$this->mlinea->guardar($clineaprocesocliente, $ccliente, $cestablecimiento, $txtLineaProc, $chxPeligro, $this->session->userdata('s_cusuario'), 'A');

			$this->result['status'] = 200;
			$this->result['message'] = 'Linea registrada correctamente.';
			$this->result['data'] = [];
		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

}
