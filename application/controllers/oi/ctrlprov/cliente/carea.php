<?php

/**
 * Class carea
 *
 * @property matarea marea
 */
class carea extends FS_Controller
{

	/**
	 * carea constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('oi/ctrlprov/cliente/matarea', 'marea');
	}

	/**
	 * Lista de areas
	 */
	public function lista()
	{
		$ccliente = $this->input->post('ccliente');
		$items = $this->marea->lista($ccliente);
		echo json_encode($items);
	}

	/**
	 * Guardar area
	 */
	public function guardar()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {
			$ccareacliente = $this->input->post('mhdnIdArea');
			$ccliente = $this->input->post('mhdnIdClieArea');
			$dareacliente = $this->input->post('str_descripcion_area');
			$estado = $this->input->post('cboEstado');
			$concesionario = $this->input->post('cboConsecionario');
			$str_jerarquia_area = $this->input->post('str_jerarquia_area');
			$ctipo = $this->input->post('cboTipoArea');
			if (empty($dareacliente)) {
				throw new Exception('Debes ingresar el nombre de area.');
			}
			$this->marea->guardar($ccliente, $ccareacliente, $dareacliente, $this->session->userdata('s_cusuario'), $estado, $concesionario, 2, $ctipo, $str_jerarquia_area);
			$this->result['status'] = 200;
			$this->result['message'] = 'Area registrada correctamente.';
		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

}
