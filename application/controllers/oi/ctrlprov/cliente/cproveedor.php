<?php

/**
 * Class cproveedor
 *
 * @property matproveedor mproveedor
 */
class cproveedor extends FS_Controller
{

	/**
	 * cproveedor constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('at/ctrlprov/cliente/matproveedor', 'mproveedor');
	}

	/**
	 * Lista de proveedores asignados al cliente
	 */
	public function lista()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		$ccliente = $this->input->post('ccliente');
		$items = $this->mproveedor->lista($ccliente);
		echo json_encode($items);
	}

	/**
	 * Lista de proveedores no asignados al cliente
	 */
	public function lista_proveedores()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		$ccliente = $this->input->post('ccliente');
		$items = $this->mproveedor->listaProveedores($ccliente);
		echo json_encode($items);
	}

	/**
	 * Guarda la relacion de clientes con su proveedor
	 */
	public function guardar()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {
			$ccliente = $this->input->post('ccliente');
			$cproveedor = $this->input->post('cproveedor');
			if (empty($this->session->userdata('s_cusuario'))) {
				throw new Exception('Debes volver a iniciar sesión!');
			}
			$this->mproveedor->guardar($ccliente, $cproveedor, $this->session->userdata('s_cusuario'), 'A', 'C');
			$this->result['status'] = 200;
			$this->result['message'] = 'Registrado correctamente.';
		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

}
