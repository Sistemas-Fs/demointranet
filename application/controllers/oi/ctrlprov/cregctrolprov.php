<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class Cregctrolprov
 * @property mregctrolprov mregctrolprov
 * @property minspeccion minspeccion
 */
class Cregctrolprov extends FS_Controller
{
	function __construct()
	{
		parent:: __construct();
		$this->load->model('oi/ctrlprov/mregctrolprov');
		$this->load->model('oi/ctrlprov/inspctrolprov/minspeccion');
		$this->load->model('mglobales');
		$this->load->library('encryption');
		$this->load->helper(array('form', 'url', 'download', 'html', 'file'));
		$this->load->library('form_validation');
	}

	/** CONTROL DE PROVEEDORES **/
	public function getcboclieserv()
	{    // Visualizar Clientes del servicio en CBO

		$resultado = $this->mregctrolprov->getcboclieserv();
		echo json_encode($resultado);
	}

	public function getcboprovxclie()
	{    // Visualizar Proveedor por cliente en CBO

		$parametros = array(
			'@ccliente' => $this->input->post('ccliente')
		);
		$resultado = $this->mregctrolprov->getcboprovxclie($parametros);
		echo json_encode($resultado);
	}

	public function getcbomaqxprov()
	{    // Visualizar Maquilador por proveedor en CBO

		$parametros = array(
			'@cproveedor' => $this->input->post('cproveedor')
		);
		$resultado = $this->mregctrolprov->getcbomaqxprov($parametros);
		echo json_encode($resultado);
	}

	public function getcostos()
	{
		$cauditoria = $this->input->post('cauditoria');
		$resultado = $this->mregctrolprov->getTipoEstableCosto($cauditoria);
		echo json_encode($resultado);
	}

	/**
	 * Visualizar Inspectores en CBO
	 */
	public function getcboinspector()
	{
		$sregistro = $this->input->post('sregistro');
		$sregistro = (empty($sregistro)) ? 'A' : $sregistro;
		$parametros = array(
			'@sregistro' => $sregistro
		);
		$resultado = $this->mregctrolprov->getcboinspector($parametros);
		echo json_encode($resultado);
	}

	public function getcboestado()
	{    // Visualizar Estado en CBO

		$resultado = $this->mregctrolprov->getcboestado();
		echo json_encode($resultado);
	}

	public function getcbocalif()
	{    // Visualizar Calificacion en CBO

		$resultado = $this->mregctrolprov->getcbocalif();
		echo json_encode($resultado);
	}

	/**
	 * Lista de registro de inspeccion
	 */
	public function getbuscarctrlprov()
	{    // Busca Control de Proveedores

		$varnull = '';

		$ccia = '2';
		$carea = '01';
		$cservicio = '01';
		$fini = $this->input->post('fdesde');
		$ffin = $this->input->post('fhasta');
		$ccliente = $this->input->post('ccliente');
		$cclienteprov = $this->input->post('dclienteprovmaq');
		$cclientemaq = trim($this->input->post('cclientemaq'));
		$inspector = $this->input->post('inspector');
		$cboestado = $this->input->post('cboestado');
		$speligro = $this->input->post('speligro');
		$codigo = $this->input->post('codigo');

		$codigo = (empty($codigo)) ? '%' : str_pad($codigo, 8, '0', STR_PAD_LEFT);

		$parametros = array(
			'@ccia' => $ccia,
			'@carea' => $carea,
			'@cservicio' => $cservicio,
			'@fini' => ($fini == '%') ? NULL : substr($fini, 6, 4) . '-' . substr($fini, 3, 2) . '-' . substr($fini, 0, 2),
			'@ffin' => ($ffin == '%') ? NULL : substr($ffin, 6, 4) . '-' . substr($ffin, 3, 2) . '-' . substr($ffin, 0, 2),
			'@ccliente' => (empty($ccliente)) ? '%' : $ccliente,
			'@cclienteprov' => (empty($cclienteprov)) ? '%' : "%{$cclienteprov}%",
			'@cclientemaq' => (empty($cclientemaq)) ? '%' : $cclientemaq,
			'@inspector' => (empty($inspector)) ? '%' : $inspector,
			'@cusuario' => '%',
			'@estado' => (empty($cboestado)) ? '%' : implode(',', $cboestado),
			'@speligro' => (empty($speligro)) ? '%' : $speligro,
			'@codigo' => (empty($codigo)) ? '%' : $codigo,
		);
		$resultado = $this->minspeccion->getbuscarctrlprov($parametros);
		echo json_encode($resultado);
	}

	public function getrecuperainsp()
	{    // Visualizar Maquilador por proveedor en CBO

		$parametros = array(
			'@idinspeccion' => $this->input->post('idinspeccion')
		);
		$resultado = $this->mregctrolprov->getrecuperainsp($parametros);
		echo json_encode($resultado);
	}

	public function getcboregestable()
	{    // Visualizar Maquilador por proveedor en CBO

		$parametros = array(
			'@ccliente' => $this->input->post('ccliente'),
			'@cproveedor' => $this->input->post('cproveedor'),
			'@cmaquilador' => $this->input->post('cmaquilador'),
			'@tipo' => $this->input->post('tipo')
		);
		$resultado = $this->mregctrolprov->getcboregestable($parametros);
		echo json_encode($resultado);
	}

	public function getdirestable()
	{    // Visualizar Proveedor por cliente en CBO

		$cestable = $this->input->post('cestablecimiento');
		$resultado = $this->mregctrolprov->getdirestable($cestable);
		echo json_encode($resultado);
	}

	public function getcboareaclie()
	{    // Visualizar Proveedor por cliente en CBO

		$ccliente = $this->input->post('ccliente');
		$resultado = $this->mregctrolprov->getcboareaclie($ccliente);
		echo json_encode($resultado);
	}

	public function getcbocartafirmante()
	{    // Visualizar Proveedor por cliente en CBO

		$ccliente = $this->input->post('ccliente');
		$resultado = $this->mregctrolprov->getcbocartafirmante($ccliente);
		echo json_encode($resultado);
	}

	public function getcbolineaproc()
	{    // Visualizar Proveedor por cliente en CBO

		$cestablecimiento = $this->input->post('cestablecimiento');
		$resultado = $this->mregctrolprov->getcbolineaproc($cestablecimiento);
		echo json_encode($resultado);
	}

	public function getcbocontacprinc()
	{    // Visualizar Proveedor por cliente en CBO

		$cproveedor = $this->input->post('cproveedor');
		$resultado = $this->mregctrolprov->getcbocontacprinc($cproveedor);
		echo json_encode($resultado);
	}

	public function getcbocartadestinatario()
	{    // Visualizar Proveedor por cliente en CBO

		$cproveedor = $this->input->post('cproveedor');
		$resultado = $this->mregctrolprov->getcbocartadestinatario($cproveedor);
		echo json_encode($resultado);
	}

	public function getcbotipoestable()
	{    // Visualizar Proveedor por cliente en CBO

		$resultado = $this->mregctrolprov->getcbotipoestable();
		echo json_encode($resultado);
	}

	public function getmontotipoestable()
	{    // Visualizar Proveedor por cliente en CBO

		$ctipoestable = $this->input->post('ctipoestable');
		$resultado = $this->mregctrolprov->getmontotipoestable($ctipoestable);
		echo json_encode($resultado);
	}

	public function setregctrlprov()
	{ // Registrar inspeccion
		$varnull = '';

		$cauditoriainspeccion = $this->input->post('mhdnregIdinsp');
		$ccliente = $this->input->post('cboregClie');
		$cproveedor = $this->input->post('cboregprovclie');
		$cmaquilador = $this->input->post('cboregmaquiprov');
		$cestablecimiento = $this->input->post('cboregestable');
		$clineaprocesocliente = $this->input->post('cboreglineaproc');
		$careacliente = $this->input->post('cboregareaclie');
		$ccontacto = $this->input->post('cbocontacprinc');
		$ctipoestablecimiento = $this->input->post('cbotipoestable');
		$icostobase = $this->input->post('txtcostoestable');
		$dperiodo = $this->input->post('mtxtregPeriodo');
		$cusuario = $this->input->post('hdnregcusuario');
		$accion = $this->input->post('mhdnAccionctrlprov');
		$cbocartafirmante = $this->input->post('cbocartafirmante');
//		$cbocartadestinatario = $this->input->post('cbocartadestinatario');

		$parametros = array(
			'@cauditoriainspeccion' => $cauditoriainspeccion,
			'@ccliente' => $ccliente,
			'@cproveedor' => $cproveedor,
			'@cmaquilador' => ($this->input->post('cboregmaquiprov') == '') ? '' : $cmaquilador,
			'@cestablecimiento' => $cestablecimiento,
			'@clineaprocesocliente' => $clineaprocesocliente,
			'@careacliente' => $careacliente,
			'@ccontacto' => $ccontacto,
			'@ctipoestablecimiento' => $ctipoestablecimiento,
			'@icostobase' => $icostobase,
			'@dperiodo' => $dperiodo,
			'@ccontactofirmante' => $cbocartafirmante,
			'@cusuario' => $cusuario,
			'@accion' => $accion,
		);
		$resultado = $this->mregctrolprov->setregctrlprov($parametros);
		echo json_encode($resultado);
	}

	public function getcbosistemaip()
	{    // Visualizar Calificacion en CBO

		$resultado = $this->mregctrolprov->getcbosistemaip();
		echo json_encode($resultado);
	}

	public function getcborubroip()
	{    // Visualizar Inspectores en CBO

		$parametros = array(
			'@idnorma' => $this->input->post('cnorma')
		);
		$resultado = $this->mregctrolprov->getcborubroip($parametros);
		echo json_encode($resultado);
	}

	public function getcbochecklist()
	{    // Visualizar Inspectores en CBO

		$parametros = array(
			'@idnorma' => $this->input->post('cnorma'),
			'@idsubnorma' => $this->input->post('csubnorma'),
			'@ccliente' => $this->input->post('ccliente')
		);
		$resultado = $this->mregctrolprov->getcbochecklist($parametros);
		echo json_encode($resultado);
	}

	public function getcbomodinforme()
	{    // Visualizar Calificacion en CBO

		$resultado = $this->mregctrolprov->getcbomodinforme();
		echo json_encode($resultado);
	}

	public function getcboinspvalconf()
	{    // Visualizar Calificacion en CBO

		$resultado = $this->mregctrolprov->getcboinspvalconf();
		echo json_encode($resultado);
	}

	public function getcboinspformula()
	{    // Visualizar Proveedor por cliente en CBO

		$cchecklist = $this->input->post('cchecklist');
		$resultado = $this->mregctrolprov->getcboinspformula($cchecklist);
		echo json_encode($resultado);
	}

	public function getcboinspcritresul()
	{    // Visualizar Calificacion en CBO

		$resultado = $this->mregctrolprov->getcboinspcritresul();
		echo json_encode($resultado);
	}

	public function setreginspeccion()
	{    // Visualizar Inspectores en CBO
		$varnull = '';

		$cauditoriainspeccion = $this->input->post('mtxtidinsp');
		$fservicio = $this->input->post('mfechainsp');
		$fechaservicio = $this->input->post('txtFInsp');
		$cusuarioconsultor = $this->input->post('cboinspinspector');
		$chkaacc = $this->input->post('chkaacc');
		if ($chkaacc == 'on') {
			$spermitircorrectivas = 'S';
		} else {
			$spermitircorrectivas = 'N';
		}
		$zctiposervicio = $this->input->post('cbotiposerv');
		$chkplaninsp = $this->input->post('chkplaninsp');
		if ($chkplaninsp == 'on') {
			$sincluyeplan = 'S';
		} else {
			$sincluyeplan = 'N';
		}
		$cnorma = $this->input->post('cboinspsistema');
		$csubnorma = $this->input->post('cboinsprubro');
		$cchecklist = $this->input->post('cboinspcchecklist');
		$cvalornoconformidad = $this->input->post('cboinspvalconf');
		$cformulaevaluacion = $this->input->post('cboinspformula');
		$dcometario = $this->input->post('mtxtinspcoment');
		$ccriterioresultado = $this->input->post('cboinspcritresul');
		$cmodeloinforme = $this->input->post('cboinspmodeloinfo');
		$periodo = $this->input->post('cboinspperiodo');
		$zctipoestado = $this->input->post('mhdnzctipoestado');
		$accion = $this->input->post('mhdnAccioninsp');

		$parametros = array(
			'@cauditoriainspeccion' => $cauditoriainspeccion,
			'@fservicio' => ($this->input->post('mfechainsp') == '') ? '1900-01-01' : substr($fservicio, 6, 4) . '-' . substr($fservicio, 3, 2) . '-' . substr($fservicio, 0, 2),
			'@fechaservicio' => ($this->input->post('txtFInsp') == 'Sin Fecha') ? '1900-01-01' : substr($fechaservicio, 6, 4) . '-' . substr($fechaservicio, 3, 2) . '-' . substr($fechaservicio, 0, 2),
			'@cusuarioconsultor' => ($this->input->post('cboinspinspector') == $varnull) ? null : $cusuarioconsultor,
			'@spermitircorrectivas' => $spermitircorrectivas,
			'@zctiposervicio' => $zctiposervicio,
			'@sincluyeplan' => $sincluyeplan,
			'@cnorma' => ($this->input->post('cboinspsistema') == $varnull) ? null : $cnorma,
			'@csubnorma' => ($this->input->post('cboinsprubro') == $varnull) ? null : $csubnorma,
			'@cchecklist' => ($this->input->post('cboinspcchecklist') == $varnull) ? null : $cchecklist,
			'@cvalornoconformidad' => ($this->input->post('cboinspvalconf') == $varnull) ? null : $cvalornoconformidad,
			'@cformulaevaluacion' => ($this->input->post('cboinspformula') == $varnull) ? null : $cformulaevaluacion,
			'@dcometario' => $dcometario,
			'@ccriterioresultado' => ($this->input->post('cboinspcritresul') == $varnull) ? null : $ccriterioresultado,
			'@cmodeloinforme' => ($this->input->post('cboinspmodeloinfo') == $varnull) ? null : $cmodeloinforme,
			'@periodo' => $periodo,
			'@zctipoestado' => $zctipoestado,
			'@accion' => $accion
		);
		$resultado = $this->mregctrolprov->setreginspeccion($parametros);
		echo json_encode($resultado);
	}

	public function getcbocierreTipo()
	{    // Visualizar Proveedor por cliente en CBO

		$resultado = $this->mregctrolprov->getcbocierreTipo();
		echo json_encode($resultado);
	}

	public function getcbocontacplanins()
	{    // Visualizar Proveedor por cliente en CBO
		$varnull = '';
		$ccliente = $this->input->get('ccliente');
		$cproveedorcliente = $this->input->get('cproveedor');
		$cmaquiladorcliente = (empty($this->input->get('cmaquilador'))) ? '' : $this->input->get('cmaquilador');

		$resultado = $this->mregctrolprov->getcbocontacplanins_new($cproveedorcliente, $cmaquiladorcliente);
		echo json_encode($resultado);
	}

	public function setcierreespecial()
	{    // Visualizar Inspectores en CBO
		$varnull = '';

		$cauditoriainspeccion = $this->input->post('txtcierreidinsp');
		$fservicio = $this->input->post('mhdncierrefservicio');
		$fechacierre = $this->input->post('txtcierrefservicio');
		$zctipoestadoservicio = $this->input->post('cbocierreTipo');
		$valorestado = $this->input->post('mhdnfprog');
		$fechaprogramada = $this->input->post('txtcierreFProg');
		$itrunco = $this->input->post('txtcierremonto');
		$ngastogeneral = $this->input->post('txtcierreviatico');
		$dcomentario = $this->input->post('mtxtcierrecomentario');
		$cusuario = $this->input->post('hdncusuario');
		$accion = $this->input->post('mhdnAccioncierre');

		$parametros = array(
			'@cauditoriainspeccion' => $cauditoriainspeccion,
			'@fservicio' => ($this->input->post('mhdncierrefservicio') == '') ? '1900-01-01' : substr($fservicio, 6, 4) . '-' . substr($fservicio, 3, 2) . '-' . substr($fservicio, 0, 2),
			'@fechacierre' => ($this->input->post('txtcierrefservicio') == '') ? '1900-01-01' : substr($fechacierre, 6, 4) . '-' . substr($fechacierre, 3, 2) . '-' . substr($fechacierre, 0, 2),
			'@zctipoestadoservicio' => $zctipoestadoservicio,
			'@valorestado' => $valorestado,
			'@fechaprogramada' => ($this->input->post('txtcierreFProg') == '') ? '1900-01-01' : substr($fechaprogramada, 6, 4) . '-' . substr($fechaprogramada, 3, 2) . '-' . substr($fechaprogramada, 0, 2),
			'@itrunco' => $itrunco,
			'@ngastogeneral' => $ngastogeneral,
			'@dcomentario' => $dcomentario,
			'@cusuario' => $cusuario,
			'@accion' => $accion
		);
		$resultado = $this->mregctrolprov->setcierreespecial($parametros);
		echo json_encode($resultado);
	}

	public function setreaperturar()
	{    // Visualizar Inspectores en CBO
		$varnull = '';

		$cauditoriainspeccion = $this->input->post('cauditoriainspeccion');
		$fservicio = $this->input->post('fservicio');
		$cusuario = $this->input->post('cusuario');

		$parametros = array(
			'@cauditoriainspeccion' => $cauditoriainspeccion,
			'@fservicio' => ($this->input->post('fservicio') == '') ? '1900-01-01' : substr($fservicio, 6, 4) . '-' . substr($fservicio, 3, 2) . '-' . substr($fservicio, 0, 2),
			'@cusuario' => $cusuario,
		);
		$resultado = $this->mregctrolprov->setreaperturar($parametros);
		echo json_encode($resultado);
	}

	public function getcbocertificadora()
	{    // Visualizar Proveedor por cliente en CBO

		$resultado = $this->mregctrolprov->getcbocertificadora();
		echo json_encode($resultado);
	}

	public function setplaninsp()
	{    // Visualizar Inspectores en CBO
		$varnull = '';

		$cauditoriainspeccion = $this->input->post('mhdnidinspplaninsp');
		$fservicio = $this->input->post('mhdnfservicioplaninsp');
		$dobjetivoplan = $this->input->post('mtxtobjeplaninsp');
		$dalcanceplan = $this->input->post('mtxtalcanplaninsp');
		$fechaplan = $this->input->post('mtxtFplanins');
		$horaplan = $this->input->post('mtxtHplanins');
		$ccontacto = $this->input->post('cbocontacplanins');
		$drequerimiento = $this->input->post('mtxtrequeplaninsp');
		$tiposerv = $this->input->post('mhdntiposervplaninsp');
		$cusuario = $this->input->post('hdncusuarioplaninsp');
		$accion = $this->input->post('mhdnAccionplaninsp');

		$parametros = array(
			'@cauditoriainspeccion' => $cauditoriainspeccion,
			'@fservicio' => ($this->input->post('mhdnfservicioplaninsp') == '') ? '1900-01-01' : substr($fservicio, 6, 4) . '-' . substr($fservicio, 3, 2) . '-' . substr($fservicio, 0, 2),
			'@dobjetivoplan' => $dobjetivoplan,
			'@dalcanceplan' => $dalcanceplan,
			'@fechaplan' => ($this->input->post('mtxtFplanins') == '') ? '1900-01-01' : substr($fechaplan, 6, 4) . '-' . substr($fechaplan, 3, 2) . '-' . substr($fechaplan, 0, 2),
			'@horaplan' => $horaplan,
			'@ccontacto' => $ccontacto,
			'@drequerimiento' => $drequerimiento,
			'@tiposerv' => $tiposerv,
			'@cusuario' => $cusuario,
			'@accion' => $accion
		);
		$resultado = $this->mregctrolprov->setplaninsp($parametros);
		echo json_encode($resultado);
	}

	public function genpdfplaninsp($idinspeccion, $fservicio)
	{ // recupera los cPTIZACION
		$this->load->library('pdfgenerator');

		$date = getdate();
		$fechaactual = date("d") . "/" . date("m") . "/" . date("Y");

		$html = '<html>
                <head>
                    <title>Plan de Inspeccion</title>
                    <style>
                        @page {
                             margin: 0.3in 0.3in 0.3in 0.3in;
                        } 
                        .teacherPage {
                            page: teacher;
                            page-break-after: always;
                        }
                        body{
                            font-family: Arial, Helvetica, sans-serif;
                            font-size: 9pt;
                            margin-top: 3cm;
                            margin-left: 0cm;
                            margin-right: 0cm;
                            margin-bottom: 0cm;
                        }  
                        header {
                            position: fixed;
                            top: 0cm;
                            left: 0cm;
                            right: 0cm;
                            height: 3cm;
                        }
                        .cuerpo {
                            text-align: justify;
                        }
                        img.izquierda {
                            float: left;
                        }
                        img.derecha {
                            float: right;
                        }
                        div.page_break {
                            page-break-before: always;
                        }
                        .page-number {
                          text-align: right;
                        }
                        
                        .page-number:before {
                          content: counter(page);
                        }
                        th { 
                            text-align: center; 
                            border: 1px solid black;
                        }
                    </style>
                </head>
                <body>
                
                <header>
                    <table  width="700px" align="center" cellspacing="0" cellpadding="2" style="border: 0px solid black;">
                        <tr>
                            <td width="70%" rowspan="4">
								<img src="' . public_url_ftp() . 'Imagenes/formatos/1/logoFS.png" width="180" height="70" />   
                            </td>
                            <td width="30%" align="right">
								Av. Del Pinar 110 of. 405-407
                            </td>
                        </tr>
                        <tr>
                            <td align="right">Santiago de Surco, Lima - Perú</td>
                        </tr>
                        <tr>
                            <td align="right">(51-1)372-1734 / 372-8182</td>
                        </tr>
                        <tr>
                            <td align="right">www.grupofs.com </td>
                        </tr>
                        <tr>
							<td align="center" colspan="2">
								<b>PLAN DE INSPECCION</b>
							</td>
                        </tr>
                    </table>
				</header>';
		$res = $this->mregctrolprov->getpdfdatosplaninsp($idinspeccion, $fservicio);
		if ($res) {
			foreach ($res as $row) {
				$empresa = $row->empresa;
				$direccion = $row->direccion;
				$dobjetivoplan = $row->dobjetivoplan;
				$dalcanceplan = $row->dalcanceplan;
				$finspeccion = $row->finspeccion;
				$hinicio = $row->hinicio;
				$contacto = $row->contacto;
				$dtelefono = $row->dtelefono;
				$inspector = $row->inspector;
				$drequerimiento = $row->drequerimiento;
				$ccompania = $row->ccompania;
				$carea = $row->carea;
				$cservicio = $row->cservicio;
				$dcriterios = $row->dcriterios;
			}
		}
		$html .= '
				<main>
					<table width="700px" align="center">
						<tr>
							<td colspan="2" style="height:30px;"><b>EMPRESA : </b>' . $empresa . '</td>
						</tr>
						<tr>
							<td colspan="2" style="height:30px;"><b>DIRECCION : </b>' . $direccion . '</td>
						</tr>
						<tr>
							<td colspan="2"><b>OBJETIVO : </b></td>
						</tr>
						<tr>
							<td colspan="2" style="height:30px;">' . $dobjetivoplan . '</td>
						</tr>
						<tr>
							<td colspan="2" style="height:30px;"><b>ALCANCE : </b>' . $dalcanceplan . '</td>
						</tr>
						<tr>
							<td colspan="2" style="height:30px;"><b>FECHA DE INSPECCION : </b>' . $finspeccion . '</td>
						</tr>
						<tr>
							<td colspan="2" style="height:30px;"><b>HORA DE INICIO : </b>' . $hinicio . '</td>
						</tr>
						<tr>
							<td colspan="2" style="height:30px;"><b>PERSONA DE CONTACTO : </b>' . $contacto . '</td>
						</tr>
						<tr>
							<td colspan="2" style="height:30px;"><b>TELEFONO : </b>' . $dtelefono . '</td>
						</tr>
						<tr>
							<td colspan="2" style="height:30px;"><b>INSPECTOR : </b>' . $inspector . '</td>
						</tr>
						<tr>
							<td colspan="2" style="height:30px;"><b>REQUISITO : </b>' . $drequerimiento . '</td>
						</tr>
						<tr>
							<td colspan="2"><b>METODOLOGIA : </b></td>
						</tr>';
		$resmet = $this->mregctrolprov->getpdfmetodoplaninsp($ccompania, $carea, $cservicio);
		if ($resmet) {
			foreach ($resmet as $rowmet) {
				$dmetodologia = $rowmet->dmetodologia;
				$html .= '<tr>
										<td colspan="2" style="text-align: justify;">&nbsp;' . $dmetodologia . '</td>
									</tr>';
			}
		}
		$html .= '<tr>
							<td colspan="2" style="height:30px;"><b>CRITERIOS DE INSPECCION : </b></td>
						</tr>
						<tr>
							<td colspan="2" style="text-align: justify;">&nbsp;' . $dcriterios . '</td>
						</tr>
						<tr>
							<td colspan="2" style="height:30px;"><b>SUSPENSION DE LA INSPECCION : </b></td>
						</tr>
						<tr>
							<td colspan="2">De darse una inspección trunca por permitir el ingreso al inspector al establecimiento o cancelarla a ultima hora tendra un costo según se muestra en la siguiente tabla:</td>
						</tr>
						<tr>
							<td colspan="2" style="height:10px;"></td>
						</tr>
						<tr>
							<td align="center" style="border: 1px solid black;" width="30%"><b>UBICACIONES</b></td>
							<td align="center" style="border: 1px solid black;"width="70%"><b>COSTOS</b></td>
						</tr>
						<tr>
							<td style="border: 1px solid black;"><b>LIMA</b></td>
							<td style="border: 1px solid black; text-align: justify;">El costo por inspecáones canceladas con menos de 2 días útiles a la inspección,  incluidas las canceladas cuando el inspedor a llegado al establecimiento,  será del  30%  en base al costo de la inspección más IGV,  según tipo de establecimiento. </td>
						</tr>
						<tr>
							<td style="border: 1px solid black;"><b>A NIVEL NACIONAL (excepto Lima Metropolitana)</b></td>
							<td style="border: 1px solid black; text-align: justify;">El costo por inspecáones  canceladas con menos de 15 días útiles de anticipación,  incluidas las canceladas  cuando el inspedor a llegado al establecimiento,  será  del  30%  en base al costo de la inspección más IGV,  según tipo de establecimiento, asi como los gastos de viáticos Que incurra el lnspedor.</td>
						</tr>
					</table>';

		$html .= '</main></body></html>';
		$filename = 'Plan-Inspeccion';
		$this->pdfgenerator->generate($html, $filename, TRUE, 'A4', 'portrait');
	}

	public function getcbocertificacion()
	{    // Visualizar Proveedor por cliente en CBO

		$ccertificadora = $this->input->post('ccertificadora');
		$resultado = $this->mregctrolprov->getcbocertificacion($ccertificadora);
		echo json_encode($resultado);
	}

	public function calculocriterio()
	{    // Visualizar Proveedor por cliente en CBO

		$vresul = $this->input->post('resultado');
		$ccriterio = $this->input->post('ccriterio');
		$resultado = $this->mregctrolprov->calculocriterio($vresul, $ccriterio);
		echo json_encode($resultado);
	}

	/**
	 * @throws Exception
	 */
	public function setconvalidacion()
	{
		try {

			$cauditoriainspeccion = $this->input->post('txtconvaliidinsp');
			$fservicio = $this->input->post('txtconvalifservicio');
			$ccertificadora = $this->input->post('cbocertificadora');
			$fcertificaciontxt = $this->input->post('txtFConva');
			$presultadocertificadora = $this->input->post('txtconvaliresul');
			$ccriterioresultado = $this->input->post('mhdncritresultconvali');
			$cdetallecriterioresultado = $this->input->post('mhdncritdetresultconvali');
			$dobservacion = $this->input->post('mtxtconvalicomentario');
			$dnrodocumento = $this->input->post('txtconvalidocu');
			$nmes = $this->input->post('txtconvalimes');
			$ccertificacion = $this->input->post('cbocertificacion');
			$cusuario = $this->input->post('hdncusuarioconvali');
			$accion = $this->input->post('mhdnAccionconvali');
			$cboclieserv = $this->input->post('cboclieserv');

			if (empty($fservicio)) {
				throw new Exception('La Fecha de Servicio no puede estar vacía.');
			}
			if (empty($fcertificaciontxt)) {
				throw new Exception('La Fecha de Convalidacion no puede estar vacía.');
			}
			if (empty($ccertificadora)) {
				throw new Exception('Debes elegir una certificadora');
			}
			if (empty($_FILES['mtxtArchivoconvali']['name'])) {
				throw new Exception('Debes elegir un archivo.');
			}

			if (empty($_FILES['mtxtArchivoconvali']['name'])) { // Si existe el archivo foto se agrega
				throw new Exception('Debes elegir el archivo convalidado.');
			}

			if (empty($ccriterioresultado)) {
				throw new Exception('No tiene un criterior de resultado.');
			}

			$fservicio = \Carbon\Carbon::createFromFormat('d/m/Y', $fservicio, 'America/Lima');
			$fcertificacion = \Carbon\Carbon::createFromFormat('d/m/Y', $fcertificaciontxt, 'America/Lima');
			$var_fperiodo = \Carbon\Carbon::createFromFormat('d/m/Y', $fcertificaciontxt, 'America/Lima')
				->addMonths($nmes)
				->setDay(1);

			// Carga del archivo adjunto
			$nombreficha = 'CONV' . $cauditoriainspeccion . $fcertificacion->format('Ymd') . '.pdf';
			$ubicacion = '10102/' . $cboclieserv . '/' . $cauditoriainspeccion . '/';
			$carpetas = RUTA_ARCHIVOS . $ubicacion;
			$rutaarchivo = $ubicacion . $nombreficha;
			!is_dir($carpetas) && @mkdir($carpetas, 0777, true);

			$config['upload_path']      = $carpetas;
			$config['file_name']        = $nombreficha;
			$config['allowed_types']    = 'pdf';
			$config['max_size']         = '60048';
			$config['overwrite'] 		= TRUE;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if (!($this->upload->do_upload('mtxtArchivoconvali'))) {
				throw new Exception($this->upload->display_errors());
			}

			$parametros = array(
				'@cauditoriainspeccion' => $cauditoriainspeccion,
				'@fservicio' => $fservicio->format('Y-m-d'),
				'@ccertificadora' => $ccertificadora,
				'@fcertificacion' => $fcertificacion->format('Y-m-d'),
				'@var_fperiodo' => $var_fperiodo->format('Y-m-d'),
				'@presultadocertificadora' => $presultadocertificadora,
				'@ccriterioresultado' => $ccriterioresultado,
				'@cdetallecriterioresultado' => $cdetallecriterioresultado,
				'@dobservacion' => $dobservacion,
				'@dnrodocumento' => $dnrodocumento,
				'@nmes' => $nmes,
				'@ccertificacion' => $ccertificacion,
				'@cusuario' => $cusuario,
				'@ddocumentoconva' => $rutaarchivo,
				'@accion' => $accion,
			);

			$this->mregctrolprov->setconvalidacion($parametros);

			$this->result['status'] = 200;
			$this->result['message'] = 'Inspección convalidada correctamente.';

		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

	public function subirArchivoconval()
	{    // Subir Acrhivo
		$IDPROPU = $this->input->post('mhdnIdPropu');
		$ANIO = substr($this->input->post('mtxtFpropu'), -4);
		$NOMBARCH = 'PROP' . substr($this->input->post('mtxtNropropuesta'), 0, 4) . substr($this->input->post('mtxtNropropuesta'), 5, 4) . 'PTFS';

		$RUTAARCH = 'FTPfileserver/Archivos/10201/' . $ANIO . '/';

		!is_dir($RUTAARCH) && @mkdir($RUTAARCH, 0777, true);

		//RUTA DONDE SE GUARDAN LOS FICHEROS
		$config['upload_path'] = $RUTAARCH;
		$config['allowed_types'] = 'pdf|xlsx|docx|xls|doc';
		$config['max_size'] = '60048';
		$config['overwrite'] = TRUE;
		$config['file_name'] = $NOMBARCH;

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if (!($this->upload->do_upload('mtxtArchivopropu'))) {
			//si al subirse hay algun error 
			$data['uploadError'] = $this->upload->display_errors();
			$error = '';
			return $error;
		} else {
			$data = $this->upload->data();
			$parametros = array(
				'@cauditoriainspeccion' => $IDPROPU,
				'@fservicio' => $IDPROPU,
				'@conval_archivo' => $data['file_name'],
				'@dubicacionfileserver' => $config['upload_path'],
				'@conval_nombarch' => $this->input->post('mtxtNomarchpropu'),
			);
			$retorna = $this->mpropuesta->subirArchivo($parametros);
			echo json_encode($retorna);
		}
	}

	public function actualizar_reg_inspeccion()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {

			$cauditoria = $this->input->post('cauditoria');
			$fservicio = $this->input->post('fservicio');
			$nuevo_estado_insp = $this->input->post('nuevo_estado_insp');

			$pdInspeccion = $this->minspeccion->buscarDauditoria($cauditoria, $fservicio);
			if (empty($pdInspeccion)) {
				throw new Exception('No se pudo encontrar la inspección.');
			}

			// Si no esta en inspector asignado o en proceso no pasara
			// 028 = Asignar fecha y/o inspector
			// 029 = Inspector Asignado
			// 030 = Fecha Asignada
			// 031 = En proceso
			// 032 = Concluido ok
			if ($pdInspeccion->ZCTIPOESTADOSERVICIO != '030' && $pdInspeccion->ZCTIPOESTADOSERVICIO != '029' && $pdInspeccion->ZCTIPOESTADOSERVICIO != '031' && $pdInspeccion->ZCTIPOESTADOSERVICIO != '032') {
				throw new Exception('La inspección no se encuentra en Inspector asignado o En proceso');
			}

			$total = $this->db->select('COUNT(*) as total')
				->from('PDAUDITORIAINSPECCION')
				->where('CAUDITORIAINSPECCION', $cauditoria)
				->where('FSERVICIO >', $fservicio)
				->get()
				->row()->total;
			if ($total > 1) {
				throw new Exception('Solo debe restablecer la última inspección.');
			}

			if ($nuevo_estado_insp == '031') {
				if ($pdInspeccion->ZCTIPOESTADOSERVICIO != '032') {
					throw new Exception('La inspección no tiene el estado correcto para continuar. Solo Conluido Ok');
				}
				$this->db->delete('PDAUDITORIAINSPECCION', ['CAUDITORIAINSPECCION' => $cauditoria, 'FSERVICIO >' => $fservicio]);
				// Se PONE programa en No
				$this->db->update('PDAUDITORIAINSPECCION', [
					'ZCTIPOESTADOSERVICIO' => $nuevo_estado_insp,
					'DINFORMEFINAL_INACTIVO' => $pdInspeccion->DINFORMEFINAL,
					'SCIERRESERVICIO2' => 'A',
					'SSERVICIOREALIZADO' => 'N',
					'sultimo' => 'S',
				], [
					'CAUDITORIAINSPECCION' => $cauditoria,
					'FSERVICIO' => $fservicio,
				]);
			}
			if ($nuevo_estado_insp == '028') {
				// Solo cuando es Fecha asignada o inspector asignado
				if ($pdInspeccion->ZCTIPOESTADOSERVICIO == '029' || $pdInspeccion->ZCTIPOESTADOSERVICIO == '030') {
					// Se restablece a Asignar fecha y/o inspector
					$this->db->update('PDAUDITORIAINSPECCION', [
						'ZCTIPOESTADOSERVICIO' => '028',
					], [
						'CAUDITORIAINSPECCION' => $cauditoria,
						'FSERVICIO' => $fservicio,
					]);
				} else {
					if ($pdInspeccion->ZCTIPOESTADOSERVICIO != '031' && $pdInspeccion->ZCTIPOESTADOSERVICIO != '032') {
						throw new Exception('La inspección no tiene el estado correcto para continuar. . Solo Conluido Ok y En Proceso');
					}
					// Elimina accion correctiva
					$this->db->delete('PACCIONCORRECTIVA', ['CAUDITORIAINSPECCION' => $cauditoria, 'FSERVICIO' => $fservicio]);

					// Peligros de la inspección
					$this->db->delete('PPELIGRO', ['CAUDITORIAINSPECCION' => $cauditoria, 'FSERVICIO' => $fservicio]);

					// Contactos de la inspección
					$this->db->delete('PRESPONSABLEAUDINSP', ['CAUDITORIAINSPECCION' => $cauditoria, 'FSERVICIO' => $fservicio]);

					// Contactos de la inspección
					$this->db->delete('PINFORMACIONADICIONAL', ['CAUDITORIAINSPECCION' => $cauditoria, 'FSERVICIO' => $fservicio]);

					// Eliminar checklist
					$this->db->delete('PVALORCHECKLIST', ['CAUDITORIAINSPECCION' => $cauditoria, 'FSERVICIO' => $fservicio]);

					// Se elimina el resultado en caso tuviese
					$this->db->delete('HRESULTADOAUDINSP', ['CAUDITORIAINSPECCION' => $cauditoria, 'FSERVICIO' => $fservicio]);

					// Se PONE programa en No
					$this->db->update('PDAUDITORIAINSPECCION', [
						'ZCTIPOESTADOSERVICIO' => $nuevo_estado_insp,
						'DUBICACIONFILESERVERAC' => null,
						'FINFORMEFIN' => null,
						'NRESULTADOCHECKLIST' => 0,
						'PRESULTADOCHECKLIST' => null,
						'CDETALLECRITERIORESULTADO' => null,
						'NREALHORAEFECTIVA' => 0,
						'NREALHORANOEFECTIVA' => 0,
						'CUSUARIOMODIFICA' => null,
						'TMODIFICACION' => null,
						'CUBIGEOMUNICIPALIDAD' => null,
						'DLICENCIAFUNCIONAMIENTO' => null,
						'SLICENCIAFUNCIONAMIENTO' => null,
						'DALCANCEINFORME' => null,
						'DMARCAINFORME' => null,
						'DADICIONALLICENCIA' => null,
						'DPERMISOAUTORIDADSANITARIA' => null,
						'CCERTIFICACION' => null,
						'scertificacion' => null,
						'DUBICACIONFILESERVER' => null,
						'SCIERRESERVICIO2' => 'A',
						'SSERVICIOREALIZADO' => 'N',
						'DINFORMEFINAL' => null,
						'DINFORMEFINAL_INACTIVO' => $pdInspeccion->DINFORMEFINAL,
						'sultimo' => 'S',
						'CUSUARIOCIERRE' => null,
						'TCIERRE' => null,
						'FCIERRESERVICIO' => null,
					], [
						'CAUDITORIAINSPECCION' => $cauditoria,
						'FSERVICIO' => $fservicio,
					]);

					// Inspección
					$this->db->update('PPROGRAMAFECHA', [
						'SREALIZADO' => 'N',
						'CUSUARIOMODIFICA' => null,
						'TMODIFICACION' => null,
					], [
						'CAUDITORIAINSPECCION' => $cauditoria,
						'FPROGRAMADASERVICIO' => $fservicio,
					]);
				}
			}

			$this->result['status'] = 200;
			$this->result['message'] = 'Inspección actualizada correctamente.';

		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

}

?>
