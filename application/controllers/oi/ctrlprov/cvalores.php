<?php

/**
 * Class cvalores
 *
 * @property mvalores mvalores
 */
class cvalores extends FS_Controller
{

	/**
	 * cvalores constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('oi/ctrlprov/mvalores', 'mvalores');
	}

	/**
	 * Lista de valores
	 */
	public function lista()
	{
		$busqueda = $this->input->post('busqueda');
		$items = $this->mvalores->lista($busqueda);
		if (!empty($items)) {
			foreach ($items as $key => $item) {
				$items[$key]->detalle = (new mvalores())->listaDetalle($item->cvalor);
			}
		}
		echo json_encode($items);
	}

}
