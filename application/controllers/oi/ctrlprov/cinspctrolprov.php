<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class Cinspctrolprov
 *
 * @property minspctrolprov minspctrolprov
 */
class Cinspctrolprov extends CI_Controller
{
	function __construct()
	{
		parent:: __construct();
		$this->load->model('oi/ctrlprov/minspctrolprov');
		$this->load->model('mglobales');
		$this->load->library('encryption');
		$this->load->helper(array('form', 'url', 'download', 'html', 'file'));
		$this->load->library('form_validation');
	}

	/** CONTROL DE PROVEEDORES **/
	public function getcboclieserv()
	{    // Visualizar Clientes del servicio en CBO

		$resultado = $this->minspctrolprov->getcboclieserv();
		echo json_encode($resultado);
	}

	/**
	 * Visualizar Estado en CBO
	 */
	public function getcboestado()
	{
		$resultado = $this->minspctrolprov->getcboestado();
		echo json_encode($resultado);
	}

	public function getcboinspector()
	{    // Visualizar Inspectores en CBO

		$parametros = array(
			'@sregistro' => $this->input->post('sregistro')
		);
		$resultado = $this->minspctrolprov->getcboinspector($parametros);
		echo json_encode($resultado);
	}

	public function getbuscarctrlprov()
	{    // Busca Control de Proveedores

		$varnull = '';

		$ccia = '1';
		$carea = '01';
		$cservicio = '02';
		$fini = $this->input->post('fdesde');
		$ffin = $this->input->post('fhasta');
		$ccliente = $this->input->post('ccliente');
		$cclienteprov = $this->input->post('dclienteprovmaq');
		$cclientemaq = $this->input->post('cclientemaq');
		$inspector = $this->input->post('inspector');
		$cboestado = $this->input->post('cboestado');

		$parametros = array(
			'@ccia' => $ccia,
			'@carea' => $carea,
			'@cservicio' => $cservicio,
			'@fini' => ($this->input->post('fdesde') == '%') ? NULL : substr($fini, 6, 4) . '-' . substr($fini, 3, 2) . '-' . substr($fini, 0, 2),
			'@ffin' => ($this->input->post('fhasta') == '%') ? NULL : substr($ffin, 6, 4) . '-' . substr($ffin, 3, 2) . '-' . substr($ffin, 0, 2),
			'@ccliente' => $ccliente,
			'@cclienteprov' => (empty($cclienteprov)) ? '%' : "%{$cclienteprov}%",
			'@cclientemaq' => (empty($cclientemaq)) ? '%' : $cclientemaq,
			'@inspector' => (empty($inspector)) ? '' : $inspector,
			'@cusuario' => '%',
			'@estado' => (empty($cboestado)) ? '%' : implode(',', $cboestado),
		);
		$resultado = $this->minspctrolprov->getbuscarctrlprov($parametros);
		echo json_encode($resultado);
	}

	public function getrecuperainsp()
	{    // Visualizar Maquilador por proveedor en CBO

		$parametros = array(
			'@idinspeccion' => $this->input->post('idinspeccion')
		);
		$resultado = $this->mregctrolprov->getrecuperainsp($parametros);
		echo json_encode($resultado);
	}

	public function getcboregestable()
	{    // Visualizar Maquilador por proveedor en CBO

		$parametros = array(
			'@ccliente' => $this->input->post('ccliente'),
			'@cproveedor' => $this->input->post('cproveedor'),
			'@cmaquilador' => $this->input->post('cmaquilador'),
			'@tipo' => $this->input->post('tipo')
		);
		$resultado = $this->mregctrolprov->getcboregestable($parametros);
		echo json_encode($resultado);
	}

	public function getdirestable()
	{    // Visualizar Proveedor por cliente en CBO

		$cestable = $this->input->post('cestablecimiento');
		$resultado = $this->mregctrolprov->getdirestable($cestable);
		echo json_encode($resultado);
	}

	public function getcboareaclie()
	{    // Visualizar Proveedor por cliente en CBO

		$ccliente = $this->input->post('ccliente');
		$resultado = $this->mregctrolprov->getcboareaclie($ccliente);
		echo json_encode($resultado);
	}

	public function getcbolineaproc()
	{    // Visualizar Proveedor por cliente en CBO

		$cestablecimiento = $this->input->post('cestablecimiento');
		$resultado = $this->mregctrolprov->getcbolineaproc($cestablecimiento);
		echo json_encode($resultado);
	}

	public function getcbocontacprinc()
	{    // Visualizar Proveedor por cliente en CBO

		$cproveedor = $this->input->post('cproveedor');
		$resultado = $this->mregctrolprov->getcbocontacprinc($cproveedor);
		echo json_encode($resultado);
	}

	public function getcbotipoestable()
	{    // Visualizar Proveedor por cliente en CBO

		$resultado = $this->mregctrolprov->getcbotipoestable();
		echo json_encode($resultado);
	}

	public function getmontotipoestable()
	{    // Visualizar Proveedor por cliente en CBO

		$ctipoestable = $this->input->post('ctipoestable');
		$resultado = $this->mregctrolprov->getmontotipoestable($ctipoestable);
		echo json_encode($resultado);
	}

	public function setregctrlprov()
	{ // Registrar inspeccion
		$varnull = '';

		$cauditoriainspeccion = $this->input->post('mhdnregIdinsp');
		$ccliente = $this->input->post('cboregClie');
		$cproveedor = $this->input->post('cboregprovclie');
		$cmaquilador = $this->input->post('cboregmaquiprov');
		$cestablecimiento = $this->input->post('cboregestable');
		$clineaprocesocliente = $this->input->post('cboreglineaproc');
		$careacliente = $this->input->post('cboregareaclie');
		$ccontacto = $this->input->post('cbocontacprinc');
		$ctipoestablecimiento = $this->input->post('cbotipoestable');
		$icostobase = $this->input->post('txtcostoestable');
		$dperiodo = $this->input->post('mtxtregPeriodo');
		$accion = $this->input->post('mhdnAccionctrlprov');

		$parametros = array(
			'@cauditoriainspeccion' => $cauditoriainspeccion,
			'@ccliente' => $ccliente,
			'@cproveedor' => $cproveedor,
			'@cmaquilador' => $cmaquilador,
			'@cestablecimiento' => $cestablecimiento,
			'@clineaprocesocliente' => $clineaprocesocliente,
			'@careacliente' => $careacliente,
			'@ccontacto' => $ccontacto,
			'@ctipoestablecimiento' => $ctipoestablecimiento,
			'@icostobase' => $icostobase,
			'@dperiodo' => $dperiodo,
			'@accion' => $accion,
		);
		$resultado = $this->mregctrolprov->setregctrlprov($parametros);
		echo json_encode($resultado);
	}

	public function getcbosistemaip()
	{    // Visualizar Calificacion en CBO

		$resultado = $this->mregctrolprov->getcbosistemaip();
		echo json_encode($resultado);
	}

	public function getcborubroip()
	{    // Visualizar Inspectores en CBO

		$parametros = array(
			'@idnorma' => $this->input->post('cnorma')
		);
		$resultado = $this->mregctrolprov->getcborubroip($parametros);
		echo json_encode($resultado);
	}

	public function getcbochecklist()
	{    // Visualizar Inspectores en CBO

		$parametros = array(
			'@idnorma' => $this->input->post('cnorma'),
			'@idsubnorma' => $this->input->post('csubnorma'),
			'@ccliente' => $this->input->post('ccliente')
		);
		$resultado = $this->mregctrolprov->getcbochecklist($parametros);
		echo json_encode($resultado);
	}

	public function getcbomodinforme()
	{    // Visualizar Calificacion en CBO

		$resultado = $this->mregctrolprov->getcbomodinforme();
		echo json_encode($resultado);
	}

	public function getcboinspvalconf()
	{    // Visualizar Calificacion en CBO

		$resultado = $this->mregctrolprov->getcboinspvalconf();
		echo json_encode($resultado);
	}

	public function getcboinspformula()
	{    // Visualizar Proveedor por cliente en CBO

		$cchecklist = $this->input->post('cchecklist');
		$resultado = $this->mregctrolprov->getcboinspformula($cchecklist);
		echo json_encode($resultado);
	}

	public function getcboinspcritresul()
	{    // Visualizar Calificacion en CBO

		$resultado = $this->mregctrolprov->getcboinspcritresul();
		echo json_encode($resultado);
	}

	public function setreginspeccion()
	{    // Visualizar Inspectores en CBO
		$varnull = '';

		$cauditoriainspeccion = $this->input->post('mtxtidinsp');
		$fservicio = $this->input->post('txtFInsp');
		$cusuarioconsultor = $this->input->post('cboinspinspector');
		$cnorma = $this->input->post('cboinspsistema');
		$csubnorma = $this->input->post('cboinsprubro');
		$cchecklist = $this->input->post('cboinspcchecklist');
		$cvalornoconformidad = $this->input->post('cboinspvalconf');
		$cformulaevaluacion = $this->input->post('cboinspformula');
		$dcometario = $this->input->post('mtxtinspcoment');
		$ccriterioresultado = $this->input->post('cboinspcritresul');
		$cmodeloinforme = $this->input->post('cboinspmodeloinfo');
		$periodo = $this->input->post('cboinspperiodo');
		$zctipoestado = $this->input->post('mhdnzctipoestado');
		$accion = $this->input->post('mhdnAccioninsp');

		$parametros = array(
			'@cauditoriainspeccion' => $cauditoriainspeccion,
			'@fservicio' => ($this->input->post('txtFInsp') == 'Sin txtHallazgo') ? '1900-01-01' : substr($fservicio, 6, 4) . '-' . substr($fservicio, 3, 2) . '-' . substr($fservicio, 0, 2),
			'@cusuarioconsultor' => ($this->input->post('cboinspinspector') == $varnull) ? null : $cusuarioconsultor,
			'@cnorma' => ($this->input->post('cboinspsistema') == $varnull) ? null : $cnorma,
			'@csubnorma' => ($this->input->post('cboinsprubro') == $varnull) ? null : $csubnorma,
			'@cchecklist' => ($this->input->post('cboinspcchecklist') == $varnull) ? null : $cchecklist,
			'@cvalornoconformidad' => ($this->input->post('cboinspvalconf') == $varnull) ? null : $cvalornoconformidad,
			'@cformulaevaluacion' => ($this->input->post('cboinspformula') == $varnull) ? null : $cformulaevaluacion,
			'@dcometario' => $dcometario,
			'@ccriterioresultado' => ($this->input->post('cboinspcritresul') == $varnull) ? null : $ccriterioresultado,
			'@cmodeloinforme' => ($this->input->post('cboinspmodeloinfo') == $varnull) ? null : $cmodeloinforme,
			'@periodo' => $periodo,
			'@zctipoestado' => $zctipoestado,
			'@accion' => $accion
		);
		$resultado = $this->mregctrolprov->setreginspeccion($parametros);
		echo json_encode($resultado);
	}

	public function getcbocierreTipo()
	{    // Visualizar Proveedor por cliente en CBO

		$resultado = $this->mregctrolprov->getcbocierreTipo();
		echo json_encode($resultado);
	}

	public function getlistachecklist()
	{    // Visualizar Inspectores en CBO

		$parametros = array(
			'@cauditoriainspeccion' => $this->input->post('cauditoriainspeccion'),
			'@fechaservicio' => $this->input->post('fservicio')
		);
		$resultado = $this->minspctrolprov->getlistachecklist($parametros);
		echo json_encode($resultado);
	}

	public function cambioestadochecklist()
	{
		$parametros = array(
			'@int_auditoria' => $this->input->post('cauditoriainspeccion'),
			'@dt_servicio' => $this->input->post('fservicio'),
			'@cchecklist' => $this->input->post('cchecklist'),
			'@ls_cgrucrieval' => '', //$this->input->post('ls_cgrucrieval'),
			'@ls_cvalnoconf' => $this->input->post('ls_cvalnoconf'),
			'@str_cod_usuario' => $this->input->post('str_cod_usuario')
		);

		$resultado = $this->minspctrolprov->cambioestadochecklist($parametros);
		echo json_encode($resultado);
	}

	public function insertarChecklist()
	{
		$this->form_validation->set_rules('cmbPuntuacionChecklist', 'idPuntuacion', 'required');
		$this->form_validation->set_rules('int_codigo_checklist', 'idChecklist', 'required');
		$this->form_validation->set_rules('int_requisito_checklist', 'idRequisito', 'required');

		if ($this->form_validation->run() == TRUE) {
			// $accion = $this->input->post('txtAccion');

			$parametros = array(
				'@cmbPuntuacionChecklist' => $this->input->post('cmbPuntuacionChecklist'),
				'@cmbCriterioHallazgo' => $this->input->post('cmbCriterioHallazgo'),
				'@txtHallazgo' => $this->input->post('txtHallazgo'),
				'@int_codigo_checklist' => $this->input->post('int_codigo_checklist'),
				'@int_requisito_checklist' => $this->input->post('int_requisito_checklist'),
				'@int_auditoria_inspeccion' => $this->input->post('int_auditoria_inspeccion')
			);

			// if ($accion == 'I') {
			$response = $this->minspctrolprov->insertarChecklist($parametros);
			// } else if($accion == 'U') {
			//     $response = $this->mhomologaciones->updateRequisitoProducto($parametros);
			// }

			echo json_encode($response);

		} else {
			$response = 2;
			echo json_encode($response);
		}

	}

	/**
	 * Vista de la inspeccion
	 * @param int $cauditoria
	 * @param string $fservicio
	 */
	public function inspeccion()
	{
		$cauditoria = $this->input->get('cauditoria');
		$fservicio = $this->input->get('fservicio');

		if (empty($cauditoria) || empty($fservicio)) {
			show_404();
		}

		$inspeccion = $this->minspctrolprov->buscar($cauditoria, $fservicio);
		if (empty($inspeccion)) {
			show_404();
		}

		$resumen = $this->minspctrolprov->obtenerResumen($cauditoria, $fservicio);
		$contactos = $this->minspctrolprov->obtenerContactos($cauditoria, $fservicio);
		$certificaciones = $this->minspctrolprov->obtenerCertificaciones();

		$this->layout->js([
			versionar_archivo('script/at/ctrlprov/inspctrolprov/checklist.js'),
			versionar_archivo('script/at/ctrlprov/inspctrolprov/accion_correctiva.js'),
		]);

		$this->parser->parse('seguridad/vprincipal', [
			'vista' => 'Ventana',
			'content_for_layout' => 'at/ctrlprov/inspeccion/vinspeccion',
			'ccia' => 1,
			'datos_ventana' => [
				'cauditoria' => $cauditoria,
				'fservicio' => $fservicio,
				'inspeccion' => $inspeccion,
				'resumen' => $resumen,
				'contactos' => $contactos,
				'certificaciones' => $certificaciones,
			]
		]);
	}

}

?>
