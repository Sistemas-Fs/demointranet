<?php

/**
 * Class cinspctrolprovacc
 *
 * @property minspctrolprovacc minspctrolprovacc
 */
class cinspctrolprovacc extends FS_Controller
{


	/**
	 * cinspctrolprovacc constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('at/ctrlprov/minspctrolprovacc');
	}

	public function lista()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		$parametros = array(
			'@CAUDITORIA' => $this->input->post('cauditoriainspeccion'),
			'@FSERVICIO' => $this->input->post('fservicio')
		);
		$resultado = $this->minspctrolprovacc->lista($parametros);
		echo json_encode($resultado);
	}

	public function buscar()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		$parametros = array(
			'@cauditoria' => $this->input->post('cauditoria'),
			'@fservicio' => $this->input->post('fservicio'),
			'@cchecklist' => $this->input->post('cchecklist'),
			'@crequisitochecklist' => $this->input->post('crequisitochecklist'),
		);
		$resultado = $this->minspctrolprovacc->buscar($parametros);
		echo json_encode($resultado);
	}

	/**
	 * Guarda el acc
	 */
	public function guardar()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {

			$acc_modal_cchecklist_id = $this->input->post('acc_modal_cchecklist_id');
			$acc_modal_requisito_cchecklist_id = $this->input->post('acc_modal_requisito_cchecklist_id');
			$acc_modal_requisito = $this->input->post('acc_modal_requisito');
			$acc_modal_hallazgo = $this->input->post('acc_modal_hallazgo');
			$acc_modal_acc = $this->input->post('acc_modal_acc');
			$acc_modal_acepta_acc = $this->input->post('acc_modal_acepta_acc');
			$acc_modal_tipo_hallazgo = $this->input->post('acc_modal_tipo_hallazgo');
			$acc_modal_fecha = $this->input->post('acc_modal_fecha');

		} catch (Exception $ex) {
			$this->result['mnessage'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

}
