<?php

/**
 * Class cinvolucrados
 *
 * @property minvolucrados minvolucrados
 */
class cinvolucrados extends FS_Controller
{

	/**
	 * cinvolucrados constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('oi/ctrlprov/inspctrolprov/minvolucrados', 'minvolucrados');
	}

	/**
	 * Lista de tipo de los involucrados
	 */
	public function lista_tipo()
	{
		$items = $this->minvolucrados->listaTipo();
		echo json_encode($items);
	}

	/**
	 * Lista de reuniones a la inspecciones
	 */
	public function lista_reuniones()
	{
		$cauditoria = $this->input->post('cauditoria');
		$fservicio = $this->input->post('fservicio');
		$items = $this->minvolucrados->listaReuniones($cauditoria, $fservicio);
		echo json_encode($items);
	}

	public function guardar_reunion()
	{
		try {
			$cauditoria = $this->input->post('cauditoria');
			$fservicio = $this->input->post('fservicio');
			$zctiporeunion = $this->input->post('zctiporeunion');
			$reunion = $this->minvolucrados->listaCReuniones($cauditoria, $fservicio, $zctiporeunion);
			if (!empty($reunion)) {
				throw new Exception('Ya existe el tipo de reunion.');
			}
			$this->db->insert('pcreunionaudinsp', [
				'CAUDITORIAINSPECCION' => $cauditoria,
				'FSERVICIO' => $fservicio,
				'ZCTIPOREUNION' => $zctiporeunion,
				'FREUNION' => date('Y-m-d H:i:s'),
				'HINICIOREUNION' => null,
				'HFINREUNION' => null,
				'CUSUARIOCREA' => $this->session->userdata('s_cusuario'),
				'TCREACION' => date('Y-m-d H:i:s'),
				'CUSUARIOMODIFICA' => null,
				'TMODIFICACION' => null,
				'SREGISTRO' => 'A',
			]);
			$this->result['status'] = 200;
			$this->result['message'] = 'Reunion registrado correctamente.';
		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

	public function eliminar_reunion()
	{
		try {
			$cauditoria = $this->input->post('cauditoria');
			$fservicio = $this->input->post('fservicio');
			$zctiporeunion = $this->input->post('zctiporeunion');
			$reunion = $this->minvolucrados->listaCReuniones($cauditoria, $fservicio, $zctiporeunion);
			if (empty($reunion)) {
				throw new Exception('No existe el tipo de reunion.');
			}
			$this->db->delete('pcreunionaudinsp', [
				'CAUDITORIAINSPECCION' => $cauditoria,
				'FSERVICIO' => $fservicio,
				'ZCTIPOREUNION' => $zctiporeunion
			]);
			$this->db->delete('pdreunionaudinsp', [
				'CAUDITORIAINSPECCION' => $cauditoria,
				'FSERVICIO' => $fservicio,
				'ZCTIPOREUNION' => $zctiporeunion
			]);
			$this->result['status'] = 200;
			$this->result['message'] = 'Reunion eliminado correctamente.';
		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

	public function lista()
	{
		$cauditoria = $this->input->post('cauditoria');
		$fservicio = $this->input->post('fservicio');
		$zctiporeunion = $this->input->post('zctiporeunion');
		$resultado = $this->minvolucrados->listaReuniones($cauditoria, $fservicio);
		if (!empty($resultado)) {
			foreach ($resultado as $key => $item) {
				$resultado[$key]->asistentes = (new minvolucrados())->listaDReuniones($cauditoria, $fservicio, $item->ZCTIPOREUNION);
			}
		}
		$asistente = $this->minvolucrados->listaDReuniones($cauditoria, $fservicio, $zctiporeunion);
		echo json_encode([
			'resultado' => $resultado,
			'asistente' => $asistente,
		]);
	}

	public function guardar_asistente()
	{
		try {
			$cauditoria = $this->input->post('cauditoria');
			$fservicio = $this->input->post('fservicio');
			$zctiporeunion = $this->input->post('zctiporeunion');
			$nombres = $this->input->post('nombres');
			$cargo = $this->input->post('cargo');

			$nombres = trim($nombres);
			$cargo = trim($cargo);
			if (empty($nombres) || empty($cargo)) {
				throw new Exception('Debes ingresar el nombre y cargo.');
			}

			$nasistente = $this->minvolucrados->maxAsistente($cauditoria, $fservicio, $zctiporeunion);
			$nasistente = $nasistente + 1;

			$this->db->insert('pdreunionaudinsp', [
				'CAUDITORIAINSPECCION' => $cauditoria,
				'FSERVICIO' => $fservicio,
				'ZCTIPOREUNION' => $zctiporeunion,
				'NASISTENTE' => $nasistente,
				'DNOMBREASISTENTE' => $nombres,
				'DCARGOASISTENTE' => $cargo,
				'CUSUARIOCREA' => $this->session->userdata('s_cusuario'),
				'TCREACION' => date('Y-m-d H:i:s'),
				'CUSUARIOMODIFICA' => null,
				'TMODIFICACION' => null,
				'SREGISTRO' => 'A',
			]);

			$this->result['status'] = 200;
			$this->result['message'] = 'Asistente creado correctamente.';
		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

	public function eliminar_asistente()
	{
		try {
			$cauditoria = $this->input->post('cauditoria');
			$fservicio = $this->input->post('fservicio');
			$zctiporeunion = $this->input->post('zctiporeunion');
			$nasistente = $this->input->post('nasistente');
			$this->db->delete('pdreunionaudinsp', [
				'CAUDITORIAINSPECCION' => $cauditoria,
				'FSERVICIO' => $fservicio,
				'ZCTIPOREUNION' => $zctiporeunion,
				'NASISTENTE' => $nasistente,
			]);
			$this->result['status'] = 200;
			$this->result['message'] = 'Asistente eliminado correctamente.';
		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

}
