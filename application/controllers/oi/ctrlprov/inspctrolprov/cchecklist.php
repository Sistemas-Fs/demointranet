<?php

/**
 * Class cchecklist
 *
 * @property mchecklist mchecklist
 * @property maccion_correctiva maccion_correctiva
 * @property minspeccion minspeccion
 */
class cchecklist extends FS_Controller
{


	/**
	 * cchecklist constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('oi/ctrlprov/inspctrolprov/mchecklist');
		$this->load->model('oi/ctrlprov/inspctrolprov/maccion_correctiva');
		$this->load->model('oi/ctrlprov/inspctrolprov/minspeccion');
	}

	/**
	 * Lista del checklist de la inspeccion
	 */
	public function lista()
	{
		$parametros = array(
			'@cauditoriainspeccion' => $this->input->post('cauditoriainspeccion'),
			'@fechaservicio' => $this->input->post('fservicio')
		);
		$resultado = $this->mchecklist->lista($parametros);
		echo json_encode($resultado);
	}

	/**
	 * Se obtiene los valores del checklist
	 */
	public function valores()
	{
		$resultado = $this->mchecklist->listaValores(['@int_valor' => $this->input->post('int_valor')]);
		echo json_encode($resultado);
	}

	/**
	 * Lista de criterios de hallazgos
	 */
	public function criterioHallazgo()
	{
		$cauditoria = $this->input->post('cauditoria');
		$fservicio = $this->input->post('fservicio');
		$resultado = $this->mchecklist->listaCriterioHallazgos($cauditoria, $fservicio);
		echo json_encode($resultado);
	}

	/**
	 * Guarda el checklist de la inspeccion
	 */
	public function guardar()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {

			$cmbPuntuacionChecklist = $this->input->post('cmbPuntuacionChecklist');
			$cmbCriterioHallazgo = $this->input->post('cmbCriterioHallazgo');
			$txtHallazgo = $this->input->post('txtHallazgo');
			$int_codigo_checklist = $this->input->post('int_codigo_checklist'); // cchecklist
			$int_requisito_checklist = $this->input->post('int_requisito_checklist'); // crequisitochecklist
			$int_auditoria_inspeccion = $this->input->post('int_auditoria_inspeccion');
			$cauditoria = $this->input->post('cauditoria');
			$fservicio = $this->input->post('fservicio');
			$criterio_hallazgo_valor = $this->input->post('criterio_hallazgo_valor');
			$txtHallazgo = trim($txtHallazgo);

			$inspeccion = $this->minspeccion->buscarDauditoria($cauditoria, $fservicio);
			if (empty($inspeccion)) {
				throw new Exception('La inspección no pudo ser encontrado.');
			}

			if (empty($int_codigo_checklist) || empty($int_requisito_checklist) || empty($int_auditoria_inspeccion)) {
				throw new Exception('Falta de parametros para continuar.');
			}
			if (empty($cmbPuntuacionChecklist)) {
				throw new Exception('Debes elegir una puntuación.');
			}

			if (empty($this->session->userdata('s_cusuario'))) {
				throw new Exception('Debes volver a iniciar sesión.');
			}

			$checklist = $this->mchecklist->buscar($cauditoria, $fservicio, $int_codigo_checklist, $int_requisito_checklist);
			if (empty($checklist)) {
				throw new Exception('El checklist no pudo ser encontrado.');
			}

			// Valor actual
			$valorActual = (new mchecklist())->buscarValor($checklist->CVALORREQUISITO, $checklist->CDETALLEVALORREQUISITO);
			if (empty($valorActual)) {
				throw new Exception('No pudo encontrar el valor actual del checklist');
			}
			// Valor nuevo
			$valorNuevo = (new mchecklist())->buscarValor($checklist->CVALORREQUISITO, $cmbPuntuacionChecklist);
			if (empty($valorNuevo)) {
				throw new Exception('No pudo encontrar el nuevo valor del checklist');
			}
			// Solo cuando cambia de valor
			if ($valorNuevo->NDETALLEVALOR < $valorActual->NDETALLEVALOR) {
				if (empty($cmbCriterioHallazgo) || empty($txtHallazgo)) {
					throw new Exception('Debes elegir un criterio y detallar el hallazgo.');
				}
			}

			$this->db->trans_begin();

			$response = $this->mchecklist->guardar([
				'@cmbPuntuacionChecklist' => $cmbPuntuacionChecklist,
				'@cmbCriterioHallazgo' => $cmbCriterioHallazgo,
				'@txtHallazgo' => $txtHallazgo,
				'@int_codigo_checklist' => $int_codigo_checklist,
				'@int_requisito_checklist' => $int_requisito_checklist,
				'@int_auditoria_inspeccion' => $int_auditoria_inspeccion,
				'@CVALORREQUISITO' => $checklist->CVALORREQUISITO,
			]);

			if (!$response) {
				throw new Exception('Error en el proceso de ejecución.');
			}

			// Se crea la acción correctiva
			if ($criterio_hallazgo_valor) {
				$this->maccion_correctiva->guardar(
					$cauditoria,
					$fservicio,
					$int_codigo_checklist,
					$int_requisito_checklist,
					'',
					null,
					'N',
					null,
					$this->session->userdata('s_cusuario'),
					'A',
					null,
					date('Y-m-d h:i:s'),
					''
				);
			}

			// Actualizacion del resultado checklist
			$resultado = $this->mchecklist->lista([
				'@cauditoriainspeccion' => $cauditoria,
				'@fechaservicio' => $fservicio
			]);

			$totalValorMax = 0;
			$totalNoConformidad = 0;
			$totalExcluyentes = 0;
			if (!empty($resultado)) {
				foreach ($resultado as $key => $value) {
					if ($value->srequisito == 'H') {
						$nValorMax = (isset($value->nvalormaxrequisito)) ? floatval($value->nvalormaxrequisito) : null;
						$noconformidad = (isset($value->nvalorrequisito) && !empty($value->nvalorrequisito)) ? floatval($value->nvalorrequisito) : -1000;
						if (!empty($nValorMax)) {
							// Cuando es NA, NE o SC es cero
							if ($value->cdetallevalorrequisito === "004" || $value->cdetallevalorrequisito === "005" || $value->cdetallevalorrequisito === "006") {
								$nValorMax = 0;
							}
							$totalValorMax += $nValorMax;
							if ($noconformidad >= 0) {
								$totalNoConformidad += $noconformidad;
							}
						}
						// Solo si es un exluyente con monto cero se le considera para bajarle el nivel
						if (strtoupper($value->sexcluyente) == 'S' && $noconformidad == 0) {
							++$totalExcluyentes;
						}
					}
				}
			}

			$nvalorResultado = ($totalValorMax > 0) ? round(($totalNoConformidad * 100) / $totalValorMax, 2) : 0;
			$CDETALLECRITERIORESULTADO = null;
			$DDETALLECRITERIORESULTADO = '';
			$CDETALLECRITERIORESULTADO_MAYOR = 0;

			// Buscar detalle resultado
			$detalleResultado = $this->mchecklist->buscarDetalleResultado($inspeccion->CCRITERIORESULTADO);
			if (!empty($detalleResultado)) {
				foreach ($detalleResultado as $key => $resultado) {
					$nvalorInicial = floatval($resultado->NVALORINICIAL);
					$nvalorfinal = floatval($resultado->NVALORFINAL);
					// Solo si cumple con el valor
					if ($nvalorResultado >= $nvalorInicial && $nvalorResultado <= $nvalorfinal) {
						$CDETALLECRITERIORESULTADO = $resultado->CDETALLECRITERIORESULTADO;
						$DDETALLECRITERIORESULTADO = $resultado->DDETALLECRITERIORESULTADO;
					}
					// Se almacena el mayor ID
					if ($CDETALLECRITERIORESULTADO_MAYOR < $resultado->CDETALLECRITERIORESULTADO) {
						$CDETALLECRITERIORESULTADO_MAYOR = $resultado->CDETALLECRITERIORESULTADO;
					}
				}
			}

			// Se actualiza el criterio de resultado encontrado
			if (!empty($CDETALLECRITERIORESULTADO)) {
				// Se bajara de nivel en caso tenga excluyentes
				if ($totalExcluyentes > 0) {
					$CDETALLECRITERIORESULTADO = $CDETALLECRITERIORESULTADO + $totalExcluyentes;
					// Solo si supera el mayor rango se toma el rango mayor encontrado en el resultado
					if ($CDETALLECRITERIORESULTADO > $CDETALLECRITERIORESULTADO_MAYOR) {
						$CDETALLECRITERIORESULTADO = $CDETALLECRITERIORESULTADO_MAYOR;
					}
					// Se busca el criterio de resultado y se cambia a la minima expresion
					$DETALLECRITERIORESULTADO = $this->mchecklist->buscarDetalleResultado($inspeccion->CCRITERIORESULTADO, $CDETALLECRITERIORESULTADO);
					$nvalorResultado = $DETALLECRITERIORESULTADO[0]->NVALORFINAL;
					$DDETALLECRITERIORESULTADO = $DETALLECRITERIORESULTADO[0]->DDETALLECRITERIORESULTADO;
				}
				$this->db->update('PDAUDITORIAINSPECCION', [
					'NRESULTADOCHECKLIST' => $nvalorResultado,
					'PRESULTADOCHECKLIST' => $nvalorResultado,
					'CDETALLECRITERIORESULTADO' => $CDETALLECRITERIORESULTADO,
				], [
					'CAUDITORIAINSPECCION' => $cauditoria,
					'FSERVICIO' => $fservicio,
				]);
			}

			$this->db->query('CALL sp_formula_insprov_valmax_01(?,?,?)', [
				'@AS_CAUDI' => $cauditoria,
				'@AD_FECHA' => $fservicio,
				'@AS_CCHECKLIST' => $inspeccion->CCHECKLIST,
			]);

			$this->db->query('CALL sp_formula_insprov_valobtenido_01(?,?,?)', [
				'@AS_CAUDI' => $cauditoria,
				'@AD_FECHA' => $fservicio,
				'@AS_CCHECKLIST' => $inspeccion->CCHECKLIST,
			]);

			$this->db->trans_commit();

			$this->result['status'] = 200;
			$this->result['message'] = 'CheckList guardado correctamente.';
			$this->result['data']['resultado_porcentaje'] = $nvalorResultado;
			$this->result['data']['resultado'] = $DDETALLECRITERIORESULTADO;

		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
			$this->db->trans_rollback();
		}

		responseResult($this->result);
	}

	public function descargar($cauditoria, $fservicio)
	{
		$parametros = array(
			'@cauditoriainspeccion' => $cauditoria,
			'@fechaservicio' => $fservicio
		);
		$resultado = $this->mchecklist->lista($parametros);
		if (empty($resultado)) {
			show_404();
		}

		$spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setTitle('Check list descargado');

		$spreadsheet->getDefaultStyle()
			->getFont()
			->setName('Arial')
			->setSize(11);

		$sheet->setCellValue('A1', 'N°');
		$sheet->setCellValue('B1', 'Requisito');
		$sheet->setCellValue('C1', 'Valor maximo');
		$sheet->setCellValue('D1', 'Valor obtenido');
		$sheet->setCellValue('E1', '% de Conformidad');
		$sheet->setCellValue('F1', 'Criterio de Hallazgo');
		$sheet->setCellValue('G1', 'Hallazgo');

		$titulo = [
			'font' => [
				'name' => 'Arial',
				'size' => 11,
				'color' => array('rgb' => '000000'),
				'bold' => true,
			],
		];

		$cabeceraTabla = [
			'font' => [
				'name' => 'Arial',
				'size' => 11,
				'color' => array('rgb' => 'FFFFFF'),
				'bold' => true,
			],
			'fill' => [
				'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
				'startColor' => [
					'rgb' => '29B037'
				]
			],
			'borders' => [
				'allBorders' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => [
						'rgb' => '000000'
					]
				]
			],
			'alignment' => [
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'wrapText' => true,
			]
		];
		$sheet->getStyle('A1:G1')->applyFromArray($cabeceraTabla);
		$sheet->getStyle('A1')->applyFromArray($titulo);
		$sheet->getStyle('B1')->applyFromArray($titulo);
		$sheet->getStyle('C1')->applyFromArray($titulo);
		$sheet->getStyle('D1')->applyFromArray($titulo);
		$sheet->getStyle('E1')->applyFromArray($titulo);
		$sheet->getStyle('F1')->applyFromArray($titulo);
		$sheet->getStyle('G1')->applyFromArray($titulo);

		$item = 2;
		foreach ($resultado as $key => $value) {
			$nvalormaxrequisito = (!empty($value->nvalormaxrequisito)) ? intval($value->nvalormaxrequisito) : '';
			$nvalorrequisito = (!empty($value->nvalorrequisito)) ? intval($value->nvalorrequisito) : '';
			if ($nvalorrequisito < 0) {
				$nvalorrequisito = $value->noconformidad;
			}
			$tipohallazgo = (!empty($value->tipohallazgo)) ? $value->tipohallazgo : '';
			$dhallazgotext = (!empty($value->dhallazgotext)) ? $value->dhallazgotext : '';
			$pvalorrequisito = (!empty($value->pvalorrequisito)) ? $value->pvalorrequisito : 0;
			$sheet->setCellValue('A' . $item, $value->dnumerador);
			$sheet->setCellValue('B' . $item, $value->drequisito);
			$sheet->setCellValue('C' . $item, $nvalormaxrequisito);
			$sheet->setCellValue('D' . $item, $nvalorrequisito);
			$sheet->setCellValue('E' . $item, $pvalorrequisito);
			$sheet->setCellValue('F' . $item, $tipohallazgo);
			$sheet->setCellValue('G' . $item, $dhallazgotext);
			$sheet->getStyle('B' . $item)->getAlignment()->setWrapText(true);
			$sheet->getStyle('G' . $item)->getAlignment()->setWrapText(true);
			$item++;
		}

		foreach (range('A', 'G') as $column) {
			switch($column)
			{
				case "B":
					$sheet->getColumnDimension($column)->setWidth(100);
					break;
				case "G":
					$sheet->getColumnDimension($column)->setWidth(140);
					break;
				default:
					$sheet->getColumnDimension($column)->setAutoSize(true);
					break;
			}
		}

		$filename = 'checklist-' . $cauditoria . '-' . date('Ymd', strtotime($fservicio));
		$writer = new PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="'. urlencode($filename . '.xlsx').'"');
		$writer->save('php://output');
	}

}
