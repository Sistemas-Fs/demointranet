<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class cinspeccion
 *
 * @property minspeccion minspeccion
 * @property maccion_correctiva maccion_correctiva
 * @property mpeligros mpeligros
 */
class cinspeccion extends FS_Controller
{
	function __construct()
	{
		parent:: __construct();
		$this->load->model('oi/ctrlprov/inspctrolprov/minspeccion');
		$this->load->model('mglobales');
		$this->load->model('oi/ctrlprov/inspctrolprov/maccion_correctiva');
		$this->load->model('oi/ctrlprov/inspctrolprov/mpeligros');
		$this->load->library('encryption');
		$this->load->helper(array('form', 'url', 'download', 'html', 'file'));
		$this->load->library('form_validation');
	}

	/** CONTROL DE PROVEEDORES **/
	public function getcboclieserv()
	{    // Visualizar Clientes del servicio en CBO
		$resultado = $this->minspeccion->getcboclieserv();
		echo json_encode($resultado);
	}

	/**
	 * Visualizar Estado en CBO
	 */
	public function getcboestado()
	{
		$resultado = $this->minspeccion->getcboestado();
		echo json_encode($resultado);
	}

	public function getcboinspector()
	{    // Visualizar Inspectores en CBO

		$parametros = array(
			'@sregistro' => $this->input->post('sregistro')
		);
		$resultado = $this->minspeccion->getcboinspector($parametros);
		echo json_encode($resultado);
	}

	public function getbuscarctrlprov()
	{    // Busca Control de Proveedores

		$varnull = '';

		$ccia = '2';
		$carea = '01';
		$cservicio = '01';
		$fini = $this->input->post('fdesde');
		$ffin = $this->input->post('fhasta');
		$ccliente = $this->input->post('ccliente');
		$cclienteprov = trim($this->input->post('dclienteprovmaq'));
		$cclientemaq = trim($this->input->post('cclientemaq'));
		$inspector = $this->session->userdata('s_cusuario');
		$cboestado = $this->input->post('cboestado');
		$speligro = $this->input->post('speligro');

		$parametros = array(
			'@ccia' => $ccia,
			'@carea' => $carea,
			'@cservicio' => $cservicio,
			'@fini' => ($this->input->post('fdesde') == '%') ? NULL : substr($fini, 6, 4) . '-' . substr($fini, 3, 2) . '-' . substr($fini, 0, 2),
			'@ffin' => ($this->input->post('fhasta') == '%') ? NULL : substr($ffin, 6, 4) . '-' . substr($ffin, 3, 2) . '-' . substr($ffin, 0, 2),
			'@ccliente' => $ccliente,
			'@cclienteprov' => (empty($cclienteprov)) ? '%' : "%{$cclienteprov}%",
			'@cclientemaq' => (empty($cclientemaq)) ? '%' : $cclientemaq,
			'@inspector' => $this->session->userdata('s_cusuario'),
			'@cusuario' => '%',
			'@estado' => (empty($cboestado)) ? '%' : implode(',', $cboestado),
			'@speligro' => (empty($speligro)) ? '%' : $speligro,
			'@codigo' => '%',
		);
		$resultado = $this->minspeccion->getbuscarctrlprov($parametros);
		echo json_encode($resultado);
	}

	public function getbuscarctrlprov_historico()
	{
		$cauditoria = $this->input->post('cauditoria');
		$fservicio = $this->input->post('fservicio');

		$parametros = array(
			'@CAUDITORIA' => $cauditoria,
			'@FSERVICIO' => $fservicio,
		);
		$resultado = $this->minspeccion->getbuscarctrlprovHistorico($parametros);
		echo json_encode($resultado);
	}

	public function getrecuperainsp()
	{    // Visualizar Maquilador por proveedor en CBO

		$parametros = array(
			'@idinspeccion' => $this->input->post('idinspeccion')
		);
		$resultado = $this->mregctrolprov->getrecuperainsp($parametros);
		echo json_encode($resultado);
	}

	public function getcboregestable()
	{    // Visualizar Maquilador por proveedor en CBO

		$parametros = array(
			'@ccliente' => $this->input->post('ccliente'),
			'@cproveedor' => $this->input->post('cproveedor'),
			'@cmaquilador' => $this->input->post('cmaquilador'),
			'@tipo' => $this->input->post('tipo')
		);
		$resultado = $this->mregctrolprov->getcboregestable($parametros);
		echo json_encode($resultado);
	}

	public function getdirestable()
	{    // Visualizar Proveedor por cliente en CBO

		$cestable = $this->input->post('cestablecimiento');
		$resultado = $this->mregctrolprov->getdirestable($cestable);
		echo json_encode($resultado);
	}

	public function getcboareaclie()
	{    // Visualizar Proveedor por cliente en CBO

		$ccliente = $this->input->post('ccliente');
		$resultado = $this->mregctrolprov->getcboareaclie($ccliente);
		echo json_encode($resultado);
	}

	public function getcbolineaproc()
	{    // Visualizar Proveedor por cliente en CBO

		$cestablecimiento = $this->input->post('cestablecimiento');
		$resultado = $this->mregctrolprov->getcbolineaproc($cestablecimiento);
		echo json_encode($resultado);
	}

	public function getcbocontacprinc()
	{    // Visualizar Proveedor por cliente en CBO

		$cproveedor = $this->input->post('cproveedor');
		$resultado = $this->mregctrolprov->getcbocontacprinc($cproveedor);
		echo json_encode($resultado);
	}

	public function getcbotipoestable()
	{    // Visualizar Proveedor por cliente en CBO

		$resultado = $this->mregctrolprov->getcbotipoestable();
		echo json_encode($resultado);
	}

	public function getmontotipoestable()
	{    // Visualizar Proveedor por cliente en CBO

		$ctipoestable = $this->input->post('ctipoestable');
		$resultado = $this->mregctrolprov->getmontotipoestable($ctipoestable);
		echo json_encode($resultado);
	}

	public function setregctrlprov()
	{ // Registrar inspeccion
		$varnull = '';

		$cauditoriainspeccion = $this->input->post('mhdnregIdinsp');
		$ccliente = $this->input->post('cboregClie');
		$cproveedor = $this->input->post('cboregprovclie');
		$cmaquilador = $this->input->post('cboregmaquiprov');
		$cestablecimiento = $this->input->post('cboregestable');
		$clineaprocesocliente = $this->input->post('cboreglineaproc');
		$careacliente = $this->input->post('cboregareaclie');
		$ccontacto = $this->input->post('cbocontacprinc');
		$ctipoestablecimiento = $this->input->post('cbotipoestable');
		$icostobase = $this->input->post('txtcostoestable');
		$dperiodo = $this->input->post('mtxtregPeriodo');
		$accion = $this->input->post('mhdnAccionctrlprov');

		$parametros = array(
			'@cauditoriainspeccion' => $cauditoriainspeccion,
			'@ccliente' => $ccliente,
			'@cproveedor' => $cproveedor,
			'@cmaquilador' => $cmaquilador,
			'@cestablecimiento' => $cestablecimiento,
			'@clineaprocesocliente' => $clineaprocesocliente,
			'@careacliente' => $careacliente,
			'@ccontacto' => $ccontacto,
			'@ctipoestablecimiento' => $ctipoestablecimiento,
			'@icostobase' => $icostobase,
			'@dperiodo' => $dperiodo,
			'@accion' => $accion,
		);
		$resultado = $this->mregctrolprov->setregctrlprov($parametros);
		echo json_encode($resultado);
	}

	public function getcbosistemaip()
	{    // Visualizar Calificacion en CBO

		$resultado = $this->mregctrolprov->getcbosistemaip();
		echo json_encode($resultado);
	}

	public function getcborubroip()
	{    // Visualizar Inspectores en CBO

		$parametros = array(
			'@idnorma' => $this->input->post('cnorma')
		);
		$resultado = $this->mregctrolprov->getcborubroip($parametros);
		echo json_encode($resultado);
	}

	public function getcbochecklist()
	{    // Visualizar Inspectores en CBO

		$parametros = array(
			'@idnorma' => $this->input->post('cnorma'),
			'@idsubnorma' => $this->input->post('csubnorma'),
			'@ccliente' => $this->input->post('ccliente')
		);
		$resultado = $this->mregctrolprov->getcbochecklist($parametros);
		echo json_encode($resultado);
	}

	public function getcbomodinforme()
	{    // Visualizar Calificacion en CBO

		$resultado = $this->mregctrolprov->getcbomodinforme();
		echo json_encode($resultado);
	}

	public function getcboinspvalconf()
	{    // Visualizar Calificacion en CBO

		$resultado = $this->mregctrolprov->getcboinspvalconf();
		echo json_encode($resultado);
	}

	public function getcboinspformula()
	{    // Visualizar Proveedor por cliente en CBO

		$cchecklist = $this->input->post('cchecklist');
		$resultado = $this->mregctrolprov->getcboinspformula($cchecklist);
		echo json_encode($resultado);
	}

	public function getcboinspcritresul()
	{    // Visualizar Calificacion en CBO

		$resultado = $this->mregctrolprov->getcboinspcritresul();
		echo json_encode($resultado);
	}

	public function setreginspeccion()
	{    // Visualizar Inspectores en CBO
		$varnull = '';

		$cauditoriainspeccion = $this->input->post('mtxtidinsp');
		$fservicio = $this->input->post('txtFInsp');
		$cusuarioconsultor = $this->input->post('cboinspinspector');
		$cnorma = $this->input->post('cboinspsistema');
		$csubnorma = $this->input->post('cboinsprubro');
		$cchecklist = $this->input->post('cboinspcchecklist');
		$cvalornoconformidad = $this->input->post('cboinspvalconf');
		$cformulaevaluacion = $this->input->post('cboinspformula');
		$dcometario = $this->input->post('mtxtinspcoment');
		$ccriterioresultado = $this->input->post('cboinspcritresul');
		$cmodeloinforme = $this->input->post('cboinspmodeloinfo');
		$periodo = $this->input->post('cboinspperiodo');
		$zctipoestado = $this->input->post('mhdnzctipoestado');
		$accion = $this->input->post('mhdnAccioninsp');

		$parametros = array(
			'@cauditoriainspeccion' => $cauditoriainspeccion,
			'@fservicio' => ($this->input->post('txtFInsp') == 'Sin txtHallazgo') ? '1900-01-01' : substr($fservicio, 6, 4) . '-' . substr($fservicio, 3, 2) . '-' . substr($fservicio, 0, 2),
			'@cusuarioconsultor' => ($this->input->post('cboinspinspector') == $varnull) ? null : $cusuarioconsultor,
			'@cnorma' => ($this->input->post('cboinspsistema') == $varnull) ? null : $cnorma,
			'@csubnorma' => ($this->input->post('cboinsprubro') == $varnull) ? null : $csubnorma,
			'@cchecklist' => ($this->input->post('cboinspcchecklist') == $varnull) ? null : $cchecklist,
			'@cvalornoconformidad' => ($this->input->post('cboinspvalconf') == $varnull) ? null : $cvalornoconformidad,
			'@cformulaevaluacion' => ($this->input->post('cboinspformula') == $varnull) ? null : $cformulaevaluacion,
			'@dcometario' => $dcometario,
			'@ccriterioresultado' => ($this->input->post('cboinspcritresul') == $varnull) ? null : $ccriterioresultado,
			'@cmodeloinforme' => ($this->input->post('cboinspmodeloinfo') == $varnull) ? null : $cmodeloinforme,
			'@periodo' => $periodo,
			'@zctipoestado' => $zctipoestado,
			'@accion' => $accion
		);
		$resultado = $this->mregctrolprov->setreginspeccion($parametros);
		echo json_encode($resultado);
	}

	public function getcbocierreTipo()
	{    // Visualizar Proveedor por cliente en CBO

		$resultado = $this->mregctrolprov->getcbocierreTipo();
		echo json_encode($resultado);
	}

	public function cambioestadochecklist()
	{
		$cauditoria = $this->input->post('cauditoriainspeccion');
		$fservicio = $this->input->post('fservicio');
		$cchecklist = $this->input->post('cchecklist');

		$parametros = array(
			'@int_auditoria' => $cauditoria,
			'@dt_servicio' => $fservicio,
			'@cchecklist' => $cchecklist,
			'@ls_cgrucrieval' => '', //$this->input->post('ls_cgrucrieval'),
			'@ls_cvalnoconf' => $this->input->post('ls_cvalnoconf'),
			'@str_cod_usuario' => $this->input->post('str_cod_usuario')
		);

		$resultado = $this->minspeccion->cambioestadochecklist($parametros);

		$this->db->query('CALL sp_formula_insprov_valobtenido_01(?,?,?)', [
			'@AS_CAUDI' => $cauditoria,
			'@AD_FECHA' => $fservicio,
			'@AS_CCHECKLIST' => $cchecklist,
		]);

		echo json_encode($resultado);
	}

	/**
	 * Vista de la inspeccion
	 * @param int $cauditoria
	 * @param string $fservicio
	 */
	public function editar()
	{
		$cauditoria = $this->input->get('cauditoria');
		$fservicio = $this->input->get('fservicio');

		if (empty($cauditoria) || empty($fservicio)) {
			show_404();
		}

		$inspeccion = $this->minspeccion->buscar($cauditoria, $fservicio);
		if (empty($inspeccion)) {
			show_404();
		}

		$resumen = $this->minspeccion->obtenerResumen($cauditoria, $fservicio);
		$certificaciones = $this->minspeccion->obtenerCertificaciones();
		$informes = $this->minspeccion->obtenerInformes();

		// Inspección anterior
		$inspeccionAnt = $this->minspeccion->buscarInformeAnterior($cauditoria, $fservicio);
		$ubigeos = $this->minspeccion->obtenerUbigeo('');

		$this->layout->js([
			versionar_archivo('script/oi/ctrlprov/inspctrolprov/checklist.js'),
			versionar_archivo('script/oi/ctrlprov/inspctrolprov/accion_correctiva.js'),
			versionar_archivo('script/oi/ctrlprov/inspctrolprov/peligro.js'),
			versionar_archivo('script/oi/ctrlprov/inspctrolprov/resumen.js'),
			versionar_archivo('script/oi/ctrlprov/inspctrolprov/resumen_contacto.js'),
			versionar_archivo('script/oi/ctrlprov/inspctrolprov/resumen_tema.js'),
			versionar_archivo('script/oi/ctrlprov/inspctrolprov/involucrados.js'),
		]);

		$speligro = ($inspeccion->espeligro == "S");

		$inspeccionAbierto = ($resumen->ZCTIPOESTADOSERVICIO == '031') ? 1 : 0;

		$this->parser->parse('seguridad/vprincipal', [
			'vista' => 'Ventana',
			'content_for_layout' => 'oi/ctrlprov/inspctrolprov/vinspeccion',
			'ccia' => 1,
			'datos_ventana' => [
				'cauditoria' => $cauditoria,
				'fservicio' => $fservicio,
				'inspeccion' => $inspeccion,
				'resumen' => $resumen,
				'inspeccionAbierto' => $inspeccionAbierto,
				'certificaciones' => $certificaciones,
				'inspeccionAnt' => $inspeccionAnt,
				'speligro' => $speligro,
				'informes' => $informes,
				'ubigeos' => $ubigeos,
			]
		]);
	}

	/**
	 * Descargar el ACC
	 */
	public function descargar_acc()
	{
		$cauditoria = $this->input->get('cauditoria');
		$fservicio = $this->input->get('fservicio');
		$inspeccion = $this->minspeccion->buscar($cauditoria, $fservicio);
		if (empty($inspeccion)) {
			show_404();
		}
		if (empty($inspeccion->DUBICACIONFILESERVERAC)) {
			show_404();
		}
		$rutaficha = RUTA_ARCHIVOS . $inspeccion->DUBICACIONFILESERVERAC;
		$this->load->helper('download');
		force_download($rutaficha, null);
	}

	public function cerrar_informe()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {

			$cauditoria = $this->input->post('cauditoria');
			$fservicio = $this->input->post('fservicio');

			$dInspeccion = $this->minspeccion->buscarDauditoria($cauditoria, $fservicio);
			if (empty($dInspeccion)) {
				throw new Exception('La inspección no pudo ser encontrado.');
			}

			$inspeccion = $this->minspeccion->buscar($cauditoria, $fservicio);

			if ($dInspeccion->SCIERRESERVICIO2 != 'A') {
				throw new Exception('La inspección ya se encuentra cerrada.');
			}

			if (empty($dInspeccion->PRESULTADOCHECKLIST)) {
				throw new Exception('Falta de calcular el checklist');
			}

			if ($inspeccion->espeligro == "S") {
				$peligros = $this->mpeligros->listar($cauditoria, $fservicio);
				if (empty($peligros)) {
					throw new Exception('Debes ingresar los peligros.');
				}
			}

			$li_dchklist_nulost = $this->db->select('count(1) as li_dchklist_nulost')
				->from('pvalorchecklist')
				->where('CAUDITORIAINSPECCION', $cauditoria)
				->where('FSERVICIO >', $fservicio)
				->where('ISNULL(nvalorrequisito, -9999) =', '-9999', false)
				->get()
				->row()
				->li_dchklist_nulost;

			if ($li_dchklist_nulost != 0) {
				throw new Exception('Tiene que llenar todo el checklist antes de proceder a cerrarlo.');
			}

			if (empty($dInspeccion->DINFORMEFINAL)) {
				throw new Exception('No se puede cerrar la inspección mientras no tenga un Nro de Informe generado.');
			}

			// Busqueda de valore requisito
			$lde_nrmax = $this->db->select('max( PV.nvalormaxrequisito) as total')
				->from('PVALORCHECKLIST PV')
				->where('CAUDITORIAINSPECCION', $cauditoria)
				->where('FSERVICIO >', $fservicio)
				->get()
				->row()
				->total;

			// Busqueda de areas
			$compania = $this->db->select('DPT.CCOMPANIA, DPT.CAREA, DPT.CSERVICIO')
				->from('PCAUDITORIAINSPECCION PC')
				->join('PDPTE DPT', 'PC.CINTERNOPTE = DPT.CINTERNOPTE and PC.NORDEN = DPT.NORDEN')
				->where('PC.CAUDITORIAINSPECCION', $cauditoria)
				->get()
				->row();

			// No conformidades
			$li_noconformidades = $this->db->select('count(*) as total')
				->from('pvalorchecklist A')
				->where('A.CAUDITORIAINSPECCION', $cauditoria)
				->where('A.FSERVICIO >', $fservicio)
				->where_in('A.CVALORNOCONFORMIDAD + A.CDETALLEVALORNOCONFORMIDAD', "(select dv.cvalor + dv.cdetallevalor from MDETALLEVALOR dv join MVALOR v on dv.cvalor = v.cvalor where v.stipovalor = 'N' and ndetallevalor = 1 )", false)
				->get()
				->row()
				->total;

			// Acciones realizadas
			$li_accionesrealizadas = $this->db->select('COUNT(*) as total')
				->from('paccioncorrectiva')
				->where('CAUDITORIAINSPECCION', $cauditoria)
				->where('FSERVICIO >', $fservicio)
				->get()
				->row()
				->total;

			if ($li_noconformidades != $li_accionesrealizadas) {
				throw new Exception('Debe generar acciones correctivas a las no conformidades ingresadas');
			}

			// Nro de meses
			$LI_NUMMES = $this->db->select('TOP 1 MD.NMES')
				->from('MDETALLECRITERIORESULTADO MD')
				->where('CCRITERIORESULTADO', "(SELECT CCRITERIORESULTADO FROM PDAUDITORIAINSPECCION PD WHERE PD.CAUDITORIAINSPECCION = '{$cauditoria}' AND PD.FSERVICIO = '{$fservicio}')", false)
				->where('MD.NVALORINICIAL <=', $dInspeccion->PRESULTADOCHECKLIST)
				->order_by('MD.NVALORINICIAL', 'DESC')
				->get()
				->row()
				->NMES;

			if ($LI_NUMMES == 0) {
				throw new Exception('El Resultado de checklist de la Inspección no tiene intervalo de meses asignado');
			}

			$LDA_FECHANEW = \Carbon\Carbon::createFromFormat('Y-m-d', $fservicio, 'America/Lima')->addMonths($LI_NUMMES);
			// Verificando si es comienzo de mes
			if ($LDA_FECHANEW->day > 1) {
				// Asignando primer dia del mes
				$LDA_FECHANEW->setDay(1);
				// Verificando si es domingo
				if ($LDA_FECHANEW->dayOfWeek === \Carbon\Carbon::SUNDAY) {
					// Se cambia a Lunes
					$LDA_FECHANEW->addDays(1);
				}
			}

			$ld_fcreacion_new = $LDA_FECHANEW->format('Y-m-d');

			$ldt_fec = date('Y-m-d');
			$cusuario = $this->session->userdata('s_cusuario');

			$this->db->update('PDAUDITORIAINSPECCION', [
				'SSERVICIOREALIZADO' => 'S',
				'SCIERRESERVICIO2' => 'C',
				'FCIERRESERVICIO' => $ldt_fec,
				'CUSUARIOMODIFICA' => $cusuario,
				'TMODIFICACION' => $ldt_fec,
				'ZCTIPOESTADOSERVICIO' => '032', // Concluido Ok
				'CUSUARIOCIERRE' => $cusuario,
				'TCIERRE' => $ldt_fec,
				'sultimo' => 'N'
			], [
				'CAUDITORIAINSPECCION' => $cauditoria,
				'FSERVICIO' => $fservicio,
			]);

			$this->db->update('PPROGRAMAFECHA', [
				'SREALIZADO' => 'S',
				'CUSUARIOMODIFICA' => $cusuario,
				'TMODIFICACION' => $ldt_fec,
			], [
				'CAUDITORIAINSPECCION' => $cauditoria,
				'FPROGRAMADASERVICIO' => $fservicio,
			]);

			$this->db->update('HRESULTADOAUDINSP', [
				'ZCTIPOESTADOSERVICIO' => '032',
			], [
				'CAUDITORIAINSPECCION' => $cauditoria,
				'FSERVICIO' => $fservicio,
			]);

			$this->db->insert('PDAUDITORIAINSPECCION', [
				'CAUDITORIAINSPECCION' => $cauditoria,
				'FSERVICIO' => $LDA_FECHANEW->format('Y-m-d'),
				'CUSUARIOCONSULTOR' => null,
				'STIPOCHEQUEO' => $dInspeccion->STIPOCHEQUEO,
				'CNORMA' => $dInspeccion->CNORMA,
				'CSUBNORMA' => $dInspeccion->CSUBNORMA,
				'CCHECKLIST' => $dInspeccion->CCHECKLIST,
				'CGRUPOCRITERIO' => $dInspeccion->CGRUPOCRITERIO,
				'SLLENADOCHECKLIST' => $dInspeccion->SLLENADOCHECKLIST,
				'CVALORNOCONFORMIDAD' => $dInspeccion->CVALORNOCONFORMIDAD,
				'CFORMULAEVALUACION' => $dInspeccion->CFORMULAEVALUACION,
				'SPERMITIRCORRECTIVAS' => $dInspeccion->SPERMITIRCORRECTIVAS,
				'SPREVIA' => $dInspeccion->SPREVIA,
				'SSEGUIMIENTO' => $dInspeccion->SSEGUIMIENTO,
				'SENVIOCARTA' => $dInspeccion->SENVIOCARTA,
				'SSERVICIOREALIZADO' => 'N',
				'SCIERRESERVICIO2' => 'A',
				'CUSUARIOCREA' => $dInspeccion->CUSUARIOCREA,
				'TCREACION' => date('Y-m-d'),
				'SREGISTRO' => 'A',
				'DUBICACIONFILESERVER' => null,
				'CCRITERIORESULTADO' => $dInspeccion->CCRITERIORESULTADO,
				'CDETALLECRITERIORESULTADO' => null,
				'ZCTIPOESTADOSERVICIO' => '028',
				'SINCLUYEPLAN' => $dInspeccion->SINCLUYEPLAN,
				'ZCTIPOSERVICIO' => $dInspeccion->ZCTIPOSERVICIO,
				'CMODELOINFORME' => $dInspeccion->CMODELOINFORME,
				'CUBIGEOMUNICIPALIDAD' => $dInspeccion->CUBIGEOMUNICIPALIDAD,
				'DLICENCIAFUNCIONAMIENTO' => $dInspeccion->DLICENCIAFUNCIONAMIENTO,
				'SLICENCIAFUNCIONAMIENTO' => $dInspeccion->SLICENCIAFUNCIONAMIENTO,
				'DALCANCEINFORME' => $dInspeccion->DALCANCEINFORME,
				'DMARCAINFORME' => $dInspeccion->DMARCAINFORME,
				'DADICIONALLICENCIA' => $dInspeccion->DADICIONALLICENCIA,
				'DPERMISOAUTORIDADSANITARIA' => $dInspeccion->DPERMISOAUTORIDADSANITARIA,
				'FCREACION' => $ld_fcreacion_new,
				'sultimo' => 'S'
			]);

			$this->db->insert('PPROGRAMAFECHA', [
				'CAUDITORIAINSPECCION' => $cauditoria,
				'FPROGRAMADASERVICIO' => '1900-01-01',
				'SREALIZADO' => 'N',
				'CUSUARIOCREA' => $dInspeccion->CUSUARIOCREA,
				'TCREACION' => date('Y-m-d'),
				'SREGISTRO' => 'A',
			]);

			$this->result['status'] = 200;
			$this->result['message'] = 'Inspección cerrada correctamente.';

		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

	public function reapertura()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {

			$cauditoria = $this->input->post('cauditoria');
			$fservicio = $this->input->post('fservicio');

			$dInspeccion = $this->minspeccion->buscarDauditoria($cauditoria, $fservicio);
			if (empty($dInspeccion)) {
				throw new Exception('La inspección no pudo ser encontrado.');
			}

			$ls_cierre = $dInspeccion->SCIERRESERVICIO2;
			$ls_zctipoestadoservicio = $dInspeccion->ZCTIPOESTADOSERVICIO;
			$ls_consul = $dInspeccion->CUSUARIOCONSULTOR;
			$ld_fcierre = $dInspeccion->FINFORMEFIN;
			$FSERVICIO = $dInspeccion->FSERVICIO;
			$FCREACION = $dInspeccion->FCREACION;

			$ldt_fec = date('Y-m-d');

			if ((empty($ld_fcierre) || $ld_fcierre == '1900-01-01') && $ls_cierre == 'A' && $ls_zctipoestadoservicio == '031') {
				throw new Exception('Generar fecha de Termino de Informe');
			}

			if ($ls_cierre != "C") {
				throw new Exception('La inspección no se encuentra cerrada.');
			}
			// se verifica si ele stado es Trunco sin Programacion se le coloca una variable ‘1’ de lo contrario si tiene inspecciones anteriores
			if ($ls_zctipoestadoservicio == '881') {
				$li_cont_pp = 1;
			} else {
				$li_cont_pp = $this->db->select('COUNT(*) li_cont_pp')
					->from('PDAUDITORIAINSPECCION')
					->where('CAUDITORIAINSPECCION', $cauditoria)
					->where('FSERVICIO >', $fservicio)
					->get()
					->row()
					->li_cont_pp;
			}

			//tiene inspecciones posteriores
			if ($li_cont_pp != 1) {
				throw new Exception('No puede abrir esta inspección porque la siguiente se encuentra cerrada');
			}

			$li_cont_pp = 0;
			if ($ls_zctipoestadoservicio == "881") {
				$li_cont_pp = 1;
			} else {
				$li_cont_pp = $this->db->select('COUNT(*) li_cont_pp')
					->from('PDAUDITORIAINSPECCION')
					->where('CAUDITORIAINSPECCION', $cauditoria)
					->where('FSERVICIO >', $fservicio)
					->where_in('ZCTIPOESTADOSERVICIO', ['028', '029', '030'])
					->get()
					->row()
					->li_cont_pp;
			}
			if ($li_cont_pp == 0) {
				throw new Exception('No puede abrir esta inspección porque la siguiente se encuentra en proceso');
			}

			// Inspeccion posterior
			$dInspeccionPosterior = $this->db->select('*')
				->from('PDAUDITORIAINSPECCION')
				->where('CAUDITORIAINSPECCION', $cauditoria)
				->where('FSERVICIO >', $fservicio)
				->where_in('ZCTIPOESTADOSERVICIO', ['028', '029', '030'])
				->get()
				->row();

			$LDA_FECHA_SERVICIO = $dInspeccionPosterior->FSERVICIO;

			// eliminamos el registro de esta tabla con la fecha posterior
			$this->db->delete('PPROGRAMAFECHA', ['CAUDITORIAINSPECCION' => $cauditoria, 'FPROGRAMADASERVICIO' => $LDA_FECHA_SERVICIO]);

			// eliminamos el registro de esta tabla con la fecha posterior
			$this->db->delete('PPLANAUDINSP', ['CAUDITORIAINSPECCION' => $cauditoria, 'FPROGRAMADASERVICIO' => $LDA_FECHA_SERVICIO]);

			// eliminamos el registro de esta tabla con la fecha posterior
			$this->db->delete('PRESPONSABLEAUDINSP', ['CAUDITORIAINSPECCION' => $cauditoria, 'FPROGRAMADASERVICIO' => $LDA_FECHA_SERVICIO]);

			// eliminamos el registro de esta tabla con la fecha posterior
			$this->db->delete('PDAUDITORIAINSPECCION', ['CAUDITORIAINSPECCION' => $cauditoria, 'FPROGRAMADASERVICIO' => $LDA_FECHA_SERVICIO]);

			// si el estado del registro es trunco o convalidado o trunco sin programar
			if ($ls_zctipoestadoservicio == '515' || $ls_zctipoestadoservicio == '519' || $ls_zctipoestadoservicio == '881') {
				// verifica si el campo cusuarioconsultor no tenga datos y que el campo fservicio tampoco tenga fecha
				if ((empty($ls_consul) || strlen(trim($ls_consul)) == 0) && (empty($fservicio) || $fservicio == '1900-01-01')) {
					$ls_estadohab = '028';
				} else {
					if (empty($ls_consul) || strlen(trim($ls_consul)) == 0) {
						$ls_estadohab = '030';
					} else {
						$ls_estadohab = '029';
					}
				}
			} else {
				$ls_estadohab = '031';
			}

			// se cambio el campo actual SCIERRESERVICIO2 = 'A', el estado del registro actual, el usuario quien cerro y la fecha del cierre actual
			$this->db->update('PDAUDITORIAINSPECCION', [
				'SCIERRESERVICIO2' => 'A',
				'ZCTIPOESTADOSERVICIO' => $ls_estadohab,
				'CUSUARIOCIERRE' => $this->session->userdata('s_cusuario'),
				'TCIERRE' => $ld_fcierre,
				'sultimo' => 'S',
			], [
				'CAUDITORIAINSPECCION' => $cauditoria,
				'FSERVICIO' => $fservicio
			]);

			// eliminamos el registro de esta tabla del registro actual
			$this->db->delete('PACCIONCORRECTIVA', [['CAUDITORIAINSPECCION' => $cauditoria, 'FPROGRAMADASERVICIO' => $fservicio]]);

			$this->result['status'] = 200;
			$this->result['message'] = 'Se reabrió la inspección correctamente';

		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

}

?>
