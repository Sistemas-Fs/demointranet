<?php

/**
 * Class cresumen
 *
 * @property minspeccion minspeccion
 * @property mhresultado mhresultado
 * @property mpeligros mpeligros
 * @property mresumen_contacto mresumen_contacto
 */
class cresumen extends FS_Controller
{

	/**
	 * cresumen constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('oi/ctrlprov/inspctrolprov/minspeccion');
		$this->load->model('oi/ctrlprov/inspctrolprov/mhresultado');
		$this->load->model('oi/ctrlprov/inspctrolprov/mpeligros');
		$this->load->model('oi/ctrlprov/inspctrolprov/mresumen_contacto');
	}

	/**
	 * Actualiza los datos de la inspección
	 */
	public function guardar()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {
			$cauditoria = $this->input->post('cauditoria');
			$fservicio = $this->input->post('fservicio');
			$resumen_real_hora_efectiva = $this->input->post('resumen_real_hora_efectiva');
			$resumen_real_hora_no_efectiva = $this->input->post('resumen_real_hora_no_efectiva');
			$resumen_certificaciones = $this->input->post('resumen_certificaciones');
			$resumen_resultado_estado = $this->input->post('resumen_resultado_estado');
			$resumen_detalle = $this->input->post('resumen_detalle');
			$resumen_alcance_final = $this->input->post('resumen_alcance_final');
			$resumen_producto = $this->input->post('resumen_producto');
			$resumen_marca = $this->input->post('resumen_marca');
			$resumen_otro_doc_municipio = $this->input->post('resumen_otro_doc_municipio');
			$resumen_otro_doc_licencia = $this->input->post('resumen_otro_doc_licencia');
			$resumen_otro_doc_estado = $this->input->post('resumen_otro_doc_estado');
			$resumen_otro_doc_observacion = $this->input->post('resumen_otro_doc_observacion');

			$dinspeccion = $this->minspeccion->buscarDauditoria($cauditoria, $fservicio);
			if (empty($dinspeccion)) {
				throw new Exception('La inspección no pudo ser encontrado.');
			}

			$data = [
				'NREALHORAEFECTIVA' => $resumen_real_hora_efectiva,
				'NREALHORANOEFECTIVA' => $resumen_real_hora_no_efectiva,
				'CUSUARIOMODIFICA' => $this->session->userdata('s_cusuario'),
				'TMODIFICACION' => date('Y-m-d H:i:s'),
				'CUBIGEOMUNICIPALIDAD' => $resumen_otro_doc_municipio,
				'DLICENCIAFUNCIONAMIENTO' => trim($resumen_otro_doc_licencia),
				'SLICENCIAFUNCIONAMIENTO' => $resumen_otro_doc_estado,
				'DALCANCEINFORME' => trim($resumen_alcance_final),
				'DMARCAINFORME' => trim($resumen_producto),
				'DMARCAINFORME2' => trim($resumen_marca),
				'DADICIONALLICENCIA' => trim($resumen_otro_doc_observacion),
				'DPERMISOAUTORIDADSANITARIA' => trim($resumen_detalle),
				'CCERTIFICACION' => $resumen_certificaciones,
				'scertificacion' => $resumen_resultado_estado,
			];

			$resp = $this->db->update('PDAUDITORIAINSPECCION', $data, [
				'CAUDITORIAINSPECCION' => $cauditoria,
				'FSERVICIO' => $fservicio,
			]);

			if (!$resp) {
				throw new Exception('Error al actualizar la inspección');
			}

			$this->result['status'] = 200;
			$this->result['message'] = 'Resumen actualizado correctamente.';

		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

	/**
	 * Guarda la ACC en excel
	 */
	public function guardar_archivo()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {
			$cauditoria = $this->input->post('cauditoria');
			$fservicio = $this->input->post('fservicio');

			$cinspeccion = $this->minspeccion->buscar($cauditoria, $fservicio);
			if (empty($cinspeccion)) {
				throw new Exception('Inspección no pudo ser encontrada.');
			}

			$cfservicio = \Carbon\Carbon::createFromFormat('Y-m-d', $fservicio, 'America/Lima');

			$carpetas = '10102/' . $cinspeccion->ccliente . '/' . $cauditoria . '/' . $cfservicio->format('Ymd') . '/';
			$rutaficha = RUTA_ARCHIVOS . $carpetas;

			!is_dir($rutaficha) && @mkdir($rutaficha, 0777, true);

			$config['upload_path'] = $rutaficha;
			$config['allowed_types'] = '*';
			$config['max_size'] = 0;
			$config['overwrite'] = TRUE;

			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			if (!($this->upload->do_upload('resumen_informe'))) {
				throw new Exception($this->upload->display_errors());
			}

			$nombreArchivo = $this->upload->data('file_name');
			$rutaCompleta = $carpetas . $nombreArchivo;

			$this->db->update('PDAUDITORIAINSPECCION', [
				'DUBICACIONFILESERVER' => $rutaCompleta
			], [
				'CAUDITORIAINSPECCION' => $cauditoria,
				'FSERVICIO' => $fservicio,
			]);

			$this->result['status'] = 200;
			$this->result['message'] = "Archivo guardado correctamente";
			$this->result['data']['filename'] = $nombreArchivo;
			$this->result['data']['path'] = $nombreArchivo;

		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

	public function generar_fecha()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}

		try {

			$cauditoria = $this->input->post('cauditoria');
			$fservicio = $this->input->post('fservicio');

			$inspeccion = $this->minspeccion->buscarDauditoria($cauditoria, $fservicio);
			if (empty($inspeccion)) {
				throw new Exception('La inspección no pudo ser encontrada.');
			}

			if ($inspeccion->SCIERRESERVICIO2 != "A") {
				throw new Exception('La inspección está CERRADA, no se puede modificar la fecha de término de informe');
			}

			if (!empty($inspeccion->FINFORMEFIN)) {
				throw new Exception('La fecha de termino ya fue registrada anteriormente!');
			}

			$fechaCierre = date('Y-m-d');

			$this->db->update('PDAUDITORIAINSPECCION', ['FINFORMEFIN' => $fechaCierre], [
				'CAUDITORIAINSPECCION' => $cauditoria,
				'FSERVICIO' => $fservicio,
			]);

			$this->result['status'] = 200;
			$this->result['message'] = 'Se registró la fecha de término de informe';
			$this->result['data'] = $fechaCierre;

		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

	/**
	 * Genera el numero de informe de la inspeccion
	 */
	public function nro_informe()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {

			$cauditoria = $this->input->post('cauditoria');
			$fservicio = $this->input->post('fservicio');

			$inspeccion = $this->minspeccion->buscarDauditoria($cauditoria, $fservicio);
			if (empty($inspeccion)) {
				throw new Exception('La inspección no fue encontrada.');
			}

			if (!empty($inspeccion->DINFORMEFINAL_INACTIVO) && empty($inspeccion->DINFORMEFINAL)) {

				$this->db->update('HRESULTADOAUDINSP', [
					'DINFORME' => $inspeccion->DINFORMEFINAL_INACTIVO
				], [
					'CAUDITORIAINSPECCION' => $cauditoria,
					'FSERVICIO' => $fservicio,
				]);

				$this->db->update('PDAUDITORIAINSPECCION', [
					'DINFORMEFINAL' => $inspeccion->DINFORMEFINAL_INACTIVO,
					'DINFORMEFINAL_INACTIVO' => null,
				], [
					'CAUDITORIAINSPECCION' => $cauditoria,
					'FSERVICIO' => $fservicio,
				]);

			} else {
				if (!empty($inspeccion->DINFORMEFINAL)) {
					throw new Exception('El informe ya fue generado,');
				}

				$compania = $this->db->select('DPT.CCOMPANIA, DPT.CAREA, DPT.CSERVICIO')
					->from('PCAUDITORIAINSPECCION PC')
					->join('PDPTE DPT', 'PC.CINTERNOPTE = DPT.CINTERNOPTE and PC.NORDEN = DPT.NORDEN')
					->where('PC.CAUDITORIAINSPECCION', $cauditoria)
					->get()
					->row();

				if (empty($compania)) {
					throw new Exception('Error al encontrar la compañia.');
				}

				$this->db->query("CALL sp_obtiene_numero_doc('I', '{$compania->CCOMPANIA}', '{$compania->CAREA}', '{$fservicio}', '{$cauditoria}', '{$this->session->userdata('s_cusuario')}')");
			}

			$inspeccion = $this->minspeccion->buscarDauditoria($cauditoria, $fservicio);

			$this->result['status'] = 200;
			$this->result['message'] = 'Informe generado correctamente.';
			$this->result['data'] = $inspeccion->DINFORMEFINAL;

		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

	public function generar_informe()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {

			$cauditoria = $this->input->post('cauditoria');
			$fservicio = $this->input->post('fservicio');

			$cinspeccion = $this->minspeccion->buscarCauditoria($cauditoria);
			if (empty($cinspeccion)) {
				throw new Exception('La inspeccion no pudo ser encontrada.');
			}
			$dinspeccion = $this->minspeccion->buscarDauditoria($cauditoria, $fservicio);
			if (empty($dinspeccion)) {
				throw new Exception('La inspeccion programada no pudo ser encontrada.');
			}

			$inspeccion = $this->minspeccion->buscar($cauditoria, $fservicio);

			// validar peligros
			if ($inspeccion->espeligro == "S") {
				$peligros = $this->mpeligros->listar($cauditoria, $fservicio);
				if (empty($peligros)) {
					throw new Exception('Debes ingresar los peligros.');
				}
			}

			$contactos = $this->mresumen_contacto->lista($cauditoria, $fservicio);
			if (empty($contactos)) {
				throw new Exception('Debes ingresar contactos.');
			}

			if (empty($dinspeccion->DINFORMEFINAL)) {
				throw new Exception('Debes generar el nro de informe.');
			}

			if (empty($dinspeccion->FINFORMEFIN) || $dinspeccion->FINFORMEFIN == '1900-01-01') {
				throw new Exception('Debes generar la fecha de informe.');
			}

			$resp = $this->mhresultado->guardar(
				$cauditoria,
				$fservicio,
				2,
				'01',
				'01',
				$cinspeccion->CCLIENTE,
				$cinspeccion->CESTABLECIMIENTO,
				$cinspeccion->CPROVEEDORCLIENTE,
				$cinspeccion->CESTABLECIMIENTOPROV,
				$cinspeccion->CMAQUILADORCLIENTE,
				$cinspeccion->CESTABLECIMIENTOMAQUILA,
				$dinspeccion->NRESULTADOCHECKLIST,
				$cinspeccion->CLINEAPROCESOCLIENTE,
				$dinspeccion->CCRITERIORESULTADO,
				$dinspeccion->CDETALLECRITERIORESULTADO,
				$dinspeccion->DINFORMEFINAL,
				$this->session->userdata('s_cusuario'),
				'A',
				$dinspeccion->PRESULTADOCHECKLIST,
				$dinspeccion->ZCTIPOESTADOSERVICIO
			);

			if (!$resp) {
				throw new Exception('No se pudo actualizar el historico de la inspección.');
			}

			$this->result['status'] = 200;
			$this->result['message'] = 'Historico actualizado correctamente.';
			$this->result['data']['link'] = base_url('oi/ctrlprov/ccons_insp/pdf?codigo=' . $cauditoria . '&fecha=' . $fservicio);

		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

}
