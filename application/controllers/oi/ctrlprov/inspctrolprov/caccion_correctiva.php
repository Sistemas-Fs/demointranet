<?php

use \PhpOffice\PhpSpreadsheet\Style\Fill;
use \PhpOffice\PhpSpreadsheet\Style\Alignment;
use \PhpOffice\PhpSpreadsheet\Style\Border;

/**
 * Class Caccion_correctiva
 *
 * @property maccion_correctiva maccion_correctiva
 * @property minspeccion minspeccion
 */
class Caccion_correctiva extends FS_Controller
{

	/**
	 * cinspctrolprovacc constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('oi/ctrlprov/inspctrolprov/minspeccion');
		$this->load->model('oi/ctrlprov/inspctrolprov/maccion_correctiva');
	}

	public function lista()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		$parametros = array(
			'@CAUDITORIA' => $this->input->post('cauditoriainspeccion'),
			'@FSERVICIO' => $this->input->post('fservicio')
		);
		$resultado = $this->maccion_correctiva->lista($parametros);
		echo json_encode($resultado);
	}

	public function buscar()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		$parametros = array(
			'@cauditoria' => $this->input->post('cauditoria'),
			'@fservicio' => $this->input->post('fservicio'),
			'@cchecklist' => $this->input->post('cchecklist'),
			'@crequisitochecklist' => $this->input->post('crequisitochecklist'),
		);
		$resultado = $this->maccion_correctiva->buscar($parametros);
		echo json_encode($resultado);
	}

	/**
	 * Guarda el acc
	 */
	public function guardar()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {

			$cauditoria = $this->input->post('cauditoria');
			$fservicio = $this->input->post('fservicio');
			$acc_modal_cchecklist_id = $this->input->post('acc_modal_cchecklist_id');
			$acc_modal_requisito_cchecklist_id = $this->input->post('acc_modal_requisito_cchecklist_id');
//			$acc_modal_requisito = $this->input->post('acc_modal_requisito');
//			$acc_modal_hallazgo = $this->input->post('acc_modal_hallazgo');
			$acc_modal_acc = $this->input->post('acc_modal_acc');
			$acc_modal_acepta_acc = $this->input->post('acc_modal_acepta_acc');
//			$acc_modal_tipo_hallazgo = $this->input->post('acc_modal_tipo_hallazgo');
			$acc_modal_fecha = $this->input->post('acc_modal_fecha');
			$acc_modal_observaciones = $this->input->post('acc_modal_observaciones');
			$acc_modal_responsable = $this->input->post('acc_modal_responsable');

			$acc_modal_acepta_acc = (empty($acc_modal_acepta_acc)) ? 'N' : 'S';

			$acc_modal_observaciones = trim($acc_modal_observaciones);
			$acc_modal_acc = trim($acc_modal_acc);

			if (!validateDate($acc_modal_fecha, 'd/m/Y')) {
				throw new Exception('La fecha tiene un formato invalido. Dia/Mes/Año');
			}

			if (empty($this->session->userdata('s_cusuario'))) {
				throw new Exception('Debes volver a iniciar sesión.');
			}

			$acc_modal_fecha = \Carbon\Carbon::createFromFormat('d/m/Y', $acc_modal_fecha, 'America/Lima')->format('Y-m-d');

			$this->maccion_correctiva->guardar(
				$cauditoria,
				$fservicio,
				$acc_modal_cchecklist_id,
				$acc_modal_requisito_cchecklist_id,
				$acc_modal_acc,
				$acc_modal_fecha,
				$acc_modal_acepta_acc,
				$acc_modal_observaciones,
				$this->session->userdata('s_cusuario'),
				'A',
				null,
				date('Y-m-d'),
				trim($acc_modal_responsable)
			);

			$this->result['status'] = 200;
			$this->result['message'] = 'Acción correctiva actualizada correctamente.';

		} catch (Exception $ex) {
			$this->result['mnessage'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

	/**
	 * Guarda la ACC en excel
	 */
	public function guardar_archivo()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {
			$cauditoria = $this->input->post('cauditoria');
			$fservicio = $this->input->post('fservicio');

			if (empty($cauditoria) || empty($fservicio)) {
				throw new Exception('Falta de datos de la inspeccion.');
			}

			$inspeccion = $this->minspeccion->buscar($cauditoria, $fservicio);
			if (empty($inspeccion)) {
				throw new Exception('Inspección no pudo ser encontrada.');
			}

			if (!isset($_FILES['file_acc']['name']) || empty($_FILES['file_acc']['name'])) {
				throw new Exception('Debes elegir el archivo de AACC.');
			}

			$cfservicio = \Carbon\Carbon::createFromFormat('Y-m-d', $fservicio, 'America/Lima');

			$carpetas = '20101/' . $inspeccion->ccliente . '/' . $cauditoria . '/' . $cfservicio->format('Ymd') . '/';
			$rutaficha = RUTA_ARCHIVOS . $carpetas;

			!is_dir($rutaficha) && @mkdir($rutaficha, 0777, true);

			$config['upload_path']      = $rutaficha;
			$config['allowed_types']    = '*';
			$config['max_size']         = 0;
			$config['overwrite'] 		= TRUE;

			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			if (!($this->upload->do_upload('file_acc'))) {
				throw new Exception($this->upload->display_errors());
			}

			$nombreArchivo = $this->upload->data('file_name');
			$rutaCompleta = $carpetas . $nombreArchivo;

			$this->db->update('PDAUDITORIAINSPECCION', [
				'DUBICACIONFILESERVERAC' => $rutaCompleta,
			], [
				'CAUDITORIAINSPECCION' => $cauditoria,
				'FSERVICIO' => $fservicio,
			]);

			$resultado = $this->maccion_correctiva->lista([
				'@CAUDITORIA' => $cauditoria,
				'@FSERVICIO' => $fservicio,
			]);
			if (!empty($resultado)) {
				foreach ($resultado as $key => $value) {
					$this->db->update('PACCIONCORRECTIVA', [
						'SACEPTARACCIONCORRECTIVA' => 'S',
					], [
						'CAUDITORIAINSPECCION' => $cauditoria,
						'FSERVICIO' => $fservicio,
						'CCHECKLIST' => $value->cchecklist,
						'CREQUISITOCHECKLIST' => $value->crequisitochecklist,
					]);
				}
			}

			$this->result['status'] = 200;
			$this->result['message'] = "Archivo guardado correctamente";
			$this->result['data']['filename'] = $nombreArchivo;
			$this->result['data']['path'] = $nombreArchivo;

		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

	/**
	 * Elimina el archivo cargado
	 */
	public function eliminar_archivo()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {
			$cauditoria = $this->input->post('cauditoria');
			$fservicio = $this->input->post('fservicio');

			$inspeccion = $this->minspeccion->buscar($cauditoria, $fservicio);
			if (empty($inspeccion)) {
				throw new Exception('Inspección no pudo ser encontrada.');
			}

			if ($inspeccion->SCIERRESERVICIO2 != 'A') {
				throw new Exception('La inspección NO se encuentra abierto');
			}

			if (empty($inspeccion->DUBICACIONFILESERVERAC)) {
				throw new Exception('No existe un archivo que eliminar.');
			}

			$this->db->update('PDAUDITORIAINSPECCION', [
				'DUBICACIONFILESERVERAC' => null,
			], [
				'CAUDITORIAINSPECCION' => $cauditoria,
				'FSERVICIO' => $fservicio,
			]);

			$this->result['status'] = 200;
			$this->result['message'] = 'Archivo eliminado correctamente.';

		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

	/**
	 * Descargar archivo
	 */
	public function descargar_plantilla()
	{
		$cauditoria = $this->input->get('cauditoria');
		$fservicio = $this->input->get('fservicio');

		try {

			$cfservicio = \Carbon\Carbon::createFromFormat('Y-m-d', $fservicio);

			$inspccion = $this->minspeccion->buscar($cauditoria, $fservicio);
			if (empty($inspccion)) {
				show_404();
			}

			$spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
			$sheet = $spreadsheet->getActiveSheet();
			$sheet->setTitle('PLAN AACC');

			$spreadsheet->getDefaultStyle()
				->getFont()
				->setName('Arial')
				->setSize(11);

			$proveedor = mb_strtoupper($inspccion->proveedor, 'UTF-8');

			$sheet->setCellValue('B2', 'PROVEEDOR: ' . $proveedor);
			$sheet->setCellValue('B3', 'LINEA INSPECCIONADA: ' . mb_strtoupper($inspccion->lineaproc, 'UTF-8'));
			$sheet->setCellValue('B5', "CALIFICACION: {$inspccion->resultado}");
			$sheet->setCellValue('B6', 'FECHA DE INSPECCIÓN: '. date('d/m/Y', strtotime($fservicio)));

			$sheet->setCellValue('A8', 'ID');
			$sheet->setCellValue('B8', 'Requisito');
			$sheet->setCellValue('C8', 'Excluyente');
			$sheet->setCellValue('D8', 'Tipo Hallazgo');
			$sheet->setCellValue('E8', 'Hallazgo');
			$sheet->setCellValue('F8', 'Acción Correctiva');
			$sheet->setCellValue('G8', 'Fecha Corrección');
			$sheet->setCellValue('H8', 'Responsable');
			$sheet->setCellValue('I8', '¿Aceptado por Inspector?');
			$sheet->setCellValue('J8', 'Comentario del Inspector');

			$titulo = [
				'font' => [
					'name' => 'Arial',
					'size' => 11,
					'color' => array('rgb' => '000000'),
					'bold' => true,
				],
			];

			$cabeceraTabla = [
				'font' => [
					'name' => 'Arial',
					'size' => 11,
					'color' => array('rgb' => 'FFFFFF'),
					'bold' => true,
				],
				'fill' => [
					'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
					'startColor' => [
						'rgb' => '29B037'
					]
				],
				'borders' => [
					'allBorders' => [
						'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
						'color' => [
							'rgb' => '000000'
						]
					]
				],
				'alignment' => [
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'wrapText' => true,
				]
			];
			$sheet->getStyle('A8:J8')->applyFromArray($cabeceraTabla);
			$sheet->getStyle('B2')->applyFromArray($titulo);
			$sheet->getStyle('B3')->applyFromArray($titulo);
			$sheet->getStyle('B5')->applyFromArray($titulo);
			$sheet->getStyle('B6')->applyFromArray($titulo);

			$resultado = $this->maccion_correctiva->lista([
				'@CAUDITORIA' => $cauditoria,
				'@FSERVICIO' => $fservicio
			]);

			if (!empty($resultado)) {
				$pos = 9;
				foreach ($resultado as $key => $value) {
					$drequisito = explode("\n", trim($value->drequisito));
					$sheet->setCellValue('A' . $pos, $value->dnumerador);
					if (is_array($drequisito) && count($drequisito) > 2) {
						$sheet->setCellValue('C' . $pos, $value->sexcluyente);
						$sheet->setCellValue('D' . $pos, $value->tipohallazgo);
						$sheet->setCellValue('E' . $pos, $value->dhallazgo);
						$sheet->setCellValue('F' . $pos, '');
						$sheet->setCellValue('G' . $pos, '');
						$sheet->setCellValue('H' . $pos, '');
						$sheet->setCellValue('I' . $pos, '');
						$sheet->setCellValue('J' . $pos, '');
						$initPos = $pos;
						foreach ($drequisito as $keyreq => $valueReq) {
							$sheet->setCellValue('B' . $pos, trim($valueReq));
							$sheet->getStyle('B' . $pos)->getAlignment()->setWrapText(true);
							++$pos;
						}
						--$pos; // Se quita el ultimo para continuar
						$sheet->mergeCells('A' . $initPos . ':A' . $pos);
						$sheet->mergeCells('C' . $initPos . ':C' . $pos);
						$sheet->mergeCells('D' . $initPos . ':D' . $pos);
						$sheet->mergeCells('E' . $initPos . ':E' . $pos);
						$sheet->mergeCells('F' . $initPos . ':F' . $pos);
						$sheet->mergeCells('G' . $initPos . ':G' . $pos);
						$sheet->mergeCells('H' . $initPos . ':H' . $pos);
						$sheet->mergeCells('I' . $initPos . ':I' . $pos);
						$sheet->mergeCells('J' . $initPos . ':J' . $pos);
					} else {
						$sheet->setCellValue('B' . $pos, $value->drequisito);
						$sheet->setCellValue('C' . $pos, $value->sexcluyente);
						$sheet->setCellValue('D' . $pos, $value->tipohallazgo);
						$sheet->setCellValue('E' . $pos, $value->dhallazgo);
						$sheet->setCellValue('F' . $pos, '');
						$sheet->setCellValue('G' . $pos, '');
						$sheet->setCellValue('H' . $pos, '');
						$sheet->setCellValue('I' . $pos, '');
						$sheet->setCellValue('J' . $pos, '');
					}

					$sheet->getStyle('B' . $pos)->getAlignment()->setWrapText(true);
					$sheet->getStyle('E' . $pos)->getAlignment()->setWrapText(true);

					++$pos;
				}
			}

			foreach (range('A', 'J') as $column) {
				switch($column)
				{
					case "B":
						$sheet->getColumnDimension($column)->setWidth(70);
						break;
					case "D":
						$sheet->getColumnDimension($column)->setWidth(35);
						break;
					case "E":
						$sheet->getColumnDimension($column)->setWidth(70);
						break;
					default:
						$sheet->getColumnDimension($column)->setAutoSize(true);
						break;
				}
			}

			$filename = $cfservicio->format('mY') . '-' . $proveedor . '-AACC';
			$writer = new PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment; filename="'. urlencode($filename . '.xlsx').'"');
			$writer->save('php://output');

		} catch (Exception $ex) {
			exit($ex->getMessage());
		}
	}

	public function descargar_archivo()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {

			$cauditoria = $this->input->post('cauditoriainspeccion');
			$fservicio = $this->input->post('fservicio');

			$inspccion = $this->minspeccion->buscarDauditoria($cauditoria, $fservicio);
			if (empty($inspccion)) {
				throw new Exception('Inspección no encontraqda.');
			}

			$pathFile = null;
			if (!empty($inspccion->DUBICACIONFILESERVERAC)) {
				$pathFile = base_url('FTPfileserver/Archivos/' . $inspccion->DUBICACIONFILESERVERAC);
			}

			$resultado = $this->maccion_correctiva->lista([
				'@CAUDITORIA' => $cauditoria,
				'@FSERVICIO' => $fservicio,
			]);

			$this->result['status'] = 200;
			$this->result['data']['result'] = $resultado;
			$this->result['data']['path_file'] = $pathFile;

		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

}
