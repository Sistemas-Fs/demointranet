<?php


/**
 * Class cresumen_tema
 *
 * @property mresumen_tema mresumen_tema
 */
class cresumen_tema extends FS_Controller
{

	/**
	 * cresumen_tema constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('oi/ctrlprov/inspctrolprov/mresumen_tema');
	}

	/**
	 * Se obtiene los recursos de tema
	 */
	public function recursos()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		$cauditoria = $this->input->post('cauditoria');
		$fservicio = $this->input->post('fservicio');
		$temas = $this->mresumen_tema->obtenerRecursos($cauditoria, $fservicio);
		echo json_encode(['temas' => $temas]);
	}

	/**
	 * Se obtiene los datos del tema
	 */
	public function tema()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		$cauditoria = $this->input->post('cauditoria');
		$fservicio = $this->input->post('fservicio');
		$ctipo = $this->input->post('ctipo');
		$tema = $this->mresumen_tema->obtenerTema($cauditoria, $fservicio, $ctipo);
		echo json_encode($tema);
	}

	public function guardar()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {
			$cauditoria = $this->input->post('cauditoria');
			$fservicio = $this->input->post('fservicio');
			$ctipo = $this->input->post('res_tema_id');
			$descripcion = $this->input->post('res_tema_descripcion');

			if (empty($descripcion)) {
				throw new Exception('Debes ingresar la descripción.');
			}

			$resp = $this->mresumen_tema->guardar($cauditoria, $fservicio, $ctipo, trim($descripcion), $this->session->userdata('s_cusuario'));
			if (!$resp) {
				throw new Exception('Error en el proceso de ejecución');
			}

			$this->result['status'] = 200;
			$this->result['message'] = 'Datos guardados correctamente.';

		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

	public function eliminar()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {
			$cauditoria = $this->input->post('cauditoria');
			$fservicio = $this->input->post('fservicio');
			$ctipo = $this->input->post('ctipo');

			$tema = $this->mresumen_tema->obtenerTema($cauditoria, $fservicio, $ctipo);
			if (empty($tema)) {
				throw new Exception('El Tema a eliminar no existe.');
			}

			$res = $this->db->delete('PINFORMACIONADICIONAL', [
				'CAUDITORIAINSPECCION' => $cauditoria,
				'FSERVICIO' => $fservicio,
				'ZCINFOADICIONAL' => $ctipo,
			]);

			if (!$res) {
				throw new Exception('Error en el proceso de ejecución.');
			}

			$this->result['status'] = 200;
			$this->result['message'] = 'Eliminado correctamente';

		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

}
