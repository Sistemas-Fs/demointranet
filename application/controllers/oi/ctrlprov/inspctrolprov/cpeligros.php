<?php

/**
 * Class cpeligros
 *
 * @property Mpeligros mpeligros
 */
class cpeligros extends FS_Controller
{

	/**
	 * cpeligros constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('oi/ctrlprov/inspctrolprov/mpeligros');
	}

	/**
	 * Lista de peligros
	 */
	public function lista()
	{
		$cauditoria = $this->input->post('cauditoria');
		$fservicio = $this->input->post('fservicio');
		$resultado = $this->mpeligros->listar($cauditoria, $fservicio);
		echo json_encode($resultado);
	}

	public function buscar()
	{
		$cauditoria = $this->input->post('cauditoria');
		$fservicio = $this->input->post('fservicio');
		$cproducto = $this->input->post('cproducto');
		$ctipopeligro = $this->input->post('ctipopeligro');
		$norden = $this->input->post('norden');
		$resultado = $this->mpeligros->buscar($cauditoria, $fservicio, $cproducto, $ctipopeligro, $norden);
		echo json_encode($resultado);
	}

	public function guardar()
	{
		try {
			$cauditoria = $this->input->post('cauditoria');
			$fservicio = $this->input->post('fservicio');
			$cproducto = $this->input->post('cproducto');
			$ctipopeligro = $this->input->post('ctipopeligro');
			$norden = $this->input->post('norden');
			$ddetalle = $this->input->post('ddetalle');
			$ddetalle = trim($ddetalle);
			if (empty($ddetalle)) {
				throw new Exception('Debes ingresar el detalle.');
			}
			$resultado = $this->mpeligros->buscar($cauditoria, $fservicio, $cproducto, $ctipopeligro, $norden);
			if (empty($resultado)) {
				throw new Exception('Debes elegir el producto en peligro para continuar.');
			}

			$resp = $this->db->update('ppeligrotipocliente', [
				'DDETALLEPELIGRO' => $ddetalle,
				'CUSUARIOMODIFICA' => $this->session->userdata('s_cusuario'),
				'TMODIFICACION' => date('Y-m-d H:i:s'),
			], [
				'cauditoriainspeccion' => $cauditoria,
				'fservicio' => $fservicio,
				'cproductopeligro' => $cproducto,
				'ctipopeligro' => $ctipopeligro,
			]);
			if (!$resp) {
				throw new Exception('No pudo actualizar el detalle del hallazgo, intente mas tarde.');
			}
			$this->result['status'] = 200;
			$this->result['message'] = 'Detalle actualizado correctamente.';

		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

	public function eliminar()
	{
		try {
			$cauditoria = $this->input->post('cauditoria');
			$fservicio = $this->input->post('fservicio');
			$cproducto = $this->input->post('cproducto');
			$ctipopeligro = $this->input->post('ctipopeligro');
			$norden = $this->input->post('norden');

			$resultado = $this->mpeligros->buscar($cauditoria, $fservicio, $cproducto, $ctipopeligro, $norden);
			if (empty($resultado)) {
				throw new Exception('Debes elegir el producto en peligro para continuar.');
			}

			$resp = $this->db->delete('ppeligrotipocliente', [
				'cauditoriainspeccion' => $cauditoria,
				'fservicio' => $fservicio,
				'cproductopeligro' => $cproducto,
				'ctipopeligro' => $ctipopeligro,
			]);
			if (!$resp) {
				throw new Exception('No pudo eliminar el detalle del hallazgo, intente mas tarde.');
			}
			$this->result['status'] = 200;
			$this->result['message'] = 'Detalle eliminado correctamente.';

		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

	public function lista_peligros()
	{
		$cauditoria = $this->input->post('cauditoria');
		$inspeccion = $this->db->where('CAUDITORIAINSPECCION', $cauditoria)
			->get('PCAUDITORIAINSPECCION')
			->row();
		$resultado = $this->mpeligros->listaPeligros($inspeccion->CCLIENTE);
		echo json_encode($resultado);
	}

	public function guardar_peligro()
	{
		try {
			$cauditoria = $this->input->post('cauditoria');
			$fservicio = $this->input->post('fservicio');
			$cproducto = $this->input->post('cproducto');
			$listaPeligros = $this->mpeligros->listaPeligroProducto($cproducto);
			if (empty($listaPeligros)) {
				throw new Exception('No existe detalle del peligro elegido.');
			}
			foreach ($listaPeligros as $key => $value) {
				// Se valida que no existe, en caso que si solo pasa el resultado
				$resultado = (new mpeligros())->buscar($cauditoria, $fservicio, $cproducto, $value->ctipopeligro, $value->norden);
				if (empty($resultado)) {
					$this->db->insert('ppeligrotipocliente', [
						'CAUDITORIAINSPECCION' => $cauditoria,
						'FSERVICIO' => $fservicio,
						'CPRODUCTOPELIGRO' => $cproducto,
						'CTIPOPELIGRO' => $value->ctipopeligro,
						'NORDEN' => $value->norden,
						'DDETALLEPELIGRO' => '',
						'CUSUARIOCREA' => $this->session->userdata('s_cusuario'),
						'TCREACION' => date('Y-m-d H:i:s'),
						'CUSUARIOMODIFICA' => null,
						'TMODIFICACION' => null,
						'SREGISTRO' => 'A',
					]);
				}
			}
			$this->result['status'] = 200;
			$this->result['message'] = 'Peligro registrado correctamente.';
		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}

		responseResult($this->result);
	}

}
