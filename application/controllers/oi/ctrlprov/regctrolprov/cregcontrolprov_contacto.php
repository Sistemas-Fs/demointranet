<?php

/**
 * Class cregcontrolprov_contacto
 *
 * @property mregctrolprov_contacto mcontacto
 * @property minspeccion minspeccion
 * @property mresumen_contacto mresumen_contacto
 */
class cregcontrolprov_contacto extends FS_Controller
{

	/**
	 * cregcontrolprov_contacto constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('at/ctrlprov/regctrolprov/mregctrolprov_contacto', 'mcontacto');
		$this->load->model('at/ctrlprov/inspctrolprov/minspeccion', 'minspeccion');
		$this->load->model('at/ctrlprov/inspctrolprov/mresumen_contacto');
	}

	/**
	 * Lista de contactos principales
	 */
	public function lista()
	{
		$cauditoria = $this->input->post('cauditoria');
		$items = $this->mcontacto->lista($cauditoria);
		echo json_encode(['items' => $items]);
	}

	/**
	 * Lista de contactos para cartas
	 */
	public function lista_cartas()
	{
		$cauditoria = $this->input->post('cauditoria');
		$items = $this->mcontacto->lista($cauditoria, true);
		echo json_encode(['items' => $items]);
	}

	public function autocompletado()
	{
		$busuqeda = $this->input->get('search');
		$cauditoria = $this->input->get('cauditoria');

		$inspeccion = $this->minspeccion->buscarCauditoria($cauditoria);
		$items = [];
		if (!empty($inspeccion)) {
			$items = $this->mcontacto->autocompletado($busuqeda, $inspeccion->CPROVEEDORCLIENTE, $inspeccion->CMAQUILADORCLIENTE);
		}
		echo json_encode(['items' => $items]);
	}

	public function get_establecimientos()
	{
		$ccliente = $this->input->post('ccliente');
		$items = $this->mcontacto->getEstablecimientos($ccliente);
		echo json_encode(['items' => $items]);
	}

	public function guardar()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {

			$ccontacto = $this->input->post('ccontacto');
			$cauditoria = $this->input->post('cauditoria');
			$res_contact_apepat = $this->input->post('apepat');
			$res_contact_apemat = $this->input->post('apemat');
			$res_contact_nombres = $this->input->post('nombres');
//			$nombre = $this->input->post('res_contact_apepat');
			$cargo = $this->input->post('cargo');
			$email = $this->input->post('email');
			$telefonos = $this->input->post('telefonos');
			$tipo = $this->input->post('tipo');
			$ccliente = $this->input->post('ccliente');
			$cestablecimiento = $this->input->post('cestablecimiento');

			$inspeccion = $this->minspeccion->buscarCauditoria($cauditoria);
			if (empty($inspeccion)) {
				throw new Exception('La inspección no pudo ser encontrada.');
			}
			$cliente = $this->mresumen_contacto->buscarCliente($ccliente);
			if (empty($cliente)) {
				throw new Exception('El cliente no pudo ser encontrado.');
			}

			if (empty($cestablecimiento)) {
				throw new Exception('Debes elegir un establecimiento.');
			}

			if ($tipo == 1) {
				$ccontacto = $this->mcontacto->guardar(
					$ccontacto,
					$inspeccion->CAUDITORIAINSPECCION,
					$ccliente,
					$cestablecimiento,
					'O',
					null,
					trim($res_contact_apepat),
					trim($res_contact_apemat),
					trim($res_contact_nombres),
					$cargo,
					$email,
					$telefonos,
					$this->session->userdata('s_cusuario')
				);
			} else {
				$validar = $this->mcontacto->buscar($inspeccion->CAUDITORIAINSPECCION, $ccontacto);
				// solo si aun no estan relacionados
				if (empty($validar)) {
					$this->db->insert('pcontactoxservicio', [
						'CAUDITORIAINSPECCION' => $inspeccion->CAUDITORIAINSPECCION,
						'CCONTACTO' => $ccontacto,
						'STIPOCONTACTO' => 'O',
						'SACCIONCONTACTO' => null,
						'CUSUARIOCREA' => $this->session->userdata('s_cusuario'),
						'TCREACION' => date('Y-m-d H:i:s'),
						'TMODIFICACION' => null,
						'CUSUARIOMODIFICA' => null,
						'SREGISTRO' => 'A',
					]);
				}
			}

			$this->result['status'] = 200;
			$this->result['message'] = 'Contacto creado correctamente.';
			$this->result['data'] = $ccontacto;

		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

	/**
	 * Elimina un contacto
	 */
	public function eliminar()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {
			$cauditoria = $this->input->post('cauditoria');
			$contact_id = $this->input->post('contact_id');

			$responsable = $this->mcontacto->buscar($cauditoria, $contact_id);
			if (empty($responsable)) {
				throw new Exception('El contacto no pudo ser encontrado.');
			}

			$resp = $this->db->delete('pcontactoxservicio', [
				'CAUDITORIAINSPECCION' => $responsable->CAUDITORIAINSPECCION,
				'CCONTACTO' => $responsable->CCONTACTO,
			]);

			if ($resp) {
//				$this->db->delete('MCONTACTO', ['CCONTACTO' => $responsable->CCONTACTO]);
			}

			$this->result['status'] = 200;
			$this->result['message'] = 'Responsable eliminado correctamente.';

		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

}
