<?php

/**
 * Class cmant_checklist
 * @property mcheclist mcheclist
 * @property masociar masociar
 * @property mrequisito mrequisito
 */
class cchecklist extends FS_Controller
{

	/**
	 * cmant_checklist constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('oi/ctrlprov/checklist/mcheclist');
		$this->load->model('oi/ctrlprov/checklist/masociar');
		$this->load->model('oi/ctrlprov/checklist/mrequisito');
	}

	/**
	 * Recursos para las areas
	 */
	public function recursos_area()
	{
		$result = $this->mcheclist->recursos_area('1');
		echo json_encode($result);
	}

	/**
	 * Recusos para el servicio
	 */
	public function recursos_servicio()
	{
		$area = $this->input->post_get('area');
		$result = $this->mcheclist->recursos_servicio('1', $area);
		echo json_encode($result);
	}

	/**
	 * Recusos para el sistema
	 */
	public function recursos_sistema()
	{
		$idArea = $this->input->post_get('id_area');
		$idServicio = $this->input->post_get('id_servicio');
		$result = $this->mcheclist->recursos_sistema('1', $idArea, $idServicio);
		echo json_encode($result);
	}

	/**
	 * Recusos para el organismo
	 */
	public function recursos_organismo()
	{
		$idSistema = $this->input->post_get('id_sistema');
		$result = $this->mcheclist->recursos_organismo($idSistema);
		echo json_encode($result);
	}

	/**
	 * Recusos para el organismo
	 */
	public function recursos_rubro()
	{
		$idOrganismo = $this->input->post_get('id_organismo');
		$result = $this->mcheclist->recursos_rubro($idOrganismo);
		echo json_encode($result);
	}

	/**
	 * Recursos para la lista de checklist
	 */
	public function lista()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {
			$busqueda = $this->input->get('busqueda');
			$carea = $this->input->get('carea');
			$cservicio = $this->input->get('cservicio');
			$busqueda = (empty($busqueda)) ? '%' : $busqueda;
			$carea = (empty($carea)) ? '%' : $carea;
			$cservicio = (empty($cservicio)) ? '%' : $cservicio;
			$result = $this->mcheclist->lista('2', $busqueda, '01', '01');
			$this->result['status'] = 200;
			$this->result['data'] = $result;
		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

	/**
	 * Recursos al crear nuevo checklist
	 */
	public function recursos_crear()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {
			$areas = $this->mcheclist->recursos_area('1');
			$noConformidad = $this->mcheclist->recursos_no_conformidad();
			$this->result['status'] = 200;
			$this->result['data'] = [
				'areas' => $areas,
				'no_conformidad' => $noConformidad
			];
		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

	/**
	 * Recursos al crear nuevo checklist
	 */
	public function recursos_editar($id)
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {
			$checklist = $this->mcheclist->buscar($id);
			if (empty($checklist)) {
				throw new Exception('El CheckList no pudo ser encontrado.');
			}
			$norma = $this->mcheclist->buscarNorma($checklist->CNORMA);
			$areas = $this->mcheclist->recursos_area('1');
			$noConformidad = $this->mcheclist->recursos_no_conformidad();
			$servicios = $this->mcheclist->recursos_servicio('2', $checklist->CAREA);
			$sistemas = $this->mcheclist->recursos_sistema('2', $checklist->CAREA, $checklist->CSERVICIO);
			$organismos = $this->mcheclist->recursos_organismo($norma->CSISTEMA);
			$rubros = $this->mcheclist->recursos_rubro($checklist->CNORMA);
			$this->result['status'] = 200;
			$this->result['data'] = [
				'checklist' => $checklist,
				'norma' => $norma,
				'areas' => $areas,
				'no_conformidad' => $noConformidad,
				'servicios' => $servicios,
				'sistemas' => $sistemas,
				'organismos' => $organismos,
				'rubros' => $rubros,
			];
		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

	/**
	 * Metodo para guardar la cabecera del checklist
	 */
	public function guardar()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {
			$id = $this->input->post('checklist_id');
			$descripcion = $this->input->post('checklist_descripcion');
			$area = $this->input->post('checklist_area');
			$servicio = $this->input->post('checklist_servicio');
//			$sistema = $this->input->post('checklist_sistema');
			$estado_chklist = $this->input->post('checklist_estado_chklist');
			$organismo = $this->input->post('checklist_organismo');
			$rubro = $this->input->post('checklist_rubro');
			$uso = $this->input->post('checklist_uso');
			$asignado = $this->input->post('checklist_asignado');
			$conformidad = $this->input->post('checklist_conformidad');
			$tipo = $this->input->post('checklist_tipo');
			$pesos = $this->input->post('checklist_pesos');
			$estado = $this->input->post('checklist_estado');
//			$formula = $this->input->post('checklist_formula');

			$s_cusuario = $this->session->userdata('s_idusuario');

			$uso = (!empty($uso)) ? 'S' : 'N';
			$asignado = (!empty($asignado)) ? 'C' : 'L';

			if (empty($descripcion)) {
				throw new Exception('Se debe ingresar la descripción del checklist.');
			}

			if (empty($organismo)) {
				throw new Exception('Debe elegir un organismo.');
			}

			if (empty($rubro)) {
				throw new Exception('Debe elegir un rubro.');
			}

			if (empty($conformidad)) {
				throw new Exception('Debe elegir un No Conformidad.');
			}

			$checklist = (new mcheclist())->guardar(
				$id,
				$organismo,
				$rubro,
				'2',
				$area,
				$servicio,
				$descripcion,
				$tipo,
				$conformidad,
				$estado_chklist,
				$uso,
				$s_cusuario,
				$estado,
				null,
				$pesos,
				$asignado
			);

			$this->result['status'] = 200;
			$this->result['data'] = $checklist;

		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

	/**
	 * Se obtiene los requisitos de un checklist
	 */
	public function obtener_requisitos()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		$idCheckList = $this->input->post('idCheckList');
		$requisitos = $this->mrequisito->obtenerRequisitos($idCheckList);
		echo json_encode(['items' => $requisitos]);
	}

	/**
	 * Elimina un checklist con sus requisitos
	 */
	public function eliminar()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {
			$idCheckList = $this->input->post('idCheckList');
			$checklist = $this->mcheclist->buscar($idCheckList);
			if (empty($checklist)) {
				throw new Exception('El checklist no pudo ser encontrado.');
			}
			$this->mcheclist->eliminar($idCheckList);
			$this->result['status'] = 200;
			$this->result['message'] = 'CheckList eliminado correctamente.';
		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

}
