<?php

/**
 * Class crequisito
 *
 * @property mrequisito mrequisito
 * @property mcheclist mcheclist
 */
class crequisito extends FS_Controller
{

	/**
	 * crequisito constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('oi/ctrlprov/checklist/mrequisito');
		$this->load->model('oi/ctrlprov/checklist/mcheclist');
	}

	/**
	 * Nuevo requisito
	 */
	public function nuevo_requisito()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		$idCheckList = $this->input->post('idCheckList');
		$padreRequisitos = $this->mrequisito->obtenerPadres($idCheckList);
		$valores = $this->mrequisito->obtenerValores();
		$this->result['status'] = 200;
		$this->result['data'] = [
			'padreRequisitos' => $padreRequisitos,
			'valores' => $valores,
		];
		responseResult($this->result);
	}

	/**
	 * Se obtiene el requisito ya sea
	 */
	public function obtener_requisito()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {

			$idCheckList = $this->input->post('idCheckList');
			$idRequisito = $this->input->post('idRequisito');

			if (empty($idCheckList) || empty($idRequisito)) {
				throw new Exception('Falta de información para buscar el requisito.');
			}

			$requisito = $this->mrequisito->obtenerRequisitos($idCheckList, $idRequisito);
			if (is_array($requisito)) {
				$requisito = $requisito[0];
			}
			$padreRequisitos = $this->mrequisito->obtenerPadres($idCheckList);
			$valores = $this->mrequisito->obtenerValores();

			$this->result['status'] = 200;
			$this->result['data'] = [
				'requisito' => $requisito,
				'padreRequisitos' => $padreRequisitos,
				'valores' => $valores,
			];

		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

	/**
	 * Crea o Actualiza el requisito
	 */
	public function guardar()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {

			$checklist_id = $this->input->post('checklist_id');
			$checklist_requisito_id = $this->input->post('checklist_requisito_id');
			$checklist_padre_requisito = $this->input->post('checklist_padre_requisito');
			$checklist_requisito_descripcion = $this->input->post('checklist_requisito_descripcion');
			$checklist_normativa = $this->input->post('checklist_normativa');
			$checklist_requisito_valor = $this->input->post('checklist_requisito_valor');
			$checklist_requisito_valorado = $this->input->post('checklist_requisito_valorado');
			$checklist_requisito_excluyente = $this->input->post('checklist_requisito_excluyente');
			$checklist_requisito_factores_criticos = $this->input->post('checklist_requisito_factores_criticos');
			$checklist_requisito_peso = $this->input->post('checklist_requisito_peso');
//			$checklist_requisito_orden = $this->input->post('checklist_requisito_orden');
			$checklist_requisito_numerador = $this->input->post('checklist_requisito_numerador');
			$checklist_requisito_ordenlista = $this->input->post('checklist_requisito_ordenlista');
//			$checklist_requisito_nivel = $this->input->post('checklist_requisito_nivel');
			$checklist_inspeccion_peso = $this->input->post('checklist_inspeccion_peso');
			$checklist_inspeccion_venc = $this->input->post('checklist_inspeccion_venc');
			$checklist_inspeccion_baja = $this->input->post('checklist_inspeccion_baja');
			$checklist_inspeccion_oc = $this->input->post('checklist_inspeccion_oc');

			$checklist_requisito_valorado = (empty($checklist_requisito_valorado)) ? 'N' : 'S';
			$checklist_requisito_factores_criticos = (empty($checklist_requisito_factores_criticos)) ? null : 1;
			$checklist_inspeccion_peso = (empty($checklist_inspeccion_peso)) ? null : 1;
			$checklist_inspeccion_venc = (empty($checklist_inspeccion_venc)) ? null : 1;
			$checklist_inspeccion_baja = (empty($checklist_inspeccion_baja)) ? null : 1;
			$checklist_inspeccion_oc = (empty($checklist_inspeccion_oc)) ? null : 1;

			$checklist = $this->mcheclist->buscar($checklist_id);
			if (empty($checklist)) {
				throw new Exception('El CheckList no pudo ser encontrado.');
			}

			if (empty($checklist_requisito_valor)) {
				throw new Exception('Debe elegir un valor para el requisito.');
			}

			if (empty($checklist_padre_requisito)) {
				$checklist_padre_requisito = '0';
			}

			$dnumerador = $checklist_requisito_numerador;
			$nordenLista = intval(substr($dnumerador, -1, 2));
			$dordenLista = $checklist_requisito_ordenlista;
			$sregistro = 'H';

//			$requisito = $this->mrequisito->buscar($checklist_id, $checklist_requisito_id);
//			$sregistro = 'H';
//			$requisitoPadre = null;
//			if (!empty($requisito)) {
//				$nordenLista = $requisito->NORDENLISTA;
//				$dordenLista = $requisito->DORDENLISTA;
//				$dnumerador = $requisito->DNUMERADOR;
//				$sregistro = $requisito->SREQUISITO;
//			} else {
//				$nordenLista = '1';
//				$dordenLista = '01';
//				$dnumerador = '1';
//				// Se busca con la raiz
//				if ($checklist_padre_requisito === '0') {
//					$requisitoHermanoMayor = $this->mrequisito->buscarHermanoMayor($checklist_id, $checklist_padre_requisito);
//					// Si no existe el padre toma los valores predeterminados
//					if (!empty($requisitoHermanoMayor)) {
//						$dnumerador = $requisitoHermanoMayor->DORDENLISTA + 1;
//						$dordenLista = str_pad($dnumerador, 2, '0', STR_PAD_LEFT);
//						$nordenLista = $dnumerador;
//					}
//				} else {
//					// Se busca el padre
//					$requisitoPadre = $this->mrequisito->buscarPadre($checklist_id, $checklist_padre_requisito);
//					if (empty($requisitoPadre)) {
//						throw new Exception('El Padre Requisito no pudo ser encontrado.');
//					}
//					$requisitoHermanoMayor = $this->mrequisito->buscarHermanoMayor($checklist_id, $requisitoPadre->CREQUISITOCHECKLIST);
//					if (empty($requisitoHermanoMayor)) {
//						$dordenLista = $requisitoPadre->DORDENLISTA . '01';
//						$nordenLista = '1';
//						$dnumerador = substr(str_replace('0', '.', $dordenLista), 1);
//					} else {
//						$ultimoDigito = intval(substr($requisitoHermanoMayor->DORDENLISTA, -1, 2)) + 1;
//						$dordenLista = $requisitoHermanoMayor->DORDENLISTA . str_pad($ultimoDigito, 2, '0', STR_PAD_LEFT);
//						$nordenLista = $ultimoDigito;
//						$dnumerador = substr(str_replace('0', '.', $dordenLista), 1);
//					}
//				}
//			}

			$s_cusuario = $this->session->userdata('s_idusuario');

			$requisito = $this->mrequisito->guardar(
				$checklist_id,
				$checklist_requisito_id,
				trim($checklist_requisito_descripcion),
				trim($checklist_normativa),
				$checklist_padre_requisito,
				$sregistro,
				$checklist_requisito_valorado,
				$nordenLista,
				$dordenLista,
				$dnumerador,
				$checklist_requisito_valor,
				$checklist_requisito_excluyente,
				$checklist_requisito_peso,
				$s_cusuario,
				'A',
				$checklist_requisito_factores_criticos,
				null,
				null,
				$checklist_inspeccion_peso,
				$checklist_inspeccion_venc,
				$checklist_inspeccion_baja,
				$checklist_inspeccion_oc
			);

			// Solo si el padre existe se cambia su estado a P
			if (!empty($requisitoPadre)) {
				$this->mrequisito->actualizar(
					$requisitoPadre->CCHECKLIST,
					$requisitoPadre->CREQUISITOCHECKLIST,
					['SREQUISITO' => 'P']
				);
			}

			$this->result['status'] = 200;
			$this->result['message'] = 'Requisito obtenido correctamente';
			$this->result['data'] = $requisito;

		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

	/**
	 * Elimina un requisito del checklist
	 */
	public function eliminar()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {
			$idCheckList = $this->input->post('idCheckList');
			$idRequisitoCheckList = $this->input->post('idRequisitoCheckList');
			$requisito = $this->mrequisito->buscar($idCheckList, $idRequisitoCheckList);
			if (empty($requisito)) {
				throw new Exception('El requisito no pudo ser encontrado.');
			}
			$this->mrequisito->eliminar($idCheckList, $idRequisitoCheckList);
			$this->result['status'] = 200;
			$this->result['message'] = 'El requisito fue eliminado correctamente.';
		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

	/**
	 * Se obtiene la orden de lista posible a elegir
	 */
	public function obtener_order_lista()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {
			$idCheckList = $this->input->post('idCheckList');
			$idRequisitoPadre = $this->input->post('idRequisitoPadre');
			$idRequisitoId = $this->input->post('idRequisitoId');

			$dOrdenLista = [];
			if (!empty($idRequisitoId)) {
				// Solo se busca el requisito actual
				$requisito = $this->mrequisito->buscar($idCheckList, $idRequisitoId);
				if (!empty($requisito)) {
					$dOrdenLista[] = [
						'CREQUISITOCHECKLIST' => $requisito->CREQUISITOCHECKLIST,
						'DNUMERADOR' => $requisito->DNUMERADOR,
						'DORDENLISTA' => $requisito->DORDENLISTA,
						'NORDENLISTA' => $requisito->NORDENLISTA,
						'NIVEL' => 'Nivel ' . count(explode('.', $requisito->DNUMERADOR)),
					];
				}
			} else {
				// Se buscan los hermanos para uno nuevo
				$hermanos = $this->mrequisito->buscarHermanos($idCheckList, $idRequisitoPadre);
				if (empty($hermanos)) {
					$requisitoActual = $this->mrequisito->buscar($idCheckList, $idRequisitoPadre);
//					// Si no existe el padre toma los valores predeterminados
					if (!empty($requisitoActual)) {
						$dnumerador = $requisitoActual->DNUMERADOR . '.1';
						$nordenLista = '01';
						$dordenLista = $requisitoActual->DORDENLISTA . str_pad($nordenLista, 2, '0', STR_PAD_LEFT);
						$nivel = 'Nivel ' . count(explode('.', $dnumerador));
					} else {
						$dnumerador = '1';
						$nordenLista = '01';
						$dordenLista = '01';
						$nivel = 'Nivel 1';
					}
					$dOrdenLista[] = [
						'CREQUISITOCHECKLIST' => 0,
						'DNUMERADOR' => $dnumerador,
						'DORDENLISTA' => $dordenLista,
						'NORDENLISTA' => $nordenLista,
						'NIVEL' => $nivel,
					];
				} else {
					foreach ($hermanos as $key => $requisito) {
						$dOrdenLista[] = [
							'CREQUISITOCHECKLIST' => $requisito->CREQUISITOCHECKLIST,
							'DNUMERADOR' => $requisito->DNUMERADOR,
							'DORDENLISTA' => $requisito->DORDENLISTA,
							'NORDENLISTA' => $requisito->NORDENLISTA,
							'NIVEL' => 'Nivel ' . count(explode('.', $requisito->DNUMERADOR)),
						];
					}
					// Se encuentra el hermano mayor
					$hermanoMayor = $this->mrequisito->buscar($idCheckList, $hermanos[0]->CREQUISITOCHECKLISTPADRE);
					$lenOrdenLista = strlen($dOrdenLista[0]['DORDENLISTA']);
					// Se crea un nuevo codigo para el nuevo hermano
					$ultimoDigito = intval(substr($dOrdenLista[0]['DORDENLISTA'], $lenOrdenLista - 2, $lenOrdenLista)) + 1;
					$DORDENLISTA = substr($dOrdenLista[0]['DORDENLISTA'], 0, $lenOrdenLista - 2) . str_pad($ultimoDigito, 2, '0', STR_PAD_LEFT);
					$NORDENLISTA = $ultimoDigito;
					if (!empty($hermanoMayor)) {
						$DNUMERADOR = $hermanoMayor->DNUMERADOR . '.' . $ultimoDigito;
					} else {
						$DNUMERADOR = $ultimoDigito;
					}
					$nuevoHermano = [
						'CREQUISITOCHECKLIST' => 0,
						'DNUMERADOR' => $DNUMERADOR,
						'DORDENLISTA' => $DORDENLISTA,
						'NORDENLISTA' => $NORDENLISTA,
						'NIVEL' => 'Nivel ' . count(explode('.', $DNUMERADOR)),
					];
					$dOrdenLista = array_merge([$nuevoHermano], $dOrdenLista);
				}
			}

			$this->result['status'] = 200;
			$this->result['data'] = $dOrdenLista;
		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

}
