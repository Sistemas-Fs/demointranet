<?php

/**
 * Class casociarextends
 * @property masociar masociar
 */
class casociar extends FS_Controller
{

	/**
	 * cmant_checklist constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('oi/ctrlprov/checklist/masociar');
	}

	/**
	 * Busqueda de clientes
	 */
	public function autocompletado()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		$busqueda = $this->input->get('busqueda');
		$items = $this->masociar->autoCompletable($busqueda);
		echo json_encode(['items' => $items]);
	}

	/**
	 * Lista de clientes de un checklist
	 */
	public function listar()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {
			$idCheckList = $this->input->post('idCheckList');
			$items = $this->masociar->lista($idCheckList);
			echo json_encode(['items' => $items]);
		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
	}
	
	/**
	 * Guarda el asociado de clientes
	 */
	public function guardar()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {
			$idCliente = $this->input->post('idCliente');
			$idCheckList = $this->input->post('idCheckList');
			if (empty($idCliente)) {
				throw new Exception('Debes elegir un cliente.');
			}
			if (empty($idCheckList)) {
				throw new Exception('Se debe informar el CheckList asociado.');
			}

			$validar = $this->masociar->buscar($idCheckList, $idCliente);
			if (!empty($validar)) {
				throw new Exception('El Cliente ya esta asociado al checklist.');
			}

			$s_cusuario = $this->session->userdata('s_idusuario');

			$datos = $this->masociar->guardar($idCheckList, $idCliente, $s_cusuario);
			// Se actualiza el asignado a clientes del checklist
			$this->masociar->actualizar_checklist($idCheckList);

			$this->result['status'] = 200;
			$this->result['data'] = $datos;

		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

	/**
	 * Elimina el cliente asociado al checklist
	 */
	public function eliminar()
	{
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
		try {
			$idCliente = $this->input->post('idCliente');
			$idCheckList = $this->input->post('idCheckList');
			if (empty($idCliente)) {
				throw new Exception('Debes elegir un cliente.');
			}
			if (empty($idCheckList)) {
				throw new Exception('Se debe informar el CheckList asociado.');
			}
			$this->masociar->eliminar($idCheckList, $idCliente);
			$this->result['status'] = 200;
		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}

}
