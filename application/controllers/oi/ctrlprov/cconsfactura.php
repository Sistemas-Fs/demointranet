<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Style\Conditional;
use PhpOffice\PhpSpreadsheet\Writer\IWriter;
use PhpOffice\PhpSpreadsheet\IOFactory;

class Cconsfactura extends FS_Controller
{
	function __construct()
	{
		parent:: __construct();
		$this->load->model('at/ctrlprov/mconsfactura');
		$this->load->model('mglobales');
		$this->load->library('encryption');
		$this->load->helper(array('form', 'url', 'download', 'html', 'file'));
		$this->load->library('form_validation');
	}

	/** CONS. FACTURACIONES **/
	public function getbuscarfactura()
	{    // Busqueda
		$varnull = '';
		$celestado = '';

		$ccliente = $this->input->post('ccliente');
		$cproveedor = $this->input->post('cproveedor');
		$anio = $this->input->post('anio');
		$mes = $this->input->post('mes');
		$cestado = $this->input->post('estado');
		$monto = $this->input->post('monto');

		if (isset($cestado)) {
			foreach ($cestado as $dest) {
				$celestado = $dest . ',' . $celestado;
			}
			$countest = strlen($celestado);
			$celestado = substr($celestado, 0, $countest - 1);
		}

		$parametros = array(
			'@ccliente' => $ccliente,
			'@cproveedor' => ($this->input->post('cproveedor') == $varnull) ? '%' : $cproveedor,
			'@anio' => $anio,
			'@mes' => ($this->input->post('mes') == $varnull) ? 0 : $mes,
			'@estado' => (empty($celestado)) ? '029,031,032,515' : $celestado,
			'@monto' => ($this->input->post('monto') == $varnull) ? -1000 : $monto,
		);

		$resultado = $this->mconsfactura->getbuscarfactura($parametros);
		echo json_encode($resultado);
	}

	public function exportar()
	{
		try {
			$ccliente = $this->input->post('ccliente');
			$cproveedor = $this->input->post('cproveedor');
			$anio = $this->input->post('anio');
			$mes = $this->input->post('mes');
			$cestado = $this->input->post('estado');
			$monto = $this->input->post('monto');

			$celestado = '029,031,032,515';
			if (isset($cestado)) {
				foreach ($cestado as $dest) {
					$celestado = $dest . ',' . $celestado;
				}
				$countest = strlen($celestado);
				$celestado = substr($celestado, 0, $countest - 1);
			}

			$parametros = array(
				'@ccliente' => $ccliente,
				'@cproveedor' => (empty($this->input->post('cproveedor'))) ? '%' : $cproveedor,
				'@anio' => $anio,
				'@mes' => (empty($this->input->post('mes'))) ? 0 : $mes,
				'@estado' => $celestado,
				'@monto' => (empty($this->input->post('monto'))) ? -1000 : $monto,
			);

			$resultado = $this->mconsfactura->getbuscarfactura($parametros);

			$spreadsheet = new Spreadsheet();
			$sheet = $spreadsheet->getActiveSheet();
			$sheet->setTitle('Cuadro de Facturación');

			$spreadsheet->getDefaultStyle()
				->getFont()
				->setName('Arial')
				->setSize(10);

			$sheet->setCellValue('A1', 'CUADRO DE FACTURACIÓN')
				->mergeCells('A1:N1');

			$sheet->setCellValue('A2', 'Cliente')
				->setCellValue('B2', 'Proveedor')
				->setCellValue('C2', 'N° RUC')
				->setCellValue('D2', 'Area Cliente')
				->setCellValue('E2', 'Jerarquia')
				->setCellValue('F2', 'Importe S/. Total (sin IGV)')
				->setCellValue('G2', 'Mes Facturacion')
				->setCellValue('H2', 'Direccion de Planta')
				->setCellValue('I2', 'Linea Inspeccion')
				->setCellValue('J2', 'Fecha Inspeccion')
				->setCellValue('K2', 'N° Informe')
				->setCellValue('L2', 'Resultado CheckList')
				->setCellValue('M2', 'Resultado Texto')
				->setCellValue('N2', 'Estado')
				->setCellValue('O2', 'Inspector');

			$irow = 3;
			foreach ($resultado as $key => $value) {
				$sheet->setCellValue('A' . $irow, $value->cliente);
				$sheet->setCellValue('B' . $irow, $value->proveedor);
				$sheet->setCellValue('C' . $irow, $value->nruc);
				$sheet->setCellValue('D' . $irow, $value->dareacliente);
				$sheet->setCellValue('E' . $irow, $value->DJERARQUIA);
				$sheet->setCellValue('F' . $irow, $value->costo);
				$sheet->setCellValue('G' . $irow, $value->mes_fact);
				$sheet->setCellValue('H' . $irow, $value->dir_estab);
				$sheet->setCellValue('I' . $irow, $value->dlineaclientee);
				$sheet->setCellValue('J' . $irow, $value->fservicio);
				$sheet->setCellValue('K' . $irow, $value->dinformefinal);
				$sheet->setCellValue('L' . $irow, $value->RESULTADOCHECKLIST);
				$sheet->setCellValue('M' . $irow, $value->RESULTADOTEXTO);
				$sheet->setCellValue('N' . $irow, $value->estado);
				$sheet->setCellValue('O' . $irow, $value->INSPECTOR);
				$irow++;
			}

			$titulo = [
				'font' => [
					'name' => 'Arial',
					'size' => 12,
					'color' => array('rgb' => 'FFFFFF'),
					'bold' => true,
				],
				'fill' => [
					'fillType' => Fill::FILL_SOLID,
					'startColor' => [
						'rgb' => '29B037'
					]
				],
				'borders' => [
					'allBorders' => [
						'borderStyle' => Border::BORDER_THIN,
						'color' => [
							'rgb' => '000000'
						]
					]
				],
				'alignment' => [
					'horizontal' => Alignment::HORIZONTAL_CENTER,
					'vertical' => Alignment::VERTICAL_CENTER,
					'wrapText' => true,
				],
			];
			$cabecera = [
				'font' => [
					'name' => 'Arial',
					'size' => 10,
					'color' => array('rgb' => 'FFFFFF'),
					'bold' => true,
				],
				'fill' => [
					'fillType' => Fill::FILL_SOLID,
					'startColor' => [
						'rgb' => '29B037'
					]
				],
				'borders' => [
					'allBorders' => [
						'borderStyle' => Border::BORDER_THIN,
						'color' => [
							'rgb' => '000000'
						]
					]
				],
				'alignment' => [
					'horizontal' => Alignment::HORIZONTAL_CENTER,
					'vertical' => Alignment::VERTICAL_CENTER,
					'wrapText' => true,
				],
			];
			$sheet->getStyle('A1:N1')->applyFromArray($titulo);
			$sheet->getStyle('A2:N2')->applyFromArray($cabecera);

			foreach (range('A', 'N') as $column) {
				$sheet->getColumnDimension($column)->setAutoSize(true);
			}

			$writer = new Xlsx($spreadsheet);
			$filename = 'cuadro_facturacion_' . date('Ymd') . '.xlsx';
			$path = RUTA_ARCHIVOS . '../../temp/' . $filename;
			$writer->save($path);

			$this->result['status'] = 200;
			$this->result['message'] = 'Se realizo la exportación correctamente.';
			$this->result['data'] = $filename;
		} catch (Exception $ex) {
			$this->result['message'] = $ex->getMessage();
		}
		responseResult($this->result);
	}
}

?>
