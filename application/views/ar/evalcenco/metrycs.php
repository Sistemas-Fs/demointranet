<?php
    $idusu = $this->session->userdata('s_idusuario');
?>

<style>
    .bootstrap-switch .bootstrap-switch-handle-off.bootstrap-switch-default, .bootstrap-switch .bootstrap-switch-handle-on.bootstrap-switch-default {
        background: #28a745 !important;
        color: #1f2d3d !important;
    }
    fieldset.scheduler-border{
        border: 2px groove #2DC87B !important;
        padding: 0 0.8em 1.4em 0.8em !important;
        margin: 0 0.8em 1.5em 0.8em !important;
        box-shadow: 0px 0px 0px 0px #000 !important;
    }
    legend.scheduler-border{
        font-size: 1.2em !important;
        font-weight: bold !important;
        text-align: left !important;
        width: auto;
        padding: 0 10px;
        border-bottom: none;
        font-family: Verdana;

    }
</style>

<!-- content-header -->
<div class="content-header">   
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">METRYCS
            <small></small>
        </h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?php echo public_base_url(); ?>main"> <i class="fas fa-tachometer-alt"></i>Home</a></li>
          <li class="breadcrumb-item active">Metrycs</li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card card-success card-outline card-tabs">
					<div class="card-header p-0 pt-1 border-bottom-0">
						<ul class="nav nav-tabs" id="tabptcliente" style="background-color: #28a745;" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" style="color: #000000;" id="tab-list-listado"
								   data-toggle="pill" href="#tabReg1" role="tab"
								   aria-controls="tabReg1" aria-selected="true">DISTRIBUCIÓN DE ESTADOS</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" style="color: #000000;" id="tab-list-registro"
								   data-toggle="pill" href="#tabReg2" role="tab"
								   aria-controls="tabReg2" aria-selected="false">SOLICITUDES</a>
							</li>
						</ul>
					</div>
					<div class="card-body">
						<div class="tab-content">
							<div class="tab-pane fade show active" id="tabReg1" role="tabpanel">
								<?php $this->load->view('ar/evalcenco/vdistribucion');?>
							</div>
							<div class="tab-pane fade" id="tabReg2" role="tabpanel">
                                <?php $this->load->view('ar/evalcenco/vsolicitudes');?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- /.Main content -->


<!-- Script Generales -->
<script type="text/javascript">
    var baseurl = "<?php echo base_url();?>"; 
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.5.0/chart.min.js"></script>