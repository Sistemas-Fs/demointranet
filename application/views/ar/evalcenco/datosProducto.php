<fieldset class="scheduler-border">
    <legend class="scheduler-border text-primary">
        Datos del Producto
    </legend>
    <div class="card-body"
        style="background-color:#ffffff;">
        <form action="<?php echo base_url('ar/evalcenco/cconteval/guardarproducto') ?>" method="POST"
            accept-charset="UTF-8" id="frmProducto">
            <input type="hidden" class="d-none" id="id_producto" name="id_producto" value="">
            <input type="hidden" class="d-none" id="id_eval" name="id_eval" value="">
            <div class="form-group row">
                <label for="cliente_text" class="col-xl-2 col-lg-2 col-md-4 col-sm-12 col-12">
                    Cliente
                </label>
                <div class="col-xl-10 col-lg-10 col-md-8 col-sm-12 col-12">
                    <input type="text" class="form-control" aria-label="" readonly maxlength="0"
                        id="cliente_text"
                        value="CENCOSUD RETAIL PERU S.A."/>
                </div>
            </div>
            <div class="form-group row">
                <label for="tipo_producto_text" class="col-xl-2 col-lg-2 col-md-4 col-sm-12 col-12">
                    Tipo de Producto
                </label>
                <div class="col-xl-10 col-lg-10 col-md-8 col-sm-12 col-12">
                    <input type="text" class="form-control" aria-label="" readonly maxlength="0"
                        id="tipo_producto_text" name="tipo_producto_text"
                        value="Alimento"/>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="form-group row">
                        <label for="codigo_producto"
                            class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                            Código del Producto
                        </label>
                        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                            <input type="text" class="form-control" aria-label=""
                                id="codigo_producto" name="codigo_producto"
                                value=""/>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="form-group row">
                        <label for="categoria_id" class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                            Categoría
                        </label>
                        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                            <select class="custom-select" aria-label=""
                                    id="categoria_id" name="categoria_id">
                                    <option value="0" selected>Lima</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="form-group row">
                        <label for="producto_rs" class="col-xl-4 col-lg-2 col-md-4 col-sm-12 col-12">
                            Registro Sanitario / NSO
                            <span class="fs-requerido text-danger">*</span>
                        </label>
                        <div class="col-xl-8 col-lg-10 col-md-8 col-sm-12 col-12">
                            <input type="text" class="form-control" aria-label=""
                                id="producto_rs" name="producto_rs"
                                value=""/>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="form-group row">
                        <label for="producto_fecha_inicio" class="col-xl-5 col-lg-4 col-md-4 col-sm-12 col-12">
                            F. Emisión
                            <span class="fs-requerido text-danger">*</span>
                        </label>
                        <div class="col-xl-7 col-lg-8 col-md-8 col-sm-12 col-12">
                            <input type="text" class="form-control" aria-label=""
                                id="producto_fecha_inicio" name="producto_fecha_inicio"
                                value=""/>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="form-group row">
                        <label for="producto_fecha_vencimiento"
                            class="col-xl-6 col-lg-4 col-md-4 col-sm-12 col-12">
                            F. Vencimiento
                            <span class="fs-requerido text-danger">*</span>
                        </label>
                        <div class="col-xl-6 col-lg-8 col-md-8 col-sm-12 col-12">
                            <input type="text" class="form-control" aria-label=""
                                id="producto_fecha_vencimiento" name="producto_fecha_vencimiento"
                                value=""/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="producto_descripcion" class="col-xl-2 col-lg-2 col-md-4 col-sm-12 col-12">
                    Descripción
                </label>
                <div class="col-xl-10 col-lg-10 col-md-8 col-sm-12 col-12">
                    <input type="text" class="form-control" aria-label=""
                        id="producto_descripcion" name="producto_descripcion"
                        value=""/>
                </div>
            </div>
            <div class="form-group row">
                <label for="producto_nombre" class="col-xl-2 col-lg-2 col-md-4 col-sm-12 col-12">
                    Nombre de Producto
                    <span class="fs-requerido text-danger">*</span>
                </label>
                <div class="col-xl-10 col-lg-10 col-md-8 col-sm-12 col-12">
                    <input type="text" class="form-control" aria-label=""
                        id="producto_nombre" name="producto_nombre"
                        value=""/>
                </div>
            </div>
            <div class="form-group row">
                <label for="marca_id" class="col-xl-2 col-lg-2 col-md-4 col-sm-12 col-12">
                    Marca
                </label>
                <div class="col-xl-10 col-lg-10 col-md-8 col-sm-12 col-12">
                    <div class="row">
                        <div class="col-xl-10 col-lg-10 col-md-12 col-sm-12 col-12">
                            <select class="custom-select" aria-label=""
                                    id="marca_id" name="marca_id">
                                    <option value="0" selected>METRO</option>
                                    <option value="1">METRO-WONG-DULCE PASION</option>
                                    <option value="2">METRO-WONG-CUISINE & CO</option>
                                    <option value="3">METRO-WONG-DULCE PASION-DULCE PASION EXPRESS</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="producto_presentacion" class="col-xl-2 col-lg-2 col-md-4 col-sm-12 col-12">
                    Presentación
                </label>
                <div class="col-xl-10 col-lg-10 col-md-8 col-sm-12 col-12">
                <textarea class="form-control" aria-label=""
                        id="producto_presentacion" name="producto_presentacion"
                        rows="3"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                    <div class="form-group row">
                        <label for="fabricante_id"
                            class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                            Fabricante
                        </label>
                        <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
                            <div class="row">
                                <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                                    <select class="custom-select" aria-label=""
                                            id="fabricante_id" name="fabricante_id">
                                            <option value="0" selected>CENCOSUD RETAIL PERU S.A.</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                    <div class="form-group row">
                        <label for="producto_pais" class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                            País
                        </label>
                        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                            <select class="custom-select" aria-label=""
                                    id="producto_pais" name="producto_pais">
                                    <option value="0" selected>Perú</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="direccion_fabricante" class="col-xl-2 col-lg-2 col-md-4 col-sm-12 col-12">
                    Dirección Fabricante
                </label>
                <div class="col-xl-10 col-lg-10 col-md-8 col-sm-12 col-12">
                    <input type="text" class="form-control" aria-label=""
                        id="direccion_fabricante" name="direccion_fabricante"
                        value=""/>
                </div>
            </div>
            <div class="form-group row">
                <label for="vida_util" class="col-xl-2 col-lg-2 col-md-4 col-sm-12 col-12">
                    Vida Util
                </label>
                <div class="col-xl-10 col-lg-10 col-md-8 col-sm-12 col-12">
                    <input type="text" class="form-control" aria-label="" maxlength="250"
                        id="vida_util" name="vida_util"
                        value=""/>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4 col-lg-5 col-md-12 col-sm-12 col-12">
                    <div class="form-group row">
                        <label for="producto_estado" class="col-xl-6 col-lg-5 col-md-4 col-sm-12 col-12">
                            Estado
                            <span class="fs-requerido text-danger">*</span>
                        </label>
                        <div class="col-xl-6 col-lg-7 col-md-8 col-sm-12 col-12">
                            <select class="custom-select" aria-label=""
                                    id="producto_estado" name="producto_estado">
                                <option value="1" selected>Activo</option>
                                <option value="0">Inactivo</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer text-right">
                <button type="button" role="button" class="btn btn-primary"
                        id="btnAgregarProducto"
                        data-toggle="modal" data-target="#modalSelectProduct">
                    <i class="fa fa-plus"></i> Cargar Producto
                </button>
                <button type="button" class="btn btn-danger btn-producto-guardar">
                    <i class="fa fa-save"></i> Guardar Producto
                </button>
            </div>
        </form>
    </div>
</fieldset>