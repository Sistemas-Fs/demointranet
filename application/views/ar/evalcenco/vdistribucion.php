<div class="row">
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">      
        <label>AÑO :</label>
        <select class="form-control" id="cboAnio" name="cboAnio" style="width: 100%;">
            <option value="2021" selected="selected">2021</option>
            <option value="2020">2020</option>
            <option value="2019">2019</option>
        </select>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">      
        <label>MES :</label>
        <select class="form-control" id="cboMes" name="cboMes" style="width: 100%;">
            <option value="0" selected="selected">Todos</option>
            <option value="1">Enero</option>
            <option value="2">Febrero</option>
            <option value="3">Marzo</option>
            <option value="4">Abril</option>
            <option value="5">Mayo</option>
            <option value="6">Junio</option>
            <option value="7">Julio</option>
            <option value="8">Agosto</option>
            <option value="9">Setiembre</option>
            <option value="10">Octubre</option>
            <option value="11">Noviembre</option>
            <option value="12">Diciembre</option>
        </select>
    </div>    
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
        <br>
        <button type="submit" id="btnBuscarDistri" class="btn btn-block btn-primary" style="width: 50%;"><i class="fa fa-search"></i>Buscar Resultados</button>
    </div>
</div>

<div class="row">
    <div class="col-12"> 
        <br>
        <fieldset class="scheduler-border">
            <legend class="scheduler-border text-primary">Tendencia por estados (%)</legend>
            <div class="row">
                <div class="col-sm-5">
                    <br>
                    <table id="tbltendenciaanualdistri" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                            <th>Estado Evaluación</th>
                            <th>Cantidad (Nro)</th>
                            <th>% Indicador</th>
                            </tr>
                        </thead> 
                        <tbody></tbody>
                    </table>
                </div>
                <div class="col-sm-7">
                    <canvas id="graftendenciaanualdistri" style="width: 100%; height: 200px;"></canvas>
                </div>
            </div>
        </fieldset>
    </div>
</div>