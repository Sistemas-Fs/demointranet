<tr class = "tramite_1">
    <td class="text-center">
        <span class="font-weight-bold">1</span>
    </td>
    <td class="text-center">
        <span class="text" >Ficha Técnica</span>
    </td>
    <td class="text-center">
        <span class="text" >Si</span>
    </td>
    <td class="text-center">
        <span class="text nombre_doc_tramite_1" id="nombre_doc_tramite_1"></span>
    </td>
    <td class="text-center">
        <select class="custom-select select_estado_doc" aria-label=""
                id="estado_1" name="estado_1" >
                <option value="0" selected>Pendiente</option>
                <option value="1">Aprobado</option>
                <option value="2">Observado</option>
        </select>
    </td>
    <td class="text-center">
        <div class="opciones-documentos">
            <button type="button" role="button" class="btn btn-light btn-cargar-documento" id="btn-cargar-documento-1"
                data-toggle="modal" data-target="#modalCargarDoc" >Subir Archivo</button>
            <button type="button" role="button" class="btn btn-light btn-opciones-documento" id="btn-opciones-documento-1"
                data-toggle="modal" data-target="#modalOpcionesDoc" >Opciones</button>
        </div>
    </td>
</tr>
<tr class = "tramite_2">
    <td class="text-center">
        <span class="font-weight-bold">2</span>
    </td>
    <td class="text-center">
        <span class="text" >Registro Sanitario</span>
    </td>
    <td class="text-center">
        <span class="text" >Si</span>
    </td>
    <td class="text-center">
        <span class="text nombre_doc_tramite_2" id="nombre_doc_tramite_2"></span>
    </td>
    <td class="text-center">
        <select class="custom-select select_estado_doc" aria-label=""
                id="estado_2" name="estado_2">
                <option value="0" selected>Pendiente</option>
                <option value="1">Aprobado</option>
                <option value="2">Observado</option>
        </select>
    </td>
    <td class="text-center">
    <div class="opciones-documentos">
            <button type="button" role="button" class="btn btn-light btn-cargar-documento" id="btn-cargar-documento-2"
                data-toggle="modal" data-target="#modalCargarDoc" >Subir Archivo</button>
            <button type="button" role="button" class="btn btn-light btn-opciones-documento" id="btn-opciones-documento-2"
                data-toggle="modal" data-target="#modalOpcionesDoc" >Opciones</button>
        </div>
    </td>
</tr>
<tr class = "tramite_3">
    <td class="text-center">
        <span class="font-weight-bold">3</span>
    </td>
    <td class="text-center">
        <span class="text" >Análisis Microbiológico y fisicoquímico</span>
    </td>
    <td class="text-center">
        <span class="text" >Si</span>
    </td>
    <td class="text-center">
        <span class="text nombre_doc_tramite_3" id="nombre_doc_tramite_3"></span>
    </td>
    <td class="text-center">
        <select class="custom-select select_estado_doc" aria-label=""
                id="estado_3" name="estado_3">
                <option value="0" selected>Pendiente</option>
                <option value="1">Aprobado</option>
                <option value="2">Observado</option>
        </select>
    </td>
    <td class="text-center">
        <div class="opciones-documentos">
            <button type="button" role="button" class="btn btn-light btn-cargar-documento" id="btn-cargar-documento-3"
                data-toggle="modal" data-target="#modalCargarDoc" >Subir Archivo</button>
            <button type="button" role="button" class="btn btn-light btn-opciones-documento" id="btn-opciones-documento-3"
                data-toggle="modal" data-target="#modalOpcionesDoc" >Opciones</button>
        </div>
    </td>
</tr>
<tr class = "tramite_4">
    <td class="text-center">
        <span class="font-weight-bold">4</span>
    </td>
    <td class="text-center">
        <span class="text" >Análisis Nutricional o Carta de Declaración Jurada</span>
    </td>
    <td class="text-center">
        <span class="text" >Si</span>
    </td>
    <td class="text-center">
        <span class="text nombre_doc_tramite_4" id="nombre_doc_tramite_4"></span>
    </td>
    <td class="text-center">
        <select class="custom-select select_estado_doc" aria-label=""
                id="estado_4" name="estado_4">
                <option value="0" selected>Pendiente</option>
                <option value="1">Aprobado</option>
                <option value="2">Observado</option>
        </select>
    </td>
    <td class="text-center">
        <div class="opciones-documentos">
            <button type="button" role="button" class="btn btn-light btn-cargar-documento" id="btn-cargar-documento-4"
                data-toggle="modal" data-target="#modalCargarDoc" >Subir Archivo</button>
            <button type="button" role="button" class="btn btn-light btn-opciones-documento" id="btn-opciones-documento-4"
                data-toggle="modal" data-target="#modalOpcionesDoc" >Opciones</button>
        </div>
    </td>
</tr>
<tr class = "tramite_5">
    <td class="text-center">
        <span class="font-weight-bold">5</span>
    </td>
    <td class="text-center">
        <span class="text" >Estudio de Vida Útil</span>
    </td>
    <td class="text-center">
        <span class="text" >En caso aplique</span>
    </td>
    <td class="text-center">
        <span class="text nombre_doc_tramite_5" id="nombre_doc_tramite_5"></span>
    </td>
    <td class="text-center">
        <select class="custom-select select_estado_doc" aria-label=""
                id="estado_5" name="estado_5">
                <option value="0" selected>Pendiente</option>
                <option value="1">Aprobado</option>
                <option value="2">Observado</option>
                <option value="3">No aplica</option>
        </select>
    </td>
    <td class="text-center">
        <div class="opciones-documentos">
            <button type="button" role="button" class="btn btn-light btn-cargar-documento" id="btn-cargar-documento-5"
                data-toggle="modal" data-target="#modalCargarDoc" >Subir Archivo</button>
            <button type="button" role="button" class="btn btn-light btn-opciones-documento" id="btn-opciones-documento-5"
                data-toggle="modal" data-target="#modalOpcionesDoc" >Opciones</button>
        </div>
    </td>
</tr>
<tr class = "tramite_6">
    <td class="text-center">
        <span class="font-weight-bold">6</span>
    </td>
    <td class="text-center">
        <span class="text" >Etiqueta Original</span>
    </td>
    <td class="text-center">
        <span class="text" >Si</span>
    </td>
    <td class="text-center">
        <span class="text nombre_doc_tramite_6" id="nombre_doc_tramite_6"></span>
    </td>
    <td class="text-center">
        <select class="custom-select select_estado_doc" aria-label=""
                id="estado_6" name="estado_6">
                <option value="0" selected>Pendiente</option>
                <option value="1">Aprobado</option>
                <option value="2">Observado</option>
        </select>
    </td>
    <td class="text-center">
        <div class="opciones-documentos">
            <button type="button" role="button" class="btn btn-light btn-cargar-documento" id="btn-cargar-documento-6"
                data-toggle="modal" data-target="#modalCargarDoc" >Subir Archivo</button>
            <button type="button" role="button" class="btn btn-light btn-opciones-documento" id="btn-opciones-documento-6"
                data-toggle="modal" data-target="#modalOpcionesDoc" >Opciones</button>
        </div>
    </td>
</tr>
<tr class = "tramite_7">
    <td class="text-center">
        <span class="font-weight-bold">7</span>
    </td>
    <td class="text-center">
        <span class="text" >Sustento Claims</span>
    </td>
    <td class="text-center">
        <span class="text" >En caso aplique</span>
    </td>
    <td class="text-center">
        <span class="text nombre_doc_tramite_7" id="nombre_doc_tramite_7"></span>
    </td>
    <td class="text-center">
        <select class="custom-select select_estado_doc" aria-label=""
                id="estado_7" name="estado_7">
                <option value="0" selected>Pendiente</option>
                <option value="1">Aprobado</option>
                <option value="2">Observado</option>
                <option value="3">No aplica</option>
        </select>
    </td>
    <td class="text-center">
        <div class="opciones-documentos">
            <button type="button" role="button" class="btn btn-light btn-cargar-documento" id="btn-cargar-documento-7"
                data-toggle="modal" data-target="#modalCargarDoc" >Subir Archivo</button>
            <button type="button" role="button" class="btn btn-light btn-opciones-documento" id="btn-opciones-documento-7"
                data-toggle="modal" data-target="#modalOpcionesDoc" >Opciones</button>
        </div>
    </td>
</tr>
<tr class = "tramite_8">
    <td class="text-center">
        <span class="font-weight-bold">8</span>
    </td>
    <td class="text-center">
        <span class="text" >Excel de Advertencias Publicitarias</span>
    </td>
    <td class="text-center">
        <span class="text" >Si</span>
    </td>
    <td class="text-center">
        <span class="text nombre_doc_tramite_8" id="nombre_doc_tramite_8"></span>
    </td>
    <td class="text-center">
        <select class="custom-select select_estado_doc" aria-label=""
                id="estado_8" name="estado_8">
                <option value="0" selected>Pendiente</option>
                <option value="1">Aprobado</option>
                <option value="2">Observado</option>
        </select>
    </td>
    <td class="text-center">
        <div class="opciones-documentos">
            <button type="button" role="button" class="btn btn-light btn-cargar-documento" id="btn-cargar-documento-8"
                data-toggle="modal" data-target="#modalCargarDoc" >Subir Archivo</button>
            <button type="button" role="button" class="btn btn-light btn-opciones-documento" id="btn-opciones-documento-8"
                data-toggle="modal" data-target="#modalOpcionesDoc" >Opciones</button>
        </div>
    </td>
</tr>
<tr class = "tramite_9">
    <td class="text-center">
        <span class="font-weight-bold">9</span>
    </td>
    <td class="text-center">
        <span class="text" >Certificación de Calidad de Planta</span>
    </td>
    <td class="text-center">
        <span class="text" >Si</span>
    </td>
    <td class="text-center">
        <span class="text nombre_doc_tramite_9" id="nombre_doc_tramite_9"></span>
    </td>
    <td class="text-center">
        <select class="custom-select select_estado_doc" aria-label=""
                id="estado_9" name="estado_9">
                <option value="0" selected>Pendiente</option>
                <option value="1">Aprobado</option>
                <option value="2">Observado</option>
        </select>
    </td>
    <td class="text-center">
        <div class="opciones-documentos">
            <button type="button" role="button" class="btn btn-light btn-cargar-documento" id="btn-cargar-documento-9"
                data-toggle="modal" data-target="#modalCargarDoc" >Subir Archivo</button>
            <button type="button" role="button" class="btn btn-light btn-opciones-documento" id="btn-opciones-documento-9"
                data-toggle="modal" data-target="#modalOpcionesDoc" >Opciones</button>
        </div>
    </td>
</tr>
<tr class = "tramite_10">
    <td class="text-center">
        <span class="font-weight-bold">10</span>
    </td>
    <td class="text-center">
        <span class="text" >Certificado Orgánico Vigente</span>
    </td>
    <td class="text-center">
        <span class="text" >En caso aplique</span>
    </td>
    <td class="text-center">
        <span class="text nombre_doc_tramite_10" id="nombre_doc_tramite_10"></span>
    </td>
    <td class="text-center">
        <select class="custom-select select_estado_doc" aria-label=""
                id="estado_10" name="estado_10">
                <option value="0" selected>Pendiente</option>
                <option value="1">Aprobado</option>
                <option value="2">Observado</option>
                <option value="3">No aplica</option>
        </select>
    </td>
    <td class="text-center">
        <div class="opciones-documentos">
            <button type="button" role="button" class="btn btn-light btn-cargar-documento" id="btn-cargar-documento-10"
                data-toggle="modal" data-target="#modalCargarDoc" >Subir Archivo</button>
            <button type="button" role="button" class="btn btn-light btn-opciones-documento" id="btn-opciones-documento-10"
                data-toggle="modal" data-target="#modalOpcionesDoc" >Opciones</button>
        </div>
    </td>
</tr>