<div class="card card-success">
    <div class="card-header">
        <h3 class="card-title">BUSCAR</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                        class="fas fa-minus"></i></button>
        </div>
    </div>
    <div class="card-body">
        <form action="<?php echo base_url('ar/evalcenco/cconteval/buscar_evaluaciones'); ?>" id="frmFiltro"
                method="POST" onsubmit="return false;" accept-charset="UTF-8">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="filtro_codigo_eval">Código Evaluación</label>
                        <input type="text" class="form-control" id="filtro_codigo_eval" name="filtro_codigo_eval" placeholder="">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="filtro_codigo_rs">Código RS</label>
                        <input type="text" class="form-control" id="filtro_codigo_rs" name="filtro_codigo_rs" placeholder="">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="filtro_estado_eval">Estado Evaluación</label>
                        <select class="form-control select2bs4" id="filtro_estado_eval" name="filtro_estado_eval" style="width: 100%;">
                            <option value="0" selected="selected">Todos</option>
                            <option value="1">Aprobados</option>
                            <option value="2">Observados</option>
                            <option value="3">Truncos</option>
                        </select>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="card-footer">
        <div class="col-12 text-right">
            <button type="button" class="btn btn-primary" id="filtroBuscarEval">
                <i class="fa fa-fw fa-search"></i> Buscar
            </button>
            <!--button type="button" class="btn btn-primary" id="filtroNuevoEval">
                <i class="fa fa-fw fa-plus"></i> Nueva Evaluación
            </button-->
        </div>
    </div>
</div>
<div class="card card-outline card-success">
    <div class="card-header">
        <h3 class="card-title">Resultados</h3>
    </div>                
    <div class="card-body" id="divListado" style="overflow-x: scroll;">
    <input type="hidden" class="d-none" id="base_url" name="base_url" value="<?php echo base_url('ar/evalcenco/cconteval/buscar_evaluaciones'); ?>">
        <table id="tblFiltroEval" class="table table-striped table-bordered compact" style="width:100%">
            <thead>
            <tr>
                <th>Cód. Eval</th>
                <th>Nombre Producto</th>
                <th>Cód. RS</th>
                <th>Estado</th>
                <th>Opciones</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>