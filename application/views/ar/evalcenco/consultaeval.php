<?php
    $idusu = $this->session->userdata('s_idusuario');
?>

<style>
    .bootstrap-switch .bootstrap-switch-handle-off.bootstrap-switch-default, .bootstrap-switch .bootstrap-switch-handle-on.bootstrap-switch-default {
        background: #28a745 !important;
        color: #1f2d3d !important;
    }
</style>

<!-- content-header -->
<div class="content-header">   
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">CONSULTA EVALUACIÓN DE PRODUCTOS
            <small></small>
        </h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?php echo public_base_url(); ?>main"> <i class="fas fa-tachometer-alt"></i>Home</a></li>
          <li class="breadcrumb-item active">Consulta</li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">  
        <form action="<?php echo base_url('ar/evalcenco/cconseval/buscar') ?>" id="frmConsultaEval" accept-charset="UTF-8">
            <div class="card card-success">        
                <div class="card-header">
                    <h3 class="card-title">BUSQUEDA</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                    </div>
                </div>
                <div class="card-body">
                    <div id="filtroBusqueda";>
                        <div class="row"> 
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="codeval">Código Evaluación</label>
                                    <input type="text" class="form-control" id="codeval" name="codeval" placeholder="">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="txtnrors">Nro. RS Producto</label>
                                    <input type="text" class="form-control" id="txtnrors" name="txtnrors" placeholder="">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="txtdescprodu">Nombre del producto</label>
                                    <input type="text" class="form-control" id="txtdescprodu" name="txtdescprodu" placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="row"> 
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="estado_eval">Estado Evaluación</label>
                                    <select class="form-control" id="estado_eval" name="estado_eval" style="width: 100%;">
                                        <option value="0" selected="selected">Todos</option>
                                        <option value="1">Aprobados</option>
                                        <option value="2">Observados</option>
                                        <option value="3">Truncos</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="slc_fabricante">Fabricante</label>
                                    <select class="form-control" id="slc_fabricante" name="slc_fabricante" style="width: 100%;">
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                
                            
                <div class="card-footer justify-content-between" style="background-color: #D4EAFC;"> 
                    <div class="row">
                        <div class="col-md-12">
                            <div class="text-right">
                                <button type="button" class="btn btn-primary" id="btnBuscar"><i class="fas fa-search"></i>&nbsp;&nbsp;Buscar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div class="row">
            <div class="col-12">
                <div class="card card-outline card-success">
                    <div class="card-header">
                        <h3 class="card-title">Resultados<label id="lblCia"></label></h3>
                    </div>                
                    <div class="card-body" id="divResultados" style="overflow-x: scroll;">
                        <table id="tblResultadosEval" class="table table-striped table-bordered compact" style="width:100%">
                            <thead>
                            <tr>
                                <th>Cód. Eval</th>
                                <th>Nombre Producto</th>
                                <th>Cód. RS</th>
                                <th>Vigencia RS</th>
                                <th>Estado</th>
                                <th>Fecha Inicio</th>
                                <th>Fecha Cierre</th>
                                <th>Archivos</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>               
                </div>
            </div>
        </div>
    
    </div>
</section>
<!-- /.Main content -->

<!-- /.modal-Listado de Documentos --> 
<div class="modal fade" id="modalListdocumentos" data-backdrop="static" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header text-center bg-info">
            <h4 class="modal-title w-100 font-weight-bold">Listado de Archivos</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">                    
            <div class="form-group">
                <div class="row">
                    <div class="col-12 table-responsive">
                        <table id="tblListTramDocum" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>N°</th>
                                    <th>Requisitos</th>
                                    <th>Adjunto</th>
                                    <th>Obligatorio</th>
                                    <th>Estado</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $this->load->view('ar/evalcenco/listaTramitesModal');?>
                            </tbody>
                        </table>
                    </div>
                </div>                
            </div>   
        </div>
        <div class="modal-footer justify-content-between" style="background-color: #D4EAFC;">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-right">
                        <button type="reset" class="btn btn-default" id="btnmCerDocingre" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</div> 
<!-- /.modal-->


<!-- Script Generales -->
<script type="text/javascript">
    var baseurl = "<?php echo base_url();?>"; 
</script>
