<div class="row">
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">      
        <label>AÑO :</label>
        <select class="form-control" id="cboAnioSoli" name="cboAnioSoli" style="width: 100%;">
            <option value="2021" selected="selected">2021</option>
            <option value="2020">2020</option>
            <option value="2019">2019</option>
        </select>
    </div>    
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
        <br>
        <button type="submit" id="btnBuscarSoli" class="btn btn-block btn-primary" style="width: 50%;"><i class="fa fa-search"></i>Buscar Resultados</button>
    </div>
</div>

<div class="row">
    <div class="col-12"> 
        <br>
        <fieldset class="scheduler-border">
            <legend class="scheduler-border text-primary">Tendencia por Meses</legend>
            <div class="row">
                <div class="col-sm-5">
                    <br>
                    <table id="tbltendenciaanualsoli" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                            <th>Mes</th>
                            <th>Aprobados</th>
                            <th>Observados</th>
                            <th>Truncos</th>
                            <th>Total</th>
                            </tr>
                        </thead> 
                        <tbody></tbody>
                    </table>
                </div>
                <div class="col-sm-7">
                    <canvas id="graftendenciaanualsoli" style="width: 100%; height: 200px;"></canvas>
                </div>
            </div>
        </fieldset>
    </div>
</div>
