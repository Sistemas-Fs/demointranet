<div class="modal fade" id="modalOpcionesDoc" tabindex="-1"
	 aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl modal-dialog-scrollable">
		<div class="modal-content">
			<div class="modal-header bg-success">
				<h5 class="modal-title fs w-100 font-weight-bold">
					Opciones Documentos
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close" id="cerrarMdlOpciones">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body"
				 style="background-color:#ffffff; border-top: 1px solid #00a65a; border-bottom: 1px solid #00a65a;">
				<input type="hidden" class="d-none" id="numero_tramite" name="numero_tramite" value="">
				<table id="listaDocumentosTramite" class="table table-sm table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center col-1">Número Documento</th>
                            <th class="text-center col-5">Nombre Documento</th>
                            <th class="text-center col-1">Opciones</th>
                        </tr>
                    </thead>
					<tbody>
					</tbody>
                </table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal" id="btnCerrarOpciones">
					<i class="fa fa-save"></i> Cerrar Opciones
				</button>
			</div>
		</div>
	</div>
</div>