<tr class='tramite_1' id='tramite_1'>
    <td>1</td>
    <td>Ficha Técnica</td>
    <td><span class="doc_adjunto" id="doc_adjunto_1"></span></td>
    <td>Si</td>
    <td><span class="estado_tramite_1"></span></td>
</tr>
<tr class='tramite_2' id='tramite_2'>
    <td>2</td>
    <td>Registro Sanitario</td>
    <td><span class="doc_adjunto" id="doc_adjunto_2"></span></td>
    <td>Si</td>
    <td><span class="estado_tramite_2"></span></td>
</tr>
<tr class='tramite_3' id='tramite_3'>
    <td>3</td>
    <td>Análisis Microbiológico y fisicoquímico</td>
    <td><span class="doc_adjunto" id="doc_adjunto_3"></span></td>
    <td>Si</td>
    <td><span class="estado_tramite_3"></span></td>
</tr>
<tr class='tramite_4' id='tramite_4'>
    <td>4</td>
    <td>Análisis Nutricional o Carta de Declaración Jurada</td>
    <td><span class="doc_adjunto" id="doc_adjunto_4"></span></td>
    <td>Si</td>
    <td><span class="estado_tramite_4"></span></td>
</tr>
<tr class='tramite_5' id='tramite_5'>
    <td>5</td>
    <td>Estudio de Vida Útil</td>
    <td><span class="doc_adjunto" id="doc_adjunto_5"></span></td>
    <td>En caso aplique</td>
    <td><span class="estado_tramite_5"></span></td>
</tr>
<tr class='tramite_6' id='tramite_6'>
    <td>6</td>
    <td>Etiqueta Original</td>
    <td><span class="doc_adjunto"  id="doc_adjunto_6"></span></td>
    <td>Si</td>
    <td><span class="estado_tramite_6"></span></td>
</tr>
<tr class='tramite_7' id='tramite_7'>
    <td>7</td>
    <td>Sustento Claims</td>
    <td><span class="doc_adjunto"  id="doc_adjunto_7"></span></td>
    <td>En caso aplique</td>
    <td><span class="estado_tramite_7"></span></td>
</tr>
<tr class='tramite_8' id='tramite_8'>
    <td>8</td>
    <td>Excel de Advertencias Publicitarias</td>
    <td><span class="doc_adjunto"  id="doc_adjunto_8"></span></td>
    <td>Si</td>
    <td><span class="estado_tramite_8"></span></td>
</tr>
<tr class='tramite_9' id='tramite_9'>
    <td>9</td>
    <td>Certificación de Calidad de Planta</td>
    <td><span class="doc_adjunto"  id="doc_adjunto_9"></span></td>
    <td>Si</td>
    <td><span class="estado_tramite_9"></span></td>
</tr>
<tr class='tramite_10' id='tramite_10'>
    <td>10</td>
    <td>Certificado Orgánico Vigente</td>
    <td><span class="doc_adjunto"  id="doc_adjunto_10"></span></td>
    <td>En caso aplique</td>
    <td><span class="estado_tramite_10"></span></td>
</tr>