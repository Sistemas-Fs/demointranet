<style>
	select.custom-select {
		width: 100% !important;
	}
	.swal2-content {
		padding: 1rem 1rem 1rem;
	}
	.btn-cargar-documento {
		background-color: #4CAF50;
		border-radius: 8px;
	}
	.btn-opciones-documento {
		background-color: #D35400;
		border-radius: 8px;
	}
	.opciones-documentos{
		display:flex;
		align-items:center;
		justify-content:center;
	}
</style>

<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">
					EVALUACIÓN MARCAS PROPIAS
					<small>Módulo de Evaluación</small>
				</h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item">
						<a href="<?php echo public_base_url(); ?>cprincipal/principal">Inicio</a>
					</li>
					<li class="breadcrumb-item">Asuntos Regulatorio</li>
					<li class="breadcrumb-item">Operaciones Cenco</li>
					<li class="breadcrumb-item active">Reg. Evaluación</li>
				</ol>
			</div>
		</div>
	</div>
</div>

<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card card-success card-outline card-tabs">
					<div class="card-header p-0 pt-1 border-bottom-0">
						<ul class="nav nav-tabs" id="tabptcliente" style="background-color: #28a745;" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" style="color: #000000;" id="tab-list-listado"
								   data-toggle="pill" href="#tabReg1" role="tab"
								   aria-controls="tabReg1" aria-selected="true">LISTADO</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" style="color: #000000;" id="tab-list-registro"
								   data-toggle="pill" href="#tabReg2" role="tab"
								   aria-controls="tabReg2" aria-selected="false">REGISTRO</a>
							</li>
						</ul>
					</div>
					<div class="card-body">
						<div class="tab-content">
							<div class="tab-pane fade show active" id="tabReg1" role="tabpanel">
								<?php $this->load->view('ar/evalcenco/listadoEvaluaciones');?>
							</div>
							<div class="tab-pane fade" id="tabReg2" role="tabpanel">
								<form action="<?php echo base_url('ar/evalcenco/cconteval/guardarEval') ?>" id="formEvalu" accept-charset="UTF-8">
									<div class="row">
										<div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-12">
											<div class="form-group row">
												<label for="fecha_inicio" class="col-xl-4 col-lg-6 col-md-12 col-sm-12 col-12">
													Fecha Inicio
												</label>
												<div class="col-xl-8 col-lg-6 col-md-12 col-sm-12 col-12">
													<input type="text" class="form-control"
														id="fecha_inicio" name="fecha_inicio"
														value=""/>
												</div>
											</div>
										</div>
										<div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-12">
											<div class="form-group row">
												<label for="fecha_cierre" class="col-xl-4 col-lg-6 col-md-12 col-sm-12 col-12">
													Fecha Cierre
												</label>
												<div class="col-xl-8 col-lg-6 col-md-12 col-sm-12 col-12">
													<input type="text" class="form-control"
														id="fecha_cierre" name="fecha_cierre"
														value="" readonly/>
												</div>
											</div>
										</div>
										<div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-12">
											<div class="form-group row">
												<label for="estado_eval" class="col-xl-4 col-lg-6 col-md-12 col-sm-12 col-12">
													Estado
												</label>
												<select class="custom-select col-xl-8 col-lg-6 col-md-12 col-sm-12 col-12" id="estado_eval" name="estado_eval">
													<option value="1" selected="selected">Aprobado</option>
													<option value="2">Observado</option>
													<option value="3">Trunco</option>
												</select>
											</div>
										</div>
										<div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-12">
											<div class="form-group row">
												<label for="codigo_eval" class="col-xl-4 col-lg-6 col-md-12 col-sm-12 col-12">
													Código Evaluación
												</label>
												<div class="col-xl-8 col-lg-6 col-md-12 col-sm-12 col-12">
													<input type="text" class="form-control"
														id="codigo_eval" name="codigo_eval"
														value="" readonly/>
												</div>
											</div>
										</div>
									</div>
									<div class="row justify-content-end">
										<div class="row">
											<button type="button" role="button" class="btn btn-danger" id="btnGuardarEval">
												<i class="fa fa-save"></i> Crear Evaluación
											</button>
											<button type="button" role="button" class="btn btn-primary" id="btnActualizarEval">
												<i class="fa fa-save"></i> Actualizar Evaluación
											</button>
											<button type="button" role="button" class="btn btn-warning" id="btnCerrarEval">
												<i class="fa fa-save"></i> Cerrar Evaluación
											</button>
										</div>
									</div>
								</form>
								<!---->
								<div class="row" style="" id="contentProducto">
									<?php $this->load->view('ar/evalcenco/datosProducto');?>
									<?php $this->load->view('ar/evalcenco/mdlCargarProducto');?>
								</div>
								
								<div class="row" style="" id="contentDocuments">
									<div class="col-12">
										<fieldset class="scheduler-border">
											<legend class="scheduler-border text-primary">
												Documentos de la Evaluación
											</legend>
											<div class="box-body">
												<ul class="nav nav-tabs" id="myTab" role="tablist">
													<li class="nav-item" role="presentation">
														<a class="nav-link border-left active" id="home-tab" data-toggle="tab" href="#listaDocumentos"
														role="tab"
														aria-controls="home" aria-selected="true">Lista de Documentos</a>
													</li>
												</ul>
												<div class="tab-content border border-top-0" id="myTabContent">
													<div class="tab-pane fade show active" id="listaDocumentos" role="tabpanel"
														aria-labelledby="home-tab">
														<div class="table-responsive">
															<table class="table table-sm table-bordered table-valign-middle table-hover" id="tblDocumentos">
																<thead>
																	<tr>
																		<th class="text-center col-1">
																			N°
																		</th>
																		<th class="text-center col-3">
																			Tramite
																		</th>
																		<th class="text-center col-1">
																			Obligatorio
																		</th>
																		<th class="text-center col-3">
																			Documento(s)
																		</th>
																		<th class="text-center col-2">
																			Estado
																		</th>
																		<th class="text-center col-2">
																			Adjunto
																		</th>
																	</tr>
																</thead>
																<tbody>
																	<?php $this->load->view('ar/evalcenco/listadodocumentos');?>
																</tbody>
																<tfoot>
																</tfoot>
															</table>
														</div>
													</div>
												</div>
											</div>
										</fieldset>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php $this->load->view('ar/evalcenco/mdlCargarArchivos');?>
<?php $this->load->view('ar/evalcenco/mdlOpcionesDoc');?>