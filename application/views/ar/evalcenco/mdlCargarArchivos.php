<!-- Modal para cargar registros -->
<div class="modal fade" id="modalCargarDoc" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-scrollable">
		<div class="modal-content">
			<div class="modal-header bg-success">
				<h5 class="modal-title fs w-100 font-weight-bold text-left">
					Cargar archivo(s)
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close" id="cerrarMdlArchivos">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body"
				 style="background-color:#ffffff; border-top: 1px solid #00a65a; border-bottom: 1px solid #00a65a;">
				<form action="<?php echo base_url('ar/evalcenco/cconteval/cargar_archivo'); ?>" method="POST" enctype="multipart/form-data"
				id="form_cargar_doc">
					<input type="hidden" class="d-none" id="id_tramite" name="id_tramite" value="">
					<input type="hidden" class="d-none" id="estado_documento" name="estado_documento" value="">
					<input type="hidden" class="d-none" id="id_producto_modal" name="id_producto_modal" value="">
					<input type="hidden" class="d-none" id="id_eval_modal" name="id_eval_modal" value="">
					<div class="row">
						<div class="col-sm-12"><input type="file" name="documento" id="inputdocumento"/></div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" id="btnSaveUploadFile">
					<i class="fa fa-save"></i> Guardar
				</button>
			</div>
		</div>
	</div>
</div>
