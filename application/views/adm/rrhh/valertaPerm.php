<?php
    $idusu = $this -> session -> userdata('s_idusuario');
    $codusu = $this -> session -> userdata('s_cusuario');
    $infousuario = $this->session->userdata('s_infodato');
    $idempleado = $this->session->userdata('s_idempleado');
?>

<style>
</style>

<!-- content-header -->
<div class="content-header">   
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><i class="fas fa-file-alt"></i> Empleados que superan los 40 dias</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?php echo public_base_url(); ?>main">Home</a></li>
          <li class="breadcrumb-item active">RRHH</li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">  
        <div class="row">
            <div class="col-12">
                <div class="card card-outline card-success">
                    <div class="card-header">
                        <h3 class="card-title">Listado de Empleados con Max. Dias Pendientes</h3>
                    </div>
                
                    <div class="card-body" style="overflow-x: scroll;">                        
                        <input type="hidden" id="hdidempleado" name="hdidempleado" value= <?php echo $idempleado; ?> >
                        <table id="tblListalertaPerm" class="table table-striped table-bordered compact" style="width:100%">
                            <thead>
                            <tr>
                                <th></th>
                                <th>Area</th>
                                <th>Empleado</th>
                                <th>Fecha Ingreso</th>
                                <th>Fecha Termino</th> 
                                <th>Periodo Vacaciones</th>
                                <th>Vacaciones Anuales</th>
                                <th>Dias Vacaciones</th>
                                <th>Dias Cuenta Vacaciones</th>
                                <th>Dias Tomados Vacaciones</th>
                                <th>Dias Pendientes</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    
    </div>
</section>
<!-- /.Main content -->



<script type="text/javascript">
    var baseurl = "<?php echo base_url();?>"; 
</script>