<div class="modal fade" id="modalInspecciónHistorico" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="modal-header bg-success">
				<h5 class="modal-title fs w-100 font-weight-bold" id="textoInspHistorico" >Insp. ::CODIGO:: ::HISTORICO::</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div>
					<table id="tblListinspctrlprovHistorico"
						   class="table table-striped table-bordered compact"
						   style="width:100%">
						<thead>
						<tr>
							<th colspan="15">[Codigo] :: Proveedor - (Maquilador) -
								Establecimiento
							</th>
						</tr>
						<tr>
							<th>desc_gral</th>
							<th>Area Cliente</th>
							<th>Linea Proceso</th>
							<th>Periodo</th>
							<th>Estado</th>
							<th>Fecha</th>
							<th>Informe</th>
							<th>Resultado</th>
							<th style="width: 150px; min-width: 150px" >Responsable</th>
							<th class="text-center" style="width: 80px; min-width: 80px" >Finalizado</th>
							<th class="text-center" style="width: 80px; min-width: 80px" >Peligro</th>
							<th class="text-center" style="width: 80px; min-width: 80px" >Quejas</th>
							<th class="text-center" style="width: 80px; min-width: 80px" >AA.CC</th>
							<th></th>
						</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Save changes</button>
			</div>
		</div>
	</div>
</div>
