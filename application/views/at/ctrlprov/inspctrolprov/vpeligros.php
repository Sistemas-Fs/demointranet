<div class="row">
	<div class="col-12">
		<div class="card card-outline card-success">
			<div class="card-header">
				<h3 class="card-title">Peligros del establecimiento inspeccionado</h3>
			</div>
			<div class="card-body">
				<form action="#" method="POST" id="frmPeligro" accept-charset="UTF-8" >
					<div class="row" >
						<div class="col-xl-5 col-lg-5 col-md-5 col-12" >
							<div class="form-group row" >
								<label for="peligro_producto" class="col-xl-4 col-lg-4 col-md-4 col-12" >
									Producto
								</label>
								<div class="col-xl-8 col-lg-8 col-md-8 col-12" >
									<input type="text" class="form-control form-control-sm"
										   id="peligro_producto" name="peligro_producto"
										   value="" >
								</div>
							</div>
							<div class="form-group row" >
								<label for="peligro_cliente" class="col-xl-4 col-lg-4 col-md-4 col-12" >
									Peligro Cliente
								</label>
								<div class="col-xl-8 col-lg-8 col-md-8 col-12" >
								<textarea name="peligro_cliente" id="peligro_cliente"
										  class="form-control form-control-sm"
										  rows="3"></textarea>
								</div>
							</div>
							<div class="form-group row" >
								<label for="peligro_proveedor" class="col-xl-4 col-lg-4 col-md-4 col-12" >
									Peligro Proveedor
								</label>
								<div class="col-xl-8 col-lg-8 col-md-8 col-12" >
								<textarea name="peligro_proveedor" id="peligro_proveedor"
										  class="form-control form-control-sm"
										  rows="3"></textarea>
								</div>
							</div>
						</div>
						<div class="col-xl-7 col-lg-7 col-md-7 col-12" >
							<div class="form-group row" >
								<label for="peligro_inspeccion" class="col-xl-4 col-lg-4 col-md-4 col-12" >
									Peligro Inspección
								</label>
								<div class="col-xl-8 col-lg-8 col-md-8 col-12" >
								<textarea name="peligro_inspeccion" id="peligro_inspeccion"
										  class="form-control form-control-sm"
										  rows="5"></textarea>
								</div>
							</div>
							<div class="form-group row" >
								<label for="peligro_observacion" class="col-xl-4 col-lg-4 col-md-4 col-12" >
									Observación
								</label>
								<div class="col-xl-8 col-lg-8 col-md-8 col-12" >
								<textarea name="peligro_observacion" id="peligro_observacion"
										  class="form-control form-control-sm"
										  rows="5"></textarea>
								</div>
							</div>
						</div>
					</div>
				</form>
				<div class="btn-group btn-group-sm mb-2" >
					<?php if ($inspeccionAbierto) { ?>
						<button type="button" role="button" class="btn btn-primary btn-primary-sm" id="btnGuardarPeligro" >
							<i class="fa fa-save" ></i> Guardar Peligro
						</button>
					<?php } ?>
				</div>

				<table id="tblPeligros" class="table table-striped table-bordered"
					   style="width:100%">
					<thead>
					<tr>
						<th>N°</th>
						<th>Producto</th>
						<th>Peligro Cliente</th>
						<th>Peligro Proveedor</th>
						<th>Peligro Inspección</th>
						<th>Observación</th>
						<th style="width: 100px" ></th>
					</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
