<div class="row">
	<div class="col-12">
		<h6 class="">Requisitos que requieren Acciones Correctivas</h6>
		<div class="row my-3">
			<div class="col-xl-4 col-lg-4 col-md-4 col-12">
				<div class="input-group input-group-sm">
					<input type="text" id="acc_total"
						   class="form-control form-control-sm"
						   readonly aria-label=""
						   value=""/>
				</div>
			</div>
			<?php
			$nombreArchivoACC = '';
			if (!empty($inspeccion->DUBICACIONFILESERVERAC)) {
				$ultimoSeparador = strrpos($inspeccion->DUBICACIONFILESERVERAC, '/');
				if ($ultimoSeparador !== false) {
					$nombreArchivoACC = substr($inspeccion->DUBICACIONFILESERVERAC, $ultimoSeparador + 1, strlen($inspeccion->DUBICACIONFILESERVERAC));
				}
			}
			?>
			<div class="col-xl-8 col-lg-8 col-md-8 col-12">
				<form action="#" method="POST" accept-charset="utf8" id="frmFileACC" >
					<div class="input-group input-group-sm">
						<div class="input-group-append">
							<span class="input-group-text bg-white border-0">Excel Cargado:</span>
						</div>
						<input type="text" id="acc_archivo" name="acc_archivo"
							   class="form-control form-control-sm"
							   readonly aria-label=""
							   value="<?php echo $nombreArchivoACC ?>"/>
						<div class="input-group-button">
							<?php if ($inspeccionAbierto) { ?>
								<button type="button" role="button" class="btn btn-light btn-sm btn-existe-archivo" id="btnEliminarArchivoAC" style="<?php echo (empty($nombreArchivoACC)) ? 'display: none' : ''; ?>" >
									<i class="fa fa-trash"></i> Eliminar
								</button>
								<button type="button" role="button" class="btn btn-light btn-sm" id="btnCargarArchivoAC" >
									<i class="fa fa-cloud-upload-alt"></i> Subir
								</button>
								<button type="button" role="button" class="btn btn-light btn-sm btn-existe-archivo" id="btnDescargarArchivoAC" style="<?php echo (empty($nombreArchivoACC)) ? 'display: none' : ''; ?>" >
									<i class="fa fa-cloud-download-alt"></i> Descargar
								</button>
							<?php } ?>
							<button type="button" role="button" class="btn btn-light btn-sm" id="btnDescargarPlantillaAC" >
								<i class="fa fa-cloud-download-alt"></i> Descargar AA.CC.
							</button>
						</div>
						<input type="file" class="d-none" id="file_acc" name="file_acc" >
					</div>
				</form>
			</div>
		</div>

<!--		<div class="text-left" >-->
<!--			<button type="button" role="button" class="btn btn-primary btn-sm" id="btnCrearACC" >-->
<!--				<i class="fa fa-plus" ></i> Generar AA. CC.-->
<!--			</button>-->
<!--		</div>-->

		<div>
			<table id="tblACC" class="table table-striped table-bordered"
				   style="width:100%">
				<thead>
				<tr>
					<th>ID</th>
					<th>Requisito</th>
					<th>Excluyente</th>
					<th>Tipo Hallazgo</th>
					<th>Hallazgo</th>
					<th>Acción Correctiva</th>
					<th>Fecha Corrección</th>
					<th>Responsable</th>
					<th>Acepta Acc. Correctiva</th>
					<th>Comentario del Inspector</th>
					<th></th>
				</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
</div>

<?php $this->load->view('at/ctrlprov/inspctrolprov/vaccion_correctiva_reg'); ?>

