<?php
$codcliente = $this->session->userdata('s_ccliente');
$idusuario = $this->session->userdata('s_idusuario');
$idrol = $this->session->userdata('s_idrol');
$cia = $this->session->userdata('s_cia');
?>

<style>
	.select2-container--default .select2-selection--multiple .select2-selection__choice {
		color: #000;
	}
</style>

<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">
					QUEJAS Y PELIGROS
				</h1>
			</div>
		</div>
	</div>
</div>

<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card card-success card-outline card-tabs">
					<div class="card-body" >
						<form action="#" method="POST" accept-charset="UTF-8" id="frmGeneral" >
							<div class="row" >
								<div class="col-xl-6 col-lg-6 col-md-8 col-12" >
									<div class="form-group" >
										<label for="">
											Quejas
										</label>
										<div class="custom-file">
											<input type="file" class="custom-file-input" id="file_quejas" name="file_quejas" >
											<label class="custom-file-label" for=file_quejas" data-browse="Elegir..." id="txtQuejas" >
												Archivo...
											</label>
										</div>
										<button type="button" role="button" class="btn btn-danger btn-sm btm-eliminar-archivo" data-id="568" style="display: none" >
											<i class="fa fa-trash" ></i> Eliminar archivo
										</button>
									</div>
								</div>
							</div>
							<div class="row mt-2" >
								<div class="col-xl-6 col-lg-6 col-md-8 col-12" >
									<div class="form-group" >
										<label for="">
											Peligros
										</label>
										<div class="custom-file">
											<input type="file" class="custom-file-input" id="file_peligro" name="file_peligro" >
											<label class="custom-file-label" for=file_peligro" data-browse="Elegir..." id="txtPeligro" >
												Archivo...
											</label>
										</div>
										<button type="button" role="button" class="btn btn-danger btn-sm btm-eliminar-archivo" data-id="569" style="display: none" >
											<i class="fa fa-trash" ></i> Eliminar archivo
										</button>
									</div>
								</div>
							</div>
						</form>

						<div class="btn-group" >
							<button type="button" role="button" class="btn btn-success" id="btnSubirArchivos" >
								<i class="fa fa-upload" ></i> Subir Archivos
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
