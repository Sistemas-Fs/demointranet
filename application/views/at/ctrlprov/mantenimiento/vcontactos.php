<style>
</style>
 
 <div class="card card-primary card-outline"> 
    <div class="card-header">    
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-12">            
                <fieldset class="scheduler-border">
                    <legend class="scheduler-border text-primary">CONTACTOS</legend>
                    <input type="hidden" id="mhdnconCcliente" name="mhdnconCcliente">
                    <input type="hidden" id="mhdnconDcliente" name="mhdnconDcliente">
                    <input type="hidden" id="mhdnconDdireccion" name="mhdnconDdireccion">
                    <div class="form-group">
                        <div class="row"> 
                            <div class="col-md-2">
                                <h6><label>CLIENTE :</label></h6>
                            </div>
                            <div class="col-md-10">
                                <h6 id="lblClienteContacto"></h6>
                            </div>
                        </div>
                        <div class="row"> 
                            <div class="col-md-2">
                                <h6><label>DIRECCIÓN :</label></h6>
                            </div>
                            <div class="col-md-10">
                                <h6 id="lblDirclieContacto"></h6>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
    </div> 
    <div class="card-footer justify-content-between"> 
        <div class="row">
            <div class="col-md-12">
                <div class="text-right">
                    <button type="button" class="btn btn-secondary" id="btnRetornarListaContacto"><i class="fas fa-undo-alt"></i> Retornar</button>
                    <button type="button" class="btn btn-outline-info" id="btnContactoNuevo"><i class="fas fa-plus"></i> Crear Nuevo</button>
                    <button type="button" class="btn btn-outline-info" id="btnAddContacto"><i class="fas fa-plus"></i> Añadir contacto</button>
                </div>
            </div>
        </div>
    </div> 
</div>

<div class="card card-primary" id="cardListContacto">
    <div class="card-header">
        <h3 class="card-title"><b>LISTADO CONTACTOS</b></h3>
     </div>
                
    <div class="card-body">
        <div class="form-group">
            <div class="row"> 
                <div class="col-md-12">
                    <table id="tblListContactos" class="table table-striped table-bordered compact" style="width:100%">
                        <thead>
                            <tr>
                                <th>N°</th>
                                <th>NOMBRE</th>
                                <th>CARGO</th>
                                <th>EMAIL</th>
                                <th>TELEFONO</th>

                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card card-primary" id="cardRegContacto">
    <div class="card-header">
        <h3 class="card-title"><b>REGISTRO DE CONTACTO</b></h3>
     </div>
                
    <div class="card-body">
        <form class="form-horizontal" id="frmMantContacto" name="frmMantContacto" action="<?= base_url('oi/ctrlprov/mantenimiento/cclientes/createcontacto')?>" method="POST" enctype="multipart/form-data" role="form"> 
            <input type="hidden" id="mhdnIdContacto" name="mhdnIdContacto"> 
            <input type="hidden" id="txtestablecimiento" name="txtestablecimiento"> 
            <input type="hidden" id="mhdnIdClie" name="mhdnIdClie">
            <input type="hidden" id="mhdnAccionContacto" name="mhdnAccionContacto" value="">
            <div class="form-group">
                <div class="row"> 
                    <div class="col-sm-4">
                        <div class="text-info">Nombres</div>
                        <div>
                            <input type="text" class="form-control" name="txtNombresContacto" id="txtNombresContacto">
                        </div>
                    </div> 
                    
                    <div class="col-sm-4">
                        <div class="text-info">Ape. Paterno</div>
                        <div>
                            <input type="text" class="form-control" name="txtApePaterno" id="txtApePaterno">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="text-info">Ape. Materno</div>
                        <div>
                            <input type="text" class="form-control" name="txtApeMaterno" id="txtApeMaterno">
                        </div>
                    </div>
                </div>                
            </div> 

            <div class="form-group">
                <div class="row"> 
                    <div class="col-sm-4">
                        <div class="text-info">Cargo</div>
                        <div>
                            <input type="text" class="form-control" name="txtCargoEmp" id="txtCargoEmp">
                        </div>
                    </div> 
                    
                    <div class="col-sm-4">
                        <div class="text-info">Email</div>
                        <div>
                            <input type="email" class="form-control" name="txtEmailCon" id="txtEmailCon">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="text-info">Telefono</div>
                        <div>
                            <input type="text" class="form-control" name="txtTelefonoCon" id="txtTelefonoCon">
                        </div>
                    </div>
                </div>                
            </div> 
            
            <div class="form-group">
                <div class="row">                                     
                    <div class="col-sm-12 text-right"> 
                        <button type="submit" class="btn btn-primary" id="btnguardarcontacto"><i class="fa fa-fw fa-check"></i>Grabar</button> 
                        <button type="button" class="btn btn-secondary" id="btnCerrarContacto"><i class="fas fa-window-close"></i> Cerrar</button>
                    </div> 
                </div>                
            </div>                        
        </form>
    </div>
</div>


<!-- /.modal-ubigeo --> 
<div class="modal fade" id="modalAddContacto" data-backdrop="static" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
     
        <div class="modal-header text-center bg-primary">
            <h4 class="modal-title w-100 font-weight-bold">Añadir Contacto</h4>
        </div>

        <div class="card-body">
        <div class="form-group">
            <div class="row"> 
                <div class="col-md-12">
                    <table id="tblListAddContactos" class="table table-striped table-bordered compact" style="width:100%">
                        <thead>
                            <tr>
                                <th>N°</th>
                                <th></th>
                                <th>NOMBRE</th>
                                <th>CARGO</th>
                                <th>EMAIL</th>
                                <th>TELEFONO</th>

                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

        <div class="modal-footer justify-content-between" style="background-color: #dff0d8;">
            <button  type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
    </div>
  </div>
</div> 
<!-- /.modal-->



