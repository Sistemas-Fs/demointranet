<style>
    .dataTables_scrollBody {
        overflow:none;
    }
</style>
 
 <div class="card card-primary card-outline"> 
    <div class="card-header">    
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-12">            
                <fieldset class="scheduler-border">
                    <legend class="scheduler-border text-primary">SERVICIOS</legend>
                    <input type="hidden" id="mhdnprovCcliente" name="mhdnprovCcliente">
                    <input type="hidden" id="mhdnprovDcliente" name="mhdnprovDcliente">
                    <input type="hidden" id="mhdnprovDdireccion" name="mhdnprovDdireccion">
                    <input type="hidden" id="mhdnprovDzid" name="mhdnprovDzid">
                    <input type="hidden" id="mhdnprovCpais" name="mhdnprovCpais">
                    <input type="hidden" id="mhdnprovDciudad" name="mhdnprovDciudad">
                    <input type="hidden" id="mhdnprovDestado" name="mhdnprovDestado">
                    <input type="hidden" id="mhdnprovCubigeo" name="mhdnprovCubigeo">
                    <input type="hidden" id="mhdnprovDubigeo" name="mhdnprovDubigeo">
                    <div class="form-group">
                        <div class="row"> 
                            <div class="col-md-2">
                                <h6><label>CLIENTE :</label></h6>
                            </div>
                            <div class="col-md-10">
                                <h6 id="lblClienteServ"></h6>
                            </div>
                        </div>
                        <div class="row"> 
                            <div class="col-md-2">
                                <h6><label>DIRECCIÓN :</label></h6>
                            </div>
                            <div class="col-md-10">
                                <h6 id="lblDirclieServ"></h6>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
    </div> 
    <div class="card-footer justify-content-between"> 
        <div class="row">
            <div class="col-md-12">
                <div class="text-right">
                    <button type="button" class="btn btn-secondary" id="btnRetornarListaMaq"><i class="fas fa-undo-alt"></i> Retornar</button>
                    <button type="button" class="btn btn-outline-info" id="btnAddServiciosModal" data-toggle="modal" data-target="#modalAddServicio" ><i class="fas fa-plus"></i> Agregar Servicio</button>
                </div>
            </div>
        </div>
    </div> 
</div>






<div class="card card-primary" id="cardListServ">
    <div class="card-header">
        <h3 class="card-title"><b>LISTADO DE SERVICIOS</b></h3>
     </div>
                
    <div class="card-body">
        <div class="form-group">
            <div class="row"> 
                <div class="col-md-12">
                    <table id="tblListServicios" class="table table-striped table-bordered compact" style="width:100%">
                        <thead>
                            <tr>
                                <th>N°</th>
                                <th>CODIGO</th>
                                <th>SERVICIO</th>
                                <th>SUB SERVICIO</th>
                                <th>RESPONSABLE</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card card-primary" id="cardRegServ">
    <div class="card-header">
        <h3 class="card-title"><b>REGISTRO</b></h3>
     </div>
                
    <div class="card-body">
        <div class="row">
            <div class="col-12">
            <fieldset class="scheduler-border">
                <legend class="scheduler-border text-primary">Datos Maquilador</legend>
                <div class="card card-primary">
                    <div class="card-body">
                        <div class="form-group">
                        <form class="form-horizontal" id="frmMantptClieMaq" action="<?= base_url('oi/ctrlprov/mantenimiento/cclientes/setproveedorxmaquilador')?>" method="POST" enctype="multipart/form-data" role="form">
                            
                            <input type="text" id="hdnIdptclieserv" name="hdnIdptclieserv"> <!-- ID -->
                            <input type="hidden" id="hdnAccionptclieMaq" name="hdnAccionptclieMaq">
                            <input type="hidden" class="form-control" name="utxtlogo" id="utxtlogo"> 
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="text-info">Tipo Doc.</div>
                                    <div>  
                                        <select id="cboTipoDoc" name="cboTipoDoc" class="form-control" style="width: 100%;">
                                            <option value = "">Elige</option>
                                            <option value = "R">RUC</option>
                                            <option value = "O">OTROS</option>
                                        </select>
                                        </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="text-info">Nro documento</div>
                                    <div>  
                                        <input type="text" class="form-control" name="txtnrodoc" id="txtnrodoc">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="text-info">Razón Social</div>
                                    <div>
                                        <input type="text" class="form-control" name="txtrazonsocial" id="txtrazonsocial">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="text-info">Pais</div>
                                    <div>
                                        <select id="cboPaisMaq" name="cboPaisMaq" class="form-control select2bs4" style="width: 100%;">
                                            <option value = "">Cargando...</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3" id="boxCiudad">
                                    <div class="text-info">Ciudad</div>
                                    <div>
                                        <input type="text" class="form-control" name="txtCiudad" id="txtCiudad">
                                    </div>
                                </div>
                                <div class="col-md-3" id="boxEstado">
                                    <div class="text-info">Estado / Region / Provincia</div>
                                    <div>
                                        <input type="text" class="form-control" name="txtEstado" id="txtEstado">
                                    </div>
                                </div>
                                <div class="col-md-6" id="boxUbigeo">
                                    <div class="text-info">Departamento / Distrito / Provincia</div>
                                    <div class="input-group mb-3">
                                        <input type="text" id="mtxtUbigeoMaq" name="mtxtUbigeoMaq" class="form-control">
                                        <span class="input-group-append">
                                            <button type="button" id="btnBuscarUbigeoMaq" class="btn btn-info btn-flat"><i class="fa fa-search"></i></button>
                                        </span>
                                    </div>
                                    <input type="hidden" id="hdnidubigeomaq" name="hdnidubigeomaq">
                                </div>
                                <div class="col-md-3" id="boxEstado">
                                    <div class="text-info">Codigo Postal / ZIP</div>
                                    <div>
                                        <input type="text" class="form-control" name="txtCodigopostal" id="txtCodigopostal">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="text-info">Dirección Domicilio Fiscal</div>
                                    <div>
                                        <input type="text" class="form-control" name="txtDireccion" id="txtDireccion">
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div style="border-top: 1px solid #ccc; padding-top: 10px;"> 
                            <div class="row">
                                <div class="col-6">
                                    <h4><i class="fas fa-user-tie"></i> Datos <small> Representante Legal</small></h4>
                                </div> 
                            </div> 
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="text-info">Representante Legal</div>
                                    <div>
                                        <input type="text" class="form-control" name="txtRepresentante" id="txtRepresentante">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="text-info">Cargo Rep.</div>
                                    <div>
                                        <input type="text" class="form-control" name="txtCargorep" id="txtCargorep">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="text-info">Email Rep.</div>
                                    <div>
                                        <input type="text" class="form-control" name="txtEmailrep" id="txtEmailrep">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="text-info">Telefono</div>
                                    <div>
                                        <input type="text" class="form-control" name="txtTelefono" id="txtTelefono">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="text-info">Pagina Web</div>
                                    <div>
                                        <input type="text" class="form-control" name="txtWeb" id="txtWeb">
                                    </div>
                                </div>
                            </div>       
                            </div>
                        </form>
                        </div>
                            <br>
                            <div style="border-top: 1px solid #ccc; padding-top: 10px;"> 
                            <form id="frmFileinputLogoclieProv" name="frmFileinputLogoclieProv" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="text-info">Logo del Cliente</div>
                                    <div class="text-center">
                                            <input type="hidden" id="hdnCCliente" name="hdnCCliente">  
                                            <label for="file-input"> 
                                                <img  id="image_previa" alt="Foto de Cliente" class="profile-user-img img-fluid img-circle img-perfil" style="border: 3px solid #adb5bd; padding: 3px;" title="Click para cambiar de foto ">
                                            </label>
                                    </div>
                                </div> 
                                <div class="col-md-6" style="display: none;" id="divlogoprov">
                                    <div class="text-info">Vista Previa de Logo</div>
                                    <div class="text-center">
                                        <div class="kv-avatar">
                                            <div class="file-loading">
                                                <input id="file-input" name="file-input" type="file" onchange="registrar_imagen_prov()" ref="image" style="display: none;"/> 
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                            </form>  
                            <!--<div class="row">
                                <div class="col-md-6" style="display: none;" id="divlogo">
                                    <div class="text-info"></div>
                                    <div>
                                        <input type="hidden" class="form-control" name="utxtlogo" id="utxtlogo">
                                        <img id="image_previa" src="" width="150" height="100" class="img-circle">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="kv-avatar">
                                        <div class="file-loading">
                                            <input id="logo_image" name="logo_image" type="file" onchange="registrar_imagen()">
                                        </div>
                                    </div>
                                </div>
                            </div> -->       
                            </div>
                    </div>
                    <div class="card-footer"> 
                        <div class="row">
                            <div class="col-md-12 text-right"> 
                                <button type="submit" form="frmMantptClieMaq" class="btn btn-success" ><i class="fas fa-save"></i> Grabar</button>    
                                <button type="button" class="btn btn-secondary" id="btnRetornarMaq"><i class="fas fa-undo-alt"></i> Retornar</button>
                            </div>
                        </div>
                    </div> 
                </div> 
            </fieldset>
            </div>
        </div>
    </div>
</div>


<!-- /.modal-ubigeo --> 
<div class="modal fade" id="modalUbigeoMaq" data-backdrop="static" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form class="form-horizontal" id="frmUbigeo" name="frmUbigeo" action="" method="POST" enctype="multipart/form-data" role="form"> 

        <div class="modal-header text-center bg-primary">
            <h4 class="modal-title w-100 font-weight-bold">Seleccionar Ubigeo</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        <div class="modal-body">                                  
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text-info">Departamento</div>
                        <div>                            
                            <select class="form-control select2bs4" id="cboDepaMaq" name="cboDepaMaq" style="width: 100%;">
                                <option value="">Cargando...</option>
                            </select>
                        </div>
                    </div>  
                </div>                
            </div> 
            <div class="form-group">
                <div class="row">                
                    <div class="col-sm-12">
                        <div class="text-info">Provincia</div>
                        <div>
                            <select class="form-control select2bs4" id="cboProvMaq" name="cboProvMaq">
                                <option value="">Cargando...</option>
                            </select>
                        </div>
                    </div>   
                </div>                
            </div>
            <div class="form-group">
                <div class="row">                
                    <div class="col-sm-12">
                        <div class="text-info">Distrito</div>
                        <div>
                            <select class="form-control select2bs4" id="cboDistMaq" name="cboDistMaq">
                                <option value="">Cargando...</option>
                            </select>
                        </div>
                    </div>   
                </div>                
            </div>             
        </div>

        <div class="modal-footer justify-content-between" style="background-color: #dff0d8;">
            <button id="btnSelUbigeoPMaq" type="button" class="btn btn-primary"><i class="fa fa-save"></i> Seleccionar</button>
            <button id="btncerrarUbigeoMaq" type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        </div>
      </form>
    </div>
  </div>
</div> 
<!-- /.modal-->

<!-- /.modal-ubigeo --> 
<div class="modal fade" id="modalAddServicio" data-backdrop="static" role="dialog" aria-hidden="true">
  <div class="modal-dialog ">
    <div class="modal-content">
    <form class="form-horizontal" id="frmCreateServicio"  method="POST" enctype="multipart/form-data" role="form">
        <input type="hidden" name="idCLienteServ" id="idCLienteServ">
        <div class="modal-header text-center bg-primary">
            <h4 class="modal-title w-100 font-weight-bold">Agregar servicio</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        <div class="modal-body">                                  
            <div class="form-group">
                <div class="col-sm-12">
                    <div class="text-info">Servicio</div>
                    <div>                            
                        <select class="form-control select2bs4" id="cboServicio" name="cboServicio" style="width: 100%;">
                            <option value="">Cargando...</option>
                        </select>
                    </div>
                </div>
               
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <div class="text-info">Sub Servicio</div>
                    <div>                            
                        <select class="form-control select2bs4" id="cboSubServicio" name="cboSubServicio" style="width: 100%;">
                            <option value="">Cargando...</option>
                        </select>
                    </div>
                </div>
               
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <div class="text-info">Responsable</div>
                    <div>                            
                        <select class="form-control select2bs4" id="cboResponsableOI" name="cboResponsableOI" style="width: 100%;">
                            <option value="">Cargando...</option>
                        </select>
                    </div>
                </div>
               
            </div>
        </div>

        <div class="modal-footer justify-content-between" style="background-color: #dff0d8;">
            <button  type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            <button  type="button" class="btn btn-primary" id="btnCreateServicio">Agregar</button>
        </div>
</form>
    </div>
  </div>
</div> 
<!-- /.modal-->
