<!--Contenedor de Registro-->
<fieldset class="scheduler-border">
	<legend class="scheduler-border text-primary">Check List</legend>
	<div class="box-body">
		<form class="form-horizontal" id="frmMantRegistro"
			  action="<?= base_url('at/ctrlprov/checklist/cchecklist/guardar') ?>" method="POST" onsubmit="return false;"
			  enctype="multipart/form-data" role="form">
			<input type="hidden" id="checklist_id" name="checklist_id" value="0">
			<div class="row">
				<div class="form-group col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
					<label for="checklist_descripcion">
						Descripción
					</label>
					<input type="text" class="form-control" id="checklist_descripcion" name="checklist_descripcion"
						   value=""/>
				</div>
				<div class="form-group col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
					<label for="checklist_area">
						Área
					</label>
					<select class="custom-select" id="checklist_area" name="checklist_area"></select>
				</div>
				<div class="form-group col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
					<label for="checklist_servicio">
						Servicio
					</label>
					<select class="custom-select" id="checklist_servicio" name="checklist_servicio"></select>
				</div>
				<div class="form-group col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
					<label for="checklist_sistema">
						Sistema
					</label>
					<select class="custom-select" id="checklist_sistema" name="checklist_sistema"></select>
				</div>
			</div>
			<div class="row">
				<div class="col-xl-3 col-lg-3 col-md-3 col-sm-8 col-12">
					<div class="form-group">
						<label for="">
							Estado Checklist
						</label>
						<div>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio"
									   name="checklist_estado_chklist" id="checklist_estado_1" value="C">
								<label class="form-check-label" for="checklist_estado_1">Concluido</label>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" checked
									   name="checklist_estado_chklist" id="checklist_estado_2" value="A">
								<label class="form-check-label" for="checklist_estado_2">En Modificación</label>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group col-xl-3 col-lg-3 col-md-3 col-sm-4 col-12">
					<label for="checklist_organismo">
						Organismo
					</label>
					<select class="custom-select" id="checklist_organismo" name="checklist_organismo"></select>
				</div>
				<div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
					<label for="checklist_rubro">
						Rubro
					</label>
					<select class="custom-select" id="checklist_rubro" name="checklist_rubro"></select>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-xl-2 col-lg-2 col-md-4 col-sm-6 col-12">
					<label for="" class="d-block">
						&nbsp;
					</label>
					<div class="form-check form-check-inline">
						<input class="form-check-input" type="checkbox"
							   name="checklist_uso" id="checklist_uso" value="1">
						<label class="form-check-label" for="checklist_uso">En Uso</label>
					</div>
					<div class="form-check form-check-inline">
						<input class="form-check-input" type="checkbox" disabled
							   name="checklist_asignado" id="checklist_asignado" value="1">
						<label class="form-check-label" for="checklist_asignado">Asignados a Cliente</label>
					</div>
				</div>
				<div class="form-group col-xl-3 col-lg-3 col-md-8 col-sm-6 col-12">
					<label for="checklist_conformidad">
						No Conformidad
					</label>
					<select class="custom-select" id="checklist_conformidad" name="checklist_conformidad"></select>
				</div>
				<div class="form-group col-xl-3 col-lg-2 col-md-4 col-sm-4 col-12">
					<label for="checklist_tipo">
						Tipo
					</label>
					<select class="custom-select" id="checklist_tipo" name="checklist_tipo">
						<option value="O">Oficial</option>
						<option value="P">Propio</option>
					</select>
				</div>
				<div class="form-group col-xl-2 col-lg-2 col-md-4 col-sm-4 col-12">
					<label for="checklist_pesos">
						Utiliza Pesos
					</label>
					<select class="custom-select" id="checklist_pesos" name="checklist_pesos">
						<option value="S">Si</option>
						<option value="N" selected>No</option>
					</select>
				</div>
				<div class="form-group col-xl-2 col-lg-2 col-md-4 col-sm-4 col-12">
					<label for="checklist_estado">
						Estado
					</label>
					<select class="custom-select" id="checklist_estado" name="checklist_estado">
						<option value="A">Activo</option>
						<option value="I">Inactivo</option>
					</select>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
					<label for="checklist_formula">
						Formula
					</label>
					<input type="text" class="form-control" id="checklist_formula" name="checklist_formula"
						   value=""/>
				</div>
			</div>

			<div class="form-group row justify-content-end">
				<div class="col-sm-6 text-right">
					<button type="button" id="btnGrabarCheckList"
							class="btn btn-success">
						<i class="fa fa-save"></i>
						<span>Guardar CheckList</span>
					</button>
					<button type="button" class="btn btn-primary"
							id="btnRetornarLista"><i
								class="fa fa-fw fa-undo-alt"></i> Retornar
					</button>
				</div>
			</div>
		</form>

		<div class="row" id="contenidoRequisito" style="display: none" >
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12" id="tblRequisitos">
				<div style="height: 750px; overflow-y: scroll">
					<div class="table-responsive">
						<table class="table table-bordered table-hover">
							<thead>
							<tr>
								<th class="text-center" style="width: 5rem; min-width: 5rem;">
									Nro.
								</th>
								<th class="text-left" style="width: 25rem; min-width: 25rem">
									Requisito
								</th>
							</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
				<fieldset class="scheduler-border">
					<legend class="scheduler-border text-primary">
						<span class="spinner-border spinner-border-sm" id="btnLoadingRequisito"
							  style="display: none"
							  role="status">
							<span class="sr-only">Loading...</span>
						</span>
						Detalle de Requisito
					</legend>
					<form class="form-horizontal" id="frmMantRequisito"
						  action="<?= base_url('at/ctrlprov/checklist/crequisito/guardar') ?>" method="POST"
						  onsubmit="return false;"
						  enctype="multipart/form-data" role="form">
						<input type="hidden" class="d-none" id="checklist_requisito_ordenlista" name="checklist_requisito_ordenlista" value="" />
						<div class="box-body">
							<input type="hidden" class="d-none"
								   id="checklist_requisito_id"
								   name="checklist_requisito_id"
								   value="0"/>
							<div class="row">
								<div class="col-12 text-right">
									<button type="button" role="button" class="btn btn-secondary"
											data-toggle="modal" data-target="#formulaCheckList">
										<i class="fa fa-plus"></i> Agregar Fórmula
									</button>
									<button type="button" role="button" class="btn btn-secondary"
											data-toggle="modal" data-target="#asociadoCheckList">
										<i class="fa fa-users"></i> Asociados
									</button>
								</div>
								<div class="form-group col-12">
									<label for="checklist_padre_requisito">
										Padre Requisito
									</label>
									<select class="custom-select"
											id="checklist_padre_requisito" name="checklist_padre_requisito"></select>
								</div>
								<div class="form-group col-12">
									<label for="checklist_requisito_descripcion">
										Descripción
									</label>
									<textarea class="form-control"
											  name="checklist_requisito_descripcion"
											  id="checklist_requisito_descripcion"
											  rows="5"></textarea>
								</div>
								<div class="form-group col-12">
									<label for="checklist_normativa">
										Normativa
									</label>
									<textarea class="form-control"
											  name="checklist_normativa"
											  id="checklist_normativa"
											  rows="5"></textarea>
								</div>
							</div>
							<div class="row">
								<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
									<div class="form-group row">
										<label for="checklist_requisito_valor"
											   class="col-xl-2 col-lg-4 col-md-4 col-sm-4 col-12">
											Valor
										</label>
										<div class="col-xl-10 col-lg-10 col-md-10 col-sm-8 col-12">
											<select class="custom-select"
													id="checklist_requisito_valor"
													name="checklist_requisito_valor"></select>
										</div>
									</div>
								</div>
								<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
									<div class="form-group row">
										<label for="checklist_requisito_valorado"
											   class="col-xl-4 col-lg-5 col-md-5 col-sm-5 col-12 text-right">
											Valorado
										</label>
										<div class="col-xl-8 col-lg-7 col-md-7 col-sm-7 col-12">
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="checkbox"
													   name="checklist_requisito_valorado" id="checklist_requisito_valorado"
													   value="1">
												<label class="form-check-label"
													   for="checklist_requisito_valorado"></label>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12">
									<div class="form-group row">
										<label for="checklist_requisito_excluyente"
											   class="col-xl-4 col-lg-4 col-md-4 col-sm-5 col-12">
											Excluyente
										</label>
										<div class="col-xl-8 col-lg-8 col-md-8 col-sm-7 col-12">
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio"
													   name="checklist_requisito_excluyente"
													   id="checklist_requisito_excluyente_1" value="S">
												<label class="form-check-label" for="checklist_requisito_excluyente_1">Excluyente</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio"
													   name="checklist_requisito_excluyente"
													   id="checklist_requisito_excluyente_2" value="N">
												<label class="form-check-label" for="checklist_requisito_excluyente_2">Normal</label>
											</div>
										</div>
									</div>
								</div>
								<div class="col-xl-4 col-lg-4 col-md-4 col-sm-8 col-12">
									<div class="form-group row">
										<label for="checklist_requisito_factores_criticos"
											   class="col-xl-8 col-lg-8 col-md-8 col-sm-9 col-12 text-right">
											Grupo de Factores Criticos
										</label>
										<div class="col-xl-4 col-lg-4 col-md-4 col-sm-3 col-12">
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="checkbox"
													   name="checklist_requisito_factores_criticos"
													   id="checklist_requisito_factores_criticos"
													   value="1">
												<label class="form-check-label"
													   for="checklist_requisito_factores_criticos"></label>
											</div>
										</div>
									</div>
								</div>
								<div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-12">
									<div class="form-group row">
										<label for="checklist_requisito_peso"
											   class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-12">
											Peso
										</label>
										<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
											<input type="text" class="form-control text-center"
												   name="checklist_requisito_peso" id="checklist_requisito_peso"
												   value="1">
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12">
									<div class="form-group row">
										<label for="checklist_requisito_orden"
											   class="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-12">
											Orden de Lista
										</label>
										<div class="col-xl-7 col-lg-7 col-md-7 col-sm-7 col-12">
											<select class="custom-select"
													id="checklist_requisito_orden"
													name="checklist_requisito_orden"></select>
										</div>
									</div>
								</div>
								<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
									<div class="form-group row">
										<label for="checklist_requisito_numerador"
											   class="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-12">
											Numerador
										</label>
										<div class="col-xl-7 col-lg-7 col-md-7 col-sm-7 col-12">
											<input class="form-control text-center" readonly
												   id="checklist_requisito_numerador"
												   name="checklist_requisito_numerador"
												   value=""/>
										</div>
									</div>
								</div>
								<div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
									<div class="form-group row">
										<label for="checklist_requisito_nivel"
											   class="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-12">
											Nivel
										</label>
										<div class="col-xl-7 col-lg-7 col-md-7 col-sm-7 col-12">
											<input class="form-control text-center" readonly
												   id="checklist_requisito_nivel"
												   name="checklist_requisito_nivel"
												   value=""/>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-12 font-weight-bold text-center h5 my-2">
									INSPECCIÓN DE TIENDAS FSC
								</div>
								<div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="checkbox"
											   name="checklist_inspeccion_peso" id="checklist_inspeccion_peso"
											   value="1">
										<label class="form-check-label" for="checklist_inspeccion_peso">
											Tiene Peso
										</label>
									</div>
								</div>
								<div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="checkbox"
											   name="checklist_inspeccion_venc" id="checklist_inspeccion_venc"
											   value="1">
										<label class="form-check-label" for="checklist_inspeccion_venc">
											Tiene Prod. Venc.
										</label>
									</div>
								</div>
								<div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="checkbox"
											   name="checklist_inspeccion_baja" id="checklist_inspeccion_baja"
											   value="1">
										<label class="form-check-label" for="checklist_inspeccion_baja">
											Tiene Baja de 50%
										</label>
									</div>
								</div>
								<div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="checkbox"
											   name="checklist_inspeccion_oc" id="checklist_inspeccion_oc"
											   value="1">
										<label class="form-check-label" for="checklist_inspeccion_oc">
											Es OC
										</label>
									</div>
								</div>
							</div>
							<div class="text-center mt-3">
								<button type="button" role="button" class="btn btn-danger" id="btnNuevoRequisito">
									<i class="fa fa-plus"></i> Nuevo Requisito
								</button>
								<button type="button" role="button" class="btn btn-success mx-2" id="btnGuardarRequisito"
										disabled>
									<i class="fa fa-save"></i> Guardar Requisito
								</button>
								<button type="button" role="button" class="btn btn-secondary" id="btnEliminarRequisito"
										disabled>
									<i class="fa fa-trash"></i> Eliminar Requisito
								</button>
							</div>
						</div>
					</form>
				</fieldset>
			</div>
		</div>
	</div>
</fieldset>

<?php $this->load->view('at/ctrlprov/checklist/vchecklist_asociados'); ?>
<?php $this->load->view('at/ctrlprov/checklist/vchecklist_formula'); ?>
