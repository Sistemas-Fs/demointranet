<style>
	.select2-container .select2-selection--single {
		height: auto;
	}
</style>

<div class="modal fade" id="asociadoCheckList"
	 tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Asociar Clientes</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive" >
					<table class="table table-bordered table-striped" id="tblAsociar" >
						<thead>
						<tr>
							<th class="text-left" style="min-width: 15rem;" >Cliente</th>
							<th class="text-left" style="min-width: 8rem; width: 8rem" ></th>
							<th class="text-left" style="min-width: 8rem; width: 8rem" ></th>
						</tr>
						</thead>
						<tbody>
						</tbody>
						<tfoot>
						<tr>
							<td colspan="3" >
								<button type="button" role="button" class="btn btn-link" id="btnAsociar" >
									<i class="fa fa-plus" ></i> Agregar nuevo cliente
								</button>
							</td>
						</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
