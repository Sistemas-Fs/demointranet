<fieldset class="scheduler-border">
	<legend class="scheduler-border text-primary">Datos Cliente</legend>
	<div class="card card-primary">
		<div class="card-body">
			<div class="form-group">
				<form class="form-horizontal" id="frmMantptClie"
					  action="<?php echo base_url('at/ctrlprov/cliente/ccliente/guardar') ?>"
					  method="POST" enctype="multipart/form-data" role="form">

					<input type="hidden" id="hdnIdptclie" name="hdnIdptclie">
					<!-- ID -->
					<input type="hidden" id="hdnAccionptclie"
						   name="hdnAccionptclie">
					<input type="hidden" class="form-control" name="utxtlogo"
						   id="utxtlogo">
					<div class="row">
						<div class="col-md-3">
							<div class="text-info">Tipo Doc.</div>
							<div>
								<select id="cboTipoDoc" name="cboTipoDoc"
										class="form-control"
										style="width: 100%;">
									<option value="">Elige</option>
									<option value="R">RUC</option>
									<option value="O">OTROS</option>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class="text-info">Nro documento</div>
							<div>
								<input type="text" class="form-control"
									   name="txtnrodoc" id="txtnrodoc">
							</div>
						</div>
						<div class="col-md-6">
							<div class="text-info">Razón Social</div>
							<div>
								<input type="text" class="form-control"
									   name="txtrazonsocial"
									   id="txtrazonsocial">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="text-info">Pais</div>
							<div>
								<select id="cboPais" name="cboPais"
										class="form-control select2bs4"
										style="width: 100%;">
									<option value="">Cargando...</option>
								</select>
							</div>
						</div>
						<div class="col-md-3" id="boxCiudad">
							<div class="text-info">Ciudad</div>
							<div>
								<input type="text" class="form-control"
									   name="txtCiudad" id="txtCiudad">
							</div>
						</div>
						<div class="col-md-3" id="boxEstado">
							<div class="text-info">Estado / Region / Provincia
							</div>
							<div>
								<input type="text" class="form-control"
									   name="txtEstado" id="txtEstado">
							</div>
						</div>
						<div class="col-md-6" id="boxUbigeo">
							<div class="text-info">Departamento / Distrito /
								Provincia
							</div>
							<div class="input-group mb-3">
								<input type="text" id="mtxtUbigeo"
									   name="mtxtUbigeo" class="form-control">
								<span class="input-group-append">
																<button type="button" id="btnBuscarUbigeo"
																		class="btn btn-info btn-flat"><i
																		class="fa fa-search"></i></button>
															</span>
							</div>
							<input type="hidden" id="hdnidubigeo"
								   name="hdnidubigeo">
						</div>

					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="text-info">Dirección Domicilio Fiscal
							</div>
							<div>
								<input type="text" class="form-control"
									   name="txtDireccion" id="txtDireccion">
							</div>
						</div>
					</div>


					<br>
					<div style="border-top: 1px solid #ccc; padding-top: 10px;">
						<div class="row">
							<div class="col-6">
								<h4><i class="fas fa-user-tie"></i> Datos
									<small> Representante Legal</small></h4>
							</div>
						</div>
						<div class="row">
							<div class="col-md-5">
								<div class="text-info">Representante Legal</div>
								<div>
									<input type="text" class="form-control"
										   name="txtRepresentante"
										   id="txtRepresentante">
								</div>
							</div>
							<div class="col-md-3">
								<div class="text-info">Cargo Rep.</div>
								<div>
									<input type="text" class="form-control"
										   name="txtCargorep" id="txtCargorep">
								</div>
							</div>
							<div class="col-md-4">
								<div class="text-info">Email Rep.</div>
								<div>
									<input type="text" class="form-control"
										   name="txtEmailrep" id="txtEmailrep">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<div class="text-info">Telefono</div>
								<div>
									<input type="text" class="form-control"
										   name="txtTelefono" id="txtTelefono">
								</div>
							</div>
							<div class="col-md-4">
								<div class="text-info">Pagina Web</div>
								<div>
									<input type="text" class="form-control"
										   name="txtWeb" id="txtWeb">
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
			<br>
			<div style="border-top: 1px solid #ccc; padding-top: 10px;">
				<form id="frmFileinputLogoclie1" name="frmFileinputLogoclie1"
					  method="post" enctype="multipart/form-data">
					<div class="row">
						<div class="col-md-6">
							<div class="text-info">Logo del Cliente</div>
							<div class="text-center">
								<input type="hidden" id="hdnCCliente"
									   name="hdnCCliente">
								<label for="file-input">
									<img id="image_previa" alt="Foto de Cliente"
										 class="profile-user-img img-fluid img-circle img-perfil"
										 style="border: 3px solid #adb5bd; padding: 3px;"
										 title="Click para cambiar de foto ">
								</label>
							</div>
						</div>
						<div class="col-md-6" style="display: none;"
							 id="divlogo">
							<div class="text-info">Vista Previa de Logo</div>
							<div class="text-center">
								<div class="kv-avatar">
									<div class="file-loading">
										<input id="file-input" name="file-input"
											   type="file"
											   onchange="registrar_imagen()"
											   ref="image"
											   style="display: none;"/>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
				<!--<div class="row">
					<div class="col-md-6" style="display: none;" id="divlogo">
						<div class="text-info"></div>
						<div>
							<input type="hidden" class="form-control" name="utxtlogo" id="utxtlogo">
							<img id="image_previa" src="" width="150" height="100" class="img-circle">
						</div>
					</div>
					<div class="col-md-6">
						<div class="kv-avatar">
							<div class="file-loading">
								<input id="logo_image" name="logo_image" type="file" onchange="registrar_imagen()">
							</div>
						</div>
					</div>
				</div> -->
			</div>
		</div>
		<div class="card-footer">
			<div class="row">
				<div class="col-md-12 text-right">
					<button type="submit" form="frmMantptClie"
							class="btn btn-success" id="btnGrabar"><i
							class="fas fa-save"></i> Grabar
					</button>
					<button type="button" class="btn btn-secondary"
							id="btnRetornar"><i class="fas fa-undo-alt"></i>
						Retornar
					</button>
				</div>
			</div>
		</div>
	</div>
</fieldset>
