<div class="card card-primary" id="cardLineaProc">
	<div class="card-header">
		<h3 class="card-title"><b>Linea</b></h3>
	</div>
	<div class="card-body">
		<button type="button" class="btn btn-info" id="btnModalAddLinea">Agregar Linea</button>
		<br>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<table id="tblLineaEstable" class="table table-striped table-bordered compact" style="width:100%">
						<thead>
						<tr>
							<th style="width: 50px; min-width: 50px" ></th>
							<th style="width: 50px; min-width: 50px" ></th>
							<th>Descripción</th>
							<th>¿Peligro?</th>
						</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modalLineaAdd" data-backdrop="static" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form class="form-horizontal" id="frmLineaAdd" name="frmLineaAdd" action="<?= base_url('at/ctrlprov/cliente/cestablecimiento_linea/guardar')?>" method="POST" enctype="multipart/form-data" role="form">

				<div class="modal-header text-center bg-primary">
					<h4 class="modal-title w-100 font-weight-bold">Agregar Linea de Proceso</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div class="modal-body">
					<input type="hidden" id="hdnIdLinea" name="hdnIdLinea"> <!-- ID -->
					<input type="hidden" id="hdnIdptClieLinea" name="hdnIdptClieLinea">
<!--					<input type="hidden" id="hdnAccionptclieLinea" name="hdnAccionptclieLinea">-->
					<input type="hidden" id="txtestablecimiento" name="txtestablecimiento">
					<div class="form-group">
						<div class="row">
							<div class="col-sm-12">
								<label for="txtLineaProc" class="text-info">Descripcion de la linea de proceso</label>
								<input type="text" class="form-control" name="txtLineaProc" id="txtLineaProc">
							</div>
							<div class="col-12" >
								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" id="chxPeligro" name="chxPeligro" value="1" >
									<label class="custom-control-label" for="chxPeligro">¿Linea en peligro?</label>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer justify-content-between" style="background-color: #dff0d8;">
					<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Agregar</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
				</div>
			</form>
		</div>
	</div>
</div>
