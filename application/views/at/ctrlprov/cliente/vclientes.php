<?php
$idusu = $this->session->userdata('s_idusuario');
$cusuario = $this->session->userdata('s_cusuario');
?>

<style>
	.fa-file-image:before {
		content: "\f0c6";
	}

	.img-perfil {
		width: 200px !important;
	}

	.btn-file {
		display: none;
	}

	#file-input {
		display: none;
	}

	.text-center img {
		cursor: pointer;
		text-align: center;
	}

	.text-center:hover img {
		opacity: 0.6;
		filter: brightness(0.4);
	}
</style>

<!-- content-header -->
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark"><i class="fas fa-city"></i> MANTENIMIENTO CLIENTES</h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="<?php echo public_base_url(); ?>main">Home</a></li>
					<li class="breadcrumb-item active">Clientes FSC</li>
				</ol>
			</div>
		</div>
	</div>
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content" id="contenedorBusqueda">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card card-primary card-outline card-tabs">
					<div class="card-header p-0 pt-1 border-bottom-0">
						<ul class="nav nav-tabs bg-lightblue" id="tabptcliente" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" style="color: #000000;" id="tabptcliente-list-tab"
								   data-toggle="pill" href="#tabptcliente-list" role="tab"
								   aria-controls="tabptcliente-list" aria-selected="true">LISTADO</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" style="color: #000000;" id="tabptcliente-reg-tab" data-toggle="pill"
								   href="#tabptcliente-reg" role="tab" aria-controls="tabptcliente-reg"
								   aria-selected="false">REG. CLIENTE</a>
							</li>
							<!-- <li class="nav-item">
								<a class="nav-link" style="color: #000000;" id="tabptcliente-prov-tab" data-toggle="pill" href="#tabptcliente-prov" role="tab" aria-controls="tabptcliente-prov" aria-selected="false">REG. PROV.</a>
							</li> -->
						</ul>
					</div>
					<div class="card-body">
						<div class="tab-content" id="tabptcliente-tabContent">
							<div class="tab-pane fade show active" id="tabptcliente-list" role="tabpanel"
								 aria-labelledby="tabptcliente-list-tab">
								<div class="card card-primary">
									<div class="card-header">
										<h3 class="card-title"><b>BUSCAR CLIENTES - FS</b></h3>
										<div class="card-tools">
											<button type="button" class="btn btn-tool" data-card-widget="collapse"><i
														class="fas fa-minus"></i></button>
										</div>
									</div>

									<div class="card-body">
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<label>Cliente - Nro Documento</label>
													<input id="txtCliente" name="txtCliente" type="text"
														   class="form-control" style="width: 100%;">
												</div>
											</div>
										</div>
									</div>

									<div class="card-footer justify-content-between">
										<div class="row">
											<div class="col-md-12">
												<div class="text-right">
													<button type="submit" class="btn btn-primary" id="btnBuscar"><i
																class="fa fa-search"></i> Buscar
													</button>
													<button type="button" class="btn btn-outline-info" id="btnNuevo"><i
																class="fas fa-plus"></i> Crear Nuevo
													</button>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-12">
										<div class="card card-outline card-primary">
											<div class="card-header">
												<h3 class="card-title"><b>LISTADO DE CLIENTES - FS</b></h3>
											</div>

											<div class="card-body">
												<table id="tblListPtcliente"
													   class="table table-striped table-bordered compact"
													   style="width:100%">
													<thead>
													<tr>
														<th></th>
														<th>Cliente</th>
														<th>Dirección</th>
														<th>Telefono</th>
														<th>Representante Legal</th>
													</tr>
													</thead>
													<tbody>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="tabptcliente-reg" role="tabpanel"
								 aria-labelledby="tabptcliente-reg-tab">
								<?php $this->load->view('at/ctrlprov/cliente/vmantenimiento'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- /.Main content -->

<!-- Reg. Establecimiento -->
<section class="content" id="contenedorRegestable" style="display: none">
	<div class="container-fluid">
		<?php $this->load->view('at/ctrlprov/cliente/vestablecimiento'); ?>
	</div>
</section>
<!-- /.Reg. Establecimiento -->

<!-- Reg. Proveedores -->
<section class="content" id="contenedorRegprov" style="display: none">
	<div class="container-fluid">
		<?php $this->load->view('at/ctrlprov/cliente/vproveedor'); ?>
	</div>
</section>
<!-- /.Reg. Proveedores -->

<!-- Reg. Maquilador -->
<section class="content" id="contenedorRegmaq" style="display: none">
	<div class="container-fluid">
		<?php $this->load->view('at/ctrlprov/cliente/vmaquilador'); ?>
	</div>
</section>
<!-- /.Reg. Maquilador -->

<!-- Reg. Servicios -->
<section class="content" id="contenedorRegserv" style="display: none">
	<div class="container-fluid">
		<?php $this->load->view('at/ctrlprov/cliente/vservicios'); ?>
	</div>
</section>
<!-- /.Reg. Servicios -->

<!-- Reg. Servicios -->
<section class="content" id="contenedorRegcontacto" style="display: none">
	<div class="container-fluid">
		<?php $this->load->view('at/ctrlprov/cliente/vcontacto'); ?>
	</div>
</section>
<!-- /.Reg. Servicios -->

<!-- Reg. Area -->
<section class="content" id="contenedorRegarea" style="display: none">
	<div class="container-fluid">
		<?php $this->load->view('at/ctrlprov/cliente/varea'); ?>
	</div>
</section>
<!-- /.Reg. Area -->

<!-- /.modal-ubigeo -->
<div class="modal fade" id="modalUbigeo" data-backdrop="static" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form class="form-horizontal" id="frmUbigeo" name="frmUbigeo" action="" method="POST"
				  enctype="multipart/form-data" role="form">

				<div class="modal-header text-center bg-primary">
					<h4 class="modal-title w-100 font-weight-bold">Seleccionar Ubigeo</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div class="modal-body">
					<div class="form-group">
						<div class="row">
							<div class="col-sm-12">
								<div class="text-info">Departamento</div>
								<div>
									<select class="form-control select2bs4" id="cboDepa" name="cboDepa"
											style="width: 100%;">
										<option value="">Cargando...</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-sm-12">
								<div class="text-info">Provincia</div>
								<div>
									<select class="form-control select2bs4" id="cboProv" name="cboProv">
										<option value="">Cargando...</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-sm-12">
								<div class="text-info">Distrito</div>
								<div>
									<select class="form-control select2bs4" id="cboDist" name="cboDist">
										<option value="">Cargando...</option>
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer justify-content-between" style="background-color: #dff0d8;">
					<button id="btnSelUbigeo" type="button" class="btn btn-primary"><i class="fa fa-save"></i>
						Seleccionar
					</button>
					<button id="btncerrarUbigeo" type="button" class="btn btn-default" data-dismiss="modal">Cancelar
					</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- /.modal-->

<!-- Script Generales -->
<script type="text/javascript">
	var baseurl = "<?php echo base_url();?>";
</script>
