<div class="modal fade" id="modalGenerarSistema" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header bg-success">
				<h5 class="modal-title fs w-100 font-weight-bold">Mantenimiento de Sistema</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body"
				 style="background-color:#ffffff; border-top: 1px solid #00a65a; border-bottom: 1px solid #00a65a;">
				<form class="form-horizontal well test-form toggle-disabled" id="frmRegSistema" name="frmRegSistema"
					  action="<?= base_url('at/ctrlprov/csistema') ?>" method="POST"
					  enctype="multipart/form-data"
					  role="form">
					<input type="hidden" class="d-none" id="sistema_id" name="sistema_id" value="0" />
					<div class="form-group row">
						<label for="sistema_descripcion" class="col-xl-3 col-lg-3 col-md-5 col-sm-12 col-12">
							Descripción
						</label>
						<div class="col-xl-9 col-lg-9 col-md-7 col-sm-12 col-12">
							<input type="text" class="form-control" id="sistema_descripcion" name="sistema_descripcion"
								   value="">
						</div>
					</div>
					<div class="form-group row">
						<label for="sistema_area_id" class="col-xl-3 col-lg-3 col-md-5 col-sm-12 col-12">
							Área
						</label>
						<div class="col-xl-9 col-lg-9 col-md-7 col-sm-12 col-12">
							<select class="custom-select" id="sistema_area_id" name="sistema_area_id"></select>
						</div>
					</div>
					<div class="form-group row">
						<label for="sistema_servicio_id" class="col-xl-3 col-lg-3 col-md-5 col-sm-12 col-12">
							Servicio
						</label>
						<div class="col-xl-9 col-lg-9 col-md-7 col-sm-12 col-12">
							<select class="custom-select" id="sistema_servicio_id" name="sistema_servicio_id"></select>
						</div>
					</div>
					<div class="form-group row">
						<label for="sistema_estado" class="col-xl-3 col-lg-3 col-md-5 col-sm-12 col-12">
							Estado
						</label>
						<div class="col-xl-9 col-lg-9 col-md-7 col-sm-12 col-12">
							<select class="custom-select" id="sistema_estado" name="sistema_estado">
								<option value="A">Activo</option>
								<option value="I">Inactivo</option>
							</select>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-info" id="btnGuardarSistema" >
					<i class="fa fa-save" ></i> Guardar
				</button>
			</div>
		</div>
	</div>
</div>
