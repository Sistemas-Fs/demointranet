<?php
    $idusu = $this -> session -> userdata('s_idusuario');
?>

<style>

</style>

<!-- content-header -->
<div class="content-header">   
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">CARTAS PROVEEDORES</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?php echo public_base_url(); ?>cprincipal/principal">Home</a></li>
          <li class="breadcrumb-item active">Control de Proveedores</li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="card card-success">
            <div class="card-header">
                <h3 class="card-title">BUSQUEDA</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                </div>
            </div>
          
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Clientes</label>
                            <select class="form-control select2bs4" id="cboClie" name="cboClie" style="width: 100%;">
                                <option value="" selected="selected">Cargando...</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Año</label>
                            <select class="form-control" id="cboAnio" name="cboAnio" style="width: 100%;">
                                <option value="" selected="selected">Cargando...</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Mes</label>
                            <select class="form-control" id="cboMes" name="cboMes" style="width: 100%;">
                                <option value="" selected="selected">Cargando...</option>
                            </select>
                        </div>
                    </div>
                </div>
				<div class="row" >
					<div class="col-4" >
						<div class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input" id="chElvaprod">
							<label class="custom-control-label" for="chElvaprod">Eval. Producto</label>
						</div>
					</div>
				</div>
            </div>
                        
            <div class="card-footer justify-content-between"> 
                <div class="row justify-content-between">
                    <div class="col-md-6">
						<button type="button" role="button" class="btn btn-secondary"
								data-toggle="modal" data-target="#modalContactos"
								id="btnContactos"><i class="fas fa-search"></i> Contactos</button>
						<button type="button" role="button" class="btn btn-info"
								id="btnDescarga"><i class="fas fa-search"></i> Descargar</button>
					</div>
                    <div class="col-md-6">
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary" id="btnBuscar"><i class="fas fa-search"></i> Buscar</button>    
                            <button type="button" class="btn btn-outline-danger" id="btnPrint" ><i class="fas fa-file-pdf"></i> Generar Cartas</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-outline card-success">
                    <div class="card-header">
                        <h3 class="card-title">LISTADO</h3>
                    </div>
                
                    <div class="card-body">
						<form action="<?php echo base_url('at/ctrlprov/ccartasprov/genpdfcartasprov_elegidos') ?>"
							  method="POST" target="_blank" id="formCartasElegidas" >
							<table id="tblListcartas" class="table table-striped table-bordered" style="width:100%">
								<thead>
								<tr>
									<th></th>
									<th class="text-left" >
										<div class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" id="carta-inspeccion-todos">
											<label class="custom-control-label" for="carta-inspeccion-todos">Todos</label>
										</div>
									</th>
									<th>Interno</th>
									<th>Fecha</th>
									<th>f. Creacion</th>
									<th>RUC</th>
									<th>Proveedor</th>
									<th>Establecimiento/Maquilador</th>
									<th>Linea Proceso</th>
									<th>Checklist</th>
									<th>Checklist</th>
									<th>Enviado</th>
									<th>Ubigeo</th>
									<th>Tamaño de Empresa</th>
									<th>Eval. Prod.</th>
									<th>Costo</th>
									<th>Área</th>
									<th>Jerarquia</th>
								</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.Main content -->

<div class="modal fade" id="modalContactos" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="modal-header bg-success">
				<h5 class="modal-title fs w-100 font-weight-bold" id="staticBackdropLabel">Contactos</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="btn-group" >
					<button type="button" role="button" class="btn btn-primary" id="downloadContactos" >
						<i class="fa fa-download" ></i> Descargar contactos
					</button>
				</div>
				<div>
					<table id="tblContacto" class="table table-striped table-bordered"
						   style="width:100%">
						<thead>
						<tr>
							<th></th>
							<th>Interno</th>
							<th>Fecha</th>
							<th>f. Creacion</th>
							<th>RUC</th>
							<th>Proveedor</th>
							<th>Establecimiento/Maquilador</th>
							<th>Linea Proceso</th>
							<th>Checklist</th>
							<th>Checklist</th>
							<th>Enviado</th>
							<th>Ubigeo</th>
							<th>Tamaño de Empresa</th>
							<th>Eval. Prod.</th>
							<th>Costo</th>
							<th>Área</th>
							<th>Jerarquia</th>
							<th>Nombre Contacto</th>
							<th>Cargo Contacto</th>
							<th>Correo Contacto</th>
							<th>Telefono Contacto</th>
						</tr>
						</thead>
						<tbody></tbody>
						<tfoot>
						<tr>
							<th></th>
						</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Script Generales -->
<script type="text/javascript">
    var baseurl = "<?php echo base_url();?>"; 
</script>
