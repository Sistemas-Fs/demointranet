<?php
$idusu = $this->session->userdata('s_idusuario');
$cusuario = $this->session->userdata('s_cusuario');
?>

<style>

	tab {
		display: inline-block;
		margin-left: 100px;
	}

	tr.subgroup,
	tr.subgroup:hover {
		background-color: #F2F2F2 !important;
		/* color: blue; */
		font-weight: bold;
	}

	.group {
		background-color: #CDD1DB !important;
		font-size: 15px;
		color: #000000 !important;
		opacity: 0.7;
	}

	.subgroup {
		cursor: pointer;
	}

	td.subgroup {
		border-top: double !important;
	}

	td.expand {
		border-bottom: double !important;
	}

	.modal-lg {
		max-width: 1000px !important;
	}

	.dataTables_scrollBody thead {
		visibility: hidden;
	}

	.hidden {
		display: none;
	}
</style>

<!-- content-header -->
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">INSP. AUDITORIA</h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="<?php echo public_base_url(); ?>cprincipal/principal">Home</a>
					</li>
					<li class="breadcrumb-item active">Ctrl. Prov.</li>
				</ol>
			</div>
		</div>
	</div>
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content" style="background-color: #E0F4ED;">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card card-success card-outline card-tabs">
					<div class="card-header p-0 pt-1 border-bottom-0">
						<ul class="nav nav-tabs" id="tabinspaudi" style="background-color: #28a745;" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" style="color: #000000;" id="tabinspaudi-list-tab"
								   data-toggle="pill" href="#tabinspaudi-list" role="tab"
								   aria-controls="tabinspaudi-list" aria-selected="true">LISTADO</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" style="color: #000000;" id="tabinspaudi-det-tab" data-toggle="pill"
								   href="#tabinspaudi-det" role="tab" aria-controls="tabinspaudi-det"
								   aria-selected="false">PROGRAMACION</a>
							</li>
						</ul>
					</div>
					<div class="card-body">
						<div class="tab-content" id="tabinspaudi-tabContent">
							<div class="tab-pane fade show active" id="tabinspaudi-list" role="tabpanel"
								 aria-labelledby="tabinspaudi-list-tab">
								<div class="card card-success">
									<div class="card-header">
										<h3 class="card-title">BUSQUEDA</h3>
										<div class="card-tools">
											<button type="button" class="btn btn-tool" data-card-widget="collapse"><i
														class="fas fa-minus"></i></button>
										</div>
									</div>
									<div class="card-body">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label>Clientes</label>
													<select class="form-control select2bs4" id="cboclieserv"
															name="cboclieserv" style="width: 100%;">
														<option value="" selected="selected">Cargando...</option>
													</select>
												</div>
											</div>
											<div class="col-md-3">
												<div class="checkbox"><label>
														<input type="checkbox" id="chkFreg"/> <b>Fecha Registro ::
															Del</b>
													</label></div>
												<div class="input-group date" id="txtFDesde"
													 data-target-input="nearest">
													<input type="text" id="txtFIni" name="txtFIni"
														   class="form-control datetimepicker-input"
														   data-target="#txtFDesde" disabled/>
													<div class="input-group-append" data-target="#txtFDesde"
														 data-toggle="datetimepicker">
														<div class="input-group-text"><i class="fa fa-calendar"></i>
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-3">
												<label>Hasta</label>
												<div class="input-group date" id="txtFHasta" data-target-input="nearest">
													<input type="text" id="txtFFin" name="txtFFin" class="form-control datetimepicker-input" data-target="#txtFHasta" disabled/>
													<div class="input-group-append" data-target="#txtFHasta" data-toggle="datetimepicker">
														<div class="input-group-text"><i class="fa fa-calendar"></i></div>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label>Tiendas / Establecimientos</label>
													<input type="text" class="form-control" id="txttienesta" name="txttienesta">
												</div>
											</div>
											<div class="col-md-3">
												<div class="form-group">
													<label>Inspector</label>
													<select class="form-control select2bs4" id="cboinspector" name="cboinspector" style="width: 100%;">
														<option value="" selected="selected">Cargando...</option>
													</select>
												</div>
											</div>
											<div class="col-md-3">
												<div class="form-group">
													<label>Estado</label>
													<select class="form-control select2bs4" id="cboestado" name="cboestado" multiple="multiple" data-placeholder="Seleccionar" style="width: 100%;">
														<option value="" selected="selected">Cargando...</option>
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class="card-footer justify-content-between" style="background-color: #E0F4ED;">
										<div class="row">
											<div class="col-md-8">
												<div class="text-left">
													<button type="button" class="btn btn-outline-info" id="btnNuevo"><i class="fas fa-plus"></i> Crear Nuevo </button>
												</div>
											</div>
											<div class="col-md-4">
												<div class="text-right">
													<button type="button" class="btn btn-primary" id="btnBuscar"><i class="fa fa-search"></i> Buscar </button>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-12">
										<div class="card card-outline card-success">
											<div class="card-header">
												<div class="row">
													<div class="col-md-8">
														<h3 class="card-title">Listado de Inspecciones - <label id="lblCliente"></label></h3>
													</div>
													<div class="col-md-4 text-right">
														<button id="btn-show-all-children" type="button">Expandir </button>
														<button id="btn-hide-all-children" type="button">Contraer </button>
													</div>
												</div>
											</div>
											<div class="card-body">
												<div class="row">
													<div class="col-md-12">
														<table id="tblListinspaudi"
															   class="table table-striped table-bordered compact"
															   style="width:100%">
															<thead>
															<tr>
																<th colspan="8">[Codigo] :: Establecimiento
																</th>
															</tr>
															<tr>
																<th></th>
																<th></th>
																<th>Fecha</th>
																<th>Estado</th>
																<th>Inspector</th>
																<th>Informe</th>
																<th>Resultado</th>
																<th></th>
																<th></th>
															</tr>
															</thead>
															<tbody>
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="tabinspaudi-det" role="tabpanel"
								 aria-labelledby="tabinspaudi-det-tab">
								<fieldset class="scheduler-border" id="datoInspeccion">
									<legend class="scheduler-border text-primary">Datos de Inspección</legend>
									<div class="row">
										<div class="col-md-12">
											<div class="text-info">Codigo :: Establecimiento
											</div>
											<div class="form-group">
												<textarea class="form-control" cols="20" id="mtxtinspdatos"
														  name="mtxtinspdatos" rows="2" disabled=true></textarea>
											</div>
										</div>
									</div>
								</fieldset>
								<fieldset class="scheduler-border" id="regInspeccion">
									<legend class="scheduler-border text-primary">Programación de Inspección</legend>
									<form class="form-horizontal" id="frmRegInsp"
										  action="<?= base_url('at/ctrlprov/cregctrolprov/setreginspeccion') ?>"
										  method="POST" enctype="multipart/form-data" role="form">
										<input type="hidden" name="mfechainsp" id="mfechainsp">
										<input type="hidden" name="mhdnzctipoestado" id="mhdnzctipoestado">
										<input type="hidden" name="mhdndcliente" id="mhdndcliente">
										<input type="hidden" name="mhdnccliente" id="mhdnccliente">
										<input type="hidden" name="mhdncproveedor" id="mhdncproveedor">
										<input type="hidden" name="mhdncmaquilador" id="mhdncmaquilador">
										<input type="hidden" name="mfservicio" id="mfservicio">
										<input type="hidden" name="mhdnAccioninsp" id="mhdnAccioninsp">
										<div class="form-group">
											<div class="row">
												<div class="col-md-2">
													<div class="text-info">Código</div>
													<div>
														<input type="text" name="mtxtidinsp" id="mtxtidinsp"
															   class="form-control" readonly>
													</div>
												</div>
												<div class="col-md-3">
													<div class="text-info">Periodo</div>
													<div>
														<input type="month" name="cboinspperiodo" id="cboinspperiodo"
															   class="form-control" pattern="[0-9]{4}-[0-9]{2}">
													</div>
												</div>
												<div class="col-md-3">
													<div class="text-info">Estado Inspección</div>
													<div>
														<input type="text" name="txtinspestado" id="txtinspestado"
															   class="form-control" disabled>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-3">
													<div class="text-info">Fecha Inspeccion</div>
													<div class="input-group date" id="txtFInspeccion"
														 data-target-input="nearest">
														<div class="input-group-prepend">
															<div class="input-group-text"><a style="cursor:pointer;"
																							 onClick="javascript:quitarFecha()"><i
																			class="far fa-calendar-times"></i></a></div>
														</div>
														<input type="text" id="txtFInsp" name="txtFInsp"
															   class="form-control datetimepicker-input"
															   data-target="#txtFInspeccion"/>
														<div class="input-group-append" data-target="#txtFInspeccion"
															 data-toggle="datetimepicker">
															<div class="input-group-text"><i class="fa fa-calendar"></i>
															</div>
														</div>
													</div>
												</div>
												<div class="col-md-3">
													<div class="text-info">Inspector</div>
													<div>
														<select class="form-control select2bs4" id="cboinspinspector"
																name="cboinspinspector" style="width: 100%;">
															<option value="" selected="selected">Cargando...</option>
														</select>
													</div>
												</div>
												<div class="col-md-1" style="text-align: center;">
													<div class="text-info">AACC</div>
													<div class="form-group clearfix">
														<div class="icheck-primary d-inline">
															<input type="checkbox" name="chkaacc" id="chkaacc" checked>
															<label for="chkaacc">
															</label>
														</div>
													</div>
												</div>
												<div class="col-md-2">
													<div class="text-info">Tipo Serv.</div>
													<div>
														<select class="form-control" id="cbotiposerv" name="cbotiposerv"
																style="width: 100%;">
															<option value="013" selected="selected">Normal</option>
															<option value="014">Inopinada</option>
														</select>
													</div>
												</div>
												<div class="col-md-3">
													<div class="text-info">Incluye Plan Inspección</div>
													<div class="input-group">
														<div class="form-group clearfix">
															<div class="icheck-primary d-inline">
																<input type="checkbox" name="chkplaninsp"
																	   id="chkplaninsp" checked>
																<label for="chkplaninsp">
																</label>
															</div>
														</div>
														<div class="input-group-addon input-group-button" id="Btnplan">
															<button type="button" role="button"
																	class="btn btn-outline-info" id="mbtnnewplaninsp"
																	data-toggle="modal" data-target="#modalPlaninsp"><i
																		class="far fa-file-alt"></i> Generar Plan
															</button>
															<button type="button" role="button"
																	class="btn btn-outline-info" id="mbtnverplaninsp"><i
																		class="far fa-file-alt"></i> Visualizar Plan
															</button>
														</div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-4">
													<div class="text-info">Sistema</div>
													<div>
														<select class="form-control select2bs4" id="cboinspsistema"
																name="cboinspsistema" style="width: 100%;">
															<option value="" selected="selected">Cargando...</option>
														</select>
													</div>
												</div>
												<div class="col-md-4">
													<div class="text-info">Rubro</div>
													<div>
														<select class="form-control select2bs4" id="cboinsprubro"
																name="cboinsprubro" style="width: 100%;">
															<option value="" selected="selected">Cargando...</option>
														</select>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-8">
													<div class="text-info">Check List</div>
													<div>
														<select class="form-control select2bs4" id="cboinspcchecklist"
																name="cboinspcchecklist" style="width: 100%;">
															<option value="" selected="selected">Cargando...</option>
														</select>
													</div>
												</div>
												<div class="col-md-4">
													<div class="text-info">Modelo Informe</div>
													<div>
														<select class="form-control select2bs4" id="cboinspmodeloinfo"
																name="cboinspmodeloinfo" style="width: 100%;">
															<option value="" selected="selected">Cargando...</option>
														</select>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-4">
													<div class="text-info">Valor No Conformidad</div>
													<div>
														<select class="form-control select2bs4" id="cboinspvalconf"
																name="cboinspvalconf" style="width: 100%;">
															<option value="" selected="selected">Cargando...</option>
														</select>
													</div>
												</div>
												<div class="col-md-4">
													<div class="text-info">Formula Evaluacion</div>
													<div>
														<select class="form-control select2bs4" id="cboinspformula"
																name="cboinspformula" style="width: 100%;">
															<option value="" selected="selected">Cargando...</option>
														</select>
													</div>
												</div>
												<div class="col-md-4">
													<div class="text-info">Criterio Resultado</div>
													<div>
														<select class="form-control select2bs4" id="cboinspcritresul"
																name="cboinspcritresul" style="width: 100%;">
															<option value="" selected="selected">Cargando...</option>
														</select>
													</div>
												</div>
											</div>
											<br>
											</br>
											<div class="row">
												<div class="col-sm-12 text-right">
													<button type="submit" class="btn btn-success" id="btnGrabar"><i
																class="fas fa-save"></i> Grabar
													</button>
													<button type="button" class="btn btn-secondary"
															id="btnRetornarLista"><i class="fas fa-undo-alt"></i>
														Retornar
													</button>
												</div>
											</div>
										</div>
									</form>
								</fieldset>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- /.Main content -->


<!-- /.modal-crear-inspeccion -->
<div class="modal fade" id="modalCreactrlprov" data-backdrop="static" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form class="form-horizontal" id="frmCreactrlprov" name="frmCreactrlprov"
				  action="<?= base_url('at/ctrlprov/cregctrolprov/setregctrlprov') ?>" method="POST"
				  enctype="multipart/form-data" role="form">

				<div class="modal-header text-left bg-success">
					<h4 class="modal-title w-100 font-weight-bold">Registro de Control de Proveedores</h4>
<!--					<button type="button" class="close" data-dismiss="modal" aria-label="Close">-->
<!--						<span aria-hidden="true">&times;</span>-->
<!--					</button>-->
				</div>

				<div class="modal-body">
					<input type="hidden" id="hdnregcusuario" name="hdnregcusuario" value="<?php echo $cusuario ?>">
					<input type="hidden" id="mhdnAccionctrlprov" name="mhdnAccionctrlprov">
					<div class="form-group">
						<div class="row">
							<div class="col-md-2">
								<div class="text-info">Codigo</div>
								<div>
									<input type="text" name="mhdnregIdinsp" id="mhdnregIdinsp" class="form-control">
									<!-- ID -->
								</div>
							</div>
							<div class="col-md-10">
								<div class="text-info">Clientes <span class="text-requerido">*</span></div>
								<div>
									<select class="form-control select2bs4" id="cboregClie" name="cboregClie"
											style="width: 100%;">
										<option value="" selected="selected">Cargando...</option>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="text-info">Proveedor <span class="text-requerido">*</span></div>
								<div>
									<select class="form-control select2bs4" id="cboregprovclie" name="cboregprovclie"
											style="width: 100%;">
										<option value="" selected="selected">Cargando...</option>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="text-info">Maquilador</div>
								<div>
									<select class="form-control select2bs4" id="cboregmaquiprov" name="cboregmaquiprov"
											style="width: 100%;">
										<option value="" selected="selected">Cargando...</option>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="text-info">Establecimiento <span class="text-requerido">*</span></div>
								<div>
									<select class="form-control select2bs4" id="cboregestable" name="cboregestable"
											style="width: 100%;">
										<option value="" selected="selected">Cargando...</option>
									</select>
								</div>
							</div>
							<div class="col-md-8">
								<div class="text-info">Dir. Inspeccion</div>
								<div>
									<input type="text" name="mtxtregdirestable" id="mtxtregdirestable"
										   class="form-control" disable>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="text-info">Area del Cliente <span class="text-requerido">*</span></div>
								<div>
									<select class="form-control select2bs4" id="cboregareaclie" name="cboregareaclie"
											style="width: 100%;">
										<option value="" selected="selected">Cargando...</option>
									</select>
								</div>
							</div>
							<div class="col-md-8">
								<div class="text-info">Linea de Proceso</div>
								<div>
									<select class="form-control select2bs4" id="cboreglineaproc" name="cboreglineaproc"
											style="width: 100%;">
										<option value="" selected="selected">Cargando...</option>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="text-info">Contacto Principal</div>
								<div>
									<select class="form-control select2bs4" id="cbocontacprinc" name="cbocontacprinc"
											style="width: 100%;">
										<option value="" selected="selected">Cargando...</option>
									</select>
								</div>
							</div>
							<div class="col-md-4">
								<div class="text-info">Tipo de establecimiento</div>
								<div>
									<select class="form-control select2bs4" id="cbotipoestable" name="cbotipoestable"
											style="width: 100%;">
										<option value="" selected="selected">Cargando...</option>
									</select>
								</div>
							</div>
							<div class="col-md-2">
								<div class="text-info">Costo</div>
								<div>
									<input type="number" name="txtcostoestable" id="txtcostoestable"
										   class="form-control" placeholder="0.00" min="0.00">
								</div>
							</div>
						</div>
						<br>
						<div id="regInspeccion" style="border-top: 1px solid #ccc; padding-top: 10px;">
							<div class="row">
								<div class="col-12">
									<h4>
										<i class="fas fa-list-alt"></i> INSPECCION
									</h4>
								</div>
							</div>
							<div class="row">
								<div class="col-3">
									<div class="text-info">Periodo</div>
									<input type="month" name="mtxtregPeriodo" id="mtxtregPeriodo" class="form-control"
										   pattern="[0-9]{4}-[0-9]{2}">
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer" style="background-color: #dff0d8;">
					<button type="reset" class="btn btn-default" id="mbtnCCreactrl" data-dismiss="modal">
						<i class="fa fa-times" ></i> Cancelar
					</button>
					<button type="submit" class="btn btn-info" id="mbtnGCreactrl">
						<i class="fa fa-save" ></i> Grabar
					</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- /.modal-->

<!-- /.modal-cierre especial -->
<div class="modal fade" id="modalCierreespecial" data-backdrop="static" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-header text-center bg-success">
				<h4 class="modal-title w-100 font-weight-bold">Cierre Especial</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<div class="modal-body">
				<form class="form-horizontal" id="frmCierreespecial" name="frmCierreespecial"
					  action="<?= base_url('at/ctrlprov/cregctrolprov/setcierreespecial') ?>" method="POST"
					  enctype="multipart/form-data" role="form">
					<input type="hidden" id="mhdncierrefservicio" name="mhdncierrefservicio">
					<input type="hidden" id="mhdnAccioncierre" name="mhdnAccioncierre">
					<input type="hidden" id="mhdnfprog" name="mhdnfprog">
					<input type="hidden" id="hdncusuario" name="hdncusuario" value="<?php echo $cusuario ?>">
					<div class="form-group">
						<div class="row">
							<div class="col-md-3">
								<div class="text-info">Codigo</div>
								<div>
									<input type="text" name="txtcierreidinsp" id="txtcierreidinsp" class="form-control">
									<!-- ID -->
								</div>
							</div>
							<div class="col-md-3">
								<div class="text-info">Fecha Cierre</div>
								<div>
									<input type="text" name="txtcierrefservicio" id="txtcierrefservicio"
										   class="form-control" data-inputmask-alias="datetime"
										   data-inputmask-inputformat="dd/mm/yyyy" data-mask><!-- ID -->
								</div>
							</div>
							<div class="col-md-6">
								<div class="text-info">Tipo de Cierre</div>
								<div>
									<select class="form-control select2bs4" id="cbocierreTipo" name="cbocierreTipo"
											style="width: 100%;">
										<option value="" selected="selected">Cargando...</option>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6" id="divcierreprograma">
								<div class="text-info">F. Programado</div>
								<div class="input-group date" id="txtcierreFProgramado" data-target-input="nearest">
									<input type="text" id="txtcierreFProg" name="txtcierreFProg"
										   class="form-control datetimepicker-input"
										   data-target="#txtcierreFProgramado"/>
									<div class="input-group-append" data-target="#txtcierreFProgramado"
										 data-toggle="datetimepicker">
										<div class="input-group-text"><i class="fa fa-calendar"></i></div>
									</div>
								</div>
							</div>
							<div class="col-md-3" id="divcierremonto">
								<div class="text-info">Monto</div>
								<div>
									<input type="number" name="txtcierremonto" id="txtcierremonto" class="form-control"
										   min="0.00" value="0.00"><!-- ID -->
								</div>
							</div>
							<div class="col-md-3" id="divcierreviatico">
								<div class="text-info">Viatico</div>
								<div>
									<input type="number" name="txtcierreviatico" id="txtcierreviatico"
										   class="form-control" min="0.00" value="0.00"><!-- ID -->
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="text-info">Comentario</div>
								<div>
									<textarea class="form-control" cols="20" id="mtxtcierrecomentario"
											  name="mtxtcierrecomentario" rows="2"></textarea>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>

			<div class="modal-footer justify-content-between" style="background-color: #dff0d8;">
				<button type="reset" class="btn btn-default" id="mbtnCCierreesp" data-dismiss="modal">Cancelar</button>
				<button type="submit" form="frmCierreespecial" class="btn btn-info" id="mbtnGCierreesp">Grabar</button>
			</div>
		</div>
	</div>
</div>
<!-- /.modal-->

<!-- /.modal-convalidacion -->
<div class="modal fade" id="modalConvalidacion" data-backdrop="static" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<div class="modal-header text-center bg-success">
				<h4 class="modal-title w-100 font-weight-bold text-left">Convalidacion</h4>
<!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close">-->
<!--					<span aria-hidden="true">&times;</span>-->
<!--				</button>-->
			</div>

			<div class="modal-body">
				<form class="form-horizontal" id="frmConvalidacion" name="frmConvalidacion"
					  action="<?= base_url('at/ctrlprov/cregctrolprov/setconvalidacion') ?>"
					  method="POST" enctype="multipart/form-data" role="form">
					<input type="hidden" id="mhdnAccionconvali" name="mhdnAccionconvali">
					<input type="hidden" id="mhdncritresultconvali" name="mhdncritresultconvali">
					<input type="hidden" id="mhdncritdetresultconvali" name="mhdncritdetresultconvali">
					<input type="hidden" id="hdncusuarioconvali" name="hdncusuarioconvali"
						   value="<?php echo $cusuario ?>">
					<div class="form-group">
						<div class="row">
							<div class="col-md-2">
								<label for="txtconvaliidinsp" class="text-info">Codigo</label>
								<div>
									<input type="text" name="txtconvaliidinsp" id="txtconvaliidinsp"
										   class="form-control">
								</div>
							</div>
							<div class="col-md-3">
								<label for="txtconvalifservicio" class="text-info">Fecha Servicio</label>
								<div>
									<input type="text" name="txtconvalifservicio" id="txtconvalifservicio"
										   class="form-control" data-inputmask-alias="datetime"
										   data-inputmask-inputformat="dd/mm/yyyy" data-mask>
								</div>
							</div>
							<div class="col-md-3">
								<label for="txtFConva" class="text-info">F. Convalidacion</label>
								<div class="input-group">
									<input type="text" id="txtFConva" name="txtFConva"
										   autocomplete="off"  class="form-control" />
									<div class="input-group-append">
										<div class="input-group-text"><i class="fa fa-calendar"></i></div>
									</div>
								</div>
							</div>
<!--							<div class="col-md-4">-->
<!--								<label for="txtconvalidocu" class="text-info">Nro Documento</label>-->
<!--								<div>-->
<!--									<input type="text" name="txtconvalidocu" id="txtconvalidocu" class="form-control">-->
<!--								</div>-->
<!--							</div>-->
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-md-3">
								<label for="cbocertificadora" class="text-info">Certificadora </label>
								<div class="">
									<select class="form-control select2bs4" id="cbocertificadora"
											name="cbocertificadora" style="width: 100%;">
										<option value="" selected="selected">Cargando...</option>
									</select>
								</div>
							</div>
							<div class="col-md-4">
								<label for="cbocertificacion" class="text-info">Certificaciones </label>
								<div>
									<select class="form-control select2bs4" id="cbocertificacion"
											name="cbocertificacion" style="width: 100%;">
										<option value="" selected="selected">Cargando...</option>
									</select>
								</div>
							</div>
							<div class="col-md-2">
								<label for="txtconvaliresul" class="text-info">Resultado % </label>
								<div class="">
									<input type="number" name="txtconvaliresul" id="txtconvaliresul"
										   class="form-control" min="0" value="0">
								</div>
							</div>
							<div class="col-md-2">
								<label for="txtconvalivalor" class="text-info">Valor</label>
								<div class="input-group">
									<input type="text" name="txtconvalivalor" id="txtconvalivalor" class="form-control">
									<div class="input-group-append">
										<span class="input-group-text" id="divcolor"></span>
									</div>
								</div>
							</div>
							<div class="col-md-1">
								<label for="txtconvalimes" class="text-info">Mes</label>
								<div>
									<input type="text" name="txtconvalimes" id="txtconvalimes" class="form-control">
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">
								<label for="mtxtNomarchconvali" class="text-info">Archivo</label>
								<div class="input-group">
									<input class="form-control" type="text" name="mtxtNomarchconvali"
										   id="mtxtNomarchconvali">
									<div class="input-group-append">
										<div class="fileUpload btn btn-secondary">
											<span>Subir Archivo</span>
											<input type="file" class="upload" id="mtxtArchivoconvali"
												   accept="application/pdf"
												   name="mtxtArchivoconvali" onchange="escogerArchivo()"/>
										</div>
									</div>
								</div>
								<span style="color: red; font-size: 13px;">+ Los archivos deben estar en formato pdf, docx o xlsx y no deben pesar mas de 60 MB</span>
								<input type="hidden" name="mtxtRutaconvali" id="mtxtRutaconvali">
								<input type="hidden" name="mtxtarchivo" id="mtxtarchivo">
								<input type="hidden" name="sArchivo" id="sArchivo" value="N">
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">
								<label for="mtxtconvalicomentario" class="text-info">Observaciones</label>
								<div>
									<textarea class="form-control" cols="20" id="mtxtconvalicomentario"
											  name="mtxtconvalicomentario" rows="4"></textarea>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>

			<div class="modal-footer" style="background-color: #dff0d8;">
				<button type="reset" class="btn btn-default" id="mbtnCconvali" data-dismiss="modal">
					<i class="fa fa-times" ></i> Cancelar
				</button>
				<button type="submit" class="btn btn-info" form="frmConvalidacion" id="mbtnGconvali">
					<i class="fa fa-save" ></i> Grabar
				</button>
			</div>
		</div>
	</div>
</div>
<!-- /.modal-->

<!-- /.modal-plan inspeccion -->
<div class="modal fade" id="modalPlaninsp" data-backdrop="static" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form class="form-horizontal" id="frmPlaninsp" name="frmPlaninsp"
				  action="<?= base_url('at/ctrlprov/cregctrolprov/setplaninsp') ?>" method="POST"
				  enctype="multipart/form-data" role="form">

				<div class="modal-header text-center bg-success">
					<h4 class="modal-title w-100 font-weight-bold" id="htitleplan">Plan de Inspección</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div class="modal-body">
					<input type="hidden" id="mhdnAccionplaninsp" name="mhdnAccionplaninsp" value="N">
					<input type="hidden" id="hdncusuarioplaninsp" name="hdncusuarioplaninsp"
						   value="<?php echo $cusuario ?>">
					<input type="hidden" id="mhdnidinspplaninsp" name="mhdnidinspplaninsp">
					<input type="hidden" id="mhdnfservicioplaninsp" name="mhdnfservicioplaninsp">
					<input type="hidden" id="mhdntiposervplaninsp" name="mhdntiposervplaninsp">
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">
								<div class="text-info">Objetivo Plan</div>
								<div>
									<textarea class="form-control" cols="20" id="mtxtobjeplaninsp"
											  name="mtxtobjeplaninsp" rows="3"></textarea>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">
								<div class="text-info">Alcance Plan</div>
								<div>
									<textarea class="form-control" cols="20" id="mtxtalcanplaninsp"
											  name="mtxtalcanplaninsp" rows="2"></textarea>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-md-2" id="divfplan">
								<div class="text-info">Fecha Estimada</div>
								<div class="input-group date" id="mtxtFplaninspeccion" data-target-input="nearest">
									<input type="text" id="mtxtFplanins" name="mtxtFplanins"
										   class="form-control datetimepicker-input"
										   data-target="#mtxtFplaninspeccion"/>
									<div class="input-group-append" data-target="#mtxtFplaninspeccion"
										 data-toggle="datetimepicker">
										<div class="input-group-text"><i class="fa fa-calendar"></i></div>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="text-info">Hora Inicio/Estimado</div>
								<div class="input-group date" id="mtxtHplaninspeccion" data-target-input="nearest">
									<input type="text" id="mtxtHplanins" name="mtxtHplanins"
										   class="form-control datetimepicker-input"
										   data-target="#mtxtHplaninspeccion"/>
									<div class="input-group-append" data-target="#mtxtHplaninspeccion"
										 data-toggle="datetimepicker">
										<div class="input-group-text"><i class="far fa-clock"></i></div>
									</div>
								</div>
							</div>
							<div class="col-md-8">
								<div class="text-info">Contacto</div>
								<div>
									<select class="form-control select2bs4" id="cbocontacplanins"
											name="cbocontacplanins" style="width: 100%;">
										<option value="" selected="selected">Cargando...</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">
								<div class="text-info">Requerimiento</div>
								<div>
									<textarea class="form-control" cols="20" id="mtxtrequeplaninsp"
											  name="mtxtrequeplaninsp" rows="2"></textarea>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer justify-content-between" style="background-color: #dff0d8;">
					<button type="reset" class="btn btn-default" id="mbtnCplaninsp" data-dismiss="modal">Cancelar
					</button>
					<button type="submit" class="btn btn-info" id="mbtnGplaninsp">Grabar</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- /.modal-->

<!-- Script Generales -->
<script type="text/javascript">
	var baseurl = "<?php echo base_url();?>";
</script>
