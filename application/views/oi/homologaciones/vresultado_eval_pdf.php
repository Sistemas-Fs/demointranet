<style type="text/css">
	<!--

	.col-1 {
		width: 8.333333%;
	}

	.col-2 {
		width: 16.666667%;
	}

	.col-3 {
		width: 25%;
	}

	.col-4 {
		width: 33.333333%;
	}

	.col-5 {
		width: 41.666667%;
	}

	.col-6 {
		width: 50%;
	}

	.col-7 {
		width: 58.333333%;
	}

	.col-8 {
		width: 66.666667%;
	}

	.col-9 {
		width: 75%;
	}

	.col-10 {
		width: 83.333333%;
	}

	.col-11 {
		width: 91.666667%;
	}

	.col-12 {
		width: 100%;
	}

	.text-center {
		text-align: center;
	}

	.text-left {
		text-align: left;
	}

	.text-right {
		text-align: right;
	}

	.text-justify {
		text-align: justify;
	}

	.uppercase {
		text-transform: uppercase;
	}

	.font-weigth-bold {
		font-weight: bold;
	}

	.text-caratula {
		font-size: 16px;
	}

	.text-sm {
		font-size: 8px;
	}

	.bg-gray {
		background: #a2a1a1;
	}

	.border-gray {
		border: 1px solid #4c4c4c
	}

	.bg-green {
		background: #31a721;
	}

	.table {
		width: 100%;
		border-spacing: 0 0;
		border-collapse: collapse;
		border-color: #4c4c4c;
		font-size: 11px;
	}

	.table-bordered {
		border: 1px solid #4c4c4c;
	}

	.table td {
		padding: 5px;
		font-size: 11px;
	}

	.table th {
		padding: 8px;
		font-size: 11px;
	}

	.table-bordered td, .table-bordered th {
		border: 1px solid #4c4c4c;
	}

	h1 {
		padding: 0;
		margin: 0;
		font-size: 12px;
		font-weight: normal;
	}

	h3 {
		font-size: 11px;
		font-weight: normal;
		padding-bottom: 20px;
	}

	h4 {
		font-size: 10px;
		font-weight: normal;
		margin-top: 0;
		padding-top: 0;
		padding-bottom: 20px;
	}

	.pt {
		padding-top: 20px;
	}

	.pb {
		padding-bottom: 20px;
	}

	.px {
		padding: 20px 0;
	}

	-->
</style>
<page backtop="5mm" backbottom="5mm" backleft="20mm" backright="20mm" style="font-size: 11px">
	<page_footer>
		<div style="width: 100%">
			<table style="width: 100%" border="0" cellspacing="0" cellpadding="0" >
				<tr>
					<td style="width: 10%" ></td>
					<td style="width: 80%" >
						<table style="width: 100%" border="0" cellpadding="0" cellspacing="0" >
							<tr>
								<td style="width: 40%; font-weight: bold; font-size: 11px" >
									Pag. [[page_cu]] de [[page_nb]]
								</td>
								<td style="width: 60%; font-weight: bold; text-align: right; font-size: 11px" >
									RN
								</td>
							</tr>
						</table>
					</td>
					<td style="width: 10%" ></td>
				</tr>
			</table>
		</div>
	</page_footer>
	<div style="width: 100%;" >
		<table style="width: 100%;" border="0" cellpadding="0" cellspacing="0" >
			<tr>
				<td style="width: 20%" >
					<img src="<?php echo base64ResourceConvert(base_url('assets/images/logotransp_fsc.png')) ?>" width="120" height="60" />
				</td>
				<td style="width: 80%; text-align: center; font-weight: bold" >
					<h1 style="font-size: 13px; margin-bottom: 0px; font-weight: bold" >
						RESULTADO DE EVALUACIÓN DE LA HOMOLOGACIÓN DE PROVEEDORES
					</h1>
					<h1 style="margin-top: 5px; margin-bottom: 5px; font-weight: bold" ><?php echo $evalprod->DCLIENTEPRINCIPAL ?></h1>
					Al <?php echo date('d-m-Y', strtotime($evalprod->FINICIOSERVICIO)) ?>
				</td>
			</tr>
		</table>
		<table style="width: 100%; padding-top: 20px" border="0" cellpadding="0" cellspacing="0" >
			<tr>
				<td style="width: 10%" >&nbsp;</td>
				<td style="width: 80%" >
					<table style="width: 100%" border="0" cellspacing="0" cellpadding="0" >
						<tr>
							<td style="width: 30%; font-size: 12px; font-weight: normal" >
								Producto
							</td>
							<td style="width: 2%; font-size: 12px; font-weight: normal; vertical-align: middle" >:</td>
							<td style="width: 68%" >
								<?php echo (isset($producto->PRODUCTO) && !empty($producto->PRODUCTO)) ? $producto->PRODUCTO : ''; ?>
							</td>
						</tr>
						<tr>
							<td colspan="3" style="padding: 4px" ></td>
						</tr>
						<tr>
							<td style="width: 30%; font-size: 12px; font-weight: normal" >
								Marca
							</td>
							<td style="width: 2%; font-size: 12px; font-weight: normal; vertical-align: middle" >:</td>
							<td style="width: 68%" >
								<?php echo (isset($producto->MARCA) && !empty($producto->MARCA)) ? $producto->MARCA : ''; ?>
							</td>
						</tr>
						<tr>
							<td colspan="3" style="padding: 4px" ></td>
						</tr>
						<tr>
							<td style="width: 30%; font-size: 12px; font-weight: normal" >
								Envase Primario
							</td>
							<td style="width: 2%; font-size: 12px; font-weight: normal; vertical-align: middle" >:</td>
							<td style="width: 68%" >
								<?php echo (isset($producto->ENVASEPRIM) && !empty($producto->ENVASEPRIM)) ? $producto->ENVASEPRIM : ''; ?>
							</td>
						</tr>
						<tr>
							<td colspan="3" style="padding: 4px" ></td>
						</tr>
						<tr>
							<td style="width: 30%; font-size: 12px; font-weight: normal" >
								Envase Secundario
							</td>
							<td style="width: 2%; font-size: 12px; font-weight: normal; vertical-align: middle" >:</td>
							<td style="width: 68%" >
								<?php echo (isset($producto->ENVASESECU) && !empty($producto->ENVASESECU)) ? $producto->ENVASESECU : ''; ?>
							</td>
						</tr>
						<tr>
							<td colspan="3" style="padding: 4px" ></td>
						</tr>
						<tr>
							<td style="width: 30%; font-size: 12px; font-weight: normal" >
								Proveedor
							</td>
							<td style="width: 2%; font-size: 12px; font-weight: normal; vertical-align: middle" >:</td>
							<td style="width: 68%" >
								<?php echo $evalprod->DPROVEEDORCLIENTE ?>
							</td>
						</tr>
						<tr>
							<td colspan="3" style="padding: 4px" ></td>
						</tr>
						<tr>
							<td style="width: 30%; font-size: 12px; font-weight: normal" >
								Fabricante
							</td>
							<td style="width: 2%; font-size: 12px; font-weight: normal; vertical-align: middle" >:</td>
							<td style="width: 68%" >
								<?php echo (isset($producto->FABRICANTE) && !empty($producto->FABRICANTE)) ? $producto->FABRICANTE : ''; ?>
							</td>
						</tr>
						<tr>
							<td colspan="3" style="padding: 4px" ></td>
						</tr>
						<tr>
							<td style="width: 30%; font-size: 12px; font-weight: normal" >
								Fecha Inicio
							</td>
							<td style="width: 2%; font-size: 12px; font-weight: normal; vertical-align: middle" >:</td>
							<td style="width: 68%" >
								<?php echo date('d-m-Y', strtotime($evalprod->FINICIOSERVICIO)) ?>
							</td>
						</tr>
					</table>
				</td>
				<td style="width: 10%" >&nbsp;</td>
			</tr>
		</table>
		<div style="width: 100%; margin-top: 30px" >
			<table style="width: 100%" border="0" cellpadding="0" cellspacing="0" >
				<thead>
				<tr>
					<td style="width: 80%; text-align: center; background: #c8cbc8; font-weight: bold; padding: 2px; border: 1px solid #000; font-size: 12px" >
						Requisitos
					</td>
					<td style="text-align: center; background: #c8cbc8; font-weight: bold; padding: 2px; border: 1px solid #000; font-size: 12px" >
						Conformidad
					</td>
				</tr>
				</thead>
				<tbody>
				<?php if (!empty($requisitos)) { ?>
					<?php foreach ($requisitos as $key => $requisito) { ?>
						<?php if ($requisito->NORDEN == '0' || $requisito->STITULO != 'S') continue; ?>
						<tr>
							<td style="width: 80%; text-align: left; font-size: 12px; padding-top: 5px; padding-bottom: 5px" >
								<?php echo $requisito->REQUISITO ?>
							</td>
							<td style="width: 20%; text-align: center; font-size: 12px; padding-top: 5px; padding-bottom: 5px" >
								<?php
								switch ($requisito->CONFORMIDAD) {
									default: echo ''; break;
									case 'C': echo 'Cumple'; break;
									case 'P': echo 'Pendiente'; break;
									case 'A': echo 'No Aplica'; break;
									case 'N': echo 'No Cumple'; break;
								}
								?>
							</td>
						</tr>
					<?php } ?>
				<?php } ?>
				<tr>
					<td colspan="2" style="width: 80%; text-align: center; background: #c8cbc8; font-weight: bold; padding: 2px; border: 1px solid #000; font-size: 12px" >
						Observación
					</td>
				</tr>
				<?php if (!empty($observaciones)) { ?>
					<tr>
						<td colspan="2" style="width: 100%; text-align: left; font-size: 12px; padding-top: 5px; padding-bottom: 5px" >
							<?php echo str_replace("\r\n", '<br>', $observaciones->OBSERVACION) ?>
						</td>
					</tr>
				<?php } ?>
				</tbody>
			</table>
		</div>
		<div style="margin-top: 20px" >
			<h1 style="font-size: 11px; font-weight: bold; text-transform: uppercase" >
				POR FAVOR, ENVIAR LA DOCUMENTACIÓN FALTANTE U OBSERVADA A LA BREVEDAD, CON LA FINALIDAD DE CONTINUAR Y CONCLUIR CON SU PROCESO DE HOMOLOGACIÓN
			</h1>
		</div>
		<div style="margin-top: 20px" >
			Surco,
			<?php
			$fservicio = explode('-', $evalprod->FINICIOSERVICIO);
			echo $fservicio[2] . ' de ' . getMonthText($fservicio[1]) . ' del ' . $fservicio[0];
			?>
		</div>
		<div style="margin-top: 20px; text-align: center; font-weight: bold" >
			FS CERTIFICACIONES S.A.C
		</div>
	</div>
</page>

