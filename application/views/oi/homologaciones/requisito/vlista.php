<?php
$idusuario = $this->session->userdata('s_idusuario');
$cia = $this->session->userdata('s_cia');
$usuario = $this->session->userdata('s_usuario');
?>
<style>
	.custom-file-label.archivoGuia:after {
		display: none;
	}
</style>

<!-- content-header -->
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-6">
				<h1 class="m-0 text-dark">REQUISITO DE HOMOLOGACION</h1>
			</div>
			<div class="col-6">
				<ol class="breadcrumb float-right">
					<li class="breadcrumb-item"><a
							href="<?php echo public_base_url(); ?>cprincipal/principal">Inicio</a></li>
					<li class="breadcrumb-item">Homologaciones</li>
					<li class="breadcrumb-item active">Requisito de Homologación</li>
				</ol>
			</div>
		</div>
	</div>
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
	<div class="container-fluid">

		<div class="card card-lightblue card-outline card-tabs">

			<div class="card-header p-0 pt-1 border-bottom-0">
				<ul class="nav nav-tabs bg-lightblue" id="tabExped" role="tabExped">
					<li class="nav-item">
						<a class="nav-link active" style="color: #000000;" id="tabExped-list-tab" data-toggle="pill"
						   href="#tabExped-list" role="tab" aria-controls="tabExped-list"
						   aria-selected="true">Listado</a>
					</li>
				</ul>
			</div>
			<div class="card-body">

				<div class="tab-content" id="tabExped-tabContent">
					<div class="tab-pane fade show active" id="tabExped-list" role="tabpanel"
						 aria-labelledby="tabExped-list-tab">
						<div class="card card-lightblue">
							<div class="card-header">
								<h3 class="card-title">BUSQUEDA</h3>
								<div class="card-tools">
									<button type="button" class="btn btn-tool" data-card-widget="collapse"><i
											class="fas fa-minus"></i></button>
								</div>
							</div>
							<form method="post" id="frmBuscarExp"
								  action="<?= base_url('oi/homologaciones/creqhomologaciones/lista') ?>"
								  enctype="multipart/form-data" role="form">
								<div class="card-body">
									<!-- INICIO -->
									<div class="row">

										<div class="col-md-3 col-12">
											<!-- text input -->
											<div class="form-group">
												<label>Área</label>
												<select id="cboArea" class="form-control select2bs4"
														name="cboArea">
													<option value="" selected="selected">::Elegir</option>
												</select>
											</div>
										</div>
										<div class="col-md-5 col-12">
											<div class="form-group">
												<label>Tipo</label>
												<select id="cboTipo" class="form-control select2bs4"
														name="cboTipo">
													<option value="" selected="selected">::Elegir</option>
												</select>
											</div>
										</div>
										<div class="col-md-4 col-12">
											<div class="form-group">
												<label>Marca</label>
												<select id="cboMarca" class="form-control select2bs4"
														name="cboMarca">
													<option value="MP" >Marca Propia</option>
													<option value="OM" selected="selected" >Otras Marcas</option>
												</select>
											</div>
										</div>
									</div>


									<div class="form-group">
										<div class="col-md-12 text-right">
											<button id="btnBuscar" type="button" role="button" class="btn bg-lightblue"><i
													class="fa fa-search"></i>Buscar
											</button>
										</div>
									</div>

								</div>
							</form>
						</div>

						<div class="row">
							<div class="col-12">
								<div class="card card-lightblue">
									<div class="card-header">
										Listado de Requisitos: <span class="font-weight-bold" id="clienteExp"></span>
									</div>

									<div class="card-body">
										<table id="tblListado" class="table table-bordered table-striped "
											   style="width:100%">
											<thead>
											<tr>
												<th>Nro.</th>
												<th>Descripción</th>
												<th>Alarma</th>
												<th>Área</th>
												<th>Tipo</th>
												<th>Tipo Marca</th>
												<th>Título</th>
												<th>Estado</th>
											</tr>
											</thead>
											<tbody>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- /.Main content -->


<!-- Script Generales -->
<script type="text/javascript">
	var baseurl = "<?php echo base_url();?>";
</script>
