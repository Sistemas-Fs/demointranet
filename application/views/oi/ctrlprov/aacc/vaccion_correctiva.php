<?php
$codcliente = $this->session->userdata('s_ccliente');
$idusuario = $this->session->userdata('s_idusuario');
$idrol = $this->session->userdata('s_idrol');
$cia = $this->session->userdata('s_cia');
?>

<style>
	.select2-container--default .select2-selection--multiple .select2-selection__choice {
		color: #000;
	}
</style>

<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">
					Registro de accion correctiva
				</h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="<?php echo public_base_url(); ?>cprincipal/principal">Home</a>
					</li>
					<li class="breadcrumb-item active">Inspeccion de Proveedores</li>
				</ol>
			</div>
		</div>
	</div>
</div>

<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card card-success card-outline card-tabs">
					<div class="card-header p-0 pt-1 border-bottom-0">
						<ul class="nav nav-tabs" id="tabptcliente" style="background-color: #28a745;" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" style="color: #000000;" id="tabReg1-tab"
								   data-toggle="pill" href="#tabReg1" role="tab"
								   aria-controls="tabReg1" aria-selected="true">LISTADO</a>
							</li>
						</ul>
					</div>
					<div class="card-body">
						<div class="tab-content">
							<div class="tab-pane fade show active" id="tabReg1" role="tabpanel">
								<!--Contenedor de consulta-->
								<div class="card card-success">
									<div class="card-header">
										<h3 class="card-title">Busqueda</h3>

										<div class="card-tools">
											<button type="button" class="btn btn-tool" data-card-widget="collapse"><i
														class="fas fa-minus"></i></button>
										</div>
									</div>
									<div class="card-body">
										<form class="form-horizontal" id="frmFileACC">
											<input type="hidden" id="idcliente"
												   value="<?php echo $codcliente ?>">
											<input type="hidden" id="idusuario"
												   value="<?php echo $idusuario; ?>">
											<input type="hidden" id="idrol"
												   value="<?php echo $idrol; ?>">
											<input type="hidden" id="idcia" name="idcia"
												   value="<?php echo $cia ?>">
											<div class="row">
												<div class="col-xl-4 col-lg-5 col-md-12 col-sm-9 col-12">
													<div class="form-group">
														<label for="filtro_cliente">Cliente</label>
														<div class="input-group">
															<select name="filtro_cliente" id="filtro_cliente"
																	class="custom-select"></select>
															<div class="input-group-append" >
																<button type="button" role="button" id="btnFiltroInsp"
																		class="btn btn-light active" >
																	<i class="fa fa-search" ></i> Buscar
																</button>
															</div>
														</div>
													</div>
												</div>
												<div class="col-xl-2 col-lg-2 col-md-3 col-sm-3 col-12" >
													<div class="form-group">
														<label for="filtro_codigo">Código</label>
														<div class="input-group">
															<input type="text" class="form-control" readonly
																   id="filtro_codigo" name="filtro_codigo" aria-label=""
																   value=""/>
														</div>
													</div>
												</div>
												<div class="col-xl-4 col-lg-4 col-md-6 col-sm-9 col-12" >
													<div class="form-group">
														<label for="filtro_proveedor">Proveedor</label>
														<div class="input-group">
															<input name="filtro_proveedor" id="filtro_proveedor" readonly
																	class="form-control" />
														</div>
													</div>
												</div>
												<div class="col-xl-2 col-lg-2 col-md-3 col-sm-3 col-12">
													<div class="form-group">
														<label for="filtro_fecha" class="">
															Fecha de Inspección
														</label>
														<input type="text" class="form-control datepicker" readonly
															   id="filtro_fecha" name="filtro_fecha" aria-label=""
															   value=""/>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-xl-7 col-lg-7 col-md-9 col-sm-12 col-12">
													<div class="form-group">
														<label for="filtro_tipo_estado">Excel Cargado</label>
														<div class="input-group" >
															<div class="input-group-append btn-exist-file" style="display: none" >
																<button type="button" role="button"
																		id="btnEliminarArchivo"
																		class="btn btn-light" >
																	<i class="fa fa-trash" ></i>
																</button>
															</div>
															<input type="file" class="form-control"
																   id="file_acc" name="file_acc" aria-label=""
																   value=""/>
															<div class="input-group-append" >
																<button type="button" role="button"
																		id="btnCargarArchivo"
																		class="btn btn-danger" >
																	<i class="fa fa-save" ></i> Cargar Archivo
																</button>
																<button type="button" role="button"
																		id="btnDescargarArchivo" style="display: none"
																		class="btn btn-primary btn-exist-file" >
																	<i class="fa fa-save" ></i> Descargar Archivo
																</button>
															</div>
														</div>
													</div>
												</div>
											</div>
										</form>
									</div>
									<!--Contenedor de botones-->
									<div class="card-footer">
										<div class="d-flex flex-row justify-content-between">
											<div class="col-sm-6 col-12" >

											</div>
											<div class="col-sm-6 col-12 text-right">
												<button type="button" class="btn btn-primary mr-2" id="btnBuscar">
													<i class="fa fa-fw fa-search"></i> Buscar
												</button>
												<button type="button" class="btn btn-success" id="btnExportar">
													<i class="fa fa-fw fa-download"></i> Exportar
												</button>
											</div>
										</div>
									</div>
								</div>
								<div class="card card-success">
									<div class="card-header with-border">
										<h3 class="card-title">Listado</h3>
									</div>
									<div class="card-body">
										<div>
											<table id="tblAccionCorrectiva" class="table table-striped table-bordered"
												   style="width:100%">
												<thead class="bg-secondary" >
												<tr>
													<th class="text-center" >ID</th>
													<th class="text-left" >Requisito</th>
													<th class="text-left" >Excluyente</th>
													<th class="text-left" >Hallazgo</th>
													<th class="text-left" >Acción Correctiva</th>
													<th class="text-center" >Fecha Correción</th>
													<th class="text-left" >Responsable</th>
<!--													<th>Acepta Acc. Correctiva</th>-->
												</tr>
												</thead>
												<tbody>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Modal -->
<div class="modal fade" id="modalInspecciones" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-scrollable modal-xl">
		<div class="modal-content">
			<div class="modal-header bg-success">
				<h5 class="modal-title fs w-100 font-weight-bold" id="staticBackdropLabel">Inspecciones a proveedores</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row justify-content-end" >
					<div class="form-group col-xl-6 col-lg-6 col-md-6 col-12" >
						<label for="filter_insprov">
							Proveedor / Nro de Inspeccion
						</label>
						<div class="input-group" >
							<input type="text" class="form-control" id="filter_insprov"
								   placeholder="..."
								   value="" >
							<div class="input-group-append" >
								<button type="button" role="button" id="btnTblInsp"
										class="btn btn-light active" >
									<i class="fa fa-search" ></i> Buscar
								</button>
							</div>
						</div>
					</div>
				</div>
				<div class="table-responsive" >
					<table class="table table-sm table-bordered table-hover" id="tblInspProv" >
						<thead class="bg-success" >
						<tr>
							<th class="text-center" ></th>
							<th class="text-center" >N°</th>
							<th class="text-center" >Proveedor</th>
							<th class="text-center" >Nro. Inspección</th>
							<th class="text-center" >Fecha Servicio</th>
							<th class="text-center" >Acc. Correctiva</th>
						</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
