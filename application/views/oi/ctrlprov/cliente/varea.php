<style>
</style>
 
 <div class="card card-primary card-outline"> 
    <div class="card-header">    
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-12">            
                <fieldset class="scheduler-border">
                    <legend class="scheduler-border text-primary">CATEGORIAS DEL CLIENTE</legend>
                    <input type="hidden" id="mhdnconCcliente" name="mhdnconCcliente">
                    <input type="hidden" id="mhdnconDcliente" name="mhdnconDcliente">
                    <input type="hidden" id="mhdnconDdireccion" name="mhdnconDdireccion">
                    <input type="hidden" id="mhdnconCClienteArea" name="mhdnconCClienteArea">
                    <div class="form-group">
                        <div class="row"> 
                            <div class="col-md-2">
                                <h6><label>CLIENTE :</label></h6>
                            </div>
                            <div class="col-md-10">
                                <h6 id="lblClienteArea"></h6>
                            </div>
                        </div>
                        <div class="row"> 
                            <div class="col-md-2">
                                <h6><label>DIRECCIÓN :</label></h6>
                            </div>
                            <div class="col-md-10">
                                <h6 id="lblDirclieArea"></h6>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
    </div> 
    <div class="card-footer justify-content-between"> 
        <div class="row">
            <div class="col-md-12">
                <div class="text-right">
                    <button type="button" class="btn btn-secondary" id="btnRetornarListaContacto"><i class="fas fa-undo-alt"></i> Retornar</button>
                    <button type="button" class="btn btn-outline-info"   data-toggle="modal" data-target="#modalAddArea"><i class="fas fa-plus"></i> Crear Nuevo</button>
                </div>
            </div>
        </div>
    </div> 
</div>

<div class="card card-primary" id="cardListArea">
    <div class="card-header">
        <h3 class="card-title"><b>LISTADO DE CATEGORIAS DEL CLIENTE</b></h3>
     </div>
                
    <div class="card-body">
        <div class="form-group">
            <div class="row"> 
                <div class="col-md-12">
                    <table id="tblListArea" class="table table-striped table-bordered compact" style="width:100%">
                        <thead>
                            <tr>
                                <th>N°</th>
                                <th></th>
                                <th>DESCRIPCION</th>
                                <th>Área / División</th>
                                <th>ESTADO</th>
                                <th>TIPO</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- /.modal-ubigeo --> 
<div class="modal fade" id="modalAddArea" data-backdrop="static" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
     
        <div class="modal-header text-center bg-primary">
            <h4 class="modal-title w-100 font-weight-bold">Añadir Categoría</h4>
        </div>

        <div class="card-body">
            <form class="form-horizontal" id="frmMantArea" name="frmMantArea" method="POST" enctype="multipart/form-data" role="form"> 
                <input type="hidden" id="mhdnIdArea" name="mhdnIdArea"> 
                <input type="hidden" id="mhdnIdClieArea" name="mhdnIdClieArea">
                <input type="hidden" id="mhdnAccionContacto" name="mhdnAccionContacto" value="">
                <div class="form-group">
                    <div class="row"> 
                        <div class="col-sm-8 col-12">
                            <div class="text-info">Descripcion de Categoría *</div>
                            <div>
                                <input type="text" class="form-control" name="str_descripcion_area" id="str_descripcion_area">
                            </div>
                        </div>
                        <div class="col-sm-4 col-12" style="" >
                            <div class="text-info">Área / División *</div>
                            <div>
                                <input type="text" class="form-control" maxlength="100"
									   name="str_jerarquia_area" id="str_jerarquia_area">
                            </div>
                        </div>
					</div>
					<div class="row" >
                        <div class="col-sm-4">
                            <div class="text-info">Consecionario</div>
                            <div>
                                <select class="form-control select2bs4" id="cboConsecionario" name="cboConsecionario" style="width: 100%;">
                                    
                                    <option value="S">Si</option>
                                    <option value="N" selected>No</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-4" style="display: none" >
                            <div class="text-info">Tipo</div>
                            <div>
                            <select class="form-control select2bs4" id="cboTipoArea" name="cboTipoArea" style="width: 100%;">
                                    
                                    <option value="IP" selected>IP</option>
                                    <option value="LAB">LAB</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="text-info">Estado</div>
                            <div>
                            <select class="form-control select2bs4" id="cboEstado" name="cboEstado" style="width: 100%;">
                                   
                                    <option value="A" selected>Activo</option>
                                    <option value="I">Inactivo</option>
                                </select>
                            </div>
                        </div>
                    </div>                
                </div> 
                  
            </form>
        </div>

        <div class="modal-footer justify-content-between" style="background-color: #dff0d8;">
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-window-close"></i> Cerrar</button>
            <button type="submit" class="btn btn-primary" id="btnguardararea"><i class="fa fa-fw fa-check"></i>Grabar</button> 
        </div>
    </div>
  </div>
</div> 
<!-- /.modal-->



