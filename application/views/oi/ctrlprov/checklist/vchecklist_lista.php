<?php
$codcliente = $this->session->userdata('s_ccliente');
$idusuario = $this->session->userdata('s_idusuario');
$idrol = $this->session->userdata('s_idrol');
$cia = $this->session->userdata('s_cia');
?>

<style>
	.select2-container--default .select2-selection--multiple .select2-selection__choice {
		color: #000;
	}
	select.custom-select {
		width: 100% !important;
	}
</style>

<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">
					LISTADO DE CHECK LIST
				</h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="<?php echo public_base_url(); ?>cprincipal/principal">Home</a>
					</li>
					<li class="breadcrumb-item active">Área Técnica</li>
				</ol>
			</div>
		</div>
	</div>
</div>

<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card card-success card-outline card-tabs">
					<div class="card-header p-0 pt-1 border-bottom-0">
						<ul class="nav nav-tabs" id="tabptcliente" style="background-color: #28a745;" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" style="color: #000000;" id="tabReg1-tab"
								   data-toggle="pill" href="#tabReg1" role="tab"
								   aria-controls="tabReg1" aria-selected="true">LISTADO</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" style="color: #000000;" id="tabReg2-tab" data-toggle="pill"
								   href="#tabReg2" role="tab" aria-controls="tabReg2"
								   aria-selected="false">REGISTRO</a>
							</li>
						</ul>
					</div>
					<div class="card-body">
						<div class="tab-content">
							<div class="tab-pane fade show active" id="tabReg1" role="tabpanel">
								<!--Contenedor de consulta-->
								<div class="card card-success">
									<div class="card-header">
										<h3 class="card-title">Busqueda</h3>

										<div class="card-tools">
											<button type="button" class="btn btn-tool" data-card-widget="collapse"><i
													class="fas fa-minus"></i></button>
										</div>
									</div>
									<div class="card-body">
										<form class="form-horizontal" id="frmBuscarTramite">
											<input type="hidden" id="idcliente"
												   value="<?php echo $codcliente ?>">
											<input type="hidden" id="idusuario"
												   value="<?php echo $idusuario; ?>">
											<input type="hidden" id="idrol"
												   value="<?php echo $idrol; ?>">
											<input type="hidden" id="idcia" name="idcia"
												   value="<?php echo $cia ?>">
											<div class="row">
												<div class="col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12">
													<div class="form-group">
														<label for="filtro_texto">Buscar</label>
														<div class="input-group">
															<input type="text" class="form-control"
																   id="filtro_texto" name="filtro_texto"
																   value="" />
														</div>
													</div>
												</div>
												<div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-12"
													 style="" >
													<div class="form-group">
														<label for="filtro_area">Área</label>
														<div class="input-group">
															<select name="filtro_area" id="filtro_area"
																	class="custom-select"></select>
														</div>
													</div>
												</div>
												<div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-12"
													 style="" >
													<div class="form-group">
														<label for="filtro_servicio">Servicio</label>
														<div class="input-group">
															<select name="filtro_servicio" id="filtro_servicio"
																	class="custom-select"></select>
														</div>
													</div>
												</div>
											</div>
										</form>
									</div>
									<!--Contenedor de botones-->
									<div class="card-footer">
										<div class="d-flex flex-row justify-content-end">
											<div class="col-sm-6 col-12 text-right">
												<button type="button" class="btn btn-success" id="btnNuevoCheckList">
													<i class="fa fa-fw fa-plus"></i> Crear Check List
												</button>
												<button type="button" class="btn btn-default" id="btnBuscar">
													<i class="fa fa-fw fa-search"></i> Buscar
												</button>
											</div>
										</div>
									</div>
								</div>
								<div class="card card-success">
									<div class="card-header with-border">
										<h3 class="card-title">Listado</h3>
									</div>
									<div class="card-body">
										<div>
											<table id="tblInspecciones" class="table table-striped table-bordered"
												   style="width:100%">
												<thead>
												<tr>
													<th style="width: 80px; min-width: 80px"></th>
													<th>Código</th>
													<th>Check List Descripción</th>
													<th>Área</th>
													<th>Servicio</th>
													<th>Sistema</th>
													<th>Organismo</th>
													<th>Rubro</th>
													<th>Tipo</th>
													<th>Cerrado</th>
													<th>Uso</th>
													<th>Peso</th>
													<th>Estado</th>
												</tr>
												</thead>
												<tbody></tbody>
												<tfoot>
												<tr>
													<th></th>
												</tr>
												</tfoot>
											</table>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="tabReg2">
								<?php $this->load->view('at/ctrlprov/checklist/vchecklist_generar'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

