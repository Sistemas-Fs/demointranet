<style>
	.select2-container .select2-selection--single {
		height: auto;
	}
</style>

<div class="modal fade" id="formulaCheckList"
	 tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Formula</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="form-group row" >
					<label for="formula_checklist_codigo" class="col-xl-4 col-lg-4 col-md-5 col-sm-12 col-12" >
						Código
					</label>
					<div class="col-xl-8 col-lg-8 col-md-7 col-sm-12 col-12" >
						<input type="text" class="form-control" maxlength="0"
							   id="formula_checklist_codigo" readonly
							   value="" >
					</div>
				</div>
				<div class="form-group row" >
					<label for="formula_checklist_descripcion" class="col-xl-4 col-lg-4 col-md-5 col-sm-12 col-12" >
						Descripción
					</label>
					<div class="col-xl-8 col-lg-8 col-md-7 col-sm-12 col-12" >
						<input type="text" class="form-control" maxlength="0"
							   id="formula_checklist_descripcion" readonly
							   value="" >
					</div>
				</div>
				<div class="table-responsive" >
					<table class="table table-bordered table-striped" id="tblFormula" >
						<thead>
						<tr>
							<th class="text-left" style="min-width: 15rem;" >Formula Evaluación</th>
							<th class="text-left" style="min-width: 8rem; width: 8rem" >Estado</th>
							<th class="text-left" style="min-width: 8rem; width: 8rem" ></th>
							<th class="text-left" style="min-width: 8rem; width: 8rem" ></th>
						</tr>
						</thead>
						<tbody>
						</tbody>
						<tfoot>
						<tr>
							<td colspan="4" >
								<button type="button" role="button" class="btn btn-link" id="btnFormula" >
									<i class="fa fa-plus" ></i> Agregar nueva formula
								</button>
							</td>
						</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
