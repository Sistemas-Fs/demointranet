<style type="text/css">
	<!--

	.col-1 {
		width: 8.333333%;
	}

	.col-2 {
		width: 16.666667%;
	}

	.col-3 {
		width: 25%;
	}

	.col-4 {
		width: 33.333333%;
	}

	.col-5 {
		width: 41.666667%;
	}

	.col-6 {
		width: 50%;
	}

	.col-7 {
		width: 58.333333%;
	}

	.col-8 {
		width: 66.666667%;
	}

	.col-9 {
		width: 75%;
	}

	.col-10 {
		width: 83.333333%;
	}

	.col-11 {
		width: 91.666667%;
	}

	.col-12 {
		width: 100%;
	}

	.text-center {
		text-align: center;
	}

	.text-left {
		text-align: left;
	}

	.text-right {
		text-align: right;
	}

	.text-justify {
		text-align: justify;
	}

	.uppercase {
		text-transform: uppercase;
	}

	.font-weigth-bold {
		font-weight: bold;
	}

	.text-caratula {
		font-size: 16px;
	}

	.text-sm {
		font-size: 8px;
	}

	.bg-gray {
		background: #a2a1a1;
	}

	.border-gray {
		border: 1px solid #4c4c4c
	}

	.bg-green {
		background: #31a721;
	}

	.table {
		width: 100%;
		border-spacing: 0 0;
		border-collapse: collapse;
		border-color: #4c4c4c;
		font-size: 11px;
	}

	.table-bordered {
		border: 1px solid #4c4c4c;
	}

	.table td {
		padding: 5px;
		font-size: 11px;
	}

	.table th {
		padding: 8px;
		font-size: 11px;
	}

	.table-bordered td, .table-bordered th {
		border: 1px solid #4c4c4c;
	}

	h1 {
		padding: 0;
		margin: 0;
		font-size: 12px;
		font-weight: normal;
	}

	h3 {
		font-size: 11px;
		font-weight: bold;
		padding-bottom: 20px;
	}

	h4 {
		font-size: 10px;
		font-weight: normal;
		margin-top: 0;
		padding-top: 0;
		padding-bottom: 20px;
	}

	.pt {
		padding-top: 20px;
	}

	.pb {
		padding-bottom: 20px;
	}

	.px {
		padding: 20px 0;
	}

	-->
</style>
<page backtop="30mm" backbottom="3mm" backleft="10mm" backright="10mm" style="font-size: 11px">
	<page_header>
		<div style="padding: 15px 40px;">
			<table class="" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td style="width: 447px;">
						<img src="<?php echo base64ResourceConvert(base_url('assets/images/logoGrupoFSC.jpg')) ?>"
							 alt="GRUPOFS" style="width: 150px; height: 80px">
					</td>
					<td class="text-right" style="width: 220px; vertical-align: top; font-size: 11px; color: #6d6d6d">
						Av. Del Pinar N° 110 Of.405 - 407 <br>
						Urb. Chacarilla del Estanque <br>
						Santiago de Surco - Lima - Perú <br>
						Celular: (51-1) 999579565 <br>
						fsc@fscertificaciones.com <br>
						www.fscertificaciones.com
					</td>
				</tr>
			</table>
		</div>
	</page_header>
	<page_footer>
		<div style="text-align: right; font-size: 11px; padding: 0 27px">
			Pag. [[page_cu]] de [[page_nb]]
		</div>
	</page_footer>
	<div class="col-12 text-caratula">
		<table class="table">
			<tr>
				<td style="height: 150px">&nbsp;</td>
			</tr>
			<tr>
				<td class="col-12 text-center"
					style="vertical-align: middle; height: 170px">
					<span class="text-caratula">INFORME TECNICO N° <?php echo (empty($caratula->dinforme)) ? 'XXX-XXXX' : $caratula->dinforme; ?></span>
				</td>
			</tr>
			<tr>
				<td class="col-12" style="border: 0; padding-left: 30px;">
					<table class="table border-gray" style="background-color: #e7e7e7; border-radius: 25px" >
						<tr>
							<td class="col-12 text-center font-weigth-bold uppercase" style="padding: 15px; border-radius: 10px;">
								<span class="text-caratula">
									INSPECCION DE VERIFICACION DEL SISTEMA DE INOCUIDAD EN <br>
									LA LÍNEA DE <?php echo $caratula->lineaprocesoclte ?>
								</span>
								<br>
								<br>
								<span class="text-caratula">
									CONTROL DE PROVEEDORES <br>
									<?php echo $caratula->nomcliente ?>
								</span>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="height: 50px">&nbsp;</td>
			</tr>
			<tr>
				<td class="col-12 text-center" style="height: 50px; vertical-align: middle">
					<p class="uppercase pb text-caratula">
						<?php
						if (!empty($cuadro3)) {
							echo $cuadro3->prov_drazonsocial;
							echo (!empty($cuadro3->maqui_drazonsocial))
									? '<br>' . $cuadro3->maqui_drazonsocial
									: '';
						}
						?>
					</p>
					<p class="uppercase text-caratula" style="margin-top: 0; padding-top: 0">
						<?php
						echo $caratula->est_prov;
						?>
					</p>
				</td>
			</tr>
			<tr>
				<td class="col-12 text-center" style="height: 250px; vertical-align: middle">
					<p class="uppercase text-caratula">
						<?php
						if (empty($caratula->finformefin)) {
							echo 'XX-XX-XXXX';
						} else {
							$fservicio = explode('-', $caratula->finformefin);
							echo $fservicio[2] . ' DE ' . getMonthText($fservicio[1]) . ' DE ' . $fservicio[0];
						}
						?>
					</p>
				</td>
			</tr>
		</table>
	</div>
	<div style="page-break-after:always"></div>
	<h3 class="text-left font-weigth-bold">I. &nbsp;&nbsp;INFORMACIÓN GENERAL</h3>
	<div style="padding-left: 8px">
		<table class="table">
			<tr>
				<td class="text-left" style="width: 100%;">
					<table style="width: 100%; border: 0" cellspacing="0" cellpadding="0" border="0" >
						<tr>
							<td style="width: 70%; padding: 0" >
								<b>Razon Social:</b> <?php echo (!empty($cuadro3)) ? $cuadro3->prov_drazonsocial : ''; ?>
							</td>
							<td style="width: 30%; padding: 0" >
								<b>RUC:</b> <?php echo (!empty($cuadro3)) ? $cuadro3->prov_nruc : ''; ?>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td class="text-left" style="width: 100%; padding: 4px">
					<b>Fecha de Inspeccion:</b> <?php
					if (!empty($cuadro3)) {
						$fechaInsp = explode('-', $cuadro3->fservicio);
						echo $fechaInsp[2] . ' ' . getMonthText(intval($fechaInsp[1])) . ' ' . $fechaInsp[0];
					}
					?>
				</td>
			</tr>
			<tr>
				<td class="text-left" style="width: 100%; padding: 4px">
					<b>Tipo de Actividad:</b> <?php echo (!empty($cuadro3)) ? $cuadro3->DTIPOACTIVIDAD : ''; ?>
				</td>
			</tr>
			<tr>
				<td class="text-left" style="width: 100%; padding: 4px">
					<b>Dirección del establecimiento:</b>
					<?php echo (!empty($cuadro3)) ? $cuadro3->prov_ddireccioncliente . ' ' . $cuadro3->prov_ubigeo : ''; ?>
				</td>
			</tr>
			<tr>
				<td class="text-left" style="width: 100%; padding: 4px">
					<b>Telefono:</b>
				</td>
			</tr>
			<tr>
				<td class="text-left" style="width: 100%; padding: 4px">
					<b>Representante del proveedor:</b>
					<?php echo (!empty($cuadro3)) ? $cuadro3->drepresentante : ''; ?>
				</td>
			</tr>
			<tr>
				<td class="text-left" style="width: 100%; padding: 4px">
					<b>Email:</b>
					<?php echo (!empty($cuadro3)) ? $cuadro3->demailrepresentante : ''; ?>
				</td>
			</tr>
			<tr>
				<td class="text-left" style="width: 100%; padding: 4px">
					<?php if (!empty($responsables)) { ?>
						<?php foreach ($responsables as $key => $valueResponsable) { ?>
							<b>Responsable de la inspección:</b>
							<?php echo $valueResponsable->ap_nom ?> (<?php echo $valueResponsable->dcargocontacto ?>) <br>
							<b>Email:</b> <?php echo (!empty($valueResponsable->dmail)) ? $valueResponsable->dmail : '' ?> <br>
							<b>Tel./Cel.:</b> <?php echo (!empty($valueResponsable->dtelefono)) ? $valueResponsable->dtelefono : '' ?>
							<?php echo (isset($responsables[$key + 1])) ? '<br>' : ''; ?>
						<?php } ?>
					<?php } ?>
				</td>
			</tr>
			<tr>
				<td class="text-left" style="width: 100%; padding: 4px">
					<b>Inspector FSC:</b> <?php echo (!empty($inspector)) ? $inspector->dnombre . ' ' . $inspector->dapepat . ' ' . $inspector->dapemat : ''; ?>
				</td>
			</tr>
			<tr>
				<td class="text-left" style="width: 100%; padding: 4px">
					<b>Destino del Producto:</b> <?php echo (!empty($cuadro3)) ? $cuadro3->DTIPODESTINO : ''; ?>
				</td>
			</tr>
			<tr>
				<td class="text-left" style="width: 100%; padding: 4px">
					<b>Productos que abastecen:</b> <?php echo (!empty($cuadro1)) ? $cuadro1->marca : ''; ?>
				</td>
			</tr>
			<tr>
				<td class="text-left" style="width: 100%; padding: 4px">
					<b>Marca o denominación:</b> <?php echo (!empty($cuadro1)) ? $cuadro1->marca2 : ''; ?>
				</td>
			</tr>
		</table>
	</div>
	<h3 class="text-left font-weigth-bold">II. &nbsp;&nbsp;OBJETIVOS</h3>
	<div style="padding-left: 8px">
		<table class="table">
			<tr>
				<td class="text-left text-justify" style="width: 100%; padding: 4px">
					Verificar el grado de implementación de los requisitos de la legislación sanitaria de
					alimentos, que conlleve a la provisión de productos inocuos para <?php echo $caratula->nomcliente ?>
				</td>
			</tr>
		</table>
	</div>
	<h3 class="text-left font-weigth-bold">III. &nbsp;&nbsp;ALCANCE</h3>
	<div style="padding-left: 8px">
		<table class="table">
			<tr>
				<td class="text-left text-justify" style="width: 100%; padding: 4px">
					La inspección estuvo dirigida a la línea de <?php echo (!empty($cuadro1)) ? $cuadro1->alcance : ''; ?>
				</td>
			</tr>
		</table>
	</div>
	<h3 class="text-left font-weigth-bold">IV. &nbsp;&nbsp;CRITERIO DE INSPECCION</h3>
	<div style="padding-left: 8px">
		<table class="table">
			<tr>
				<td class="text-left text-justify" style="width: 100%; padding: 4px">
					<?php echo (!empty($criterioInspeccion)) ? nl2br($criterioInspeccion->dcriterios) : ''; ?>
				</td>
			</tr>
		</table>
	</div>
	<h3 class="text-left font-weigth-bold">V. &nbsp;&nbsp;CRITERIO DE CALIFICACION</h3>
	<div style="padding-left: 8px">
		<table class="table">
			<tr>
				<td class="text-left text-justify" style="width: 100%; padding: 4px">
					Para efectos de la inspección se sonsideran los siguientes criterios: <br>
				</td>
			</tr>
		</table>
	</div>
	<table class="table table-bordered pb" style="padding-left: 12px">
		<?php if (!empty($criterioEvaluacion)) { ?>
			<?php foreach ($criterioEvaluacion as $key => $value) { ?>
				<tr>
					<td class="text-left text-left" style="width: 200px">
						<?php echo $value->ddetallevalor ?>
					</td>
					<td class="text-left text-justify" style="width: 417px">
						<?php echo $value->dvaloracion ?>
					</td>
				</tr>
			<?php } ?>
		<?php } ?>
	</table>
	<div style="padding-left: 8px; padding-top: 10px">
		<table class="table">
			<tr>
				<td class="text-left text-justify" style="width: 100%; padding: 4px">
					Con respecto al resultado de inspección, <?php echo $caratula->nomcliente ?>, considera los siguientes
					requisitos excluyentes en su lista de verificación.
				</td>
			</tr>
		</table>
	</div>
	<table class="table table-bordered pt" style="padding-left: 12px">
		<tr>
			<td class="text-center bg-gray" style="width: 643px">
				Requisitos excluyentes
			</td>
		</tr>
	</table>
	<table class="table table-bordered" style="padding-left: 12px">
		<tr>
			<td class="text-center bg-gray" style="width: 75px;">
				N° <br> Requisito
			</td>
			<td class="text-center bg-gray" style="width: 545px;">
				Descripción del requisito
			</td>
		</tr>
	</table>
	<table class="table table-bordered" style="padding-left: 12px">
		<?php if (!empty($requisitosExcluyentes)) { ?>
			<?php foreach ($requisitosExcluyentes as $key => $value) { ?>
				<tr>
					<td class="text-left" style="width: 75px">
						<?php echo $value->dnumerador ?>
					</td>
					<td class="text-left text-justify" style="width: 545px">
						<?php echo $value->drequisito ?>
					</td>
				</tr>
			<?php } ?>
		<?php } ?>
	</table>
	<table class="table pt" style="padding-left: 8px">
		<tr>
			<td class="text-left" style="width: 100%">
				Asimismo, se considerará como excluyente, solo cuando el hallazgo sea considerado como
				No Conformidad y con puntuación de 0, bajando el nivel de calificación según el siguiente cuadro:
			</td>
		</tr>
	</table>
	<br><br>
	<div style="padding-left: 8px" >
		<table class="table table-bordered">
			<tr>
				<td class="text-center" style="width: 170px; font-weight: bold">
					N° de Excluyentes
				</td>
				<td class="text-center" style="width: 450px; font-weight: bold">
					Aplicación
				</td>
			</tr>
			<tr>
				<td class="text-center" style="width: 170px">
					1 - 2 Excluyentes
				</td>
				<td class="text-center" style="width: 450px">
					Disminuye 01 nivel de calificación
				</td>
			</tr>
			<tr>
				<td class="text-center" style="width: 170px">
					2 - 4 Excluyentes
				</td>
				<td class="text-center" style="width: 450px">
					Disminuye 02 nivel de calificación
				</td>
			</tr>
			<tr>
				<td class="text-center" style="width: 170px">
					>= 5 Excluyentes
				</td>
				<td class="text-center" style="width: 450px">
					Disminuye 03 nivel de calificación
				</td>
			</tr>
		</table>
	</div>
	<h3 class="text-left font-weigth-bold">VI. &nbsp;&nbsp;METODOLOGIA</h3>
	<div style="padding-left: 8px">
		<table class="table">
			<tr>
				<td class="text-left text-justify" style="width: 100%;">
					La inspección fue llevada a cabo a través de una visita al establecimiento.
					Durante el recorrido de las instalaciones, se realizaron observaciones de infraestructura,
					así como, a las diferentes operaciones; se realizaron entrevistas al personal responsable
					y operativo, y se revisaron los registros del Sistema de Inocuidad implementado.
				</td>
			</tr>
		</table>
		<br><br>
		<table class="table table-bordered">
			<tr>
				<td class="text-center" style="width: 40px">
					N°
				</td>
				<td class="text-center" style="width: 180px">
					Actividades
				</td>
				<td class="text-center" style="width: 375px">
					Participantes
				</td>
			</tr>
			<?php if (!empty($involucrados)) { ?>
				<?php foreach ($involucrados as $key => $involucrado) { ?>
					<tr>
						<td class="text-center" style="width: 40px">
							<?php echo ($key + 1) ?>
						</td>
						<td class="text-left" style="width: 180px">
							<?php echo $involucrado->dregistro ?>
						</td>
						<td class="text-left" style="width: 375px">
							<?php echo $involucrado->dnombreasistente ?> - <?php echo $involucrado->dcargoasistente ?>
						</td>
					</tr>
				<?php } ?>
			<?php } ?>
		</table>
	</div>
	<h3 class="text-left font-weigth-bold">VII. &nbsp;&nbsp;EVALUACIÓN</h3>
	<div style="padding-left: 8px">

	</div>

	<div style="page-break-after:always"></div>
	<h3 class="text-left font-weigth-bold">VIII. &nbsp;&nbsp;RESULTADOS</h3>
	<table class="table table-bordered text-sm">
		<tr>
			<td colspan="7" class="text-center bg-gray">
				<span class="text-sm">Listado de Verificación</span>
			</td>
		</tr>
		<tr>
			<td class="text-center bg-gray" style="width: 20px">
				<span class="text-sm">N°</span>
			</td>
			<td class="text-center bg-gray" style="width: 278px">
				<span class="text-sm">Aspecto Evaluado</span>
			</td>
			<td class="text-center bg-gray" style="width: 40px">
				<span class="text-sm">Puntaje <br> Maximo</span>
			</td>
			<td class="text-center bg-gray" style="width: 40px">
				<span class="text-sm">Puntaje <br> Obtenido</span>
			</td>
			<td class="text-center bg-gray" style="width: 55px">
				<span class="text-sm">% de <br> Conformidad</span>
			</td>
			<td class="text-center bg-gray" style="width: 30px">
				<span class="text-sm">% <br> Peso</span>
			</td>
			<td class="text-center bg-gray" style="width: 50px">
				<span class="text-sm">% de <br> Conf. Final</span>
			</td>
		</tr>
		<?php
		$totalRequisitos = 0;
		$totalMayorVal = 0;
		?>
		<?php if (!empty($cuadro2)) { ?>
			<?php foreach ($cuadro2 as $key => $value) { ?>
				<?php
				$mayorVal = round($value->mayor_val);
				$nvalorReguisito = round($value->nvalorrequisito);
				$nporcentaje = ($value->mayor_val > 0) ? round(($value->nvalorrequisito * 100) / $value->mayor_val, 2) : '0' . '%';
				if ($mayorVal <= 0 && $nvalorReguisito <= 0) {
					$mayorVal = 'N.A.';
					$nvalorReguisito = 'N.A.';
					$nporcentaje = 'N.A.';
				}
				?>
				<tr>
					<td class="text-center" style="width: 20px">
						<span class="text-sm"><?php echo $value->dnumerador ?></span>
					</td>
					<td class="text-left" style="width: 278px">
						<span class="text-sm"><?php echo $value->drequisito ?></span>
					</td>
					<td class="text-center" style="width: 40px">
							<span class="text-sm">
								<?php echo $mayorVal; ?>
							</span>
					</td>
					<td class="text-center" style="width: 40px">
							<span class="text-sm">
								<?php echo $nvalorReguisito; ?>
							</span>
					</td>
					<td class="text-center" style="width: 55px">
							<span class="text-sm">
								<?php echo $nporcentaje; ?>
							</span>
					</td>
					<td class="text-center bg-gray" style="width: 30px">
						<?php echo '' ?>
					</td>
					<td class="text-center bg-gray" style="width: 50px">
						<?php echo '' ?>
					</td>
				</tr>
				<?php
				$nro = count(explode('.', $value->dnumerador));
				if ($nro == 1) {
					$totalRequisitos += $value->nvalorrequisito;
					$totalMayorVal += $value->mayor_val;
				}
				?>
			<?php } ?>
		<?php } ?>
		<?php
		$puntajeParcial = ($totalMayorVal > 0) ? round(($totalRequisitos * 100) / $totalMayorVal, 2) : 0;
		$totalExcluyentes = count($excluyentes);
		?>
		<tr>
			<td class="text-center" colspan="2">
				<span class="text-sm">Puntaje TOTAL</span>
			</td>
			<td class="text-center">
				<span class="text-sm"><?php echo $totalMayorVal; ?></span>
			</td>
			<td class="text-center">
				<span class="text-sm"><?php echo $totalRequisitos; ?></span>
			</td>
			<td class="text-center" colspan="3">
				<span class="text-sm">
					<?php echo $puntajeParcial . '%'; ?>
				</span>
			</td>
		</tr>
		<tr>
			<td class="text-center" colspan="2">
				<span class="text-sm">
					Puntaje Final
					<?php if ($totalExcluyentes > 0) { ?>
						ajustado por excluyentes (<?php echo $totalExcluyentes; ?>)
					<?php } ?>
				</span>
			</td>
			<td class="text-center" colspan="5">
				<span class="text-sm">
					<?php echo $cuadro1->presultadochecklist . '%'; ?>
				</span>
			</td>
		</tr>
	</table>

	<table class="table table-bordered pb" style="padding-left: 100px; margin-top: 15px">
		<tr>
			<td colspan="3" class="text-center bg-gray">
				Clasificación del establecimiento
			</td>
		</tr>
		<tr>
			<td class="text-center bg-gray" style="width: 100px;">
				Calificación
			</td>
			<td class="text-center bg-gray" style="width: 180px;">
				Rango de Cumplimiento
			</td>
			<td class="text-center bg-gray" style="width: 150px;">
				Identifiación por Color
			</td>
		</tr>
		<?php if (!empty($criterioResultado)) { ?>
			<?php foreach ($criterioResultado as $key => $criterio) { ?>
				<tr>
					<td class="text-center" style="width: 100px">
						<?php echo $criterio->DDETALLECRITERIORESULTADO ?>
					</td>
					<td class="text-center" style="width: 180px">
						<?php echo $criterio->NVALORINICIAL ?>% a <?php echo $criterio->NVALORFINAL ?>%
					</td>
					<td class="text-center" style="width: 150px;
							<?php if ($cuadro1->presultadochecklist >= $criterio->NVALORINICIAL && $cuadro1->presultadochecklist <= $criterio->NVALORFINAL) { ?>
								background: rgb(<?php echo $criterio->NR ?>, <?php echo $criterio->NG ?>, <?php echo $criterio->NB ?>);
							<?php } ?>
							padding: 3px;">
						&nbsp;
					</td>
				</tr>
			<?php } ?>
		<?php } ?>
	</table>

	<div style="page-break-after:always"></div>

	<div style="padding-left: 8px">
		<table class="table">
			<tr>
				<td class="text-left text-justify" style="width: 100%; padding: 4px">
					Gráfico 1 <br><br>
					<table class="col-12">
						<tr>
							<td class="col-2"></td>
							<td class="col-8 border-gray text-center" style="padding: 20px">
								<p style="padding-top: 0; margin-top: 0; padding-bottom: 20px;">C
									Porcentaje Obtenido por Aspecto Evaluado
								</p>
								<img src="<?php echo base64ResourceConvert($imgGrafico2); ?>" alt="Grafico-1"
									 style="width: 100%; height: 220px">
							</td>
							<td class="col-2"></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>

	<div style="padding-left: 8px">
		<table class="table">
			<tr>
				<td class="text-left text-justify" style="width: 100%; padding: 4px">
					Gráfico 2 <br><br>
					<table class="col-12">
						<tr>
							<td class="col-2"></td>
							<td class="col-8 border-gray text-center" style="padding: 20px">
								<p style="padding-top: 0; margin-top: 0; padding-bottom: 20px;">CUMPLIMIENTO ENTRE
									INSPECCIONES</p>
								<img src="<?php echo base64ResourceConvert($imgGrafico1); ?>" alt="Grafico-1"
									 style="width: 100%; height: 220px">
							</td>
							<td class="col-2"></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>

	<div style="page-break-after:always"></div>

	<h3 class="pb font-weigth-bold" style="margin-top: 0; padding-top: 0">IX. CONCLUSIONES</h3>
	<div style="padding-left: 8px" >
		<?php $punotIni = 9.1; ?>
		<table class="table" >
			<tr>
				<td class="text-left text-justify" style="width: 100%; padding: 4px">
					<b class="uppercase" ><?php echo $punotIni ; ?> Conclusión General</b>
				</td>
			</tr>
			<tr>
				<td class="text-left text-justify" style="width: 100%; padding: 4px">
					<?php echo str_replace("\r\n", '<br>', $conclucionesGenerales); ?>
				</td>
			</tr>
		</table>
		<?php if (!empty($conclucionesEspecificas)) { ?>
			<?php foreach ($conclucionesEspecificas as $key => $value) { ?>
				<table class="table" style="margin-top: 10px" >
					<tr>
						<td class="text-left text-justify" style="width: 100%; padding: 4px">
							<b class="uppercase" ><?php echo $punotIni . ' ' . $value->dregistro ?></b>
						</td>
					</tr>
					<tr>
						<td class="text-left text-justify" style="width: 100%; padding: 4px">
							<?php echo str_replace("\r\n", '<br>', $value->dinfoadicional); ?>
						</td>
					</tr>
				</table>
				<?php $punotIni = round($punotIni + 0.1, 2); ?>
			<?php } ?>
		<?php } ?>
	</div>
	<div class="col-12">
		<h3 style="padding-top: 0; margin-top: 0">MANEJO DE PELIGROS ESTANDAR</h3>
		<div class="text-justify col-12">
			Con respecto a los peligros identificados por <?php echo $caratula->nomcliente ?> por tipo de producción y manejo de los mismos por parte del proveedor
			<br><br>
		</div>
		<div style="padding-left: 8px" >
			<table class="table table-bordered" style="">
				<tr>
					<td class="text-center" style="width: 120px; font-weight: bold">
						Producto
					</td>
					<td class="text-center" style="width: 20px; font-weight: bold">

					</td>
					<td class="text-center" style="width: 200px; font-weight: bold">
						Peligros Identificados por <?php echo $caratula->nomcliente ?>
					</td>
					<td class="text-center" style="width: 225px; font-weight: bold">
						Evaluación FSC
					</td>
				</tr>
				<?php if (!empty($peligros)) { ?>
					<?php foreach ($peligros as $key => $peligro) { ?>
						<tr>
							<td class="text-left" style="width: 120px;">
								<?php echo $peligro->dproductopeligro ?>
							</td>
							<td class="text-center" style="width: 20px;">
								<?php echo $peligro->ctipopeligro ?>
							</td>
							<td class="text-left" style="width: 205px;">
								<?php echo $peligro->dpeligro ?>
							</td>
							<td class="text-left" style="width: 235px;">
								<?php echo $peligro->ddetallepeligro ?>
							</td>
						</tr>
					<?php } ?>
				<?php } ?>
			</table>
		</div>
	</div>
	<div style="page-break-after:always"></div>

	<h3 class="pb font-weigth-bold" style="margin-top: 0; padding-top: 0">X. ACCIONES A SEGUIR</h3>
	<div style="width: 100%" >
		<table style="width: 100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class="col-12 text-left" style="border: none">
					El personal del área de calidad de la empresa debe elaborar un plan de acción en el cual se detallen
					las acciones correctivas para levantar las no conformidades y las observaciones identificadas
					en la presente inspección. <br><br>
					El Plan de Acción deberá enviarse al email: homologación-proveedores@fscertificaciones.com,
					en un plazo no mayor de 10 días después de la fecha de emisión
					del informe técnico de inspección. <br><br>
					Importante: Los resultados del informe técnico revelan el estado o nivel de implementación del
					sistema de inocuidad encontrado en el momento de la inspección, cualquier modificación del
					estado de la inspección, es responsabilidad del establecimiento y/o proveedor. <br><br>
					<b>Este documento, al ser emitido sin el símbolo de acreditación,
						no se encuentra dentro del marco de la acreditación otorgada por INACAL-DA.</b>
				</td>
			</tr>
		</table>
		<br><br><br><br><br><br><br><br><br>
		<div class="col-5" >
			<table style="width: 100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="col-12 text-center" style="border-top: 1px solid #000;">
						ING. <?php echo (!empty($inspector)) ? $inspector->dnombre . ' ' . $inspector->dapepat . ' ' . $inspector->dapemat : ''; ?>
						<br>
						INSPECTOR
					</td>
				</tr>
			</table>
		</div>
	</div>
</page>
