<?php
    $idusu = $this -> session -> userdata('s_idusuario');
    $cusuario = $this -> session -> userdata('s_cusuario');
?>

<style>
    tab {
        display: inline-block; 
        margin-left: 100px; 
    }
    tr.subgroup,
    tr.subgroup:hover {
        background-color: #F2F2F2 !important;
        /* color: blue; */
        font-weight: bold;
    }
    .group{
            background-color:#CDD1DB !important;
            font-size:15px;
            color:#000000!important;
            opacity:0.7;
    }
    .subgroup{
        cursor: pointer;
    }
    td.subgroup {
        border-top: double !important;
    }
    td.expand {
        border-bottom: double !important;
    }
    .modal-lg{
        max-width: 1000px !important;
    }

    .dataTables_scrollBody thead{
        visibility:hidden;
    }
    .hidden {
        display: none;
    }
</style>

<!-- content-header -->
<div class="content-header">   
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">INSPECCIÓN - CTRL. PROV.</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?php echo public_base_url(); ?>cprincipal/principal">Home</a></li>
          <li class="breadcrumb-item active">Ctrl. Prov.</li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content" style="background-color: #E0F4ED;">
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-12">
                <div class="card card-success card-outline card-tabs">
                    <div class="card-header p-0 pt-1 border-bottom-0">            
                        <ul class="nav nav-tabs" id="tabctrlprov" style="background-color: #28a745;"role="tablist">                    
                            <li class="nav-item">
                                <a class="nav-link active" style="color: #000000;" id="tabctrlprov-list-tab" data-toggle="pill" href="#tabctrlprov-list" role="tab" aria-controls="tabctrlprov-list" aria-selected="true">LISTADO</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" style="color: #000000;" id="tabctrlprov-det-tab" data-toggle="pill" href="#tabctrlprov-det" role="tab" aria-controls="tabctrlprov-det" aria-selected="false">CHECKLIST</a>
                            </li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content" id="tabctrlprov-tabContent">
                            <div class="tab-pane fade show active" id="tabctrlprov-list" role="tabpanel" aria-labelledby="tabctrlprov-list-tab">
                                <div class="card card-success">
                                    <div class="card-header">
                                        <h3 class="card-title">BUSQUEDA</h3>
                                        <div class="card-tools">
                                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                        </div>
                                    </div>                        
                                    <div class="card-body">
                                        <input type="hidden" name="cusuario" id="cusuario" value="<?php echo $cusuario ?>">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Clientes</label>
                                                    <select class="form-control select2bs4" id="cboclieserv" name="cboclieserv" style="width: 100%;">
                                                        <option value="" selected="selected">Cargando...</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">    
                                                <div class="checkbox"><label>
                                                    <input type="checkbox" id="chkFreg" /> <b>Fecha Registro :: Del</b>
                                                </label></div>                        
                                                <div class="input-group date" id="txtFDesde" data-target-input="nearest" >
                                                    <input type="text" id="txtFIni" name="txtFIni" class="form-control datetimepicker-input" data-target="#txtFDesde" disabled/>
                                                    <div class="input-group-append" data-target="#txtFDesde" data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">      
                                                <label>Hasta</label>                      
                                                <div class="input-group date" id="txtFHasta" data-target-input="nearest">
                                                    <input type="text" id="txtFFin" name="txtFFin" class="form-control datetimepicker-input" data-target="#txtFHasta" disabled/>
                                                    <div class="input-group-append" data-target="#txtFHasta" data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group"> 
                                                    <label>Proveedor / Maquilador</label>
                                                    <!--<div class="checkbox"><label>
                                                        <div class="d-inline">
                                                            <input type="radio" id="rdBProv" value="%" name="rBuscar"/> <b>Proveedor</b>
                                                        </div>
                                                        <div class="d-inline">
                                                            <input type="radio" id="rdBMaqu" value="%" name="rBuscar"/> <b>Maquilador</b>
                                                        </div>
                                                    </label></div>-->
                                                    <input type="text" class="form-control" id="txtprovmaq" name="txtprovmaq">
                                                </div>
                                            </div>
                                            <!--<div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Proveedor</label>
                                                    <select class="form-control select2bs4" id="cboprovxclie" name="cboprovxclie" style="width: 100%;">
                                                        <option value="" selected="selected">Cargando...</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Maquilador</label>
                                                    <select class="form-control select2bs4" id="cbomaqxprov" name="cbomaqxprov" style="width: 100%;">
                                                        <option value="" selected="selected">Cargando...</option>
                                                    </select>
                                                </div>
                                            </div>-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Estado</label>
                                                    <select class="form-control select2bs4" id="cboestado" name="cboestado" multiple="multiple" data-placeholder="Seleccionar" style="width: 100%;">
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
<!--                                        <div class="row">-->
<!--                                            <div class="col-md-4">-->
<!--                                                <div class="form-group">-->
<!--                                                    <label>Inspector</label>-->
<!--                                                    <select class="form-control select2bs4" id="cboinspector" name="cboinspector" style="width: 100%;">-->
<!--                                                        <option value="" selected="selected">Cargando...</option>-->
<!--                                                    </select>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </div>-->
                                    </div>
                                    <div class="card-footer justify-content-between" style="background-color: #E0F4ED;"> 
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="text-right">
                                                    <button type="submit" class="btn btn-primary" id="btnBuscar"><i class="fas fa-search"></i> Buscar</button>    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> 

                                <div class="row">
                                    <div class="col-12">
                                        <div class="card card-outline card-success">
                                            <div class="card-header">
                                                <h3 class="card-title">Listado de Inspecciones - <label id="lblCliente"></label></h3>
                                            </div>                                       
                                            <div class="card-body">
                                                <table id="tblListinspctrlprov" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                    <tr>
                                                        <th colspan="12">[Codigo] :: Proveedor - (Maquilador) - Establecimiento</th>
                                                    </tr>
                                                    <tr>
                                                        <th>desc_gral</th>
                                                        <th>Area Cliente</th>
                                                        <th>Linea Proceso</th>
                                                        <th>Periodo</th>
                                                        <th>Estado</th>
                                                        <th>Fecha</th>
                                                        <th>Informe</th>
                                                        <th>Resultado</th>
                                                        <th>Finalizado</th>
                                                        <th>Peligro</th>
                                                        <th>Quejas</th>
                                                        <th>AA.CC</th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>                
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.Main content -->

<!-- Aación correctiva -->
<div class="modal fade" id="modalAccionCorrectiva" data-backdrop="static" data-keyboard="false" tabindex="-1"
	 aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="modal-header bg-success">
				<h5 class="modal-title fs w-100 font-weight-bold">Acción Correctiva</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body"
				 style="background-color:#ffffff; border-top: 1px solid #00a65a; border-bottom: 1px solid #00a65a;">
				<div class="row">
					<div class="col-12" style="overflow-x: scroll">
						<table class="table table-striped table-bordered" id="tblAcciónCorrectiva"
							   style="width: 100%">
							<thead>
							<tr>
								<th>ID</th>
								<th>Requisito</th>
								<th>Excluyente</th>
								<th>Tipo Hallazgo</th>
								<th>Hallazgo</th>
								<th>Acción Correctiva</th>
								<th>Fecha Corrección</th>
								<th>Responsable</th>
								<th>Acepta Acc. Correctiva</th>
								<th>Comentario del Inspector</th>
							</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="modal-footer" id="download-excel" style="display: none">
				<a class="btn btn-sm btn-success" href="#" target="_blank" id="btnDownloadAccionCorrectiva">
					<i class="fa fa-file-excel"></i> Descargar Acción Correctiva
				</a>
			</div>
		</div>
	</div>
</div>


<!-- Script Generales -->
<script type="text/javascript">
    var baseurl = "<?php echo base_url();?>"; 
</script>
