<style>
	.select2-container .select2-selection--single {
		height: 36px;
	}
</style>

<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">
					INSPECCIÓN - CTRL. PROV.
				</h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="<?php echo public_base_url(); ?>cprincipal/principal">Home</a>
					</li>
					<li class="breadcrumb-item active">Ctrl. Prov.</li>
				</ol>
			</div>
		</div>
	</div>
</div>

<input type="hidden" class="d-none" id="cauditoria" name="cauditoria"
	   value="<?php echo $datos_ventana['cauditoria']; ?>">
<input type="hidden" class="d-none" id="fservicio" name="fservicio" value="<?php echo $datos_ventana['fservicio']; ?>">
<input type="hidden" class="d-none" id="inspeccion_abierto" name="inspeccion_abierto"
	   value="<?php echo $datos_ventana['inspeccionAbierto']; ?>">

<section class="content">
	<div class="container-fluid">
		<?php if ($datos_ventana['inspeccionAbierto']) { ?>
			<button type="button" role="button" class="btn btn-primary mb-2" id="btnCierreInspeccion" >
				<i class="fa fa-save" ></i> Cierre de la inspección
			</button>
		<?php } ?>
		<div class="card card-primary">
			<div class="card-body">
				<input type="hidden" name="mtxtidinsp" id="mtxtidinsp"
					   value="<?php echo $datos_ventana['cauditoria']; ?>">
				<input type="hidden" name="mhdnzctipoestado" id="mhdnzctipoestado">
				<input type="hidden" name="mhdnccliente" id="mhdnccliente" value="<?php echo $datos_ventana['inspeccion']->ccliente ?>" >
				<input type="hidden" name="mhdncproveedor" id="mhdncproveedor" value="<?php echo $datos_ventana['inspeccion']->cproveedorcliente ?>" >
				<input type="hidden" name="mhdncmaquilador" id="mhdncmaquilador" value="<?php echo $datos_ventana['inspeccion']->cmaquiladorcliente ?>" >
				<input type="hidden" name="mhdnAccioninsp" id="mhdnAccioninsp" value="A">
				<div class="form-group">
					<label>PROVEEDOR</label>
					<input type="text" class="form-control mb-3 text-left" disabled aria-label=""
						   id="mtxtinspdatos" name="mtxtinspdatos"
						   value="<?php echo date('d/m/Y', strtotime($datos_ventana['fservicio'])) . ' - ' . $datos_ventana['inspeccion']->desc_gral ?>">
					<input type="text" class="form-control mb-3 text-left" disabled aria-label=""
						   name="mtxtinsparea" id="mtxtinsparea"
						   value="AREA: <?php echo $datos_ventana['inspeccion']->areacli ?>">
					<div class="input-group" >
						<input type="text"
							   class="form-control <?php echo ($datos_ventana['speligro']) ? 'text-danger' : '' ?> text-left"
							   disabled aria-label=""
							   name="mtxtinsplinea" id="mtxtinsplinea"
							   value="LINEA: <?php echo $datos_ventana['inspeccion']->lineaproc ?>">
						<?php if ($datos_ventana['speligro']) { ?>
							<div class="input-group-append" >
								<button type="button" role="button" class="btn btn-danger btn-sm" id="alertaPeligro" >
									<i class="fa fa-link" ></i> Revisar Peligro
								</button>
							</div>
						<?php } ?>
					</div>
				</div>
				<div class="d-flex flex-row justify-content-between" >
					<div class="col-xl-4" >
						<div class="form-group text-left" >
							<label for="">
								&nbsp;
							</label>
						</div>
					</div>
					<div class="col-xl-3 col-lg-4 col-md-4 col-12" >
						<div class="form-group text-right" >
							<label for="" class="text-uppercase" >
								Inspección anterior
							</label>
							<div class="" >
								<?php if (!empty($datos_ventana['inspeccionAnt']) && !empty($datos_ventana['inspeccionAnt']->DUBICACIONFILESERVERPDF)) { ?>
									<?php
									$nombreInspAnt = '';
									$ultimoSeparador = strrpos($datos_ventana['inspeccionAnt']->DUBICACIONFILESERVERPDF, '/');
									if ($ultimoSeparador !== false) {
										$nombreInspAnt = substr($datos_ventana['inspeccionAnt']->DUBICACIONFILESERVERPDF, $ultimoSeparador + 1, strlen($datos_ventana['inspeccionAnt']->DUBICACIONFILESERVERPDF));
									}
									?>
									<a class="btn btn-secondary" download="<?php echo $nombreInspAnt; ?>" href="<?php echo base_url('FTPfileserver/Archivos/' . $datos_ventana['inspeccionAnt']->DUBICACIONFILESERVERPDF) ?>" >
										<i class="fa fa-download" ></i> Descargar Informe
									</a>
								<?php } ?>
								<?php if (!empty($datos_ventana['inspeccionAnt']) && !empty($datos_ventana['inspeccionAnt']->DUBICACIONFILESERVERAC)) { ?>
									<?php
									$nombreInspAnt = '';
									$ultimoSeparador = strrpos($datos_ventana['inspeccionAnt']->DUBICACIONFILESERVERAC, '/');
									if ($ultimoSeparador !== false) {
										$nombreInspAnt = substr($datos_ventana['inspeccionAnt']->DUBICACIONFILESERVERAC, $ultimoSeparador + 1, strlen($datos_ventana['inspeccionAnt']->DUBICACIONFILESERVERAC));
									}
									?>
									<a class="btn btn-secondary" download="<?php echo $nombreInspAnt; ?>" href="<?php echo base_url('FTPfileserver/Archivos/' . $datos_ventana['inspeccionAnt']->DUBICACIONFILESERVERAC) ?>" >
										<i class="fa fa-file-excel" ></i> Descargar AA.CC
									</a>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
				<div class="card card-primary card-outline card-tabs">
					<div class="card-header p-0 pt-1 border-bottom-0">
						<ul class="nav nav-tabs" id="tabctrlprov" style="background-color: #28a745;" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" style="color: #000000;" id="tabctrlprov-det-tab"
								   data-toggle="pill"
								   href="#tabctrlprov-checklist" role="tab"
								   aria-controls="tabctrlprov-checklist" aria-selected="false">Resultado</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" style="color: #000000;" id="tabctrlprov-peligro-tab"
								   data-toggle="pill"
								   href="#tabctrlprov-peligro" role="tab"
								   aria-controls="tabctrlprov-peligro" aria-selected="false">Peligros</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" style="color: #000000;" id="tabctrlprov-involucrados-tab"
								   data-toggle="pill"
								   href="#tabctrlprov-involucrados" role="tab"
								   aria-controls="tabctrlprov-involucrados" aria-selected="false">Involucrados</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" style="color: #000000;" id="tabctrlprov-det-tab"
								   data-toggle="pill"
								   href="#tabctrlprov-resumen" role="tab"
								   aria-controls="tabctrlprov-resumen" aria-selected="false">Resumen</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" style="color: #000000;" id="tabctrlprov-acc-tab"
								   data-toggle="pill"
								   href="#tabctrlprov-acc" role="tab"
								   aria-controls="tabctrlprov-acc" aria-selected="false">Acciones Correctivas</a>
							</li>
						</ul>
					</div>
					<div class="card-body">
						<div class="tab-content" id="tabctrlprov-tabContent">
							<div class="tab-pane fade show active" id="tabctrlprov-checklist" role="tabpanel"
								 aria-labelledby="tabctrlprov-det-tab">
								<?php $this->load->view('oi/ctrlprov/inspctrolprov/vchecklist', $datos_ventana); ?>
							</div>
							<div class="tab-pane fade" id="tabctrlprov-resumen" role="tabpanel"
								 aria-labelledby="tabctrlprov-list-tab">
								<?php $this->load->view('oi/ctrlprov/inspctrolprov/vresumen', $datos_ventana); ?>
							</div>
							<div class="tab-pane fade" id="tabctrlprov-acc" role="tabpanel"
								 aria-labelledby="tabctrlprov-acc-tab">
								<?php $this->load->view('oi/ctrlprov/inspctrolprov/vaccion_correctiva', $datos_ventana); ?>
							</div>
							<div class="tab-pane fade" id="tabctrlprov-peligro" role="tabpanel"
								 aria-labelledby="tabctrlprov-peligro-tab">
								<?php $this->load->view('oi/ctrlprov/inspctrolprov/vpeligros', $datos_ventana); ?>
							</div>
							<div class="tab-pane fade" id="tabctrlprov-involucrados" role="tabpanel"
								 aria-labelledby="tabctrlprov-involucrados-tab">
								<?php $this->load->view('oi/ctrlprov/inspctrolprov/vinvolucrados', $datos_ventana); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
