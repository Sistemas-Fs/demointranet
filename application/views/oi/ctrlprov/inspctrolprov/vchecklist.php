<div class="">
	<div class="row">
		<div class="col-xl-7 col-lg-7 col-md-7 col-12">
			<div class="form-group row">
				<label for="sistema_valor" class="col-xl-2 col-lg-2 col-md-2 col-12">
					Sitema
				</label>
				<div class="col-xl-10 col-lg-10 col-md-10 col-12">
					<input type="text" class="form-control form-control-sm text-danger"
						   id="sistema_valor" readonly
						   value="<?php echo $inspeccion->SISTEMA ?>">
				</div>
			</div>
		</div>
		<div class="col-xl-5 col-lg-5 col-md-5 col-12">
			<div class="form-group row">
				<label for="rubro_valor" class="col-xl-2 col-lg-2 col-md-2 col-12">
					Rubro
				</label>
				<div class="col-xl-10 col-lg-10 col-md-10 col-12">
					<input type="text" class="form-control form-control-sm text-danger"
						   id="rubro_valor" readonly
						   value="<?php echo $inspeccion->RUBRO ?>">
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xl-7 col-lg-7 col-md-7 col-12">
			<div class="form-group row">
				<label for="rubro_valor" class="col-xl-2 col-lg-2 col-md-2 col-12">
					CheckList
				</label>
				<div class="col-xl-10 col-lg-10 col-md-10 col-12">
					<input type="text" class="form-control form-control-sm text-danger"
						   id="rubro_valor" readonly
						   value="<?php echo $inspeccion->CHECKLIST ?>">
				</div>
			</div>
		</div>
	</div>
</div>
<div class="mt-3" >
	<a target="_blank" class="btn btn-light active" href="<?php echo base_url('oi/ctrlprov/inspctrolprov/cchecklist/descargar/' . $cauditoria . '/' . $fservicio) ?>" >
		<i class="fa fa-download" ></i> Descargar Checklist
	</a>
	<table id="tblinspeccionprov" class="table table-striped table-bordered"
		   style="width:100%">
		<thead>
		<tr>
			<th>NRO</th>
			<th>REQUISITO</th>
			<th>VALOR MAX</th>
			<th>VALOR OBTENIDO</th>
			<th>CRITERIO DE HALLAZGO</th>
			<th>HALLAZGO</th>
			<th></th>
		</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
<div class="modal fade" id="ModalItemList" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header bg-success">
				<h5 class="modal-title text-uppercase">Requisitos Checklist</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form method="post" id="frmChecklist">
					<input type="hidden" name="int_requisito_checklist" id="int_requisito_checklist">
					<input type="hidden" name="int_codigo_checklist" id="int_codigo_checklist">
					<input type="hidden" name="int_auditoria_inspeccion" id="int_auditoria_inspeccion">
					<input type="hidden" name="criterio_hallazgo_valor" id="criterio_hallazgo_valor" value="0">

					<div class="row">
						<div class="col-xl-2 col-lg-3 col-md-4 col-12">
							<span class="font-weight-bold h6" id="checklist_nro"></span>
						</div>
						<div class="col-xl-10 col-lg-9 col-md-8 col-12">
							<span class="font-weight-bold h6" id="checklist_drequisito"></span>
						</div>
					</div>
					<div class="h6 border-bottom my-3 font-weight-bold text-primary h5 text-uppercase my-3">
						Registro de Valores
					</div>
					<div class="row">
						<div class="col-xl-2 col-lg-3 col-md-4 col-12">
							<div class="form-group">
								<label for="cmbPuntuacionChecklist">Puntuacion</label>
								<select class="form-control" id="cmbPuntuacionChecklist" name="cmbPuntuacionChecklist">
									<option value="" selected="selected">Cargando...</option>
								</select>
							</div>
						</div>
						<div class="col-xl-10 col-lg-9 col-md-8 col-12">
							<div class="form-group">
								<label for="cmbCriterioHallazgo">Crit. Hallazgo</label>
								<select class="form-control" id="cmbCriterioHallazgo" name="cmbCriterioHallazgo">
									<option value="" selected="selected">Cargando...</option>
								</select>
							</div>
						</div>
					</div>

					<div class="form-group col-md-12">
						<label for="txtHallazgo">Hallazgo</label>
						<textarea class="form-control" id="txtHallazgo" rows="8" name="txtHallazgo"></textarea>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<div class="d-flex justify-content-between w-100">
					<div>
						<button type="button" class="btn btn-success" id="btnAnteriorChecklist" >
							<i class="fa fa-caret-left"></i>
							<span>anterior</span>
						</button>
					</div>
					<div>
						<button type="button" class="btn btn-secondary" data-dismiss="modal" id="btnCerrarChecklist" >
							Guardar y Cerrar
						</button>
						<button type="button" class="btn btn-primary" id="btnGuardarChecklist">
							<span><i class="fa fa-save"></i> Guardar y continuar</span>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
