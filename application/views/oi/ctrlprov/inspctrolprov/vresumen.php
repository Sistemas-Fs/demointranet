<div class="w-100 d-flex justify-content-between mb-4">
	<div class="">
		<?php if ($inspeccionAbierto) { ?>
			<button type="button" role="button" class="btn btn-info mr-2 btnGuardarResumen">
				<i class="fa fa-save"></i> Guardar datos
			</button>
		<?php } ?>
	</div>
	<div class="">

	</div>
</div>

<form action="#" method="POST" accept-charset="UTF-8" id="frmResumen">
	<div class="row">
		<div class="col-xl-12 col-lg-12 col-12">
			<div class="row">
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12" style="display: none">
					<div class="form-group row">
						<label for="resumen_real_hora_efectiva" class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							N° Real Hora Efectiva
						</label>
						<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
							<input type="text" class="form-control text-center"
								   id="resumen_real_hora_efectiva" name="resumen_real_hora_efectiva"
								   value="<?php echo (isset($resumen->nrealhoraefectiva)) ? $resumen->nrealhoraefectiva : ''; ?>">
						</div>
					</div>
				</div>
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
					<div class="form-group row">
						<label for="resumen_resultado_checklist" class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							Resultado CheckList
						</label>
						<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
							<input type="text" class="form-control text-center" readonly
								   id="resumen_resultado_checklist" name="resumen_resultado_checklist"
								   value="<?php echo (isset($resumen->presultadochecklist)) ? $resumen->presultadochecklist : ''; ?>">
						</div>
					</div>
				</div>
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
					<div class="form-group row">
						<label for="resumen_resultado" class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
							Resultado
						</label>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<input type="text" class="form-control text-center" readonly
								   id="resumen_resultado" name="resumen_resultado"
								   value="<?php echo (isset($resumen->descripcion_result)) ? $resumen->descripcion_result : ''; ?>">
						</div>
					</div>
				</div>
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
					<div class="form-group row">
						<label for="resumen_modelo_informe" class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
							Modelo Informe
						</label>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<select name="resumen_modelo_informe" id="resumen_modelo_informe" disabled
									class="custom-select  w-100" style="width: 100% !important;">
								<?php if (!empty($informes)) { ?>
									<?php foreach ($informes as $key => $informe) { ?>
										<option value="<?php echo $informe->CMODELOINFORME ?>"
												<?php echo ($informe->CMODELOINFORME == $resumen->cmodeloinforme) ? 'selected' : ''; ?> >
											<?php echo $informe->DMODELO; ?>
										</option>
									<?php } ?>
								<?php } ?>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12" style="display: none">
					<div class="form-group row">
						<label for="resumen_real_hora_no_efectiva" class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							N° Real Hora No Efectiva
						</label>
						<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
							<input type="text" class="form-control text-center"
								   id="resumen_real_hora_no_efectiva" name="resumen_real_hora_no_efectiva"
								   value="<?php echo (isset($resumen->nrealhoranoefectiva)) ? $resumen->nrealhoranoefectiva : ''; ?>">
						</div>
					</div>
				</div>
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12"></div>
			</div>
			<div class="row">
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
					<!--				<div class="form-group row">-->
					<!--					<label for="resumen_gasto_general" class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">-->
					<!--						Gasto General-->
					<!--					</label>-->
					<!--					<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">-->
					<!--						<input type="text" class="form-control text-center"-->
					<!--							   id="resumen_gasto_general" name="resumen_gasto_general"-->
					<!--							   value="-->
					<?php //echo (isset($resumen->ngastogeneral)) ? $resumen->ngastogeneral : ''; ?><!--">-->
					<!--					</div>-->
					<!--				</div>-->
				</div>
<!--				<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">-->
<!--					<div class="form-group row">-->
<!--						<label for="resumen_rubro" class="col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12">-->
<!--							Rubro-->
<!--						</label>-->
<!--						<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">-->
<!--							<input type="text" class="form-control" readonly-->
<!--								   id="resumen_rubro" name="resumen_rubro"-->
<!--								   value="--><?php //echo (isset($resumen->rubro)) ? $resumen->rubro : ''; ?><!--">-->
<!--						</div>-->
<!--					</div>-->
<!--				</div>-->
			</div>
			<div class="row">
				<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12" style="display: none">
					<div class="form-group row">
						<label for="resumen_informe" class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
							Archivo adjunto
						</label>
						<div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
							<div class="custom-file">
								<input type="file" class="custom-file-input" id="resumen_informe" name="resumen_informe">
								<label class="custom-file-label" for="resumen_informe" data-browse="Archivo...">
									Elegir archivo
								</label>
							</div>
							<?php if (!empty($resumen->DUBICACIONFILESERVER)) { ?>
							<?php $posArchivo = (strrpos($resumen->DUBICACIONFILESERVER, '/') == false) ? strrpos($resumen->DUBICACIONFILESERVER, '\\') : strrpos($resumen->DUBICACIONFILESERVER, '/'); ?>
							<?php $nombreArchivo = substr($resumen->DUBICACIONFILESERVER, $posArchivo + 1, strlen($resumen->DUBICACIONFILESERVER)); ?>
							<a class="btn btn-link"
							   href="<?php echo base_url(RUTA_ARCHIVOS . $resumen->DUBICACIONFILESERVER) ?>"
							   download="<?php echo $nombreArchivo ?>">
								<i class="fa fa-download"></i> Descargar informe adjunto
							</a>
							<?php } ?>
						</div>
					</div>
				</div>
				<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12" style="display: none">
					<div class="form-group row">
						<label for="resumen_certificaciones" class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
							Certificaciones
						</label>
						<div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
							<select name="resumen_certificaciones" id="resumen_certificaciones"
									class="custom-select  w-100" style="width: 100% !important;">
								<?php if (!empty($certificaciones)) { ?>
									<?php foreach ($certificaciones as $key => $certificacion) { ?>
										<option value="<?php echo $certificacion->CCERTIFICACION ?>"
												<?php echo ($certificacion->CCERTIFICACION == $resumen->ccertificacion) ? 'selected' : ''; ?> >
											<?php echo $certificacion->DCERTIFICACION; ?>
											(<?php echo $certificacion->CERTIFICADORA ?>)
										</option>
									<?php } ?>
								<?php } ?>
							</select>
						</div>
					</div>
				</div>
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12" style="display: none">
					<div class="form-group row">
						<label for="resumen_resultado_estado" class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
							Resultado
						</label>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<select name="resumen_resultado_estado" id="resumen_resultado_estado"
									class="custom-select  w-100" style="width: 100% !important;">
								<option value="ST" <?php echo ($resumen->scertificacion == "ST") ? 'selected' : ''; ?> >
									TIENE
								</option>
								<option value="NT" <?php echo ($resumen->scertificacion == "NT") ? 'selected' : ''; ?> >
									NO TIENE
								</option>
								<option value="ET" <?php echo ($resumen->scertificacion == "ET") ? 'selected' : ''; ?> >
									EN TRAMITE
								</option>
								<option value="NA" <?php echo ($resumen->scertificacion == "NA") ? 'selected' : ''; ?> >
									NO APLICA
								</option>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12" style="display: none">
					<div class="form-group row">
						<label for="resumen_detalle" class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12">
							Detalles / Otro
						</label>
						<div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
					<textarea name="resumen_detalle" id="resumen_detalle"
							  class="form-control "
							  rows="3"><?php echo (isset($resumen->dpermisoautoridadsanitaria)) ? $resumen->dpermisoautoridadsanitaria : ''; ?></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="form-group row">
						<label for="resumen_alcance_final" class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12">
							Alcance Final
						</label>
						<div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
					<textarea name="resumen_alcance_final" id="resumen_alcance_final"
							  class="form-control "
							  rows="3"><?php echo (isset($resumen->dalcanceinforme)) ? $resumen->dalcanceinforme : ''; ?></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="form-group row">
						<label for="resumen_producto" class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12">
							Producto
						</label>
						<div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
					<textarea name="resumen_producto" id="resumen_producto"
							  class="form-control "
							  rows="3"><?php echo (isset($resumen->dmarcainforme)) ? $resumen->dmarcainforme : ''; ?></textarea>
						</div>
					</div>
				</div>
				<div class="col-12">
					<div class="form-group row">
						<label for="resumen_marca" class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12">
							Marcas
						</label>
						<div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
					<textarea name="resumen_marca" id="resumen_marca"
							  class="form-control "
							  rows="3"><?php echo (isset($resumen->dmarcainforme2)) ? $resumen->dmarcainforme2 : ''; ?></textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xl-3 col-lg-3 col-12" style="display: none">
			<div class="form-group">
				<label for="resumen_otro_doc_municipio">
					Municipio
				</label>
				<select name="resumen_otro_doc_municipio" id="resumen_otro_doc_municipio" class="custom-select select2"
						style="width: 100% !important;">
					<option value=""></option>
					<?php if (!empty($ubigeos)) { ?>
						<?php foreach ($ubigeos as $key => $value) { ?>
							<option value="<?php echo $value->CUBIGEO ?>"
									<?php echo ($value->CUBIGEO == $resumen->CUBIGEOMUNICIPALIDAD) ? 'selected' : ''; ?> >
								<?php echo $value->DDEPARTAMENTO ?> - <?php echo $value->DPROVINCIA ?>
								- <?php echo $value->DDISTRITO ?>
							</option>
						<?php } ?>
					<?php } ?>
				</select>
			</div>
			<div class="form-group">
				<label for="resumen_otro_doc_licencia">
					Licencia
				</label>
				<input type="text" class="form-control "
					   id="resumen_otro_doc_licencia" name="resumen_otro_doc_licencia"
					   value="<?php echo $resumen->DLICENCIAFUNCIONAMIENTO ?>"/>
			</div>
			<div class="form-group">
				<label for="resumen_otro_doc_estado">
					Estado
				</label>
				<select name="resumen_otro_doc_estado" id="resumen_otro_doc_estado" class="custom-select "
						style="width: 100% !important;">
					<option value="T" <?php echo ($resumen->SLICENCIAFUNCIONAMIENTO == 'T') ? 'selected' : '' ?> >En
						tramite
					</option>
					<option value="S" <?php echo ($resumen->SLICENCIAFUNCIONAMIENTO == 'S') ? 'selected' : '' ?> >Si
						tiene
					</option>
					<option value="N" <?php echo ($resumen->SLICENCIAFUNCIONAMIENTO == 'N') ? 'selected' : '' ?> >No
						tiene
					</option>
				</select>
			</div>
			<div class="form-group">
				<label for="resumen_otro_doc_observacion">
					Observaciones
				</label>
				<textarea name="resumen_otro_doc_observacion" id="resumen_otro_doc_observacion"
						  class="form-control "
						  rows="10"><?php echo $resumen->DADICIONALLICENCIA ?></textarea>
			</div>
		</div>
	</div>
</form>

<div class="form-group row">
	<label for="resumen_producto" class="col-xl-1 col-lg-1 col-md-2 col-sm-12 col-12">
		Contacto responsable
	</label>
	<div class="col-xl-11 col-lg-11 col-md-10 col-sm-12 col-12">
		<div class="table-responsive">
			<table class="table table-bordered" id="tblResumenContactos">
				<thead>
				<tr class="table-success">
					<th>Empresa</th>
					<th>Ape. Paterno</th>
					<th>Ape. Materno</th>
					<th>Nombres</th>
					<th>Cargo</th>
					<th>Email</th>
					<th>Telefonos</th>
					<th style="width: 115px"></th>
				</tr>
				</thead>
				<tbody>
				</tbody>
				<tfoot>
				<tr>
					<th class="text-left" colspan="10" >
						<?php if ($inspeccionAbierto) { ?>
							<div class="btn-group" >
								<button type="button" role="button" class="btn btn-link" id="btnAgregarContacto" >
									<i class="fa fa-plus" ></i> Agregar nuevo contacto
								</button>
								<button type="button" role="button" class="btn btn-link ml-2" id="btnBuscarContacto" >
									<i class="fa fa-search" ></i> Buscar contacto
								</button>
							</div>
						<?php } ?>
					</th>
				</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-xl-4 col-lg-4 col-12">
		<div class="table-responsive">
			<table class="table table-bordered table-sm table-hover" id="tblResumenTema">
				<thead class="table-success">
				<tr>
					<th class="text-center">Tema</th>
				</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
	<div class="col-xl-8 col-lg-8 col-12">
		<div class="row justify-content-between">
			<div class="col-xl-4 col-lg-5 col-md-6 col-12">
				<div class="form-group">
					<label for="">
						N° de Informe Generado
					</label>
					<div class="input-group">
						<?php if ($inspeccionAbierto) { ?>
							<div class="input-group-prepend">
								<button class="btn btn-info" type="button" id="btnGenerarNroInforme">
									<i class="fa fa-download"></i>
								</button>
							</div>
						<?php } ?>
						<input type="text" class="form-control" placeholder="" aria-label=""
							   id="resumen_nro_informe" name="resumen_nro_informe" readonly
							   value="<?php echo $resumen->dinformefinal ?>">
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<?php if ($inspeccionAbierto) { ?>
							<div class="input-group-prepend">
								<button class="btn btn-info" type="button" id="btnGenerarFecha">
									<i class="fa fa-save"></i>
								</button>
							</div>
						<?php } ?>
						<div class="input-group-prepend">
							<span class="input-group-text">
								Fecha de Informe
							</span>
						</div>
						<input type="text" class="form-control" placeholder="" aria-label=""
							   id="resumen_fecha_terminada" name="resumen_fecha_terminada" readonly
							   value="<?php echo (empty($resumen->finformefin)) ? '' : date('d/m/Y', strtotime($resumen->finformefin)) ?>">
					</div>
				</div>
			</div>
		</div>
		<div>
			<?php if ($inspeccionAbierto) { ?>
				<button type="button" role="button" class="btn btn-info mr-2 btnGuardarResumen">
					<i class="fa fa-save"></i> Guardar datos
				</button>
				<button type="button" role="button" class="btn btn-secondary" id="btnGenerarInforme">
					<i class="fa fa-file-archive"></i> &nbsp;Generar Informe Técnico
				</button>
			<?php } ?>
		</div>
	</div>
</div>

<div class="modal fade" id="resumenTemaModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-success">
				<h5 class="modal-title" id="exampleModalLabel">Detalle Resumen</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="#" method="POST" accept-charset="UTF-8" id="frmResumenTema">
					<input type="hidden" class="d-none" id="res_tema_id" name="res_tema_id" value="">
					<div class="form-group">
						<label for="res_tema_value">
							Tema
						</label>
						<select name="res_tema_id" id="res_tema_value" disabled class="custom-select"
								style="width: 100% !important;"></select>
					</div>
					<div class="form-group">
						<label for="res_tema_descripcion">
							Descripción
						</label>
						<textarea name="res_tema_descripcion" id="res_tema_descripcion"
								  class="form-control"
								  rows="10"></textarea>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<?php if ($inspeccionAbierto) { ?>
					<button type="button" class="btn btn-secondary" data-dismiss="modal" id="btnResumenTemaCerrar">Cerrar
					</button>
					<button type="button" class="btn btn-primary" id="btnResumenTemaGuardar">
						<i class="fa fa-save"></i> Guardar
					</button>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
