<div class="row">
	<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
		<div class="card card-outline card-success">
			<div class="card-header">
				<h3 class="card-title">Lista de Reuniones</h3>
			</div>
			<div class="card-body">
				<div class="table-responsive" >
					<table class="table table-bordered" id="tblListaReuniones" >
						<thead>
						<tr>
							<th class="text-center" >Tipo de Reunión / Asistentes</th>
<!--							<th class="text-center" style="width: 80px !important; min-width: 80px !important;" >Fecha</th>-->
						</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
		<div class="card card-outline card-success">
			<div class="card-header">
				<h3 class="card-title">Reuniones</h3>
			</div>
			<div class="card-body">
				<div class="table-responsive" >
					<table class="table table-bordered" id="tblReuniones" >
						<thead>
						<tr>
							<th class="text-center" style="width: 30px !important; min-width: 30px !important;" ></th>
							<th class="text-center" >Tipo de Reunión</th>
							<th class="text-center" style="width: 150px !important; min-width: 150px !important;" ></th>
						</tr>
						</thead>
						<tbody>
						</tbody>
						<tfoot>
						<tr>
							<td colspan="3" >
								<button type="button" role="button" class="btn btn-link" id="btnAgregarReunion" >
									<i class="fa fa-plus" ></i> Agregar nuevo
								</button>
							</td>
						</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
		<div class="card card-outline card-success">
			<div class="card-header">
				<h3 class="card-title">Asistentes</h3>
			</div>
			<div class="card-body">
				<div class="table-responsive" >
					<table class="table table-bordered" id="tblAsistentes" >
						<thead>
						<tr>
							<th class="text-center" style="width: 30px !important; min-width: 30px !important;" ></th>
							<th class="text-center" >Nombres</th>
							<th class="text-center" >Cargo</th>
							<th class="text-center" style="width: 150px !important; min-width: 150px !important;" ></th>
						</tr>
						</thead>
						<tbody>
						</tbody>
						<tfoot>
						<tr>
							<td colspan="4" >
								<button type="button" role="button" class="btn btn-link" id="btnAgregarAsistente" >
									<i class="fa fa-plus" ></i> Agregar nuevo
								</button>
								<button type="button" role="button" class="btn btn-link" id="btnBuscarAsistente" >
									<i class="fa fa-plus" ></i> Buscar contacto
								</button>
								<input type="hidden" class="d-none" id="asistente_zctiporeunion" value="" >
							</td>
						</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
