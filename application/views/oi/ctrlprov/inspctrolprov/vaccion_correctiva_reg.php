<div class="modal fade" id="ModalAccionCorrectiva" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header bg-success">
				<h5 class="modal-title">Acción Correctiva Requisito</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form method="post" id="frmAccionCorrectiva" accept-charset="utf-8"
					  action="<?php echo base_url('oi/ctrlprov/cinspctrolprovacc/guardar') ?>" >
					<input type="hidden" name="acc_modal_cchecklist_id" id="acc_modal_cchecklist_id">
					<input type="hidden" name="acc_modal_requisito_cchecklist_id" id="acc_modal_requisito_cchecklist_id">

					<div class="form-group" >
						<label for="acc_modal_requisito">
							Check List Requisito
						</label>
						<textarea name="acc_modal_requisito" id="acc_modal_requisito"
								  class="form-control form-control-sm" readonly
								  rows="3"></textarea>
					</div>
					<div class="form-group" >
						<label for="acc_modal_tipo_hallazgo">
							Tipo Hallazgo
						</label>
						<input name="acc_modal_tipo_hallazgo" id="acc_modal_tipo_hallazgo"
							   class="form-control form-control-sm" readonly
							   value="" />
					</div>
					<div class="form-group" >
						<label for="acc_modal_hallazgo">
							Hallazgo
						</label>
						<textarea name="acc_modal_hallazgo" id="acc_modal_hallazgo"
								  class="form-control form-control-sm" readonly
								  rows="3"></textarea>
					</div>
					<div class="form-group" >
						<label for="acc_modal_acc">
							Acción Correctiva
						</label>
						<textarea name="acc_modal_acc" id="acc_modal_acc"
								  class="form-control form-control-sm"
								  rows="3"></textarea>
					</div>
					<div class="form-group" >
						<label for="acc_modal_responsable">
							Responsable
						</label>
						<input type="text" class="form-control"
							   id="acc_modal_responsable" name="acc_modal_responsable"
							   value="" >
					</div>
					<div class="row" >
						<div class="col-xl-4 col-lg-4 col-md-4 col-12" >
							<div class="form-group" >
								<label for="acc_modal_fecha">
									Fecha de Correción
								</label>
								<div class="input-group input-group-sm">
									<input type="text" class="form-control"
										   id="acc_modal_fecha" name="acc_modal_fecha"
										   value=""/>
									<div class="input-group-append" data-target="#FechaDesde">
										<div class="input-group-text"><i class="fa fa-calendar"></i></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group" >
						<div class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input"
								   id="acc_modal_acepta_acc" name="acc_modal_acepta_acc" value="1" >
							<label class="custom-control-label" for="acc_modal_acepta_acc">Acepta Acción Correctiva Observación</label>
						</div>
					</div>
					<div class="form-group" >
						<label for="acc_modal_observaciones">
							Observaciones del inspector
						</label>
						<textarea name="acc_modal_observaciones" id="acc_modal_observaciones"
								  class="form-control form-control-sm"
								  rows="3"></textarea>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<div class="d-flex justify-content-between w-100">
					<div style="<?php echo (isset($soloGuardar) && $soloGuardar) ? 'display: none' : ''; ?>" >
						<button type="button" class="btn btn-success" id="btnAnteriorACC" >
							<i class="fa fa-caret-left"></i>
							<span>anterior</span>
						</button>
					</div>
					<div>
						<button type="button" class="btn btn-secondary" data-dismiss="modal" id="btnCerrarACC" >
							Guardar y Cerrar
						</button>
						<button type="button" class="btn btn-primary" id="btnGuardarACC" style="<?php echo (isset($soloGuardar) && $soloGuardar) ? 'display: none' : ''; ?>" >
							<span><i class="fa fa-save"></i> Guardar y continuar</span>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
