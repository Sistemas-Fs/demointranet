<div class="row">
	<div class="col-12">
		<div class="card card-outline card-success">
			<div class="card-header">
				<h3 class="card-title">Peligros del establecimiento inspeccionado</h3>
			</div>
			<div class="card-body">
				<div class="btn-group btn-group-sm mb-2" >
					<button type="button" role="button" class="btn btn-primary btn-primary-sm"
							data-toggle="modal" data-target="#modalPeligros">
						<i class="fa fa-plus" ></i> Agregar Peligro
					</button>
				</div>

				<div>
					<table id="tblPeligros" class="table table-striped table-bordered"
						   style="width:100%">
						<thead>
						<tr>
<!--							<th >N°</th>-->
							<th >Producto</th>
							<th style="width: 10px !important; min-width: 10px !important;" >Tipo</th>
							<th>Peligro</th>
							<th>Detalle</th>
							<th style="width: 10px !important; min-width: 10px !important;" ></th>
						</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
				<div>
					<div class="row" >
						<div class="form-group col-12" >
							<label for="dproducto_peligro">
								Producto
							</label>
							<input type="text" class="form-control" id="dproducto_peligro" readonly maxlength="0" value="" >
							<input type="hidden" class="d-none" id="cproducto_peligro" readonly maxlength="0" value="" >
						</div>
						<div class="col-lg-4 col-xl-4 col-sm-12 col-12" >
							<div class="input-group" >
								<label for="dproducto_peligro_tipo" class="col-xl-8 col-xl-8 col-sm-12 col-12" >
									Peligro Identificado por cliente
								</label>
								<div class="col-xl-4 col-lg-4 col-sm-12 col-12" >
									<input type="text" class="form-control" id="dproducto_peligro_tipo"
										   readonly maxlength="0"
										   value="" >
								</div>
							</div>
						</div>
						<div class="col-12 mt-2" >
							<input type="text" class="form-control" id="dproducto_peligro_peligro"
								   aria-label=""
								   readonly maxlength="0" value="" >
						</div>
						<div class="col-12 mt-2" >
							<textarea class="form-control" aria-label=""
									name="dproducto_peligro_dtexto" id="dproducto_peligro_dtexto"
									  rows="10"></textarea>
							<div class="btn-group mt-2" id="contenidoDetallePeligro" style="display: none"  >
								<button type="button" class="btn btn-danger mr-2" id="btnGuardarDetallePeligro" >
									<i class="fa fa-save" ></i> Guardar
								</button>
								<button type="button" class="btn btn-secondary" id="btnEliminarDetallePeligro" >
									<i class="fa fa-trash" ></i> Eliminar
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modalPeligros" tabindex="-1"
	 aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Productos Peligros</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div>
					<table id="tblListaPeligros" class="table table-striped table-bordered"
						   style="width:100%">
						<thead>
						<tr>
							<th>Código</th>
							<th>Producto</th>
							<th style="width: 50px !important; min-width: 50px !important;" ></th>
						</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>
