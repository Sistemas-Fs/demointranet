<?php
  $idusuario = $this -> session -> userdata('s_idusuario');
  $idempleado = $this -> session -> userdata('s_idempleado');
  $datosuser = $this -> session -> userdata('s_name');
?>
<style>
  #cboRecuperahora[readonly]{
    background: #eee;
    cursor:no-drop;
    -webkit-appearance: none;
  }

  #cboRecuperahora[readonly] option{
      display:none;
  }
</style>

<!-- content-header -->
<div class="content-header">   
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">DASHBOARD</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="">Home</a></li>
          <!--<li class="breadcrumb-item active">Dashboard Interno</li>-->
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-4 col-6">
        <div class="small-box bg-success">
          <div class="inner">
            <h4><b>PERMISOS</b> &nbsp;<a data-toggle="modal" href="#modalCuadroPermisos" class="small-box-footer"><i class="far fa-list-alt"></i></a></h4>
            <p style="font: icon;" id="pdatosPerm"></p>            
          </div>
          <div class="icon">
            <i class="ion ion-log-out"></i>
          </div>
          <a id="a-modallistperm" data-toggle="modal" href="#modalRegPermisos" class="small-box-footer">Registrar <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>

      <div class="col-lg-4 col-6">
        <div class="small-box bg-warning">
          <div class="inner">
            <h4><b>VACACIONES</b> &nbsp;<a data-toggle="modal" href="#modalCuadroVacaciones" class="small-box-footer"><i class="far fa-list-alt"></i></a></h4>
            <p style="font: icon;" id="pdatosVaca"></p>
          </div>
          <div class="icon">
            <i class="ion ion-briefcase"></i>
          </div>
          <a id="a-modallistvaca" data-toggle="modal" href="#modalRegVacaciones" class="small-box-footer">Registrar <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>

      <div class="col-lg-4 col-6">
        <div class="small-box bg-danger">
          <div class="inner">
            <h4><b>HORAS ACUMULADAS</b> &nbsp;<a data-toggle="modal" href="#modalCuadroExtras" class="small-box-footer"><i class="far fa-list-alt"></i></a></h4>
            <p style="font: icon;" id="pdatosHextra"></p>
          </div>
          <div class="icon">
            <i class="ion ion-clock"></i>
          </div>
          <a id="a-modallistextra" data-toggle="modal" href="#modalRegExtras" class="small-box-footer">Registrar <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div id="accordion">       
        <div class="card card-primary">
            <div class="card-header">
              <h4 class="card-title">
                <a data-toggle="collapse"  href="#collapseResumenPerm">
                  :: Resumen Permisos
                </a>
              </h4>
              <div class="card-tools">
                  <ul class="nav nav-pills ml-auto">
                    <li class="nav-item">
                      <a  onclick="excelResumen(<?php echo $idempleado; ?>);" class="btn btn-outline-success btn-sm hidden-sm"><span class="fas fa-file-excel" aria-hidden="true"> </span> &nbsp;Excel Detalle</a>
                    </li>
                  </ul>
              </div>
            </div>
            <div id="collapseResumenPerm" class="panel-collapse collapse" data-parent="#accordion">
              <div class="card-body">
                <div class="row">
                  <div class="col-12 col-sm-4">
                    <div class="info-box bg-light">
                      <div class="info-box-content">
                        <span class="info-box-text text-center text-muted">TOTAL HORAS</span>
                        <span class="info-box-text text-center text-muted">EXTRAS ACUMULADAS</span>
                        <span class="info-box-number text-center text-muted mb-0" id='spanhoraacum'></span>
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-sm-4">
                    <div class="info-box bg-light">
                      <div class="info-box-content">
                        <span class="info-box-text text-center text-muted">TOTAL HORAS</span>
                        <span class="info-box-text text-center text-muted">PERMISOS ACUMULADOS</span>
                        <span class="info-box-number text-center text-muted mb-0" id='spanpermacum'></span>
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-sm-4">
                    <div class="info-box bg-light">
                      <div class="info-box-content">
                        <span class="info-box-text text-center text-muted">HORAS A</span>
                        <span class="info-box-text text-center text-muted" id='spanpermest'></span>
                        <span class="info-box-number text-center text-muted mb-0" id='spanhorapend'><span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12 col-md-12 col-lg-12 order-2 order-md-1">
                    <div class="row">
                      <div class="col-12 col-sm-4">
                        <div class="info-box bg-light">
                          <div class="info-box-content">
                            <span class="info-box-text text-center text-muted">DIAS DE </span>
                            <span class="info-box-text text-center text-muted">VACACIONES</span>
                            <span class="info-box-number text-center text-muted mb-0" id='spandiavaca'></span>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-sm-4">
                        <div class="info-box bg-light">
                          <div class="info-box-content">
                            <span class="info-box-text text-center text-muted">DIAS TOMADOS POR</span>
                            <span class="info-box-text text-center text-muted">VACACIONES</span>
                            <span class="info-box-number text-center text-muted mb-0" id='spandiaxvaca'></span>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-sm-4">
                        <div class="info-box bg-light">
                          <div class="info-box-content">
                            <span class="info-box-text text-center text-muted">DIAS</span>
                            <span class="info-box-text text-center text-muted" id='spanvacaest'></span>
                            <span class="info-box-number text-center text-muted mb-0" id='spandiapend'><span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- /.Main content -->

<!-- /.modal-permiso -->  
<div class="modal" id="modalCuadroPermisos" data-backdrop="static" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg" id="popupListVacaciones">
    <div class="modal-content">
      <div class="modal-header text-center bg-success">
        <h4 class="modal-title w-100 font-weight-bold">Control de Permisos</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">  
        <div class="row">
          <div class="col-md-12" style="overflow-x: scroll;">                 
            <table id="tblPermisos" class="table" style="width:100%">
                <thead>
                <tr>
                    <th>FECHA H. PERMISOS</th>
                    <th>HORAS PERMISOS</th>
                    <th>HORAS UTILIZADAS</th>
                    <th>MOTIVO</th>
                    <th>FUNDAMENTACIÓN</th>
                </tr>
                </thead>
                <tbody></tbody>               
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer justify-content-between" style="background-color: #dff0d8;">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modalRegPermisos" data-backdrop="static" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header text-center bg-success">
            <h4 class="modal-title w-100 font-weight-bold">Permisos</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div> 
        <div class="modal-body">      
          <form class="form-horizontal" id="frmMantPermisos" name="frmMantPermisos" action="<?= base_url('adm/rrhh/cctrlpermisos/setpermisos')?>" method="POST" enctype="multipart/form-data" role="form"> 
            <input type="hidden" id="mhdnIdPerm" name="mhdnIdPerm"> <!-- ID -->
            <input type="hidden" id="mhdnAccionPerm" name="mhdnAccionPerm">
            <input type="hidden" id="mhdnEmpPerm" name="mhdnEmpPerm" value= <?php echo $idempleado; ?> > 
            <div class="form-group">
                <div class="row">
                <div class="col-sm-4">
                    <div class="text-light-black">Fecha Registro</div>
                    <div>
                    <input class="form-control" id="mtxtFregistroperm" name="mtxtFregistroperm" type="text" readonly>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="text-light-blue">Motivo</div> 
                    <select id="mcboMotivo" name="mcboMotivo" class="form-control select" style="width: 100%; background-position: right 15px center;"  data-validation="required">
                        <option value = "" selected="selected">::Elija</option>
                        <option value="P">PERSONALES</option>
                        <option value="S">SALUD</option>
                        <option value="O">OTROS</option>
                    </select>
                </div>  
                </div>                
            </div>
            <div class="form-group">
                <div class="row">
                <div class="col-sm-4">
                    <div class="text-light-blue">Fecha Salida</div>
                    <div class="input-group date" id="mtxtFsalidaperm" data-target-input="nearest">
                    <input type="text" id="mtxtFsalperm" name="mtxtFsalperm" class="form-control datetimepicker-input" data-target="#mtxtFsalidaperm"/>
                    <div class="input-group-append" data-target="#mtxtFsalidaperm" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                    </div>
                </div>  
                <div class="col-sm-4">
                    <div class="text-light-blue">Hora Salida</div>
                    <div class="input-group date" id="mtxtHsalidaperm" data-target-input="nearest">
                    <input type="text" id="mtxtHsalperm" name="mtxtHsalperm" class="form-control datetimepicker-input" data-target="#mtxtHsalidaperm"/>
                    <div class="input-group-append" data-target="#mtxtHsalidaperm" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="far fa-clock"></i></div>
                    </div>
                    </div>
                </div>  
                <div class="col-sm-4">
                    <div class="text-light-blue">Hora Retorno</div>
                    <div class="input-group date" id="mtxtHretornoperm"  data-target-input="nearest">
                    <input type="text" id="mtxtHretorperm" name="mtxtHretorperm" class="form-control datetimepicker-input" data-target="#mtxtHretornoperm"/>
                    <div class="input-group-append" data-target="#mtxtHretornoperm" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="far fa-clock"></i></div>
                    </div>
                    </div>
                </div> 
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                <div class="col-sm-12"> 
                    <div class="text-light-blue">Fundamentación</div> 
                    <textarea type="text" class="form-control" id="mtxtFundamentoperm" name="mtxtFundamentoperm" rows="3" cols="40" data-validation="required"></textarea>
                </div>
                </div> 
            </div>
            <div class="form-group">
                <div class="row">
                <div class="col-sm-8"> 
                    <div class="text-light-blue">Recupera Horas</div>                   
                    <select id="cboRecuperahora" name="cboRecuperahora" class="form-control" style="width: 100%;" style="width: 100%;">
                    <option value="S" selected="selected">CON OPCIÓN A RECUPERAR</option>
                    <option value="V">A CUENTA DE VACACIONES</option> 
                    </select>
                </div>
                <div class="col-sm-4" id="recuHora">
                    <div class="text-light-blue">Fecha Recuperación</div>
                    <div class="input-group date" id="mtxtFrecupera" data-target-input="nearest">
                    <input type="text"  id="mtxtFrecuperm" name="mtxtFrecuperm" class="form-control datetimepicker-input" data-target="#mtxtFrecupera"/>
                    <div class="input-group-append" data-target="#mtxtFrecupera" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                    </div>
                </div>
                </div>                
            </div>                     
          </form>
        </div>
        <div class="modal-footer">
            <button type="reset" class="btn btn-default" id="btnRetornarPerm" data-dismiss="modal">Cerrar</button>
            <button type="submit" form="frmMantPermisos" class="btn btn-info" id="mbtnGPerm">Grabar</button>
        </div>
    </div>
  </div>
</div>
<!-- /.modal-->

<!-- /.modal-vacaciones -->  
<div class="modal" id="modalCuadroVacaciones" data-backdrop="static" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg" id="popupListVacaciones">
    <div class="modal-content">
      <div class="modal-header text-center bg-success">
        <h4 class="modal-title w-100 font-weight-bold">Control de Vacaciones</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">  
        <div class="row">
          <div class="col-md-12" style="overflow-x: scroll;">   
            <table id="tblVacaciones" class="table table-striped table-bordered compact" style="width:100%">
                <thead>
                <tr>
                    <th>F. Salida Vacaciones</th>
                    <th>F. Retorno Vacaciones</th>
                    <th>Dias Tomados</th>
                    <th>Detalle</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer justify-content-between" style="background-color: #dff0d8;">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modalRegVacaciones" data-backdrop="static" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header text-center bg-success">
          <h4 class="modal-title w-100 font-weight-bold">Registro de Vacaciones</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body"> 
          <form class="form-horizontal" id="frmMantVacaciones" name="frmMantVacaciones" action="<?= base_url('adm/rrhh/cctrlpermisos/setvacaciones')?>" method="POST" enctype="multipart/form-data" role="form"> 
            <input type="hidden" id="mhdnIdVaca" name="mhdnIdVaca"> <!-- ID -->
            <input type="hidden" id="mhdnAccionVaca" name="mhdnAccionVaca">
            <input type="hidden" id="mhdnEmpVaca" name="mhdnEmpVaca" value= <?php echo $idempleado; ?> > 
            <div class="form-group">
              <div class="row">                                         
                <div class="col-sm-4">
                  <div class="text-light-black">Fecha Registro</div>
                  <div>
                    <input class="form-control" id="mtxtFregistrovaca" name="mtxtFregistrovaca" type="text" readonly>
                  </div>
                </div>                             
              </div>                
            </div> 
            <div class="form-group">
              <div class="row">
                <div class="col-sm-6">
                  <div class="text-light-blue">Fecha Salida</div>
                  <div class="input-group date" id="mtxtFsalidavaca" data-target-input="nearest">
                    <input type="text" id="mtxtFsalvaca" name="mtxtFsalvaca" class="form-control datetimepicker-input" data-target="#mtxtFsalidavaca"/>
                    <div class="input-group-append" data-target="#mtxtFsalidavaca" data-toggle="datetimepicker">
                      <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                  </div>
                </div>  
                <div class="col-sm-6">
                  <div class="text-light-blue">Fecha Retorno</div>
                  <div class="input-group date" id="mtxtFretornovaca" data-target-input="nearest">
                    <input type="text" id="mtxtFretovaca" name="mtxtFretovaca" class="form-control datetimepicker-input" data-target="#mtxtFretornovaca"/>
                    <div class="input-group-append" data-target="#mtxtFretornovaca" data-toggle="datetimepicker">
                      <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                  </div>
                </div>   
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-sm-12"> 
                  <div class="text-light-blue">Comentarios</div> 
                  <textarea type="text" class="form-control" id="mtxtFundamentovaca" name="mtxtFundamentovaca" rows="3" cols="40" data-validation="required"></textarea>
                </div>
              </div> 
            </div>                    
          </form>
        </div>

        <div class="modal-footer justify-content-between" style="background-color: #dff0d8;">
          <button type="reset" class="btn btn-default" id="btnRetornarVaca" data-dismiss="modal">Cancelar</button>
          <button type="submit" form="frmMantVacaciones" class="btn btn-info" id="mbtnGVaca">Grabar</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- /.modal-->

<!-- /.modal-extras --> 
<div class="modal" id="modalCuadroExtras" data-backdrop="static" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg" id="popupListVacaciones">
    <div class="modal-content">
      <div class="modal-header text-center bg-success">
        <h4 class="modal-title w-100 font-weight-bold">Control de Horas Extras</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">  
        <div class="row">
          <div class="col-md-12" style="overflow-x: scroll;">                 
            <table id="tblExtras" class="table" style="width:100%">
                <thead>
                <tr>
                    <th>FECHA H. EXTRAS</th>
                    <th>HORAS INGRESO</th>
                    <th>HORAS SALIDA</th>
                    <th>MOTIVO</th>
                </tr>
                </thead>
                <tbody></tbody>               
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer justify-content-between" style="background-color: #dff0d8;">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modalRegExtras" data-backdrop="static" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header text-center bg-success">
            <h4 class="modal-title w-100 font-weight-bold">Horas Extras</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div> 
        <div class="modal-body">    
          <form class="form-horizontal" id="frmMantHorasextras" name="frmMantHorasextras" action="<?= base_url('adm/rrhh/cctrlpermisos/sethorasextras')?>" method="POST" enctype="multipart/form-data" role="form"> 
            <input type="hidden" id="mhdnIdHextra" name="mhdnIdHextra"> <!-- ID -->
            <input type="hidden" id="mhdnAccionHextra" name="mhdnAccionHextra">
            <input type="hidden" id="mhdnEmpHextra" name="mhdnEmpHextra">
            <div class="form-group">
                <div class="row">
                <div class="col-sm-4">
                    <div class="text-light-black">Fecha Registro</div>
                    <div>
                    <input class="form-control" id="mtxtFregistrohextras" name="mtxtFregistrohextras" type="text" readonly>
                    </div>
                </div>
                </div>                
            </div>
            <div class="form-group">
                <div class="row">
                <div class="col-sm-4">
                    <div class="text-light-blue">Fecha Horas Extras</div>
                    <div class="input-group date" id="mtxtFsalidahextras" data-target-input="nearest">
                    <input type="text" id="mtxtFsalhextras" name="mtxtFsalhextras" class="form-control datetimepicker-input" data-target="#mtxtFsalidahextras"/>
                    <div class="input-group-append" data-target="#mtxtFsalidahextras" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                    </div>
                </div>  
                <div class="col-sm-4">
                    <div class="text-light-blue">Hora Entrada</div>
                    <div class="input-group date" id="mtxtHsalidahextras" data-target-input="nearest">
                    <input type="text" id="mtxtHsalhextras" name="mtxtHsalhextras" class="form-control datetimepicker-input" data-target="#mtxtHsalidahextras"/>
                    <div class="input-group-append" data-target="#mtxtHsalidahextras" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="far fa-clock"></i></div>
                    </div>
                    </div>
                </div>  
                <div class="col-sm-4">
                    <div class="text-light-blue">Hora Salida</div>
                    <div class="input-group date" id="mtxtHretornohextras"  data-target-input="nearest">
                    <input type="text" id="mtxtHretorhextras" name="mtxtHretorhextras" class="form-control datetimepicker-input" data-target="#mtxtHretornohextras"/>
                    <div class="input-group-append" data-target="#mtxtHretornohextras" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="far fa-clock"></i></div>
                    </div>
                    </div>
                </div> 
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                <div class="col-sm-12"> 
                    <div class="text-light-blue">Motivo</div> 
                    <textarea type="text" class="form-control" id="mtxtMotivohextras" name="mtxtMotivohextras" rows="3" cols="40" data-validation="required"></textarea>
                </div>
                </div> 
            </div>                      
          </form>
        </div>
        <div class="modal-footer">
            <button type="reset" class="btn btn-default" id="btnRetornarHextras" data-dismiss="modal">Cerrar</button>
            <button type="submit" form="frmMantHorasextras" class="btn btn-info" id="mbtnGHextras">Grabar</button>
        </div>
    </div>
  </div>
</div> 
<!-- /.modal-->


