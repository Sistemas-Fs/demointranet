<?php
    $idusu = $this -> session -> userdata('s_idusuario');
?>

<style>
</style>

<!-- content-header -->
<div class="content-header">   
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">GENERAR INFORMES</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?php echo public_base_url(); ?>main">Home</a></li>
          <li class="breadcrumb-item active">Laboratorio</li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">  
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">BUSQUEDA</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                </div>
            </div>
          
            <div class="card-body">
                <input type="hidden" name="mtxtidusupropu" class="form-control" id="mtxtidusupropu" value="<?php echo $idusu ?>">
                <div class="row"> 
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Clientes</label>
                            <select class="form-control select2bs4" id="cboclieserv" name="cboclieserv" style="width: 100%;">
                                <option value="" selected="selected">Cargando...</option>
                            </select>
                        </div>
                    </div>   
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Tipo Consulta</label>
                            <select class="form-control select2bs4" id="cbotipobuscar" name="cbotipobuscar" style="width: 100%;">
                                <option value="C" selected="selected">Por Cotizacion</option>
                                <option value="T">Por OT</option>
                                <option value="I">Por Informe</option>
                            </select>
                        </div>
                    </div> 
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label>&nbsp;&nbsp;</label> 
                            <input type="text" class="form-control" id="txtbuscar" name="txtbuscar" placeholder="..."  onkeypress="pulsarListarCoti(event)" />
                        </div>
                    </div>      
                </div>
            </div>                
                        
            <div class="card-footer justify-content-between"> 
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-right">
                            <button type="button" class="btn btn-primary" id="btnBuscar"><i class="fas fa-search"></i> Buscar</button>    
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-outline card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Listado </h3>
                    </div>
                
                    <div class="card-body">
                        <table id="tblListgeninf" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                            <tr> 
                                <th></th>
                                <th></th>
                                <th>CLIENTE</th>
                                <th>TIPO INFORME</th>
                                <th>RESULTADOS</th>
                                <th>BORRADOR</th>
                                <th>COTIZACION</th>
                                <th>ORDEN TRABAJO</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    
    </div>
</section>
<!-- /.Main content -->

<!-- /.modal- por Muestras --> 
<div class="modal fade" id="modalPormuestras" data-backdrop="static" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-header text-center bg-primary">
            <h4 class="modal-title w-100 font-weight-bold">Borrador por Muestras</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <input type="hidden" name="mhdnidordenserviciomuestra" id="mhdnidordenserviciomuestra" class="form-control">
            <div class="form-group">  
                <!--<div class="row">
                    <div class="col-sm-12">
                        <div class="form-group clearfix">
                            <div class="icheck-primary d-inline">
                                <input type="radio" id="rdinf" value="I" name="restado" checked>
                                <label for="rdinf">
                                    INF.
                                </label>
                            </div>
                            <div class="icheck-primary d-inline">
                                <input type="radio" id="rdcc" value="C" name="restado">
                                <label for="rdcc">
                                    C.C.
                                </label>
                            </div>
                        </div>
                    </div>
                </div>-->
                <div class="row">
                    <div class="col-md-12"> 
                            <!--<label>Muestras del Borrador</label>
                            <select class="form-control" id="cbomuestrasborrador" name="cbomuestrasborrador" style="width: 100%;">
                                <option value="" selected="selected">Seleccionar</option>
                            </select>-->
                        
                        <div class="card card-outline card-lightblue">
                            <div class="card-header"> 
                                <h3 class="card-title">Listado de Muestras </h3>
                            </div>                                        
                            <div class="card-body" style="overflow-x: scroll;">
                                <table id="tblListmuestrasborrador" class="table table-striped table-bordered compact" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Nro Muestra</th>
                                        <th>Seleccion</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>  
                </div>   
            </div>
        </div>
        <div class="modal-footer" style="background-color: #dff0d8;">
            <button type="reset" class="btn btn-default" id="mbtnCPormuestra" data-dismiss="modal">Cancelar</button>   
            <button type="button" class="btn btn-info" id="mbtnBorrador">Borrador</button>             
        </div>
      
    </div>
  </div>
</div> 
<!-- /.modal-->

<!-- /.modal-Personalizar --> 
<div class="modal fade" id="modalPersonalizar" data-backdrop="static" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
        <div class="modal-header text-center bg-primary">
            <h4 class="modal-title w-100 font-weight-bold">Generar Informe Personalizado</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        <div class="modal-body">
            <input type="hidden" name="mhdnidordenservicio" id="mhdnidordenservicio" class="form-control">  
            <input type="hidden" name="mhdnidcotizacion" id="mhdnidcotizacion" class="form-control"> 
            <input type="hidden" name="mhdnidinforme" id="mhdnidinforme" class="form-control">
            <input type="hidden" name="mhdnconcc" id="mhdnconcc" class="form-control">
                       
            <div class="form-group">                    
                <div class="row">
                    <div class="col-md-8 text-left">
                        <button type="button" class="btn btn-info" id="mbtnGGenInfPersonalizado">Generar Informe</button>
                    </div> 
                    <div class="col-md-4 text-right">                    
                        <select class="form-control" id="cbogenmuestras" name="cbogenmuestras" style="width: 100%;">
                            <option value="" selected="selected">Cargando...</option>
                        </select>
                    </div> 
                </div>
            </div>
            <div class="form-group">                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-primary">
                            <div class="card-header">
                            </div> 
                            <div class="card-body">
                            <div class="table-responsive">
                            <table id="tblListensayoperso" class="table table-striped table-bordered compact" style="width:100%">
                            <thead>
                            <tr>
                                <th>MUESTRA</th>
                                <th>TIPO ENSAYO</th>
                                <th></th>
                                <th>CODIGO</th>
                                <th>ENSAYO</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            </table>
                            </div>   
                            </div>     
                            </div> 
                        </div> 
                    </div>    
            </div>
        <!--</form>-->
        </div>

        <div class="modal-footer justify-content-between" style="background-color: #dff0d8;">
            <button type="reset" class="btn btn-default" id="mbtnCGenInfPersonalizado" data-dismiss="modal">Cancelar</button>            
        </div>
      
    </div>
  </div>
</div> 
<!-- /.modal-->

<!-- /.modal-Resultados --> 
<div class="modal fade" id="modalResultados" data-backdrop="static" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
        <div class="modal-header text-center bg-primary">
            <h4 class="modal-title w-100 font-weight-bold">Resultados</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        <div class="modal-body" style="overflow-x: scroll;">
            <div class="row">
                <div class="col-12 table-responsive" id="divtblListResultados"> 
                    <table id="tblListResultados" class="table table-striped table-bordered compact" style="width:100%">
                        <thead>
                            <tr>
                                                                    <th style="min-width: 20px; width: 20px;">#</th>
                                                                    <th></th>
                                                                    <th style="min-width: 110px; width: 110px;">Cod. Ensayo</th>
                                                                    <th style="min-width: 150px; width: 150px;">Ensayo</th>
                                                                    <th style="min-width: 100px; width: 100px;">Unidad</th>
                                                                    <th style="min-width: 50px; width: 50px;"></th>
                                                                    <th style="min-width: 60px; width: 60px;">Especif.</th>
                                                                    <th style="min-width: 10px; width: 10px;">x10</th>
                                                                    <th style="min-width: 50px; width: 50px;"></th>
                                                                    <th style="min-width: 60px; width: 60px;">Resultados</th>
                                                                    <th style="min-width: 10px; width: 10px;">x10</th>
                                                                    <th style="min-width: 100px; width: 100px;">Observacion</th>
                                                                    <th style="min-width: 100px; width: 100px;">Conclusion</th>
                                                                    <th style="min-width: 30px; width: 30px;">Selec.</th>
                                                                    <th style="min-width: 30px; width: 30px;">VB</th>
                            </tr>
                        </thead><tbody></tbody>
                    </table>
                </div> 
                <div class="col-12" id="divtblListResultadosold"> 
                    <table id="tblListResultadosold" class="table table-striped table-bordered compact" style="width:100%">
                        <thead>
                            <tr>
                                                                    <th style="min-width: 20px; width: 20px;">#</th>
                                                                    <th></th>
                                                                    <th style="min-width: 120px; width: 120px;">Cod. Ensayo</th>
                                                                    <th style="min-width: 220px; width: 220px;">Ensayo</th>
                                                                    <th style="min-width: 150px; width: 150px;">Unidad</th>
                                                                    <th style="min-width: 80px; width: 80px;">Resultados</th>
                                                                    <th style="min-width: 50px; width: 50px;">x10</th>
                                                                    <th style="min-width: 50px; width: 50px;">Selec.</th>
                                                                    <th style="min-width: 50px; width: 50px;">VB</th>
                            </tr>
                        </thead><tbody></tbody>
                    </table>
                </div> 
            </div>
        </div>

        <div class="modal-footer justify-content-between" style="background-color: #dff0d8;">
            <button type="reset" class="btn btn-default" id="mbtnCGenInfPersonalizado" data-dismiss="modal">Cancelar</button>            
        </div>
      
    </div>
  </div>
</div> 
<!-- /.modal-->


<!-- Script Generales -->
<script type="text/javascript">
    var baseurl = "<?php echo base_url();?>"; 
</script>