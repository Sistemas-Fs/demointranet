<?php
    $idusu = $this -> session -> userdata('s_idusuario');
?>

<style>
</style>

<!-- content-header -->
<div class="content-header">   
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">CONSULTA DE RESULTADOS GENERAL</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?php echo public_base_url(); ?>main">Home</a></li>
          <li class="breadcrumb-item active">Laboratorio</li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">  
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">BUSQUEDA</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                </div>
            </div>
          
            <div class="card-body">
            <form class="form-horizontal" id="frmresultgral" name="frmresultgral" action="<?= base_url('lab/consultas/cconsresultgral/excelresultgral')?>" method="POST" enctype="multipart/form-data" role="form"> 
                <input type="hidden" name="mtxtidusupropu" class="form-control" id="mtxtidusupropu" value="<?php echo $idusu ?>">
                <div class="row"> 
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Clientes</label>
                            <select class="form-control select2bs4" id="cboclieserv" name="cboclieserv" style="width: 100%;">
                                <option value="" selected="selected">Cargando...</option>
                            </select>
                        </div>
                    </div>  
                    <div class="col-md-2">    
                        <label>Desde</label>                        
                        <div class="input-group date" id="txtFDesde" data-target-input="nearest" >
                            <input type="text" id="txtFIni" name="txtFIni" class="form-control datetimepicker-input" data-target="#txtFDesde" disabled/>
                            <div class="input-group-append" data-target="#txtFDesde" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">      
                        <label>Hasta</label>                      
                        <div class="input-group date" id="txtFHasta" data-target-input="nearest">
                            <input type="text" id="txtFFin" name="txtFFin" class="form-control datetimepicker-input" data-target="#txtFHasta" disabled/>
                            <div class="input-group-append" data-target="#txtFHasta" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                    </div> 
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>&nbsp;&nbsp;</label> 
                            <input type="text" class="form-control" id="txtbuscar" name="txtbuscar" placeholder="...">
                        </div>
                    </div>      
                </div>                  
            </form>              
            </div>                
                        
            <div class="card-footer justify-content-between"> 
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-right">
                            <button type="button" class="btn btn-primary" id="btnBuscar"><i class="fas fa-search"></i> Buscar</button> 
                            <button type="submit" form="frmresultgral" class="btn btn-info" id="btnexcel" disabled="true"><i class="fa fw fa-file-excel-o"></i> Exportar Excel</button>     
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-outline card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Listado </h3>
                    </div>
                
                    <div class="card-body">
                        <table id="tblListconsresultgral" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                            <tr>
                                <th></th>
                                <th>COTIZACION</th>
                                <th>FECHA RECEPCION</th>
                                <th>NOMBRE PRODUCTO</th>
                                <th>PRESENTACION</th>
                                <th>CONDICION</th>
                                <th>ENSAYO</th>
                                <th>VIA</th>
                                <th>RESULTADO</th>
                                <th>UNIDAD</th>
                                <th>ESTADO</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    
    </div>
</section>
<!-- /.Main content -->


<!-- /.modal- Elementos --> 
<div class="modal fade" id="modalElementos" data-backdrop="static" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header text-center bg-primary">
            <h4 class="modal-title w-100 font-weight-bold">Elementos Resultados</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <input type="hidden" name="mhdecinternoordenservicio" id="mhdecinternoordenservicio" class="form-control">
            <input type="hidden" name="mhdecinternocotizacion" id="mhdecinternocotizacion" class="form-control"> 
            <input type="hidden" name="mhdenordenproducto" id="mhdenordenproducto" class="form-control">
            <input type="hidden" name="mhdecmuestra" id="mhdecmuestra" class="form-control"> 
            <input type="hidden" name="mhdecensayo" id="mhdecensayo" class="form-control"> 
            <input type="hidden" name="mhdenviausado" id="mhdenviausado" class="form-control"> 
            <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                            <div class="card-body"> 
                            <div class="table-responsive">
                            <table class="table m-0 tbl-tramites table-striped table-bordered compact" id="tblElementos">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th style="min-width: 250px; width: 250px;">Elementos</th>
                                <th style="min-width: 180px; width: 180px;">Unidad</th>
                                <th style="min-width: 200px; width: 200px;">Resultados</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                            </table>
                            </div>   
                            </div>     
                            </div> 
                        </div>  
                    </div>   
            </div>
        </div>

        <div class="modal-footer" style="background-color: #dff0d8;">
            <button type="reset" class="btn btn-default" id="mbtnCElementos" data-dismiss="modal">Cancelar</button>            
        </div>
      
    </div>
  </div>
</div> 
<!-- /.modal-->