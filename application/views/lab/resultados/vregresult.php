<?php
    $idusu = $this -> session -> userdata('s_idusuario');
    $cusuario = $this -> session -> userdata('s_cusuario');
?>

<style>
    
    .ocultar {
        color: white !important;
        opacity: 0.0 !important;
        font-size: 0.4rem;
    }

    tab {
        display: inline-block; 
        margin-left: 30px; 
    }
    tr.subgroup,
    tr.subgroup:hover {
        background-color: #F2F2F2 !important;
        /* color: blue; */
        font-weight: bold;
    }
    .group{
        background-color:#D5D8DC !important;
        font-size:15px;
        color:#000000!important;
        opacity:0.8;
        cursor: pointer;
    }
    .subgroup{
        cursor: pointer;
        font-weight: normal !important;
        font-size: 15px !important;
    }

    .btn-circle {
        width: 45px;
        height: 45px;
        line-height: 45px;
        text-align: center;
        padding: 0;
        border-radius: 50%;
    }
    
    .btn-circle i {
        position: relative;
        top: -1px;
    }

    .btn-circle-sm {
        width: 35px;
        height: 35px;
        line-height: 35px;
        font-size: 0.9rem;
    }

    .btn-circle-lg {
        width: 55px;
        height: 55px;
        line-height: 55px;
        font-size: 1.1rem;
    }

    .btn-circle-xl {
        width: 70px;
        height: 70px;
        line-height: 70px;
        font-size: 1.3rem;
    }

    .fileUpload {
        position: relative;
        overflow: hidden;
        margin: 0px;
    }
    .fileUpload input.upload {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }
    
    .dropdown-item:hover{
        border-color: #0067ab;
        background-color: #e83e8c !important;
    }

    td.details-control {
        background: url('<?php echo public_base_url(); ?>assets/images/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.details td.details-control {
        background: url('<?php echo public_base_url(); ?>assets/images/details_close.png') no-repeat center center;
    }
</style>

<!-- content-header -->
<div class="content-header">   
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">INGRESO DE RESULTADOS</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?php echo public_base_url(); ?>main"> <i class="fas fa-tachometer-alt"></i>Home</a></li>
          <li class="breadcrumb-item active">Laboratorio</li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content" id="contenedorCotizacion" style="background-color: #E0F4ED;">
    <div class="container-fluid">  
        <div class="row">
            <div class="col-12">
                <div class="card card-primary card-outline card-tabs">
                    <div class="card-header p-0 pt-1 border-bottom-0">            
                        <ul class="nav nav-tabs" id="tablab" style="background-color: #2875A7;" role="tablist">                    
                            <li class="nav-item">
                                <a class="nav-link active" style="color: #000000;" id="tablab-list-tab" data-toggle="pill" href="#tablab-list" role="tab" aria-controls="tablab-list" aria-selected="true">LISTADO</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" style="color: #000000;" id="tablab-reg-tab" data-toggle="pill" href="#tablab-reg" role="tab" aria-controls="tablab-reg" aria-selected="false">REGISTRO</a>
                            </li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content" id="tablab-tabContent">
                            <div class="tab-pane fade show active" id="tablab-list" role="tabpanel" aria-labelledby="tablab-list-tab">                                
                                <div class="card card-primary">
                                    <div class="card-header">
                                        <h3 class="card-title">BUSQUEDA</h3>
                                        <div class="card-tools">
                                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                        </div>
                                    </div>   
                                    <form class="form-horizontal" id="frmbuscarservlab" name="frmbuscarservlab" action="<?= base_url('lab/resultados/cregresultExport/excelservlab')?>" method="POST" enctype="multipart/form-data" role="form">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Clientes</label>
                                                    <select class="form-control select2bs4" id="cboclieserv" name="cboclieserv" style="width: 100%;">
                                                        <option value="" selected="selected">Cargando...</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <label>Buscar por</label>
                                                <div>
                                                    <select class="form-control" id="cbobuspor" name="cbobuspor">
                                                        <option value="C" selected="selected">Cotización</option>
                                                        <option value="T">Orden Trabajo</option>
                                                        <option value="I">Informe</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2"> 
                                                <label id="lblbuscar" name="lblbuscar">Numero</label> 
                                                <div>
                                                    <input type="text" id="txtbuscarnro" name="txtbuscarnro" class="form-control"  onkeypress="pulsarListarCoti(event)"/>
                                                </div>
                                            </div>
                                            <div class="col-md-2">    
                                                <div class="checkbox"><label>
                                                    <input type="checkbox" id="chkFreg" /> <b>Desde</b>
                                                </label></div>                        
                                                <div class="input-group date" id="txtFDesde" data-target-input="nearest" >
                                                    <input type="text" id="txtFIni" name="txtFIni" class="form-control datetimepicker-input" data-target="#txtFDesde" disabled/>
                                                    <div class="input-group-append" data-target="#txtFDesde" data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">      
                                                <label>Hasta</label>                      
                                                <div class="input-group date" id="txtFHasta" data-target-input="nearest">
                                                    <input type="text" id="txtFFin" name="txtFFin" class="form-control datetimepicker-input" data-target="#txtFHasta" disabled/>
                                                    <div class="input-group-append" data-target="#txtFHasta" data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4"> 
                                                <label>Nombre Producto/Muestra</label> 
                                                <div>
                                                    <input type="text" id="txtdescri" name="txtdescri" class="form-control"  onkeypress="pulsarListarCoti(event)"/>
                                                </div>
                                            </div>
                                            <div class="col-md-4"> 
                                                <label>Nombre/Codigo Ensayo</label> 
                                                <div>
                                                    <input type="text" id="txtensayo" name="txtensayo" class="form-control"  onkeypress="pulsarListarCoti(event)"/>
                                                </div>
                                            </div>
                                            <!--<div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Area de Servicio</label>
                                                    <select class="form-control select2bs4" id="cboareaserv" name="cboareaserv" multiple="multiple" data-placeholder="Seleccionar" style="width: 100%;">
                                                        <option value="%" selected="selected">Todos</option>
                                                        <option value="M">Microbiología</option>
                                                        <option value="F">Fisicoquímica</option>
                                                        <option value="I">Instrumental</option>
                                                    </select>
                                                </div>
                                            </div>-->
                                        </div>
                                    </div>                
                                                
                                    <div class="card-footer justify-content-between"> 
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="text-right">
                                                    <button type="button" class="btn btn-primary" id="btnBuscar"><i class="fas fa-search"></i> Buscar</button> 
                                                    <button type="submit" class="btn btn-success" id="btnexcel" disabled="true"><i class="far fa-file-excel"></i> Exportar Excel</button>    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card card-outline card-primary">
                                            <div class="card-header">
                                                <div class="row">
                                                <div class="col-md-8">
                                                    <h3 class="card-title"><b>LISTADO DE SERVICIOS LABORATORIO</b></h3>
                                                </div>                                                 
                                                <!--<div class="col-md-4 text-right">
                                                    <button id="btn-show-all-children" type="button">Expandir</button>
                                                    <button id="btn-hide-all-children" type="button">Contraer</button>
                                                </div>  /.Main content -->
                                                </div>                                                
                                            </div>                                        
                                            <div class="card-body">
                                                <table id="tblListServiciolab" class="table table-striped table-bordered compact" style="width:100%">
                                                    <thead>
                                                    <tr align="center">
                                                        <th>Cliente</th>
                                                        <th></th>
                                                        <th>coti</th>
                                                        <th></th>
                                                        <th></th>
                                                        <th>codmuestra</th>
                                                        <th>MUESTRAS - PRODUCTOS</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tablab-reg" role="tabpanel" aria-labelledby="tablab-reg-tab">
                                <fieldset class="scheduler-border-fsc" id="regCoti">
                                    <legend class="scheduler-border-fsc text-primary">DATOS DEL SERVICIO</legend>
                                    <form class="form-horizontal" id="frmsetservicio" name="frmsetservicio" action="<?= base_url('lab/resultados/cregresult/setservicio')?>" method="POST" enctype="multipart/form-data" role="form">       
                                    <input type="hidden" name="txtidcotizacion" class="form-control" id="txtidcotizacion">
                                    <input type="hidden" name="txtnroversion" class="form-control" id="txtnroversion">
                                    <input type="hidden" name="txtidordenservicio" class="form-control" id="txtidordenservicio">
                                    <input type="hidden" name="txttipoingreso" class="form-control" id="txttipoingreso">
                                    <input type="hidden" name="hdconcc" class="form-control" id="hdconcc">
                                    <input type="hidden" name="txtzctipocerticalidad" class="form-control" id="txtzctipocerticalidad">
                                    <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="text-info">Cliente</div>
                                            <div>
                                                <input type="text" id="txtcliente" name="txtcliente" class="form-control" disabled = true/>
                                            </div>                                            
                                        </div>
                                        <div class="col-md-2">
                                            <div class="text-info">Cotizacion</div>
                                            <div>
                                                <input type="text" id="txtcotizacion" name="txtcotizacion" class="form-control" disabled = true/>
                                            </div> 
                                        </div>
                                        <div class="col-md-2">
                                            <div class="text-info">Fecha Coti.</div>
                                            <div>
                                                <input type="text" id="txtfcotizacion" name="txtfcotizacion" class="form-control" disabled = true/>
                                            </div> 
                                        </div>
                                        <div class="col-md-2">
                                            <div class="text-info">OT</div>
                                            <div>
                                                <input type="text" id="txtnroot" name="txtnroot" class="form-control" disabled = true/>
                                            </div> 
                                        </div>
                                        <div class="col-md-2">
                                            <div class="text-info">Fecha OT</div>
                                            <div>
                                                <input type="text" id="txtfot" name="txtfot" class="form-control" disabled = true/>
                                            </div> 
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="text-info">Fecha Analisis</div>
                                            <div class="input-group date" id="mtxtFanalisis" data-target-input="nearest">
                                            <input type="text" id="mtxtFanali" name="mtxtFanali" class="form-control datetimepicker-input" data-target="#mtxtFanalisis"/>
                                            <div class="input-group-append" data-target="#mtxtFanalisis" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                            </div>
                                        </div>  
                                        <div class="col-md-2">
                                            <div class="text-info">Hora Analisis</div>
                                            <div class="input-group date" id="mtxtHanalisis" data-target-input="nearest">
                                            <input type="text" id="mtxtHanali" name="mtxtHanali" class="form-control datetimepicker-input" data-target="#mtxtHanalisis"/>
                                            <div class="input-group-append" data-target="#mtxtHanalisis" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="far fa-clock"></i></div>
                                            </div>
                                            </div>
                                        </div>  
                                        <div class="col-md-5">
                                            <div class="text-info">Observación</div>
                                            <div>
                                                <input type="text" id="txtobserva" name="txtobserva" class="form-control" />
                                            </div> 
                                        </div>
                                        <div class="col-md-3">
                                            <div class="text-info">&nbsp;</div>
                                            <div>
                                                <button type="button" class="btn btn-primary" id="btnObservamuestra"><i class="fas fa-atlas"></i> Obs. por muestras</button>
                                            </div> 
                                        </div> 
                                    </div>
                                    <br>
                                    <div class="row" style="background-color: #dff0d8;">                                                         
                                        <div class="col-6 text-left">
                                            <button type="button" class="btn btn-secondary" id="btnRetornarLista"><i class="fas fa-undo-alt"></i> Retornar</button>
                                        </div>                                                          
                                        <div class="col-6 text-right">                                            
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-info" id="btnverinformes"><i class="fas fa-clipboard-list"></i> Formatos</button>
                                                <button type="button" class="btn btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                                                <span class="sr-only">Toggle Dropdown</span>
                                                <div class="dropdown-menu" role="menu">
                                                    <a class="dropdown-item" title="Preliminar" onclick="pdfInfensayoPreliminar();" class="pull-left"><span class="fas fa-file-pdf" aria-hidden="true">&nbsp;</span>&nbsp;Preliminar</a>
                                                    <a class="dropdown-item" title="Inf. Vista Previa" onclick="pdfInfensayoPrevia();" class="pull-left"><span class="fas fa-file-pdf" aria-hidden="true">&nbsp;</span>&nbsp;Inf. Vista Previa</a>
                                                    <a id="aprevcerti" class="dropdown-item" title="Cert. Vista Previa" onclick="pdfCerticalidadPrevia();" class="pull-left"><span class="fas fa-file-pdf" aria-hidden="true">&nbsp;</span>&nbsp;Cert. Vista Previa</a>
                                                </div>
                                                </button>
                                            </div>
                                            <button type="button" class="btn btn-secondary" id="btnaddNormas"><i class="fas fa-atlas"></i> Asociar Normas</button>
                                            <button type="button" class="btn btn-outline-secondary" id="btnLCmuestra"><i class="fas fa-file-contract"></i> L.C.</button>
                                            <button type="submit" form="frmsetservicio" class="btn btn-success" id="btngrabarResult"><i class="fas fa-clipboard-list"></i> Grabar</button>
                                        </div>    
                                    </div>
                                    </div>
                                    </form>
                                </fieldset>
                                <fieldset class="scheduler-border-fsc" id="regCoti">
                                    <legend class="scheduler-border-fsc text-primary">INGRESO RESULTADOS</legend>
                                    <div class="form-group">
                                        <div class="row"> 
                                            <div class="col-12">
                                                <div class="card card-outline card-primary">
                                                    <div class="card-header">
                                                        <h3 class="card-title">Listado de Resultados</h3>
                                                    </div> 
                                                    <div class="card-footer">  
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <div class="text-info">Area de Servicio</div>
                                                                    <select class="form-control select2bs4" id="cboareaserv" name="cboareaserv">
                                                                        <option value="%" selected="selected">Todos</option>
                                                                        <option value="MICRO">Microbiología</option>
                                                                        <option value="FQ">Fisicoquímica</option>
                                                                        <option value="INSTRU">Instrumental</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="text-info">Tipo ensayo</div>
                                                                <div>
                                                                    <select class="form-control select2bs4" id="mcbotipoensayo" name="mcbotipoensayo">
                                                                        <option value="%" selected="selected">Elegir...</option>
                                                                    </select>
                                                                </div>
                                                            </div> 
                                                            <div class="col-md-2">
                                                                <div class="text-info">Acreditado</div>
                                                                <div>
                                                                    <select class="form-control select2bs4" id="mcboacreditado" name="mcboacreditado">
                                                                        <option value="%" selected="selected">Elegir...</option>
                                                                        <option value="A">Acreditado</option>
                                                                        <option value="N">No Acreditado</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 text-left" >
                                                                <div class="text-info">&nbsp</div>
                                                                <div>
                                                                    <button type="button" class="btn btn-success btn-circle-s" id="btnupdate"><i class="fas fa-sync-alt"></i></button>
                                                                </div>
                                                            </div> 
                                                        </div>
                                                    </div>                                                                                           
                                                    <div class="card-body" style="overflow-x: scroll;">
                                                        <div class="row">
                                                            <div class="col-12 table-responsive" id="divtblListResultados"> 
                                                            <table id="tblListResultados" class="table table-striped table-bordered compact" style="width:100%">
                                                                <thead>
                                                                <tr>
                                                                    <th style="min-width: 20px; width: 20px;">#</th>
                                                                    <th></th>
                                                                    <th style="min-width: 110px; width: 110px;">Cod. Ensayo</th>
                                                                    <th style="min-width: 150px; width: 150px;">Ensayo</th>
                                                                    <th style="min-width: 100px; width: 100px;">Unidad</th>
                                                                    <th style="min-width: 50px; width: 50px;"></th>
                                                                    <th style="min-width: 60px; width: 60px;">Especif.</th>
                                                                    <th style="min-width: 10px; width: 10px;">x10</th>
                                                                    <th style="min-width: 50px; width: 50px;"></th>
                                                                    <th style="min-width: 60px; width: 60px;">Resultados</th>
                                                                    <th style="min-width: 10px; width: 10px;">x10</th>
                                                                    <th style="min-width: 100px; width: 100px;">Observacion</th>
                                                                    <th style="min-width: 100px; width: 100px;">Conclusion</th>
                                                                    <th style="min-width: 30px; width: 30px;">Selec.</th>
                                                                    <th style="min-width: 30px; width: 30px;">VB</th>
                                                                    <th></th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                            </div> 
                                                            <div class="col-12" id="divtblListResultadosold"> 
                                                            <table id="tblListResultadosold" class="table table-striped table-bordered compact" style="width:100%">
                                                                <thead>
                                                                <tr>
                                                                    <th style="min-width: 20px; width: 20px;">#</th>
                                                                    <th></th>
                                                                    <th style="min-width: 120px; width: 120px;">Cod. Ensayo</th>
                                                                    <th style="min-width: 220px; width: 220px;">Ensayo</th>
                                                                    <th style="min-width: 150px; width: 150px;">Unidad</th>
                                                                    <th style="min-width: 80px; width: 80px;">Resultados</th>
                                                                    <th style="min-width: 50px; width: 50px;">x10</th>
                                                                    <th style="min-width: 50px; width: 50px;">Selec.</th>
                                                                    <th style="min-width: 50px; width: 50px;">VB</th>
                                                                    <th></th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                            </div> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>                
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.Main content -->



<!-- /.modal- Sensorial --> 
<div class="modal fade" id="modalSensorial" data-backdrop="static" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
        <div class="modal-header text-center bg-primary">
            <h4 class="modal-title w-100 font-weight-bold">Registro de Resultados</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        <div class="modal-body">
            <input type="hidden" name="mhdncinternoordenservicio" id="mhdncinternoordenservicio" class="form-control">
            <input type="hidden" name="mhdncinternocotizacion" id="mhdncinternocotizacion" class="form-control"> 
            <input type="hidden" name="mhdnnordenproducto" id="mhdnnordenproducto" class="form-control">
            <input type="hidden" name="mhdncmuestra" id="mhdncmuestra" class="form-control"> 
            <input type="hidden" name="mhdncensayo" id="mhdncensayo" class="form-control"> 
            <input type="hidden" name="mhdnnviausado" id="mhdnnviausado" class="form-control">
            <input type="hidden" name="mhdsconcc" class="form-control" id="mhdsconcc"> 
            <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    Agregar <button type="button" class="btn btn-success btn-circle-s" id="btnaddSenso"><i class="fas fa-plus-circle"></i></button>
                                </div> 
                                <div class="card-body">
                                    <div class="table-responsive" id="divtblSensorial">
                                        <table class="table m-0" id="tblSensorial">
                                            <thead>
                                            <tr>
                                                <th></th>
                                                <th>ATRIBUTO</th>
                                                <th>ESPECIFICACION</th>
                                                <th>RESULTADO</th>
                                                <th>CONCLUSION</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                    <div class="table-responsive" id="divtblSensorialold">
                                        <table class="table m-0" id="tblSensorialold">
                                            <thead>
                                            <tr>
                                                <th></th>
                                                <th>ATRIBUTO</th>
                                                <th>RESULTADO</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>   
                                </div>     
                            </div> 
                        </div>  
                    </div>   
            </div>
        </div>
        <div class="modal-footer" style="background-color: #dff0d8;">
            <button type="reset" class="btn btn-default" id="mbtnCSenso" data-dismiss="modal">Cancelar</button>            
        </div>
      
    </div>
  </div>
</div> 
<!-- /.modal-->
<!-- /.modal- Elementos --> 
<div class="modal fade" id="modalElementos" data-backdrop="static" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
        <div class="modal-header text-center bg-primary">
            <h4 class="modal-title w-100 font-weight-bold">Registro de Resultados</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <input type="hidden" name="mhdecinternoordenservicio" id="mhdecinternoordenservicio" class="form-control">
            <input type="hidden" name="mhdecinternocotizacion" id="mhdecinternocotizacion" class="form-control"> 
            <input type="hidden" name="mhdenordenproducto" id="mhdenordenproducto" class="form-control">
            <input type="hidden" name="mhdecmuestra" id="mhdecmuestra" class="form-control"> 
            <input type="hidden" name="mhdecensayo" id="mhdecensayo" class="form-control"> 
            <input type="hidden" name="mhdenviausado" id="mhdenviausado" class="form-control"> 
            <input type="hidden" name="mhdezctipoensayo" id="mhdezctipoensayo" class="form-control"> 
            <input type="hidden" name="mhdeconcc" class="form-control" id="mhdeconcc">
            <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                            <div class="card-header">
                                <div class="row fs--paginate" >
                                    <div class="col-md-8 text-left" >
                                        Recargar <button type="button" class="btn btn-success btn-circle-s" id="btnrecargar"><i class="fas fa-sync-alt"></i></button>
                                    </div>
                                    <div class="col-md-4 text-right" >
                                        <div class="form-group" >
                                            <input type="text" class="form-control form-control-sm fs--paginate-search"
                                            placeholder="Buscar" aria-label=""
                                            value="" >
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <div class="card-body">
                            <div class="table-responsive" id="divtblElementos">
                            <table class="table m-0 tbl-tramites table-striped table-bordered compact" id="tblElementos">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th style="min-width: 150px; width: 150px;">Elementos</th>
                                <th style="min-width: 80px; width: 80px;">Unidad</th>
                                <th style="min-width: 45px; width: 45px;"></th>
                                <th style="min-width: 60px; width: 60px;">Epecif.</th>
                                <th style="min-width: 10px; width: 10px;">X10</th>
                                <th style="min-width: 45px; width: 45px;"></th>
                                <th style="min-width: 60px; width: 60px;">Resultados</th>
                                <th style="min-width: 10px; width: 10px;">X10</th>
                                <th style="min-width: 100px; width: 100px;">Conclusion</th>
                                <th style="min-width: 30px; width: 30px;">Selec.</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                            </table>
                            </div>  
                            <div class="table-responsive" id="divtblElementosold">
                            <table class="table m-0 tbl-tramites table-striped table-bordered compact" id="tblElementosold">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th style="min-width: 250px; width: 250px;">Elementos</th>
                                <th style="min-width: 180px; width: 180px;">Unidad</th>
                                <th style="min-width: 200px; width: 200px;">Resultados</th>
                                <th style="min-width: 40px; width: 40px;">Selec.</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                            </table>
                            </div>   
                            </div>     
                            </div> 
                        </div>  
                    </div>   
            </div>
        </div>

        <div class="modal-footer" style="background-color: #dff0d8;">
            <button type="reset" class="btn btn-default" id="mbtnCElementos" data-dismiss="modal">Cancelar</button>            
        </div>
      
    </div>
  </div>
</div> 
<!-- /.modal-->
<!-- /.modal- Esterilidad --> 
<div class="modal fade" id="modalEsterilidad" data-backdrop="static" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
        <div class="modal-header text-center bg-primary">
            <h4 class="modal-title w-100 font-weight-bold">Registro de Resultados</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        <div class="modal-body">
        <form class="form-horizontal" id="frmRegEsterilidad" action="<?= base_url('lab/resultados/cregresult/setesterilidad')?>" method="POST" enctype="multipart/form-data" role="form">
            <input type="hidden" name="mhdnscinternoordenservicio" id="mhdnscinternoordenservicio" class="form-control">
            <input type="hidden" name="mhdnscinternocotizacion" id="mhdnscinternocotizacion" class="form-control"> 
            <input type="hidden" name="mhdnsnordenproducto" id="mhdnsnordenproducto" class="form-control">
            <input type="hidden" name="mhdnscmuestra" id="mhdnscmuestra" class="form-control"> 
            <input type="hidden" name="mhdnscensayo" id="mhdnscensayo" class="form-control"> 
            <input type="hidden" name="mhdnsnviausado" id="mhdnsnviausado" class="form-control"> 

            <input type="hidden" name="mhdnsaccionEsteril" id="mhdnsaccionEsteril" class="form-control" value='N'>
                 
            <div class="form-group">
                <div class="row">    
                    <div class="col-md-2">
                        <div class="text-info">Tipo de Acidez</div>
                        <div>
                            <input type="hidden" name="mcbotipoacidez" id="mcbotipoacidez" class="form-control"> 
                            <input type="text" name="mtxttipoacidez" class="form-control" id="mtxttipoacidez" disabled = true/>
                            <!--<select class="form-control select2bs4" id="mcbotipoacidez" name="mcbotipoacidez">
                                <option value="A" selected="selected">Alta acidez (pH<4.6)</option>
                                <option value="B">Baja acidez (pH>4.6)</option>
                            </select>-->
                        </div>
                    </div>  
                    <div class="col-md-3">
                        <div class="text-info">Preincubacion 35°C/14 días</div>
                        <div>  
                            <input type="text" name="mtxtdpreincubacion" class="form-control" id="mtxtdpreincubacion"/>  
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="text-info">pH</div>
                        <div>  
                            <input type="text" name="mtxtph" class="form-control" id="mtxtph"/>  
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="text-info">Resultado</div>
                        <div>
                            <select class="form-control select2bs4" id="mcbosresultado" name="mcbosresultado" disabled = true>
                                <option value="E" selected="selected">Estéril</option>
                                <option value="N">No Estéril</option>
                            </select>
                        </div>
                    </div>  
                    <!--<div class="col-md-2">
                        <div class="text-info">&nbsp;</div>
                        
                    </div>-->
                </div> 
            </div>      
            <fieldset class="scheduler-border-fsc" id="divtblesterilA">             
                <div class="form-group">
                    <div class="row"> 
                        <div class="col-md-6"> 
                            <legend class="scheduler-border-fsc text-primary">Mesófilos (30ºC)</legend>
                        </div> 
                        <div class="col-md-6"> 
                            <legend class="scheduler-border-fsc text-primary">Termófilos (55ºC)</legend> 
                        </div> 
                    </div> 
                    <div class="row">     
                        <div class="col-md-3">
                            <div class="text-info">Caldo ácido 96 h</div>
                            <div>
                                <select class="form-control select2bs4" id="mcbodacmes30ca" name="mcbodacmes30ca">
                                    <option value="0/2" selected="selected">0/2</option>
                                    <option value="1/2">1/2</option>
                                    <option value="2/2">2/2</option>
                                </select>
                            </div>
                        </div>   
                        <div class="col-md-3">
                            <div class="text-info">Caldo extracto de malta 96 h</div>
                            <div>
                                <select class="form-control select2bs4" id="mcbodacmes30cm" name="mcbodacmes30cm">
                                    <option value="0/2" selected="selected">0/2</option>
                                    <option value="1/2">1/2</option>
                                    <option value="2/2">2/2</option>
                                </select>
                            </div>
                        </div>  
                        <div class="col-md-3">
                            <div class="text-info">Caldo ácido 48 h</div>
                            <div>
                                <select class="form-control select2bs4" id="mcbodacter55ca" name="mcbodacter55ca">
                                    <option value="0/2" selected="selected">0/2</option>
                                    <option value="1/2">1/2</option>
                                    <option value="2/2">2/2</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset >  
            <fieldset class="scheduler-border-fsc" id="divtblesterilB">          
                <div class="form-group">
                    <div class="row"> 
                        <div class="col-md-6"> 
                            <legend class="scheduler-border-fsc text-primary">Mesófilos (35ºC)</legend> 
                        </div> 
                        <div class="col-md-6"> 
                        <legend class="scheduler-border-fsc text-primary">Termófilos (55ºC)</legend>
                        </div> 
                    </div> 
                    <div class="row">  
                   
                        <div class="col-md-3">
                            <div class="text-info">Caldo púrpura de bromocresol 120 h</div>
                            <div>
                                <select class="form-control select2bs4" id="mcbodbames35cp" name="mcbodbames35cp">
                                    <option value="0/2" selected="selected">0/2</option>
                                    <option value="1/2">1/2</option>
                                    <option value="2/2">2/2</option>
                                </select>
                            </div>
                        </div>   
                        <div class="col-md-3">
                            <div class="text-info">Caldo carne cocida 72 h</div>
                            <div>
                                <select class="form-control select2bs4" id="mcbodbames35cc" name="mcbodbames35cc">
                                    <option value="0/2" selected="selected">0/2</option>
                                    <option value="1/2">1/2</option>
                                    <option value="2/2">2/2</option>
                                </select>
                            </div>
                        </div>
                    
                        <div class="col-md-3">
                            <div class="text-info">Caldo púrpura de bromocresol 48 h</div>
                            <div>
                                <select class="form-control select2bs4" id="mcbodbater55cp" name="mcbodbater55cp">
                                    <option value="0/2" selected="selected">0/2</option>
                                    <option value="1/2">1/2</option>
                                    <option value="2/2">2/2</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="text-info">Caldo carne cocida 72 h</div>
                            <div>
                                <select class="form-control select2bs4" id="mcbodbater55cc" name="mcbodbater55cc">
                                    <option value="0/2" selected="selected">0/2</option>
                                    <option value="1/2">1/2</option>
                                    <option value="2/2">2/2</option>
                                </select>
                            </div>
                        </div>
                    </div> 
                </div> 
            </fieldset >
        </form>
        </div>

        <div class="modal-footer" style="background-color: #dff0d8;">
            <button type="reset" class="btn btn-default" id="mbtnCSenso" data-dismiss="modal">Cancelar</button> 
            <button type="submit" form="frmRegEsterilidad" class="btn btn-default" id="mbtngabareste" >Grabar</button>            
        </div>
      
    </div>
  </div>
</div> 
<!-- /.modal-->


<!-- /.modal- Asociar Normas --> 
<div class="modal fade" id="modalAddnormas" data-backdrop="static" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
        <div class="modal-header text-center bg-primary">
            <h4 class="modal-title w-100 font-weight-bold">Asociar Normas</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <input type="hidden" name="mhdnNormascordenservicio" id="mhdnNormascordenservicio" class="form-control">
        </div>

        <div class="modal-body">
            <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="table-responsive" id="divtblAddnormas">
                                        <table class="table table-striped table-bordered" id="tblAddnormas">
                                            <thead>
                                            <tr>
                                                <th>MUESTRA</th>
                                                <th>TIPO ENSAYO</th>
                                                <th>PRODUCTO</th>
                                                <th>NORMA</th>
                                                <th>GRUPO</th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div> 
                                      
                                    <div class="table-responsive" id="divtblFindnormas" align="center">
                                        <form class="form-horizontal" id="frmaddnormas" action="" method="POST" enctype="multipart/form-data" role="form">
                                            <input type="hidden" name="hdnasocinternoordenservicio" id="hdnasocinternoordenservicio" class="form-control">
                                            <input type="hidden" name="hdnasonordenproducto" id="hdnasonordenproducto" class="form-control">
                                            <input type="hidden" name="hdnasocmuestra" id="hdnasocmuestra" class="form-control">
                                            <input type="hidden" name="hdnasozctipoinforme" id="hdnasozctipoinforme" class="form-control">
                                            <input type="hidden" name="hdnasozctipoensayo" id="hdnasozctipoensayo" class="form-control">
                                            <input type="hidden" name="hdnasondorden" id="hdnasondorden" class="form-control">
                                            <input type="hidden" name="hdnasoaccion" id="hdnasoaccion" class="form-control">
                                        </form>
                                        <table class="table table-striped table-bordered" id="tblListnormas">
                                            <thead>
                                            <tr>
                                                <th>GRUPO NORMA</th>
                                                <th></th>
                                                <th>NORMA</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div> 
                                </div>     
                            </div> 
                        </div>  
                    </div>   
            </div>
        </div>
        <div class="modal-footer" style="background-color: #dff0d8;">
            <button type="button" class="btn btn-default" id="mbtnCretornarnormas">Retornar</button>    
            <button type="reset" class="btn btn-default" id="mbtnCAddnormas" data-dismiss="modal">Cancelar</button>            
        </div>
      
    </div>
  </div>
</div> 
<!-- /.modal-->


<!-- /.modal- Observaciones por muestras --> 
<div class="modal fade" id="modalObservamuestras" data-backdrop="static" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header text-center bg-primary">
            <h4 class="modal-title w-100 font-weight-bold">Observaciones por muestras</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        <div class="modal-body">
            <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div>
                                        <table class="table table-striped table-bordered compact" id="tblObsmuestra">
                                            <thead>
                                            <tr>
                                                <th>MUESTRA</th>
                                                <th>OBSERVACION</th>
                                                <th>ID</th>
                                            </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>  
                                </div>     
                            </div> 
                        </div>  
                    </div>   
            </div>
        </div>
        <div class="modal-footer" style="background-color: #dff0d8;">
            <button type="reset" class="btn btn-default" id="mbtnCObsmuestra" data-dismiss="modal">Cancelar</button>            
        </div>
      
    </div>
  </div>
</div> 
<!-- /.modal-->
<!-- /.modal- LC por muestras --> 
<div class="modal fade" id="modalLCmuestras" data-backdrop="static" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header text-center bg-primary">
            <h4 class="modal-title w-100 font-weight-bold">LC por muestras</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        <div class="modal-body">
            <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div>
                                        <table class="table table-striped table-bordered compact" id="tblLCmuestra">
                                            <thead>
                                            <tr>
                                                <th>MUESTRA</th>
                                                <th>LC</th>
                                                <th>ID</th>
                                            </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>  
                                </div>     
                            </div> 
                        </div>  
                    </div>   
            </div>
        </div>
        <div class="modal-footer" style="background-color: #dff0d8;">
            <button type="reset" class="btn btn-default" id="mbtnCLCmuestra" data-dismiss="modal">Cancelar</button>            
        </div>
      
    </div>
  </div>
</div> 
<!-- /.modal-->
