<?php
    $idusu = $this -> session -> userdata('s_idusuario');
    $cusuario = $this -> session -> userdata('s_cusuario');
?>

<style>
    tab {
        display: inline-block; 
        margin-left: 30px; 
    }
    tr.subgroup,
    tr.subgroup:hover {
        background-color: #F2F2F2 !important;
        /* color: blue; */
        font-weight: bold;
    }
    .group{
            background-color:#D5D8DC !important;
            font-size:15px;
            color:#000000!important;
            opacity:0.7;
    }
    .subgroup{
        cursor: pointer;
    }
    .modal-lg{
        max-width: 1000px !important;
    }
</style>

<!-- content-header -->
<div class="content-header">   
<div class="container-fluid">
<div class="row mb-2">
  <div class="col-sm-6">
    <h1 class="m-0 text-dark">MANTENIMIENTO ENSAYOS</h1>
  </div>
  <div class="col-sm-6">
    <ol class="breadcrumb float-sm-right">
      <li class="breadcrumb-item"><a href="<?php echo public_base_url(); ?>main">Home</a></li>
      <li class="breadcrumb-item active">Laboratorio</li>
    </ol>
  </div>
</div>
</div>
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content" id="contenedorBusqueda">
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-12">
                <div class="card card-lightblue card-outline card-tabs">
                    <div class="card-header p-0 pt-1 border-bottom-0">            
                        <ul class="nav nav-tabs tabfsc" id="tablabensayo" role="tablist">                    
                            <li class="nav-item">
                                <a class="nav-link active" style="color: #000000;" id="tablabensayo-list-tab" data-toggle="pill" href="#tablabensayo-list" role="tab" aria-controls="tablabensayo-list" aria-selected="true">LISTADO</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" style="color: #000000;" id="tablabensayo-reg-tab" data-toggle="pill" href="#tablabensayo-reg" role="tab" aria-controls="tablabensayo-reg" aria-selected="false">REGISTRO</a>
                            </li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content" id="tablabensayo-tabContent">
                            <div class="tab-pane fade show active" id="tablabensayo-list" role="tabpanel" aria-labelledby="tablabensayo-list-tab">                            
                                <div class="card card-lightblue">
                                    <div class="card-header">
                                        <h3 class="card-title">BUSQUEDA</h3>
                                        <div class="card-tools">
                                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                        </div>
                                    </div>                                
                                    <div class="card-body">
                                        <div class="row">                
                                            <div class="col-md-4">
                                                <div class="form-group"> 
                                                    <label>Ensayo</label>
                                                    <input type="text" class="form-control" id="txtensayo" name="txtensayo">
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                                
                                    <div class="card-footer justify-content-between"> 
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="text-right">
                                                    <button type="submit" class="btn btn-primary" id="btnBuscar"><i class="fas fa-search"></i> Buscar</button>    
                                                    <button type="button" class="btn btn-outline-info" id="btnNuevo"><i class="fas fa-plus"></i> Crear Nuevo</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card card-outline card-lightblue">                                        
                                            <div class="card-body">
                                                <table id="tblListEnsayo" class="table table-striped table-bordered compact" style="width:100%">
                                                    <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th>Código</th>
                                                        <th>Descripción</th>
                                                        <th>Tipo</th>
                                                        <th>AC/NoAC</th>
                                                        <th>Estado</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>                            
                            </div>
                            <div class="tab-pane fade" id="tablabensayo-reg" role="tabpanel" aria-labelledby="tablabensayo-reg-tab">
                                <div class="row">
                                    <div class="col-12">
                                        <fieldset class="scheduler-border">
                                            <legend class="scheduler-border text-primary">Datos del Ensayo</legend>
                                            <div class="card card-lightblue">
                                                <div class="card-body">
                                                    <div class="form-group">
                                                    <form class="form-horizontal" id="frmMantensayo" action="<?= base_url('lab/mante/cmantensayo/setensayo')?>" method="POST" enctype="multipart/form-data" role="form">
                                                        <input type="hidden" id="hdnIdcensayo" name="hdnIdcensayo"> <!-- ID -->
                                                        <input type="hidden" name="hdnAccregensayo" id="hdnAccregensayo" class="form-control">
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <div class="text-info">Ensayo</div>
                                                                <div>  
                                                                    <select id="cboSTipoensayo" name="cboSTipoensayo" class="form-control" style="width: 100%;">
                                                                        <option value = "ME" selected="selected">ME</option>
                                                                        <option value = "EX">EX</option>
                                                                    </select>
                                                                 </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="text-info">Codigo FS</div>
                                                                <div>
                                                                    <input type="text" class="form-control" name="txtCodigofs" id="txtCodigofs">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="text-info">Version</div>
                                                                <div>  
                                                                    <select id="cboSVersion" name="cboSVersion" class="form-control" style="width: 100%;">
                                                                        <option value = "1" selected="selected">NUEVO</option>
                                                                        <option value = "0">ANTIGUO</option>
                                                                    </select>
                                                                 </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="text-info">Tipo Ensayo</div>
                                                                <div>  
                                                                    <select id="cboZTipoensayo" name="cboZTipoensayo" class="form-control" style="width: 100%;">
                                                                        <option value = "" selected="selected"></option>
                                                                    </select>
                                                                 </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="text-info">AC/NO</div>
                                                                <div>  
                                                                    <select id="cbosacnoac" name="cbosacnoac" class="form-control" style="width: 100%;">
                                                                        <option value = "A" selected="selected">AC</option>
                                                                        <option value = "N">NO AC</option>
                                                                    </select>
                                                                 </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-7">
                                                                <div class="text-info">Descripcion</div>
                                                                <div>
                                                                    <input type="text" class="form-control" name="txtDensayo" id="txtDensayo">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="text-info">UM</div>
                                                                <div>
                                                                    <input type="text" class="form-control" name="txtUM" id="txtUM">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="text-info">Valor VR</div>
                                                                <div>
                                                                    <input type="number" class="form-control" name="txtValorvr" id="txtValorvr" placeholder="0.00" min="0.00">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-10">
                                                                <div class="text-info">Titulo Norma</div>
                                                                <div>
                                                                    <textarea type="text" class="form-control" name="txtTituloNorma" id="txtTituloNorma" rows="2"></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="text-info">Año</div>
                                                                <div>
                                                                    <input type="text" class="form-control" name="txtAniopubli" id="txtAniopubli">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="text-info">Norma</div>
                                                                <div>
                                                                    <textarea type="text" class="form-control" name="txtDnorma" id="txtDnorma" rows="3"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    </div>
                                                </div>
                                                <div class="card-footer"> 
                                                    <div class="row">
                                                        <div class="col-md-12 text-right"> 
                                                            <button type="submit" form="frmMantensayo" class="btn btn-success" id="btnGrabar"><i class="fas fa-save"></i> Grabar</button>    
                                                            <button type="button" class="btn btn-secondary" id="btnRetornarList"><i class="fas fa-undo-alt"></i> Retornar</button>
                                                        </div>
                                                    </div>
                                                </div> 
                                            </div> 
                                        </fieldset>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="divDetalle">
                                    <div class="col-md-6">
                                        <fieldset class="scheduler-border">
                                            <legend class="scheduler-border text-primary">Costos por Laboratorio</legend>
                                            <div class="card card-outline card-lightblue">
                                            <div class="card-header text-right">                                                
                                                <button type="button" class="btn btn-success btn-circle-s" id="btnaddLab"><i class="fas fa-plus-circle"></i></button>
                                                <button type="button" class="btn btn-info btn-circle-s" id="btncancelLab"><i class="far fa-arrow-alt-circle-left"></i></button>
                                                <button type="submit" form="frmMantlab" class="btn btn-success btn-circle-s" id="btnsaveLab"><i class="far fa-save"></i></button>
                                            </div>                                        
                                            <div class="card-body" style="overflow-x: scroll;">                                                
                                                <div class="form-group" id="divregLaboratorio">
                                                    <form class="form-horizontal" id="frmMantlab" action="<?= base_url('lab/mante/cmantensayo/setlaboratorio')?>" method="POST" enctype="multipart/form-data" role="form">
                                                        <input type="hidden" id="hdnlabccensayo" name="hdnlabccensayo"> <!-- ID -->
                                                        <div class="row">
                                                            <div class="col-md-9">
                                                                <div class="text-info">Laboratorio</div>
                                                                <div class="input-group input-group-select-button">
                                                                    <select class="form-control select2bs4" id="cboreglab" name="cboreglab" style="width: 100%;">
                                                                        <option value="" selected="selected">Cargando...</option>
                                                                    </select>
                                                                    <div class="input-group-addon input-group-button">
                                                                        <button type="button" role="button" class="btn btn-outline-info" id="btnnewlab" data-toggle="modal" data-target="#modalReglab"><i class="fas fa-plus-circle"></i></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="text-info">Costo</div>
                                                                <div>
                                                                    <input type="number" class="form-control" name="txtlabcosto" id="txtlabcosto" placeholder="0.00" min="0.00">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                <table id="tblListLaboratorio" class="table table-striped table-bordered compact" style="width:100%">
                                                    <thead><tr>
                                                        <th>Laboratorio</th>
                                                        <th>Costo</th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr></thead><tbody></tbody>
                                                </table>
                                            </div>
                                            </div>                                         
                                        </fieldset>
                                    </div>
                                    <div class="col-md-6">
                                        <fieldset class="scheduler-border">
                                            <legend class="scheduler-border text-primary">Matriz</legend>
                                            <div class="card card-outline card-lightblue">
                                            <div class="card-header text-right">                                                
                                            <button type="button" class="btn btn-success btn-circle-s" id="btnaddMatriz"><i class="fas fa-plus-circle"></i></button>
                                            <button type="button" class="btn btn-info btn-circle-s" id="btncancelMatriz"><i class="far fa-arrow-alt-circle-left"></i></button>
                                            <button type= type="submit" form="frmMantmatriz" class="btn btn-success btn-circle-s" id="btnsaveMatriz"><i class="far fa-save"></i></button>
                                            </div>                                        
                                            <div class="card-body" style="overflow-x: scroll;">                                               
                                                <div class="form-group" id="divregMatriz">
                                                    <form class="form-horizontal" id="frmMantmatriz" action="<?= base_url('lab/mante/cmantensayo/setmatriz')?>" method="POST" enctype="multipart/form-data" role="form">
                                                        <input type="hidden" id="hdnmatrizccensayo" name="hdnmatrizccensayo"> <!-- ID -->
                                                        <input type="hidden" id="hdnmatrizcdensayo" name="hdnmatrizcdensayo">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="text-info">Descripcion</div>
                                                                <div>
                                                                    <input type="text" class="form-control" name="txtdescmatriz" id="txtdescmatriz">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                <table id="tblListMatriz" class="table table-striped table-bordered compact" style="width:100%">
                                                    <thead><tr>
                                                        <th>Descripción</th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr></thead><tbody></tbody>
                                                </table>   
                                            </div>
                                            </div>                                      
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.Main content -->

<!-- /.modal-Mante Laboratorio --> 
<div class="modal fade" id="modalReglab" role="dialog" tabindex="-1" data-backdrop="static" data-keyboard="false" aria-labelledby="exampleModalLabel" aria-hidden="true" >
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
        <div class="modal-header text-center bg-success">
            <h4 class="modal-title w-100 font-weight-bold">Registrar Laboratorio</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body"> 
        <form class="form-horizontal" id="frmReglab" name="frmReglab" action="<?= base_url('lab/mante/cmantensayo/setreglab')?>" method="POST" enctype="multipart/form-data" role="form"> 
            <input type="hidden" id="mhdnidlaboratorio" name="mhdnidlaboratorio">
            <input type="hidden" id="mhdnAccionlaboratorio" name="mhdnAccionlaboratorio">
            <div class="form-group">        
                <div class="row">
                    <div class="col-md-12"> 
                        <div class="text-info">Laboratorio <span class="text-requerido">*</span></div>
                        <div>    
                            <input type="text" name="txtdeslaboratorio"id="txtdeslaboratorio" class="form-control"><!-- disable -->
                        </div>
                    </div>   
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer w-100 d-flex flex-row" style="background-color: #D4EAFC;">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-right">
                        <button type="reset" class="btn btn-default" id="mbtnCMantlaboratorio" data-dismiss="modal">Cancelar</button>
                        <button type="submit" form="frmReglab" class="btn btn-info" id="mbtnGMantlaboratorio">Grabar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</div> 
<!-- /.modal-->


<!-- Script Generales -->
<script type="text/javascript">
    var baseurl = "<?php echo base_url();?>"; 
</script>
