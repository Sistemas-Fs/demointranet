<?php
    $idusu = $this -> session -> userdata('s_idusuario');
    $codusu = $this -> session -> userdata('s_cusuario');
    $infousuario = $this->session->userdata('s_infodato');
?>

<style>
</style>

<!-- content-header -->
<div class="content-header">   
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">PROCESAMIENTO CONVENCIONAL</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?php echo public_base_url(); ?>cprincipal/principalClie">Home</a></li>
          <li class="breadcrumb-item active">Gestion Procesos Termicos</li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content" style="background-color: #E0F4ED;">
    <div class="container-fluid">
        <form class="form-horizontal" id="frmexportarexcel" name="frmexportarexcel" action="<?= base_url('pt/cservcliente/excelproconvequipo')?>" method="POST" enctype="multipart/form-data" role="form"> 
        <input type="hidden" id="hdnccliente" name="hdnccliente" value="<?php echo $ccliente ?>">
        </form>
        <form class="form-horizontal" id="frmexportarexcel_a" name="frmexportarexcel" action="<?= base_url('pt/cservcliente/excelproconvproducto')?>" method="POST" enctype="multipart/form-data" role="form"> 
        <input type="hidden" id="hdnccliente_a" name="hdnccliente_a" value="<?php echo $ccliente ?>">
        </form>
        <div class="row">
            <div class="col-12">
                <div class="card card-outline card-success">
                    <div class="card-header">
                        <h3 class="card-title">ESTUDIO DE DISTRIBUCIÓN DE TEMPERATURA</h3>
                        <div class="col-md-12 text-right" >
                            Excel <button type="submit" form="frmexportarexcel" class="btn btn-success btn-circle-s" id="btnExcelproconvequipo"><i class="fas fa-file-excel"></i></button>
                        </div>
                    </div>    
                    <div id="contenido" class="card-body" style="overflow-x: scroll;">
                        <table id="tblListEquipos" class="table table-striped table-bordered compact" style="width:100%">
                            <thead>
                            <tr>
                                <th>TIPO</th>
                                <th></th>
                                <th>NRO INFORME</th>
                                <th>MEDIO CALENTAMIENTO</th>
                                <th>FABRICANTE</th>
                                <th>ENVASE</th>
                                <th>DIMENSIONES</th>
                                <th>#ID ESTUDIO</th>
                                <th>Nro EQUIPOS</th>
                                <th>NRO CANASTILLAS</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-outline card-success">
                    <div class="card-header">
                        <h3 class="card-title">ESTUDIO DE PENETRACIÓN DE CALOR</h3>
                        <div class="col-md-12 text-right" >
                            Excel <button type="submit" form="frmexportarexcel_a" class="btn btn-success btn-circle-s" id="btnExcelproconvproducto"><i class="fas fa-file-excel"></i></button>
                        </div>
                    </div>                
                    <div class="card-body" style="overflow-x: scroll;">
                        <table id="tblListProductos" class="table table-striped table-bordered compact" style="width:100%">
                            <thead>
                            <tr>
                                <th>TIPO</th>
                                <th></th>
                                <th>NRO INFORME</th>
                                <th>NOMBRE</th>
                                <th>ENVASE</th>
                                <th>NOMBRE COMUN</th>
                                <th>DIMENSIONES</th>
                                <th>TIPO DE EQUIPO</th>
                                <th>FABRICANTE DEL EQUIPO</th>
                                <th>#ID EQUIPO</th>
                                <th>PROCAL</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div> 
    </div>
</section>
<!-- /.Main content -->

<script type="text/javascript">
    var baseurl = "<?php echo base_url();?>";   
</script>