<?php
    $idusu = $this -> session -> userdata('s_idusuario');
    $codusu = $this -> session -> userdata('s_cusuario');
    $infousuario = $this->session->userdata('s_infodato');
     
?>

<style>
</style>

<!-- content-header -->
<div class="content-header">   
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">LLENADO EN CALIENTE - FRIO</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?php echo public_base_url(); ?>main">Home</a></li>
          <li class="breadcrumb-item active">Gestion Procesos Termicos</li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content" style="background-color: #E0F4ED;">
    <div class="container-fluid"> 
        <form class="form-horizontal" id="frmexportarexcel" name="frmexportarexcel" action="<?= base_url('pt/cservcliente/excelcalfrioproducto')?>" method="POST" enctype="multipart/form-data" role="form"> 
        <input type="hidden" id="hdnccliente" name="hdnccliente" value="<?php echo $ccliente ?>">
        </form>
        <div class="row">
            <div class="col-12">
                <div class="card card-outline card-success">
                    <div class="card-header">
                        <h3 class="card-title">PRODUCTOS</h3>
                        <div class="col-md-12 text-right" >
                            Excel <button type="submit" form="frmexportarexcel" class="btn btn-success btn-circle-s" id="btnExcelcalfrioproducto"><i class="fas fa-file-excel"></i></button>
                        </div>
                    </div>                
                    <div class="card-body">
                        <table id="tblListProductos" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                            <tr>
                                <th>TIPO</th>
                                <th></th>
                                <th>NRO INFORME</th>
                                <th>NOMBRE</th>
                                <th>ENVASE</th>
                                <th>DIMENSIONES</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.Main content -->

<script type="text/javascript">
    var baseurl = "<?php echo base_url();?>"; 
</script>