<?php
    $idusuario = $this -> session -> userdata('s_idusuario');
    $cusuario = $this -> session -> userdata('s_cusuario');
?>

<style>
    .bootstrap-switch .bootstrap-switch-handle-off.bootstrap-switch-default, .bootstrap-switch .bootstrap-switch-handle-on.bootstrap-switch-default {
        background: red !important;
        color: white !important;
    }
</style>

<!-- content-header -->
<div class="content-header">   
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><i class="fas fa-address-book"></i> REGISTRO DE USUARIO - CLIENTES</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?php echo public_base_url(); ?>main">Home</a></li>
          <li class="breadcrumb-item active">Procesos Termicos</li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">  
        <div class="card card-success">
            <div class="card-header">
                <h3 class="card-title"><b>BUSCAR CLIENTES USUARIOS</b></h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                </div>
            </div>
          
            <div class="card-body">
                <input type="hidden" class="form-control" id="hdnIdUsuario" name="hdnIdUsuario" value="<?php echo $idusuario ?>">
            </div>                
                        
            <div class="card-footer justify-content-between"> 
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-right">
                            <button type="button" class="btn btn-primary" id="btnBuscar"><i class="fas fa-search"></i> Buscar</button>    
                            <button type="button" role="button" class="btn btn-outline-info" id="btnNuevo" data-toggle="modal" data-target="#modalUsuarios"><i class="fas fa-plus"></i> Crear Nuevo</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-outline card-success">
                    <div class="card-header">
                        <h3 class="card-title"><b>LISTADO DE CLIENTES USUARIOS</b></h3>
                    </div>
                
                    <div class="card-body">
                        <table id="tblListclieusuario" class="table table-striped table-bordered compact" style="width:100%">
                            <thead>
                            <tr>
                                <th>NRO</th>
                                <th>NOMBRE</th>
                                <th>EMAIL ACCESO</th>
                                <th>CLIENTE</th>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    
    </div>
</section>
<!-- /.Main content -->


<!-- /.modal-Mante cliente usuario --> 
<div class="modal fade" id="modalUsuarios" role="dialog" tabindex="-1" data-backdrop="static" data-keyboard="false" aria-labelledby="exampleModalLabel" aria-hidden="true" >
  <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
    <div class="modal-content">
        <div class="modal-header text-center bg-success">
            <h4 class="modal-title w-100 font-weight-bold">Mantenimiento Clientes Usuarios</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body"> 
        <form class="form-horizontal" id="frmMantclieusu" name="frmMantclieusu" action="<?= base_url('pt/cclieusuarios/setregclieusu')?>" method="POST" enctype="multipart/form-data" role="form"> 
            <input type="hidden" id="mhdnidusuario" name="mhdnidusuario">
            <input type="hidden" id="mhdnidadministrado" name="mhdnidadministrado">
            <input type="hidden" id="mhdnAccion" name="mhdnAccion">  
            <div class="form-group">  
                <div class="row">
                    <div class="col-6">
                        <h4><i class="fas fa-user-tie"></i> Datos <small> Personales</small></h4>
                    </div> 
                </div> 
                <div class="row">
                    <div class="col-md-3"> 
                        <div class="text-info">Nro Documento<span class="text-requerido">*</span></div>
                        <div>    
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <span id="btntipodoc">DNI</span>
                                    </button>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" onClick="javascript:DNI()">DNI</a>
                                        <a class="dropdown-item" onClick="javascript:EXT()">CExt</a>
                                    </div>
                                    <input type="hidden" id="mhdntipodoc" name="mhdntipodoc">  
                                </div>
                                <input type="text" name="mtxtnrodoc"id="mtxtnrodoc" class="form-control" >
                            </div> 
                        </div>
                    </div> 
                    <div class="col-md-3"> 
                        <div class="text-info">Apellido Paterno</div>
                        <div>    
                            <input type="text" name="mtxtapepat"id="mtxtapepat" class="form-control" ><!-- disable -->
                        </div>
                    </div> 
                    <div class="col-md-3"> 
                        <div class="text-info">Apellido Materno</div>
                        <div>    
                            <input type="text" name="mtxtapemat"id="mtxtapemat" class="form-control" ><!-- disable -->
                        </div>
                    </div> 
                    <div class="col-md-3"> 
                        <div class="text-info">Nombres</div>
                        <div>    
                            <input type="text" name="mtxtnombres"id="mtxtnombres" class="form-control" ><!-- disable -->
                        </div>
                    </div>         
                </div>                  
            </div>    
            
            <div style="border-top: 1px solid #ccc; padding-top: 10px;">     
            <div class="form-group">  
                <div class="row">
                    <div class="col-6">
                        <h4><i class="far fa-id-card"></i> Informacion <small> de Acceso </small></h4>                        
                    </div>
                </div>    
                <div class="row">
                    <div class="col-md-4"> 
                        <div class="text-info">Email de Acceso</div>
                        <div>    
                            <input type="text" name="mtxtemail"id="mtxtemail" class="form-control" ><!-- disable -->
                        </div>
                    </div>     
                    <div class="col-md-8">
                        <div class="text-info">Cliente <span class="text-requerido">*</span></div>
                        <div>                            
                            <select class="form-control select2bs4 addcliente" id="mcboClie" name="mcboClie" style="width: 100%;">
                                <option value="" selected="selected">Cargando...</option>
                            </select>
                        </div>
                    </div>                     
                </div>  
                
            </div>         
            </div>
            
        </form>
        </div>
        <div class="modal-footer w-100 d-flex flex-row" style="background-color: #D4EAFC;">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-right">
                        <button type="reset" class="btn btn-default" id="mbtnCMante" data-dismiss="modal">Cancelar</button>
                        <button type="submit" form="frmMantclieusu" class="btn btn-info" id="mbtnGMante">Grabar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</div> 
<!-- /.modal-->
