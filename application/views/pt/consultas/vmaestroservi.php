<?php
    $idusu = $this -> session -> userdata('s_idusuario');
    $codusu = $this -> session -> userdata('s_cusuario');
    $infousuario = $this->session->userdata('s_infodato');
?>

<style>
    .fileUpload {
        position: relative;
        overflow: hidden;
        margin: 0px;
    }
    .fileUpload input.upload {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }
</style>

<!-- content-header -->
<div class="content-header">   
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><i class="fas fa-file-alt"></i> CONSULTA MAESTROS DE SERVICIOS</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?php echo public_base_url(); ?>main">Home</a></li>
          <li class="breadcrumb-item active">Gestion Procesos Termicos</li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">  
        <div class="card card-success">
        
            <div class="card-header">
                <h3 class="card-title">BUSQUEDA</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                </div>
            </div>
          
            <div class="card-body">
            <form class="form-horizontal" id="frmbuscapropu" name="frmbuscapropu" action="<?= base_url('pt/cpropuesta/excelpropu')?>" method="POST" enctype="multipart/form-data" role="form"> 
                <input type="hidden" name="mtxtidusupropu" class="form-control" id="mtxtidusupropu" value="<?php echo $idusu ?>">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Clientes</label>
                            <select class="form-control select2bs4" id="cboClie" name="cboClie" style="width: 100%;">
                                <option value="" selected="selected">Cargando...</option>
                            </select>
                        </div>
                    </div>
                </div>
            </form>
            </div>                
                             
        </div>
        
        <div class="row">
        
                <div class="col-sm-4 col-md-2" id="divmapter" style="display: none">
                    <div class="callout callout-info" style="height: 200px;">
                        <div class="text-center">
                        <img class="profile-user-img img-fluid img-circle" src="<?php echo public_url(); ?>images/procesoTermico.svg" alt="user image">
                        </div>
                        <div class="text-center">
                        <label>
                            <a onclick="procesoTermico()" style="text-decoration: none; color: #007bff; cursor:pointer;">MAPEO TÉRMICO</a>
                        </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-md-2" id="divproconv" style="display: none">
                    <div class="callout callout-info" style="height: 200px;">
                        <div class="text-center">
                        <img class="profile-user-img img-fluid img-circle" src="<?php echo public_url(); ?>images/processConve.svg" alt="user image">
                        </div>
                        <div class="text-center">
                        <label>
                            <a onclick="processConve()" style="text-decoration: none; color: #007bff; cursor:pointer;">PROCESAMIENTO CONVENCIONAL</a>
                        </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-md-2" id="divcalfrio" style="display: none">
                    <div class="callout callout-info" style="height: 200px;"> 
                        <div class="text-center">
                        <img class="profile-user-img img-fluid img-circle" src="<?php echo public_url(); ?>images/calienteFrio.svg" alt="user image">
                        </div>
                        <div class="text-center">
                        <label>
                            <a onclick="calienteFrio()" style="text-decoration: none; color: #007bff; cursor:pointer;">LLENADO EN CALIENTE - FRIO</a>
                        </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-md-2" id="divproacep" style="display: none">
                    <div class="callout callout-info" style="height: 200px;">
                        <div class="text-center">
                        <img class="profile-user-img img-fluid img-circle" src="<?php echo public_url(); ?>images/processAseptico.svg" alt="user image">
                        </div>
                        <div class="text-center">
                        <label>
                            <a onclick="processAseptico()" style="text-decoration: none; color: #007bff; cursor:pointer;">PROCESAMIENTO ASÉPTICO</a>
                        </label>                         
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-md-2" id="divevaldesvi" style="display: none">
                    <div class="callout callout-info" style="height: 200px;">
                        <div class="text-center">
                        <img class="profile-user-img img-fluid img-circle" src="<?php echo public_url(); ?>images/evalDesvia.svg" alt="user image">
                        </div>
                        <div class="text-center">
                        <label>
                            <a onclick="evalDesvia()" style="text-decoration: none; color: #007bff; cursor:pointer;">EVALUACIÓN DE DESVIACIONES</a>
                        </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-md-2" id="divcocsechor" style="display: none">
                    <div class="callout callout-info" style="height: 200px;">
                        <div class="text-center">
                        <img class="profile-user-img img-fluid img-circle" src="<?php echo public_url(); ?>images/cocinaHorno.svg" alt="user image">
                        </div>
                        <div class="text-center">
                        <label>
                            <a onclick="cocinaHorno()" style="text-decoration: none; color: #007bff; cursor:pointer;">COCINADOR-SECADOR-HORNO</a>
                        </label>
                        </div>
                    </div>
                </div>
        </div>
    
    </div>
</section>
<!-- /.Main content -->




<script type="text/javascript">
    var baseurl = "<?php echo base_url();?>"; 
</script>