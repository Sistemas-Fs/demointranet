<?php
    $cia = $this->session->userdata('s_cia');
    $idrol = $this->session->userdata('s_idrol');
?> 

<!-- content-header -->
<div class="content-header">   
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><i class="fas fa-pencil-ruler"></i>CONSULTA SERVICIOS CLIENTES</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?php echo public_base_url(); ?>main">Home</a></li>
            <li class="breadcrumb-item active">Procesos Termicos</li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content" style="background-color: #E0F4ED;">
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-12">
                <div class="card card-success card-outline card-tabs">
                    <div class="card-header p-0 pt-1 border-bottom-0">            
                        <ul class="nav nav-tabs" id="tabptaudi" style="background-color: #28a745;" role="tablist">                    
                            <li class="nav-item">
                                <a class="nav-link active" style="color: #000000;" id="tabptaudi-list-tab" data-toggle="pill" href="#tabptaudi-list" role="tab" aria-controls="tabptaudi-list" aria-selected="true">LISTADO</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" style="color: #000000;" id="tabptaudi-det-tab" data-toggle="pill" href="#tabptaudi-det" role="tab" aria-controls="tabptaudi-det" aria-selected="false">REGISTRO</a>
                            </li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content" id="tabptaudi-tabContent">
                            <div class="tab-pane fade show active" id="tabptaudi-list" role="tabpanel" aria-labelledby="tabptaudi-list-tab">
                                <div class="card card-success">
                                    <div class="card-header">
                                        <h3 class="card-title">BUSQUEDA</h3>
                                        <div class="card-tools">
                                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                        </div>
                                    </div>                        
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Clientes</label>
                                                    <select class="form-control select2bs4" id="cboclieserv" name="cboclieserv" style="width: 100%;">
                                                        <option value="" selected="selected">Cargando...</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">    
                                                <div class="checkbox"><label>
                                                    <input type="checkbox" id="chkFreg" /> <b>Fecha Registro :: Del</b>
                                                </label></div>                        
                                                <div class="input-group date" id="txtFDesde" data-target-input="nearest" >
                                                    <input type="text" id="txtFIni" name="txtFIni" class="form-control datetimepicker-input" data-target="#txtFDesde" disabled/>
                                                    <div class="input-group-append" data-target="#txtFDesde" data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">      
                                                <label>Hasta</label>                      
                                                <div class="input-group date" id="txtFHasta" data-target-input="nearest">
                                                    <input type="text" id="txtFFin" name="txtFFin" class="form-control datetimepicker-input" data-target="#txtFHasta" disabled/>
                                                    <div class="input-group-append" data-target="#txtFHasta" data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Proveedor</label>
                                                    <select class="form-control select2bs4" id="cboproveedor" name="cboproveedor"style="width: 100%;">
                                                        <option value="" selected="selected">Cargando...</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Estado</label>
                                                    <select class="form-control select2bs4" id="cboestado" name="cboestado" multiple="multiple" data-placeholder="Seleccionar" style="width: 100%;">
                                                        <option value="" selected="selected">Cargando...</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer justify-content-between" style="background-color: #E0F4ED;"> 
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="text-left"> 
                                                    <button type="button" class="btn btn-outline-info" id="btnNuevo"><i class="fas fa-plus"></i> Crear Nuevo</button>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="text-right">
                                                    <button type="submit" class="btn btn-primary" id="btnBuscar"><i class="fas fa-search"></i> Buscar</button>    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> 

                                <div class="row">
                                    <div class="col-12">
                                        <div class="card card-outline card-success">
                                            <div class="card-header">
                                                <div class="row">
                                                <div class="col-md-8">
                                                    <h3 class="card-title">Listado de Auditorias</h3>
                                                </div>     
                                                </div>
                                            </div>                                       
                                            <div class="card-body">
                                                <div class="row">
                                                <div class="col-md-12">
                                                <table id="tblListauditoria" class="table table-striped table-bordered compact" style="width:100%">
                                                    <thead>
                                                    <tr>
                                                        <th>Cliente</th>
                                                        <th>Proveedor</th>
                                                        <th>Establecimiento</th>
                                                        <th>Fecha Auditoria</th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                                </div> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                            <div class="tab-pane fade" id="tabptaudi-det" role="tabpanel" aria-labelledby="tabptaudi-det-tab">
                            </div>
                        </div>
                    </div>                
                </div>
            </div>
        </div>     
    </div>
</section>
<!-- /.Main content -->



<section class="content">
    <div class="container-fluid">
        <div class="card card-gray">
            <div class="card-header">
                <h3 class="card-title"> <i class="fas fa-tag"></i> PROCESOS TERMICOS</h3>                
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                </div>
            </div>
            <div class="card-body">
            <input type="hidden" id="hdccliente" value= <?php echo $ccliente; ?> >
            <div class="row">
                <div class="col-sm-4 col-md-2" id="divmapter" style="display: none">
                    <div class="callout callout-info" style="height: 200px;">
                        <div class="text-center">
                        <img class="profile-user-img img-fluid img-circle" src="<?php echo public_url(); ?>images/procesoTermico.svg" alt="user image">
                        </div>
                        <div class="text-center">
                        <label>
                            <a href="<?php echo base_url()?>pt/cservcliente/viewmapter" style="text-decoration: none; color: #007bff;">MAPEO TÉRMICO</a>
                        </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-md-2" id="divproconv" style="display: none">
                    <div class="callout callout-info" style="height: 200px;">
                        <div class="text-center">
                        <img class="profile-user-img img-fluid img-circle" src="<?php echo public_url(); ?>images/processConve.svg" alt="user image">
                        </div>
                        <div class="text-center">
                        <label>
                            <a href="<?php echo base_url()?>pt/cservcliente/viewproconv" style="text-decoration: none; color: #007bff;">PROCESAMIENTO CONVENCIONAL</a>
                        </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-md-2" id="divcalfrio" style="display: none">
                    <div class="callout callout-info" style="height: 200px;"> 
                        <div class="text-center">
                        <img class="profile-user-img img-fluid img-circle" src="<?php echo public_url(); ?>images/calienteFrio.svg" alt="user image">
                        </div>
                        <div class="text-center">
                        <label>
                            <a href="<?php echo base_url()?>pt/cservcliente/viewcalfrio" style="text-decoration: none; color: #007bff;">LLENADO EN CALIENTE - FRIO</a>
                        </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-md-2" id="divproacep" style="display: none">
                    <div class="callout callout-info" style="height: 200px;">
                        <div class="text-center">
                        <img class="profile-user-img img-fluid img-circle" src="<?php echo public_url(); ?>images/processAseptico.svg" alt="user image">
                        </div>
                        <div class="text-center">
                        <label>
                            <a href="<?php echo base_url()?>pt/cservcliente/viewproacep" style="text-decoration: none; color: #007bff;">PROCESAMIENTO ASÉPTICO</a>
                        </label>                         
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-md-2" id="divevaldesvi" style="display: none">
                    <div class="callout callout-info" style="height: 200px;">
                        <div class="text-center">
                        <img class="profile-user-img img-fluid img-circle" src="<?php echo public_url(); ?>images/evalDesvia.svg" alt="user image">
                        </div>
                        <div class="text-center">
                        <label>
                            <a href="<?php echo base_url()?>pt/cservcliente/viewevaldesvi" style="text-decoration: none; color: #007bff;">EVALUACIÓN DE DESVIACIONES</a>
                        </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-md-2" id="divcocsechor" style="display: none">
                    <div class="callout callout-info" style="height: 200px;">
                        <div class="text-center">
                        <img class="profile-user-img img-fluid img-circle" src="<?php echo public_url(); ?>images/cocinaHorno.svg" alt="user image">
                        </div>
                        <div class="text-center">
                        <label>
                            <a href="<?php echo base_url()?>pt/cservcliente/viewcocsechor" style="text-decoration: none; color: #007bff;">COCINADOR-SECADOR-HORNO</a>
                        </label>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->