<?php
    $idusu = $this -> session -> userdata('s_idusuario');
    $codusu = $this -> session -> userdata('s_cusuario');
    $infousuario = $this->session->userdata('s_infodato');
?>

<style>
    .fileUpload {
        position: relative;
        overflow: hidden;
        margin: 0px;
    }
    .fileUpload input.upload {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }
</style>

<!-- content-header -->
<div class="content-header">   
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><i class="fas fa-file-alt"></i> CARTAS</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?php echo public_base_url(); ?>main">Home</a></li>
          <li class="breadcrumb-item active">Gestion Procesos Termicos</li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">  
        <div class="card card-success">
        
            <div class="card-header">
                <h3 class="card-title">BUSQUEDA</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                </div>
            </div>
          
            <div class="card-body">
            <form class="form-horizontal" id="frmbuscapropu" name="frmbuscapropu" action="<?= base_url('pt/cpropuesta/excelpropu')?>" method="POST" enctype="multipart/form-data" role="form"> 
                <input type="hidden" name="mtxtidusupropu" class="form-control" id="mtxtidusupropu" value="<?php echo $idusu ?>">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Clientes</label>
                            <select class="form-control select2bs4" id="cboClie" name="cboClie" style="width: 100%;">
                                <option value="" selected="selected">Cargando...</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">    
                        <div class="checkbox"><label>
                            <input type="checkbox" id="chkFreg" /> <b>Fecha Registro :: Del</b>
                        </label></div>                        
                        <div class="input-group date" id="txtFDesde" data-target-input="nearest" >
                            <input type="text" id="txtFIni" name="txtFIni" class="form-control datetimepicker-input" data-target="#txtFDesde" disabled/>
                            <div class="input-group-append" data-target="#txtFDesde" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">      
                        <label>Hasta</label>                      
                        <div class="input-group date" id="txtFHasta" data-target-input="nearest">
                            <input type="text" id="txtFFin" name="txtFFin" class="form-control datetimepicker-input" data-target="#txtFHasta" disabled/>
                            <div class="input-group-append" data-target="#txtFHasta" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Nro / Detalle Carta</label>
                            <input type="text" class="form-control" id="txtnrodet" name="txtnrodet" placeholder="...">
                        </div>
                    </div>
                </div>
            </form>
            </div>                
                        
            <div class="card-footer justify-content-between"> 
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-right">
                            <button type="button" class="btn btn-primary" id="btnBuscar"><i class="fas fa-search"></i> Buscar</button>    
                            <button type="button" class="btn btn-outline-info" id="btnNuevo" data-toggle="modal" data-target="#modalCreaCarta"><i class="fas fa-plus"></i> Crear Nuevo</button> 
                        </div>
                    </div>
                </div>
            </div>        
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-outline card-success">
                    <div class="card-header">
                        <h3 class="card-title">Listado de Cartas</h3>
                    </div>
                
                    <div class="card-body" style="overflow-x: scroll;">
                        <table id="tblListCarta" class="table table-striped table-bordered compact" style="width:100%">
                            <thead>
                            <tr>
                                <th></th>
                                <th>N° Carta</th>
                                <th>Cliente</th>
                                <th>Detalle Carta</th>
                                <th>Fecha</th>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    
    </div>
</section>
<!-- /.Main content -->

<!-- /.modal-crear-propuesta --> 
<div class="modal fade" id="modalCreaCarta" data-backdrop="static" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">    
        <div class="modal-header text-center bg-success">
            <h4 class="modal-title w-100 font-weight-bold">Registro de Carta</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        <div class="modal-body"> 
        <form class="form-horizontal" id="frmCreaCarta" name="frmCreaCarta" action="<?= base_url('pt/ccartas/setcarta')?>" method="POST" enctype="multipart/form-data" role="form">         
            <input type="hidden" id="mhdnIdCarta" name="mhdnIdCarta"> <!-- ID -->
            <input type="hidden" id="mhdnAccionCarta" name="mhdnAccionCarta" value="">

            <input type="hidden" id="mtxtidusucarta" name="mtxtidusucarta"  value="<?php echo $idusu ?>">
            <input type="hidden" id="mtxtusercarta" name="mtxtusercarta" value="<?php echo $codusu ?>">
            <input type="hidden" id="mtxtinfousuario" name="mtxtinfousuario"  value="<?php echo $infousuario ?>">
                        
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="text-info">Cliente <span class="text-requerido">*</span></div>
                        <div>                            
                            <select class="form-control select2bs4" id="mcboClie" name="mcboClie" style="width: 100%;">
                                <option value="" selected="selected">Cargando...</option>
                            </select>
                        </div>
                    </div> 
                    <div class="col-md-4">                          
                        <div class="checkbox" id="lbchkpropu">
                            <div class="text-info">  
                                <b><input type="checkbox" id="chkPropu" name="chkPropu" ></b> &nbsp;&nbsp;Propuesta
                            </div>
                        </div> 
                        <input type="text" class="form-control" id="txtRegPropu" name="txtRegPropu" disabled>
                        <div id="divRegPropu">
                            <select class="form-control select2bs4" id="cboRegPropu" name="cboRegPropu" style="width: 100%;" >
                                <option value="" selected="selected">Cargando...</option>
                            </select>
                        </div>
                    </div>                     
                    <div class="col-sm-4">
                        <div class="text-info">Fecha Carta</div>
                        <div class="input-group date" id="mtxtFregcarta" data-target-input="nearest">
                            <input type="text" id="mtxtFcarta" name="mtxtFcarta" class="form-control datetimepicker-input" data-target="#mtxtFregcarta"/>
                            <div class="input-group-append" data-target="#mtxtFregcarta" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>                        
                    </div>
                </div>                
            </div> 
            <div class="form-group">
                <div class="row">                
                    <div class="col-sm-6">                             
                        <div class="checkbox" id="lbchkcarta">
                            <div class="text-info">Nro Carta  &nbsp;&nbsp;
                                <b><input type="checkbox" id="chkNroAntiguo" name="chkNroAntiguo" > Antiguo</b>
                            </div>
                        </div>                           
                        <div>                            
                            <input type="text" name="mtxtNrocarta" class="form-control" id="mtxtNrocarta"/> 
                            <span style="color: #b94a48">Ej. 0100-2018/PT/FS</span>
                        </div>
                    </div>   
                    <div class="col-sm-6">  
                        <div class="text-info">Responsable</div> 
                        <div>                            
                            <select class="form-control" id="mcboRespon" name="mcboRespon" >
                                <option value=1>SU-TZE LIU</option>
                                <option value=42>CARLOS VILLACORTA</option>
                                <option value=128>DIEGO LI</option>
                                <option value=64>JOSE OLIDEN</option>
                            </select>
                        </div> 
                    </div>    
                </div>                
            </div> 
            <div class="form-group">
                <div class="row">                
                    <div class="col-sm-12">
                        <div class="text-info">Descripción <span style="color: #FD0202">*</span></div>
                        <div>   
                            <textarea class="form-control" cols="20" data-val="true" data-val-length="No debe superar los 300 caracteres." data-val-length-max="500" id="mtxtDetaCarta" name="mtxtDetaCarta" rows="2" data-val-maxlength-max="500" data-validation="required"></textarea>
                            <span class="help-inline" style="padding-left:0px; color:#999; font-size:0.9em;">Caracteres: 0 / 300</span>    
                        </div> 
                    </div> 
                </div>                
            </div> 
            <div class="form-group">
                <div class="row">                
                    <div class="col-sm-12">
                        <div class="text-info">Archivo</div>                        
                        <div class="input-group">
                            <input class="form-control" type="text" name="mtxtNomarchcarta" id="mtxtNomarchcarta">                            
                            <span class="input-group-append">                                
                                <div class="fileUpload btn btn-secondary">
                                    <span>Subir Archivo</span>
                                    <input type="file" class="upload" id="mtxtArchivocarta" name="mtxtArchivocarta" onchange="escogerArchivo()"/>                      
                                </div> 
                            </span>  
                        </div>
                        <span style="color: red; font-size: 13px;">+ Los archivos deben estar en formato pdf, docx o xlsx y no deben pesar mas de 60 MB</span>                        
                        <input type="hidden" name="mtxtRutacarta" id="mtxtRutacarta">
                        <input type="hidden" name="mtxtarchivo" id="mtxtarchivo">
                        <input type="hidden" name="sArchivo" id="sArchivo" value="N"> 
                    </div> 
                </div>
            </div>
        </form>            
        </div>

        <div class="modal-footer w-100 d-flex flex-row" style="background-color: #dff0d8;">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-right">
                        <button type="reset" class="btn btn-default" id="mbtnCCreaCarta" data-dismiss="modal">Cancelar</button>
                        <button type="submit" form="frmCreaCarta" class="btn btn-info" id="mbtnGCreaCarta">Grabar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</div> 
<!-- /.modal-->

<!-- /.modal-det-documentos --> 
<div class="modal fade" id="modalDetaCarta" data-backdrop="static" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form class="form-horizontal" id="frmDetaCarta" name="frmDetaCarta" action="<?= base_url('pt/ccartas/setdetcartas')?>" method="POST" enctype="multipart/form-data" role="form"> 

        <div class="modal-header text-center bg-success">
            <h4 class="modal-title w-100 font-weight-bold">Documentos de Cartas</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
            
        <div class="modal-body">
            <div class="form-group">
                <div class="row">                
                    <div class="col-sm-12">
                        <input type="hidden" name="mtxtiddetacarta" id="mtxtiddetacarta" class="form-control">
                        <input type="hidden" name="mtxtfechadetacarta" id="mtxtfechadetacarta" class="form-control">
                        <input type="hidden" name="mtxtnrodetacarta" id="mtxtnrodetacarta" class="form-control">
                        <input type="hidden" name="mtxtcantdetacarta" id="mtxtcantdetacarta" class="form-control">
                        <div class="text-info">Archivos</div>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="mtxtDetArchivocarta" name="mtxtDetArchivocarta[]" multiple size="20" onchange="subirDetCarta()">
                            <span style="color: red">+ Los documentos deben estar en formato pdf, docx o xlsx</span> 
                            <br>
                            <span style="color: red">+ Los archivos no deben pesar mas de 60 MB</span> 
                            <label class="custom-file-label" for="mtxtArchivocarta">Escoger Archivo</label>
                        </div>
                    </div>
                </div>
            </div>
            <table id="tblDetacarta" class="table table-striped table-bordered" style="width:100%">
                <thead>
                <tr>
                    <th style="min-width: 5px; max-width:10px;">Nro.</th>       
                    <th>Nombre</th> 
                    <th style="min-width: 50px; max-width: 50px;">Archivo</th>
                    <th style="min-width: 50px; max-width: 50px;"></th>
                </tr>
                </thead> 
            </table>  
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-outline-info" id="btnnewguia" name="btnnewguia" data-toggle="modal" data-target="#modalNewDetaCarta"><i class="fa fa-newspaper-o"></i> Crear Nuevo</button> 
            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-fw fa-close"></i> Cerrar</button>
        </div>
       </form>
    </div>
   </div>
</div>
<!-- /.modal-->


<script type="text/javascript">
    var baseurl = "<?php echo base_url();?>"; 
</script>