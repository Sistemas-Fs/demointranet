<?php
    $idusu = $this -> session -> userdata('s_idusuario');
    $idempleado= $this -> session -> userdata('s_idempleado');
?>

<style>
      
    
      [data-title]:hover:after {
          opacity: 1;
          transition: all 0.1s ease 0.5s;
          visibility: visible;
      }
  
      [data-title]:after {
          content: attr(data-title);
          background-color: #333;
          color: #fff;
          font-size: 14px;
          font-family: Raleway;
          position: absolute;
          padding: 3px 10px;
          bottom: -1.6em;
          left: -50%;
          right: -50%;
          /*white-space: nowrap;*/
          box-shadow: 1px 1px 3px #222222;
          opacity: 0;
          border: 1px solid #111111;
          z-index: 99999;
          visibility: hidden;
          border-radius: 6px;
          
      }
      [data-title] {
          position: relative;
      }
      .col_ellipsis[data-title][data-title-position="bottom"]:before {
          bottom: auto;
          margin-top: .5rem;
          top: 100%;
      }
      .col_ellipsis[data-title][data-title-position="bottom"]:after {
          bottom: auto;
          /*top: 100%;*/
          width: 300px;
      }
</style>

<!-- content-header -->
<div class="content-header">   
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><i class="fas fa-book"></i> CAPACITACIONES</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?php echo public_base_url(); ?>main">Home</a></li>
          <li class="breadcrumb-item active">Procesos Termicos</li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-12">
                <div class="card card-success card-outline card-tabs">
                    <div class="card-header p-0 pt-1 border-bottom-0">            
                        <ul class="nav nav-tabs" id="tabcapa" role="tablist">                    
                            <li class="nav-item">
                                <a class="nav-link active" id="tabcapa-list-tab" data-toggle="pill" href="#tabcapa-list" role="tab" aria-controls="tabcapa-list" aria-selected="true">LISTADO</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="tabcapa-reg-tab" data-toggle="pill" href="#tabcapa-reg" role="tab" aria-controls="tabcapa-reg" aria-selected="false">REGISTRO</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="tabcapa-parti-tab" data-toggle="pill" href="#tabcapa-parti" role="tab" aria-controls="tabcapa-parti" aria-selected="false">PARTICIPANTES</a>
                            </li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content" id="tabcapa-tabContent">
                            <div class="tab-pane fade show active" id="tabcapa-list" role="tabpanel" aria-labelledby="tabcapa-list-tab">
                                <div class="card card-success">
                                    <div class="card-header">
                                        <h3 class="card-title">BUSQUEDA</h3>
                                        <div class="card-tools">
                                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                        </div>
                                    </div>                        
                                    <div class="card-body">
                                        <input type="hidden" name="mtxtidusuinfor" class="form-control" id="mtxtidusuinfor" value="<?php echo $idusu ?>">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Clientes</label>
                                                    <select class="form-control select2bs4" id="cboClie" name="cboClie" style="width: 100%;">
                                                        <option value="" selected="selected">Cargando...</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">    
                                                <div class="checkbox"><label>
                                                    <input type="checkbox" id="chkFreg" /> <b>Fecha Registro :: Del</b>
                                                </label></div>                        
                                                <div class="input-group date" id="txtFDesde" data-target-input="nearest" >
                                                    <input type="text" id="txtFIni" name="txtFIni" class="form-control datetimepicker-input" data-target="#txtFDesde" disabled/>
                                                    <div class="input-group-append" data-target="#txtFDesde" data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">      
                                                <label>Hasta</label>                      
                                                <div class="input-group date" id="txtFHasta" data-target-input="nearest">
                                                    <input type="text" id="txtFFin" name="txtFFin" class="form-control datetimepicker-input" data-target="#txtFHasta" disabled/>
                                                    <div class="input-group-append" data-target="#txtFHasta" data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Cursos</label>
                                                    <select class="form-control select2bs4" id="cboCursos" name="cboCursos" style="width: 100%;">
                                                        <option value="" selected="selected">Cargando...</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                        
                                    <div class="card-footer justify-content-between"> 
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="text-right">
                                                    <button type="submit" class="btn btn-primary" id="btnBuscar"><i class="fas fa-search"></i> Buscar</button>
                                                    <button type="button" class="btn btn-outline-info" id="btnNuevo"><i class="fas fa-plus"></i> Crear Nuevo</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card card-outline card-success">
                                            <div class="card-header">
                                                <h3 class="card-title">Listado de Capacitaciones</h3>
                                            </div>                                        
                                            <div class="card-body">
                                                <table id="tblListCapacita" class="table table-striped table-bordered compact" style="width:100%">
                                                    <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th>Cliente</th>
                                                        <th>Fecha Capacitaciones</th>
                                                        <th>Curso</th>
                                                        <th></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                            <div class="tab-pane fade" id="tabcapa-reg" role="tabpanel" aria-labelledby="tabcapa-reg-tab">
                                <fieldset class="scheduler-border" id="regCapa">
                                    <legend class="scheduler-border text-primary">REG. CAPACITACION</legend>
                                    <form class="form-horizontal" id="frmRegCapa" action="<?= base_url('pt/cptcapacitacion/setregcapacitacion')?>" method="POST" enctype="multipart/form-data" role="form">
                                        <input type="hidden" name="mtxtidcapa" id="mtxtidcapa" class="form-control">
                                        <input type="hidden" name="hdnAccionregcapa" id="hdnAccionregcapa" class="form-control">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">  
                                                    <div class="text-info">Clientes</div>
                                                    <div>       
                                                        <select class="form-control select2bs4" id="cboregClie" name="cboregClie" style="width: 100%;">
                                                            <option value="" selected="selected">Cargando...</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <div class="text-info">Establecimiento</div>
                                                    <div>   
                                                        <select class="form-control select2bs4" id="cboregEstab" name="cboregEstab" style="width: 100%;">
                                                            <option value="" selected="selected">Cargando...</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>  
                                        </div>
                                        <div class="row">            
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="text-info">Fecha Inicio</div>
                                                    <div class="input-group date" id="mtxtFCapaini" data-target-input="nearest">
                                                        <input type="text" id="mtxtFinicio" name="mtxtFinicio" class="form-control datetimepicker-input" data-target="#mtxtFCapaini"/>
                                                        <div class="input-group-append" data-target="#mtxtFCapaini" data-toggle="datetimepicker">
                                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                        </div>
                                                    </div>                        
                                                </div> 
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="text-info">Horas de duración</div>
                                                <input type="number" name="mtxthduracion"id="mtxthduracion" class="form-control" value="0" min="0" pattern="^[0-9]+">
                                            </div> 
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <div class="text-info">Fechas de duración</div>
                                                    <div>   
                                                        <input type="text" name="mtxtfduracion"id="mtxtfduracion" class="form-control" >
                                                    </div>
                                                </div>
                                            </div>  
                                        </div>                                        
                                        <div class="row"> 
                                            <div class="col-sm-4">
                                                <div class="text-info">Curso</div>
                                                <div class="input-group input-group-select-button">
                                                    <select class="form-control select2bs4" id="mcboCursos" name="mcboCursos" style="width: 100%;">
                                                        <option value="" selected="selected">Cargando...</option>
                                                    </select>
                                                    <div class="input-group-addon input-group-button">
                                                        <button type="button" role="button" class="btn btn-outline-info" id="mbtnnewcurso" data-toggle="modal" data-target="#modalMantcurso"><i class="fas fa-plus-circle"></i></button>
                                                    </div>
                                                </div>
                                            </div>  
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <div class="text-info">Comentarios</div>
                                                    <div>   
                                                        <input type="text" name="mtxtComentarios"id="mtxtComentarios" class="form-control" >
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6 text-left">
                                                <button type="button" class="btn btn-info" id="btnAdjuntos"><i class="fas fa-paperclip"></i> Adjuntar Archivos</button> 
                                                <button type="button" class="btn btn-primary" id="btnParticiopantes"><i class="fas fa-user-friends"></i> Participantes</button>  
                                            </div> 
                                            <div class="col-sm-6 text-right"> 
                                                <button type="submit" class="btn btn-success" id="btnRegistrar"><i class="fas fa-save"></i> Registrar</button>   
                                                <button type="button" class="btn btn-secondary" id="btnRetornarLista"><i class="fas fa-undo-alt"></i> Retornar</button>
                                            </div>
                                        </div>
                                    </form>
                                    
                                    <div id="regAdjuntos" style="border-top: 1px solid #ccc; padding-top: 10px;">
                                        <div class="row">
                                            <div class="col-6">
                                                <h4>
                                                    <i class="fas fa-weight"></i> ADJUNTOS
                                                </h4>
                                            </div> 
                                        </div> 
                                        <div class="row"> 
                                            <div class="col-12">
                                                <div class="card card-outline card-lightblue">
                                                    <div class="card-header">
                                                        <h3 class="card-title">Listado de Adjuntos</h3>
                                                    </div>                                        
                                                    <div class="card-body" style="overflow-x: scroll;">
                                                        <table id="tblListAdjuntos" class="table table-striped table-bordered compact" style="width:100%">
                                                            <thead>
                                                            <tr>
                                                                <th></th>
                                                                <th>Tipo</th>
                                                                <th>Descripcion</th>
                                                                <th>Archivo</th>
                                                                <th></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="tab-pane fade" id="tabcapa-parti" role="tabpanel" aria-labelledby="tabcapa-parti-tab">
                                <fieldset class="scheduler-border" id="regCapa">
                                    <legend class="scheduler-border text-primary">LISTADO PARTICIPANTES</legend>
                                    <div class="row">
                                        <div class="col-sm-12 text-left">                                    
                                            <div class="input-group mb-3">
                                                <input type="hidden" id="mhdnIdcapacitaparti" name="mhdnIdcapacitaparti" >
                                                <button type="button" class="btn btn-outline-info" id="btnNuevoParti" ><i class="fas fa-plus"></i> Ingresar Nuevo Participante</button>
                                                &nbsp;&nbsp;
                                                <button type="button" class="btn btn-secondary" id="btnRetornarReg"><i class="fas fa-undo-alt"></i> Retornar Capacitacion</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="card card-outline card-success">
                                                <div class="card-body">  
                                                <table id="tblListParticipantes" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>DNI</th>
                                                        <th>Participantes</th>
                                                        <th># Certificado</th>
                                                        <th>Nota</th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                    </thead>
                                                <tbody>
                                                </tbody>
                                                </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="card card-outline card-success">
                                                <div class="card-header">
                                                    <h3 class="card-title">Registro Participante</h3>
                                                </div>                                        
                                                <form class="form-horizontal" id="frmRegParti" action="<?= base_url('pt/cptcapacitacion/setparticipante')?>" method="POST" enctype="multipart/form-data" role="form">              
                                                <div class="card-body">
                                                    <input type="hidden" id="mhdnIdparti" name="mhdnIdparti" > <!-- ID -->
                                                    <input type="hidden" id="mhdnIdcapaParti" name="mhdnIdcapaParti" >
                                                    <input type="hidden" id="mhdnIdadmParti" name="mhdnIdadmParti" >   
                                                    <input type="hidden" id="mhdnAccionParti" name="mhdnAccionParti" value="N"> <!-- ACCION -->                              
                                                        <div class="form-group">
                                                            <div class="col-sm-12"> 
                                                                <div class="text-light-blue">DNI</div> 
                                                                <div class="input-group mb-3" id="partitxt">
                                                                    <input type="text" name="mtxtDniparti" class="form-control" id="mtxtDniparti" /> 
                                                                    <span class="input-group-append">
                                                                        <button type="button" id="btnValidarAdmi" class="btn btn-info btn-flat"><i class="fas fa-user-check"></i></button>
                                                                        <button type="button" id="btnBuscarAdmi" class="btn btn-primary btn-flat"><i class="fas fa-search"></i></button>
                                                                    </span> 
                                                                </div>                                  
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-12"> 
                                                                <div class="text-light-blue">NOMBRES</div> 
                                                                <input type="text" class="form-control"  name="mtxtPerparti"id="mtxtPerparti" value=""/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-12"> 
                                                                <div class="text-light-blue">APELLIDO PATERNO</div> 
                                                                <input type="text" class="form-control"  name="mtxtAppatparti"id="mtxtAppatparti" value=""/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-12"> 
                                                                <div class="text-light-blue">APELLIDO MATERNO</div> 
                                                                <input type="text" class="form-control"  name="mtxtApmatparti"id="mtxtApmatparti" value=""/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-12"> 
                                                                <div class="text-light-blue">NOTA</div> 
                                                                <input type="text" class="form-control"  name="mtxtNotaparti"id="mtxtNotaparti" value=""/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-12"> 
                                                                <div class="text-light-blue">Email</div> 
                                                                <input type="text" class="form-control"  name="mtxtEmailparti"id="mtxtEmailparti" value=""/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-12"> 
                                                                <div class="text-light-blue">Telefono</div> 
                                                                <input type="text" class="form-control"  name="mtxtTelparti"id="mtxtTelparti" value="" />
                                                            </div>
                                                        </div>  
                                                </div>                    
                                                <div class="card-footer">
                                                    <div class="text-right">
                                                        <button type="submit" id="btnGrabarParti" class="btn btn-success">Guardar</button>
                                                    </div>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>                
                </div>
            </div>
        </div>     
    </div>
</section>
<!-- /.Main content -->

<!-- /.modal-subir-adjunto --> 
<div class="modal fade" id="modalAdjunto" data-backdrop="static" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form class="form-horizontal" id="frmSubadjunto" name="frmSubadjunto" action="<?= base_url('pt/cptcapacitacion/setregadjunto')?>" method="POST" enctype="multipart/form-data" role="form"> 
        <div class="modal-header text-center bg-success">
            <h4 class="modal-title w-100 font-weight-bold">Subir Adjuntos</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">          
            <input type="hidden" id="mhdnIdCapaadjunto" name="mhdnIdCapaadjunto"> <!-- ID -->   
            <input type="hidden" id="mtxtFinicioadjunto" name="mtxtFinicioadjunto">      
            <div class="form-group">                               
                 <div class="row"> 
                    <div class="col-sm-4">
                        <div class="text-info">Tipo de Adjunto</div>
                        <div>                            
                            <select class="form-control select2bs4" id="mcboadjTipo" name="mcboadjTipo" style="width: 100%;">
                                <option value="" selected="selected">Elejir::</option>
                                <option value="PRESENTACION">PRESENTACION</option>
                                <option value="LISTADO">LISTADO</option>
                                <option value="CERTIFICADOS">CERTIFICADOS</option>
                                <option value="TALLER">TALLER</option>
                                <option value="OTROS">OTROS</option>
                            </select>
                        </div>
                    </div>  
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="text-info">Descripcion</div>
                            <div>   
                                <input type="text" name="mtxtadjDescripcion"id="mtxtadjDescripcion" class="form-control" >
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="row">                
                    <div class="col-sm-12">
                        <div class="text-info">Adjunto</div>                        
                        <div class="input-group">
                            <input class="form-control" type="text" name="stxtNomarchadj" id="stxtNomarchadj">                            
                            <span class="input-group-append">                                
                                <div class="fileUpload btn btn-secondary">
                                    <span>Subir Adjunto</span>
                                    <input type="file" class="upload" id="stxtArchivoadj" name="stxtArchivoadj" onchange="adjArchivo()"/>                      
                                </div> 
                            </span>  
                        </div>
                        <span style="color: red; font-size: 13px;">+ Los archivos deben estar en formato pdf, docx o xlsx y no deben pesar mas de 60 MB</span>                        
                        <input type="hidden" name="stxtRutaadj" id="stxtRutaadj">
                        <input type="hidden" name="stxtadjunto" id="stxtadjunto"> 
                    </div> 
                </div>
            </div>
        </div>
        <div class="modal-footer justify-content-between" style="background-color: #dff0d8;">
            <button type="reset" class="btn btn-default" id="mbtnCSubadjunto" data-dismiss="modal">Cancelar</button>  
        </div>
      </form>
    </div>
  </div>
</div> 
<!-- /.modal-->

<!-- /.modal-importar-parti -->
<div class="modal fade" id="modalImportparti" data-backdrop="static" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content"> 
                <div class="modal-header text-center bg-success">
                    <h4 class="modal-title w-100 font-weight-bold">Importar Participantes</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body"> 
                    <div class="form-group">     
                    <div class="row">                
                        <div class="col-sm-12">           
                        <!----> 
                        <form class="form-horizontal" id="frmImport" name="frmImport" action="" method="POST" enctype="multipart/form-data" role="form">
                            <input type="hidden" id="mhdnIdCapamigra" name="mhdnIdCapamigra">
                            <div class="text-info">Archivo</div>                                                   
                            <div class="input-group">
                                <input class="form-control" type="text" name="txtFile" id="txtFile">                                                     
                                <span class="input-group-append">                                
                                    <div class="fileUpload btn btn-secondary">
                                        <span>Archivo</span>
                                        <input type="file" class="upload" id="fileMigra" name="fileMigra" onchange="adjFile()"/>                      
                                    </div> 
                                </span>  
                            </div> 
                            <span style="color: red; font-size: 13px;">+ Los archivos deben estar en formato xls y no deben pesar mas de 60 MB</span>                        
                            <input type="hidden" name="txtFileRuta" id="txtFileRuta">
                            <input type="hidden" name="mtxtFilearchivo" id="mtxtFilearchivo"> 

                            <div class="modal-footer" style="background-color: #dff0d8;">
                                <button type="button" class="btn btn-info" id="mbtnGUpload">Migrar</button>
                                <button type="reset" class="btn btn-default" id="mbtnCImportparti" data-dismiss="modal">Cancelar</button>
                            </div>
                        </form>
                        <!-- 
                        <?php   
                            echo form_open_multipart('at/capa/cregcapa/import_parti');
                        ?>
                            
                        <?php
                            $upload_data = array(
                                'type'  => 'file',
                                'name'  => 'fileMigra',
                                'id'    => 'fileMigra',
                                'class' => 'upload'
                            );
                            echo form_upload($upload_data);
                            echo '<br/>';
                            $data = [
                                'id'      => 'mbtnGUpload',
                                'type'    => 'submit',
                                'class'   => 'btn btn-info',
                                'content' => 'Migrar'
                            ];                
                            echo form_button($data);
                        ?>
                            <button type="reset" class="btn btn-default" id="mbtnCImportparti" data-dismiss="modal">Cancelar</button>
                            <input type="hidden" id="mhdnIdCapamigra" name="mhdnIdCapamigra">
                        <?php
                            echo form_close();
                        ?>   -->
                        </div> 
                    </div>
                    </div>
                </div>
        </div>
    </div>
</div>    
<!-- /.modal-->

<!-- /.modal-Mante Curso --> 
<div class="modal fade" id="modalMantcurso" role="dialog" tabindex="-1" data-backdrop="static" data-keyboard="false" aria-labelledby="exampleModalLabel" aria-hidden="true" >
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
        <div class="modal-header text-center bg-success">
            <h4 class="modal-title w-100 font-weight-bold">Registrar Curso</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body"> 
        <form class="form-horizontal" id="frmMantcurso" name="frmMantcurso" action="<?= base_url('pt/cptcapacitacion/setcapacurso')?>" method="POST" enctype="multipart/form-data" role="form"> 
            <input type="hidden" id="mhdnidptcapacurso" name="mhdnidptcapacurso">
            <input type="hidden" id="mhdnAccioncapacurso" name="mhdnAccioncapacurso">
            <div class="form-group">        
                <div class="row">
                    <div class="col-md-12"> 
                        <div class="text-info">Descripción</div>
                        <div>    
                            <input type="text" name="txtdescripcion"id="txtdescripcion" class="form-control"><!-- disable -->
                        </div>
                    </div>   
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer w-100 d-flex flex-row" style="background-color: #D4EAFC;">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-right">
                        <button type="reset" class="btn btn-default" id="mbtnCMantcurso" data-dismiss="modal">Cancelar</button>
                        <button type="submit" form="frmMantcurso" class="btn btn-info" id="mbtnGMantcurso">Grabar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</div> 
<!-- /.modal-->

<!-- Script Generales -->
<script type="text/javascript">
    var baseurl = "<?php echo base_url();?>"; 
</script>