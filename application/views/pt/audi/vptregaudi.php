<?php
    $idusu = $this -> session -> userdata('s_idusuario');
    $cusuario = $this -> session -> userdata('s_cusuario');
?>

<style>

    tab {
        display: inline-block; 
        margin-left: 100px; 
    }
    tr.subgroup,
    tr.subgroup:hover {
        background-color: #F2F2F2 !important;
        /* color: blue; */
        font-weight: bold;
    }
    .group{
        background-color: #CDD1DB !important;
            font-size:15px;
            color:#000000 !important;
            opacity:0.7;
    }
    .subgroup{
        cursor: pointer;
    }
    td.subgroup {
        border-top: double !important;
    }
    td.expand {
        border-bottom: double !important;
    }
    .modal-lg{
        max-width: 1000px !important;
    }

    .dataTables_scrollBody thead{
        visibility:hidden;
    }
    .hidden {
        display: none;
    }
</style>

<!-- content-header -->
<div class="content-header">   
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">PT AUDITORIA DE TERCEROS</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?php echo public_base_url(); ?>cprincipal/principal">Home</a></li>
          <li class="breadcrumb-item active">Procesos Termicos</li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content" style="background-color: #E0F4ED;">
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-12">
                <div class="card card-success card-outline card-tabs">
                    <div class="card-header p-0 pt-1 border-bottom-0">            
                        <ul class="nav nav-tabs" id="tabptaudi" style="background-color: #28a745;" role="tablist">                    
                            <li class="nav-item">
                                <a class="nav-link active" style="color: #000000;" id="tabptaudi-list-tab" data-toggle="pill" href="#tabptaudi-list" role="tab" aria-controls="tabptaudi-list" aria-selected="true">LISTADO</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" style="color: #000000;" id="tabptaudi-det-tab" data-toggle="pill" href="#tabptaudi-det" role="tab" aria-controls="tabptaudi-det" aria-selected="false">REGISTRO</a>
                            </li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content" id="tabptaudi-tabContent">
                            <div class="tab-pane fade show active" id="tabptaudi-list" role="tabpanel" aria-labelledby="tabptaudi-list-tab">
                                <div class="card card-success">
                                    <div class="card-header">
                                        <h3 class="card-title">BUSQUEDA</h3>
                                        <div class="card-tools">
                                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                        </div>
                                    </div>                        
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Clientes</label>
                                                    <select class="form-control select2bs4" id="cboclieserv" name="cboclieserv" style="width: 100%;">
                                                        <option value="" selected="selected">Cargando...</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">    
                                                <div class="checkbox"><label>
                                                    <input type="checkbox" id="chkFreg" /> <b>Fecha Registro :: Del</b>
                                                </label></div>                        
                                                <div class="input-group date" id="txtFDesde" data-target-input="nearest" >
                                                    <input type="text" id="txtFIni" name="txtFIni" class="form-control datetimepicker-input" data-target="#txtFDesde" disabled/>
                                                    <div class="input-group-append" data-target="#txtFDesde" data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">      
                                                <label>Hasta</label>                      
                                                <div class="input-group date" id="txtFHasta" data-target-input="nearest">
                                                    <input type="text" id="txtFFin" name="txtFFin" class="form-control datetimepicker-input" data-target="#txtFHasta" disabled/>
                                                    <div class="input-group-append" data-target="#txtFHasta" data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Nro Propuesta</label>
                                                    <input type="text" class="form-control" id="txtnropropu" name="txtnropropu" placeholder="...">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Nro Informe</label>
                                                    <input type="text" class="form-control" id="txtnroinfor" name="txtnroinfor" placeholder="...">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer justify-content-between" style="background-color: #E0F4ED;"> 
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="text-left"> 
                                                    <button type="button" class="btn btn-outline-info" id="btnNuevo"><i class="fas fa-plus"></i> Crear Nuevo</button>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="text-right">
                                                    <button type="submit" class="btn btn-primary" id="btnBuscar"><i class="fas fa-search"></i> Buscar</button>    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> 

                                <div class="row">
                                    <div class="col-12">
                                        <div class="card card-outline card-success">
                                            <div class="card-header">
                                                <div class="row">
                                                <div class="col-md-8">
                                                    <h3 class="card-title">Listado de Auditorias</h3>
                                                </div>     
                                                </div>
                                            </div>                                       
                                            <div class="card-body">
                                                <div class="row">
                                                <div class="col-md-12">
                                                <table id="tblListAudi" class="table table-striped table-bordered compact" style="width:100%">
                                                    <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th>N° Informe</th>
                                                        <th>Cliente</th>
                                                        <th>Proveedor</th>
                                                        <th>Establecimiento</th>
                                                        <th>Fecha Informe</th>
                                                        <th>N° Propuesta</th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                                </div> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                            <div class="tab-pane fade" id="tabptaudi-det" role="tabpanel" aria-labelledby="tabptaudi-det-tab">
                                <fieldset class="scheduler-border" id="regAuditoria">
                                    <legend class="scheduler-border text-primary">Auditoria</legend>
                                    <form class="form-horizontal" id="frmRegAudi" action="<?= base_url('pt/cptregaudi/setregauditoria')?>" method="POST" enctype="multipart/form-data" role="form">
                                        <input type="hidden" id="hdnidauditoria" name="hdnidauditoria">
                                        <input type="hidden" id="mhdnIdInfor" name="mhdnIdInfor">
                                        <input type="hidden" id="mhdnIdpteval" name="mhdnIdpteval">
                                        <input type="hidden" id="mhdnIdptregistro" name="mhdnIdptregistro">
                                        <input type="hidden" id="mhdnIdresponsable" name="mhdnIdresponsable">

                                        <input type="hidden" name="hdnAccionaudi" id="hdnAccionaudi">
                                        
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <div class="text-info">Clientes</div>
                                                    <div id="divRegClie">
                                                        <select class="form-control select2bs4" id="cboRegClie" name="cboRegClie" style="width: 100%;">
                                                            <option value="" selected="selected">Cargando...</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="text-info">Propuesta</div>
                                                    <!--<input type="text" class="form-control" id="txtRegPropu" name="txtRegPropu" disabled>-->
                                                    <div id="divRegPropu">
                                                        <select class="form-control select2bs4" id="cboRegPropu" name="cboRegPropu" style="width: 100%;" >
                                                            <option value="" selected="selected">Cargando...</option>
                                                        </select>
                                                    </div>
                                                </div>             
                                                <div class="col-md-2">
                                                    <div class="text-info">Fecha Informe</div>
                                                    <div class="input-group date" id="mtxtFreginforme" data-target-input="nearest">
                                                        <input type="text" id="mtxtFinfor" name="mtxtFinfor" class="form-control datetimepicker-input" data-target="#mtxtFreginforme"/>
                                                        <div class="input-group-append" data-target="#mtxtFreginforme" data-toggle="datetimepicker">
                                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                        </div>
                                                    </div>                        
                                                </div>   
                                                <div class="col-md-3">                              
                                                    <div class="checkbox" id="lbchkinf">
                                                        <div class="text-info">Nro Informe  &nbsp;&nbsp;
                                                            <b><input type="checkbox" id="chkNroAntiguo" name="chkNroAntiguo" > Antiguo</b>
                                                        </div>
                                                    </div>  
                                                    <div>                            
                                                        <input type="text" name="mtxtNroinfor" class="form-control" id="mtxtNroinfor">
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">  
                                                <div class="col-md-4">                        
                                                    <div class="text-info">Proveedor <span class="text-requerido">*</span></div>
                                                    <div>
                                                        <select class="form-control select2bs4" id="cboregprovclie" name="cboregprovclie" style="width: 100%;">
                                                            <option value="" selected="selected">Cargando...</option>
                                                        </select>
                                                    </div>
                                                </div> 
                                                <div class="col-md-3">
                                                    <div class="text-info">Establecimiento</div>
                                                    <div>
                                                        <select class="form-control select2bs4" id="cboregestable" name="cboregestable" style="width: 100%;">
                                                            <option value="" selected="selected">Cargando...</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-5">
                                                    <div class="text-info">Dir. Inspeccion</div>
                                                    <div>
                                                        <input type="text" name="mtxtregdirestable"id="mtxtregdirestable" class="form-control" disable>
                                                    </div>
                                                </div>      
                                            </div>                
                                        </div> 
                                        <div class="form-group">                                        
                                            <div class="row">       
                                                <div class="col-md-4"> 
                                                    <div class="text-info">Responsable<span style="color: #FD0202">*</span></div> 
                                                    <div>                            
                                                        <select class="form-control" id="mcboContacInfor" name="mcboContacInfor" >
                                                            <option value=1>SU-TZE LIU</option>
                                                            <option value=42>CARLOS VILLACORTA</option>
                                                            <option value=128>DIEGO LI</option>
                                                            <option value=64>JOSE OLIDEN</option>
                                                        </select>
                                                    </div>
                                                </div> 
                                                <div class="col-md-4">
                                                        <div class="text-info">Servicio a Auditar</div> 
                                                        <select class="form-control select2bs4" id="cboservicios" name="cboservicios" style="width: 100%;">
                                                            <option value="" selected="selected">Cargando...</option>
                                                        </select>
                                                </div>
                                                <div class="col-md-4"> 
                                                    <div class="text-info">Fecha Auditoria</div>                     
                                                    <div class="input-group date" id="txtFAuditoria" data-target-input="nearest">
                                                        <input type="text" id="txtFAudi" name="txtFAudi" class="form-control datetimepicker-input" data-target="#txtFAuditoria"/>
                                                        <div class="input-group-append" data-target="#txtFAuditoria" data-toggle="datetimepicker">
                                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                        </div>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-12">
                                                        <div class="text-info">Equipo(s)</div> 
                                                        <input type="text" class="form-control" id="txtEquipos" name="txtEquipos">
                                                </div>
                                            </div>  
                                        </div> 
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-12">
                                                        <div class="text-info">Productos - Líneas</div> 
                                                        <input type="text" class="form-control" id="txtProdLinea" name="txtProdLinea">
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <div class="row">                
                                                <div class="col-md-12">
                                                    <div class="text-info">Archivo</div>                                     
                                                    <div class="input-group">
                                                        <input class="form-control" type="text" name="mtxtNomarchaudi" id="mtxtNomarchaudi">                            
                                                        <span class="input-group-append">                                
                                                            <div class="fileUpload btn btn-secondary">
                                                                <span>Subir Archivo</span>
                                                                <input type="file" class="upload" id="mtxtArchivoaudi" name="mtxtArchivoaudi" onchange="escogerArchivoAudi()"/>                      
                                                            </div> 
                                                        </span>  
                                                    </div>
                                                    <span style="color: red; font-size: 13px;">+ Los archivos deben estar en formato pdf, docx o xlsx y no deben pesar mas de 60 MB</span>                        
                                                    <input type="hidden" name="mtxtRutaarchiaudi" id="mtxtRutaarchiaudi">
                                                    <input type="hidden" name="mtxtArchinfor" id="mtxtArchinfor">
                                                    <input type="hidden" name="sArchivo" id="sArchivo" value="N"> 
                                                </div> 
                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <div class="row">  
                                                <div class="col-md-12">
                                                    <div class="text-info">Comentarios</div>
                                                    <div>
                                                        <input type="text" name="mtxtregcomentario"id="mtxtregcomentario" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="form-group">    
                                            <div class="row">
                                                <div class="col-md-12 text-right"> 
                                                    <button type="submit" class="btn btn-success" id="btnGrabar"><i class="fas fa-save"></i> Grabar</button> 
                                                    <button type="button" class="btn btn-secondary" id="btnRetornarLista"><i class="fas fa-undo-alt"></i> Retornar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </fieldset>
                            </div>
                        </div>
                    </div>                
                </div>
            </div>
        </div>     
    </div>
</section>
<!-- /.Main content -->


<!-- /.modal-det-documentos --> 
<div class="modal fade" id="modalDetaInfo" data-backdrop="static" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form class="form-horizontal" id="frmDetaInfo" name="frmDetaPropu" action="<?= base_url('pt/cinforme/setdetinforme')?>" method="POST" enctype="multipart/form-data" role="form"> 

        <div class="modal-header text-center bg-success">
            <h4 class="modal-title w-100 font-weight-bold">Documentos de Informe</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
            
        <div class="modal-body">
            <div class="form-group">
                <div class="row">                
                    <div class="col-sm-12">
                        <input type="hidden" name="mtxtiddetainfo" id="mtxtiddetainfo" class="form-control">
                        <input type="hidden" name="mtxtfechadetainfo" id="mtxtfechadetainfo" class="form-control">
                        <input type="hidden" name="mtxtnrodetainfo" id="mtxtnrodetainfo" class="form-control">
                        <input type="hidden" name="mtxtcantdetainfo" id="mtxtcantdetainfo" class="form-control">
                        <div class="text-info">Archivos</div>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="mtxtDetArchivoinfo" name="mtxtDetArchivoinfo[]" multiple size="20" onchange="subirDetInforme()">
                            <span style="color: red">+ Los documentos deben estar en formato pdf, docx o xlsx</span> 
                            <br>
                            <span style="color: red">+ Los archivos no deben pesar mas de 60 MB</span> 
                            <label class="custom-file-label" for="mtxtArchivoinfo">Escoger Archivo</label>
                        </div>
                    </div>
                </div>
            </div>
            <table id="tblDetainfo" class="table table-striped table-bordered" style="width:100%">
                <thead>
                <tr>
                    <th style="min-width: 5px; max-width:10px;">Nro.</th>       
                    <th>Nombre</th> 
                    <th style="min-width: 50px; max-width: 50px;">Archivo</th>
                    <th style="min-width: 50px; max-width: 50px;"></th>
                </tr>
                </thead> 
            </table>  
        </div>

        <div class="modal-footer">
            <!--<button type="button" class="btn btn-outline-info" id="btnnewguia" name="btnnewguia" data-toggle="modal" data-target="#modalNewDetaInfo"><i class="fa fa-newspaper-o"></i> Crear Nuevo</button> -->
            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-fw fa-close"></i> Cerrar</button>
        </div>
       </form>
    </div>
   </div>
</div>
<!-- /.modal-->

<!-- Script Generales -->
<script type="text/javascript">
    var baseurl = "<?php echo base_url();?>"; 
</script>