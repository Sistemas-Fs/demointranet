<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/** COTIZACION **/ 
class Mregresult extends CI_Model {
	function __construct() {
		parent:: __construct();	
		$this->load->library('session');
    }
    
   /** LISTADO **/ 
    public function getbuscaringresoresult($parametros) { // Buscar Cotizacion	
        $procedure = "call usp_lab_coti_getbuscaringresoresult(?,?,?,?,?,?,?,?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
    }
    public function getbuscaringresoresultdet($cinternoordenservicio,$cmuestra) { // Listar Ensayos	
        $sql = "select C.dregistro as 'TIPOENSAYO', B.censayofs as 'CODENSAYO', B.densayo as 'ENSAYO', '' AS 'BLANCO'
                from PRESULTADO A
                    join mensayo B on B.censayo = A.censayo 
                    join ttabla C on C.ctipo = B.zctipoensayo
                where A.cinternoordenservicio = ".$cinternoordenservicio." and A.cmuestra = '".$cmuestra."';";        
        $query  = $this->db->query($sql);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
    }
    public function getrecuperaservicio($cinternoordenservicio) { // Listar Ensayos	
        $sql = "select c.drazonsocial, convert(varchar,a.fanalisis,103) as 'fanalisis', a.hanalisis, b.dcotizacion, convert(varchar,b.fcotizacion,103) as 'fcotizacion', a.nordenservicio, convert(varchar,a.fordenservicio,103) as 'fordenservicio',
                    a.dobservacionresultados, b.zctipoinforme, b.zctipocerticalidad, b.cinternocotizacion, b.nversioncotizacion, a.cinternoordenservicio,
                    (select count(1) from presultado z where z.cinternoordenservicio = a.cinternoordenservicio and (z.ctipoproducto not in ('0','2') or z.ctiporesultado = 'N')) as 'conttipoprod',
                    isnull(b.concc,'N') as 'concc'
                from pordenserviciotrabajo a
                    join pcotizacionlaboratorio b on b.cinternocotizacion = a.cinternocotizacion AND b.nversioncotizacion = a.nversioncotizacion
                    join mcliente c ON c.ccliente = b.ccliente
                where a.cinternoordenservicio = ".$cinternoordenservicio.";";        
        $query  = $this->db->query($sql);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
    }
   /** REGISTRO **/ 
    public function getcbotipoensayo($cinternoordenservicio){
        $sql = "select distinct C.zctipoensayo, D.dregistro as 'tipoensayo' 
                from presultado A   
                    join precepcionmuestra B ON B.cinternocotizacion = A.cinternocotizacion and B.nversioncotizacion = A.nversioncotizacion and B.nordenproducto = A.nordenproducto and B.cmuestra = A.cmuestra
                    join mensayo C ON A.censayo = C.censayo 
                    join ttabla D ON D.ctipo = C.zctipoensayo
                where A.cinternoordenservicio = ".$cinternoordenservicio."
                order by D.dregistro;";
		$query  = $this->db->query($sql);

        $listas = '<option value="%" selected="selected">Todos</option>';
        
        if ($query->num_rows() > 0) {
            
            foreach ($query->result() as $row)
            {
                $listas .= '<option value="'.$row->zctipoensayo.'">'.$row->tipoensayo.'</option>';  
            }
               return $listas;
        }{
            return false;
        }	
    }

    public function getlistobsmuestra($cinternoordenservicio) { // Listar Ensayos	
        $sql = "select cast(a.cinternoordenservicio as char(10))+';'+cast(a.cmuestra as char(10)) AS 'ID', A.CMUESTRA, A.dobservacion as 'DOBSERVMUESTRA' 
                from PRECEPCIONMUESTRA A join PORDENSERVICIOTRABAJO B on A.CINTERNOCOTIZACION  = B.CINTERNOCOTIZACION  
                where B.CINTERNOORDENSERVICIO = ".$cinternoordenservicio.";";        
        $query  = $this->db->query($sql);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
    }
	public function setobsmuestra($parametros = array()) { // Comprobar Email
		$this->db->where("cinternoordenservicio",$parametros["cinternoordenservicio"]);
		$this->db->where("cmuestra",$parametros["cmuestra"]);
        
		if($this->db->update("PRECEPCIONMUESTRA", $parametros)){
            return TRUE; 
		}else{
			return FALSE;
		}
    }  
    
    public function getlistLCmuestra($cinternoordenservicio) { // Listar Ensayos	
        $sql = "select cast(a.cinternoordenservicio as char(10))+';'+cast(a.cmuestra as char(10)) AS 'ID', A.CMUESTRA, A.dlclab as 'DLCLAB' 
                from PRECEPCIONMUESTRA A join PORDENSERVICIOTRABAJO B on A.CINTERNOCOTIZACION  = B.CINTERNOCOTIZACION  
                where B.CINTERNOORDENSERVICIO = ".$cinternoordenservicio.";";        
        $query  = $this->db->query($sql);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
    }
	public function setLCmuestra($parametros = array()) { // Comprobar Email
		$this->db->where("cinternoordenservicio",$parametros["cinternoordenservicio"]);
		$this->db->where("cmuestra",$parametros["cmuestra"]);
        
		if($this->db->update("PRECEPCIONMUESTRA", $parametros)){
            return TRUE; 
		}else{
			return FALSE;
		}
    }  

    public function getlistresultadoscab($cinternoordenservicio,$zctipoensayo,$sacnoac){
        $sql = "select distinct A.cmuestra, (A.cmuestra +'-'+ cast(A.nviausado as char(10))) as 'codmuestra', B.drealproducto, A.nviausado  
                from presultado A   
                    join precepcionmuestra B ON B.cinternocotizacion = A.cinternocotizacion and B.nversioncotizacion = A.nversioncotizacion and B.nordenproducto = A.nordenproducto and B.cmuestra = A.cmuestra
                where A.cinternoordenservicio = ".$cinternoordenservicio."                    
                    and (select count(1) from mensayo C where A.censayo = C.censayo and (zctipoensayo = '".$zctipoensayo."' or '".$zctipoensayo."' = '%')) > 0
                    and (select count(1) from mensayo C where A.censayo = C.censayo and (sacnoac = '".$sacnoac."' or '".$sacnoac."' = '%')) > 0
                order by A.cmuestra;";
        $query  = $this->db->query($sql);
        
        if (!$query) return [];
        return ($query->num_rows() > 0) ? $query->result() : [];
    }
    public function getlistresultadoscabtipo($cinternoordenservicio,$cmuestra,$zctipoensayo,$sacnoac){
        $sql = "select distinct C.zctipoensayo, D.dregistro as 'tipoensayo' 
                from presultado A   
                    join precepcionmuestra B ON B.cinternocotizacion = A.cinternocotizacion and B.nversioncotizacion = A.nversioncotizacion and B.nordenproducto = A.nordenproducto and B.cmuestra = A.cmuestra
                    join mensayo C ON A.censayo = C.censayo 
                    join ttabla D ON D.ctipo = C.zctipoensayo
                where A.cinternoordenservicio = ".$cinternoordenservicio."
                    and A.cmuestra = '".$cmuestra."'
                    and C.zctipoensayo like '".$zctipoensayo."'
                    and C.sacnoac like '".$sacnoac."'   
                order by D.dregistro;";
        $query  = $this->db->query($sql);
        
        if (!$query) return [];
        return ($query->num_rows() > 0) ? $query->result() : [];
    }
    
    public function getlistresultados($parametros) { // Buscar Cotizacion	
        $procedure = "call usp_lab_coti_getlistresultados_old(?,?,?,?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
    }
 

    public function setservicio($cinternoordenservicio,$fanalisis,$hanalisis,$dobservacionresultados,$ctipoinforme){
        return $this->db->update('pordenserviciotrabajo', [
            'fanalisis' => $fanalisis,
            'hanalisis' => $hanalisis,
            'dobservacionresultados' => trim($dobservacionresultados),
            'ctipoinforme' => $ctipoinforme,
        ], ['cinternoordenservicio' => $cinternoordenservicio]);
    }
    
	public function setresultadosold($parametros = array()) { // Comprobar Email
		$this->db->where("cinternoordenservicio",$parametros["cinternoordenservicio"]);
		$this->db->where("nordenproducto",$parametros["nordenproducto"]);
		$this->db->where("cmuestra",$parametros["cmuestra"]);
        $this->db->where("censayo",$parametros["censayo"]);
        $this->db->where("nviausado",$parametros["nviausado"]);
        
		if($this->db->update("presultado", $parametros)){
            $this->db->trans_begin();

            $procedure = "call usp_lab_resultados_calcularetiqueta(?,?,?,?,?,?,?);";
            $query = $this->db->query($procedure,$parametros);

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                return FALSE;
            }
            else
            {
                $this->db->trans_commit();
                return TRUE; 
            }
		}else{
			return FALSE;
		}
    }    
	public function setresultados($parametros = array()) { // Comprobar Email
		$this->db->where("cinternoordenservicio",$parametros["cinternoordenservicio"]);
		$this->db->where("nordenproducto",$parametros["nordenproducto"]);
		$this->db->where("cmuestra",$parametros["cmuestra"]);
        $this->db->where("censayo",$parametros["censayo"]);
        $this->db->where("nviausado",$parametros["nviausado"]);
        
		if($this->db->update("presultado", $parametros)){
            $this->db->trans_begin();

            $procedure = "call usp_lab_resultados_calcularcondi(?,?,?,?,?,?,?);";
            $query = $this->db->query($procedure,$parametros);

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                return FALSE;
            }
            else
            {
                $this->db->trans_commit();
                return TRUE; 
            }
		}else{
			return FALSE;
		}
    }
    
	public function updseleccion($parametros = array()) { // Comprobar Email
		$this->db->where("cinternoordenservicio",$parametros["cinternoordenservicio"]);
		$this->db->where("cinternocotizacion",$parametros["cinternocotizacion"]);
		$this->db->where("nordenproducto",$parametros["nordenproducto"]);
		$this->db->where("cmuestra",$parametros["cmuestra"]);
        $this->db->where("censayo",$parametros["censayo"]);
        $this->db->where("nviausado",$parametros["nviausado"]);
        
		if($this->db->update("presultado", $parametros)){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function updvistobueno($parametros = array()) { // Comprobar Email
		$this->db->where("cinternoordenservicio",$parametros["cinternoordenservicio"]);
		$this->db->where("cinternocotizacion",$parametros["cinternocotizacion"]);
		$this->db->where("nordenproducto",$parametros["nordenproducto"]);
		$this->db->where("cmuestra",$parametros["cmuestra"]);
        $this->db->where("censayo",$parametros["censayo"]);
        $this->db->where("nviausado",$parametros["nviausado"]);
        
		if($this->db->update("presultado", $parametros)){
			return TRUE;
		}else{
			return FALSE;
		}
	}

    public function getlistresultelementos($parametros) { // Buscar Cotizacion	
        $procedure = "call usp_lab_coti_getlistresultelementos_old(?,?,?,?,?,?,?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
    }
	public function setresultelementos($parametros = array()) { // Comprobar Email
		$this->db->where("cinternoordenservicio",$parametros["cinternoordenservicio"]);
		$this->db->where("cinternocotizacion",$parametros["cinternocotizacion"]);
		$this->db->where("nordenproducto",$parametros["nordenproducto"]);
		$this->db->where("cmuestra",$parametros["cmuestra"]);
        $this->db->where("censayo",$parametros["censayo"]);
        $this->db->where("zcelemento",$parametros["zcelemento"]);
        
		if($this->db->update("presultadometalesfitosanitarios", $parametros)){
            $this->db->trans_begin();

            $procedure = "call usp_lab_resultelementos_calcularcondi(?,?,?,?,?,?,?,?);";
            $query = $this->db->query($procedure,$parametros);
            
            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                return FALSE;
            }
            else
            {
                $this->db->trans_commit();
                return TRUE; 
            }
		}else{
			return FALSE;
        }
	}
	public function setresultelementos_old($parametros = array()) { // Comprobar Email
		$this->db->where("cinternoordenservicio",$parametros["cinternoordenservicio"]);
		$this->db->where("cinternocotizacion",$parametros["cinternocotizacion"]);
		$this->db->where("nordenproducto",$parametros["nordenproducto"]);
		$this->db->where("cmuestra",$parametros["cmuestra"]);
        $this->db->where("censayo",$parametros["censayo"]);
        $this->db->where("zcelemento",$parametros["zcelemento"]);
        
		if($this->db->update("presultadometalesfitosanitarios", $parametros)){
            return TRUE; 
		}else{
			return FALSE;
        }
	}
	public function updestadoelementos($parametros = array()) { // Comprobar Email
		$this->db->where("cinternoordenservicio",$parametros["cinternoordenservicio"]);
		$this->db->where("cinternocotizacion",$parametros["cinternocotizacion"]);
		$this->db->where("nordenproducto",$parametros["nordenproducto"]);
		$this->db->where("cmuestra",$parametros["cmuestra"]);
        $this->db->where("censayo",$parametros["censayo"]);
        $this->db->where("zcelemento",$parametros["zcelemento"]);
        
		if($this->db->update("presultadometalesfitosanitarios", $parametros)){
			return TRUE;
		}else{
			return FALSE;
		}
    }

    public function getlistresultsenso($parametros) { // Buscar Cotizacion	
        $procedure = "call usp_lab_coti_getlistresultsenso(?,?,?,?,?,?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
    }
    public function setaddsensorial($parametros) { // Buscar Cotizacion
        $this->db->trans_begin();

        $procedure = "call usp_lab_resultados_setaddsensorial(?,?,?,?,?,?);";
        $query = $this->db->query($procedure,$parametros);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
            return $query->result(); 
        }   
    }
    public function delsensorial($cinternoordenservicio,$cinternocotizacion,$nordenproducto,$cmuestra,$censayo,$norden) { // Eliminar detalle propuesta    
        $this->db->trans_begin();
        $this->db->delete('presultadosensorial', array('cinternoordenservicio' => $cinternoordenservicio,'cinternocotizacion' => $cinternocotizacion,'nordenproducto' => $nordenproducto,'cmuestra' => $cmuestra,'censayo' => $censayo,'norden' => $norden));
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_commit();
            return true; 
        }
    }
	public function setresultsenso($parametros = array()) { // Comprobar Email
		$this->db->where("cinternoordenservicio",$parametros["cinternoordenservicio"]);
		$this->db->where("cinternocotizacion",$parametros["cinternocotizacion"]);
		$this->db->where("nordenproducto",$parametros["nordenproducto"]);
		$this->db->where("cmuestra",$parametros["cmuestra"]);
        $this->db->where("censayo",$parametros["censayo"]);
        $this->db->where("norden",$parametros["norden"]);
        
		if($this->db->update("presultadosensorial", $parametros)){
			return TRUE;
		}else{
			return FALSE;
		}
	}

    public function setesterilidad($parametros) {  // Registrar evaluacion PT
        $this->db->trans_begin();

        $procedure = "call usp_lab_coti_setesterilidad(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
        $query = $this->db->query($procedure,$parametros);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
            return $query->result(); 
        }   
    } 	

    public function getlistaesteril($parametros) {  // Registrar evaluacion PT
        $procedure = "call usp_lab_coti_getlistaesteril(?,?,?,?,?,?);";
        $query = $this->db->query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}	 
    } 
    public function getlistaddnormas($cinternoordenservicio){
        $sql = "select distinct a.cmuestra, c.zctipoensayo, d.dregistro, a.drealproducto,
                    (select z.dnumeronorma from mnormalab z where z.cnormalab = e.cnormalab) as 'dnumeronorma',
                    (select y.dnumerogrupo from mgruponormalab y where y.cnormalab = e.cnormalab and y.cgruponormalab = e.cgruponormalab) as 'dnumerogrupo',
                    a.cinternocotizacion, b.cinternoordenservicio, '762' as 'zctipoinforme', a.nordenproducto, e.cnormalab, e.cgruponormalab, e.ndorden, ' ' as 'blanco'
                from precepcionmuestra a
                    join presultado b on b.cinternocotizacion = a.cinternocotizacion and b.nversioncotizacion = a.nversioncotizacion and b.nordenproducto = a.nordenproducto and b.cmuestra = a.cmuestra 
                    join mensayo c on c.censayo = b.censayo
                    join ttabla d on d.ctipo = c.zctipoensayo
                    left join pnormaxinforme e on e.cinternoordenservicio = b.cinternoordenservicio and e.norden = b.nordenproducto and e.cmuestra = b.cmuestra and e.zctipoensayo = c.zctipoensayo   
                where b.cinternoordenservicio = ".$cinternoordenservicio."  
                order by a.cmuestra, d.dregistro;";
        $query  = $this->db->query($sql);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}	
    }
    public function getlistnormas($zctipoensayo){
        $sql = "select d.cnormalab, d.dnumeronorma, d.dnormalab, e.cgruponormalab, if e.dnumerogrupo = '0' then '' else e.dnumerogrupo end if as 'dnumerogrupo', e.dnombregrupo
                from mnormalab d  left outer join mgruponormalab e on e.cnormalab = d.cnormalab
                where d.CNORMALAB in (select te.cnormalab from MRTIPOENSAYONORMALAB te where te.zctipoensayo = '".$zctipoensayo."')
                order by d.cnormalab, e.cgruponormalab;";
        $query  = $this->db->query($sql);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}	
    }

    public function setasocianorma($parametros) {  // Registrar evaluacion PT
        $this->db->trans_begin();

        $procedure = "call usp_lab_resultados_setasocianorma(?,?,?,?,?,?,?,?,?);";
        $query = $this->db->query($procedure,$parametros);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
            return $query->result(); 
        }   
    } 

	public function deleteasocianorma($cinternoordenservicio,$nordenproducto,$cmuestra,$zctipoinforme,$zctipoensayo,$ndorden) { //Eliminar Registro	
        $this->db->trans_begin();
        $this->db->delete('pnormaxinforme', array('cinternoordenservicio' => $cinternoordenservicio,'norden' => $nordenproducto,'cmuestra' => $cmuestra,'zctipoinforme' => $zctipoinforme,'zctipoensayo' => $zctipoensayo,'ndorden' => $ndorden));
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_commit();
            return true; 
        } 
	}
    

    
   ///////////////////////
    
    public function getcboum() { // Visualizar 	
        
        $sql = "select CTIPO, dregistro from ttabla where CTABLA = '38' and NCORRELATIVO > 0 order by NCORRELATIVO;";
		$query  = $this->db->query($sql);

        $listas = '<option value="0" selected="selected">::Elegir</option>';
        
        if ($query->num_rows() > 0) {
            
            foreach ($query->result() as $row)
            {
                $listas .= '<option value="'.$row->CTIPO.'">'.$row->dregistro.'</option>';  
            }
               return $listas;
        }{
            return false;
        }	
    }
    public function getum() { // Visualizar 	
        
        $sql = "select CTIPO, dregistro from ttabla where CTABLA = '38' and NCORRELATIVO > 0 order by NCORRELATIVO;";
		$query  = $this->db->query($sql);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}
    }
   ///////////////////////    
}
?>