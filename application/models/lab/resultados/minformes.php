<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Minformes extends CI_Model {
	function __construct() {
		parent:: __construct();	
		$this->load->library('session');
    }
    
    public function getinfpreliminar_caratula($parametros) { // Buscar Cotizacion	
        $procedure = "call usp_lab_informe_getinfpreliminar_caratula(?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
    }
    public function getinfpreliminar_muestras($parametros) { // Buscar Cotizacion	
        $procedure = "call usp_lab_informe_getinfpreliminar_muestras(?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
    }


    public function getinfxmuestras_caratula($parametros) { // Buscar Cotizacion	
        $procedure = "call usp_lab_consinf_getinfxmuestras_caratula(?,?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
	}
	public function getinfpersonalizado_caratula($parametros) { // Buscar Cotizacion	
        $procedure = "call usp_lab_consinf_getinfpersonalizado_caratula(?,?,?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
	}
	
    public function getcertixmuestras_caratula($parametros) { // Buscar Cotizacion	
        $procedure = "call usp_lab_consinf_getcertixmuestras_caratula(?,?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
	}
    public function getcertiunsolonumero($cinternoordenservicio) {			
        $sql = "select distinct dnumeroinforme as 'nrocerti' from pinformelabgenerado where cinternoordenservicio = ".$cinternoordenservicio;        
        $query  = $this->db->query($sql);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}	

	}
	
    public function getcertipersonalizado_caratula($parametros) { // Buscar Cotizacion	
        $procedure = "call usp_lab_consinf_getcertipersonalizado_caratula(?,?,?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
	}
	
    public function getinfxmuestras_resmicro($parametros) { // Buscar Cotizacion	
        $procedure = "call usp_lab_consinf_getinfxmuestras_resmicro(?,?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
    }	
    public function getinfpersonalizado_resmicro($parametros) { // Buscar Cotizacion	
        $procedure = "call usp_lab_consinf_getinfpersonalizado_resmicro(?,?,?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
    } 
    public function getinfxmuestras_resfq($parametros) { // Buscar Cotizacion	
        $procedure = "call usp_lab_consinf_getinfxmuestras_resfq(?,?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
	}
    public function getinfpersonalizado_resfq($parametros) { // Buscar Cotizacion	
        $procedure = "call usp_lab_consinf_getinfpersonalizado_resfq(?,?,?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
	}
    public function getinfxmuestras_resinstru($parametros) { // Buscar Cotizacion	
        $procedure = "call usp_lab_consinf_getinfxmuestras_resinstru(?,?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
	}
    public function getinfpersonalizado_resinstru($parametros) { // Buscar Cotizacion	
        $procedure = "call usp_lab_consinf_getinfpersonalizado_resinstru(?,?,?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
	}
    public function getinfxmuestras_ressenso($parametros) { // Buscar Cotizacion	
        $procedure = "call usp_lab_consinf_getinfxmuestras_ressenso(?,?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
	}
    public function getinfpersonalizado_ressenso($parametros) { // Buscar Cotizacion	
        $procedure = "call usp_lab_consinf_getinfpersonalizado_ressenso(?,?,?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
	}
    public function getinfxmuestras_reselementos($parametros) { // Buscar Cotizacion	
        $procedure = "call usp_lab_consinf_getinfxmuestras_reselementos(?,?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
	}
    public function getinfpersonalizado_reselementos($parametros) { // Buscar Cotizacion	
        $procedure = "call usp_lab_consinf_getinfpersonalizado_reselementos(?,?,?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
	}
    public function getinfxmuestras_resesteri($parametros) { // Buscar Cotizacion	
        $procedure = "call usp_lab_consinf_getinfxmuestras_resesteri(?,?,?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
	}
    public function getinfpersonalizado_resesteri($parametros) { // Buscar Cotizacion	
        $procedure = "call usp_lab_consinf_getinfpersonalizado_resesteri(?,?,?,?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
	}
    public function getinfxmuestras_resetiquetado($parametros) { // Buscar Cotizacion	
        $procedure = "call usp_lab_consinf_getinfxmuestras_resetiquetado(?,?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
	}
    public function getinfpersonalizado_resetiquetado($parametros) { // Buscar Cotizacion	
        $procedure = "call usp_lab_consinf_getinfpersonalizado_resetiquetado(?,?,?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
	}
    public function getinfxmuestras_nota01($parametros) { // Buscar Cotizacion	
        $procedure = "call usp_lab_consinf_getinfxmuestras_nota01(?,?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
    }
    public function getinfpersonalizado_nota01($parametros) { // Buscar Cotizacion	
        $procedure = "call usp_lab_consinf_getinfpersonalizado_nota01(?,?,?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
    }
    public function getmetodosensayos($parametros) { // Buscar Cotizacion	
        $procedure = "call usp_lab_consinf_getmetodosensayos(?,?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
    }
    public function getmetodosensayospersonalizado($parametros) { // Buscar Cotizacion	
        $procedure = "call usp_lab_consinf_getmetodosensayospersonalizado(?,?,?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
    }
    public function getinfxmuestras_fechafirma($parametros) { // Buscar Cotizacion	
        $procedure = "call usp_lab_consinf_getinfxmuestras_fechafirma(?,?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
    }
    public function getinfpersonalizado_fechafirma($parametros) { // Buscar Cotizacion	
        $procedure = "call usp_lab_consinf_getinfpersonalizado_fechafirma(?,?,?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
    }
    public function getdocnormativo($cinternoordenservicio,$cmuestra) { // Listar Ensayos	
        $sql = "select DISTINCT A.cnormalab, A.dnormalab, B.cgruponormalab 
				from mnormalab A   
					JOIN pnormaxinforme B ON B.cnormalab = A.cnormalab  
				where B.cinternoordenservicio = ".$cinternoordenservicio."   
					AND B.zctipoinforme = '762' 
					AND B.cmuestra = '".$cmuestra."'   
				order by A.cnormalab;";        
        $query  = $this->db->query($sql);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
    }
    public function getdocnormativo_det($cnormalab,$cgruponormalab) { // Listar Ensayos	
        $sql = "select DISTINCT A.dnumerogrupo, A.dnombregrupo 
				from mgruponormalab A    
					JOIN pnormaxinforme B ON B.cnormalab = A.cnormalab AND B.cgruponormalab = A.cgruponormalab 
				where B.cnormalab = ".$cnormalab."                     
					AND B.cgruponormalab = ".$cgruponormalab." 
				order by A.dnumerogrupo";        
        $query  = $this->db->query($sql);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
    }
    public function getcertixmuestras_conclusion($parametros) { // Buscar Cotizacion	
        $procedure = "call usp_lab_consinf_getcertixmuestras_conclusion(?,?,?,?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
    }
}
?>