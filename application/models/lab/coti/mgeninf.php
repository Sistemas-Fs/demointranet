<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mgeninf extends CI_Model {
	function __construct() {
		parent:: __construct();	
		$this->load->library('session');
    }
    
    public function generarformatos($parametros) { // Buscar Cotizacion	
        $procedure = "call usp_lab_consinf_generarformatos(?,?,?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
    }
    public function detgenerarformatos($parametros) { // Buscar Cotizacion	
        $procedure = "call usp_lab_consinf_detgenerarformatos(?,?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
    }
    public function setgeninforme($parametros) { // Buscar Cotizacion
        $this->db->trans_begin();

        $procedure = "call usp_lab_cotizacion_setgeninforme(?,?,?);";
        $query = $this->db->query($procedure,$parametros);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
            return $query->result(); 
        }   
    }
    public function setgeninformepersonalizado($parametros) { // Buscar Cotizacion
        $this->db->trans_begin();

        $procedure = "call usp_lab_cotizacion_setgeninformepersonalizado(?,?,?);";
        $query = $this->db->query($procedure,$parametros);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
            return $query->result(); 
        }   
    }
    
    public function setgencertificado($parametros) { // Buscar Cotizacion
        $this->db->trans_begin();

        $procedure = "call usp_lab_cotizacion_setgencertificado(?,?,?);";
        $query = $this->db->query($procedure,$parametros);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
            return $query->result(); 
        }   
    }
	public function cambiartipoinf($parametros = array()) { // Comprobar Email
		$this->db->where("CINTERNOCOTIZACION",$parametros["CINTERNOCOTIZACION"]);
        
		if($this->db->update("PCOTIZACIONLABORATORIO", $parametros)){
            return TRUE; 
		}else{
			return FALSE;
		}
    }     
    public function getlistensayosperso($parametros) { // Buscar Cotizacion	
        $procedure = "call usp_lab_coti_getlistensayosperso(?,?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
    }
    public function setidinforme($parametros) { // Buscar Cotizacion
        $this->db->trans_begin();

        $procedure = "call usp_lab_cotizacion_setidinforme(?);";
        $query = $this->db->query($procedure,$parametros);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
            return $query->result(); 
        }   
    }
    public function geninfoseleccionados($parametros) { // Buscar Cotizacion
        $this->db->trans_begin();

        $procedure = "call usp_lab_cotizacion_geninfoseleccionados(?,?,?,?,?,?,?);";
        $query = $this->db->query($procedure,$parametros);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
            return $query->result(); 
        }   
    }

    public function cbogenmuestras($cordenservicio) { // Visualizar Clientes con propuestas en CBO	
        
        $sql = "select cmuestra from PRECEPCIONMUESTRA where cinternoordenservicio = '".$cordenservicio."'   
                order by cmuestra asc;";
		$query  = $this->db->query($sql);
        
        if ($query->num_rows() > 0) {

            $listas = '<option value="0" selected="selected">::Elejir</option>';
            
            foreach ($query->result() as $row)
            {
                $listas .= '<option value="'.$row->cmuestra.'">'.$row->cmuestra.'</option>';  
            }
               return $listas;
        }{
            return false;
        }	
    }

    public function getcbomuestrasborrador1($cinternoordenservicio) { // Visualizar Clientes con propuestas en CBO	
        
        $sql = "select cmuestra from PRECEPCIONMUESTRA where cinternoordenservicio = '".$cinternoordenservicio."'   
                order by cmuestra asc;";
		$query  = $this->db->query($sql);
        
        if ($query->num_rows() > 0) {

            $listas = '<option value="0" selected="selected">::Elejir</option>';
            
            foreach ($query->result() as $row)
            {
                $listas .= '<option value="'.$row->cmuestra.'">'.$row->cmuestra.'</option>';  
            }
               return $listas;
        }{
            return false;
        }	
    }
      
    public function getcbomuestrasborrador($cinternoordenservicio) { // Buscar Cotizacion	
        $sql = "select cmuestra, '' as 'SPACE' from PRECEPCIONMUESTRA where cinternoordenservicio = '".$cinternoordenservicio."'   
                order by cmuestra asc;";
		$query = $this->db-> query($sql);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
    }
}
?>