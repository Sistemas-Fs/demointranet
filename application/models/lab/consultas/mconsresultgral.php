<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mconsresultgral extends CI_Model {
	function __construct() {
		parent:: __construct();	
		$this->load->library('session');
    }
    
    public function resultadosgeneral($parametros) { // Buscar Cotizacion	
        $procedure = "call usp_lab_consulta_resultadosgeneral(?,?,?,?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
    }

    public function getlistelementos($parametros) { // Buscar Cotizacion	
        $procedure = "call usp_lab_coti_getlistelementos(?,?,?,?,?,?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
    }
}
?>