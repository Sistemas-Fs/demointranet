<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mconsinf extends CI_Model {
	function __construct() {
		parent:: __construct();	
		$this->load->library('session');
    }
    
    public function consultaformatos($parametros) { // Buscar Cotizacion	
        $procedure = "call usp_lab_consinf_consultaformatos(?,?,?,?,?,?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
    }
    public function detconsultaformatos($parametros) { // Buscar Cotizacion	
        $procedure = "call usp_lab_consinf_detconsultaformatos(?,?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
    }
}
?>