<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mmantensayo extends CI_Model {
	function __construct() {
		parent:: __construct();	
		$this->load->library('session');
    }

    public function getbuscarensayo($parametros) { // Lista de consultas de Ensayo
        $procedure = "call usp_lab_mant_buscarensayo(?)";
        $query = $this->db-> query($procedure,$parametros);

        if ($query->num_rows() > 0) { 
            return $query->result();
        }{
            return False;
        }		   
    }   
    public function getcboztipoensayo() { // Visualizar Clientes del servicio en CBO 
        $sql = "select ctipo, dregistro from ttabla z where z.ctabla = '18' and ncorrelativo <> 0
                order by dregistro asc;";
        $query  = $this->db->query($sql);
            
        if ($query->num_rows() > 0) {
 
             $listas = '<option value="">::Elige</option>';
             
             foreach ($query->result() as $row){
                 $listas .= '<option value="'.$row->ctipo.'">'.$row->dregistro.'</option>';  
             }
                return $listas;
         }{
             return false;
         }		   
    }
    public function setensayo($parametros) { // Guardar Cliente
        $this->db->trans_begin();

        $procedure = "call usp_lab_mant_setensayo(?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $query = $this->db-> query($procedure,$parametros);

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else {
            $this->db->trans_commit();
            return $query->result(); 
        }   
    } 
    public function getcboreglab() { // Visualizar Clientes del servicio en CBO 
        $sql = "select claboratorio, dlaboratorio from mlaboratorio where sregistro = 'A' 
                order by dlaboratorio asc;";
        $query  = $this->db->query($sql);
            
        if ($query->num_rows() > 0) {
 
             $listas = '<option value="">::Elige</option>';
             
             foreach ($query->result() as $row){
                 $listas .= '<option value="'.$row->claboratorio.'">'.$row->dlaboratorio.'</option>';  
             }
                return $listas;
         }{
             return false;
         }		   
    }
    public function getlistlaboratorio($censayo) { // Visualizar 
        $sql = "select a.claboratorio, b.dlaboratorio, a.icosto, a.censayo  
                from mensayoxlaboratorio a join mlaboratorio b on b.claboratorio = a.claboratorio
                where a.censayo =  '".$censayo."' and a.sregistro = 'A';";
        $query  = $this->db->query($sql);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
            return False;
        }		   
    } 
    public function setlaboratorio($parametros) { // Guardar Cliente
        $this->db->trans_begin();

        $procedure = "call usp_lab_mant_setlaboratorio(?,?,?,?)";
        $query = $this->db-> query($procedure,$parametros);

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else {
            $this->db->trans_commit();
            return $query->result(); 
        }   
    } 

    public function getlistmatriz($censayo) { // Visualizar 
        $sql = "select censayo, cdensayo, ddetalle  
                from mdensayo
                where censayo =  '".$censayo."';";
        $query  = $this->db->query($sql);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
            return False;
        }		   
    } 
    public function setmatriz($parametros) { // Guardar Cliente
        $this->db->trans_begin();

        $procedure = "call usp_lab_mant_setmatriz(?,?,?,?)";
        $query = $this->db-> query($procedure,$parametros);

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else {
            $this->db->trans_commit();
            return $query->result(); 
        }   
    } 

	public function setreglab($parametros) { // Registrar Vacaciones		
        $this->db->trans_begin();
    
        $procedure = "call usp_lab_mant_setreglab(?,?,?)";
        $query = $this->db-> query($procedure,$parametros); 
           
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }
        else{
            $this->db->trans_commit();
            if ($query->num_rows() > 0) {
                return $query->result();
            }{
                return False;
            }	
        }   
    }
}
?>