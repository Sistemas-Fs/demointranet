<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mlabcliente extends CI_Model {
	function __construct() {
		parent:: __construct();	
		$this->load->library('session');
    }

    public function getbuscarclientes($parametros) { // Lista de consultas de Cliente
        $procedure = "call usp_lab_mant_buscarcliente(?)";
        $query = $this->db-> query($procedure,$parametros);

        if ($query->num_rows() > 0) { 
            return $query->result();
        }{
            return False;
        }		   
    }
    public function getvalidarnroruc($parametros) { // Validar numero de ruc del cliente
        $procedure = "call usp_lab_mant_validarnroruc(?)";
        $query = $this->db-> query($procedure,$parametros);

        if ($query->num_rows() > 0) { 
            return $query->result();
        }{
            return False;
        }		   
    }    
    public function getcliente($ccliente) { // Visualizar 
        $sql = "select a.ccliente, a.nruc, a.drazonsocial, a.cpais, a.dciudad, a.destado, a.dzip, a.cubigeo, a.ddireccioncliente, a.dtelefono,
                    a.dfax, a.dweb, a.zctipotamanoempresa, a.ntrabajador, a.drepresentante, a.dcargorepresentante, a.demailrepresentante,if a.druta = '' then 'unknown.png' else isnull(a.druta,'unknown.png') end if as 'druta', a.tipodoc, 
                    if a.cpais = '290' then (select ('('+z.ddepartamento+' - '+z.dprovincia+' - '+z.ddistrito+')') as dubigeo from tubigeo z where z.cubigeo = a.CUBIGEO) else if isnull(a.cpais,'') = '' then '' else ('('+a.dciudad+' - '+a.destado+' - '+(select z.dregistro from ttabla z where z.ctipo = a.cpais)+')') end if end if AS 'dubigeo'  
                from mcliente a 
                where a.ccliente = '".$ccliente."';";
        $query  = $this->db->query($sql);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
            return False;
        }		   
    }   
		
    public function setlabcliente($parametros) { // Guardar Cliente
        $this->db->trans_begin();

        $procedure = "call usp_lab_mant_setcliente(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $query = $this->db-> query($procedure,$parametros);

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else {
            $this->db->trans_commit();
            return $query->result(); 
        }   
    } 

    public function getbuscarestablecimiento($parametros) { // Lista de consultas de Cliente
        $procedure = "call usp_lab_mant_buscarestablecimiento(?)";
        $query = $this->db-> query($procedure,$parametros);

        if ($query->num_rows() > 0) { 
            return $query->result();
        }{
            return False;
        }		   
    }
		
    public function mantgral_establecimiento($parametros) { // Guardar Cliente
        $this->db->trans_begin();

        $procedure = "call usp_lab_mant_setestablecimiento(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $query = $this->db-> query($procedure,$parametros);

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else {
            $this->db->trans_commit();
            return $query->result(); 
        }   
    } 

}
?>