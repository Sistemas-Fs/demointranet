<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mconstramdigesa extends CI_Model {
    function __construct() {
        parent::__construct();	
        $this->load->library('session');
    }
    
   /** TRAMITES DIGESA **/ 

    public function getconsulta_grid_tr($parametros) { // Recupera Listado de Propuestas      
		$procedure = "call usp_ar_tramite_getconsulta_grid_tr(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}	
    }

    public function getconsulta_excel_tr($parametros) { // Recupera Listado de Propuestas      
		$procedure = "call usp_ar_tramite_getconsulta_excel_tr(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}	
    }

    public function getbuscartramite($parametros) { // Recupera Listado de Propuestas      
		$procedure = "call usp_ar_tramite_getbuscartramite(?,?,?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}	
    }

    public function getdocum_aarr($parametros) { // Recupera Listado de Propuestas      
		$procedure = "call usp_ar_tramite_getdocum_aarr(?,?,?,?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}	
		}
		
		public function getArchivos(string $CASUNTOREGULATORIO, string $CENTIDADREGULA, string $CTRAMITE, string $CDOCUMENTO) {
			$this->db->select('DUBICACIONFILESERVER');
			$this->db->from('PDOCUMENTOREGULATORIOARCHIVOS');
			$this->db->where('CASUNTOREGULATORIO', $CASUNTOREGULATORIO);
			$this->db->where('CENTIDADREGULA', $CENTIDADREGULA);
			$this->db->where('CTRAMITE', $CTRAMITE);
			$this->db->where('CDOCUMENTO', $CDOCUMENTO);
			$this->db->where('SREGISTRO', 'A');
			$query = $this->db->get();
			if (!$query) {
				return [];
			}
			return ($query->num_rows()) ? $query->result() : [];
		}

}
?>