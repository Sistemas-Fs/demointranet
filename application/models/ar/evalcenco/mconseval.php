<?php

class mconseval extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

    public function consultarEvaluaciones(string $codeval,string $nrors, string $nombreproducto, int $estado,int $fabricante)
    {
        $this->db->select('CEN_EVALUACION.IDEVAL AS CODIGOEVAL,
                        CEN_EVALUACION.ESTADO AS ESTADOEVAL,
                        CEN_EVALUACION.FECHAINICIO AS FECHAINICIO,
                        CEN_EVALUACION.FECHACIERRE AS FECHACIERRE,
                        CEN_PRODUCTOS.NOMBREPRODUCTO AS NOMBREPRODUCTO,
                        CEN_PRODUCTOS.REGSANITARIO AS REGSANITARIO,
                        CEN_PRODUCTOS.FECHAVENCIMIENTO AS VIGENCIARS');
        $this->db->from('CEN_EVALUACION');
        $this->db->join('CEN_PRODUCTOS','CEN_PRODUCTOS.IDPRODUCTO = CEN_EVALUACION.IDPRODUCTO');
        if(!empty($codeval)){
            $this->db->where('CEN_EVALUACION.IDEVAL',$codeval);
        }
        if(!empty($nrors)){
            $this->db->like('CEN_PRODUCTOS.REGSANITARIO',$nrors,'after');
        }
        if(!empty($nombreproducto)){
            $this->db->like('CEN_PRODUCTOS.NOMBREPRODUCTO',$nombreproducto,'both');
        }
        if($estado != 0){
            $this->db->where('CEN_EVALUACION.ESTADO',$estado);
        }
        if($fabricante !=0){
            $this->db->where('CEN_PRODUCTOS.FABRICANTE',$fabricante);
        }
        $this->db->order_by('CEN_EVALUACION.IDEVAL','desc');
        $query = $this->db->get();
        if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
    }

    public function buscarDocumentoxEval(int $codigoeval):array
    {
        $this->db->select('*');
        $this->db->from('CEN_DOCUMENTOS');
        $this->db->where('CEN_DOCUMENTOS.IDEVAL',$codigoeval);
        $query = $this->db->get();
        if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
    }

    public function autoload_fabricantes()
    {
        $this->db->select('DFABRICANTE AS NOMBREFABRICANTE,CFABRICANTE AS IDFABRICANTE');
		$this->db->where('CCLIENTE','00654');
		$query = $this->db->get('MFABRICANTEXCLIENTE');

        if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
    }

}
