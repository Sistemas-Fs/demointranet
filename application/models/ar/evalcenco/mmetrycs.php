<?php

class mmetrycs extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}
    
    public function getDistriAprobados(int $anio,int $mes)
    {
        if($mes != 0){
            if($mes<10){
                $mesS = (string)'0'.$mes;
            }else{
                $mesS = (string)$mes;
            }
            $fechaI = $anio.'-'.$mesS.'-01';
            if($mes == 2){
                $fechaF = $anio.'-'.$mesS.'-28';
            }else if($mes == 4 || $mes == 6 || $mes == 9 || $mes == 11){
                $fechaF = $anio.'-'.$mesS.'-30';
            }else{
                $fechaF = $anio.'-'.$mesS.'-31';
            }
            
        }else{
            $fechaI = $anio.'-1-1';
            $fechaF = $anio.'-12-31';
        }
        $this->db->where('FECHAINICIO <=',$fechaF);
        $this->db->where('FECHAINICIO >=',$fechaI);
        $this->db->where('IDPRODUCTO !=',NULL);
        $this->db->where('ESTADO',1);
        $query = $this->db->get('CEN_EVALUACION');
        return $query->num_rows();
    }

    public function getDistriObservados(int $anio,int $mes)
    {
        if($mes != 0){
            if($mes<10){
                $mesS = (string)'0'.$mes;
            }else{
                $mesS = (string)$mes;
            }
            $fechaI = $anio.'-'.$mesS.'-01';
            if($mes == 2){
                $fechaF = $anio.'-'.$mesS.'-28';
            }else if($mes == 4 || $mes == 6 || $mes == 9 || $mes == 11){
                $fechaF = $anio.'-'.$mesS.'-30';
            }else{
                $fechaF = $anio.'-'.$mesS.'-31';
            }
            
        }else{
            $fechaI = $anio.'-1-1';
            $fechaF = $anio.'-12-31';
        }
        $this->db->where('FECHAINICIO <=',$fechaF);
        $this->db->where('FECHAINICIO >=',$fechaI);
        $this->db->where('IDPRODUCTO !=',NULL);
        $this->db->where('ESTADO',2);
        $query = $this->db->get('CEN_EVALUACION');
        return $query->num_rows();
    }

    public function getDistriTruncos(int $anio,int $mes)
    {
        if($mes != 0){
            if($mes<10){
                $mesS = (string)'0'.$mes;
            }else{
                $mesS = (string)$mes;
            }
            $fechaI = $anio.'-'.$mesS.'-01';
            if($mes == 2){
                $fechaF = $anio.'-'.$mesS.'-28';
            }else if($mes == 4 || $mes == 6 || $mes == 9 || $mes == 11){
                $fechaF = $anio.'-'.$mesS.'-30';
            }else{
                $fechaF = $anio.'-'.$mesS.'-31';
            }
            
        }else{
            $fechaI = $anio.'-01-01';
            $fechaF = $anio.'-12-31';
        }
        $this->db->where('FECHAINICIO <=',$fechaF);
        $this->db->where('FECHAINICIO >=',$fechaI);
        $this->db->where('IDPRODUCTO !=',NULL);
        $this->db->where('ESTADO',3);
        $query = $this->db->get('CEN_EVALUACION');
        
        return $query->num_rows();
    }

    private function getSolixMes(int $anio,int $mes)
    {
        if($mes<10){
            $mesS = (string)'0'.$mes;
        }else{
            $mesS = (string)$mes;
        }
        $fechaI = $anio.'-'.$mesS.'-01';
        if($mes == 2){
            $fechaF = $anio.'-'.$mesS.'-28';
        }else if($mes == 4 || $mes == 6 || $mes == 9 || $mes == 11){
            $fechaF = $anio.'-'.$mesS.'-30';
        }else{
            $fechaF = $anio.'-'.$mesS.'-31';
        }
        $this->db->where('FECHAINICIO <=',$fechaF);
        $this->db->where('FECHAINICIO >=',$fechaI);
        $this->db->where('IDPRODUCTO !=',NULL);
        $this->db->where('ESTADO',1);
        $query1 = $this->db->get('CEN_EVALUACION');

        $this->db->where('FECHAINICIO <=',$fechaF);
        $this->db->where('FECHAINICIO >=',$fechaI);
        $this->db->where('IDPRODUCTO !=',NULL);
        $this->db->where('ESTADO',2);
        $query2 = $this->db->get('CEN_EVALUACION');
        
        $this->db->where('FECHAINICIO <=',$fechaF);
        $this->db->where('FECHAINICIO >=',$fechaI);
        $this->db->where('IDPRODUCTO !=',NULL);
        $this->db->where('ESTADO',3);
        $query3 = $this->db->get('CEN_EVALUACION');

        $resulta = ['A'=>$query1->num_rows(),'O'=>$query2->num_rows(),'T'=>$query3->num_rows()];

        return $resulta;
    }

    public function getSoliTotal($anio)
    {
        $soli = [];
        for ($i=1; $i <=12 ; $i++) { 
            $result = $this->getSolixMes($anio,$i);
            $soli[$i] = $result;
        }

        return $soli;
    }

}
