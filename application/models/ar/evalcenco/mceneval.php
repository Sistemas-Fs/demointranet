<?php

class mceneval extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public function buscarEvalxID(int $id):array
	{
		$this->db->select('*');
		$this->db->from('CEN_EVALUACION');
		$this->db->where('CEN_EVALUACION.IDEVAL',$id);
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	public function buscarProductoxID(int $id):array
	{
		$this->db->select('*');
		$this->db->from('CEN_PRODUCTOS');
		$this->db->where('CEN_PRODUCTOS.IDPRODUCTO',$id);
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	public function buscarDocumentosxProducto(int $idProducto):array
	{
		$this->db->select('*');
		$this->db->from('CEN_DOCUMENTOS');
		$this->db->where('CEN_DOCUMENTOS.IDPRODUCTO',$idProducto);
		$this->db->order_by('CEN_DOCUMENTOS.IDTRAMITE','ASC');
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

    private function obtenerUltimoIDEval()
	{
		$query = $this->db->select('MAX(IDEVAL) as id')
			->from('CEN_EVALUACION')
			->get();
		if (!$query) {
			throw new Exception('Error al encontrar el Código de Evaluación');
		}
		return intval($query->row()->id);
	}

	public function crearEval(string $fechaInicio, int $estado):array
	{
        $fechaInicioAlt = \Carbon\Carbon::createFromFormat('d/m/Y', $fechaInicio, 'America/Lima')->format('Y-m-d');
		//$fechaCierreAlt = \Carbon\Carbon::createFromFormat('d/m/Y', $fechaInicio, 'America/Lima')->addDays(60)->format('Y-m-d');
        $datos = [
            'FECHAINICIO' => $fechaInicioAlt,
            'ESTADO' => $estado
        ];
		$res = $this->db->insert('CEN_EVALUACION',$datos);
        
        if (!$res) {
			throw new Exception('Trámite de Evaluación no pudo ser creado correctamente.');
		}else{
            $datos['IDEVAL'] = $this->obtenerUltimoIDEval();
        }
		return $datos;
	}

	public function actualizarEval(string $fechaInicio, int $estado, string $codigoeval,int $cerrar):array
	{
        $fechaInicioAlt = \Carbon\Carbon::createFromFormat('d/m/Y', $fechaInicio, 'America/Lima')->format('Y-m-d');
		$fechaCierreAlt = \Carbon\Carbon::now('America/Lima')->format('Y-m-d');

        $datos = [
            'FECHAINICIO' => $fechaInicioAlt,
            'ESTADO' => $estado
        ];
		if($cerrar != 0){
			$datos['FECHACIERRE'] = $fechaCierreAlt;
		}
		$this->db->where('CEN_EVALUACION.IDEVAL',$codigoeval);
		$res = $this->db->update('CEN_EVALUACION',$datos);
        
        if (!$res) {
			throw new Exception('Trámite de Evaluación no pudo ser actualizado correctamente.');
		}else{
            $datos['IDEVAL'] = $this->obtenerUltimoIDEval();
        }
		return $datos;
	}

    private function obtenerUltimoIDProducto()
	{
		$query = $this->db->select('MAX(IDPRODUCTO) as id')
			->from('CEN_PRODUCTOS')
			->get();
		if (!$query) {
			throw new Exception('Error al encontrar el Código de producto');
		}
		return intval($query->row()->id);
	}

	public function crearDocumentosxProducto(int $idEval, int $idProducto)
	{
		$arrayDocumentos = [
			'Ficha Técnica',
			'Registro Sanitario',
			'Análisis Microbiológico y fisicoquímico',
			'Análisis Nutricional o Carta de Declaración Jurada',
			'Estudio de Vida Útil',
			'Etiqueta Original',
			'Sustento Claims',
			'Excel de Advertencias Publicitarias',
			'Certificación de Calidad de Planta',
			'Certificado Orgánico Vigente'
		];
		$data = [
			'IDEVAL' => $idEval,
			'IDPRODUCTO' => $idProducto,
			'ESTADOTRAMITE' =>0
		];
		$numeroDocumento = 1;
		foreach($arrayDocumentos as $documento){
			$data['IDTRAMITE'] = $numeroDocumento;
			$data['NOMBRETRAMITE'] = $documento;
			$res = $this->db->insert('CEN_DOCUMENTOS',$data);
			$numeroDocumento++;
		}
	}

    public function crearProducto(int $ideval,
                                string $codigoProducto,
                                int $categoria,
                                string $regsanitario,
                                string $fechaemision,
                                string $fechavencimiento,
                                string $descripcion,
                                string $nombreProducto,
                                int $marca,
                                string $presentacion,
                                int $fabricante,
                                int $pais,
                                string $direccionfabricante,
                                string $vidautil,
                                int $estado):array
	{
        $fechaEmisionAlt = \Carbon\Carbon::createFromFormat('d/m/Y', $fechaemision, 'America/Lima')->format('Y-m-d');
		$fechaVencimientoAlt = \Carbon\Carbon::createFromFormat('d/m/Y', $fechavencimiento, 'America/Lima')->format('Y-m-d');
        $datos = [
			'CODIGOPRODUCTO' => $codigoProducto,
			'CATEGORIA' => $categoria,
			'REGSANITARIO' => $regsanitario,
			'FECHAEMISION' => $fechaEmisionAlt,
			'FECHAVENCIMIENTO' => $fechaVencimientoAlt,
			'DESCRIPCION' => $descripcion,
			'NOMBREPRODUCTO' => $nombreProducto,
			'MARCA' => $marca,
			'PRESENTACION' => $presentacion,
			'FABRICANTE' => $fabricante,
			'PAISFABRICANTE' => $pais,
			'VIDAUTIL' => $vidautil,
            'ESTADO' => $estado,
			'DIRECCIONFABRICANTE'=>$direccionfabricante
		];

		$res = $this->db->insert('CEN_PRODUCTOS',$datos);

        if (!$res) {
			throw new Exception('Error al intentar guardar el producto.');
		}else{
            $datos['IDPRODUCTO'] = $this->obtenerUltimoIDProducto();
			$this->crearDocumentosxProducto($ideval,$datos['IDPRODUCTO']);
            $eval = $this->db->update('CEN_EVALUACION', ['IDPRODUCTO' => $datos['IDPRODUCTO']],['IDEVAL' => $ideval]);
        }
		return $datos;
	}

	public function buscarEvaluaciones(string $codigo_eval,string $codigo_rs,int $estado_eval):array
	{
		$this->db->select('CEN_EVALUACION.IDEVAL AS IDEVAL,
						CEN_EVALUACION.ESTADO AS ESTADOEVAL,
						CEN_PRODUCTOS.NOMBREPRODUCTO AS NOMBREPRODUCTO,
						CEN_PRODUCTOS.REGSANITARIO AS CODIGORS');
		$this->db->from('CEN_EVALUACION');
		$this->db->join('CEN_PRODUCTOS','CEN_PRODUCTOS.IDPRODUCTO = CEN_EVALUACION.IDPRODUCTO','left');

		if(!empty($codigo_eval)){
			$this->db->where('CEN_EVALUACION.IDEVAL',$codigo_eval);
		}
		if(!empty($codigo_rs)){
			$this->db->where('CEN_EVALUACION.IDEVAL',$codigo_rs);
		}
		if($estado_eval != 0){
            $this->db->where('CEN_EVALUACION.ESTADO',$estado_eval);
        }
		$this->db->order_by('CEN_EVALUACION.IDEVAL','desc');
		$query = $this->db->get();

		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}


    public function guardarDocumento(string $idtramite,string $ideval,string $idproducto,string $nombre,string $ruta,int $estado):array
	{	
		$documentos = $this->documentos_tramite($ideval,$idtramite);
		$set = 0;
		$datos = [
			'IDTRAMITE'=>$idtramite,
			'IDEVAL'=>$ideval,
			'IDPRODUCTO'=>$idproducto
		];
		if($documentos->DOC1==null){
			$datos['NOMBREDOCUMENTO'] = $nombre;
			$datos['RUTADOCUMENTO'] = $ruta;
			$datos['ESTADOTRAMITE'] = $estado;
			$set=1;
		}
		if($documentos->DOC2==null && $set == 0){
			$datos['NOMBREDOCUMENTO2'] = $nombre;
			$datos['RUTADOCUMENTO2'] = $ruta;
			$datos['ESTADOTRAMITE2'] = $estado;
			$set=1;
		}
		if($documentos->DOC3==null && $set == 0){
			$datos['NOMBREDOCUMENTO3'] = $nombre;
			$datos['RUTADOCUMENTO3'] = $ruta;
			$datos['ESTADOTRAMITE3'] = $estado;
			$set=1;
		}
		if($documentos->DOC4==null && $set == 0){
			$datos['NOMBREDOCUMENTO4'] = $nombre;
			$datos['RUTADOCUMENTO4'] = $ruta;
			$datos['ESTADOTRAMITE4'] = $estado;
			$set=1;
		}
		if($documentos->DOC5==null && $set == 0){
			$datos['NOMBREDOCUMENTO5'] = $nombre;
			$datos['RUTADOCUMENTO5'] = $ruta;
			$datos['ESTADOTRAMITE5'] = $estado;
			$set=1;
		}
		if ($set==0) {
			throw new Exception('No se puede agregar mas documentos.');
		}

		$this->db->where('IDEVAL',$ideval);
		$this->db->where('IDPRODUCTO',$idproducto);
		$this->db->where('IDTRAMITE',$idtramite);
		$res = $this->db->update('CEN_DOCUMENTOS',$datos);
        if (!$res) {
			throw new Exception('Documento no pudo ser actualizado correctamente.');
		}
		return $datos;
	}

	public function cargar_categorias_cenco(){
		$this->db->select('MCATEGORIACLIENTE.CCATEGORIACLIENTE AS IDCATEGORIA,
					MCATEGORIACLIENTE.DCATEGORIACLIENTE AS NOMBRECATEGORIA');
		$this->db->from('MCATEGORIACLIENTE');
		$this->db->where('MCATEGORIACLIENTE.CCLIENTE','00654');
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	public function cargar_marcas_cenco(){
		$this->db->select('MMARCASXCLIENTE.CMARCA AS IDMARCA,
						MMARCASXCLIENTE.DMARCA AS NOMBREMARCA');
		$this->db->from('MMARCASXCLIENTE');
		$this->db->where('MMARCASXCLIENTE.CCLIENTE','00654');
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	public function cargarProducto(string $producto,string $rsproducto)
	{
		$this->db->select('MPRODUCTOCLIENTE.CPRODUCTOFS AS IDPRODUCTO,
						MPRODUCTOCLIENTE.DNOMBREPRODUCTO AS NOMBREPRODUCTO,
						MPRODUCTOCLIENTE.DPRODUCTOCLIENTE AS DESCRIPCION,
						MPRODUCTOCLIENTE.DPRESENTACION AS PRESENTACION,
						MPRODUCTOCLIENTE.DDIRECCIONFABRICANTE AS DIRECCIONFABRICANTE,
						MPRODUCTOCLIENTE.DREGISTROSANITARIO AS REGSANITARIO,
						MPRODUCTOCLIENTE.FINICIOREGSANITARIO AS FECHAINICIO,
						MPRODUCTOCLIENTE.FFINREGSANITARIO AS FECHAFIN,
						MPRODUCTOCLIENTE.VIDAUTIL AS VIDAUTIL,
						MPRODUCTOCLIENTE.SREGISTRO AS ESTADO,
						MPRODUCTOCLIENTE.CPRODUCTOCLIENTE AS CODIGOPRODUCTO,
						MMARCAXCLIENTE.DMARCA AS MARCA,
						MFABRICANTEXCLIENTE.DFABRICANTE AS NOMBREFABRICANTE,
						TTABLA.DREGISTRO AS PAIS,
						MCATEGORIACLIENTE.DCATEGORIACLIENTE AS CATEGORIA,
						MPRODUCTOCLIENTE.CMARCA AS IDMARCA,
						MPRODUCTOCLIENTE.CFABRICANTE AS IDFABRICANTE,
						MPRODUCTOCLIENTE.ZCPAISFABRICANTE AS IDPAIS,
						MPRODUCTOCLIENTE.ZCTIPOCATEGORIAPRODUCTO AS IDCATEGORIA
		');
		$this->db->from('MPRODUCTOCLIENTE');
		$this->db->join('MMARCAXCLIENTE','MMARCAXCLIENTE.CMARCA = MPRODUCTOCLIENTE.CMARCA');
		$this->db->join('MFABRICANTEXCLIENTE','MFABRICANTEXCLIENTE.CFABRICANTE = MPRODUCTOCLIENTE.CFABRICANTE');
		$this->db->join('TTABLA','TTABLA.CTIPO = MPRODUCTOCLIENTE.ZCPAISFABRICANTE');
		$this->db->join('MCATEGORIACLIENTE','MCATEGORIACLIENTE.CCATEGORIACLIENTE = MPRODUCTOCLIENTE.ZCTIPOCATEGORIAPRODUCTO');
		$this->db->where('MPRODUCTOCLIENTE.CCLIENTE','00654');
		$this->db->where('MMARCAXCLIENTE.CCLIENTE','00654');
		$this->db->where('MFABRICANTEXCLIENTE.CCLIENTE','00654');
		if(!empty($producto)){
			$this->db->where('MPRODUCTOCLIENTE.CPRODUCTOFS',$producto);
		}
		if(!empty($rsproducto)){
			$this->db->where('MPRODUCTOCLIENTE.DREGISTROSANITARIO',$rsproducto);
		}
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		$resultado = $query->result();
		return ($query->num_rows() > 0) ? $resultado[0] : [];

	}

	public function datos_adicionales(string $idcategoria, string $idmarca, string $idfabricante, string $idpais):array
	{
		while(strlen($idcategoria)<3){
			$idcategoria = '0'.$idcategoria;
		}
		$this->db->select('DCATEGORIACLIENTE AS NOMBRECATEGORIA');
		$this->db->where('CCATEGORIACLIENTE',$idcategoria);
		$query1 = $this->db->get('MCATEGORIACLIENTE');
		foreach($query1->result() as $row){
			$nombrecategoria = $row->NOMBRECATEGORIA;
		}
		while(strlen($idmarca)<5){
			$idmarca = '0'.$idmarca;
		}
		$this->db->select('DMARCA AS NOMBREMARCA');
		$this->db->where('CMARCA',$idmarca);
		$this->db->where('CCLIENTE','00654');
		$query2 = $this->db->get('MMARCAXCLIENTE');
		foreach($query2->result() as $row){
			$nombremarca = $row->NOMBREMARCA;
		}
		while(strlen($idfabricante)<4){
			$idfabricante = '0'.$idfabricante;
		}
		$this->db->select('DFABRICANTE AS NOMBREFABRICANTE');
		$this->db->where('CFABRICANTE',$idfabricante);
		$this->db->where('CCLIENTE','00654');
		$query3 = $this->db->get('MFABRICANTEXCLIENTE');
		foreach($query3->result() as $row){
			$nombrefabricante = $row->NOMBREFABRICANTE;
		}
		while(strlen($idpais)<3){
			$idpais = '0'.$idpais;
		}
		$this->db->select('DREGISTRO AS NOMBREPAIS');
		$this->db->where('CTIPO',$idpais);
		$query4 = $this->db->get('TTABLA');
		foreach($query4->result() as $row){
			$nombrepais = $row->NOMBREPAIS;
		}

		$data = ['NOMBRECATEGORIA' => $nombrecategoria,
			'NOMBREMARCA' =>$nombremarca,
			'NOMBREFABRICANTE' => $nombrefabricante,
			'NOMBREPAIS' => $nombrepais
		];

		return $data;
	}

	public function documentos_tramite(int $ideval, int $idtramite)
	{
		$this->db->select('CEN_DOCUMENTOS.NOMBREDOCUMENTO AS DOC1,
						CEN_DOCUMENTOS.RUTADOCUMENTO AS RUTA1,
						CEN_DOCUMENTOS.ESTADOTRAMITE AS ESTADO1,
						CEN_DOCUMENTOS.NOMBREDOCUMENTO2 AS DOC2,
						CEN_DOCUMENTOS.RUTADOCUMENTO2 AS RUTA2,
						CEN_DOCUMENTOS.ESTADOTRAMITE2 AS ESTADO2,
						CEN_DOCUMENTOS.NOMBREDOCUMENTO3 AS DOC3,
						CEN_DOCUMENTOS.RUTADOCUMENTO3 AS RUTA3,
						CEN_DOCUMENTOS.ESTADOTRAMITE3 AS ESTADO3,
						CEN_DOCUMENTOS.NOMBREDOCUMENTO4 AS DOC4,
						CEN_DOCUMENTOS.RUTADOCUMENTO4 AS RUTA4,
						CEN_DOCUMENTOS.ESTADOTRAMITE4 AS ESTADO4,
						CEN_DOCUMENTOS.NOMBREDOCUMENTO5 AS DOC5,
						CEN_DOCUMENTOS.RUTADOCUMENTO5 AS RUTA5,
						CEN_DOCUMENTOS.ESTADOTRAMITE5 AS ESTADO5,
						CEN_DOCUMENTOS.IDEVAL AS IDEVAL,
						CEN_DOCUMENTOS.IDTRAMITE AS IDTRAMITE
		');
		$this->db->from('CEN_DOCUMENTOS');
		$this->db->where('CEN_DOCUMENTOS.IDEVAL',$ideval);
		$this->db->where('CEN_DOCUMENTOS.IDTRAMITE',$idtramite);
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		$resultado = $query->result();
		return ($query->num_rows() > 0) ? $resultado[0] : [];
	}

	public function eliminar_documento(int $ideval, int $idtramite, int $numeroDoc)
	{
		$documentos = $this->documentos_tramite($ideval,$idtramite);
		$arrayDoc = [];
		if($numeroDoc!=1){
			$arrayDoc['NOMBREDOCUMENTO'] = $documentos->DOC1;
			$arrayDoc['RUTADOCUMENTO'] = $documentos->RUTA1;
			$arrayDoc['ESTADOTRAMITE'] = $documentos->ESTADO1;
		}else{
			$arrayDoc['NOMBREDOCUMENTO'] = null;
			$arrayDoc['RUTADOCUMENTO'] = null;
			$arrayDoc['ESTADOTRAMITE'] = null;
		}
		if($numeroDoc!=2){
			$arrayDoc['NOMBREDOCUMENTO2'] = $documentos->DOC2;
			$arrayDoc['RUTADOCUMENTO2'] = $documentos->RUTA2;
			$arrayDoc['ESTADOTRAMITE2'] = $documentos->ESTADO2;
		}else{
			$arrayDoc['NOMBREDOCUMENTO2'] = null;
			$arrayDoc['RUTADOCUMENTO2'] = null;
			$arrayDoc['ESTADOTRAMITE2'] = null;
		}
		if($numeroDoc!=3){
			$arrayDoc['NOMBREDOCUMENTO3'] = $documentos->DOC3;
			$arrayDoc['RUTADOCUMENTO3'] = $documentos->RUTA3;
			$arrayDoc['ESTADOTRAMITE3'] = $documentos->ESTADO3;
		}else{
			$arrayDoc['NOMBREDOCUMENTO3'] = null;
			$arrayDoc['RUTADOCUMENTO3'] = null;
			$arrayDoc['ESTADOTRAMITE3'] = null;
		}
		if($numeroDoc!=4){
			$arrayDoc['NOMBREDOCUMENTO4'] = $documentos->DOC4;
			$arrayDoc['RUTADOCUMENTO4'] = $documentos->RUTA4;
			$arrayDoc['ESTADOTRAMITE4'] = $documentos->ESTADO4;
		}else{
			$arrayDoc['NOMBREDOCUMENTO4'] = null;
			$arrayDoc['RUTADOCUMENTO4'] = null;
			$arrayDoc['ESTADOTRAMITE4'] = null;
		}
		if($numeroDoc!=5){
			$arrayDoc['NOMBREDOCUMENTO5'] = $documentos->DOC5;
			$arrayDoc['RUTADOCUMENTO5'] = $documentos->RUTA5;
			$arrayDoc['ESTADOTRAMITE5'] = $documentos->ESTADO5;
		}else{
			$arrayDoc['NOMBREDOCUMENTO5'] = null;
			$arrayDoc['RUTADOCUMENTO5'] = null;
			$arrayDoc['ESTADOTRAMITE5'] = null;
		}
		
		$this->db->set($arrayDoc);
		$this->db->where('IDEVAL',$ideval);
		$this->db->where('IDTRAMITE',$idtramite);
		$this->db->update('CEN_DOCUMENTOS');

		$documentosResult = $this->documentos_tramite($ideval,$idtramite);

		return $documentosResult;
	}

	public function cambiar_estado(int $ideval, int $idtramite, int $estado)
	{
		$data['ESTADOTRAMITE'] = $estado;
		$this->db->set($data);
		$this->db->where('IDEVAL',$ideval);
		$this->db->where('IDTRAMITE',$idtramite);
		$this->db->update('CEN_DOCUMENTOS');

		$documentosResult = $this->documentos_tramite($ideval,$idtramite);

		return $documentosResult;
	}

}