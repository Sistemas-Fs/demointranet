<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mcartas extends CI_Model {
	function __construct() {
		parent:: __construct();	
		$this->load->library('session');
    }
    
   /** CARTAS **/ 
    public function getclientecarta() { // Visualizar Clientes con propuestas en CBO	
        
        $procedure = "call sp_appweb_pt_getclientecarta()";
		$query = $this->db-> query($procedure);
        
        if ($query->num_rows() > 0) {

            $listas = '<option value="0" selected="selected">::Elegir</option>';
            
            foreach ($query->result() as $row)
            {
                $listas .= '<option value="'.$row->IDCLIE.'">'.$row->DESCRIPCLIE.'</option>';  
            }
               return $listas;
        }{
            return false;
        }	
    }
    public function getbuscarcarta($parametros) { // Tabla de busqueda de clientes
        
		$procedure = "call sp_appweb_pt_getbuscarcarta(?,?,?,?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}	
    }	
    public function getbuscarCliente() { // Tabla de busqueda de clientes
        
		$procedure = "call sp_appweb_pt_getclienteinterno()";
		$query = $this->db-> query($procedure);
        
        if ($query->num_rows() > 0) {

            $listas = '<option value="0" selected="selected">::Elegir</option>';
            
            foreach ($query->result() as $row)
            {
                $listas .= '<option value="'.$row->CCLIENTE.'">'.$row->RAZONSOCIAL.'</option>';  
            }
               return $listas;
        }{
            return false;
        }	
    }
    public function getnrocarta($parametros) { // Obtener numero de propuesta
        
		$procedure = "call sp_appweb_pt_getnrocarta(?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}	
    }
    public function subirArchivo($parametros) {  // Registrar tramite PT
        $this->db->trans_begin();

        $procedure = "call sp_appweb_pt_cartasubirarchivo(?,?,?,?);";
        $query = $this->db->query($procedure,$parametros);

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }
        else{
            $this->db->trans_commit();
            return $query->result(); 
        }   
    } 
    public function setcarta($parametros) {  // Registrar informe PT
        $this->db->trans_begin();

        $procedure = "call sp_appweb_pt_setcarta(?,?,?,?,?,?,?,?);";
        $query = $this->db->query($procedure,$parametros);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
            return $query->result(); 
        }   
    } 
    public function getcartaevaluar($parametros) { // Visualizar NRO propuestas a evaluaren CBO	
        
        $procedure = "call sp_appweb_pt_getcartaevaluar(?)";
		$query = $this->db-> query($procedure,$parametros);
        
        if ($query->num_rows() > 0) {

            $listas = '<option value="" selected="selected">::Elegir</option>';
            
            foreach ($query->result() as $row)
            {
                $listas .= '<option value="'.$row->IDPTPROPU.'">'.$row->NROPROPU.'</option>';  
            }
               return $listas;
        }{
            return false;
        }	
    }
    public function getbuscardetacarta($parametros) {	// Recupera Listado de Documentos Propuestas
        $procedure = "call sp_appweb_pt_buscardetacarta(?)";
        $query = $this->db-> query($procedure,$parametros);

        if ($query->num_rows() > 0) { 
            return $query->result();
        }{
            return False;
        }		   
    }
    public function guardar_multiarchivocarta($datos) { // Guardar multiples archivos
        return $this->db->insert("pt_detcartaarch", $datos);
    }
    public function deldetcarta($item) { // Eliminar detalle propuesta    
        $this->db->trans_begin();
        $this->db->delete('pt_detcartaarch', array('item' => $item));
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_commit();
            return true; 
        }
    }


}
?>