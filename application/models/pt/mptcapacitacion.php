<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mptcapacitacion extends CI_Model {
	function __construct() {
		parent:: __construct();	
		$this->load->library('session');
    }
    
   /** CAPACITACIONES **/ 
   
    public function getclientecapa() { // Visualizar Servicios en CBO	
        
        $sql = "select distinct b.ccliente as 'IDCLIE', LTRIM (b.drazonsocial) as 'DESCRIPCLIE' from pt_capacitacion a join mcliente b on b.ccliente = a.ccliente order by DESCRIPCLIE ;";
        $query  = $this->db->query($sql);

        if ($query->num_rows() > 0) {

            $listas = '<option value="" selected="selected">::Elegir</option>';
            
            foreach ($query->result() as $row)
            {
                $listas .= '<option value="'.$row->IDCLIE.'">'.$row->DESCRIPCLIE.'</option>';  
            }
            return $listas;
        }{
            return false;
        }	
    }
    public function getbuscarcapa($parametros) { // Recupera Listado de informes
        
        $procedure = "call sp_appweb_pt_buscarcapacitacion(?,?,?,?)";
        $query = $this->db-> query($procedure,$parametros);

        if ($query->num_rows() > 0) { 
            return $query->result();
        }{
            return False;
        }	
    }
    public function getclienteinternopt() { // Visualizar Clientes en CBO	
        
        $procedure = "call sp_appweb_pt_getclienteinterno()";
		$query = $this->db-> query($procedure);
        
        if ($query->num_rows() > 0) {

            $listas = '<option value="0">::Elegir</option>';
            
            foreach ($query->result() as $row)
            {
                $listas .= '<option value="'.$row->CCLIENTE.'">'.$row->RAZONSOCIAL.'</option>';  
            }
               return $listas;
        }{
            return false;
        }	
    }	
    public function getptcursos() { // Visualizar Servicios en CBO	
        
        $sql = "select idptcapacurso,descripcion from pt_capacurso order by descripcion ;";
        $query  = $this->db->query($sql);

        if ($query->num_rows() > 0) {

            $listas = '<option value="" selected="selected">::Elegir</option>';
            
            foreach ($query->result() as $row)
            {
                $listas .= '<option value="'.$row->idptcapacurso.'">'.$row->descripcion.'</option>';  
            }
            return $listas;
        }{
            return false;
        }	
    }
    public function setregcapacitacion($parametros) {  // Registrar capa
        $this->db->trans_begin();

        $procedure = "call sp_appweb_pt_setregcapacitacion(?,?,?,?,?,?,?,?,?);";
        $query = $this->db->query($procedure,$parametros);

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_commit();
            return $query->result(); 
        }   
    } 	
    public function adjArchivo($parametros) {  // Registrar tramite PT
        $this->db->trans_begin();

        $procedure = "call sp_appweb_ptcapa_subiradjunto(?,?,?,?,?,?,?);";
        $query = $this->db->query($procedure,$parametros);

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }
        else{
            $this->db->trans_commit();
            return $query->result(); 
        }   
    } 
    public function getlistaradjuntos($idptcapacitacion) { // Listar Ensayos	
        $sql = "select idptcapacitacion,idptcapaadjunto,tipoadjunto,descripcion,ruta_capaadj,archivo_capaadj,descarchivo_capaadj 
                from pt_capaadjunto where idptcapacitacion = ".$idptcapacitacion.";";
        $query  = $this->db->query($sql);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
    }
    public function setcapacurso($parametros) {  // Registrar capa
        $this->db->trans_begin();

        $procedure = "call sp_appweb_pt_setcapacurso(?,?,?);";
        $query = $this->db->query($procedure,$parametros);

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_commit();
            return $query->result(); 
        }   
    } 
    public function delcapa($parametros) {	// Eliminar de propuesta
        $this->db->trans_begin();

        $procedure = "call sp_appweb_ptcapa_delcapa(?);";
        $query = $this->db->query($procedure,$parametros);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
            return $query->result(); 
        }   
    } 
    public function delcapaadj($parametros) {	// Eliminar de propuesta
        $this->db->trans_begin();

        $procedure = "call sp_appweb_ptcapa_delcapaadj(?,?);";
        $query = $this->db->query($procedure,$parametros);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
            return $query->result(); 
        }   
    } 
    public function getvalidarnrodni($parametros) { // Validar numero de ruc del cliente
        $procedure = "call sp_appweb_ptcapa_validarnrodni(?)";
        $query = $this->db-> query($procedure,$parametros);

        if ($query->num_rows() > 0) { 
            return $query->result();
        }{
            return False;
        }		   
    }    
    public function getadministrado($id_administrado) { // Visualizar 
        $sql = "select id_administrado, isnull(nombres,'') as 'nombres', isnull(ape_paterno,'') as 'ape_paterno', isnull(ape_materno,'') as 'ape_materno', isnull(email,'') as 'email', isnull(fono_celular,'') as 'fono_celular' from adm_administrado 
                where id_administrado = ".$id_administrado.";";
        $query  = $this->db->query($sql);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
            return False;
        }		   
    } 
    public function getlistparticipante($parametros) { // Recupera Listado de Propuestas        
		$procedure = "call usp_at_capa_getlistparticipante(?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}	
    }
    public function setparticipante($parametros) {  // Registrar informe PT
        $this->db->trans_begin();

        $procedure = "call sp_appweb_ptcapa_setparticipante(?,?,?,?,?);";
        $query = $this->db->query($procedure,$parametros);

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_commit();
            return $query->result(); 
        }   
    } 
    public function delparti($parametros) {	// Eliminar de propuesta
        $this->db->trans_begin();

        $procedure = "call usp_at_capa_deleteparticipante(?);";
        $query = $this->db->query($procedure,$parametros);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
            return $query->result(); 
        }   
    } 

     



    public function getexpositorcapa() { // Visualizar Servicios en CBO	
        
        $procedure = "call usp_at_capa_getcboexpositor()";
		$query = $this->db-> query($procedure);
        
        if ($query->num_rows() > 0) {
            $listas = '<option value="0" selected="selected">::Elija</option>';            
            foreach ($query->result() as $row){
                $listas .= '<option value="'.$row->id_capaexpo.'">'.$row->datosrazonsocial.'</option>';  
            }
               return $listas;
        }{
            return false;
        }	
    }    
	public function getclienteinternoat() { // Visualizar Clientes con propuestas en CBO			 
		$procedure = "call usp_at_capa_getclienteinternoat()";
		$query = $this->db-> query($procedure);
		 
		if ($query->num_rows() > 0) {
 
			 $listas = '<option value="0" selected="selected">::Elegir</option>';
			 
			 foreach ($query->result() as $row)
			 {
				 $listas .= '<option value="'.$row->CCLIENTE.'">'.$row->RAZONSOCIAL.'</option>';  
			 }
				return $listas;
		}{
			 return false;
		}	
	}
    public function buscar_establexcliente($parametros) { // Visualizar Servicios en CBO	
        
        $procedure = "call usp_at_capa_getestablexcliente(?)";
		$query = $this->db-> query($procedure,$parametros);
        
        if ($query->num_rows() > 0) {

            $listas = '<option value="%" selected="selected">::Elija</option>';            
            foreach ($query->result() as $row){
                $listas .= '<option value="'.$row->CESTABLE.'">'.$row->DESCRIPESTA.'</option>';  
            }
               return $listas;
        }{
            return false;
        }	
    }
    public function gettemaxcurso($parametros) { // Visualizar Servicios en CBO	
        
        $procedure = "call usp_at_capa_getcbotemaxcurso(?)";
		$query = $this->db-> query($procedure,$parametros);
        
        if ($query->num_rows() > 0) {
            $listas = '<option value="0" selected="selected">::Elija</option>';            
            foreach ($query->result() as $row){
                $listas .= '<option value="'.$row->id_capamodulo.'">'.$row->desc_modulo.'</option>';  
            }
               return $listas;
        }{
            return false;
        }	
    }
    public function setcapadet($parametros) {  // Registrar informe PT
        $this->db->trans_begin();

        $procedure = "call usp_at_capa_setcapadet(?,?,?,?,?,?,?,?,?,?,?,?,?);";
        $query = $this->db->query($procedure,$parametros);

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_commit();
            return $query->result(); 
        }   
    } 
    public function subirTaller($parametros) {  // Registrar tramite PT
        $this->db->trans_begin();

        $procedure = "call usp_at_capa_subirTaller(?,?,?);";
        $query = $this->db->query($procedure,$parametros);

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }
        else{
            $this->db->trans_commit();
            return $query->result(); 
        }   
    } 
    public function subirExamen($parametros) {  // Registrar tramite PT
        $this->db->trans_begin();

        $procedure = "call usp_at_capa_subirExamen(?,?,?);";
        $query = $this->db->query($procedure,$parametros);

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }
        else{
            $this->db->trans_commit();
            return $query->result(); 
        }   
    } 
    public function subirLista($parametros) {  // Registrar tramite PT
        $this->db->trans_begin();

        $procedure = "call usp_at_capa_subirLista(?,?,?);";
        $query = $this->db->query($procedure,$parametros);

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }
        else{
            $this->db->trans_commit();
            return $query->result(); 
        }   
    }
    public function subirCerti($parametros) {  // Registrar tramite PT
        $this->db->trans_begin();

        $procedure = "call usp_at_capa_subirCerti(?,?,?);";
        $query = $this->db->query($procedure,$parametros);

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }
        else{
            $this->db->trans_commit();
            return $query->result(); 
        }   
    }
    public function getlistcapadet($parametros) { // Recupera Listado de Propuestas        
		$procedure = "call usp_at_capa_getlistcapadet(?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}	
    }
    public function setprograma($parametros) {  // Registrar informe PT
        $this->db->trans_begin();

        $procedure = "call usp_at_capa_setprograma(?,?,?,?,?,?,?,?);";
        $query = $this->db->query($procedure,$parametros);

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_commit();
            return $query->result(); 
        }   
    } 
    public function getlistprograma($parametros) { // Recupera Listado de Propuestas        
		$procedure = "call usp_at_capa_getlistprograma(?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}	
    }
    public function delprogram($parametros) {	// Eliminar de propuesta
        $this->db->trans_begin();

        $procedure = "call usp_at_capa_deleteprogram(?);";
        $query = $this->db->query($procedure,$parametros);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
            return $query->result(); 
        }   
    } 

}
?>