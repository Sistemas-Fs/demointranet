<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class mclieusuarios extends CI_Model {
	function __construct() {
		parent:: __construct();	
		$this->load->library('session');
    }
    public function getbuscarclieusuario($parametros) { // Buscar Cotizacion	
        $procedure = "call sp_appweb_pt_getbuscarclieusuario(?)";
		$query = $this->db-> query($procedure,$parametros);

		if ($query->num_rows() > 0) { 
			return $query->result();
		}{
			return False;
		}		
    }	
	public function setregclieusu($parametros) { // Registrar Vacaciones		
        $this->db->trans_begin();
    
        $procedure = "call sp_appweb_pt_setregclieusu(?,?,?,?,?,?,?,?,?,?)";
        $query = $this->db-> query($procedure,$parametros); 
           
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }
        else{
            $this->db->trans_commit();
            if ($query->num_rows() > 0) {
                return $query->result();
            }{
                return False;
            }	
        }   
    }
    public function setbloquear($id_usuario) { // Recuperar Password
		
        $data = array(
            "tipo_acceso" => 'B',
        );
        $this->db->where("id_usuario", $id_usuario);
		if($this->db->update("segu_usuario", $data)){
			return TRUE;
		}
    }
    public function setdesbloquear($id_usuario) { // Recuperar Password
		
        $data = array(
            "tipo_acceso" => 'N',
        );
        $this->db->where("id_usuario", $id_usuario);
		if($this->db->update("segu_usuario", $data)){
			return TRUE;
		}
    }
    public function delUsuario($id_usuario) { // Eliminar detalle propuesta    
        $this->db->trans_begin();
        $this->db->delete('segu_usuario_rol', array('id_usuario' => $id_usuario));
        $this->db->delete('segu_usuario', array('id_usuario' => $id_usuario));
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_commit();
            return true; 
        }
    } 





}
?>