<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mprocal extends CI_Model {
	function __construct() {
		parent:: __construct();	
		$this->load->library('session');
    }
    
   /** CARTAS **/ 
    
    public function setprocal($parametros) {  // Registrar evaluacion PT
        $this->db->trans_begin();

        $procedure = "call sp_appweb_pt_setprocal(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
        $query = $this->db->query($procedure,$parametros);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
            return $query->result(); 
        }   
    }


}
?>