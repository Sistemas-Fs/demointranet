<?php

/**
 * Class mcheclistcab
 */
class mcheclist extends CI_Model
{

	/**
	 * mcheclistcab constructor.
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * @param $id
	 * @return array|mixed|object|null
	 */
	public function buscar($id)
	{
		$query = $this->db->select('*')
			->from('mchecklist')
			->where('cchecklist', $id)
			->get();
		if (!$query) {
			return null;
		}
		return ($query->num_rows() > 0) ? $query->row() : null;
	}

	/**
	 * @param $ccompania
	 * @return array|array[]|object|object[]
	 */
	public function recursos_area($ccompania)
	{
		$query = $this->db->select('
			carea as id,
			darea as text
		')->from('marea')
			->where('ccompania', $ccompania)
			->where('sregistro', 'A')
			->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @param $ccompania
	 * @return array|array[]|object|object[]
	 */
	public function recursos_servicio($ccompania, $carea)
	{
		$query = $this->db->select('
			cservicio as id,
			dservicio as text
		')->from('mservicio')
			->where('ccompania', $ccompania)
			->where('carea', $carea)
			->where('sregistro', 'A')
			->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @return array|array[]|object|object[]
	 */
	public function recursos_no_conformidad()
	{
		$query = $this->db->select('
			mvalor.cvalor as id,
			mvalor.dvalor as text,
			mvalor.*
		')->from('mvalor')
			->where('sregistro', 'A')
			->where('stipovalor', 'N')
			->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @param $csistema
	 * @return array|array[]|object|object[]
	 */
	public function recursos_sistema($ccompania, $carea, $cservicio)
	{
		$query = $this->db->select('
			msistema.csistema as id,
			msistema.dsistema as text,
			msistema.*
		')->from('msistema')
			->where('msistema.ccompania', $ccompania)
			->where('msistema.carea', $carea)
			->where('msistema.cservicio', $cservicio)
			->where('msistema.sregistro', 'A')
			->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @param $csistema
	 * @return array|array[]|object|object[]
	 */
	public function recursos_organismo($csistema)
	{
		$query = $this->db->select('
			mnorma.cnorma as id,
			mnorma.dnorma as text,
			mnorma.*
		')->from('mnorma')
			->where('mnorma.csistema', $csistema)
			->where('mnorma.sregistro', 'A')
			->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @param $id
	 * @return array|array[]|object|object[]
	 */
	public function buscarNorma($id)
	{
		$query = $this->db->select('*')
			->from('mnorma')
			->where('cnorma', $id)
			->get();
		if (!$query) {
			return null;
		}
		return ($query->num_rows() > 0) ? $query->row() : null;
	}


	public function recursos_rubro($cnorma)
	{
		$query = $this->db->select('
			msubnorma.csubnorma as id,
			msubnorma.dsubnorma as text,
			msubnorma.*
		')->from('msubnorma')
			->where('msubnorma.cnorma', $cnorma)
			->where('msubnorma.sregistro', 'A')
			->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @param $busqueda
	 * @param $ccompania
	 * @param $carea
	 * @param $cservicio
	 * @return array
	 */
	public function lista($ccompania, $busqueda, $carea, $cservicio)
	{
		$sql = "
			SELECT DISTINCT mchecklist.cchecklist,   
				mchecklist.ccompania,   
				mchecklist.dchecklist,   
				mchecklist.schecklist,   
				mchecklist.cvalornoconformidad,   
				mchecklist.scerrado,   
				mchecklist.suso,   
				mchecklist.tcreacion,   
				marea.darea,   
				mservicio.dservicio,   
				mnorma.dnorma,   
				msubnorma.dsubnorma,   
				msistema.dsistema,   
				mchecklist.speso,
			  	mchecklist.sregistro ";
		$sql .= " FROM mchecklist,   
				marea,   
				mnorma,   
				mservicio,   
				msubnorma,   
				msistema ";
		$sql .= " WHERE ( msubnorma.cnorma = mnorma.cnorma ) and  
				( mchecklist.cnorma = msubnorma.cnorma ) and  
			 	( mchecklist.csubnorma = msubnorma.csubnorma ) and  
			 	( mservicio.ccompania = mchecklist.ccompania ) and  
			 	( mservicio.carea = mchecklist.carea ) and  
			 	( mservicio.cservicio = mchecklist.cservicio ) and  
			 	( marea.ccompania = mservicio.ccompania ) and  
			 	( marea.carea = mservicio.carea ) and  
			 	( mnorma.csistema = msistema.csistema ) and
			 	(mchecklist.cchecklist like '{$busqueda}' OR
			 	mchecklist.dchecklist like '{$busqueda}' OR
			 	mservicio.dservicio like '{$busqueda}' ) AND
			  	mchecklist.ccompania = '{$ccompania}'
			and ( mchecklist.carea = '{$carea}' or '{$carea}' = '%')
			and ( mchecklist.cservicio = '{$cservicio}' or '{$cservicio}' = '%') ";
		$sql .= " GROUP BY mchecklist.cchecklist,
				mchecklist.ccompania,
			 	mchecklist.dchecklist,
			 	mchecklist.schecklist,
			 	mchecklist.cvalornoconformidad,
			 	mchecklist.scerrado,
			 	mchecklist.suso,
			 	mchecklist.tcreacion,
			 	marea.darea,
			 	mservicio.dservicio,
			 	msistema.dsistema,
			 	mnorma.dnorma,
			 	msubnorma.dsubnorma,
			 	mchecklist.speso,
				mchecklist.sregistro
				ORDER BY mchecklist.cchecklist ASC
		";
		$query = $this->db->query($sql);
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @return string
	 * @throws Exception
	 */
	public function obtenerNuevoID()
	{
		$query = $this->db->select('MAX(CCHECKLIST) as id')
			->from('mchecklist')
			->get();
		if (!$query) {
			throw new Exception('Error al encontrar el Código');
		}
		return str_pad(intval($query->row()->id) + 1, 5, '0', STR_PAD_LEFT);
	}

	/**
	 * @param $CCHECKLIST
	 * @param $CNORMA
	 * @param $CSUBNORMA
	 * @param $CCOMPANIA
	 * @param $CAREA
	 * @param $CSERVICIO
	 * @param $DCHECKLIST
	 * @param $SCHECKLIST
	 * @param $CVALORNOCONFORMIDAD
	 * @param $SCERRADO
	 * @param $SUSO
	 * @param $CUSUARIO
	 * @param $SREGISTRO
	 * @param $CGRUPOCRITERIONINSPECCION
	 * @param $SPESO
	 * @param $SPROPIEDAD
	 * @return array
	 * @throws Exception
	 */
	public function guardar(
		$CCHECKLIST,
		$CNORMA,
		$CSUBNORMA,
		$CCOMPANIA,
		$CAREA,
		$CSERVICIO,
		$DCHECKLIST,
		$SCHECKLIST,
		$CVALORNOCONFORMIDAD,
		$SCERRADO,
		$SUSO,
		$CUSUARIO,
		$SREGISTRO,
		$CGRUPOCRITERIONINSPECCION,
		$SPESO,
		$SPROPIEDAD)
	{
		$data = [
			'CCHECKLIST' => $CCHECKLIST,
			'CNORMA' => $CNORMA,
			'CSUBNORMA' => $CSUBNORMA,
			'CCOMPANIA' => $CCOMPANIA,
			'CAREA' => $CAREA,
			'CSERVICIO' => $CSERVICIO,
			'DCHECKLIST' => $DCHECKLIST,
			'SCHECKLIST' => $SCHECKLIST,
			'CVALORNOCONFORMIDAD' => $CVALORNOCONFORMIDAD,
			'SCERRADO' => $SCERRADO,
			'SUSO' => $SUSO,
			'CUSUARIOCREA' => $CUSUARIO,
			'TCREACION' => date('Y-m-d H:i:s'),
			'CUSUARIOMODIFICA' => $CUSUARIO,
			'TMODIFICACION' => date('Y-m-d H:i:s'),
			'SREGISTRO' => $SREGISTRO,
			'CGRUPOCRITERIONINSPECCION' => $CGRUPOCRITERIONINSPECCION,
			'SPESO' => $SPESO,
			'SPROPIEDAD' => $SPROPIEDAD,
		];
		$res = (empty($CCHECKLIST)) ? $this->crear($data) : $this->actualizar($CCHECKLIST, $data);
		if (!$res) {
			throw new Exception('Error al intentar guardar el checklist.');
		}
		return $data;
	}

	/**
	 * Crear
	 * @param array $datos
	 * @return bool
	 * @throws Exception
	 */
	public function crear(array &$datos): bool
	{
		$datos['CCHECKLIST'] = $this->obtenerNuevoID();
		$datos['CUSUARIOMODIFICA'] = null;
		$datos['TMODIFICACION'] = null;
		$res = $this->db->insert('mchecklist', $datos);
		if (!$res) {
			throw new Exception('El checklist no pudo ser creado correctamente.');
		}
		return $res;
	}

	/**
	 * Editar
	 * @param string $id
	 * @param array $datos
	 * @return bool
	 * @throws Exception
	 */
	public function actualizar(string $id, array &$datos): bool
	{
		if (empty($id)) {
			throw new Exception('El AR no es valido para actualizar.');
		}
		$datos['TMODIFICACION'] = date('Y-m-d H:i:s');
		if (isset($datos['TCREACION'])) unset($datos['TCREACION']);
		if (isset($datos['CUSUARIOCREA'])) unset($datos['CUSUARIOCREA']);
		$res = $this->db->update('mchecklist', $datos, ['CCHECKLIST' => $id]);
		if (!$res) {
			throw new Exception('El checklist no pudo ser actualizado correctamente.');
		}
		return $res;
	}

	/**
	 * @param $idCheckList
	 * @return mixed|string
	 * @throws Exception
	 */
	public function eliminar($idCheckList)
	{
		$this->db->delete('mrchecklistformula', ['CCHECKLIST' => $idCheckList]);
		$this->db->delete('MRCHECKLISTCLIENTE', ['CCHECKLIST' => $idCheckList]);
		$this->db->delete('MREQUISITOCHECKLIST', ['CCHECKLIST' => $idCheckList]);
		$query = $this->db->delete('mchecklist', ['CCHECKLIST' => $idCheckList]);
		if (!$query) {
			throw new Exception('Error al eliminar el checklist.');
		}
		return $query;
	}

}
