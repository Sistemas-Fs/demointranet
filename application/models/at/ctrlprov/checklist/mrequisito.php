<?php

/**
 * Class mrequisito
 */
class mrequisito extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * @param $cchecklist
	 * @return string
	 * @throws Exception
	 */
	public function obtenerNuevoID($cchecklist)
	{
		$query = $this->db->select('CREQUISITOCHECKLIST')
			->from('mrequisitochecklist')
			->where('cchecklist', $cchecklist)
			->order_by('CREQUISITOCHECKLIST', 'DESC')
			->limitAnyWhere(1)
			->get();
		if (!$query) {
			throw new Exception('Error al generar el ID del requisito');
		}
		return ($query->num_rows() > 0)
			? str_pad($query->row()->CREQUISITOCHECKLIST + 1, 4, '0', STR_PAD_LEFT)
			: '0001';
	}

	/**
	 * @param $cchecklist
	 * @param $crequisitochecklist
	 * @return array|array[]|object|object[]
	 */
	public function obtenerRequisitos($cchecklist, $crequisitochecklist = null)
	{
		$this->db->select('
			mrequisitochecklist.*,
         	mchecklist.cvalornoconformidad
		');
		$this->db->from('mrequisitochecklist');
		$this->db->join('mchecklist', 'mrequisitochecklist.cchecklist = mchecklist.cchecklist', 'inner');
		$this->db->where('mrequisitochecklist.CCHECKLIST', $cchecklist);
		if (!empty($crequisitochecklist)) {
			$this->db->where('mrequisitochecklist.crequisitochecklist', $crequisitochecklist);
		}
		$this->db->order_by('mrequisitochecklist.cchecklist', 'ASC');
		$this->db->order_by('mrequisitochecklist.dordenlista', 'ASC');
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * Se obtiene los requisitos padres
	 * @param $cchecklist
	 * @param $crequisitochecklist
	 */
	public function obtenerPadres($cchecklist)
	{
		$query = $this->db->select('
			mrequisitochecklist.crequisitochecklist,
         	mrequisitochecklist.drequisito,
         	mrequisitochecklist.cchecklist,
         	mrequisitochecklist.dnumerador
		')->from('mrequisitochecklist')
			->where('mrequisitochecklist.cchecklist', $cchecklist)
			->where('mrequisitochecklist.sregistro', 'A')
			->where('mrequisitochecklist.svalor', 'N')
			->order_by('mrequisitochecklist.dordenlista', 'ASC')
			->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * Valores de requisito
	 * @return array|array[]|object|object[]
	 */
	public function obtenerValores()
	{
		$query = $this->db->select('
			mvalor.cvalor,
			mvalor.dvalor,
			mvalor.stipovalor,
			mvalor.nvalor,
			mvalor.stipollenado
		')->from('mvalor')
			->where('sregistro', 'A')
			->where('stipovalor', 'C')
			->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @param $CCHECKLIST
	 * @param $CREQUISITOCHECKLIST
	 * @return array|mixed|object|null
	 */
	public function buscar($CCHECKLIST, $CREQUISITOCHECKLIST)
	{
		$query = $this->db->select('*')
			->from('MREQUISITOCHECKLIST')
			->where('CCHECKLIST', $CCHECKLIST)
			->where('CREQUISITOCHECKLIST', $CREQUISITOCHECKLIST)
			->limitAnyWhere(1)
			->order_by('DORDENLISTA', 'DESC')
			->get();
		if (!$query) {
			return null;
		}
		return ($query->num_rows() > 0) ? $query->row() : null;
	}

	/**
	 * @param $CCHECKLIST
	 * @param $CREQUISITOCHECKLISTPADRE
	 * @return array|mixed|object|null
	 */
	public function buscarPadre($CCHECKLIST, $CREQUISITOCHECKLISTPADRE)
	{
		return $this->buscar($CCHECKLIST, $CREQUISITOCHECKLISTPADRE);
	}

	/**
	 * @param $CCHECKLIST
	 * @param $CREQUISITOCHECKLISTPADRE
	 * @return array|mixed|object|null
	 */
	public function buscarHermanoMayor($CCHECKLIST, $CREQUISITOCHECKLISTPADRE)
	{
		return $this->buscarHermanos($CCHECKLIST, $CREQUISITOCHECKLISTPADRE, true);
	}

	/**
	 * @param $CCHECKLIST
	 * @param $CREQUISITOCHECKLISTPADRE
	 * @param boolean $soloMayor default false
	 * @return array|mixed|object|null
	 */
	public function buscarHermanos($CCHECKLIST, $CREQUISITOCHECKLISTPADRE, $soloMayor = false)
	{
		$this->db->select('*');
		$this->db->from('MREQUISITOCHECKLIST');
		$this->db->where('CCHECKLIST', $CCHECKLIST);
		$this->db->where('CREQUISITOCHECKLISTPADRE', $CREQUISITOCHECKLISTPADRE);
		if ($soloMayor) {
			$this->db->limitAnyWhere(1);
		}
		$this->db->order_by('DORDENLISTA', 'DESC');
		$query = $this->db->get();
		if (!$query) {
			return ($soloMayor) ? null : [];
		}
		if ($soloMayor) {
			return ($query->num_rows() > 0) ? $query->row() : null;
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @param $CCHECKLIST
	 * @param $CREQUISITOCHECKLIST
	 * @param $DREQUISITO
	 * @param $DNORMATIVA
	 * @param $CREQUISITOCHECKLISTPADRE
	 * @param $SREQUISITO
	 * @param $SVALOR
	 * @param $NORDENLISTA
	 * @param $DORDENLISTA
	 * @param $DNUMERADOR
	 * @param $CVALOR
	 * @param $SEXCLUYENTE
	 * @param $NPESO
	 * @param $CUSUARIO
	 * @param $SREGISTRO
	 * @param $SDISMINUYECALIFICACION
	 * @param $FC_OBLIGATORIO
	 * @param $idclase
	 * @param $SPRODPESO
	 * @param $SPRODVENCE
	 * @param $BAJA_MITAD
	 * @param $SOC
	 * @return array
	 * @throws Exception
	 */
	public function guardar(
		$CCHECKLIST,
		$CREQUISITOCHECKLIST,
		$DREQUISITO,
		$DNORMATIVA,
		$CREQUISITOCHECKLISTPADRE,
		$SREQUISITO,
		$SVALOR,
		$NORDENLISTA,
		$DORDENLISTA,
		$DNUMERADOR,
		$CVALOR,
		$SEXCLUYENTE,
		$NPESO,
		$CUSUARIO,
		$SREGISTRO,
		$SDISMINUYECALIFICACION,
		$FC_OBLIGATORIO,
		$idclase,
		$SPRODPESO,
		$SPRODVENCE,
		$BAJA_MITAD,
		$SOC
	)
	{
		$data = [
			'CCHECKLIST' => $CCHECKLIST,
			'CREQUISITOCHECKLIST' => $CREQUISITOCHECKLIST,
			'DREQUISITO' => $DREQUISITO,
			'DNORMATIVA' => $DNORMATIVA,
			'CREQUISITOCHECKLISTPADRE' => $CREQUISITOCHECKLISTPADRE,
			'SREQUISITO' => $SREQUISITO,
			'SVALOR' => $SVALOR,
			'NORDENLISTA' => $NORDENLISTA,
			'DORDENLISTA' => $DORDENLISTA,
			'DNUMERADOR' => $DNUMERADOR,
			'CVALOR' => $CVALOR,
			'SEXCLUYENTE' => $SEXCLUYENTE,
			'NPESO' => $NPESO,
			'CUSUARIOCREA' => $CUSUARIO,
			'TCREACION' => date('Y-m-d H:i:s'),
			'CUSUARIOMODIFICA' => $CUSUARIO,
			'TMODIFICACION' => date('Y-m-d H:i:s'),
			'SREGISTRO' => $SREGISTRO,
			'SDISMINUYECALIFICACION' => $SDISMINUYECALIFICACION,
			'FC_OBLIGATORIO' => $FC_OBLIGATORIO,
			'idclase' => $idclase,
			'SPRODPESO' => $SPRODPESO,
			'SPRODVENCE' => $SPRODVENCE,
			'BAJA_MITAD' => $BAJA_MITAD,
			'SOC' => $SOC,
		];
		$res = (!empty($CCHECKLIST) && !empty($CREQUISITOCHECKLIST))
			? $this->actualizar($CCHECKLIST, $CREQUISITOCHECKLIST, $data)
			: $this->crear($data);
		if (!$res) {
			throw new Exception('Error al intentar guardar el checklist.');
		}
		return $data;
	}

	/**
	 * Crear
	 * @param array $datos
	 * @return bool
	 * @throws Exception
	 */
	public function crear(array &$datos): bool
	{
		$datos['CREQUISITOCHECKLIST'] = $this->obtenerNuevoID($datos['CCHECKLIST']);
		$datos['CUSUARIOMODIFICA'] = null;
		$datos['TMODIFICACION'] = null;
		$res = $this->db->insert('MREQUISITOCHECKLIST', $datos);
		if (!$res) {
			throw new Exception('El requisito no pudo ser creado correctamente.');
		}
		return $res;
	}

	/**
	 * Editar
	 * @param string $id
	 * @param array $datos
	 * @return bool
	 * @throws Exception
	 */
	public function actualizar(string $CCHECKLIST, string $CREQUISITOCHECKLIST, array $datos): bool
	{
		$datos['TMODIFICACION'] = date('Y-m-d H:i:s');
		if (isset($datos['TCREACION'])) unset($datos['TCREACION']);
		if (isset($datos['CUSUARIOCREA'])) unset($datos['CUSUARIOCREA']);
		if (isset($datos['CCHECKLIST'])) unset($datos['CCHECKLIST']);
		if (isset($datos['CREQUISITOCHECKLIST'])) unset($datos['CREQUISITOCHECKLIST']);
		$res = $this->db->update('MREQUISITOCHECKLIST', $datos, [
			'CCHECKLIST' => $CCHECKLIST,
			'CREQUISITOCHECKLIST' => $CREQUISITOCHECKLIST
		]);
		if (!$res) {
			throw new Exception('El requisito no pudo ser actualizado correctamente.');
		}
		return $res;
	}

	/**
	 * @param string $CCHECKLIST
	 * @param string $CREQUISITOCHECKLIST
	 * @return mixed|string
	 * @throws Exception
	 */
	public function eliminar(string $CCHECKLIST, string $CREQUISITOCHECKLIST)
	{
		$query = $this->db->delete('MREQUISITOCHECKLIST', ['CCHECKLIST' => $CCHECKLIST, 'CREQUISITOCHECKLIST' => $CREQUISITOCHECKLIST]);
		if (!$query) {
			throw new Exception('El requisito no pudo ser eliminado.');
		}
		return $query;
	}

}
