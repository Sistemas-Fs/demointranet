<?php


class mquejapeligros extends CI_Model
{

	/**
	 * mquejapeligros constructor.
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * @return null
	 */
	public function obtenerArchivo($codigo)
	{
		$this->db->select('*');
		$this->db->from('ttabla');
		$this->db->where('ctipo', $codigo);
		$query = $this->db->get();
		if (!$query) {
			return null;
		}
		return ($query->num_rows() > 0) ? $query->row() : null;
	}

	/**
	 * @return null
	 */
	public function obtenerArchivoPeligro()
	{
		$this->db->select('DREGISTRO');
		$this->db->from('ttabla');
		$this->db->where('ctipo', '569');
		$query = $this->db->get();
		if (!$query) {
			return null;
		}
		return ($query->num_rows() > 0) ? $query->row()->DREGISTRO : null;
	}

	/**
	 * @return null
	 */
	public function obtenerArchivoQuejas()
	{
		$this->db->select('DREGISTRO');
		$this->db->from('ttabla');
		$this->db->where('ctipo', '568');
		$query = $this->db->get();
		if (!$query) {
			return null;
		}
		return ($query->num_rows() > 0) ? $query->row()->DREGISTRO : null;
	}

}
