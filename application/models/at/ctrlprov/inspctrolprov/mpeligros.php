<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class Mpeligros
 */
class Mpeligros extends CI_Model
{

	/**
	 * mpeligros constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
	}

	/**
	 * Lista de peligros
	 * @param $cauditoria
	 * @param $fservicio
	 * @return array|array[]|object|object[]
	 */
	public function listar($cauditoria, $fservicio)
	{
		$this->db->select('ppeligro.*');
		$this->db->from('ppeligro');
		$this->db->where('ppeligro.cauditoriainspeccion', $cauditoria);
		$this->db->where('ppeligro.fservicio', $fservicio);
		$this->db->order_by('ppeligro.nordenpeligro', 'ASC');
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @param $CAUDITORIAINSPECCION
	 * @param $FSERVICIO
	 * @return int
	 */
	public function obtenerOrden($CAUDITORIAINSPECCION, $FSERVICIO)
	{
		$this->db->select('MAX(NORDENPELIGRO) AS Orden');
		$this->db->from('ppeligro');
		$this->db->where('CAUDITORIAINSPECCION', $CAUDITORIAINSPECCION);
		$this->db->where('FSERVICIO', $FSERVICIO);
		$query = $this->db->get();
		if (!$query) {
			return 1;
		}
		return ($query->num_rows() > 0) ? $query->row()->Orden + 1 : 1;
	}

	/**
	 * Guarda el peligro
	 */
	public function guardar($CAUDITORIAINSPECCION, $FSERVICIO,
							$DPRODUCTO, $DPELIGROCLIENTE, $DPELIGROPROVEEDOR,
							$DPELIGROINSPECCION, $DOBSERVACION, $CUSUARIO, $SREGISTRO)
	{
		$datos = [
			"CAUDITORIAINSPECCION" => $CAUDITORIAINSPECCION,
			"FSERVICIO" => $FSERVICIO,
			"DPRODUCTO" => $DPRODUCTO,
			"DPELIGROCLIENTE" => $DPELIGROCLIENTE,
			"DPELIGROPROVEEDOR" => $DPELIGROPROVEEDOR,
			"DPELIGROINSPECCION" => $DPELIGROINSPECCION,
			"DOBSERVACION" => $DOBSERVACION,
			"SREGISTRO" => $SREGISTRO,
		];
		// Se busca el correlativo del peligro
		$datos['NORDENPELIGRO'] = $this->obtenerOrden($CAUDITORIAINSPECCION, $FSERVICIO);

		$datos['CUSUARIOCREA'] = $CUSUARIO;
		$datos['TCREACION'] = date('Y-m-d h:i:s');
		$datos['CUSUARIOMODIFICA'] = null;
		$datos['TMODIFICACION'] = null;

		$resp = $this->db->insert('ppeligro', $datos);
		if (!$resp) {
			throw new Exception('Error al crear el pligro');
		}
		return $resp;
	}

}
