<?php

/**
 * Class mresumen_contacto
 */
class mresumen_contacto extends CI_Model
{

	/**
	 * mresumen_contacto constructor.
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * @return string
	 */
	private function obtenerContactoKey()
	{
		$query = $this->db->select('MAX(CCONTACTO) as id')
			->from('MCONTACTO')
			->get();
		if (!$query) {
			return '00001';
		}
		$id = intval($query->row()->id) + 1;
		return ($query->num_rows() > 0) ? str_pad($id, 5, '0', STR_PAD_LEFT) : '00001';
	}

	/**
	 * Se obtiene los cambios del resumen
	 * @param $cauditoria
	 * @param $fservicio
	 * @return array
	 */
	public function lista($cauditoria, $fservicio)
	{
		$sql = "
			SELECT presponsableaudinsp.cauditoriainspeccion,   
				 presponsableaudinsp.fservicio,   
				 presponsableaudinsp.ccontacto,   
				 presponsableaudinsp.cusuariocrea,   
				 presponsableaudinsp.tcreacion,   
				 presponsableaudinsp.cusuariomodifica,   
				 presponsableaudinsp.tmodificacion,   
				 presponsableaudinsp.sregistro,   
				 mcontacto.dapepat,   
				 mcontacto.dapemat,   
				 mcontacto.dnombre,   
				 mcontacto.dapepat+' '+mcontacto.dapemat+' '+mcontacto.dnombre as ap_nom,   
				 mcontacto.dcargocontacto,   
				 mcontacto.dmail,   
				 mcontacto.dtelefono,
			     mcontacto.ccliente,
			     MCLIENTE.NRUC + ' ' + MCLIENTE.DRAZONSOCIAL AS cliente
			FROM presponsableaudinsp   
				 INNER JOIN mcontacto ON presponsableaudinsp.ccontacto = mcontacto.ccontacto
				 INNER JOIN mcliente ON mcontacto.CCLIENTE = mcliente.CCLIENTE
		    WHERE ( ( presponsableaudinsp.cauditoriainspeccion = '{$cauditoria}' ) AND  
				 ( presponsableaudinsp.fservicio = '{$fservicio}' ) )
		";
		$query = $this->db->query($sql);
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @param $CCONTACTO
	 * @param $CAUDITORIAINSPECCION
	 * @param $FSERVICIO
	 * @param $CCLIENTE
	 * @param $CESTABLECIMIENTO
	 * @param $DAPEPAT
	 * @param $DAPEMAT
	 * @param $DNOMBRE
	 * @param $DNOMBRECARGA
	 * @param $DCARGOCONTACTO
	 * @param $DMAIL
	 * @param $DTELEFONO
	 * @param $CUSUARIO
	 * @return bool
	 */
	public function guardar($CCONTACTO, $CAUDITORIAINSPECCION, $FSERVICIO, $CCLIENTE, $CESTABLECIMIENTO, $DAPEPAT, $DAPEMAT, $DNOMBRE, $DCARGOCONTACTO, $DMAIL, $DTELEFONO, $CUSUARIO)
	{
		$fecha = date('Y-m-d H:i:s');

		$DNOMBRECARGA = $DAPEPAT . ' ' . $DAPEMAT . ' ' . $DNOMBRE;
		$data = [
			'CCONTACTO' => $CCONTACTO,
			'CCLIENTE' => $CCLIENTE,
			'CESTABLECIMIENTO' => $CESTABLECIMIENTO,
			'DAPEPAT' => $DAPEPAT,
			'DAPEMAT' => $DAPEMAT,
			'DNOMBRE' => $DNOMBRE,
			'DNOMBRECARGA' => $DNOMBRECARGA,
			'DCARGOCONTACTO' => $DCARGOCONTACTO,
			'DMAIL' => $DMAIL,
			'DTELEFONO' => $DTELEFONO,
			'CUSUARIOCREA' => $CUSUARIO,
			'TCREACION' => $fecha,
			'TMODIFICACION' => null,
			'CUSUARIOMODIFICA' => null,
			'SREGISTRO' => 'A',
		];

		if (empty($CCONTACTO)) {
			$CCONTACTO = $this->obtenerContactoKey();
			$data['CCONTACTO'] = $CCONTACTO;
			$scontacto = $this->db->insert('MCONTACTO', $data);
			$this->db->insert('presponsableaudinsp', [
				'CAUDITORIAINSPECCION' => $CAUDITORIAINSPECCION,
				'FSERVICIO' => $FSERVICIO,
				'CCONTACTO' => $CCONTACTO,
				'CUSUARIOCREA' => $CUSUARIO,
				'TCREACION' => $fecha,
				'TMODIFICACION' => null,
				'CUSUARIOMODIFICA' => null,
				'SREGISTRO' => 'A',
			]);
		} else {
			unset($data['CCONTACTO']);
			unset($data['CCLIENTE']);
			$scontacto = $this->db->update('MCONTACTO', $data, ['CCONTACTO' => $CCONTACTO, 'CCLIENTE' => $CCLIENTE]);
		}

		if (!$scontacto) {
			return false;
		}

		return true;
	}

	/**
	 * @param $CAUDITORIAINSPECCION
	 * @param $FSERVICIO
	 * @param $CCONTACTO
	 * @return array|mixed|object|null
	 */
	public function buscarResponsable($CAUDITORIAINSPECCION, $FSERVICIO, $CCONTACTO)
	{
		$query = $this->db->select('*')
			->from('presponsableaudinsp')
			->where('CAUDITORIAINSPECCION', $CAUDITORIAINSPECCION)
			->where('FSERVICIO', $FSERVICIO)
			->where('CCONTACTO', $CCONTACTO)
			->get();
		if (!$query) {
			return null;
		}
		return ($query->num_rows() > 0) ? $query->row() : null;
	}

	/**
	 * @param $CCONTACTO
	 * @return array|mixed|object|null
	 */
	public function buscarContacto($CCONTACTO)
	{
		$query = $this->db->select('*')
			->from('MCONTACTO')
			->where('CCONTACTO', $CCONTACTO)
			->get();
		if (!$query) {
			return null;
		}
		return ($query->num_rows() > 0) ? $query->row() : null;
	}

	/**
	 * @param $CCLIENTE
	 * @return array|mixed|object|null
	 */
	public function buscarCliente($CCLIENTE)
	{
		$query = $this->db->select('*')
			->from('MCLIENTE')
			->where('CCLIENTE', $CCLIENTE)
			->get();
		if (!$query) {
			return null;
		}
		return ($query->num_rows() > 0) ? $query->row() : null;
	}

	/**
	 * @param $search
	 * @param $CCLIENTE
	 * @param $CPROVEEDORCLIENTE
	 * @param $CMAQUILADORCLIENTE
	 * @return array|array[]|object|object[]
	 */
	public function listaClientes($search, $CCLIENTE, $CPROVEEDORCLIENTE, $CMAQUILADORCLIENTE)
	{
		$this->db->select("
			MCLIENTE.CCLIENTE AS id,
			(MCLIENTE.NRUC + ' ' + MCLIENTE.DRAZONSOCIAL) AS text
		");
		$this->db->from('MCLIENTE');
		$this->db->where('SREGISTRO', 'A');
		$this->db->where_in('STIPOCLIENTE', ['C', 'P']);
		$this->db->group_start();
		$this->db->like('NRUC', $search);
		$this->db->or_like('DRAZONSOCIAL', $search);
		$this->db->group_end();
//		$this->db->group_start();
//		$this->db->where('CCLIENTE', $CCLIENTE);
//		if (!empty($CPROVEEDORCLIENTE)) {
//			$this->db->or_where('CCLIENTE', $CPROVEEDORCLIENTE);
//		}
//		if (!empty($CMAQUILADORCLIENTE)) {
//			$this->db->or_where('CCLIENTE', $CMAQUILADORCLIENTE);
//		}
//		$this->db->group_end();
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @param $search
	 * @param $CCLIENTE
	 * @param $CPROVEEDORCLIENTE
	 * @param $CMAQUILADORCLIENTE
	 * @return array|array[]|object|object[]
	 */
	public function autocompletado($search, $CCLIENTE, $CPROVEEDORCLIENTE, $CMAQUILADORCLIENTE)
	{
		$this->db->select("
			mcontacto.CCONTACTO as id,
			mcontacto.dapepat + ' ' + mcontacto.dapemat + ' ' + mcontacto.dnombre as text,   
			mcontacto.dcargocontacto,   
			mcontacto.dmail,   
			mcontacto.dtelefono,
			mcontacto.ccliente,
			MCLIENTE.NRUC + ' ' + MCLIENTE.DRAZONSOCIAL AS cliente
		");
		$this->db->from('mcontacto');
		$this->db->join('MCLIENTE', 'MCONTACTO.CCLIENTE = MCLIENTE.CCLIENTE', 'INNER');
		$this->db->group_start();
		$this->db->where('mcontacto.CCLIENTE', $CCLIENTE);
		if (!empty($CPROVEEDORCLIENTE)) {
			$this->db->or_where('mcontacto.CCLIENTE', $CPROVEEDORCLIENTE);
		}
		if (!empty($CPROVEEDORCLIENTE)) {
			$this->db->or_where('mcontacto.CCLIENTE', $CMAQUILADORCLIENTE);
		}
		$this->db->group_end();
		$this->db->group_start();
		$this->db->like('DAPEPAT', $search);
		$this->db->or_like('DAPEMAT', $search);
		$this->db->or_like('DNOMBRE', $search);
		$this->db->or_like('DMAIL', $search);
		$this->db->group_end();
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

}
