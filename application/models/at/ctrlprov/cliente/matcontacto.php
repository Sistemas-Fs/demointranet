<?php

/**
 * Class matcontacto
 */
class matcontacto extends CI_Model
{

	/**
	 * matcontacto constructor.
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * @param $CCLIENTE
	 * @return array|array[]|object|object[]
	 */
	public function lista($CCLIENTE, $CESTABLECIMIENTO = null)
	{
		$this->db->select('*');
		$this->db->from('MCONTACTO');
		$this->db->where('CCLIENTE', $CCLIENTE);
		if (!empty($CESTABLECIMIENTO)) {
			$this->db->where('CESTABLECIMIENTO', $CESTABLECIMIENTO);
		} else {
//			$this->db->group_start();
			$this->db->where('cestablecimiento IS NULL', null, false);
			$this->db->or_where('cestablecimiento =', "''", false);
//			$this->db->group_end();
		}
		$this->db->order_by('CCONTACTO DESC');
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @return string
	 */
	private function obtenerId()
	{
		$query = $this->db->select('MAX(CCONTACTO) as id')
			->from('MCONTACTO')
			->get();
		if (!$query) {
			return '00001';
		}
		$id = intval($query->row()->id) + 1;
		return ($query->num_rows() > 0) ? str_pad($id, 5, '0', STR_PAD_LEFT) : '00001';
	}

	/**
	 * @param $CCONTACTO
	 * @param $CCLIENTE
	 * @param $CESTABLECIMIENTO
	 * @param $DAPEPAT
	 * @param $DAPEMAT
	 * @param $DNOMBRE
	 * @param $DCARGOCONTACTO
	 * @param $DMAIL
	 * @param $DTELEFONO
	 * @param $CUSUARIO
	 * @return bool
	 * @throws Exception
	 */
	public function guardar($CCONTACTO, $CCLIENTE, $CESTABLECIMIENTO, $DAPEPAT, $DAPEMAT, $DNOMBRE, $DCARGOCONTACTO, $DMAIL, $DTELEFONO, $CUSUARIO)
	{
		$fecha = date('Y-m-d H:i:s');
		$DNOMBRECARGA = $DAPEPAT . ' ' . $DAPEMAT . ' ' . $DNOMBRE;
		$data = [
			'CCONTACTO' => $CCONTACTO,
			'CCLIENTE' => $CCLIENTE,
			'CESTABLECIMIENTO' => $CESTABLECIMIENTO,
			'DAPEPAT' => $DAPEPAT,
			'DAPEMAT' => $DAPEMAT,
			'DNOMBRE' => $DNOMBRE,
			'DNOMBRECARGA' => $DNOMBRECARGA,
			'DCARGOCONTACTO' => $DCARGOCONTACTO,
			'DMAIL' => $DMAIL,
			'DTELEFONO' => $DTELEFONO,
			'CUSUARIOCREA' => $CUSUARIO,
			'TCREACION' => $fecha,
			'TMODIFICACION' => null,
			'CUSUARIOMODIFICA' => null,
			'SREGISTRO' => 'A',
		];

		if (empty($CCONTACTO)) {
			$CCONTACTO = $this->obtenerId();
			$data['CCONTACTO'] = $CCONTACTO;
			$scontacto = $this->db->insert('MCONTACTO', $data);
		} else {
			unset($data['CCONTACTO']);
			unset($data['CCLIENTE']);
			unset($data['CUSUARIOCREA']);
			unset($data['TCREACION']);
			$data['TMODIFICACION'] = $fecha;
			$data['CUSUARIOMODIFICA'] = $CUSUARIO;
			$scontacto = $this->db->update('MCONTACTO', $data, ['CCONTACTO' => $CCONTACTO, 'CCLIENTE' => $CCLIENTE]);
		}

		if (!$scontacto) {
			throw new Exception('Error al guardar el contacto.');
		}

		return true;
	}

	/**
	 * @param $CCONTACTO
	 * @return array|mixed|object|null
	 */
	public function buscar($CCONTACTO)
	{
		$query = $this->db->from('MCONTACTO')
			->where('CCONTACTO', $CCONTACTO)
			->get();
		if (!$query) {
			return null;
		}
		return ($query->num_rows() > 0) ? $query->row() : null;
	}

}
