<?php


class matproveedor extends CI_Model
{

	/**
	 * matproveedor constructor.
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * @param $ccliente
	 * @return array|array[]|object|object[]
	 */
	public function lista($ccliente)
	{
		$this->db->select("
		 	mcp.ccliente ,
			mcp.cproveedorcliente,
			mc.drazonsocial,
			mc.nruc,
			mc.ddireccioncliente,
			'' as 'SPACE'
		");
		$this->db->from('mrclienteproveedor mcp');
		$this->db->join('mcliente mc', 'mc.ccliente = mcp.cproveedorcliente', 'inner');
		$this->db->where('mcp.ccliente', $ccliente);
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @param $ccliente
	 * @return array|array[]|object|object[]
	 */
	public function listaProveedores($ccliente)
	{
		$this->db->select("
			 DISTINCT
			mc.drazonsocial,
			mc.ddireccioncliente,
			mc.drepresentante,
			mc.nruc,
			mc.ccliente,
			'' as 'SPACE'
		");
		$this->db->from('mcliente mc');
//		$this->db->from('mrclienteproveedor mcp');
//		$this->db->join('mcliente mc', 'mc.ccliente = mcp.cproveedorcliente', 'inner');
//		$this->db->where('mcp.ccliente !=', $ccliente);
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @param $CCLIENTE
	 * @param $CPROVEEDORCLIENTE
	 * @param $CUSUARIO
	 * @param $sregistro
	 * @param $FESNUEVO
	 * @return mixed
	 * @throws Exception
	 */
	public function guardar($CCLIENTE, $CPROVEEDORCLIENTE, $CUSUARIO, $sregistro, $FESNUEVO)
	{
		$data = [
			'CCLIENTE' => $CCLIENTE,
			'CPROVEEDORCLIENTE' => $CPROVEEDORCLIENTE,
			'CUSUARIOCREA' => $CUSUARIO,
			'TCREACION' => date('Y-m-d H:i:s'),
			'SREGISTRO' => $sregistro,
			'FESNUEVO' => $FESNUEVO,
		];
		if (empty($CESTABLECIMIENTO)) {
			$save = $this->db->insert('MRCLIENTEPROVEEDOR', $data);
		} else {
			unset($data['CCLIENTE']);
			unset($data['CPROVEEDORCLIENTE']);
			unset($data['CUSUARIOCREA']);
			unset($data['TCREACION']);
			$data['TMODIFICACION'] = date('Y-m-d');
			$data['CUSUARIOMODIFICA'] = $CUSUARIO;
			$save = $this->db->update('MRCLIENTEPROVEEDOR', $data, ['CCLIENTE' => $CCLIENTE, 'CPROVEEDORCLIENTE' => $CPROVEEDORCLIENTE]);
		}

		if (!$save) {
			throw new Exception('Error al guardar el establecimiento.');
		}

		return true;
	}

}
