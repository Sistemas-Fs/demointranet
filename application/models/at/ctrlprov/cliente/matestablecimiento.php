<?php

class matestablecimiento extends CI_Model
{

	/**
	 * matestablecimiento constructor.
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * @param $CCLIENTE
	 * @return array|array[]|object|object[]
	 */
	public function lista($CCLIENTE)
	{
		$this->db->select("
			distinct C.CCLIENTE AS 'COD_CLIENTE', 
			E.CESTABLECIMIENTO AS 'COD_ESTABLE', 
			ISNULL(E.DESTABLECIMIENTO,'') AS 'DESCRIPESTABLE', 
			ISNULL(E.DDIRECCION,'') AS 'DIRECCION', 
			ISNULL(E.CPAISESTABLECIMIENTO,'') AS 'PAIS', 
			ISNULL(E.CUBIGEO,'') AS 'UBIGEO', 
			ISNULL(E.DCIUDADESTABLECIMIENTO,'') AS 'CIUDAD', 
			ISNULL(E.DESTADOESTABLECIMIENTO,'') AS 'ESTESTABLE', 
			ISNULL(E.DZIPESTABLECIMIENTO,'') AS 'DZIP', 
			(select (z.ddepartamento+' - '+z.dprovincia+' - '+z.ddistrito) as dubigeo from tubigeo z where z.cubigeo = E.CUBIGEO) as 'DUBIGEO', 
			ISNULL(E.FCE,'') AS 'FCE', ISNULL(E.ECP,'') AS 'ECP', 
			ISNULL(E.FFRN,'') AS 'FFRN', 
			ISNULL(E.DRESPONSABLECALIDAD,'') AS 'RESPONCALIDAD', 
			ISNULL(E.DCARGORESPONSABLECALIDAD,'') AS 'CARGOCALIDAD', 
			ISNULL(E.DEMAILRESPONSABLECALIDAD,'') AS 'EMAILCALIDAD',
			ISNULL(E.NTELEFONOREPONSABLECALIDAD,'') AS 'TELEFONOCALIDAD', 
			E.SREGISTRO AS 'ESTADO',
			'' as 'SPACE',
			ISNULL(E.dreferencia,'') AS 'REFERENCIA', 
			ISNULL(E.dtelefono,'') AS 'TELEFONO', 
			C.DRAZONSOCIAL
		");
		$this->db->from('MESTABLECIMIENTOCLIENTE E');
		$this->db->join('MCLIENTE C', 'C.CCLIENTE = E.CCLIENTE', 'inner');
//		$this->db->join('pt_clienteestablecimientos b', 'b.ccliente = c.ccliente and b.cestablecimiento = e.cestablecimiento', 'inner');
		$this->db->where('C.CCLIENTE', $CCLIENTE);
		$this->db->order_by('E.CESTABLECIMIENTO DESC');
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @return string
	 */
	public function obtenerId()
	{
		$query = $this->db->select("
			right(('000000'+cast(((cast(isnull(max(CESTABLECIMIENTO),0) as integer) + 1)) as char(6))),6) as id
		")
			->from('MESTABLECIMIENTOCLIENTE')
			->get();
		return ($query && $query->num_rows() > 0) ? $query->row()->id : '00001';
	}

	/**
	 * @param $parametros
	 * @return mixed
	 */
	public function guardar($CCLIENTE, $CESTABLECIMIENTO, $DESTABLECIMIENTO, $CTIPOESTABLECIMIENTO, $DDIRECCION,
							$DZIPESTABLECIMIENTO, $CUSUARIO, $SREGISTRO, $DTELEFONO, $CUBIGEO, $CPAISESTABLECIMIENTO,
							$DCIUDADESTABLECIMIENTO, $DESTADOESTABLECIMIENTO, $DREFERENCIA)
	{
		$data = [
			'CCLIENTE' => $CCLIENTE,
			'CESTABLECIMIENTO' => $CESTABLECIMIENTO,
			'DESTABLECIMIENTO' => $DESTABLECIMIENTO,
			'CTIPOESTABLECIMIENTO' => $CTIPOESTABLECIMIENTO,
			'DDIRECCION' => $DDIRECCION,
			'DZIPESTABLECIMIENTO' => $DZIPESTABLECIMIENTO,
			'CUSUARIOCREA' => $CUSUARIO,
			'TCREACION' => date('Y-m-d H:i:s'),
			'SREGISTRO' => $SREGISTRO,
			'DTELEFONO' => $DTELEFONO,
			'CUBIGEO' => $CUBIGEO,
			'CPAISESTABLECIMIENTO' => $CPAISESTABLECIMIENTO,
			'DCIUDADESTABLECIMIENTO' => $DCIUDADESTABLECIMIENTO,
			'DESTADOESTABLECIMIENTO' => $DESTADOESTABLECIMIENTO,
			'DREFERENCIA' => $DREFERENCIA,
		];
		if (empty($CESTABLECIMIENTO)) {
			$data['CESTABLECIMIENTO'] = $this->obtenerId();
			$save = $this->db->insert('MESTABLECIMIENTOCLIENTE', $data);
			// relacion
			$this->db->insert('pt_clienteestablecimientos', ['CCLIENTE' => $CCLIENTE, 'CESTABLECIMIENTO' => $data['CESTABLECIMIENTO']]);
		} else {
			unset($data['CCLIENTE']);
			unset($data['CESTABLECIMIENTO']);
			unset($data['CUSUARIOCREA']);
			unset($data['TCREACION']);
			$data['TMODIFICACION'] = date('Y-m-d');
			$data['CUSUARIOMODIFICA'] = $CUSUARIO;
			$save = $this->db->update('MESTABLECIMIENTOCLIENTE', $data, ['CESTABLECIMIENTO' => $CESTABLECIMIENTO]);
		}

		if (!$save) {
			throw new Exception('Error al guardar el establecimiento.');
		}

		return $data['CESTABLECIMIENTO'];
	}

}
