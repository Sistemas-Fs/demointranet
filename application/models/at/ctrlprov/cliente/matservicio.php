<?php

/**
 * Class matservicio
 */
class matservicio extends CI_Model
{

	/**
	 * mservicio constructor.
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * @param $ccliente
	 * @return array|array[]|object|object[]
	 */
	public function lista($ccliente)
	{
		$this->db->select("
			d.cinternopte,
			d.norden,d.ccompania,
			d.carea,d.cservicio,s.dservicio,d.csubservicio,ss.dsubservicio,d.cresponsable,c.DNUMEROPTE,C.CCOMPANIA,C.CAREA,C.CUSUARIOAPRUEBA,C.CCLIENTE, a.datosrazonsocial, '' as 'SPACE'
		");
		$this->db->from('PDPTE d');
		$this->db->join('PCPTE c', 'c.cinternopte = d.cinternopte', 'inner');
		$this->db->join('MSERVICIO s', "d.cservicio = s.cservicio and d.ccompania = s.ccompania and d.carea = s.carea and s.sregistro = 'A'", 'inner');
		$this->db->join('MSUBSERVICIO ss', "d.cservicio = ss.cservicio and d.ccompania = ss.ccompania and d.carea = ss.carea and d.csubservicio = ss.csubservicio and ss.sregistro = 'A'", 'inner');
		$this->db->join('segu_usuario u', "d.cresponsable = u.cusuario and u.estado = 'A'", 'inner');
		$this->db->join('adm_administrado a', "u.id_administrado = a.id_administrado", 'inner');
		$this->db->where('d.sregistro', 'A');
		$this->db->where('c.ccliente', $ccliente);
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @return array|array[]|object|object[]
	 */
	public function getServicios()
	{
		return $this->db->select('*')
			->from('MSERVICIO')
			->where('CCOMPANIA', '1')
			->where('CAREA', '01')
			->get()
			->result();
	}

	/**
	 * @return array|array[]|object|object[]
	 */
	public function getSubServicios($cservicio)
	{
		return $this->db->select('*')
			->from('MSUBSERVICIO')
			->where('CCOMPANIA', '1')
			->where('CAREA', '01')
			->where('CSERVICIO', $cservicio)
			->get()
			->result();
	}

	/**
	 * @return array|array[]|object|object[]
	 */
	public function getResponsables()
	{
		return $this->db->select("
			C.datosrazonsocial, B.email, B.id_empleado, B.id_administrado, D.cusuario
		")
			->from('adm_rrhh_contrato A')
			->join('adm_rrhh_empleado B', 'B.id_empleado = A.id_empleado', 'inner')
			->join('adm_administrado C', 'C.id_administrado = B.id_administrado', 'inner')
			->join('segu_usuario D', 'D.id_administrado = C.id_administrado', 'inner')
			->where('A.estado_contrato', 'A')
			->where('A.ccompania', '1')
			->where('A.carea', '01')
			->get()
			->result();
	}

	public function createServicio($parametros) { // Guardar Cliente
		$this->db->trans_begin();

		$procedure = "call sp_appweb_at_create_servicio_pcte(?,?,?,?,?,?)";
		return $this->db-> query($procedure,$parametros);
	}

}
