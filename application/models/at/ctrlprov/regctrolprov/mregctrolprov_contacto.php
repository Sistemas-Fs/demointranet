<?php

/**
 * Class mregctrolprov_contacto
 */
class mregctrolprov_contacto extends CI_Model
{

	/**
	 * mregctrolprov_contacto constructor.
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * @param $CAUDITORIAINSPECCION
	 * @return array|array[]|object|object[]
	 */
	public function lista($CAUDITORIAINSPECCION, $paraCartas = false)
	{
		$this->db->select("
			MCLIENTE.NRUC + ' ' + MCLIENTE.DRAZONSOCIAL AS cliente,
			MESTABLECIMIENTOCLIENTE.DESTABLECIMIENTO,
			mcontacto.CCONTACTO as id,
			mcontacto.dapepat,
			mcontacto.dapemat,
			mcontacto.dnombre,   
			mcontacto.dcargocontacto,   
			mcontacto.dmail,   
			mcontacto.dtelefono,
			mcontacto.ccliente,
			mcontacto.dnombrecarga,
			pcontactoxservicio.STIPOCONTACTO,
			pcontactoxservicio.SACCIONCONTACTO
		");
		$this->db->from('pcontactoxservicio');
		$this->db->join('MCONTACTO', 'pcontactoxservicio.CCONTACTO = MCONTACTO.CCONTACTO', 'inner');
		$this->db->join('MCLIENTE', 'MCONTACTO.CCLIENTE = MCLIENTE.CCLIENTE', 'inner');
		$this->db->join('MESTABLECIMIENTOCLIENTE', 'MCONTACTO.CESTABLECIMIENTO = MESTABLECIMIENTOCLIENTE.CESTABLECIMIENTO', 'inner');
		$this->db->where('PCONTACTOXSERVICIO.CAUDITORIAINSPECCION', $CAUDITORIAINSPECCION);
		if ($paraCartas) {
			$this->db->where('pcontactoxservicio.STIPOCONTACTO', 'P');
//			$this->db->where("pcontactoxservicio.SACCIONCONTACTO IS NOT NULL", null, false);
		} else {
			$this->db->where('pcontactoxservicio.STIPOCONTACTO', 'O');
			$this->db->group_start();
			$this->db->where("pcontactoxservicio.SACCIONCONTACTO IS NULL", null, false);
			$this->db->or_where("pcontactoxservicio.SACCIONCONTACTO = ''", null, false);
			$this->db->group_end();
		}
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @param $search
	 * @return array|array[]|object|object[]
	 */
	public function autocompletado($search, $CPROVEEDORCLIENTE, $CMAQUILADORCLIENTE)
	{
		$this->db->select("
			mcontacto.CCONTACTO as id,
			mcontacto.dapepat + ' ' + mcontacto.dapemat + ' ' + mcontacto.dnombre as text,   
			mcontacto.dcargocontacto,   
			mcontacto.dmail,   
			mcontacto.dtelefono,
			mcontacto.ccliente,
			MCLIENTE.NRUC + ' ' + MCLIENTE.DRAZONSOCIAL AS cliente
		");
		$this->db->from('mcontacto');
		$this->db->join('MCLIENTE', 'MCONTACTO.CCLIENTE = MCLIENTE.CCLIENTE', 'INNER');
		$this->db->group_start();
		$this->db->like('mcontacto.CCLIENTE', $CPROVEEDORCLIENTE, 'both', false);
		$this->db->like('mcontacto.CCLIENTE', $CMAQUILADORCLIENTE, 'both', false);
		$this->db->group_end();
		$this->db->group_start();
		$this->db->like('DAPEPAT', $search, 'both', false);
		$this->db->or_like('DAPEMAT', $search, 'both', false);
		$this->db->or_like('DNOMBRE', $search, 'both', false);
		$this->db->or_like('DMAIL', $search, 'both', false);
		$this->db->group_end();
		$this->db->limitAnyWhere(80);
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	public function getEstablecimientos($ccliente)
	{
		$this->db->select("
		        distinct E.CCLIENTE                      AS 'COD_CLIENTE',
                E.CESTABLECIMIENTO                       AS 'COD_ESTABLE',
                ISNULL(E.DESTABLECIMIENTO, '')           AS 'DESCRIPESTABLE',
                ISNULL(E.DDIRECCION, '')                 AS 'DIRECCION',
                ISNULL(E.CPAISESTABLECIMIENTO, '')       AS 'PAIS',
                ISNULL(E.CUBIGEO, '')                    AS 'UBIGEO',
                ISNULL(E.DCIUDADESTABLECIMIENTO, '')     AS 'CIUDAD',
                ISNULL(E.DESTADOESTABLECIMIENTO, '')     AS 'ESTESTABLE',
                ISNULL(E.DZIPESTABLECIMIENTO, '')        AS 'DZIP',
                (select (z.ddepartamento + ' - ' + z.dprovincia + ' - ' + z.ddistrito) as dubigeo
                 from tubigeo z
                 where z.cubigeo = E.CUBIGEO)            as 'DUBIGEO',
                ISNULL(E.FCE, '')                        AS 'FCE',
                ISNULL(E.ECP, '')                        AS 'ECP',
                ISNULL(E.FFRN, '')                       AS 'FFRN',
                ISNULL(E.DRESPONSABLECALIDAD, '')        AS 'RESPONCALIDAD',
                ISNULL(E.DCARGORESPONSABLECALIDAD, '')   AS 'CARGOCALIDAD',
                ISNULL(E.DEMAILRESPONSABLECALIDAD, '')   AS 'EMAILCALIDAD',
                ISNULL(E.NTELEFONOREPONSABLECALIDAD, '') AS 'TELEFONOCALIDAD',
                E.SREGISTRO                              AS 'ESTADO',
                ''                                       as 'SPACE',
                ISNULL(E.dreferencia, '')                AS 'REFERENCIA',
                ISNULL(E.dtelefono, '')                  AS 'TELEFONO'
		");
		$this->db->from('MESTABLECIMIENTOCLIENTE E');
		$this->db->where('E.CCLIENTE', $ccliente);
		$this->db->order_by('E.CESTABLECIMIENTO', 'DESC');
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @return string
	 */
	private function obtenerContactoKey()
	{
		$query = $this->db->select('MAX(CCONTACTO) as id')
			->from('MCONTACTO')
			->get();
		if (!$query) {
			return '00001';
		}
		$id = intval($query->row()->id) + 1;
		return ($query->num_rows() > 0) ? str_pad($id, 5, '0', STR_PAD_LEFT) : '00001';
	}

	/**
	 * @param $CCONTACTO
	 * @param $CAUDITORIAINSPECCION
	 * @param $CCLIENTE
	 * @param $CESTABLECIMIENTO
	 * @param $STIPOCONTACTO
	 * @param $SACCIONCONTACTO
	 * @param $DAPEPAT
	 * @param $DAPEMAT
	 * @param $DNOMBRE
	 * @param $DCARGOCONTACTO
	 * @param $DMAIL
	 * @param $DTELEFONO
	 * @param $CUSUARIO
	 * @return bool
	 */
	public function guardar($CCONTACTO, $CAUDITORIAINSPECCION, $CCLIENTE, $CESTABLECIMIENTO, $STIPOCONTACTO, $SACCIONCONTACTO, $DAPEPAT, $DAPEMAT, $DNOMBRE, $DCARGOCONTACTO, $DMAIL, $DTELEFONO, $CUSUARIO)
	{
		$fecha = date('Y-m-d H:i:s');

		$DNOMBRECARGA = $DAPEPAT . ' ' . $DAPEMAT . ' ' . $DNOMBRE;
		$data = [
			'CCONTACTO' => $CCONTACTO,
			'CCLIENTE' => $CCLIENTE,
			'CESTABLECIMIENTO' => $CESTABLECIMIENTO,
			'DAPEPAT' => $DAPEPAT,
			'DAPEMAT' => $DAPEMAT,
			'DNOMBRE' => $DNOMBRE,
			'DNOMBRECARGA' => $DNOMBRECARGA,
			'DCARGOCONTACTO' => $DCARGOCONTACTO,
			'DMAIL' => $DMAIL,
			'DTELEFONO' => $DTELEFONO,
			'CUSUARIOCREA' => $CUSUARIO,
			'TCREACION' => $fecha,
			'TMODIFICACION' => null,
			'CUSUARIOMODIFICA' => null,
			'SREGISTRO' => 'A',
		];

		if (empty($CCONTACTO)) {
			$CCONTACTO = $this->obtenerContactoKey();
			$data['CCONTACTO'] = $CCONTACTO;
			$scontacto = $this->db->insert('MCONTACTO', $data);
			// Relacion con la inspeccion
			$this->db->insert('pcontactoxservicio', [
				'CAUDITORIAINSPECCION' => $CAUDITORIAINSPECCION,
				'CCONTACTO' => $CCONTACTO,
				'STIPOCONTACTO' => $STIPOCONTACTO,
				'SACCIONCONTACTO' => $SACCIONCONTACTO,
				'CUSUARIOCREA' => $CUSUARIO,
				'TCREACION' => $fecha,
				'TMODIFICACION' => null,
				'CUSUARIOMODIFICA' => null,
				'SREGISTRO' => 'A',
			]);
		} else {
			unset($data['CCONTACTO']);
			unset($data['CCLIENTE']);
			$scontacto = $this->db->update('MCONTACTO', $data, ['CCONTACTO' => $CCONTACTO, 'CCLIENTE' => $CCLIENTE]);
		}

		if (!$scontacto) {
			throw new Exception('Error al registrar el contacto.');
		}

		return $CCONTACTO;
	}

	public function buscar($cauditoria, $ccontacto)
	{
		return $this->db->where('CAUDITORIAINSPECCION', $cauditoria)
			->where('CCONTACTO', $ccontacto)
			->get('pcontactoxservicio')
			->row();
	}

}
