<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mcartasprov extends CI_Model {
	function __construct() {
		parent:: __construct();	
		$this->load->library('session');
    }

    public function getclientes() { // recupera los clientes 
       $sql = "select c.ccliente, c.drazonsocial from PDPTE a 
					join PCPTE b on b.cinternopte = a.cinternopte
					join MCLIENTE c on c.cgrupoempresarial = b.cgrupoempresarial
				where a.ccompania = '1' and a.carea = '01' and a.cservicio = '02';";
       $query  = $this->db->query($sql);
           
       if ($query->num_rows() > 0) {

            $listas = '<option value="">Elige</option>';
            
            foreach ($query->result() as $row){
                $listas .= '<option value="'.$row->ccliente.'">'.$row->drazonsocial.'</option>';  
            }
               return $listas;
        }{
            return false;
        }		   
	}
		
    public function getbuscarcartas($parametros) {  // recupera los cartas a proveedores
        $this->db->trans_begin();
		error_reporting(0);
        $procedure = "call usp_at_ctrlprov_getlistcartas(?,?,?,?);";
        $query = $this->db->query($procedure,$parametros);

        if ($this->db->trans_status() === FALSE)        {
            $this->db->trans_rollback();
        }else{
            $this->db->trans_commit();
            return $query->result(); 
        }   
    } 
		
    public function getcartasprov($parametros) {  // recupera los cartas a proveedores
        $this->db->trans_begin();

        $procedure = "call sp_formatocartas_ip(?,?,?,?);";
        $query = $this->db->query($procedure,$parametros);

        if ($this->db->trans_status() === FALSE)        {
            $this->db->trans_rollback();
        }else{
            $this->db->trans_commit();
            return $query->result(); 
        }   
    }

	/***
	 * @param $inspeccion
	 * @param $fservicio
	 * @return null
	 */
    public function getcartaprov($inspeccion, $fservicio)
	{
		$sql = "
				SELECT pdauditoriainspeccion.cauditoriainspeccion,   
			 pdauditoriainspeccion.fservicio,   
			 pdauditoriainspeccion.cchecklist,   
			 mcliente.drazonsocial as 'cliente_drazonsocial',   
			 mlineaprocesocliente.dlineaclientee,   
			 mcompania.drazonsocial,   
			 YEAR(pdauditoriainspeccion.FCREACION) as anio,
			 (select sum(isnull(icostobase,0)) from pcostoservicio pcs where pcs.cauditoriainspeccion = pcauditoriainspeccion.cauditoriainspeccion ) as costo,   
			 (select mc.DCARGOCONTACTO from pcontactoxservicio PCS , mcontacto MC where PCS.ccontacto = MC.ccontacto and PCS.cauditoriainspeccion = pcauditoriainspeccion.cauditoriainspeccion  and PCS.saccioncontacto = 'E') as cargo_contacto,   
			 (select Upper(mc.dapepat)+' '+Upper(mc.dapemat)+', '+Upper(mc.dnombre) from pcontactoxservicio PCS , mcontacto MC where PCS.ccontacto = MC.ccontacto and PCS.cauditoriainspeccion = pcauditoriainspeccion.cauditoriainspeccion  and PCS.saccioncontacto = 'E') as contacto,   
			 (select Upper(c.DRAZONSOCIAL) from pcontactoxservicio PCS , mcontacto MC , mcliente C where PCS.ccontacto = MC.ccontacto and MC.ccliente = C.ccliente and PCS.cauditoriainspeccion = pcauditoriainspeccion.cauditoriainspeccion  and PCS.saccioncontacto = 'E') as empresa_contacto,   
			 (SELECT MEST.DDIRECCION+' - '+t.DDISTRITO+' - '+t.DPROVINCIA FROM MESTABLECIMIENTOCLIENTE MEST, tubigeo T  WHERE MEST.cubigeo = T.cubigeo and MEST.CESTABLECIMIENTO = cesta) as dir_contacto,   
			 pdauditoriainspeccion.fenviocarta,   
			 (select Upper(mc.dnombre)+' '+Upper(mc.dapepat)+' '+Upper(mc.dapemat) from pcontactoxservicio PCS , mcontacto MC where PCS.ccontacto = MC.ccontacto and PCS.cauditoriainspeccion = pcauditoriainspeccion.cauditoriainspeccion  and PCS.saccioncontacto = 'F') as firmante,   
			 (select mc.DCARGOCONTACTO from pcontactoxservicio PCS , mcontacto MC where PCS.ccontacto = MC.ccontacto and PCS.cauditoriainspeccion = pcauditoriainspeccion.cauditoriainspeccion  and PCS.saccioncontacto = 'F') as cargo_firmante ,
			 (select t.DDEPARTAMENTO+' .-' FROM MESTABLECIMIENTOCLIENTE MEST, tubigeo T  WHERE MEST.cubigeo = T.cubigeo and MEST.CESTABLECIMIENTO = cesta) as departamento ,
			 (select isnull(cestablecimientomaquila,cestablecimientoprov) from pcauditoriainspeccion dir where dir.cauditoriainspeccion = pcauditoriainspeccion.cauditoriainspeccion) as cesta,
			 ('Lima, '+cast(day( today(*) ) as varchar(2))+' de '+case month(today(*)) when 1 then 'Enero' when 2 then 'Febrero' when 3 then 'Marzo' when 4 then 'Abril' when 5 then 'Mayo' when 6 then 'Junio' when 7 then 'Julio' when 8 then 'Agosto' when 9 then 'Setiembre' when 10 then 'Octubre' when 11 then 'Noviembre' when 12 then 'Diciembre' else '' end+' del ' +cast(year(today(*))as varchar(4))) as 'fecha',
			 (case month(pdauditoriainspeccion.FCREACION) when 1 then 'ENERO' when 2 then 'FEBRERO' when 3 then 'MARZO' when 4 then 'ABRIL' when 5 then 'MAYO' when 6 then 'JUNIO' when 7 then 'JULIO' when 8 then 'AGOSTO' when 9 then 'SETIEMBRE' when 10 then 'OCTUBRE' when 11 then 'NOVIEMBRE' when 12 then 'DICIEMBRE' else '' end) as 'mes',
			 (SELECT (IF cestablecimientomaquila = null THEN MEST.DESTABLECIMIENTO ELSE mcliente_c.drazonsocial+' - '+MEST.DESTABLECIMIENTO END IF) FROM MESTABLECIMIENTOCLIENTE MEST WHERE MEST.CESTABLECIMIENTO = cesta) as establecimiento
		FROM pdauditoriainspeccion,   
			 pcauditoriainspeccion LEFT OUTER JOIN mcliente mcliente_c ON pcauditoriainspeccion.cmaquiladorcliente = mcliente_c.ccliente  ,   
			 mcliente,   
			 mlineaprocesocliente,   
			 pcpte,   
			 pdpte,   
			 mcompania
	   WHERE ( pdauditoriainspeccion.cauditoriainspeccion = pcauditoriainspeccion.cauditoriainspeccion ) and  
			 ( pcauditoriainspeccion.ccliente = mcliente.ccliente ) and  
			 ( mlineaprocesocliente.clineaprocesocliente = pcauditoriainspeccion.clineaprocesocliente ) and  
			 ( pdpte.cinternopte = pcpte.cinternopte ) and  
			 ( pdpte.cinternopte = pcauditoriainspeccion.cinternopte ) and  
			 ( pdpte.norden = pcauditoriainspeccion.norden ) and  
			 ( mcompania.ccompania = pcpte.ccompania ) 
			AND  ( pdpte.ccompania = '1' ) 
			AND  ( pdpte.carea = '01' ) 
			AND  ( pdpte.cservicio = '02') 
			AND  pdauditoriainspeccion.cauditoriainspeccion = '{$inspeccion}'
			AND  pdauditoriainspeccion.fservicio = '{$fservicio}'
		";
		$query = $this->db->query($sql);
		if (!$query) {
			return null;
		}
		return ($query->num_rows() > 0) ? $query->row() : null;
	}

}
?>
