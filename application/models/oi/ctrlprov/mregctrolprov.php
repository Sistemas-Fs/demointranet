<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mregctrolprov extends CI_Model
{
	function __construct()
	{
		parent:: __construct();
		$this->load->library('session');
	}

	/** CONTROL DE PROVEEDORES **/
	public function getcboclieserv()
	{ // Visualizar Clientes del servicio en CBO

		$this->db->select("distinct a.ccliente as 'IDCLIE', b.drazonsocial as 'DESCRIPCLIE'", false);
		$this->db->from("pcauditoriainspeccion a ");
		$this->db->join("mcliente b", " b.ccliente = a.ccliente");
		$this->db->join("pdpte c", "c.cinternopte = a.cinternopte and c.norden = a.norden");
		$this->db->where('c.ccompania', 2);
		$this->db->where('c.carea', '01');
		$this->db->where('c.cservicio', '01');
		$query = $this->db->get();

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->IDCLIE . '">' . $row->DESCRIPCLIE . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function getcboprovxclie($parametros)
	{ // Visualizar Proveedor por cliente en CBO

		$procedure = "call usp_oi_ctrlprov_getcboprovxclie(?)";
		$query = $this->db->query($procedure, $parametros);

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->IDPROV . '">' . $row->DESCRIPPROV . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function getcbomaqxprov($parametros)
	{ // Visualizar Maquilador por proveedor en CBO

//		$procedure = "call usp_at_ctrlprov_getcbomaqxprov(?)";
//		$query = $this->db->query($procedure, $parametros);

		$query = $this->db->select("distinct a.CCLIENTE as 'IDMAQ', a.drazonsocial as 'DESCRIPMAQ'")
			->from('mcliente a')
			->join('mrclienteproveedor b', 'b.cproveedorcliente = a.ccliente', 'inner')
			->where('b.ccliente', $parametros['@cproveedor'])
			->order_by('a.drazonsocial')
			->get();
		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->IDMAQ . '">' . $row->DESCRIPMAQ . '</option>';
			}
			return $listas;
		}
		{
			$listas = '<option value="" selected="selected"></option>';
			return $listas;
		}
	}

	public function getcboinspector($parametros)
	{ // Visualizar Clientes del servicio en CBO

		$this->db->select("a.cusuario as 'IDINSP', a.ddatos as 'DESCRIPINSP'");
		$this->db->from("musuario a");
		$this->db->join("musuarioxcia b", "a.cusuario = b.cusuario");
		$this->db->join("mcargousuario c", "b.cusuario = c.cusuario and b.ccompania = c.ccompania");
		$this->db->where("b.ccompania", '2');
		$this->db->where("c.carea", "01");
		$this->db->where("c.cservicio", "01");
		$this->db->where_in("c.zctipocargo", ['458', '080']);
		$this->db->where("a.sregistro", 'A');
		$this->db->where("b.sregistro", "A");
		$this->db->where("c.sregistro", "A");
		$this->db->where_in("a.stipousuario", ['I']);
		$this->db->order_by('a.ddatos');
		$query = $this->db->get();

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->IDINSP . '">' . $row->DESCRIPINSP . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function getcboestado()
	{    // Visualizar Estado en CBO

		$procedure = "call usp_at_ctrlprov_getcboestado()";
		$query = $this->db->query($procedure);

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->IDESTADO . '">' . $row->DESCRIPESTADO . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function getcbocalif()
	{ // // Visualizar Calificacion en CBO

		$procedure = "call usp_at_ctrlprov_getcbocalif()";
		$query = $this->db->query($procedure);

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->IDCALIF . '">' . $row->DESCRIPCALIF . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function getbuscarctrlprov($parametros)
	{ // Recupera Listado de Propuestas
		$procedure = "call usp_at_ctrlprov_getbuscarctrlprov(?,?,?,?,?,?,?,?,?,?,?)";
		$query = $this->db->query($procedure, $parametros);

		if ($query->num_rows() > 0) {
			return $query->result();
		}
		{
			return False;
		}
	}

	public function getrecuperainsp($parametros)
	{ // Recupera Listado de Propuestas
		$procedure = "call usp_at_ctrlprov_getrecuperainsp(?)";
		$query = $this->db->query($procedure, $parametros);

		if ($query->num_rows() > 0) {
			return $query->result();
		}
		{
			return False;
		}
	}

	public function getcboregestable($parametros)
	{ // Visualizar Proveedor por cliente en CBO

		$procedure = "call usp_at_ctrlprov_getcboregestable(?,?,?,?)";
		$query = $this->db->query($procedure, $parametros);

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->CESTABLECIMIENTO . '">' . $row->DESTABLECIMIENTO . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function getdirestable($cestable)
	{ // Listar Ensayos
		$sql = "select ((select x.DDEPARTAMENTO+' - '+x.DPROVINCIA+' - '+x.DDISTRITO from TUBIGEO x where x.cubigeo = z.cubigeo) +' - '+ z.DDIRECCION) as 'DIRESTABLE' from MESTABLECIMIENTOCLIENTE z where z.CESTABLECIMIENTO = '" . $cestable . "';";
		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {
			return $query->result();
		}
		{
			return False;
		}
	}

	public function getcboareaclie($ccliente)
	{ // Listar Ensayos
		$sql = "select careacliente, dareacliente, djerarquia from mareacliente where ccliente  = '" . $ccliente . "' and ccompania = '2';";
		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->careacliente . '" data-adiv="' . $row->djerarquia . '" >' . $row->dareacliente . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function getcbocartafirmante($ccliente)
	{ // Listar Ensayos
		$sql = "select (dapepat+' '+dapemat+' '+dnombre )+' ('+dcargocontacto+')' as 'nombcontacto', dmail as 'mailcontacto', 
                        ccontacto, ccliente, cestablecimiento FROM MCONTACTO WHERE CCLIENTE = '{$ccliente}' ";
		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->ccontacto . '">' . $row->nombcontacto . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function getcbolineaproc($ccliente)
	{ // Listar Ensayos
		$sql = "select clineaprocesocliente, dlineaclientee + (if speligro = 'S' then '  --> ( PELIGRO ) ' else '' end if) as 'dlineacliente'
                from mlineaprocesocliente  
                where cestablecimiento = '" . $ccliente . "' and  sregistro = 'A';";
		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->clineaprocesocliente . '">' . $row->dlineacliente . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function getcbocontacprinc($cproveedor)
	{ // Listar Ensayos
		$sql = "select (dapepat+' '+dapemat+' '+dnombre )+' ('+dcargocontacto+')' as 'nombcontacto', dmail as 'mailcontacto', 
                        ccontacto, ccliente, cestablecimiento
                from mcontacto  
                where (sregistro='A') and
                    (ccliente = '" . $cproveedor . "') and      
                    exists (select 1 from MESTABLECIMIENTOCLIENTE e
                            where e.ccliente = mcontacto.ccliente
                            and e.cestablecimiento = mcontacto.cestablecimiento
                            and e.sregistro = 'A');";
		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->ccontacto . '">' . $row->nombcontacto . ' Email: ' . $row->mailcontacto . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function getcbocartadestinatario($cproveedor)
	{ // Listar Ensayos
		$sql = "select (dapepat+' '+dapemat+' '+dnombre )+' ('+dcargocontacto+')' as 'nombcontacto', dmail as 'mailcontacto', 
                        ccontacto, ccliente, cestablecimiento
                from mcontacto  
                where (sregistro='A') and
                    (ccliente = '" . $cproveedor . "')";
		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->ccontacto . '">' . $row->nombcontacto . ' Email: ' . $row->mailcontacto . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function getcbotipoestable()
	{ // Listar Ensayos
		$sql = "select ctipoestablecimiento, dtipoestablecimiento 
                from mtipoestablecimiento where ccompania = '2'  
                order by dtipoestablecimiento asc ;";
		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->ctipoestablecimiento . '">' . $row->dtipoestablecimiento . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	/**
	 * @param $cauditoria
	 * @return array|mixed|object|null
	 */
	public function getTipoEstableCosto($cauditoria)
	{
		$this->db->select('top 1 MTIPOESTABLECIMIENTO.CTIPOESTABLECIMIENTO, MTIPOESTABLECIMIENTO.DTIPOESTABLECIMIENTO, pcostoservicio.icostobase as costo', false);
		$this->db->from('pcostoservicio');
		$this->db->join('MTIPOESTABLECIMIENTO', 'PCOSTOSERVICIO.CTIPOESTABLECIMIENTO = MTIPOESTABLECIMIENTO.CTIPOESTABLECIMIENTO', 'inner');
		$this->db->where('pcostoservicio.cauditoriainspeccion', $cauditoria);
		$query = $this->db->get();

		if (!$query) {
			return null;
		}
		return ($query->num_rows() > 0) ? $query->row() : null;
	}

	public function getmontotipoestable($ctipoestable)
	{ // Listar Ensayos
		$sql = "select isnull(icosto,0.00) as 'icosto' from mtipoestablecimiento where ctipoestablecimiento = '" . $ctipoestable . "' ;";
		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {

			foreach ($query->result() as $row) {
				$montocosto = $row->icosto;
			}
			return $montocosto;
		}
		{
			return false;
		}
	}

	public function setregctrlprov($parametros)
	{  // Registrar evaluacion PT
		$this->db->trans_begin();

		$procedure = "call usp_oi_ctrlprov_setpcauditoriainspeccion(?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
		$query = $this->db->query($procedure, $parametros);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
			return $query->result();
		}
	}

	public function getcbosistemaip()
	{ // Visualizar Sistema en CBO

		$this->db->select("DISTINCT b.dsistema + '  >  ' +  a.dnorma as 'DESCSISTEMA', a.cnorma as 'IDNORMA'");
		$this->db->from("mnorma a");
		$this->db->join("msistema b", "b.csistema = a.csistema");
		$this->db->join("mchecklist c", "c.cnorma = a.cnorma and c.ccompania = b.ccompania");
		$this->db->where('b.ccompania', '2');
		$this->db->where('b.carea', '01');
		$this->db->where('b.cservicio', '01');
		$query = $this->db->get();

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->IDNORMA . '">' . $row->DESCSISTEMA . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function getcborubroip($parametros)
	{ // Visualizar Rubro en CBO

		$procedure = "call usp_at_ctrlprov_getcborubroip(?)";
		$query = $this->db->query($procedure, $parametros);

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->IDSUBNORMA . '">' . $row->DESCSUBNORMA . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function getcbochecklist($parametros)
	{ // Visualizar Checklist en CBO

		$procedure = "call usp_at_ctrlprov_getcbochecklist(?,?,?)";
		$query = $this->db->query($procedure, $parametros);

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->IDCHECKLIST . '">' . $row->DESCCHECKLIST . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function getcboformula($parametros)
	{ // Visualizar Formula en CBO

		$procedure = "call usp_at_ctrlprov_getcboformula(?)";
		$query = $this->db->query($procedure, $parametros);

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->IDFORMULA . '">' . $row->DESCFORMULA . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function getcbocriterio($parametros)
	{ // Visualizar Criterio en CBO

		$procedure = "call usp_at_audi_getcbocriterio(?)";
		$query = $this->db->query($procedure, $parametros);

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->IDCRITERIO . '">' . $row->DESCCRITERIO . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function getcbomodinforme()
	{ // Listar Ensayos
		$sql = "select cmodeloinforme,dmodelo from mmodeloinforme where ccompania = '2' and carea = '01' and cservicio = '01';";
		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->cmodeloinforme . '">' . $row->dmodelo . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function getcboinspvalconf()
	{ // Listar Ensayos
		$sql = "select cvalor,dvalor from mvalor where sregistro = 'A' and stipovalor = 'N';";
		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->cvalor . '">' . $row->dvalor . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function getcboinspformula($cchecklist)
	{ // Listar Ensayos
		$sql = "select b.cformulaevaluacion,b.dformula 
                from mrchecklistformula a right outer join mformulaevaluacion b on b.cformulaevaluacion = a.cformulaevaluacion  
                where a.cchecklist = '" . $cchecklist . "';";
		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->cformulaevaluacion . '">' . $row->dformula . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function getcboinspcritresul()
	{ // Listar Ensayos
		$sql = "select ccriterioresultado, dcriterioresultado from mcriterioresultado ;";
		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->ccriterioresultado . '">' . $row->dcriterioresultado . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function getcbocontacplanins_new($cproveedorcliente, $cmaquiladorcliente)
	{ // Listar Ensayos
		$this->db->select("
			a.ccontacto as id, 
			(a.dapepat+' '+a.dapemat+' '+a.dnombre) as text,
			a.dcargocontacto,   
			a.dmail,   
			a.dtelefono,
			(b.NRUC + ' ' + b.DRAZONSOCIAL) AS cliente
		");
		$this->db->from('mcontacto a');
		$this->db->join('mcliente b', 'b.ccliente = a.ccliente', 'inner');
		$this->db->join('mestablecimientocliente c', 'c.ccliente = a.ccliente and c.cestablecimiento = a.cestablecimiento', 'inner');
		$this->db->group_start();
		$this->db->where('a.ccliente', $cproveedorcliente);
		if (!empty($cmaquiladorcliente)) {
			$this->db->or_where('a.ccliente', $cmaquiladorcliente);
		}
		$this->db->group_end();
		$this->db->where('a.sregistro', 'A');
		$this->db->order_by('b.drazonsocial');
		$this->db->order_by('c.destablecimiento');
		$this->db->order_by('text');
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	public function getcbocontacplanins($cproveedorcliente, $cmaquiladorcliente)
	{ // Listar Ensayos
		$sql = "select a.ccontacto,(a.dapepat+' '+a.dapemat+' '+a.dnombre) as apnom
                from mcontacto a   
                    join mcliente b on b.ccliente = a.ccliente   
                    join mestablecimientocliente c on c.ccliente = a.ccliente and c.cestablecimiento = a.cestablecimiento  
                where a.ccliente = (case '" . $cmaquiladorcliente . "' when '' then '" . $cproveedorcliente . "' else '" . $cmaquiladorcliente . "' end) AND   
                    a.sregistro = 'A'  
                order by b.drazonsocial, c.destablecimiento, apnom;";
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->ccontacto . '">' . $row->apnom . '</option>';
			}
			return $listas;
		}
		return false;
	}

	public function setreginspeccion($parametros)
	{  // Registrar evaluacion PT
		$this->db->trans_begin();

		$procedure = "call usp_at_ctrlprov_setpdauditoriainspeccion(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
		$query = $this->db->query($procedure, $parametros);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
			return $query->result();
		}
	}

	public function getcbocierreTipo()
	{ // Listar Ensayos
		$sql = "select ctipo, dregistro, nvalor from ttabla 
                where ctabla = '09' and ncorrelativo <> 0 and IsNull(nvalor,9) <> 9 and spermitemodificar = 'N' order by dregistro ASC ;";
		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->ctipo . '" data-lat="' . $row->nvalor . '">' . $row->dregistro . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function setcierreespecial($parametros)
	{  // Registrar evaluacion PT
		$this->db->trans_begin();

		$procedure = "call usp_at_ctrlprov_setcierreespecial(?,?,?,?,?,?,?,?,?,?,?);";
		$query = $this->db->query($procedure, $parametros);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
			return $query->result();
		}
	}

	public function setreaperturar($parametros)
	{  // Registrar evaluacion PT
		$this->db->trans_begin();

		$procedure = "call usp_at_ctrlprov_setreaperturar(?,?,?);";
		$query = $this->db->query($procedure, $parametros);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
			return $query->result();
		}
	}

	public function getcbocertificadora()
	{ // Listar Ensayos
		$sql = "select ccertificadora, drazonsocial from mcertificadora order by drazonsocial ASC ;";
		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->ccertificadora . '" >' . $row->drazonsocial . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function setplaninsp($parametros)
	{  // Registrar
		$this->db->trans_begin();

		$procedure = "call usp_at_ctrlprov_setplaninsp(?,?,?,?,?,?,?,?,?,?,?);";
		$query = $this->db->query($procedure, $parametros);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
			return $query->result();
		}
	}


	public function getpdfdatosplaninsp($idinspeccion, $fservicio)
	{ // Listar Ensayos
		$sql = "select b.cauditoriainspeccion, b.fservicio, b.cchecklist,
                        (select case IsNull( pcai.cmaquiladorcliente,'') 
                                when '' 
                                then (select drazonsocial from MCLIENTE C where c.ccliente = pcai.cproveedorcliente ) 
                                else 
                                    (select CL.drazonsocial from MCLIENTE CL where CL.ccliente = PCAI.cproveedorcliente )  +' - '+
                                    (select CL.drazonsocial from MCLIENTE CL where CL.ccliente = PCAI.cmaquiladorcliente ) 
                                end 
                        from mestablecimientocliente MEC
                            join pcauditoriainspeccion PCAI on
                                    MEC.ccliente = (case Isnull(PCAI.cestablecimientoprov,'0') when '0'  then PCAI.cmaquiladorcliente else PCAI.cproveedorcliente end ) and 
                                    MEC.cestablecimiento = (case Isnull(PCAI.cestablecimientoprov,'0') when '0' then PCAI.cestablecimientomaquila else PCAI.cestablecimientoprov end )
                        where PCAI.cauditoriainspeccion = A.cauditoriainspeccion )  as 'empresa',        
                        (select MEC.DDIRECCION+' - '+(select x.DDEPARTAMENTO+'-'+x.DPROVINCIA+'-'+x.DDISTRITO from TUBIGEO x where x.cubigeo = MEC.cubigeo) 
                        from mestablecimientocliente MEC
                            join pcauditoriainspeccion PCAI on
                                    MEC.ccliente = (case Isnull(PCAI.cestablecimientoprov,'') when ''  then PCAI.cmaquiladorcliente else PCAI.cproveedorcliente end ) and 
                                    MEC.cestablecimiento = (case Isnull(PCAI.cestablecimientoprov,'') when '' then PCAI.cestablecimientomaquila else PCAI.cestablecimientoprov end )
                        where PCAI.cauditoriainspeccion = A.cauditoriainspeccion )  as 'direccion',
                    c.dobjetivoplan, c.dalcanceplan, DATEFORMAT(b.fservicio,'dd/mm/yyyy') as 'finspeccion',c.hinicio,
                    (If IsNull(d.dnombre,'')<>'' then d.dnombre end if+' '+ If IsNull(d.dapepat,'')<>'' then d.dapepat end if +' '+ If IsNull(d.dapemat,'')<>'' then d.dapemat end if +If IsNull(d.dcargocontacto,'')<>'' then '  (' +d.dcargocontacto+')'end if) as 'contacto',
                    d.dtelefono, (If IsNull(e.dapepat,'')<>'' then e.dapepat end if+' '+ If isNull(e.dapemat,'')<>'' then e.dapemat end if +' '+ If IsNull(e.dnombre,'')<>'' then e.dnombre end if ) as 'inspector',
                    c.drequerimiento, f.cchecklist, f.cnorma, f.csubnorma, f.ccompania, f.carea, f.cservicio, REPLACE(g.dcriterios, CHAR(13),'<br>') as 'dcriterios' 
                from  pcauditoriainspeccion a    
                    INNER join pdauditoriainspeccion b on b.cauditoriainspeccion = a.cauditoriainspeccion   
                    INNER join pplanaudinsp c on c.cauditoriainspeccion = b.cauditoriainspeccion and c.fservicio = b.fservicio   
                    INNER join mcontacto d on d.ccontacto = c.ccontacto
                    INNER join mchecklist f on f.cchecklist = b.cchecklist
                    INNER join msubnorma g on g.cnorma = b.cnorma and g.csubnorma = b.csubnorma
					LEFT join musuario e on e.cusuario = b.cusuarioconsultor
                where b.cauditoriainspeccion = '" . $idinspeccion . "'  
                    and b.fservicio = '" . $fservicio . "';";
		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {
			return $query->result();
		}
		{
			return False;
		}
	}

	public function getpdfmetodoplaninsp($ccompania, $carea, $cservicio)
	{ // Listar Ensayos
		$sql = "select dmetodologia  
                from mservicio  
                where ccompania = '" . $ccompania . "' and  
                    carea = '" . $carea . "' and  
                    cservicio = '" . $cservicio . "';";
		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {
			return $query->result();
		}
		{
			return False;
		}
	}


	public function getcbocertificacion($ccertificadora)
	{ // Listar Ensayos
		$sql = "select a.dcertificacion +'('+b.drazonsocial+')' as 'dcertificacion', a.ccertificacion  
                from mcertificaciones a join MCERTIFICADORA b on a.ccertificadora = b.ccertificadora
                where ( a.ccompania = '1') AND  (b.ccertificadora = '" . $ccertificadora . "') and ( a.sregistro = 'A' );";
		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->ccertificacion . '">' . $row->dcertificacion . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function calculocriterio($vresul, $ccriterio)
	{ // Listar Ensayos
		$sql = "select cdetallecriterioresultado, ddetallecriterioresultado, nr, ng,nb,nmes
                from MDETALLECRITERIORESULTADO
                where ccriterioresultado = '" . $ccriterio . "' and (nvalorinicial <= " . $vresul . "  and nvalorfinal  >= " . $vresul . " ) ;";
		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {
			return $query->result();
		}
		{
			return False;
		}
	}

	public function setconvalidacion($parametros)
	{  // Registrar evaluacion PT
		$this->db->trans_begin();

		$procedure = "call usp_at_ctrlprov_setconvalidacion(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
		$query = $this->db->query($procedure, $parametros);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
			return $query->result();
		}
	}
}

?>

