<?php

/**
 * Class mestablecimiento_linea
 */
class mestablecimiento_linea extends CI_Model
{

	/**
	 * mestablecimiento_linea constructor.
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * @param $ccliente
	 * @param $cestablecimiento
	 * @return array|array[]|object|object[]
	 */
	public function lista($ccliente, $cestablecimiento)
	{
		$this->db->select("
			mlineaprocesocliente.*,
			IF(isnull(mlineaprocesocliente.SPELIGRO, 'N') = 'S') THEN 'Si' ELSE 'No' END IF as Peligro
		");
		$this->db->from('mlineaprocesocliente');
		$this->db->where('ccliente', $ccliente);
		$this->db->where('cestablecimiento', $cestablecimiento);
		$this->db->order_by('CLINEAPROCESOCLIENTE DESC');
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @return string
	 */
	public function obtenerId()
	{
		$query = $this->db->select("
			right(('000000'+cast(((cast(isnull(max(CLINEAPROCESOCLIENTE),0) as integer) + 1)) as char(6))),6) as id
		")
			->from('MLINEAPROCESOCLIENTE')
			->get();
		return ($query && $query->num_rows() > 0) ? $query->row()->id : '00001';
	}

	/**
	 * @param $CLINEAPROCESOCLIENTE
	 * @param $CCLIENTE
	 * @param $CESTABLECIMIENTO
	 * @param $DLINEACLIENTEE
	 * @param $SPELIGRO
	 * @param $CUSUARIO
	 * @param $SREGISTRO
	 * @return mixed
	 * @throws Exception
	 */
	public function guardar($CLINEAPROCESOCLIENTE, $CCLIENTE, $CESTABLECIMIENTO, $DLINEACLIENTEE, $SPELIGRO, $CUSUARIO, $SREGISTRO)
	{
		$data = [
			'CLINEAPROCESOCLIENTE' => $CLINEAPROCESOCLIENTE,
			'CCLIENTE' => $CCLIENTE,
			'CESTABLECIMIENTO' => $CESTABLECIMIENTO,
			'DLINEACLIENTEE' => $DLINEACLIENTEE,
			'SPELIGRO' => $SPELIGRO,
			'CUSUARIOCREA' => $CUSUARIO,
			'TCREACION' => date('Y-m-d H:i:s'),
			'SREGISTRO' => $SREGISTRO,
		];
		if (empty($CLINEAPROCESOCLIENTE)) {
			$data['CLINEAPROCESOCLIENTE'] = $this->obtenerId();
			$save = $this->db->insert('MLINEAPROCESOCLIENTE', $data);
		} else {
			unset($data['CLINEAPROCESOCLIENTE']);
			unset($data['CCLIENTE']);
			unset($data['CESTABLECIMIENTO']);
			unset($data['CUSUARIOCREA']);
			unset($data['TCREACION']);
			$data['TMODIFICACION'] = date('Y-m-d H:i:s');
			$data['CUSUARIOMODIFICA'] = $CUSUARIO;
			$save = $this->db->update('MLINEAPROCESOCLIENTE', $data, ['CLINEAPROCESOCLIENTE' => $CLINEAPROCESOCLIENTE]);
		}

		if (!$save) {
			throw new Exception('Error al guardar el establecimiento.');
		}

		return $data['CESTABLECIMIENTO'];
	}

}
