<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class matclientes
 */
class Matclientes extends CI_Model
{

	function __construct()
	{
		parent:: __construct();
		$this->load->library('session');
	}

	/**
	 * @param $ccliente
	 * @return array|mixed|object|null
	 */
	public function buscar($ccliente)
	{
		$this->db->select('*');
		$this->db->from('MCLIENTE');
		$this->db->where('CCLIENTE', $ccliente);
		$query = $this->db->get();
		if (!$query) {
			return null;
		}
		return ($query->num_rows() > 0) ? $query->row() : [];
	}

	/**
	 * Lista de consultas de Cliente
	 * @param int|string $parametros
	 */
	public function lista($clientes)
	{
		$this->db->select("
				a.CCLIENTE, 
				a.CGRUPOEMPRESARIAL, 
				ISNULL(a.DRAZONSOCIAL,'') AS 'DRAZONSOCIAL', 
				a.NRUC,
				a.DDIRECCIONCLIENTE, 
				a.CUBIGEO,
				a.ZCTIPOTAMANOEMPRESA,
				ISNULL(a.DTELEFONO,'') AS 'DTELEFONO',
            	ISNULL(a.DFAX,'') AS 'DFAX', 
            	ISNULL(a.DREPRESENTANTE,'') AS 'DREPRESENTANTE', 
            	ISNULL(a.DCARGOREPRESENTANTE,'') AS 'DCARGOREPRESENTANTE', 
            	ISNULL(a.DEMAILREPRESENTANTE,'') AS 'DEMAILREPRESENTANTE',
            	a.NTRABAJADOR, 
            	a.cpais, 
            	a.dciudad, 
            	a.destado, 
            	ISNULL(a.dzip,'') AS 'dzip', 
            	ISNULL(a.dweb,'') AS 'dweb',
            	if len(isnull(a.DRUTA,'')) = 0 then 'unknown.png' else a.DRUTA end if AS 'DRUTA', 
            	isnull(a.tipodoc,'') AS 'TIPODOC',
            	'' as 'SPACE', 
            	if a.cpais = '290' then (select ('('+z.ddepartamento+' - '+z.dprovincia+' - '+z.ddistrito+')') as dubigeo from tubigeo z where z.cubigeo = a.CUBIGEO) else  ('('+a.dciudad+' - '+a.destado+' - '+(select z.dregistro from ttabla z where z.ctipo = a.cpais)+')') end if AS 'DUBIGEO'
		");
		$this->db->from('mcliente a');
		$this->db->join('pt_clienteestablecimientos b', 'b on b.ccliente = a.ccliente', 'left');
		$this->db->group_start();
		$this->db->like('a.DRAZONSOCIAL', $clientes);
		$this->db->or_like('a.NRUC', $clientes);
		$this->db->group_end();
		$this->db->order_by('a.CCLIENTE', 'DESC');
//		$this->db->group_by('a.CCLIENTE');
		$this->db->limitAnyWhere(80);
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @return string
	 */
	public function obtenerId()
	{
		$query = $this->db->select("
			right(('00000'+cast(((cast(isnull(max(ccliente),0) as integer) + 1)) as char(5))),5) as id
		")
			->from('mcliente')
			->get();
		return ($query && $query->num_rows() > 0) ? $query->row()->id : '00001';
	}

	/**
	 * @param $CCLIENTE
	 * @param $CMAILHOMOLOGACION
	 * @param $COLORFONDO
	 * @param $COLORTEXT
	 * @param $cpais
	 * @param $CUBIGEO
	 * @param $CUSUARIO
	 * @param $DCARGOREPRESENTANTE
	 * @param $dciudad
	 * @param $DDIRECCIONCLIENTE
	 * @param $DEMAILREPRESENTANTE
	 * @param $destado
	 * @param $DFAX
	 * @param $DRAZONSOCIAL
	 * @param $DREPRESENTANTE
	 * @param $DRUTA
	 * @param $DTELEFONO
	 * @param $dweb
	 * @param $dzip
	 * @param $NMESMAILAARR
	 * @param $NRUC
	 * @param $NTRABAJADOR
	 * @param $SREGISTRO
	 * @param $tipodoc
	 * @return bool
	 */
	public function guardar($CCLIENTE, $CMAILHOMOLOGACION, $COLORFONDO, $COLORTEXT,
							$cpais, $CUBIGEO, $CUSUARIO, $DCARGOREPRESENTANTE, $dciudad,
							$DDIRECCIONCLIENTE, $DEMAILREPRESENTANTE, $destado, $DFAX, $DRAZONSOCIAL,
							$DREPRESENTANTE, $DRUTA, $DTELEFONO, $dweb, $dzip,
							$NMESMAILAARR, $NRUC, $NTRABAJADOR, $SREGISTRO, $tipodoc
	)
	{
		$data = [
			'CCLIENTE' => $CCLIENTE,
			'CGRUPOEMPRESARIAL' => '00000',
			'CMAILHOMOLOGACION' => $CMAILHOMOLOGACION,
			'COLORFONDO' => $COLORFONDO,
			'COLORTEXT' => $COLORTEXT,
			'cpais' => $cpais,
			'CUBIGEO' => $CUBIGEO,
			'DCARGOREPRESENTANTE' => $DCARGOREPRESENTANTE,
			'dciudad' => $dciudad,
			'DDIRECCIONCLIENTE' => $DDIRECCIONCLIENTE,
			'DEMAILREPRESENTANTE' => $DEMAILREPRESENTANTE,
			'destado' => $destado,
			'DFAX' => $DFAX,
			'DRAZONSOCIAL' => $DRAZONSOCIAL,
			'DREPRESENTANTE' => $DREPRESENTANTE,
			'DRUTA' => $DRUTA,
			'DTELEFONO' => $DTELEFONO,
			'dweb' => $dweb,
			'dzip' => $dzip,
			'NMESMAILAARR' => $NMESMAILAARR,
			'NRUC' => $NRUC,
			'NTRABAJADOR' => $NTRABAJADOR,
			'CUSUARIOCREA' => $CUSUARIO,
			'TCREACION' => date('Y-m-d'),
			'SREGISTRO' => $SREGISTRO,
			'STIPOCLIENTE' => 'C',
			'tipodoc' => $tipodoc,
			'ZCTIPOTAMANOEMPRESA' => 112,
		];
		if (empty($CCLIENTE)) {
			$CCLIENTE = $this->obtenerId();
			$data['CCLIENTE'] = $CCLIENTE;
			$save = $this->db->insert('MCLIENTE', $data);
		} else {
			unset($data['CCLIENTE']);
			unset($data['CUSUARIOCREA']);
			unset($data['TCREACION']);
			$data['TMODIFICACION'] = date('Y-m-d');
			$data['CUSUARIOMODIFICA'] = $CUSUARIO;
			$save = $this->db->update('MCLIENTE', $data, ['CCLIENTE' => $CCLIENTE]);
		}

		if (!$save) {
			throw new Exception('Error al guardar el cliente.');
		}

		return $CCLIENTE;
	}

	/**
	 * Guardar Cliente
	 * @return string
	 */
	public function getsizeempresa()
	{
		$sql = "select ctipo,dregistro,XCIA from ttabla where ctabla = '33' and SREGISTRO = 'A' AND CTIPO <>'396' ORDER BY NCORRELATIVO DESC;";
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			$listas = '<option value="" selected="selected">::Elegir</option>';
			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->ctipo . '">' . $row->dregistro . '</option>';
			}
			return $listas;
		}
		return false;
	}

	/**
	 * @return false|string
	 */
	public function getgrupoempresarial()
	{
		$sql = "select CGRUPOEMPRESARIAL,DGRUPOEMPRESARIAL from MGRUPOEMPRESARIAL WHERE SREGISTRO = 'A' ORDER BY DGRUPOEMPRESARIAL ASC";
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			$listas = '<option value="" selected="selected">::Elegir</option>';
			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->CGRUPOEMPRESARIAL . '">' . $row->DGRUPOEMPRESARIAL . '</option>';
			}
			return $listas;
		}
		return false;
	}

}
