<?php

/**
 * Class matarea
 */
class matarea extends CI_Model
{

	/**
	 * marea constructor.
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * @param $ccliente
	 * @return array|array[]|object|object[]
	 */
	public function lista($ccliente)
	{
		$this->db->select('*');
		$this->db->from('mareacliente');
		$this->db->where('ccliente', $ccliente);
		$this->db->where('ctipo', 'IP');
		$this->db->where('ccompania', '2');
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @return string
	 */
	public function obtenerId()
	{
		$query = $this->db->select("MAX(CAREACLIENTE) AS id")
			->from('MAREACLIENTE')
			->get();
		return ($query && $query->num_rows() > 0) ? intval($query->row()->id) + 1 : 1;
	}

	/**
	 * @param $CCLIENTE
	 * @param $CAREACLIENTE
	 * @param $DAREACLIENTE
	 * @param $CUSUARIO
	 * @param $SREGISTRO
	 * @param $ESCONCESIONARIO
	 * @param $CCOMPANIA
	 * @param $CTIPO
	 * @return int|mixed|string
	 * @throws Exception
	 */
	public function guardar($CCLIENTE, $CAREACLIENTE, $DAREACLIENTE, $CUSUARIO, $SREGISTRO, $ESCONCESIONARIO, $CCOMPANIA, $CTIPO, $DJERARQUIA)
	{
		// MAREACLIENTE
		$data = [
			'CCLIENTE' => $CCLIENTE,
			'CAREACLIENTE' => $CAREACLIENTE,
			'DAREACLIENTE' => $DAREACLIENTE,
			'CUSUARIOCREA' => $CUSUARIO,
			'TCREACION' => date('Y-m-d H:i:s'),
			'SREGISTRO' => $SREGISTRO,
			'ESCONCESIONARIO' => $ESCONCESIONARIO,
			'CCOMPANIA' => $CCOMPANIA,
			'DJERARQUIA' => $DJERARQUIA,
			'CTIPO' => $CTIPO,
		];
		if (empty($CAREACLIENTE)) {
			$CAREACLIENTE = $this->obtenerId();
			$data['CAREACLIENTE'] = $CAREACLIENTE;
			$save = $this->db->insert('MAREACLIENTE', $data);
		} else {
			unset($data['CCLIENTE']);
			unset($data['CAREACLIENTE']);
			unset($data['CUSUARIOCREA']);
			unset($data['TCREACION']);
			$data['TMODIFICACION'] = date('Y-m-d');
			$data['CUSUARIOMODIFICA'] = $CUSUARIO;
			$save = $this->db->update('MAREACLIENTE', $data, ['CAREACLIENTE' => $CAREACLIENTE]);
		}

		if (!$save) {
			throw new Exception('Error al guardar el area.');
		}

		return $CAREACLIENTE;
	}

}
