<?php

/**
 * Class matmaquilador
 */
class matmaquilador extends CI_Model
{

	/**
	 * mmaquilador constructor.
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * @param $ccliente
	 * @return array|array[]|object|object[]
	 */
	public function lista($ccliente)
	{
		$this->db->select("
		 	mcp.ccliente as cproveedor,
			mcp.cproveedorcliente as cmaquilador,
			'' as 'SPACE',
			mc.*
		");
		$this->db->from('mrclienteproveedor mcp');
		$this->db->join('mcliente mc', 'mc.ccliente = mcp.cproveedorcliente', 'inner');
		$this->db->where('mcp.ccliente', $ccliente);
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @param $ccliente
	 * @return array|array[]|object|object[]
	 */
	public function listaProv($ccliente)
	{
		$this->db->select("
			DISTINCT
			'' as 'SPACE',
			mc.*
		");
		$this->db->from('mrclienteproveedor mcp');
		$this->db->join('mcliente mc', 'mc.ccliente = mcp.cproveedorcliente', 'inner');
		$this->db->where('mcp.ccliente !=', $ccliente);
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @return string
	 */
	public function obtenerId()
	{
		$query = $this->db->select("
			right(('00000'+cast(((cast(isnull(max(ccliente),0) as integer) + 1)) as char(5))),5) as id
		")
			->from('mcliente')
			->get();
		return ($query && $query->num_rows() > 0) ? $query->row()->id : '00001';
	}

	/**
	 * @param $CCLIENTE
	 * @param $CMAILHOMOLOGACION
	 * @param $COLORFONDO
	 * @param $COLORTEXT
	 * @param $cpais
	 * @param $CUBIGEO
	 * @param $CUSUARIO
	 * @param $DCARGOREPRESENTANTE
	 * @param $dciudad
	 * @param $DDIRECCIONCLIENTE
	 * @param $DEMAILREPRESENTANTE
	 * @param $destado
	 * @param $DFAX
	 * @param $DRAZONSOCIAL
	 * @param $DREPRESENTANTE
	 * @param $DRUTA
	 * @param $DTELEFONO
	 * @param $dweb
	 * @param $dzip
	 * @param $NMESMAILAARR
	 * @param $NRUC
	 * @param $NTRABAJADOR
	 * @param $SREGISTRO
	 * @param $tipodoc
	 * @return bool
	 */
	public function guardar($CPROVEEDORCLIENTE, $CCLIENTE, $CMAILHOMOLOGACION, $COLORFONDO, $COLORTEXT,
							$cpais, $CUBIGEO, $CUSUARIO, $DCARGOREPRESENTANTE, $dciudad,
							$DDIRECCIONCLIENTE, $DEMAILREPRESENTANTE, $destado, $DFAX, $DRAZONSOCIAL,
							$DREPRESENTANTE, $DRUTA, $DTELEFONO, $dweb, $dzip,
							$NMESMAILAARR, $NRUC, $NTRABAJADOR, $SREGISTRO, $tipodoc
	)
	{
		$data = [
			'CCLIENTE' => $CCLIENTE,
			'CGRUPOEMPRESARIAL' => '00000',
			'CMAILHOMOLOGACION' => $CMAILHOMOLOGACION,
			'COLORFONDO' => $COLORFONDO,
			'COLORTEXT' => $COLORTEXT,
			'cpais' => $cpais,
			'CUBIGEO' => $CUBIGEO,
			'DCARGOREPRESENTANTE' => $DCARGOREPRESENTANTE,
			'dciudad' => $dciudad,
			'DDIRECCIONCLIENTE' => $DDIRECCIONCLIENTE,
			'DEMAILREPRESENTANTE' => $DEMAILREPRESENTANTE,
			'destado' => $destado,
			'DFAX' => $DFAX,
			'DRAZONSOCIAL' => $DRAZONSOCIAL,
			'DREPRESENTANTE' => $DREPRESENTANTE,
			'DRUTA' => $DRUTA,
			'DTELEFONO' => $DTELEFONO,
			'dweb' => $dweb,
			'dzip' => $dzip,
			'NMESMAILAARR' => $NMESMAILAARR,
			'NRUC' => $NRUC,
			'NTRABAJADOR' => $NTRABAJADOR,
			'CUSUARIOCREA' => $CUSUARIO,
			'TCREACION' => date('Y-m-d'),
			'SREGISTRO' => $SREGISTRO,
			'STIPOCLIENTE' => 'C',
			'tipodoc' => $tipodoc,
			'ZCTIPOTAMANOEMPRESA' => 112,
		];
		if (empty($CPROVEEDORCLIENTE)) {
			$CPROVEEDORCLIENTE = $this->obtenerId();
			$data['CCLIENTE'] = $CPROVEEDORCLIENTE;
			$this->db->insert('MCLIENTE', $data);
			$data = [
				'CCLIENTE' => $CCLIENTE,
				'CPROVEEDORCLIENTE' => $CPROVEEDORCLIENTE,
				'CUSUARIOCREA' => $CUSUARIO,
				'TCREACION' => date('Y-m-d H:i:s'),
				'SREGISTRO' => 'A',
				'FESNUEVO' => 'C',
			];
			$save = $this->db->insert('MRCLIENTEPROVEEDOR', $data);
		} else {
			unset($data['CCLIENTE']);
			unset($data['CPROVEEDORCLIENTE']);
			unset($data['CUSUARIOCREA']);
			unset($data['TCREACION']);
			$data['TMODIFICACION'] = date('Y-m-d');
			$data['CUSUARIOMODIFICA'] = $CUSUARIO;
			$save = $this->db->update('MCLIENTE', $data, ['CCLIENTE' => $CPROVEEDORCLIENTE]);
		}

		if (!$save) {
			throw new Exception('Error al guardar el cliente.');
		}

		return $CPROVEEDORCLIENTE;
	}

	/**
	 * Agrega nuevo maquilador
	 * @param $CCLIENTE
	 * @param $CPROVEEDORCLIENTE
	 * @param $CUSUARIO
	 * @return bool
	 */
	public function agregar($CCLIENTE, $CPROVEEDORCLIENTE, $CUSUARIO)
	{
		$data = [
			'CCLIENTE' => $CCLIENTE,
			'CPROVEEDORCLIENTE' => $CPROVEEDORCLIENTE,
			'CUSUARIOCREA' => $CUSUARIO,
			'TCREACION' => date('Y-m-d H:i:s'),
			'SREGISTRO' => 'A',
			'FESNUEVO' => 'C',
		];
		return $this->db->insert('MRCLIENTEPROVEEDOR', $data);
	}

}
