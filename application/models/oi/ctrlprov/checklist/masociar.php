<?php

/**
 * Class mcheclistcab_clientes
 */
class masociar extends CI_Model
{

	/**
	 * mcheclistcab_clientes constructor.
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * @param $CCHECKLIST
	 * @param $CCLIENTE
	 * @return array|mixed|object|null
	 */
	public function buscar($CCHECKLIST, $CCLIENTE)
	{
		$query = $this->db->select('*')
			->from('mrchecklistcliente')
			->where('CCHECKLIST', $CCHECKLIST)
			->where('CCLIENTE', $CCLIENTE)
			->get();
		if (!$query) {
			return null;
		}
		return ($query->num_rows() > 0) ? $query->row() : null;
	}

	/**
	 * @param $busqueda
	 * @return array|array[]|object|object[]
	 */
	public function autoCompletable($busqueda)
	{
		$query = $this->db->select("
			CCLIENTE as id,
			STRING(NRUC, ' ', DRAZONSOCIAL) as text,
			*
		")->from('MCLIENTE')
			->like('DRAZONSOCIAL', $busqueda, 'both', false)
			->or_like('NRUC', $busqueda, 'both', false)
			->limitAnyWhere(LIMITE_AUTOCOMPLETADO)
			->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @param $CCHECKLIST
	 * @return array|mixed
	 */
	public function lista($CCHECKLIST)
	{
		$query = $this->db->select('
			MCLIENTE.*
		')->from('mrchecklistcliente')
			->join('MCLIENTE', 'mrchecklistcliente.CCLIENTE = MCLIENTE.CCLIENTE', 'inner')
			->where('mrchecklistcliente.CCHECKLIST', $CCHECKLIST)
			->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @param $CCHECKLIST
	 * @param $CCLIENTE
	 * @param $CUSUARIO
	 * @return array
	 * @throws Exception
	 */
	public function guardar($CCHECKLIST, $CCLIENTE, $CUSUARIO)
	{
		$datos = [
			'CCHECKLIST' => $CCHECKLIST,
			'CCLIENTE' => $CCLIENTE,
			'CUSUARIOCREA' => $CUSUARIO,
			'TCREACION' => date('Y-m-d H:i:s'),
			'CUSUARIOMODIFICA' => null,
			'TMODIFICACION' => null,
			'SREGISTRO' => 'S',
		];
		$res = $this->db->insert('mrchecklistcliente', $datos);
		if (!$res) {
			throw new Exception('No se pudo asociar el cliente al checklist.');
		}
		return $datos;
	}

	/**
	 * @param $CCHECKLIST
	 * @param $CCLIENTE
	 * @return bool
	 * @throws Exception
	 */
	public function eliminar($CCHECKLIST, $CCLIENTE)
	{
		$res = $this->db->delete('mrchecklistcliente', ['CCHECKLIST' => $CCHECKLIST, 'CCLIENTE' => $CCLIENTE]);
		if (!$res) {
			throw new Exception('No se pudo eliminar el asociado del cliente al checklist.');
		}
		return $res;
	}

	/**
	 * Actualiza el estado de asociado en el checklist
	 * @param $idCheckList
	 * @return bool
	 * @throws Exception
	 */
	public function actualizar_checklist($idCheckList)
	{
		$query = $this->db->update('mchecklist', ['SPROPIEDAD' => 'C'], ['CCHECKLIST' => $idCheckList]);
		if (!$query) {
			throw new Exception('No se pudo actualizar el asociado del cliente al checklist.');
		}
		return $query;
	}

}
