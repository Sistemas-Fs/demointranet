<?php

/**
 * Class mformula
 */
class mformula extends CI_Model
{

	/**
	 * mformula constructor.
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * @param $CCHECKLIST
	 * @param $CCLIENTE
	 * @return array|mixed|object|null
	 */
	public function buscar($CCHECKLIST, $CCLIENTE)
	{
		$query = $this->db->select('*')
			->from('mrchecklistformula')
			->where('CCHECKLIST', $CCHECKLIST)
			->where('CFORMULAEVALUACION', $CCLIENTE)
			->get();
		if (!$query) {
			return null;
		}
		return ($query->num_rows() > 0) ? $query->row() : null;
	}

	/**
	 * @param $busqueda
	 * @return array|array[]|object|object[]
	 */
	public function autoCompletable($busqueda)
	{
		$query = $this->db->select("
			CFORMULAEVALUACION as id,
			DFORMULA as text,
			*
		")->from('MFORMULAEVALUACION')
			->like('DFORMULA', $busqueda, 'both', false)
			->limitAnyWhere(LIMITE_AUTOCOMPLETADO)
			->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @param $CCHECKLIST
	 * @return array|mixed
	 */
	public function lista($CCHECKLIST)
	{
		$query = $this->db->select('
			MFORMULAEVALUACION.CFORMULAEVALUACION as id,
			MFORMULAEVALUACION.DFORMULA as text,
			MFORMULAEVALUACION.CFORMULAEVALUACION,
			MFORMULAEVALUACION.DFORMULA,
			mrchecklistformula.SREGISTRO
		')->from('mrchecklistformula')
			->join('MFORMULAEVALUACION', 'mrchecklistformula.CFORMULAEVALUACION = MFORMULAEVALUACION.CFORMULAEVALUACION', 'inner')
			->where('mrchecklistformula.CCHECKLIST', $CCHECKLIST)
			->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @param $CCHECKLIST
	 * @param $CFORMULAEVALUACION
	 * @param $CUSUARIO
	 * @return array
	 * @throws Exception
	 */
	public function guardar($CCHECKLIST, $CFORMULAEVALUACION, $SREGISTRO, $CUSUARIO)
	{
		$datos = [
			'CCHECKLIST' => $CCHECKLIST,
			'CFORMULAEVALUACION' => $CFORMULAEVALUACION,
			'CUSUARIOCREA' => $CUSUARIO,
			'TCREACION' => date('Y-m-d H:i:s'),
			'CUSUARIOMODIFICA' => null,
			'TMODIFICACION' => null,
			'SREGISTRO' => $SREGISTRO,
			'CVALOR' => null,
			'CCRITERIORESULTADO' => null,
			'idItem' => null,
		];
		$res = $this->db->insert('mrchecklistformula', $datos);
		if (!$res) {
			throw new Exception('No se pudo asociar la formula al checklist.');
		}
		return $datos;
	}

	/**
	 * @param $CCHECKLIST
	 * @param $CFORMULAEVALUACION
	 * @param $datos
	 * @return mixed
	 * @throws Exception
	 */
	public function actualizar($CCHECKLIST, $CFORMULAEVALUACION, $datos)
	{
		$res = $this->db->update('mrchecklistformula', $datos, [
			'CCHECKLIST' => $CCHECKLIST,
			'CFORMULAEVALUACION' => $CFORMULAEVALUACION,
		]);
		if (!$res) {
			throw new Exception('No se pudo asociar la formula al checklist.');
		}
		return $datos;
	}

}
