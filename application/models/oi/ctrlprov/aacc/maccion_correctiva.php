<?php

/**
 * Class maccion_correctiva
 */
class maccion_correctiva extends CI_Model
{

	/**
	 * mcons_insp constructor.
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * @param $ccliente
	 * @param $filtro
	 * @return array|array[]|object|object[]
	 */
	public function listaInsp($ccliente, $filtro = "")
	{
		$this->db->select("
			D.DRAZONSOCIAL,
         	A.CAUDITORIAINSPECCION,
         	DATEFORMAT(A.FSERVICIO, 'dd/mm/yyyy') as FINSPECCION,
         	A.FSERVICIO,
         	B.CCLIENTE AS 'CCLIENTE',
			B.CPROVEEDORCLIENTE AS 'CPROVEEDORCLIENTE',
         	(SELECT COUNT() FROM PACCIONCORRECTIVA PA 
			WHERE PA.CAUDITORIAINSPECCION  = A.CAUDITORIAINSPECCION 
			AND PA.FSERVICIO = A.FSERVICIO) AS 'NRODEACCCORRECTIVAS', 
			C.FACEPTACORRECTIVA,
			C.DUBICACIONFILESERVERAC
		");
		$this->db->from("PACCIONCORRECTIVA A");
		$this->db->join("PDAUDITORIAINSPECCION C", "A.CAUDITORIAINSPECCION = C.CAUDITORIAINSPECCION and A.FSERVICIO = C.FSERVICIO", "INNER");
		$this->db->join("PCAUDITORIAINSPECCION B", "A.CAUDITORIAINSPECCION = B.CAUDITORIAINSPECCION", "INNER");
		$this->db->join("MCLIENTE D", "B.CPROVEEDORCLIENTE = D.CCLIENTE", "INNER");
		$this->db->where("B.CCLIENTE", $ccliente);
		if (!empty($filtro)) {
			$this->db->group_start();
			$this->db->like('D.DRAZONSOCIAL', $filtro);
			$this->db->or_like('A.CAUDITORIAINSPECCION', $filtro);
			$this->db->group_end();
		}
		$this->db->group_by('D.DRAZONSOCIAL, A.CAUDITORIAINSPECCION, A.FSERVICIO, B.CCLIENTE, B.CPROVEEDORCLIENTE, C.FACEPTACORRECTIVA, C.DUBICACIONFILESERVERAC', TRUE);
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @param $cauditoria
	 * @param $fservicio
	 * @return array
	 */
	public function listaAACC($cauditoria, $fservicio)
	{
		$sql = "
			SELECT  paccioncorrectiva.cauditoriainspeccion ,           
				paccioncorrectiva.fservicio ,           
				paccioncorrectiva.cchecklist ,           
				paccioncorrectiva.crequisitochecklist ,           
				paccioncorrectiva.daccioncorrectiva ,           
				paccioncorrectiva.saceptaraccioncorrectiva ,           
				paccioncorrectiva.dobservacion ,           
				paccioncorrectiva.cusuariocrea ,           
				paccioncorrectiva.tcreacion ,           
				paccioncorrectiva.cusuariomodifica ,           
				paccioncorrectiva.tmodificacion ,           
				paccioncorrectiva.sregistro ,           
				mrequisitochecklist.dnumerador ,           
				mrequisitochecklist.drequisito ,          
				 mrequisitochecklist.sexcluyente ,          
				 mrequisitochecklist.svalor ,           
				pvalorchecklist.dhallazgo ,           
				pvalorchecklist.dhallazgotext ,           
				paccioncorrectiva.fcorrectiva ,           
				paccioncorrectiva.dresponsablecliente     
				FROM mrequisitochecklist ,           
				paccioncorrectiva ,           
				pvalorchecklist ,           
				pcauditoriainspeccion ,           
				pdauditoriainspeccion ,           
				pcpte ,           
				pdpte
			WHERE ( mrequisitochecklist.cchecklist = paccioncorrectiva.cchecklist ) 
				and ( mrequisitochecklist.crequisitochecklist = paccioncorrectiva.crequisitochecklist ) 
				and ( paccioncorrectiva.cauditoriainspeccion = pvalorchecklist.cauditoriainspeccion ) 
				and ( paccioncorrectiva.fservicio = pvalorchecklist.fservicio ) 
				and ( paccioncorrectiva.cchecklist = pvalorchecklist.cchecklist ) 
				and ( paccioncorrectiva.crequisitochecklist = pvalorchecklist.crequisitochecklist ) 
				and ( pdauditoriainspeccion.cauditoriainspeccion = pcauditoriainspeccion.cauditoriainspeccion ) 
				and ( pvalorchecklist.cauditoriainspeccion = pdauditoriainspeccion.cauditoriainspeccion ) 
				and ( pvalorchecklist.fservicio = pdauditoriainspeccion.fservicio ) 
				and ( pcpte.cinternopte = pdpte.cinternopte )
				 and ( pcauditoriainspeccion.cinternopte = pdpte.cinternopte )
				 and ( pcauditoriainspeccion.norden = pdpte.norden ) 
				and ( ( pdpte.ccompania = 2 ) 
				and ( pdpte.carea = '01' ) 
				and ( pdpte.cservicio = '01' ) 
				and ( paccioncorrectiva.cauditoriainspeccion = '" . $cauditoria . "' ) 
				and ( paccioncorrectiva.fservicio = '" . $fservicio . "' ) )  
		";
		$query = $this->db->query($sql);
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];

	}

}
