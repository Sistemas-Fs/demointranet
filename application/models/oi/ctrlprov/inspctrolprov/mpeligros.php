<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class Mpeligros
 */
class Mpeligros extends CI_Model
{

	/**
	 * mpeligros constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
	}

	/**
	 * Lista de peligros
	 * @param $cauditoria
	 * @param $fservicio
	 * @return array|array[]|object|object[]
	 */
	public function listar($cauditoria, $fservicio)
	{
		$sql = "
			  SELECT ppeligrotipocliente.cauditoriainspeccion,
			         ppeligrotipocliente.fservicio,   
				 	 ppeligrotipocliente.cproductopeligro,
			         ppeligrotipocliente.ctipopeligro,   
				 	 ppeligrotipocliente.norden,
			         ppeligrotipocliente.ddetallepeligro,   
				 	 mdpeligrocliente.dpeligro,
			         mcpeligrocliente.dproductopeligro  
			FROM ppeligrotipocliente,
			     mdpeligrocliente,
			     mcpeligrocliente  
		   WHERE ( ppeligrotipocliente.cproductopeligro = mdpeligrocliente.cproductopeligro ) and  
				 ( ppeligrotipocliente.ctipopeligro = mdpeligrocliente.ctipopeligro ) and  
				 ( ppeligrotipocliente.norden = mdpeligrocliente.norden ) and  
				 ( mdpeligrocliente.cproductopeligro = mcpeligrocliente.cproductopeligro ) and  
				 ( ( ppeligrotipocliente.cauditoriainspeccion = '" . $cauditoria . "' ) AND  
				 ( ppeligrotipocliente.fservicio = '" . $fservicio . "' ) )
		   ORDER BY ppeligrotipocliente.cproductopeligro ASC
		";
		$query = $this->db->query($sql);
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	public function buscar($cauditoria, $fservicio, $cproducto, $ctipopeligro, $norden)
	{
		$sql = "
			SELECT ppeligrotipocliente.cauditoriainspeccion,
			       ppeligrotipocliente.fservicio,   
				   ppeligrotipocliente.cproductopeligro,
			       ppeligrotipocliente.ctipopeligro,   
				   ppeligrotipocliente.norden,
			       ppeligrotipocliente.ddetallepeligro,   
				   ppeligrotipocliente.cusuariocrea,
			       ppeligrotipocliente.tcreacion,   
				   ppeligrotipocliente.cusuariomodifica,
			       ppeligrotipocliente.tmodificacion,   
				   ppeligrotipocliente.sregistro,
			       mdpeligrocliente.dpeligro,
			       mcpeligrocliente.dproductopeligro  
			FROM ppeligrotipocliente,
			     mdpeligrocliente,
			     mcpeligrocliente
		   WHERE ( ppeligrotipocliente.cproductopeligro = mdpeligrocliente.cproductopeligro ) and  
				 ( ppeligrotipocliente.ctipopeligro = mdpeligrocliente.ctipopeligro ) and  
				 ( ppeligrotipocliente.norden = mdpeligrocliente.norden ) and 
		         ( mdpeligrocliente.cproductopeligro = mcpeligrocliente.cproductopeligro ) and
				 ( ( ppeligrotipocliente.cauditoriainspeccion = '" . $cauditoria . "' ) AND  
				 ( ppeligrotipocliente.fservicio = '" . $fservicio . "' ) AND  
				 ( ppeligrotipocliente.cproductopeligro = '" . $cproducto . "' ) AND  
				 ( ppeligrotipocliente.ctipopeligro = '" . $ctipopeligro . "' ))  			 
		";
		// AND ( ppeligrotipocliente.norden = '" . $norden . "' ) )
		$query = $this->db->query($sql);
		if (!$query) {
			return null;
		}
		return ($query->num_rows() > 0) ? $query->row() : null;
	}

	public function listaPeligros($ccliente)
	{
		$sql = "
			  SELECT mcpeligrocliente.cproductopeligro,    mcpeligrocliente.ccliente,  mcpeligrocliente.dproductopeligro  
				FROM mcpeligrocliente  
			   WHERE ( mcpeligrocliente.ccliente = '" . $ccliente . "' )
		";
		$query = $this->db->query($sql);
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	public function listaPeligroProducto($cproducto)
	{
		$sql = "
			SELECT 
			       mdpeligrocliente.cproductopeligro,
			       mdpeligrocliente.ctipopeligro,   
         		   mdpeligrocliente.norden,
			       mdpeligrocliente.dpeligro
			FROM mcpeligrocliente,
			     mdpeligrocliente
		   WHERE ( mcpeligrocliente.cproductopeligro = mdpeligrocliente.cproductopeligro ) and
				 ( mcpeligrocliente.cproductopeligro = '" . $cproducto . "' )  
		";
		$query = $this->db->query($sql);
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

}
