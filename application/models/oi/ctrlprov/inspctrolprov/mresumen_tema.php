<?php

/**
 * Class mresumen_tema
 */
class mresumen_tema extends CI_Model
{

	/**
	 * mresumen_tema constructor.
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * @param $CAUDITORIAINSPECCION
	 * @param $FSERVICIO
	 * @param $ZCINFOADICIONAL
	 * @param $DINFOADICIONAL
	 * @param $CUSUARIO
	 * @return bool
	 */
	public function guardar($CAUDITORIAINSPECCION, $FSERVICIO, $ZCINFOADICIONAL, $DINFOADICIONAL, $CUSUARIO)
	{
		$fecha = date('Y-m-d H:i:s');
		$datos = [
			'CAUDITORIAINSPECCION' => $CAUDITORIAINSPECCION,
			'FSERVICIO' => $FSERVICIO,
			'ZCINFOADICIONAL' => $ZCINFOADICIONAL,
			'DINFOADICIONAL' => $DINFOADICIONAL,
			'CUSUARIOCREA' => $CUSUARIO,
			'TCREACION' => $fecha,
			'CUSUARIOMODIFICA' => $CUSUARIO,
			'TMODIFICACION' => $fecha,
			'SREGISTRO' => 'A',
		];
		$validar = $this->obtenerTema($CAUDITORIAINSPECCION, $FSERVICIO, $ZCINFOADICIONAL);
		if (empty($validar)) {
			// Se crea
			$datos['CUSUARIOMODIFICA'] = null;
			$datos['TMODIFICACION'] = null;
			$resp = $this->db->insert('pinformacionadicional', $datos);
		} else {
			// Se edita
			unset($datos['TCREACION']);
			unset($datos['CUSUARIOCREA']);
			$resp = $this->db->update('pinformacionadicional', $datos, [
				'CAUDITORIAINSPECCION' => $CAUDITORIAINSPECCION,
				'FSERVICIO' => $FSERVICIO,
				'ZCINFOADICIONAL' => $ZCINFOADICIONAL,
			]);
		}
		return $resp;
	}

	/**
	 * @return array|array[]|object|object[]
	 */
	public function obtenerRecursos($cauditoria, $fservicio)
	{
		$query = $this->db->select('
			TTABLA.*,
			PINFORMACIONADICIONAL.ZCINFOADICIONAL,
			PINFORMACIONADICIONAL.DINFOADICIONAL
		')
			->from('ttabla')
			->join('PINFORMACIONADICIONAL', "TTABLA.CTIPO = PINFORMACIONADICIONAL.ZCINFOADICIONAL AND PINFORMACIONADICIONAL.CAUDITORIAINSPECCION = '{$cauditoria}' AND PINFORMACIONADICIONAL.FSERVICIO = '{$fservicio}'", 'left')
			->where('ttabla.CTABLA', '62')
			->order_by('ttabla.NCORRELATIVO', 'ASC')
			->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @param $cauditoria
	 * @param $fservicio
	 * @param $ctipo
	 * @return array|mixed|object|null
	 */
	public function obtenerTema($cauditoria, $fservicio, $ctipo)
	{
		$query = $this->db->select('*')
			->from('pinformacionadicional')
			->where('CAUDITORIAINSPECCION', $cauditoria)
			->where('FSERVICIO', $fservicio)
			->where('ZCINFOADICIONAL', $ctipo)
			->get();
		if (!$query) {
			return null;
		}
		return ($query->num_rows() > 0) ? $query->row() : null;
	}

}
