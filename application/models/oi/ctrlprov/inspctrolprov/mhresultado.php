<?php

/**
 * Class mhresultado
 */
class mhresultado extends CI_Model
{

	/**
	 * mresumen constructor.
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * @return int
	 */
	public function obtenerId()
	{
		$query = $this->db->select('MAX(CHISTORICO) as id')
			->from('HRESULTADOAUDINSP')
			->get();
		if (!$query) {
			return 1;
		}
		return ($query->num_rows() > 0) ? $query->row()->id + 1 : 1;
	}

	/**
	 * @param $CAUDITORIAINSPECCION
	 * @param $FSERVICIO
	 * @return array|mixed|object|null
	 */
	public function buscarInsp($CAUDITORIAINSPECCION, $FSERVICIO)
	{
		$query = $this->db->select('*')
			->from('HRESULTADOAUDINSP')
			->where('CAUDITORIAINSPECCION', $CAUDITORIAINSPECCION)
			->where('FSERVICIO', $FSERVICIO)
			->get();
		if (!$query) {
			return null;
		}
		return ($query->num_rows() > 0) ? $query->row() : null;
	}

	/**
	 * @param $CAUDITORIAINSPECCION
	 * @param $FSERVICIO
	 * @param $CCOMPANIA
	 * @param $CAREA
	 * @param $CSERVICIO
	 * @param $CCLIENTE
	 * @param $CESTABLECIMIENTO
	 * @param $CPROVEEDORCLIENTE
	 * @param $CESTABLECIMIENTOPROV
	 * @param $CMAQUILADORCLIENTE
	 * @param $CESTABLECIMIENTOMAQUILA
	 * @param $NRESULTADO
	 * @param $CLINEAPROCESOCLIENTE
	 * @param $CCRITERIORESULTADO
	 * @param $CDETALLECRITERIORESULTADO
	 * @param $DINFORME
	 * @param $CUSUARIO
	 * @param $SREGISTRO
	 * @param $PRESULTADOCHECKLIST
	 * @param $ZCTIPOESTADOSERVICIO
	 */
	public function guardar($CAUDITORIAINSPECCION, $FSERVICIO, $CCOMPANIA, $CAREA, $CSERVICIO, $CCLIENTE, $CESTABLECIMIENTO,
							$CPROVEEDORCLIENTE, $CESTABLECIMIENTOPROV, $CMAQUILADORCLIENTE, $CESTABLECIMIENTOMAQUILA,
							$NRESULTADO, $CLINEAPROCESOCLIENTE, $CCRITERIORESULTADO, $CDETALLECRITERIORESULTADO,
							$DINFORME, $CUSUARIO, $SREGISTRO, $PRESULTADOCHECKLIST, $ZCTIPOESTADOSERVICIO)
	{
		$fecha = date('Y-m-d H:i:s');
		$data = [
			'CHISTORICO' => 0,
			'CAUDITORIAINSPECCION' => $CAUDITORIAINSPECCION,
			'FSERVICIO' => $FSERVICIO,
			'CCOMPANIA' => $CCOMPANIA,
			'CAREA' => $CAREA,
			'CSERVICIO' => $CSERVICIO,
			'CCLIENTE' => $CCLIENTE,
			'CESTABLECIMIENTO' => $CESTABLECIMIENTO,
			'CPROVEEDORCLIENTE' => $CPROVEEDORCLIENTE,
			'CESTABLECIMIENTOPROV' => $CESTABLECIMIENTOPROV,
			'CMAQUILADORCLIENTE' => $CMAQUILADORCLIENTE,
			'CESTABLECIMIENTOMAQUILA' => $CESTABLECIMIENTOMAQUILA,
			'NRESULTADO' => $NRESULTADO,
			'CLINEAPROCESOCLIENTE' => $CLINEAPROCESOCLIENTE,
			'CCRITERIORESULTADO' => $CCRITERIORESULTADO,
			'CDETALLECRITERIORESULTADO' => $CDETALLECRITERIORESULTADO,
			'DINFORME' => $DINFORME,
			'CUSUARIOCREA' => $CUSUARIO,
			'TCREACION' => $fecha,
			'CUSUARIOMODIFICA' => $CUSUARIO,
			'TMODIFICACION' => $fecha,
			'SREGISTRO' => $SREGISTRO,
			'PRESULTADOCHECKLIST' => $PRESULTADOCHECKLIST,
			'ZCTIPOESTADOSERVICIO' => $ZCTIPOESTADOSERVICIO,
		];

		$insp = $this->buscarInsp($CAUDITORIAINSPECCION, $FSERVICIO);
		$resp = false;
		if (empty($insp)) {
			// Se crea
			$data['CHISTORICO'] = $this->obtenerId();
			$data['CUSUARIOMODIFICA'] = null;
			$data['TMODIFICACION'] = null;
			$resp = $this->db->insert('HRESULTADOAUDINSP', $data);
		} else {
			unset($data['CHISTORICO']);
			unset($data['CAUDITORIAINSPECCION']);
			unset($data['FSERVICIO']);
			unset($data['TCREACION']);
			unset($data['CUSUARIOCREA']);
			// Se edita
			$resp = $this->db->update('HRESULTADOAUDINSP', $data, [
				'CAUDITORIAINSPECCION' => $CAUDITORIAINSPECCION,
				'FSERVICIO' => $FSERVICIO,
			]);
			$resp = true;
		}
		return $resp;
	}

}
