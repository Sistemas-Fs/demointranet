<?php

/**
 * Class maccion_correctiva
 */
class maccion_correctiva extends CI_Model
{


	/**
	 * minspctrolprovacc constructor.
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * @param $CAUDITORIAINSPECCION
	 * @param $FSERVICIO
	 * @param $CCHECKLIST
	 * @param $CREQUISITOCHECKLIST
	 * @param $DACCIONCORRECTIVA
	 * @param $FCORRECTIVA
	 * @param $SACEPTARAACCIONCORRECTIVA
	 * @param $DOBSERVACIOM
	 * @param $CUSUARIO
	 * @param $SREGISTRO
	 * @param $FACEPTACIONES
	 * @param $FINGRESOACCION
	 * @param $DRESPONSABLECLIENTE
	 * @return bool
	 */
	public function guardar($CAUDITORIAINSPECCION, $FSERVICIO, $CCHECKLIST, $CREQUISITOCHECKLIST, $DACCIONCORRECTIVA,
							$FCORRECTIVA, $SACEPTARACCIONCORRECTIVA, $DOBSERVACION, $CUSUARIO, $SREGISTRO,
							$FACEPTACIONFS = null, $FINGRESOACCION = null, $DRESPONSABLECLIENTE = null)
	{
		$data = [
			'CAUDITORIAINSPECCION' => $CAUDITORIAINSPECCION,
			'FSERVICIO' => $FSERVICIO,
			'CCHECKLIST' => $CCHECKLIST,
			'CREQUISITOCHECKLIST' => $CREQUISITOCHECKLIST,
			'DACCIONCORRECTIVA' => $DACCIONCORRECTIVA,
			'FCORRECTIVA' => $FCORRECTIVA,
			'SACEPTARACCIONCORRECTIVA' => $SACEPTARACCIONCORRECTIVA,
			'DOBSERVACION' => $DOBSERVACION,
			'SREGISTRO' => $SREGISTRO,
			'FACEPTACIONFS' => $FACEPTACIONFS,
			'FINGRESOACCION' => $FINGRESOACCION,
			'DRESPONSABLECLIENTE' => $DRESPONSABLECLIENTE,
		];

		$ac = $this->buscar([
			'@cauditoria' => $CAUDITORIAINSPECCION,
			'@fservicio' => $FSERVICIO,
			'@cchecklist' => $CCHECKLIST,
			'@crequisitochecklist' => $CREQUISITOCHECKLIST,
		]);

		if ($ac) {
			// Se eliman los Key
			unset($data['CAUDITORIAINSPECCION']);
			unset($data['FSERVICIO']);
			unset($data['CCHECKLIST']);
			unset($data['CREQUISITOCHECKLIST']);
			$data['CUSUARIOMODIFICA'] = $CUSUARIO;
			$data['TMODIFICACION'] = date('Y-m-d h:i:s');
			$query = $this->db->update('PACCIONCORRECTIVA', $data, [
				'CAUDITORIAINSPECCION' => $CAUDITORIAINSPECCION,
				'FSERVICIO' => $FSERVICIO,
				'CCHECKLIST' => $CCHECKLIST,
				'CREQUISITOCHECKLIST' => $CREQUISITOCHECKLIST,
			]);
			if (!$query) {
				throw new Exception('Error en el proceso de ejecución al actualizar le A.C.');
			}
		} else {
			$data['CUSUARIOCREA'] = $CUSUARIO;
			$data['TCREACION'] = date('Y-m-d h:i:s');
			$query = $this->db->insert('PACCIONCORRECTIVA', $data);
			if (!$query) {
				throw new Exception('Error en el proceso de ejecución al crear le A.C.');
			}
		}
		return $query;
	}

	/**
	 * @param $parametros
	 * @return array
	 */
	public function lista($parametros)
	{ // Recupera Listado de Propuestas
		$procedure = "call usp_at_ctrlprov_getlistaaccioncorrectiva(?,?)";
		$query = $this->db->query($procedure, $parametros);
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return [];
	}

	/**
	 * @param $parametros
	 * @return null
	 */
	public function buscar($parametros)
	{ // Recupera Listado de Propuestas
		$procedure = "call usp_at_ctrlprov_getbuscaaccioncorrectiva(?,?,?,?)";
		$query = $this->db->query($procedure, $parametros);
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		return null;
	}

}
