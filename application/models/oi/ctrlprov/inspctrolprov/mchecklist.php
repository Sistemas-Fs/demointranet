<?php


class mchecklist extends CI_Model
{

	/**
	 * mchecklist constructor.
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Recupera los checklist de la inspeccion
	 * @param $parametros
	 * @return array
	 */
	public function lista($parametros)
	{
		$procedure = "call usp_oi_ctrlprov_getlistachecklist(?,?)";
		$query = $this->db->query($procedure, $parametros);
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * Devuelve los valores para el checklist
	 * @param $parametros
	 * @return mixed
	 */
	public function valores($parametros)
	{
		$procedure = "call usp_oi_ctrlprov_getvalorchecklist(?)";
		$query = $this->db->query($procedure, $parametros);
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @param $parametros
	 * @return string
	 */
	public function listaValores($parametros)
	{
		$resultado = $this->valores($parametros);
		$listas = '<option value="" selected="selected">::Elegir</option>';
		if ($resultado) {
			foreach ($resultado as $row) {
				$listas .= '<option value="' . $row->cdetallevalor . '" data-tipovalor="' . $row->stipovalor. '" >' . $row->ddetallevalor . '</option>';
			}
		}
		return $listas;
	}

	/**
	 * @param $cauditoria
	 * @param $fservicio
	 * @return array|array[]|object|object[]
	 */
	public function criterioHallazgos($cauditoria, $fservicio)
	{
		$this->db->select('*');
		$this->db->from('MDETALLEVALOR');
		$this->db->where_in('cvalor', "(select distinct cvalornoconformidad from PVALORCHECKLIST where CAUDITORIAINSPECCION = '" . $cauditoria . "' and FSERVICIO = '" . $fservicio . "')", false);
		$this->db->order_by('ndetallevalor', 'desc');
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @param $cauditoria
	 * @param $fservicio
	 * @return string
	 */
	public function listaCriterioHallazgos($cauditoria, $fservicio)
	{
		$resultado = $this->criterioHallazgos($cauditoria, $fservicio);
		$listas = '<option value="" selected="selected">::Elegir</option>';
		if (!empty($resultado)) {
			foreach ($resultado as $key => $row) {
				$listas .= '<option value="' . $row->CDETALLEVALOR . '" data-valor="' . $row->NDETALLEVALOR . '" data-tipovalor="' . $row->STIPOVALOR . '" >' . $row->DDETALLEVALOR . ' ---> ' . $row->NDETALLEVALOR . '</option>';
			}
		}
		return $listas;
	}

	/**
	 * Guarda el checklist
	 * @param $parametros
	 * @return false|int
	 */
	public function guardar($parametros)
	{
		$procedure = "call usp_oi_ctrlprov_update_checklist(?,?,?,?,?,?,?);";
		$query = $this->db->query($procedure, $parametros);
		return ($query == true) ? 1 : false;
	}

	/**
	 * @param $CCRITERIORESULTADO
	 * @param null $CDETALLECRITERIORESULTADO
	 * @return array|array[]|object|object[]
	 */
	public function buscarDetalleResultado($CCRITERIORESULTADO, $CDETALLECRITERIORESULTADO = null)
	{
		$this->db->select('*');
		$this->db->from('MDETALLECRITERIORESULTADO');
		$this->db->where('CCRITERIORESULTADO', $CCRITERIORESULTADO);
		if (!empty($CDETALLECRITERIORESULTADO)) {
			$this->db->where('CDETALLECRITERIORESULTADO', $CDETALLECRITERIORESULTADO);
		}
		$this->db->where('SREGISTRO', 'A');
		$this->db->order_by('CDETALLECRITERIORESULTADO', 'ASC');
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @param $cauditoriainspeccion
	 * @param $fservicio
	 * @param $cchecklist
	 * @param $crequisito
	 * @return array|mixed|object|null
	 */
	public function buscar($cauditoriainspeccion, $fservicio, $cchecklist, $crequisito)
	{
		$this->db->select('PVALORCHECKLIST.*');
		$this->db->from('PVALORCHECKLIST');
//		$this->db->join('MREQUISITOCHECKLIST', 'PVALORCHECKLIST.CCHECKLIST = MREQUISITOCHECKLIST.CCHECKLIST AND PVALORCHECKLIST.CREQUISITOCHECKLIST = MREQUISITOCHECKLIST.CREQUISITOCHECKLIST', 'inner');
		$this->db->where('PVALORCHECKLIST.CAUDITORIAINSPECCION', $cauditoriainspeccion);
		$this->db->where('PVALORCHECKLIST.FSERVICIO', $fservicio);
		$this->db->where('PVALORCHECKLIST.CCHECKLIST', $cchecklist);
		$this->db->where('PVALORCHECKLIST.CREQUISITOCHECKLIST', $crequisito);
		$query = $this->db->get();
		if (!$query) {
			return null;
		}
		return ($query->num_rows() > 0) ? $query->row() : null;
	}

	/**
	 * @param $cvalor
	 * @param $cdetallevalor
	 * @return array|mixed|object|null
	 */
	public function buscarValor($cvalor, $cdetallevalor)
	{
		$this->db->select('*');
		$this->db->from('MDETALLEVALOR');
		$this->db->where('CVALOR', $cvalor);
		$this->db->where('CDETALLEVALOR', $cdetallevalor);
		$query = $this->db->get();
		if (!$query) {
			return null;
		}
		return ($query->num_rows() > 0) ? $query->row() : null;
	}

}
