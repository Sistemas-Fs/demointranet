<?php

/**
 * Class minvolucrados
 */
class minvolucrados extends CI_Model
{

	/**
	 * minvolucrados constructor.
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * @return array|array[]|object|object[]
	 */
	public function listaTipo()
	{
		$this->db->select('ttabla.*');
		$this->db->from('ttabla');
		$this->db->where('ttabla.ctabla', '32');
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @param $cauditoria
	 * @param $fservicio
	 * @return array|array[]|object|object[]
	 */
	public function listaReuniones($cauditoria, $fservicio)
	{
		$this->db->select('pcreunionaudinsp.*, ttabla.dregistro');
		$this->db->from('pcreunionaudinsp');
		$this->db->join('ttabla', 'pcreunionaudinsp.ZCTIPOREUNION = ttabla.ctipo', 'inner');
		$this->db->where('pcreunionaudinsp.cauditoriainspeccion', $cauditoria);
		$this->db->where('pcreunionaudinsp.fservicio', $fservicio);
		$this->db->order_by('pcreunionaudinsp.zctiporeunion', 'ASC');
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @param $cauditoria
	 * @param $fservicio
	 * @param $zctiporeunion
	 * @return array|array[]|object|object[]
	 */
	public function listaCReuniones($cauditoria, $fservicio, $zctiporeunion = null)
	{
		$this->db->select('*');
		$this->db->from('pcreunionaudinsp');
		$this->db->where('pcreunionaudinsp.cauditoriainspeccion', $cauditoria);
		$this->db->where('pcreunionaudinsp.fservicio', $fservicio);
		if (!empty($zctiporeunion)) {
			$this->db->where('pcreunionaudinsp.zctiporeunion', $zctiporeunion);
		}
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @param $cauditoria
	 * @param $fservicio
	 * @param $zctiporeunion
	 * @return array|array[]|object|object[]
	 */
	public function listaDReuniones($cauditoria, $fservicio, $zctiporeunion)
	{
		$this->db->select('*');
		$this->db->from('pdreunionaudinsp');
		$this->db->where('pdreunionaudinsp.cauditoriainspeccion', $cauditoria);
		$this->db->where('pdreunionaudinsp.fservicio', $fservicio);
		$this->db->where('pdreunionaudinsp.zctiporeunion', $zctiporeunion);
		$this->db->order_by('pdreunionaudinsp.NASISTENTE', 'ASC');
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @param $cauditoria
	 * @param $fservicio
	 * @param $zctiporeunion
	 * @return int
	 */
	public function maxAsistente($cauditoria, $fservicio, $zctiporeunion)
	{
		$this->db->select('MAX(NASISTENTE) AS NASISTENTE');
		$this->db->from('pdreunionaudinsp');
		$this->db->where('pdreunionaudinsp.cauditoriainspeccion', $cauditoria);
		$this->db->where('pdreunionaudinsp.fservicio', $fservicio);
		$this->db->where('pdreunionaudinsp.zctiporeunion', $zctiporeunion);
		$query = $this->db->get();
		if (!$query) {
			return 1;
		}
		return ($query->num_rows() > 0) ? intval($query->row()->NASISTENTE) : 1;
	}

	/**
	 * @param $cauditoria
	 * @param $fservicio
	 * @param $zctiporeunion
	 * @return array|array[]|object|object[]
	 */
	public function lista($cauditoria, $fservicio)
	{
		$sql = "
		  SELECT pcreunionaudinsp.cauditoriainspeccion,
		         pcreunionaudinsp.zctiporeunion,   
         		 pcreunionaudinsp.freunion,
		         pcreunionaudinsp.hinicioreunion,   
         		 pcreunionaudinsp.hfinreunion,
		         pcreunionaudinsp.sregistro,   
         		 pdreunionaudinsp.dnombreasistente,
		         pdreunionaudinsp.dcargoasistente  
    FROM pcreunionaudinsp,
         pdreunionaudinsp  
   WHERE ( pdreunionaudinsp.cauditoriainspeccion = pcreunionaudinsp.cauditoriainspeccion ) and   
         ( pdreunionaudinsp.fservicio = pcreunionaudinsp.fservicio ) and
         ( pdreunionaudinsp.zctiporeunion = pcreunionaudinsp.zctiporeunion ) and  
         ( ( pcreunionaudinsp.cauditoriainspeccion = '" . $cauditoria . "' ) AND  
         ( pcreunionaudinsp.fservicio = '" . $fservicio . "' ) ) 
		";
		$query = $this->db->query($sql);
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

}

