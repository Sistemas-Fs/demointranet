<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class Minspeccion
 */
class Minspeccion extends CI_Model
{
	function __construct()
	{
		parent:: __construct();
		$this->load->library('session');
	}

	/**
	 * @param $cauditoria
	 * @param $fservicio
	 * @return mixed
	 */
	public function buscar($cauditoria, $fservicio)
	{
		$sql = "
			SELECT '['+A.cauditoriainspeccion+'] :: '+
				(select case IsNull( pcai.cmaquiladorcliente,'') 
						when '' 
						then (select drazonsocial from MCLIENTE C where c.ccliente = pcai.cproveedorcliente ) 
						else 
							(select CL.drazonsocial from MCLIENTE CL where CL.ccliente = PCAI.cproveedorcliente )  +' ('+
							(select CL.drazonsocial from MCLIENTE CL where CL.ccliente = PCAI.cmaquiladorcliente )+')' 
						end +' - '+MEC.dcortaestablecimiento+' '+MEC.destablecimiento 
				 from mestablecimientocliente MEC
					join pcauditoriainspeccion PCAI on
							MEC.ccliente = (case Isnull(PCAI.cestablecimientoprov,'0') when '0'  then PCAI.cmaquiladorcliente else PCAI.cproveedorcliente end ) and 
							MEC.cestablecimiento = (case Isnull(PCAI.cestablecimientoprov,'0') when '0' then PCAI.cestablecimientomaquila else PCAI.cestablecimientoprov end )
				 where PCAI.cauditoriainspeccion = A.cauditoriainspeccion ) + ' ('+(F.ddepartamento+'-'+F.dprovincia+'-'+F.ddistrito)+')' as 'desc_gral',
				(select AC.dareacliente from mareacliente AC where AC.ccliente = A.ccliente and AC.careacliente = A.careacliente) as 'areacli',
				(select ML.dlineaclientee from MLINEAPROCESOCLIENTE ML  where ML.clineaprocesocliente = A.clineaprocesocliente) as 'lineaproc',
				(convert(varchar(4),year(g.fcreacion))+'-'+RIGHT(('00'+(convert(varchar(2),month(g.fcreacion)))),2)) as 'periodo',
				(SELECT TT.DREGISTRO FROM TTABLA TT WHERE TT.CTIPO =  g.ZCTIPOESTADOSERVICIO )as 'destado',DATEFORMAT(g.fservicio,'dd/mm/yyyy') as 'finspeccion',
				IsNull( (SELECT DNOMBRE+' '+MU.DAPEPAT FROM MUSUARIO MU WHERE MU.CUSUARIO = g.CUSUARIOCONSULTOR ),'POR ASIGNAR') AS 'inspector',	
				(case when presultadochecklist is null then '' else cast(cast(presultadochecklist as decimal(12,2)) as varchar(10))+ ' % ' end) + 
					(case when isnull(descripcion_result,'') = '' then '' else '(' + descripcion_result +')' end ) as 'resultado', 
				ISNULL(g.dinformefinal,'') AS 'dinformefinal', 
				(SELECT TT.NVALOR FROM TTABLA TT WHERE TT.CTIPO =  g.ZCTIPOESTADOSERVICIO )as 'valorestado',
				A.cauditoriainspeccion, A.sregistro, D.dsubservicio, A.ccliente, A.zctiposervicio, F.cubigeo,
				(SELECT MD.DDETALLECRITERIORESULTADO FROM MDETALLECRITERIORESULTADO MD WHERE MD.CCRITERIORESULTADO = g.ccriterioresultado AND	MD.CDETALLECRITERIORESULTADO = g.cdetallecriterioresultado) as 'descripcion_result',
				(select destablecimiento from MESTABLECIMIENTOCLIENTE ME where ME.ccliente =  A.cmaquiladorcliente and ME.CESTABLECIMIENTO = A.cestablecimientomaquila) as destab_maq,
				(select destablecimiento from MESTABLECIMIENTOCLIENTE ME where ME.ccliente =  A.cproveedorcliente and ME.CESTABLECIMIENTO = A.cestablecimientoprov) as destab_prov,
				(select CL.drazonsocial from MCLIENTE CL where CL.ccliente = A.cproveedorcliente) as 'proveedor',
				(select ML.speligro from MLINEAPROCESOCLIENTE ML where ML.clineaprocesocliente = A.clineaprocesocliente) as espeligro,
				isnull(G.cusuarioconsultor,'') as 'cusuarioconsultor',G.fservicio,isnull(G.cnorma,'') as 'cnorma',isnull(G.csubnorma,'') as 'csubnorma',isnull(G.cchecklist,'') as 'cchecklist',
				isnull(G.cmodeloinforme,'') as 'cmodeloinforme',isnull(G.cvalornoconformidad,'') as 'cvalornoconformidad',isnull(G.cformulaevaluacion,'') as 'cformulaevaluacion',isnull(G.ccriterioresultado,'') as 'ccriterioresultado',isnull(G.dcomentario,'') as 'dcomentario',
				G.zctipoestadoservicio, G.SSERVICIOREALIZADO, G.SCIERRESERVICIO2, isnull(sultimo,'N') as 'sultimo', G.spermitircorrectivas, G.sincluyeplan, G.zctiposervicio, A.cproveedorcliente, isnull(A.cmaquiladorcliente,'') as 'cmaquiladorcliente',
				A.CCLIENTE AS ccliente,
				(select Z.drazonsocial from MCLIENTE Z where Z.ccliente = A.ccliente) as 'dcliente',
				(select COUNT(1) from pplanaudinsp Z where Z.cauditoriainspeccion = G.cauditoriainspeccion and Z.fservicio = G.fservicio) as 'splan',
				G.CGRUPOCRITERIO AS cgrupocriterio,
			   	G.DUBICACIONFILESERVERAC,
			   	G.FINFORMEFIN,
			   (SELECT DCHECKLIST FROM mchecklist WHERE mchecklist.cchecklist = G.cchecklist) AS 'CHECKLIST',
				(SELECT DISTINCT msistema.dsistema + ' >> ' + mnorma.dnorma FROM mnorma, msistema, mchecklist 
                WHERE ( msistema.csistema = mnorma.csistema ) and ( mnorma.cnorma = mchecklist.cnorma ) and  
                      ( msistema.ccompania = mchecklist.ccompania ) and ( mnorma.sregistro = 'A' ) AND
                       mchecklist.cchecklist = G.cchecklist) AS 'SISTEMA',
				(SELECT dsubnorma 
				FROM msubnorma 
				    INNER JOIN mchecklist ON  msubnorma.cnorma = mchecklist.CNORMA AND MSUBNORMA.CSUBNORMA = MCHECKLIST.CSUBNORMA 
				WHERE mchecklist.CCHECKLIST = G.CCHECKLIST) as 'RUBRO',
				G.CUBIGEOMUNICIPALIDAD,
			    G.DLICENCIAFUNCIONAMIENTO,
			   	G.DADICIONALLICENCIA
			FROM pcauditoriainspeccion A
				JOIN pdpte B ON B.cinternopte = A.cinternopte AND B.norden = A.norden
				JOIN pcpte C ON C.cinternopte = B.cinternopte   
				JOIN msubservicio D ON D.carea = B.carea AND D.cservicio = B.cservicio AND D.csubservicio = B.csubservicio AND D.ccompania = B.ccompania
				JOIN mestablecimientocliente E ON 
						E.ccliente = (case Isnull(A.cestablecimientoprov,'0') when '0' then A.cmaquiladorcliente else A.cproveedorcliente end) AND   
						E.cestablecimiento =  (case Isnull(A.cestablecimientoprov,'0') when '0' then A.cestablecimientomaquila else A.cestablecimientoprov end)
				JOIN tubigeo F ON F.cubigeo = E.cubigeo
				JOIN PDAUDITORIAINSPECCION G ON G.cauditoriainspeccion = A.cauditoriainspeccion 	
			WHERE B.ccompania = '2' AND   
				B.carea = '01' AND
				B.cservicio = '01' AND
				G.CAUDITORIAINSPECCION = '{$cauditoria}' AND
				G.FSERVICIO = '{$fservicio}'
			ORDER BY proveedor, areacli, lineaproc, periodo desc, sultimo desc, finspeccion desc
		";
		$query = $this->db->query($sql);
		if (!$query) {
			return null;
		}
		return ($query->num_rows() > 0) ? $query->row() : null;
	}

	/**
	 * @param $cauditoria
	 * @param $fservicio
	 * @return array|mixed|object|null
	 */
	public function buscarDauditoria($cauditoria, $fservicio)
	{
		$this->db->select('*');
		$this->db->from('PDAUDITORIAINSPECCION');
		$this->db->where_in('cauditoriainspeccion', $cauditoria);
		$this->db->where_in('FSERVICIO', $fservicio);
		$query = $this->db->get();
		if (!$query) {
			return null;
		}
		return ($query->num_rows() > 0) ? $query->row() : null;
	}

	/**
	 * @param $cauditoria
	 * @return array|mixed|object|null
	 */
	public function buscarCauditoria($cauditoria)
	{
		$this->db->select('*');
		$this->db->from('PCAUDITORIAINSPECCION');
		$this->db->where_in('CAUDITORIAINSPECCION', $cauditoria);
		$query = $this->db->get();
		if (!$query) {
			return null;
		}
		return ($query->num_rows() > 0) ? $query->row() : null;
	}

	/**
	 * Se obtiene el resumen de una inspección
	 * @param $cauditoria
	 * @param $fservicio
	 * @return mixed
	 */
	public function obtenerResumen($cauditoria, $fservicio)
	{
		$sql = "
			SELECT pdauditoriainspeccion.cauditoriainspeccion,   
					 pdauditoriainspeccion.fservicio,   
					 pdauditoriainspeccion.nresultadochecklist,   
					 pdauditoriainspeccion.nresultadonoconformidad,   
					 pdauditoriainspeccion.ngastogeneral,   
					 pdauditoriainspeccion.dresponsablecliente,   
					 pdauditoriainspeccion.cmodeloinforme,   
					 pdauditoriainspeccion.nrealhoraefectiva,   
					 pdauditoriainspeccion.nrealhoranoefectiva  ,
					 pdauditoriainspeccion.dinformefinal  ,
					 pdauditoriainspeccion.finformefin  ,
			       	 pdauditoriainspeccion.CUBIGEOMUNICIPALIDAD,
					 pdauditoriainspeccion.SLICENCIAFUNCIONAMIENTO,
					 pdauditoriainspeccion.DLICENCIAFUNCIONAMIENTO,
					 pdauditoriainspeccion.DADICIONALLICENCIA,
					 pdauditoriainspeccion.DUBICACIONFILESERVER,
					 pdauditoriainspeccion.DUBICACIONFILESERVERQUEJA,
					 pdauditoriainspeccion.ZCTIPOESTADOSERVICIO,
			(SELECT TOP 1 MD.DDETALLECRITERIORESULTADO
			FROM  MDETALLECRITERIORESULTADO MD
			WHERE CCRITERIORESULTADO = (SELECT CCRITERIORESULTADO FROM PDAUDITORIAINSPECCION PD WHERE PD.CAUDITORIAINSPECCION = pdauditoriainspeccion.CAUDITORIAINSPECCION AND PD.FSERVICIO =pdauditoriainspeccion.fservicio ) 
			AND MD.NVALORINICIAL <= (isnull(pdauditoriainspeccion.presultadochecklist,0)) 
			ORDER BY MD.NVALORINICIAL DESC	)	as descripcion_result,
			(Select dalcanceplan from PPLANAUDINSP PP  where PP.cauditoriainspeccion = pdauditoriainspeccion.cauditoriainspeccion and PP.fservicio =  pdauditoriainspeccion.fservicio ) as alcance	,
				pdauditoriainspeccion.dalcanceinforme,
				pdauditoriainspeccion.dmarcainforme,
			   pdauditoriainspeccion.presultadochecklist 
				,pdauditoriainspeccion.dpermisoautoridadsanitaria  
				,pdauditoriainspeccion.csubnorma,
			(select dsubnorma from pdauditoriainspeccion P inner join MSUBNORMA S on P.cnorma = S.cnorma and P.CSUBNORMA = S.CSUBNORMA where P.cauditoriainspeccion =  pdauditoriainspeccion.cauditoriainspeccion and P.fservicio =  pdauditoriainspeccion.fservicio  ) as rubro,
			pdauditoriainspeccion.ccertificacion,
				pdauditoriainspeccion.dmarcainforme2, (select ccompania from pdpte key join PcAUDITORIAINSPECCION  where PcAUDITORIAINSPECCION.CAUDITORIAINSPECCION = pdauditoriainspeccion.cauditoriainspeccion) as ccompania, scertificacion,
				 pdauditoriainspeccion.DUBICACIONFILESERVERIP,  pdauditoriainspeccion.dubicacionfileserver
			   FROM pdauditoriainspeccion  
			   WHERE ( pdauditoriainspeccion.cauditoriainspeccion = '{$cauditoria}' ) AND  
					 ( pdauditoriainspeccion.fservicio = '{$fservicio}' )
		";
		$query = $this->db->query($sql);
		if (!$query) {
			return null;
		}
		return ($query->num_rows() > 0) ? $query->row() : null;
	}

	/**
	 * @return array|array[]|object|object[]
	 */
	public function obtenerInformes()
	{
		$res = $this->db->select('*')
			->from('MMODELOINFORME')
			->where('CCOMPANIA', 1)
			->where('CAREA', '01')
			->where('CSERVICIO', '02')
			->get();
		if (!$res) {
			return [];
		}
		return ($res->num_rows() > 0) ? $res->result() : [];
	}

	/**
	 * Certificaciones
	 * @return array|array[]|object|object[]
	 */
	public function obtenerCertificaciones()
	{
		$this->db->select('
			MCERTIFICACIONES.*,
			(SELECT DRAZONSOCIAL FROM MCERTIFICADORA WHERE MCERTIFICACIONES.CCERTIFICADORA = CCERTIFICADORA) as CERTIFICADORA
		');
		$this->db->from('MCERTIFICACIONES');
		$this->db->where('MCERTIFICACIONES.CCOMPANIA', 1);
		$this->db->where('MCERTIFICACIONES.SREGISTRO', 'A');
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/** CONTROL DE PROVEEDORES **/
	public function getcboclieserv()
	{ // Visualizar Clientes del servicio en CBO

		$procedure = "call usp_at_ctrlprov_getcboclieserv()";
		$query = $this->db->query($procedure);

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->IDCLIE . '">' . $row->DESCRIPCLIE . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function getcboinspector($parametros)
	{ // Visualizar Clientes del servicio en CBO

		$procedure = "call usp_at_ctrlprov_getcboinspector(?)";
		$query = $this->db->query($procedure, $parametros);

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->IDINSP . '">' . $row->DESCRIPINSP . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	/**
	 * Visualizar Estado en CBO
	 * @return false|string
	 */
	public function getcboestado()
	{
		$procedure = "call usp_at_ctrlprov_getcboestado()";
		$query = $this->db->query($procedure);
		if ($query->num_rows() > 0) {
			$listas = '';
			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->IDESTADO . '">' . $row->DESCRIPESTADO . '</option>';
			}
			return $listas;
		}
		return false;
	}

	public function getcbocalif()
	{ // // Visualizar Calificacion en CBO

		$procedure = "call usp_oi_ctrlprov_getcbocalif()";
		$query = $this->db->query($procedure);

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->IDCALIF . '">' . $row->DESCRIPCALIF . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	/**
	 * Recupera Listado de Propuestas
	 * @param $parametros
	 * @return array|false
	 */
	public function getbuscarctrlprov($parametros)
	{
		$procedure = "call usp_oi_ctrlprov_getbuscarctrlprov_porinsp(?,?,?,?,?,?,?,?,?,?,?,?,?)";
		$query = $this->db->query($procedure, $parametros);
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return false;
	}

	/**
	 * Recupera Listado de Propuestas
	 * @param $parametros
	 * @return array|false
	 */
	public function getbuscarctrlprovHistorico($parametros)
	{
		$procedure = "call usp_at_ctrlprov_getbuscarctrlprov_historico(?,?)";
		$query = $this->db->query($procedure, $parametros);
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return false;
	}

	public function getrecuperainsp($parametros)
	{ // Recupera Listado de Propuestas
		$procedure = "call usp_at_ctrlprov_getrecuperainsp(?)";
		$query = $this->db->query($procedure, $parametros);

		if ($query->num_rows() > 0) {
			return $query->result();
		}
		{
			return False;
		}
	}

	public function getcboregestable($parametros)
	{ // Visualizar Proveedor por cliente en CBO

		$procedure = "call usp_at_ctrlprov_getcboregestable(?,?,?,?)";
		$query = $this->db->query($procedure, $parametros);

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->CESTABLECIMIENTO . '">' . $row->DESTABLECIMIENTO . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function getdirestable($cestable)
	{ // Listar Ensayos
		$sql = "select ((select x.DDEPARTAMENTO+' - '+x.DPROVINCIA+' - '+x.DDISTRITO from TUBIGEO x where x.cubigeo = z.cubigeo) +' - '+ z.DDIRECCION) as 'DIRESTABLE' from MESTABLECIMIENTOCLIENTE z where z.CESTABLECIMIENTO = '" . $cestable . "';";
		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {
			return $query->result();
		}
		{
			return False;
		}
	}

	public function getcboareaclie($ccliente)
	{ // Listar Ensayos
		$sql = "select careacliente, dareacliente from mareacliente where ccliente  = '" . $ccliente . "' and ccompania = '2';";
		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->careacliente . '">' . $row->dareacliente . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function getcbolineaproc($ccliente)
	{ // Listar Ensayos
		$sql = "select clineaprocesocliente, dlineaclientee + (if speligro = 'S' then '  --> ( PELIGRO ) ' else '' end if) as 'dlineacliente'
                from mlineaprocesocliente  
                where cestablecimiento = '" . $ccliente . "' and  sregistro = 'A';";
		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->clineaprocesocliente . '">' . $row->dlineacliente . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function getcbocontacprinc($cproveedor)
	{ // Listar Ensayos
		$sql = "select (dapepat+' '+dapemat+' '+dnombre )+' ('+dcargocontacto+')' as 'nombcontacto', dmail as 'mailcontacto', 
                        ccontacto, ccliente, cestablecimiento
                from mcontacto  
                where (sregistro='A') and
                    (ccliente = '" . $cproveedor . "') and      
                    exists (select 1 from MESTABLECIMIENTOCLIENTE e
                            where e.ccliente = mcontacto.ccliente
                            and e.cestablecimiento = mcontacto.cestablecimiento
                            and e.sregistro = 'A');";
		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->ccontacto . '">' . $row->nombcontacto . ' Email: ' . $row->mailcontacto . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function getcbotipoestable()
	{ // Listar Ensayos
		$sql = "select ctipoestablecimiento, dtipoestablecimiento 
                from mtipoestablecimiento where ccompania = '2'  
                order by dtipoestablecimiento asc ;";
		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->ctipoestablecimiento . '">' . $row->dtipoestablecimiento . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function getmontotipoestable($ctipoestable)
	{ // Listar Ensayos
		$sql = "select isnull(icosto,0.00) as 'icosto' from mtipoestablecimiento where ctipoestablecimiento = '" . $ctipoestable . "' ;";
		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {

			foreach ($query->result() as $row) {
				$montocosto = $row->icosto;
			}
			return $montocosto;
		}
		{
			return false;
		}
	}

	public function setregctrlprov($parametros)
	{  // Registrar evaluacion PT
		$this->db->trans_begin();

		$procedure = "call usp_oi_ctrlprov_setpcauditoriainspeccion(?,?,?,?,?,?,?,?,?,?,?,?);";
		$query = $this->db->query($procedure, $parametros);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
			return $query->result();
		}
	}

	public function getcbosistemaip()
	{ // Visualizar Sistema en CBO

		$procedure = "call usp_oi_ctrlprov_getcbosistemaip()";
		$query = $this->db->query($procedure);

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->IDNORMA . '">' . $row->DESCSISTEMA . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function getcborubroip($parametros)
	{ // Visualizar Rubro en CBO

		$procedure = "call usp_oi_ctrlprov_getcborubroip(?)";
		$query = $this->db->query($procedure, $parametros);

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->IDSUBNORMA . '">' . $row->DESCSUBNORMA . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function getcbochecklist($parametros)
	{ // Visualizar Checklist en CBO

		$procedure = "call usp_oi_ctrlprov_getcbochecklist(?,?,?)";
		$query = $this->db->query($procedure, $parametros);

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->IDCHECKLIST . '">' . $row->DESCCHECKLIST . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function getcboformula($parametros)
	{ // Visualizar Formula en CBO

		$procedure = "call usp_oi_ctrlprov_getcboformula(?)";
		$query = $this->db->query($procedure, $parametros);

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->IDFORMULA . '">' . $row->DESCFORMULA . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function getcbocriterio($parametros)
	{ // Visualizar Criterio en CBO

		$procedure = "call usp_oi_audi_getcbocriterio(?)";
		$query = $this->db->query($procedure, $parametros);

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->IDCRITERIO . '">' . $row->DESCCRITERIO . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function getcbomodinforme()
	{ // Listar Ensayos
		$sql = "select cmodeloinforme,dmodelo from mmodeloinforme where ccompania = '2' and carea = '01' and cservicio = '01';";
		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->cmodeloinforme . '">' . $row->dmodelo . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function getcboinspvalconf()
	{ // Listar Ensayos
		$sql = "select cvalor,dvalor from mvalor where sregistro = 'A' and stipovalor = 'N';";
		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->cvalor . '">' . $row->dvalor . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function getcboinspformula($cchecklist)
	{ // Listar Ensayos
		$sql = "select b.cformulaevaluacion,b.dformula 
                from mrchecklistformula a right outer join mformulaevaluacion b on b.cformulaevaluacion = a.cformulaevaluacion  
                where a.cchecklist = '" . $cchecklist . "';";
		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->cformulaevaluacion . '">' . $row->dformula . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function getcboinspcritresul()
	{ // Listar Ensayos
		$sql = "select ccriterioresultado, dcriterioresultado from mcriterioresultado ;";
		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->ccriterioresultado . '">' . $row->dcriterioresultado . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function setreginspeccion($parametros)
	{  // Registrar evaluacion PT
		$this->db->trans_begin();

		$procedure = "call usp_oi_ctrlprov_setpdauditoriainspeccion(?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
		$query = $this->db->query($procedure, $parametros);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
			return $query->result();
		}
	}

	public function getcbocierreTipo()
	{ // Listar Ensayos
		$sql = "select ctipo, dregistro, nvalor from ttabla 
                where ctabla = '09' and ncorrelativo <> 0 and IsNull(nvalor,9) <> 9 and spermitemodificar = 'N' order by dregistro ASC ;";
		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected="selected">::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->ctipo . '">' . $row->dregistro . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function cambioestadochecklist($parametros)
	{
		$procedure = "call usp_at_ctrlprov_update_estado_checklist(?,?,?,?,?,?)";
		$query = $this->db->query($procedure, $parametros);
		return ($query == true) ? 1 : false;
	}

	/**
	 * @param $cauditoria
	 * @param $fservicio
	 * @return array|mixed|object|null
	 */
	public function buscarInformeAnterior($cauditoria, $fservicio)
	{
		$this->db->select('*');
		$this->db->from('PDAUDITORIAINSPECCION');
		$this->db->where('CAUDITORIAINSPECCION', $cauditoria);
		$this->db->where('FSERVICIO <', $fservicio);
		$this->db->order_by('FSERVICIO', 'DESC');
		$this->db->limitAnyWhere(1);
		$query = $this->db->get();
		if (!$query) {
			return null;
		}
		return ($query->num_rows() > 0) ? $query->row() : null;
	}

	/**
	 * @param $busqueda
	 * @return array|array[]|object|object[]
	 */
	public function obtenerUbigeo($busqueda)
	{
		$query = $this->db->select('
			distinct(DIS.cdepartamento),
			DIS.CUBIGEO,
			DIS.DDEPARTAMENTO,
			DIS.DPROVINCIA,
			DIS.DDISTRITO
		')->from('tubigeo DIS')
			->join('tubigeo DEP', 'DIS.CDEPARTAMENTO = DEP.CDEPARTAMENTO')
			->join('tubigeo PRO', 'DIS.CPROVINCIA = PRO.CPROVINCIA')
			->group_start()
			->like('DIS.DDEPARTAMENTO', $busqueda, 'both', false)
			->or_like('DIS.DPROVINCIA', $busqueda, 'both', false)
			->or_like('DIS.DDISTRITO', $busqueda, 'both', false)
			->group_end()
			->where('DIS.CPAIS', 290)
			->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @param $CAUDITORIAINSPECCION
	 * @param $FSERVICIO
	 * @return array|mixed|object|null
	 */
	public function obtenerUltimoHistorico($CAUDITORIAINSPECCION, $FSERVICIO)
	{
		$this->db->select('*');
		$this->db->from('HRESULTADOAUDINSP');
		$this->db->where('CAUDITORIAINSPECCION', $CAUDITORIAINSPECCION);
		$this->db->where('FSERVICIO', $FSERVICIO);
		$this->db->where('SREGISTRO', 'A');
		$this->db->order_by('CHISTORICO', 'DESC');
		$query = $this->db->get();
		if (!$query) {
			return null;
		}
		return ($query->num_rows() > 0) ? $query->row() : null;
	}

}

?>
