<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mclientes extends CI_Model {
	function __construct() {
		parent:: __construct();	
		$this->load->library('session');
    }

    public function getbuscarclientes($parametros) { // Lista de consultas de Cliente
        $procedure = "call sp_appweb_mantgeneral_buscarcliente(?)";
        $query = $this->db-> query($procedure,$parametros);

        if ($query->num_rows() > 0) { 
            return $query->result();
        }{
            return False;
        }		   
    }
		
    public function setptcliente($parametros) { // Guardar Cliente
        $this->db->trans_begin();

        $procedure = "call sp_appweb_mantgeneral_setcliente(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $query = $this->db-> query($procedure,$parametros);

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else {
            $this->db->trans_commit();
            return $query->result(); 
        }   
    } 

    public function setcliente($parametros) { // Guardar Cliente
        $this->db->trans_begin();

        $procedure = "call sp_appweb_mantgeneral_setcliente(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $query = $this->db-> query($procedure,$parametros);

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else {
            $this->db->trans_commit();
            return $query->result(); 
        }   
    } 

    public function getbuscarestablecimiento($parametros) { // Lista de consultas de Cliente
        $procedure = "call sp_appweb_mantgeneral_buscarestablecimiento(?)";
        $query = $this->db-> query($procedure,$parametros);

        if ($query->num_rows() > 0) { 
            return $query->result();
        }{
            return False;
        }		   
    }
		
    public function mantgral_establecimiento($parametros) { // Guardar Cliente
        $this->db->trans_begin();

        $procedure = "call sp_appweb_oi_mantgeneral_establecimiento(?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $query = $this->db-> query($procedure,$parametros);

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else {
            $this->db->trans_commit();
            return $query->result(); 
        }   
    } 

    public function getbuscarproveedor($parametros) { // Lista de consultas de Cliente
        $procedure = "call sp_appweb_mantgeneral_buscarprovxcliente(?)";
        $query = $this->db-> query($procedure,$parametros);

        if ($query->num_rows() > 0) { 
            return $query->result();
        }{
            return False;
        }		   
    }

    public function getaddproveedor($parametros) { // Lista de consultas de Cliente
        $procedure = "call sp_appweb_mantgeneral_buscarproveedor(?)";
        $query = $this->db-> query($procedure,$parametros);

        if ($query->num_rows() > 0) { 
            return $query->result();
        }{
            return False;
        }		   
    }

    public function insertproveedorcliente($parametros) { // Lista de consultas de Cliente
        $procedure = "call sp_appweb_mantgeneral_setproveedor(?,?,?)";
        $query =$this->db-> query($procedure,$parametros);
        
        if ($query) {
            return true;
        } else{
            return false;
        }  
    }
    
    public function setptclientexproveedor($parametros) { // Guardar Cliente
        $this->db->trans_begin();

        $procedure = "call sp_appweb_mantgeneral_setclientexproveedor(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $query = $this->db-> query($procedure,$parametros);

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else {
            $this->db->trans_commit();
            return $query->result(); 
        }   
    } 

    public function setproveedorxmaquilador($parametros) { // Guardar Cliente
        $this->db->trans_begin();

        $procedure = "call sp_appweb_mantgeneral_setclientexproveedor(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $query = $this->db-> query($procedure,$parametros);

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else {
            $this->db->trans_commit();
            return $query->result(); 
        }   
    }  


    public function getsizeempresa() { // Guardar Cliente
        $sql = "select ctipo,dregistro,XCIA from ttabla where ctabla = '33' and SREGISTRO = 'A' AND CTIPO <>'396' ORDER BY NCORRELATIVO DESC;";
        $query  = $this->db->query($sql);

        if ($query->num_rows() > 0) {

            $listas = '<option value="" selected="selected">::Elegir</option>';
            
            foreach ($query->result() as $row)
            {
                $listas .= '<option value="'.$row->ctipo.'">'.$row->dregistro.'</option>';  
            }
            return $listas;
        }{
            return false;
        }		
    }  

    public function getgrupoempresarial() { // Guardar Cliente
        $sql = "select CGRUPOEMPRESARIAL,DGRUPOEMPRESARIAL from MGRUPOEMPRESARIAL WHERE SREGISTRO = 'A' ORDER BY DGRUPOEMPRESARIAL ASC";
        $query  = $this->db->query($sql);

        if ($query->num_rows() > 0) {

            $listas = '<option value="" selected="selected">::Elegir</option>';
            
            foreach ($query->result() as $row)
            {
                $listas .= '<option value="'.$row->CGRUPOEMPRESARIAL.'">'.$row->DGRUPOEMPRESARIAL.'</option>';  
            }
            return $listas;
        }{
            return false;
        }		
    } 

    public function getservicios() { // Guardar Cliente
        $sql = "select * from MSERVICIO  WHERE CCOMPANIA = '2' AND CAREA = '01'";
        $query  = $this->db->query($sql);

        if ($query->num_rows() > 0) {

            $listas = '<option value="" selected="selected">::Elegir</option>';
            
            foreach ($query->result() as $row)
            {
                $listas .= '<option value="'.$row->CSERVICIO.'">'.$row->DSERVICIO.'</option>';  
            }
            return $listas;
        }{
            return false;
        }		
    } 
     public function getsubservicios($id) { // Guardar Cliente
        $sql = "select * from MSUBSERVICIO  WHERE CCOMPANIA = '2' AND CAREA = '01' and CSERVICIO = '".$id."'";
        $query  = $this->db->query($sql);

        if ($query->num_rows() > 0) {

            $listas = '<option value="" selected="selected">::Elegir</option>';
            
            foreach ($query->result() as $row)
            {
                $listas .= '<option value="'.$row->CSUBSERVICIO.'">'.$row->DSUBSERVICIO.'</option>';  
            }
            return $listas;
        }{
            return false;
        }		
    }
    
    public function getbuscarcontacto($id) { // buscar contacto
        $this->db->trans_begin();
        $sql = "SELECT * FROM MCONTACTO WHERE CCLIENTE = '".$id."' and cestablecimiento IS NULL or cestablecimiento = '' ORDER BY CCONTACTO DESC";
        $query  = $this->db->query($sql);

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else {
            $this->db->trans_commit();
            return $query->result(); 
        }   	
    }

    public function createcontacto($parametros) { // Guardar Cliente
        $this->db->trans_begin();

        $procedure = "call sp_appweb_pt_mantgeneral_contactoxcliente(?,?,?,?,?,?,?,?,?,?,?)";
        $query = $this->db-> query($procedure,$parametros);
        if ($query) {
            return true;
        } else{
            return false;
        }
    }  

    public function getbuscarcontactoxestable($id,$id_estable) { // buscar contacto
        $this->db->trans_begin();
        $sql = "SELECT * FROM MCONTACTO WHERE CCLIENTE = '".$id."' and CESTABLECIMIENTO ='".$id_estable."'  ORDER BY CCONTACTO DESC";
        $query  = $this->db->query($sql);

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else {
            $this->db->trans_commit();
            return $query->result(); 
        }   	
    }

    public function getbuscarlineaxestablec($id,$id_estable) { // buscar contacto
        $this->db->trans_begin();
        $sql = "select * from mlineaprocesocliente where ccliente = '".$id."' and cestablecimiento = '".$id_estable."' and sregistro = 'A'  ORDER BY CLINEAPROCESOCLIENTE DESC";
        $query  = $this->db->query($sql);

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else {
            $this->db->trans_commit();
            return $query->result(); 
        }   	
    }

    public function createLineaProc($parametros) { // Guardar Cliente
        $this->db->trans_begin();

        $procedure = "call sp_appweb_mantgeneral_setlineaxestablecimiento(?,?,?,?,?)";
        $query = $this->db-> query($procedure,$parametros);
        if ($query) {
            return true;
        } else{
            return false;
        }
    }
    
    public function getResponsablesOI(){
        $this->db->trans_begin();

        $procedure = "call sp_appweb_oi_getresponsables()";
        $query  = $this->db->query($procedure);

        if ($query->num_rows() > 0) {

            $listas = '<option value="" selected="selected">::Elegir</option>';
            
            foreach ($query->result() as $row)
            {
                $listas .= '<option value="'.$row->cusuario.'">'.$row->datosrazonsocial.'</option>';  
            }
            return $listas;
        }{
            return false;
        }		
    }

    public function createServicio($parametros) { // Guardar Cliente
        $this->db->trans_begin();

        $procedure = "call sp_appweb_oi_create_servicio_pcte(?,?,?,?,?,?)";
        $query = $this->db-> query($procedure,$parametros);
        if ($query) {
            return true;
        } else{
            return false;
        }
    }

    public function getserviciosxcliente($id){
        $this->db->trans_begin();
        $sql = "call sp_appweb_oi_getservicio_pcpte(?)";
        $query  = $this->db->query($sql,$id);

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else {
            $this->db->trans_commit();
            return $query->result(); 
        }
    }
    
    public function createarea($parametros) { // Guardar Cliente
        $this->db->trans_begin();

        $procedure = "call sp_appweb_oi_setareaxcliente(?,?,?,?,?,?)";
        $query = $this->db-> query($procedure,$parametros);
        if ($query) {
            return true;
        } else{
            return false;
        }
    }

    public function getareacliente($id){
        $sql = "call sp_appweb_oi_getareaxcliente(?)";
        $query  = $this->db->query($sql,$id);
        
        if ($query->num_rows() > 0) { 
            return $query->result();
        }{
            return FALSE;
        }
    }
}
?>