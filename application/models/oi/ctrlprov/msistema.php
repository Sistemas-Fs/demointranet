<?php

/**
 * Class msistema
 */
class msistema extends CI_Model
{

	/**
	 * msistema constructor.
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * @param $ccompania
	 * @param $busqueda
	 * @return array|array[]|object|object[]
	 */
	public function lista($ccompania, $busqueda)
	{
		$this->db->select('
			msistema.csistema,   
			msistema.dsistema,   
			msistema.carea,   
			msistema.cservicio,   
			msistema.cusuariocrea,   
			msistema.tcreacion,   
			msistema.cusuariomodifica,   
			msistema.tmodificacion,   
			msistema.sregistro,   
			mservicio.dservicio,   
			marea.darea
		');
		$this->db->from('msistema');
		$this->db->join('mservicio', 'msistema.CSERVICIO = mservicio.CSERVICIO AND msistema.ccompania = mservicio.ccompania', 'inner');
		$this->db->join('marea', 'msistema.CAREA = marea.CAREA AND msistema.ccompania = marea.ccompania', 'inner');
		$this->db->where('msistema.ccompania', $ccompania);
		$this->db->group_start();
		$this->db->like('msistema.dsistema', $busqueda, 'both', false);
		$this->db->or_like('mservicio.dservicio', $busqueda, 'both', false);
		$this->db->group_end();
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @param $id
	 * @return array|mixed|object|null
	 */
	public function buscar($id)
	{
		$this->db->select('*');
		$this->db->from('msistema');
		$this->db->where('CSISTEMA', $id);
		$query = $this->db->get();
		if (!$query) {
			return null;
		}
		return ($query->num_rows() > 0) ? $query->row() : null;
	}

	/**
	 * @return array|array[]|object|object[]
	 */
	public function lista_areas()
	{
		$this->db->select('*');
		$this->db->from('marea');
		$this->db->where('SREGISTRO', 'A');
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @return array|array[]|object|object[]
	 */
	public function lista_servicios()
	{
		$this->db->select('*');
		$this->db->from('mservicio');
		$this->db->where('SREGISTRO', 'A');
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @param $CCCOMPANIA
	 * @return string
	 */
	public function obtenerNuevoID($CCCOMPANIA)
	{
		$this->db->select('*');
		$this->db->from('msistema');
		$this->db->where('CCOMPANIA', $CCCOMPANIA);
		$this->db->order_by('CSISTEMA', 'DESC');
		$query = $this->db->get();
		if (!$query) {
			return '001';
		}
		return ($query->num_rows() > 0) ? str_pad($query->row()->CSISTEMA + 1, 2, '0', STR_PAD_LEFT) : '001';
	}

	/**
	 * @param $CSISTEMA
	 * @param $DSISTEMA
	 * @param $CCOMPANIA
	 * @param $CAREA
	 * @param $CSERVICIO
	 * @param $CUSUARIO
	 * @param $SREGISTRO
	 * @return object
	 * @throws Exception
	 */
	public function guardar($CSISTEMA, $DSISTEMA, $CCOMPANIA, $CAREA, $CSERVICIO, $CUSUARIO, $SREGISTRO)
	{
		$data = [
			'CSISTEMA' => $CSISTEMA,
			'DSISTEMA' => $DSISTEMA,
			'CCOMPANIA' => $CCOMPANIA,
			'CAREA' => $CAREA,
			'CSERVICIO' => $CSERVICIO,
			'CUSUARIOCREA' => $CUSUARIO,
			'TCREACION' => date('Y-m-d H:i:s'),
			'CUSUARIOMODIFICA' => $CUSUARIO,
			'TMODIFICACION' => date('Y-m-d H:i:s'),
			'SREGISTRO' => $SREGISTRO,
		];
		$res = (empty($CSISTEMA)) ? $this->crear($data) : $this->actualizar($CSISTEMA, $data);
		if (!$res) {
			throw new Exception('Error al intentar guardar el sistema.');
		}
		return (Object)$data;
	}

	/**
	 * Crear
	 * @param array $datos
	 * @return bool
	 * @throws Exception
	 */
	public function crear(array &$datos): bool
	{
		$datos['CSISTEMA'] = $this->obtenerNuevoID($datos['CCOMPANIA']);
		$datos['CUSUARIOMODIFICA'] = null;
		$datos['TMODIFICACION'] = null;
		$res = $this->db->insert('msistema', $datos);
		if (!$res) {
			throw new Exception('El sistema no pudo ser creado correctamente.');
		}
		return $res;
	}

	/**
	 * Editar
	 * @param string $id
	 * @param array $datos
	 * @return bool
	 * @throws Exception
	 */
	public function actualizar(string $id, array &$datos): bool
	{
		if (empty($id)) {
			throw new Exception('El AR no es valido para actualizar.');
		}
		$datos['TMODIFICACION'] = date('Y-m-d H:i:s');
		if (isset($datos['TCREACION'])) unset($datos['TCREACION']);
		if (isset($datos['CUSUARIOCREA'])) unset($datos['CUSUARIOCREA']);
		$res = $this->db->update('msistema', $datos, ['CSISTEMA' => $id]);
		if (!$res) {
			throw new Exception('El sistema no pudo ser actualizado correctamente.');
		}
		return $res;
	}

	/**
	 * @param $CSISTEMA
	 * @return false|mixed|string
	 * @throws Exception
	 */
	public function eliminar($CSISTEMA)
	{
		$norma = $this->db->from('mnorma')
			->where('CSISTEMA', $CSISTEMA)
			->get();
		if ($norma && $norma->num_rows() > 0) {
			throw new Exception('No puede ser Eliminado se encuentra amarrado a una norma.');
		}
		return $this->db->delete('msistema', ['CSISTEMA' => $CSISTEMA]);
	}

}
