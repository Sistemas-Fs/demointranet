<?php

/**
 * Class mvalores
 */
class mvalores extends CI_Model
{

	/**
	 * mvalores constructor.
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * @param $busqueda
	 * @return array|array[]|object|object[]
	 */
	public function lista($busqueda)
	{
		$this->db->select('mvalor.*');
		$this->db->from("mvalor");
		$this->db->like('mvalor.dvalor', $busqueda, 'both', false);
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @param $busqueda
	 * @return array|array[]|object|object[]
	 */
	public function listaDetalle($cvalor)
	{
		$this->db->select('mdetallevalor.*');
		$this->db->from("mdetallevalor");
		$this->db->where('mdetallevalor.cvalor', $cvalor);
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

}
