<?php

/**
 * Class minspctrolprovacc
 */
class minspctrolprovacc extends CI_Model
{


	/**
	 * minspctrolprovacc constructor.
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * @param $parametros
	 * @return array
	 */
	public function lista($parametros)
	{ // Recupera Listado de Propuestas
		$procedure = "call usp_at_ctrlprov_getlistaaccioncorrectiva(?,?)";
		$query = $this->db->query($procedure, $parametros);
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return [];
	}

	/**
	 * @param $parametros
	 * @return null
	 */
	public function buscar($parametros)
	{ // Recupera Listado de Propuestas
		$procedure = "call usp_at_ctrlprov_getbuscaaccioncorrectiva(?,?,?,?)";
		$query = $this->db->query($procedure, $parametros);
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		return null;
	}

}
