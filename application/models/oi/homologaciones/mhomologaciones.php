<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mhomologaciones extends CI_Model
{
	function __construct()
	{
		parent::__construct();
//		$this->load->library('session');
	}

	public function getClientes()
	{
		$procedure = "call sp_appweb_oi_listar_clientes()";
		$query = $this->db->query($procedure);

		if ($query->num_rows() > 0) {
			$listas = '<option value="" >::Elegir</option>';
			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->CODIGO . '">' . $row->CLIENTE . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function getEstadoExp()
	{
		$procedure = "select ctipo as 'ID', upper(dregistro) as 'VALUE' from ttabla where ctabla = '28'";
		$query = $this->db->query($procedure);

		if ($query->num_rows() > 0) {
			$listas = '<option value="%" selected>::Elegir</option>';
			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->ID . '">' . $row->VALUE . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function getArea()
	{
		$procedure = "call sp_appweb_oi_listar_areaxcliente()";
		$query = $this->db->query($procedure);

		if ($query->num_rows() > 0) {
			$listas = '<option value="" selected>::Elegir</option>';
			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->careacliente . '">' . $row->dareacliente . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function getAreaxCliente($ccliente)
	{
		$query = $this->db->select('*')
			->from('MAREACLIENTE')
			->where('CCLIENTE', $ccliente)
			->get();

		if ($query->num_rows() > 0) {
			$listas = '<option value="" selected>::Elegir</option>';
			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->CAREACLIENTE . '">' . $row->DAREACLIENTE . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function getRequisitos()
	{
		$procedure = "call sp_appweb_oi_listar_areaxcliente()";
		$query = $this->db->query($procedure);

		if ($query->num_rows() > 0) {
			$listas = '<option value="" selected>::Elegir</option>';
			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->careacliente . '">' . $row->dareacliente . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function getbuscarexpediente($parametros)
	{
		$procedure = "call sp_appweb_oi_buscar_expediente(?,?,?,?)";
		$query = $this->db->query($procedure, $parametros);

		if ($query->num_rows() > 0) {
			return $query->result();
		}
		{
			return false;
		}
	}

	public function getbuscarproductoxespediente($parametros)
	{
		$procedure = "call sp_appweb_oi_buscar_productoxexpediente(?)";
		$query = $this->db->query($procedure, $parametros);

		if ($query->num_rows() > 0) {
			$items = $query->result();
			foreach ($items as $key => $value) {
				$marca = str_replace('"', '', $value->MARCA);
				$producto = str_replace('"', '', $value->PRODUCTO);
				$items[$key]->MARCA = $marca;
				$items[$key]->PRODUCTO = $producto;
			}
			return $items;
		}
		{
			return false;
		}
	}

	public function getClienteDetallado($parametros)
	{
		$procedure = "call sp_appweb_oi_clientes_detallado(?)";
		$query = $this->db->query($procedure, $parametros);

		if ($query->num_rows() > 0) {
			return $query->result();
		}
		{
			return false;
		}
	}

	/**
	 * @param $cevaluacion
	 * @return array|mixed|object|null
	 */
	public function buscarExpdiente($cevaluacion)
	{
		$query = $this->db->select('
			eval.*,
			cli.DRAZONSOCIAL AS DCLIENTEPRINCIPAL,
			cpro.DRAZONSOCIAL AS DPROVEEDORCLIENTE,
		')->from('PEVALUACIONPRODUCTO eval')
			->join('MCLIENTE cli', 'eval.CCLIENTEPRINCIPAL = cli.CCLIENTE', 'INNER')
			->join('MCLIENTE cpro', 'eval.CPROVEEDORCLIENTE = cpro.CCLIENTE', 'INNER')
			->where('CEVALUACIONPRODUCTO', $cevaluacion)
			->get();
		if (!$query) {
			return null;
		}
		return ($query->num_rows() > 0) ? $query->row() : null;
	}

	public function getContactoProveedor($parametros)
	{
		// $procedure = "call sp_appweb_oi_buscar_contactoxproveedor(?)";
		$procedure = "SELECT distinct(contact.ccontacto) as 'IDCONTACTO', contact.DNOMBRE+' '+contact.DAPEPAT AS'NOMBRE', contact.DMAIL AS 'EMAIL' FROM MCONTACTO contact INNER JOIN PEVALUACIONPRODUCTO prod on contact.ccliente = prod.cproveedorcliente  WHERE prod.cproveedorcliente LIKE '%" . $parametros . "'";
		$query = $this->db->query($procedure, $parametros);

		if ($query->num_rows() > 0) {
			$listas = '<option value="%" >::Elegir</option>';
			foreach ($query->result() as $row) {
				$listas .= '<option email="' . $row->EMAIL . '" value="' . $row->IDCONTACTO . '">' . $row->NOMBRE . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function getProveedorxCliente($parametros)
	{
		$procedure = "call sp_appweb_oi_buscar_proveedorxcliente(?)";
		// $procedure = "select cliente.drazonsocial as 'NOMBRE',cliente.ccliente as 'ID' FROM MCLIENTE cliente
		// INNER JOIN PEVALUACIONPRODUCTO prod on cliente.ccliente = prod.cproveedorcliente where prod.cevaluacionproducto = '".$parametros."'";
		$query = $this->db->query($procedure, $parametros);

		if ($query->num_rows() > 0) {

			$listas = '<option value="%" >::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->ID . '">' . $row->NOMBRE . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function getbuscarequisitoxproducto($parametros)
	{
		$procedure = "call sp_appweb_oi_requisitos_producto(?,?)";

		$query = $this->db->query($procedure, $parametros);

		if ($query->num_rows() > 0) {
			return $query->result();
		}
		{
			return false;
		}
	}

	public function getbuscarobservacionxproducto($parametros)
	{
		$procedure = "call sp_appweb_oi_observacion_producto(?,?)";

		$query = $this->db->query($procedure, $parametros);

		if ($query->num_rows() > 0) {
			return $query->result();
		}
		{
			return false;
		}
	}

	public function insert($parametros)
	{
		$procedure = "call sp_appweb_oi_insert_prodxevaluar(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
		$query = $this->db->query($procedure, $parametros);
		return ($query == true) ? 1 : false;
	}

	public function update($parametros)
	{
		$procedure = "call sp_appweb_oi_update_producto(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
		$query = $this->db->query($procedure, $parametros);
		return ($query == true) ? 1 : false;
	}

	public function delete($parametros)
	{
		// DESACTIVARA EL PRODUCTO PARA NO SER VISUALIZAR
		$procedure = "call sp_appweb_oi_delete_producto(?,?)";
		$query = $this->db->query($procedure, $parametros);
		return ($query == true) ? 1 : false;
	}

	public function insertarObsRequisito($parametros)
	{
		$procedure = "call sp_appweb_oi_insert_observacion_requisito_prod(?,?,?,?,?,?,?,?,?,?,?);";
		$query = $this->db->query($procedure, $parametros);
		return ($query == true) ? 1 : false;

	}

	public function updateProductoProveedor($parametros)
	{
		$procedure = "call sp_appweb_oi_insertar_productoproveedor(?,?,?,?,?,?,?);";
		$query = $this->db->query($procedure, $parametros);
		return ($query == true) ? 1 : false;
	}

	public function getTipoRequisito($parametros)
	{
		$procedure = "call sp_appweb_oi_listar_tipo_req_producto(?)";
		$query = $this->db->query($procedure, $parametros);

		if ($query->num_rows() > 0) {

			$listas = '<option value="" selected>::Elegir</option>';

			foreach ($query->result() as $row) {
				$listas .= '<option value="' . $row->ID . '">' . $row->DOCUMENTO . '</option>';
			}
			return $listas;
		}
		{
			return false;
		}
	}

	public function insertarRequisitoProducto($parametros)
	{
		$procedure = "call sp_appweb_oi_registrar_req_prod(?,?,?,?,?,?,?,?,?);";
		$query = $this->db->query($procedure, $parametros);
		return ($query == true) ? 1 : false;
	}

	public function deleteRequisitoProd($parametros)
	{
		// DESACTIVARA EL REQUISITO PARA NO SER VISUALIZAR
		$procedure = "call sp_appweb_oi_delete_requisito_producto(?,?,?)";
		$query = $this->db->query($procedure, $parametros);
		return ($query == true) ? 1 : false;
	}

	public function updateRequisitoProducto($parametros)
	{
		$procedure = "call sp_appweb_oi_update_req_prod(?,?,?,?,?,?,?,?,?);";
		$query = $this->db->query($procedure, $parametros);
		return ($query == true) ? 1 : false;
	}

	/**
	 * Devuelve las entidades reguladoras
	 * @param $search
	 * @return array
	 */
	public function listaGrupoEmpresarial($search = null): array
	{
		$this->db->select('
			pcpte.cgrupoempresarial as id,
			mgrupoempresarial.dgrupoempresarial as text
		');
		$this->db->from('pcpte');
		$this->db->join('pdpte', 'pcpte.cinternopte = pdpte.cinternopte AND pcpte.ccompania = pdpte.ccompania', 'inner');
		$this->db->join('mgrupoempresarial', 'mgrupoempresarial.cgrupoempresarial = pcpte.cgrupoempresarial', 'inner');
		$this->db->where('pcpte.ccompania', '2');
		$this->db->where('pdpte.CSERVICIO', '06');
		$this->db->where('pcpte.sregistro', 'A');
		if (!empty($search)) {
			$this->db->like('mgrupoempresarial.dgrupoempresarial', $search);
		}
		$this->db->order_by('mgrupoempresarial.dgrupoempresarial', 'ASC');
		$this->db->limitAnyWhere(LIMITE_AUTOCOMPLETADO);
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @param $cgrupoemp
	 * @return array|array[]|object|object[]
	 */
	public function listaClientes($cgrupoemp)
	{
		$this->db->select('
			mcliente.ccliente as CODIGO, 
			mcliente.drazonsocial AS CLIENTE
		');
		$this->db->from('pdpte');
		$this->db->join('pcpte', 'pcpte.cinternopte = pdpte.cinternopte', 'inner');
		$this->db->join('mgrupoempresarial', 'pcpte.cgrupoempresarial = mgrupoempresarial.cgrupoempresarial', 'inner');
		$this->db->join('mcliente', 'mcliente.cgrupoempresarial = mgrupoempresarial.cgrupoempresarial', 'inner');
		$this->db->where('mcliente.CGRUPOEMPRESARIAL', $cgrupoemp);
		$this->db->where('pcpte.ccompania', 2);
		$this->db->where('pcpte.carea', '01');
		$this->db->where('pdpte.cservicio', '06');
		$this->db->where('pdpte.sregistro', 'A');
		$this->db->order_by('mcliente.drazonsocial', 'ASC');
		$this->db->limitAnyWhere(LIMITE_AUTOCOMPLETADO);
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @return array|array[]|object|object[]
	 */
	public function listaProveedores($ccliente)
	{
		$this->db->select('
			mcliente.ccliente as CODIGO, 
			mcliente.drazonsocial AS CLIENTE
		');
		$this->db->from('mrclienteproveedor');
		$this->db->join('mcliente', 'mcliente.ccliente = mrclienteproveedor.cproveedorcliente', 'inner');
		$this->db->where('mrclienteproveedor.sregistro', 'A');
		$this->db->where('mrclienteproveedor.CCLIENTE', $ccliente);
		$this->db->order_by('mcliente.drazonsocial', 'ASC');
		$this->db->limitAnyWhere(LIMITE_AUTOCOMPLETADO);
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	public function guardarEval($cevaluar, $ccliente, $carea, $sregistro, $cproveedor, $ccontacto1, $ccontacto2, $nproveedor, $cusuario)
	{
		$data = [
			'CEVALUACIONPRODUCTO' => $cevaluar,
			'CCLIENTEPRINCIPAL' => $ccliente,
			'CPROVEEDORCLIENTE' => $cproveedor,
			'CCONTACTOPROVEEDOR1' => $ccontacto1,
			'CCONTACTOPROVEEDOR2' => $ccontacto2,
			'CAREACLIENTE' => $carea,
			'SNUEVOPROVEEDOR' => $nproveedor,
			'SMUESTRA' => 'N',
			'SFICHATECNICA' => 'N',
			'SREGISTROEVALUACION' => 'S',
			'SHOJASEGURIDAD' => 'N',
			'SLICENCIAFUNCIONAMIENTO' => 'N',
			'SINSPECHIGIENICOSANITARIO' => 'N',
			'SOTROS' => 'N',
			'SCIERRESERVICIO' => 'A',
			'CUSUARIOCREA' => $cusuario,
			'CUSUARIOMODIFICA' => $cusuario,
			'SREGISTRO' => $sregistro,
			'CCONTACTOAREA' => null,
		];

		return (empty($cevaluar)) ? $this->createEvaluar($data) : $this->updateEvaluar($cevaluar, $data);
	}

	public function createEvaluar(&$data)
	{
		$data['CEVALUACIONPRODUCTO'] = $this->obtenerID();
		$data['TCREACION'] = date('Y-m-d H:i:s');
		$data['FINICIOSERVICIO'] = date('Y-m-d');
		$data['FCIERRESERVICIO'] = null;
		$data['CUSUARIOMODIFICA'] = null;
		$pdpte = $this->getPDPTE($data['CCLIENTEPRINCIPAL']);
		if (empty($pdpte)) {
			throw new Exception('Error al encontrar el pdpte para la evaluación');
		}
		$data['CINTERNOPTE'] = $pdpte->cinternopte;
		$data['NORDEN'] = $pdpte->norden;
		$data['NDETALLE'] = 1;
		$resp = $this->db->insert('PEVALUACIONPRODUCTO', $data);
		return [
			'resp' => $resp,
			'id' => $data['CEVALUACIONPRODUCTO'],
		];
	}

	public function updateEvaluar($id, &$data)
	{
		$data['TMODIFICACION'] = date('Y-m-d H:i:s');
		$id = $data['CEVALUACIONPRODUCTO'];
		if (isset($data['CEVALUACIONPRODUCTO'])) unset($data['CEVALUACIONPRODUCTO']);
		if (isset($data['CAREACLIENTE'])) unset($data['CAREACLIENTE']);
		if (isset($data['SMUESTRA'])) unset($data['SMUESTRA']);
		if (isset($data['SFICHATECNICA'])) unset($data['SFICHATECNICA']);
		if (isset($data['SREGISTROEVALUACION'])) unset($data['SREGISTROEVALUACION']);
		if (isset($data['SHOJASEGURIDAD'])) unset($data['SHOJASEGURIDAD']);
		if (isset($data['SLICENCIAFUNCIONAMIENTO'])) unset($data['SLICENCIAFUNCIONAMIENTO']);
		if (isset($data['SINSPECHIGIENICOSANITARIO'])) unset($data['SINSPECHIGIENICOSANITARIO']);
		if (isset($data['SOTROS'])) unset($data['SOTROS']);
		if (isset($data['SCIERRESERVICIO'])) unset($data['SCIERRESERVICIO']);
		if (isset($data['CUSUARIOCREA'])) unset($data['CUSUARIOCREA']);
		if (isset($data['CCONTACTOAREA'])) unset($data['CCONTACTOAREA']);
		$resp = $this->db->update('PEVALUACIONPRODUCTO', $data, ['CEVALUACIONPRODUCTO' => $id]);
		return [
			'resp' => $resp,
			'id' => $id,
		];
	}

	public function obtenerID()
	{
		return $this->db->select("
		right(('00000000'+cast((ISNULL(max(cast(CEVALUACIONPRODUCTO as integer)),0) + 1) as char(8))),6) as id
		", false)
			->from('PEVALUACIONPRODUCTO')
			->get()
			->row()->id;
	}

	public function getPDPTE($ccliente)
	{
		$query = $this->db->select('b.cinternopte, c.norden')
			->from('mcliente a')
			->join('pcpte b', 'b.cgrupoempresarial = a.cgrupoempresarial', 'inner')
			->join('pdpte c', 'c.cinternopte = b.cinternopte', 'inner')
			->where('a.ccliente', $ccliente)
			->where('c.ccompania', '2')
			->where('c.cservicio', '06')
			->get();
		if (!$query) {
			return null;
		}
		return ($query->num_rows() > 0) ? $query->row() : null;
	}

	public function pdfBuscarProdEval($idExpediente, $idProducto)
	{
		$query = $this->db->select("
			prod.CPRODUCTOFSEVALUAR AS'IDPROD' ,prod.DPRODUCTO AS 'PRODUCTO',prod.DMARCA AS 'MARCA',prod.DREGISTROSANITARIO AS 'SANITARIO', prod.FEMISION AS 'EMISION',prod.FVENCIMIENTO AS 'VENCE', 
			estado.DREGISTRO as 'ESTADO',estado.CTIPO AS 'IDESTADO', prod.FEVALUACION AS 'FINICIO', prod.FTERMINOPROCESO AS 'FFIN', prod.DENVASEPRIMARIO AS 'ENVASEPRIM', prod.DENVASESECUNDARIO AS 'ENVASESECU', area.dareacliente as 'TIPOREQUISITO',area.CAREACLIENTE AS 'IDTIPREQUI',prod.NMONTOFACTURA AS 'MONTO',
			prod.FFACTURA AS 'FCOBRO', prod.FEVALUACION AS 'RECEPDOC', prod.NTIERESPROV AS 'TIEMRESPROV', prod.FPRIMEREVAL AS 'PRIMEVAL', prod.NTIERESFSC AS 'TIEMRESPFSC', prod.FLEVANTAOBSERVACION AS 'LEVAOBSERV', prod.FTERMINOPROCESO AS 'FINPROCESO',prod.DORIGENPRODUCTO as 'ORIGEN',prod.DCONDICIONALMACENAJE AS 'CONDICIONALM',
			prod.DALMACEN AS 'ALMACEN', prod.DALMACENDIRECCION AS 'DIRECCIONALM', prod.DFABRICANTE as 'FABRICANTE', prod.DFABRICANTEDIRECCION as 'FABRICADIREC', prod.ZCTIPOMARCA as 'TIPOMARCA',prod.DVIDAUTIL AS 'VIDA_UTIL',prod.SFACTURAPAGAR AS 'PAGAR'  ,prod.ZCTIPOPRODUCTOEVALUAR AS 'IDTIPOREQU',
			prod.dplantaorigen as 'DPLANTAORIGEN', prod.dpaisorigen as 'DPAISORIGEN', prod.dcodigosap as 'DCODIGOSAP'
		")->from("PPRODUCTOEVALUAR prod")
			->join('ttabla estado', 'estado.ctipo = prod.ZCESTADOEVALUACION', 'inner')
			->join('MAREACLIENTE area', 'area.careacliente = prod.ZCTIPOPRODUCTOEVALUAR', 'inner')
			->where('prod.cevaluacionproducto', $idExpediente)
			->where('prod.CPRODUCTOFSEVALUAR', $idProducto)
			->get();
		if (!$query) {
			return null;
		}
		return ($query->num_rows() > 0) ? $query->row() : null;
	}

	/**
	 * @param $idExpediente
	 * @param $idProducto
	 * @return array|array[]|object|object[]
	 */
	public function pdfBuscarProdRequisitos($idExpediente, $idProducto)
	{

		$this->db->select("
			req.*,req.crequisitopdto as 'IDREQU',
			req.DDOCUMENTO AS 'REQUISITO',
			eval.SCUMPLIMIENTO AS 'CONFORMIDAD',
			eval.DCUMPLIMIENTO AS 'DESCRIPCION',
			eval.DUBICACIONFILESERVER AS 'RUTA', 
			eval.FCUMPLIMIENTO AS 'FECHA', 
			eval.DNOTA AS 'NOTA', 
			req.STIPOCAMPO AS 'TIPO', 
			prod.ZCTIPOPRODUCTOEVALUAR AS 'IDREQUISITO',
    		prod.CEVALUACIONPRODUCTO AS 'IDEXP', 
    		prod.CPRODUCTOFSEVALUAR as 'IDPROD',
    		req.NORDEN,
    		req.STITULO
		");
		$this->db->from("PREQUISITOPDTOEVALUAR eval");
		$this->db->join("MREQUISITOEVALPRODUCTO req", "req.CREQUISITOPDTO = eval.CREQUISITOPDTO", "inner");
		$this->db->join("PPRODUCTOEVALUAR prod", "prod.CPRODUCTOFSEVALUAR = eval.CPRODUCTOFSEVALUAR AND prod.CEVALUACIONPRODUCTO = eval.CEVALUACIONPRODUCTO", "inner");
		$this->db->where('prod.CEVALUACIONPRODUCTO', $idExpediente);
		$this->db->where('prod.CPRODUCTOFSEVALUAR', $idProducto);
		$this->db->where('eval.sregistro', 'A');
		$this->db->order_by('req.crequisitopdto', 'ASC');
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * @param $idExpediente
	 * @param $idProducto
	 * @return array|array[]|object|object[]
	 */
	public function pdfBuscarProdRequisitoObs($idExpediente, $idProducto)
	{

		$query = $this->db->select("
			DOBSERVACIONTEXT AS 'OBSERVACION',
			DACUERDOSTEXT AS 'ACUERDOS',
			FEVALUACION AS 'RECEPDOC', 
			NTIERESPROV AS 'TIEMRESPROV',
			FPRIMEREVAL AS 'PRIMEVAL', 
			NTIERESFSC AS 'TIEMRESPFSC', 
			FLEVANTAOBSERVACION AS 'LEVAOBSERV',
			FTERMINOPROCESO AS 'FINPROCESO',
			NTIEDURPROC AS 'TIMEDURACION'
		")->from("PPRODUCTOEVALUAR")
			->where('CEVALUACIONPRODUCTO', $idExpediente)
			->where('CPRODUCTOFSEVALUAR', $idProducto)
			->get();
		if (!$query) {
			return null;
		}
		return ($query->num_rows() > 0) ? $query->row() : null;
	}

	/**
	 * @param $idExpediente
	 * @return array|array[]|object|object[]
	 */
	public function pdfBuscarProductos($idExpediente)
	{

		$this->db->select("*");
		$this->db->from("PPRODUCTOEVALUAR");
		$this->db->where('CEVALUACIONPRODUCTO', $idExpediente);
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

}

?>
