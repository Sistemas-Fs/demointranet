<?php


class mreqhomologacion extends CI_Model
{

	/**
	 * mreqhomologacion constructor.
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Lista de area
	 * @return array|array[]|object|object[]
	 */
	public function listaArea()
	{
		$this->db->select('*');
		$this->db->from('ttabla');
		$this->db->where('ctabla', '76');
		$this->db->where('sregistro', 'A');
		$this->db->order_by('DREGISTRO', 'ASC');
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * Lista de tipos por area
	 * @return array|array[]|object|object[]
	 */
	public function listaTipo($idArea)
	{
		$this->db->select('MAREACLIENTE.*');
		$this->db->from('PEVALPRODAREATIPOPROV');
		$this->db->join('MAREACLIENTE', 'PEVALPRODAREATIPOPROV.ZCTIPOPRODUCTOEVALUAR = MAREACLIENTE.CAREACLIENTE', 'inner');
		$this->db->where('PEVALPRODAREATIPOPROV.ZCAREAREQUISITO', $idArea);
		$this->db->where('MAREACLIENTE.SREGISTRO', 'A');
		$this->db->order_by('MAREACLIENTE.DAREACLIENTE', 'ASC');
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

	/**
	 * Lista de requisitos de homologacion
	 * @return array|array[]|object|object[]
	 */
	public function lista($idTipo, $idMarca)
	{
		$this->db->select('
			mrequisitoevalproducto.crequisitopdto,   
			 mrequisitoevalproducto.ccompania,   
			 mrequisitoevalproducto.ddocumento,   
			 mrequisitoevalproducto.stipocampo,   
			 mrequisitoevalproducto.cusuariocrea,   
			 mrequisitoevalproducto.tcreacion,   
			 mrequisitoevalproducto.cusuariomodifica,   
			 mrequisitoevalproducto.tmodificacion,   
			 mrequisitoevalproducto.sregistro,   
			 mrequisitoevalproducto.zctipoproductoevaluar,   
			 mrequisitoevalproducto.norden,   
			 mrequisitoevalproducto.stitulo,   
			 mareacliente.dareacliente,   
			 mrequisitoevalproducto.crequisitogeneral,   
			 mrequisitoevalproducto.zcarearequisito,   
			 mrequisitoevalproducto.zctipomarca
		');
		$this->db->from('MREQUISITOEVALPRODUCTO');
		$this->db->join('MAREACLIENTE', 'MREQUISITOEVALPRODUCTO.ZCTIPOPRODUCTOEVALUAR = MAREACLIENTE.CAREACLIENTE', 'inner');
		$this->db->where('MREQUISITOEVALPRODUCTO.CCOMPANIA', 2);
		$this->db->group_start();
		$this->db->like('MREQUISITOEVALPRODUCTO.ZCTIPOPRODUCTOEVALUAR', $idTipo, 'both', false);
		$this->db->where('MREQUISITOEVALPRODUCTO.ZCTIPOMARCA', $idMarca);
		$this->db->order_by(' mrequisitoevalproducto.norden', 'asc');
		$this->db->group_end();
		$query = $this->db->get();
		if (!$query) {
			return [];
		}
		return ($query->num_rows() > 0) ? $query->result() : [];
	}

}
