/*!
 *
 * @version 1.0.0
 */

const objProveedor = {};
var tblProveedores, oTableAddProv;

$(function () {

	objProveedor.mostrarRegProv = function (ccliente, razonsocial, direccioncliente, dzip, cpais, dciudad, destado, cubigeo, dubigeo) {
		console.log('ccliente', ccliente)
		const boton = $('#btnAccionContenedorLista');
		const icon = boton.find('i');
		if (icon.hasClass('fa-plus')) icon.removeClass('fa-plus');
		icon.addClass('fa-minus');
		boton.click();

		$('#btnProvNuevo').hide();
		$('#cardRegprov').hide();
		$('#cardListProv').show();

		document.querySelector('#lblClienteProv').innerText = razonsocial;
		document.querySelector('#lblDirclieProv').innerText = direccioncliente;

		$('#mhdnIdClie').val(ccliente);
		$('#hdnIdptclieprov').val(ccliente);

		$('#mhdnprovCcliente').val(ccliente);
		$('#mhdnprovDcliente').val(razonsocial);
		$('#mhdnprovDdireccion').val(direccioncliente);
		$('#mhdnprovDzid').val(dzip);
		$('#mhdnprovCpais').val(cpais);
		$('#mhdnprovDciudad').val(dciudad);
		$('#mhdnprovDestado').val(destado);
		$('#mhdnprovCubigeo').val(cubigeo);
		$('#mhdnprovDubigeo').val(dubigeo);

		listProvee(ccliente);

		$('#contenedorRegprov').show();
		$('#contenedorBusqueda').hide();
		$('#contenedorRegestable').hide();
		$('#contenedorRegmaq').hide();
		$('#contenedorRegserv').hide();
		$('#contenedorRegcontacto').hide();
		$('#contenedorRegarea').hide();
	};

});

$(document).ready(function () {

});

listProvee = function (ccliente) {

	tblProveedores = $('#tblListProveedores').DataTable({
		dom: 'Bfrtip',
		buttons: [
			'csvHtml5', 'excelHtml5'
		],
		"processing": true,
		"bDestroy": true,
		"stateSave": true,
		"bJQueryUI": true,
		'bStateSave': true,
		"scrollY": "500px",
		"scrollX": true,
		'AutoWidth': true,
		"paging": false,
		"info": true,
		"filter": true,
		"ordering": false,
		"responsive": false,
		"select": true,
		//'fixedColumns':{
		//  'leftColumns': false,// Fijo primera columna
		//  'rightColumns':1
		//},
		//'lengthMenu'  : [[10, 20, 30, -1], [10, 20, 30, "Todo"]],
		'ajax': {
			"url": baseurl + "at/ctrlprov/cliente/cproveedor/lista",
			"type": "POST",
			"data": function (d) {
				d.ccliente = ccliente
			},
			dataSrc: ''
		},
		'columns': [
			{data: 'SPACE', "class": "col-xxs"},
			{
				"orderable": false, "class": "col-xxs",
				render: function (data, type, row) {
					var drazonsocial = row.drazonsocial.replace('\'', '');
					var ddireccioncliente = row.ddireccioncliente.replace('\'', '');
					return '<div class="dropdown" style="text-align: center;">' +
						'<a  data-toggle="dropdown" href="#"><span class="fas fa-bars"></span></a>' +
						'<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">' +
						'<li><a title="Establecimientos" style="cursor:pointer; color:blue;" onClick="objEstablecimiento.mostrarRegEstable(\'' + row.cproveedorcliente + '\',\'' + drazonsocial + '\',\'' + ddireccioncliente + '\');"><span class="fas fa-map-marked-alt" aria-hidden="true">&nbsp;</span>&nbsp;Establecimientos</a></li>' +
						'<li><a title="Maquilador" style="cursor:pointer; color:blue;" onClick="objMaquilador.mostrarRegMaq(\'' + row.cproveedorcliente + '\',\'' + drazonsocial + '\',\'' + ddireccioncliente + '\');"><span class="fas fa-users" aria-hidden="true">&nbsp;</span>&nbsp;Maquilador</a></li>' +
						// '<li><a title="Contactos" style="cursor:pointer; color:blue;" onClick="objFormulario.mostrarRegProv(\''+row.ccliente+'\',\''+row.drazonsocial+'\',\''+row.ddireccioncliente+'\');"><span class="fas fa-user-plus" aria-hidden="true">&nbsp;</span>&nbsp;Contactos</a></li>'+
						'</ul>' +
						'</div>'
				}
			},
			{data: 'cproveedorcliente', "class": "col-xxs"},
			{data: 'drazonsocial', "class": "col-lm"},
			{data: 'nruc', "class": "col-lm"},
		],
	});
	// Enumeracion
	tblProveedores.on('order.dt search.dt', function () {
		tblProveedores.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
			cell.innerHTML = i + 1;
		});
	}).draw();
};

listarclienteEnProv = function () {
	oTableAddProv = $('#tblListPtclienteProv').DataTable({
		"processing": true,
		"bDestroy": true,
		"stateSave": true,
		"bJQueryUI": true,
		"scrollY": "500px",
		"scrollX": true,
		'AutoWidth': true,
		"paging": true,
		"info": true,
		"filter": true,
		"ordering": false,
		"responsive": true,
		"select": true,
		"serverSide": false,
		'ajax': {
			"url": baseurl + "at/ctrlprov/cliente/cproveedor/lista_proveedores",
			"type": "POST",
			"data": function (d) {
				d.ccliente = $('#hdnIdptclieprov').val();
			},
			dataSrc: ''
		},
		'columns': [
			{data: 'SPACE', "class": "col-xxs"},
			{
				"orderable": false, "class": "col-xxs",
				render: function (data, type, row) {
					return '<div class="dropdown" style="text-align: center;">' +
						'<a  data-toggle="dropdown" href="#"><span class="fas fa-bars"></span></a>' +
						'<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">' +
						'<li><a title="Agregar Proveedor" style="cursor:pointer; color:blue;" onClick="addProveedorCliente(\'' + row.ccliente + '\',\'' + row.drazonsocial + '\',\'' + row.ddireccioncliente + '\');"><span class="fas fa-user-plus" aria-hidden="true">&nbsp;</span>&nbsp;Agregar Proveedor</a></li>' +

						'</ul>' +
						'</div>'

				}
			},
			{data: 'ccliente', "class": "col-xxs"},
			{data: 'drazonsocial', "class": "col-md"},
			{data: 'ddireccioncliente', "class": "col-md"},
			{data: 'drepresentante', "class": "col-md"},
			{data: 'nruc', "class": "col-md"},
		],
	});
	// Enumeracion
	oTableAddProv.on('order.dt search.dt', function () {
		oTableAddProv.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
			cell.innerHTML = i + 1;
		});
	}).draw();
};

addProveedorCliente = function (ccliente, drazonsocial, ddireccioncliente) {
	var cclientePrincipal = $("#hdnIdptclieprov").val();
	var params = {"ccliente": cclientePrincipal, "cproveedor": ccliente};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "at/ctrlprov/cliente/cproveedor/guardar",
		dataType: "JSON",
		async: true,
		data: params,
		error: function () {
			Vtitle = 'Error en el proceso!';
			Vtype = 'error';
			sweetalert(Vtitle, Vtype);
		}
	}).done(function() {
		sweetalert('Datos Guardados correctamente', 'success');
		oTableAddProv.ajax.reload();
		tblProveedores.ajax.reload();
	});
}
