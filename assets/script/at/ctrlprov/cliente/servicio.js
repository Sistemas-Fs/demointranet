/*!
 *
 * @version 1.0.0
 */

const objServicio = {};
var tblServicios;

$(function() {

	objServicio.mostrarRegServ = function (ccliente, razonsocial, ddireccioncliente) {
		console.log('ccliente', ccliente)

		const boton = $('#btnAccionContenedorLista');
		const icon = boton.find('i');
		if (icon.hasClass('fa-plus')) icon.removeClass('fa-plus');
		icon.addClass('fa-minus');
		boton.click();

		$('#cardRegServ').hide();
		$('#cardListServ').show();

		document.querySelector('#lblClienteServ').innerText = razonsocial;
		document.querySelector('#lblDirclieServ').innerText = ddireccioncliente;

		$('#mhdnIdClie').val(ccliente);
		$('#hdnIdptclieserv').val(ccliente);

		$('#mhdnprovCcliente').val(ccliente);
		$('#mhdnprovDcliente').val(razonsocial);
		$('#mhdnprovDdireccion').val(ddireccioncliente);

		$("#idCLienteServ").val(ccliente);

		listServicios(ccliente);
		$('#contenedorRegserv').show();
		$('#contenedorRegmaq').hide();
		$('#contenedorRegprov').hide();
		$('#contenedorBusqueda').hide();
		$('#contenedorRegestable').hide();
		$('#contenedorRegcontacto').hide();
		$('#contenedorRegarea').hide();
	};

});

$(document).ready(function() {

	$('#btnAddServiciosModal').click(function () {
		$.ajax({
			type: 'ajax',
			method: 'post',
			url: baseurl + "at/ctrlprov/cliente/cservicio/get_servicios",
			dataType: "JSON",
			async: true,
			success: function (result) {
				let opciones = '';
				if (result && Array.isArray(result)) {
					result.forEach(function(item) {
						opciones += '<option value="' + item.CSERVICIO + '" >' + item.DSERVICIO + '</option>';
					});
				}
				$("#cboServicio").html(opciones);
			},
			error: function () {
				Vtitle = 'Error en el proceso!';
				Vtype = 'error';
				sweetalert(Vtitle, Vtype);
			}
		});

		$.ajax({
			type: 'ajax',
			method: 'post',
			url: baseurl + "at/ctrlprov/cliente/cservicio/get_responsabled",
			dataType: "JSON",
			success: function (result) {
				let opciones = '';
				if (result && Array.isArray(result)) {
					result.forEach(function(item) {
						opciones += '<option value="' + item.cusuario + '" >' + item.datosrazonsocial + '</option>';
					});
				}
				$("#cboResponsableOI").html(opciones);
			},
			error: function () {
				Vtitle = 'Error en el proceso!';
				Vtype = 'error';
				sweetalert(Vtitle, Vtype);
			}
		});
	});

	$('#cboServicio').change(function () {
		var id = $('#cboServicio').val();
		$.ajax({
			type: 'ajax',
			method: 'post',
			url: baseurl + "at/ctrlprov/cliente/cservicio/get_subservicios",
			dataType: "JSON",
			data: {id: id},
			success: function (result) {
				let opciones = '';
				if (result && Array.isArray(result)) {
					result.forEach(function(item) {
						opciones += '<option value="' + item.CSUBSERVICIO + '" >' + item.DSUBSERVICIO + '</option>';
					});
				}
				$("#cboSubServicio").html(opciones);
			},
			error: function () {
				Vtitle = 'Error en el proceso cboSubServicio!';
				Vtype = 'error';
				sweetalert(Vtitle, Vtype);
			}

		});
	});

	$("#btnCreateServicio").click(function () {
		var data = $("#frmCreateServicio").serialize();
		console.log('data', data)

		var servicio = $("#cboServicio").val();
		var subservicio = $("#cboSubServicio").val();
		var responsable = $("#cboResponsableOI").val();

		if (subservicio === "" || servicio === "" || responsable === "") {
			Vtitle = 'Completar todos los campos';
			Vtype = 'error';
			sweetalert(Vtitle, Vtype);
		} else {
			$.ajax({
				type: 'ajax',
				data: data,
				method: 'post',
				url: baseurl + "oi/ctrlprov/mantenimiento/cclientes/createServicio",
				dataType: "JSON",
				async: true,
				success: function (result) {
					Vtitle = 'Se genero el pcte correctamente!';
					Vtype = 'success';
					sweetalert(Vtitle, Vtype);
					$("#modalAddServicio").modal('hide');
					tblServicios.ajax.reload();
				},
				error: function () {
					Vtitle = 'Error en el proceso!';
					Vtype = 'error';
					sweetalert(Vtitle, Vtype);
				}

			});
		}
	});

});

listServicios = function (ccliente) {
	tblServicios = $('#tblListServicios').DataTable({
		dom: 'Bfrtip',
		buttons: [
			'csvHtml5', 'excelHtml5'
		],
		"processing": true,
		"bDestroy": true,
		"stateSave": true,
		"bJQueryUI": true,
		'bStateSave': true,
		"scrollY": "500px",
		"scrollX": true,
		'AutoWidth': true,
		"paging": false,
		"info": true,
		"filter": true,
		"ordering": false,
		"responsive": false,
		"select": true,
		//'fixedColumns':{
		//  'leftColumns': false,// Fijo primera columna
		//  'rightColumns':1
		//},
		//'lengthMenu'  : [[10, 20, 30, -1], [10, 20, 30, "Todo"]],
		'ajax': {
			"url": baseurl + "at/ctrlprov/cliente/cservicio/lista",
			"type": "POST",
			"data": function (d) {
				d.ccliente = ccliente
			},
			dataSrc: ''
		},
		'columns': [
			{data: 'SPACE', "class": "col-xxs"},
			{data: 'cinternopte', "class": "col-xxs"},
			{data: 'dservicio', "class": "col-lm"},
			{data: 'dsubservicio', "class": "col-lm"},
			{data: 'datosrazonsocial', "class": "col-lm"}
		],
	});
	// Enumeracion
	tblServicios.on('order.dt search.dt', function () {
		tblServicios.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
			cell.innerHTML = i + 1;
		});
	}).draw();
};
