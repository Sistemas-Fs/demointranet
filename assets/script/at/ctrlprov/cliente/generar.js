/*!
 *
 * @version 1.0.0
 */

const objClienteGenerar = {};

$(function() {

	objClienteGenerar.limpiarForm = function() {
		$('#frmMantptClie').trigger("reset");
		$('#hdnIdptclie').val('');
		$('#cboPais').val('').trigger("change");
		$('#cboUbigeo').val('').trigger("change");
	};

	objClienteGenerar.mostrarDatos = function(ccliente, nruc, drazonsocial, cpais, dciudad, destado, dzip, cubigeo, ddireccioncliente, dtelefono,
									  dfax, dweb, zctipotamanoempresa, ntrabajador, drepresentante, dcargorepresentante, demailrepresentante,
									  druta, tipodoc, dubigeo) {
		var ruta_imagen;
		ruta_imagen = baseurl + 'FTPfileserver/Imagenes/clientes/' + druta;
		$('#hdnIdptclie').val(ccliente);
		$('#txtnrodoc').val(nruc);
		$('#txtrazonsocial').val(drazonsocial);
		$('#cboPais').val(cpais).trigger("change");
		$('#txtCiudad').val(dciudad);
		$('#txtEstado').val(destado);
		$('#hdnidubigeo').val(cubigeo);
		$('#mtxtUbigeo').val(dubigeo);
		$('#txtCodigopostal').val(dzip);
		$('#txtDireccion').val(ddireccioncliente);
		$('#txtTelefono').val(dtelefono);
		$('#txtFax').val(dfax);
		$('#txtWeb').val(dweb);
		$('#txtTipoempresa').val(zctipotamanoempresa);
		$('#txtNroTrab').val(ntrabajador);
		$('#txtRepresentante').val(drepresentante);
		$('#txtCargorep').val(dcargorepresentante);
		$('#txtEmailrep').val(demailrepresentante);
		$('#hdnAccionptclie').val('E');
		$('#utxtlogo').val(druta);
		$('#cboTipoDoc').val(tipodoc);
		document.getElementById("image_previa").src = ruta_imagen;
		$('#tabptcliente a[href="#tabptcliente-reg"]').tab('show');
		$('#divlogo').hide();
		$("#mbtnsavecliente").prop('disabled', false);
		$('#hdnCCliente').val(ccliente);
	};

	objClienteGenerar.salir = function() {
		objClienteGenerar.limpiarForm();
		objClienteLista.refrescarLista();
		$('#tabptcliente a[href="#tabptcliente-list"]').tab('show');
	};

});

$(document).ready(function() {

	$('#frmMantptClie').submit(function (event) {
		event.preventDefault();
		const form = $('#frmMantptClie');
		$.ajax({
			url: form.attr("action"),
			type: 'POST',
			data: form.serialize(),
			dataType: 'json',
		}).done(function (resp) {
			console.log('respuesta', resp);
			sweetalert(resp.message, 'success');
			objClienteGenerar.salir();
		}).fail(function(jqxhr) {
			const message = (jqxhr.responseJSON && jqxhr.responseJSON.message)
				? jqxhr.responseJSON.message
				: 'Error en el proceso de ejecución.';
			sweetalert(message, 'error');
		});
	});

	$('#btnRetornar').click(function () {
		objClienteGenerar.salir();
	});

});
