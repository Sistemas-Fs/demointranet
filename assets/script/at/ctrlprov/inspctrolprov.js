var otblListinspctrlprov, otblinspeccionprov;
var varfdesde = '%', varfhasta = '%';

$(document).ready(function () {
	$('#tabctrlprov a[href="#tabctrlprov-list-tab"]').attr('class', 'disabled');
	$('#tabctrlprov a[href="#tabctrlprov-det-tab"]').attr('class', 'disabled active');

	$('#tabctrlprov a[href="#tabctrlprov-list-tab"]').not('#store-tab.disabled').click(function (event) {
		$('#tabctrlprov a[href="#tabctrlprov-list"]').attr('class', 'active');
		$('#tabctrlprov a[href="#tabctrlprov-det"]').attr('class', '');
		return true;
	});
	$('#tabctrlprov a[href="#tabctrlprov-det-tab"]').not('#bank-tab.disabled').click(function (event) {
		$('#tabctrlprov a[href="#tabctrlprov-det"]').attr('class', 'active');
		$('#tabctrlprov a[href="#tabctrlprov-list"]').attr('class', '');
		return true;
	});

	$('#tabctrlprov a[href="#tabctrlprov-list"]').click(function (event) {
		return false;
	});
	$('#tabctrlprov a[href="#tabctrlprov-det"]').click(function (event) {
		return false;
	});

	$('#txtFDesde,#txtFHasta,#txtFInspeccion').datetimepicker({
		format: 'DD/MM/YYYY',
		daysOfWeekDisabled: [0],
		locale: 'es',
		autoclose: true,
		todayBtn: true
	});
	fechaActual();

	/*LLENADO DE COMBOS*/
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "at/ctrlprov/cinspctrolprov/getcboclieserv",
		dataType: "JSON",
		async: true,
		success: function (result) {
			$('#cboclieserv').html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboclieserv');
		}
	});
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "at/ctrlprov/cinspctrolprov/getcboestado",
		dataType: "JSON",
		async: true,
		success: function (result) {
			$('#cboestado').html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboestado');
		}
	})
	var params = {"sregistro": '%'};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "at/ctrlprov/cinspctrolprov/getcboinspector",
		dataType: "JSON",
		async: true,
		data: params,
		success: function (result) {
			$('#cboinspector').html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboinspector');
		}
	})

	//
	$('#frmCreactrlprov').validate({
		rules: {
			cboregClie: {
				required: true,
			},
			cboregprovclie: {
				required: true,
			},
			cboregestable: {
				required: true,
			},
			cboregareaclie: {
				required: true,
			},
		},
		messages: {
			cboregClie: {
				required: "Por Favor escoja un Cliente"
			},
			cboregprovclie: {
				required: "Por Favor escoja un Proveedor"
			},
			cboregestable: {
				required: "Por Favor escoja un Establecimiento"
			},
			cboregareaclie: {
				required: "Por Favor escoja un Area"
			},
		},
		errorElement: 'span',
		errorPlacement: function (error, element) {
			error.addClass('invalid-feedback');
			element.closest('.form-group').append(error);
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass('is-invalid');
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).removeClass('is-invalid');
		},
		submitHandler: function (form) {
			const botonEvaluar = $('#mbtnGCreactrl');
			var request = $.ajax({
				url: $('#frmCreactrlprov').attr("action"),
				type: $('#frmCreactrlprov').attr("method"),
				data: $('#frmCreactrlprov').serialize(),
				error: function () {
					Vtitle = 'Error en Guardar!!!';
					Vtype = 'error';
					sweetalert(Vtitle, Vtype);
					objPrincipal.liberarBoton(botonEvaluar);
				},
				beforeSend: function () {
					objPrincipal.botonCargando(botonEvaluar);
				}
			});
			request.done(function (respuesta) {
				var posts = JSON.parse(respuesta);

				$.each(posts, function () {
					$('#mhdnregIdinsp').val(this.cauditoriainspeccion);

					Vtitle = 'Inspección Guardada!!!';
					Vtype = 'success';
					sweetalert(Vtitle, Vtype);
					otblListinspctrlprovv.ajax.reload(null, false);
					objPrincipal.liberarBoton(botonEvaluar);
				});
			});
			return false;
		}
	});
	$('#frmRegInsp').validate({
		rules: {},
		messages: {},
		errorElement: 'span',
		errorPlacement: function (error, element) {
			error.addClass('invalid-feedback');
			element.closest('.form-group').append(error);
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass('is-invalid');
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).removeClass('is-invalid');
		},
		submitHandler: function (form) {
			const botonEvaluar = $('#btnRegistrar');
			var request = $.ajax({
				url: $('#frmRegInsp').attr("action"),
				type: $('#frmRegInsp').attr("method"),
				data: $('#frmRegInsp').serialize(),
				error: function () {
					Vtitle = 'Error en Guardar!!!';
					Vtype = 'error';
					sweetalert(Vtitle, Vtype);
					objPrincipal.liberarBoton(botonEvaluar);
				},
				beforeSend: function () {
					objPrincipal.botonCargando(botonEvaluar);
				}
			});
			request.done(function (respuesta) {
				var posts = JSON.parse(respuesta);

				$.each(posts, function () {
					//$('#mhdnregIdinsp').val(this.cauditoriainspeccion);

					Vtitle = 'Inspección Guardada!!!';
					Vtype = 'success';
					sweetalert(Vtitle, Vtype);
					objPrincipal.liberarBoton(botonEvaluar);
				});
			});
			return false;
		}
	});

	descargarPeligroQuejas();
});

fechaAnioActual = function () {
	$("#txtFIni").prop("disabled", false);
	$("#txtFFin").prop("disabled", false);

	varfdesde = '';
	varfhasta = '';

	var fecha = new Date();
	var fechatring1 = "01/01/" + (fecha.getFullYear());
	var fechatring2 = ("0" + fecha.getDate()).slice(-2) + "/" + ("0" + (fecha.getMonth() + 1)).slice(-2) + "/" + fecha.getFullYear();

	$('#txtFDesde').datetimepicker('date', moment(fechatring1, 'DD/MM/YYYY'));
	$('#txtFHasta').datetimepicker('date', moment(fechatring2, 'DD/MM/YYYY'));
};

fechaActual = function () {
	var fecha = new Date();
	var fechatring = ("0" + fecha.getDate()).slice(-2) + "/" + ("0" + (fecha.getMonth() + 1)).slice(-2) + "/" + fecha.getFullYear();

	$('#txtFDesde').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY'));
	$('#txtFHasta').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY'));
	$('#mtxtFCoti').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY'));

};

$('#txtFDesde').on('change.datetimepicker', function (e) {

	$('#txtFHasta').datetimepicker({
		format: 'DD/MM/YYYY',
		daysOfWeekDisabled: [0],
		locale: 'es'
	});

	var fecha = moment(e.date).format('DD/MM/YYYY');

	$('#txtFHasta').datetimepicker('minDate', fecha);
	$('#txtFHasta').datetimepicker('date', fecha);

});

$("#chkFreg").on("change", function () {
	if ($("#chkFreg").is(":checked") == true) {
		$("#txtFIni").prop("disabled", false);
		$("#txtFFin").prop("disabled", false);

		varfdesde = '';
		varfhasta = '';

		var fecha = new Date();
		var fechatring1 = "01/01/" + (fecha.getFullYear());
		var fechatring2 = ("0" + fecha.getDate()).slice(-2) + "/" + ("0" + (fecha.getMonth() + 1)).slice(-2) + "/" + fecha.getFullYear();

		$('#txtFDesde').datetimepicker('date', fechatring1);
		$('#txtFHasta').datetimepicker('date', fechatring2);
	} else if ($("#chkFreg").is(":checked") == false) {
		$("#txtFIni").prop("disabled", true);
		$("#txtFFin").prop("disabled", true);

		varfdesde = '%';
		varfhasta = '%';

		fechaActual();
	}
	;
});

$("#cboclieserv").change(function () {

	var select = document.getElementById("cboclieserv"), //El <select>
		value = select.value, //El valor seleccionado
		text = select.options[select.selectedIndex].innerText;
	document.querySelector('#lblCliente').innerText = text;

	var v_cboclieserv = $('#cboclieserv').val();
	var params = {"ccliente": v_cboclieserv};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/cinspctrolprov/getcboprovxclie",
		dataType: "JSON",
		async: true,
		data: params,
		success: function (result) {
			$("#cboprovxclie").html(result);
			$('#cboprovxclie').val('').trigger("change");
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboprovxclie');
		}
	});

	/*if(v_cboclieserv != 0){
		$("#btnNuevo").prop("disabled",false);
	}else{
		$("#btnNuevo").prop("disabled",true);
	}*/
});

$("#cboprovxclie").change(function () {

	var v_cboprov = $('#cboprovxclie').val();
	var params = {"cproveedor": v_cboprov};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/cinspctrolprov/getcbomaqxprov",
		dataType: "JSON",
		async: true,
		data: params,
		success: function (result) {
			$("#cbomaqxprov").html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cbomaqxprov');
		}
	});
});

$("#btnBuscar").click(function () {
	let fdesde = '%';
	let fhasta = '%';
	if ($('#chkFreg').is(':checked')) {
		fdesde = $('#txtFIni').val();
		fhasta = $('#txtFFin').val();
	}
	const boton = $("#btnBuscar");
	var groupColumn = 0;
	otblListinspctrlprov = $('#tblListinspctrlprov').DataTable({
		'responsive': false,
		'bJQueryUI': true,
		'scrollY': '400px',
		'scrollX': true,
		'paging': true,
		'processing': true,
		'bDestroy': true,
		'AutoWidth': true,
		'info': true,
		'filter': true,
		'ordering': false,
		'stateSave': true,
		'ajax': {
			"url": baseurl + "at/ctrlprov/cinspctrolprov/getbuscarctrlprov",
			"type": "POST",
			"data": function (d) {
				d.ccliente = $('#cboclieserv').val();
				d.fdesde = fdesde;
				d.fhasta = fhasta;
				d.dclienteprovmaq = $('#txtprovmaq').val();
				d.inspector = $('#cboinspector').val();
				d.cboestado = $('#cboestado').val();
			},
			dataSrc: function (data) {
				objPrincipal.liberarBoton(boton);
				return data;
			},
		},
		'columns': [
			{"orderable": false, data: 'desc_gral', targets: 0, "visible": false},
			{"orderable": false, data: 'areacli', targets: 1, "visible": false},
			{"orderable": false, data: 'lineaproc', targets: 2, "visible": false},
			{"orderable": false, data: 'periodo', "class": "col-s", targets: 3},
			{"orderable": false, data: 'destado', "class": "col-sm", targets: 4},
			{"orderable": false, data: 'finspeccion', "class": "col-s", targets: 5},
			{"orderable": false, data: 'dinformefinal', "class": "col-sm", targets: 6},
			{"orderable": false, data: 'resultado', "class": "col-sm", targets: 7},
			{
				responsivePriority: 1,
				"orderable": false, // Se edito el procedimiento usp_oi_ctrlprov_getbuscarctrlprov
				render: function (data, type, row) {
					let result = '';
					switch (row.zctipoestadoservicio) {
						case '031': // EN PROCESO
							result = '<div><a title="Checklist" style="cursor:pointer; color:#3c763d;" onClick="javascript:selInspe(\'' + row.desc_gral + '\',\'' + row.areacli + '\',\'' + row.lineaproc + '\',\'' + row.cauditoriainspeccion + '\',\'' + row.fservicio + '\');"><span class="fas fa-th-list fa-2x" aria-hidden="true"> </span> </a></div>';
							break;
						case '029': // INSPECTOR ASIGNADO
							result = '<div><a title="Iniciar checklist" style="cursor:pointer; color:#0069d9;" onClick="javascript:iniciarChecklist(\'' + row.desc_gral + '\',\'' + row.areacli + '\',\'' + row.lineaproc + '\',\'' + row.cauditoriainspeccion + '\',\'' + row.fservicio + '\',\'' + row.finspeccion + '\',\'' + row.cchecklist + '\',\'' + row.cmodeloinforme + '\',\'' + row.cgrupocriterio + '\',\'' + row.cvalornoconformidad + '\');"><span class="fas fa-check fa-2x" aria-hidden="true"> </span> </a></div>';
							// result = '<div><a title="Iniciar checklist" style="cursor:pointer; color:#0069d9;" href="' + BASE_URL + 'at/ctrlprov/cinspctrolprov/inspeccion/' + row.cauditoriainspeccion + '" ><span class="fas fa-check fa-2x" aria-hidden="true"> </span> </a></div>';
							break;
					}
					return result;
				}
			}
		],
		"columnDefs": [
			{"targets": [0], "visible": false},
		],
		"drawCallback": function (settings) {
			var api = this.api();
			var rows = api.rows({page: 'all'}).nodes();
			var last = null;
			var grupo, grupo01;

			api.column([0], {}).data().each(function (ctra, i) {
				grupo = api.column(1).data()[i];
				grupo01 = api.column(2).data()[i];
				if (last !== ctra) {
					$(rows).eq(i).before(
						'<tr class="group"><td colspan="10" class="subgroup"><strong>' + ctra.toUpperCase() + '</strong></td></tr>' +
						'<tr class="group"><td colspan="10">Area : ' + grupo + '<tab><tab>Linea : ' + grupo01 + '</td></tr>'
					);
					last = ctra;
				}
			});
		}
	});
	otblListinspctrlprov.column(1).visible(false);
	otblListinspctrlprov.column(2).visible(false);
});

//selInspe = function(cauditoriainspeccion,desc_gral,areacli,lineaproc,cusuarioconsultor,periodo,fservicio,cnorma,csubnorma,cchecklist,cmodeloinforme,cvalornoconformidad,cformulaevaluacion,ccriterioresultado,dcomentario,zctipoestadoservicio,destado,ccliente){
selInspe = function (desc_gral, areacli, lineaproc, cauditoriainspeccion, fservicio) {
	$('#tabctrlprov a[href="#tabctrlprov-det"]').tab('show');

	$('#frmRegInsp').trigger("reset");

	var proveedor = 'PROVEEDOR: ' + desc_gral;
	var area = 'AREA: ' + areacli;
	var linea = 'LINEA: ' + lineaproc;
	$('#mtxtidinsp').val(cauditoriainspeccion);
	$("#mtxtinspdatos").text(proveedor);
	$("#mtxtinsparea").text(area);
	$("#mtxtinsplinea").text(linea);

	$('#mhdnAccioninsp').val('A');
	//$('#mhdnccliente').val(ccliente);

	//iniInspe(cusuarioconsultor,'T',cnorma,csubnorma,cchecklist,ccliente,cvalornoconformidad,cmodeloinforme,cformulaevaluacion,ccriterioresultado);

	/*$('#mtxtinspdatos').val(desc_gral);
	$('#mtxtinsparea').val('AREÁ : '+areacli);
	$('#mtxtinsplinea').val('LINEA : '+lineaproc);

	$('#cboinspperiodo').val(periodo);
	$('#mtxtinspcoment').val(dcomentario);
	$('#mhdnzctipoestado').val(zctipoestadoservicio);
	$('#txtinspestado').val(destado);*/

	listChecklist(cauditoriainspeccion, fservicio);

	$('#txtFInspeccion').datetimepicker({
		format: 'DD/MM/YYYY',
		daysOfWeekDisabled: [0],
		locale: 'es',
		autoclose: true,
		todayBtn: true
	});
	if (fservicio == '01/01/1900') {
		$('#txtFInsp').val('Sin Fecha');
	} else {
		$('#txtFInsp').val(fservicio);
	}

};

$("#cboinspsistema").change(function () {
	var v_cnorma = $("#cboinspsistema option:selected").attr("value");

	var paramsrubro = {"cnorma": v_cnorma};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/cinspctrolprov/getcborubroip",
		dataType: "JSON",
		async: true,
		data: paramsrubro,
		success: function (result) {
			$('#cboinsprubro').html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboinsprubro');
		}
	});
});

$("#cboinsprubro").change(function () {
	var v_csubnorma = $("#cboinsprubro option:selected").attr("value");
	var v_cnorma = $('#cboinspsistema').val();
	var v_ccliente = $('#mhdnccliente').val();
	var paramschecklist = {
		"cnorma": v_cnorma,
		"csubnorma": v_csubnorma,
		"ccliente": v_ccliente,
	};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/cinspctrolprov/getcbochecklist",
		dataType: "JSON",
		async: true,
		data: paramschecklist,
		success: function (result) {
			$('#cboinspcchecklist').html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboinspcchecklist');
		}
	});
});

$("#cboinspcchecklist").change(function () {
	var v_cchecklist = $("#cboinspcchecklist option:selected").attr("value");
	var paramsform = {"cchecklist": v_cchecklist};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/cinspctrolprov/getcboinspformula",
		dataType: "JSON",
		async: true,
		data: paramsform,
		success: function (result) {
			$('#cboinspformula').html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboinspformula');
		}
	});
});


$('#btnRetornarLista').click(function () {
	$('#tabctrlprov a[href="#tabctrlprov-list"]').tab('show');
	$('#btnBuscar').click();
});

listChecklist = function (varcauditoria, varfservicio) {
	otblinspeccionprov = $('#tblinspeccionprov').DataTable({
		'responsive': false,
		'bJQueryUI': true,
		'scrollY': '400px',
		'scrollX': true,
		'paging': false,
		'processing': true,
		'bDestroy': true,
		'AutoWidth': true,
		'info': true,
		'filter': true,
		'ordering': false,
		'stateSave': true,
		'ajax': {
			"url": baseurl + "oi/ctrlprov/cinspctrolprov/getlistachecklist/",
			"type": "POST",
			"data": function (d) {
				d.cauditoriainspeccion = varcauditoria;
				d.fservicio = varfservicio;
			},
			dataSrc: ''
		},
		'columns': [
			{"orderable": false, data: 'dnumeradorpadre', targets: 0},
			{"orderable": false, data: 'dnumerador', targets: 1},
			{"orderable": false, data: 'drequisito', targets: 2},
			{"orderable": false, data: 'nvalormaxrequisito', targets: 3},
			{"orderable": false, data: 'nvalorrequisito', targets: 4},
			{"orderable": false, data: 'noconformidad', targets: 5},
			{"orderable": false, data: 'existeHallazgo', targets: 6},
			{"orderable": false, data: 'dhallazgo', targets: 7},
			{
				responsivePriority: 1, "orderable": false,
				render: function (data, type, row) {
					if (row.srequisito == 'H') {
						return '<div>' +
							'&nbsp; &nbsp;' +
							'<a href="#" data-original-title="Registrar" onClick="selChecklist(\'' + row.cdetallevalornoconformidad + '\',\'' + row.cdetallevalorrequisito + '\',\'' + row.cchecklist + '\',\'' + row.crequisitochecklist + '\',\'' + row.cauditoriainspeccion + '\',\'' + row.cvalorrequisito + '\',\'' + row.cvalornoconformidad + '\',\'' + row.dhallazgo + '\');"><i class="fa fa-edit fa-3x" data-original-title="Registrar" data-toggle="tooltip"></i></a>' +

							'</div>'
					} else {
						return '';
					}

				}
			}
		],
		"columnDefs": [
			{"targets": [0], "visible": true},
		],
	});
};

selChecklist = function (cdetallevalornoconformidad, cdetallevalorrequisito, cchecklist, crequisitochecklist, cauditoriainspeccion, cvalorrequisito, cvalornoconformidad, dhallazgo) {
	$("#int_requisito_checklist").val(crequisitochecklist);
	$("#int_codigo_checklist").val(cchecklist);
	$("#int_auditoria_inspeccion").val(cauditoriainspeccion);

	console.log('CVALORREQUISITO', cvalorrequisito)

	var params = {"int_valor": cvalorrequisito};
	var params2 = {"int_valor": cvalornoconformidad};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/cinspctrolprov/getvalorchecklist",
		dataType: "JSON",
		async: true,
		data: params,
		success: function (result) {
			$('#cmbPuntuacionChecklist').html(result);
			$('#cmbPuntuacionChecklist').val(cdetallevalorrequisito);

		},
		error: function () {
			alert('Error, No se puede autenticar por error ');
		}
	});

	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/cinspctrolprov/getvalorchecklist",
		dataType: "JSON",
		async: true,
		data: params2,
		success: function (result) {
			$('#cmbCriterioHallazgo').html(result);
			$('#cmbCriterioHallazgo').val(cdetallevalornoconformidad);
		},
		error: function () {
			alert('Error, No se puede autenticar por error ');
		}
	});
	$("#txtHallazgo").text('');
	$("#txtHallazgo").text(dhallazgo);


	$("#ModalItemList").modal('show');
}

iniciarChecklist = function (desc_gral, areacli, lineaproc, cauditoriainspeccion, fservicio, finspeccion, cchecklist, cmodeloinforme, cgrupocriterio, cvalornoconformidad) {
	var cod_usuario = $("#cusuario").val();
	Swal.fire({
		title: 'Inciar Checklist',
		text: "¿Está seguro de iniciar el registro de checklist?",
		icon: 'question',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Si, iniciar!'
	}).then((result) => {
		if (result.value) {
			var params = {
				"cauditoriainspeccion": cauditoriainspeccion,
				"fservicio": fservicio,
				"cchecklist": cchecklist,
				"ls_cgrucrieval": cgrupocriterio,
				"ls_cvalnoconf": cvalornoconformidad,
				"str_cod_usuario": cod_usuario
			};

			$.ajax({
				type: 'ajax',
				method: 'post',
				url: baseurl + "at/ctrlprov/cinspctrolprov/cambioestadochecklist",
				dataType: "JSON",
				async: true,
				data: params,
				success: function (result) {
					if (result == 1) {
						$('#tblListinspctrlprov').DataTable().ajax.reload(); // verificar si al actualizar necesita parametros
						Vtitle = 'Checklist iniciado!';
						Vtype = 'success';
						sweetalert(Vtitle, Vtype);
						// location.href = BASE_URL + 'at/ctrlprov/cinspctrolprov/inspeccion?cauditoria=' + cauditoriainspeccion + '&fservicio=' + fservicio;
						selInspe(desc_gral, areacli, lineaproc, cauditoriainspeccion, fservicio, finspeccion, cchecklist, cmodeloinforme);
					} else {
						Vtitle = 'Ups,error con el registro!';
						Vtype = 'error';
						sweetalert(Vtitle, Vtype);
					}
				},
				error: function () {
					alert('Error, No se puede autenticar por error ');
				}
			});
		}
	})

}

$("#btnGuardarChecklist").click(function () {

	var datos = $("#frmChecklist").serialize();
	console.log('datos', datos)

	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + 'oi/ctrlprov/cinspctrolprov/insertarChecklist',
		dataType: 'json',
		async: true,
		data: datos,
		success: function (result) {
			if (result == 1) {
				$("#tblinspeccionprov").DataTable().ajax.reload();
				$('#ModalItemList').modal('hide');
				Vtitle = 'Se guardaron los cambios correctamente';
				Vtype = 'success';
				sweetalert(Vtitle, Vtype);

			} else if (result == 2) {
				Vtitle = 'Ingrese los campos requeridos';
				Vtype = 'warning';
				sweetalert(Vtitle, Vtype);
			}
		},
		error: function () {

			Vtitle = 'Problemas con el Servidor, solicita ayuda!';
			Vtype = 'error';
			sweetalert(Vtitle, Vtype);

		}
	})
});

function descargarPeligroQuejas() {
	$.ajax({
		method: 'post',
		url: baseurl + 'at/ctrlprov/inspctrolprov/cinspeccion/descargar_peligro',
		dataType: 'json',
		async: true,
		data: {},
		beforeSend: function () {}
	}).done(function (resp) {
		if (resp.archivo) {
			const el = $('#descargarPeligro');
			el.attr('href', resp.archivo);
			el.attr('download', resp.nombre);
			el.show();
		}
	}).fail(function (jqxhr) {
		const message = (jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la petición HTTP.';
		sweetalert(message, 'error');
	});
	$.ajax({
		method: 'post',
		url: baseurl + 'at/ctrlprov/inspctrolprov/cinspeccion/descargar_queja',
		dataType: 'json',
		async: true,
		data: {},
		beforeSend: function () {}
	}).done(function (resp) {
		if (resp.archivo) {
			const el = $('#descargarQuejas');
			el.attr('href', resp.archivo);
			el.attr('download', resp.nombre);
			el.show();
		}
	}).fail(function (jqxhr) {
		const message = (jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la petición HTTP.';
		sweetalert(message, 'error');
	});
}
