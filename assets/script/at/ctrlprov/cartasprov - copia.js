
var oTableListcartas;

var seleccionados = [];

$(document).ready(function() {
    
    $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"at/ctrlprov/ccartasprov/getclientes",
        dataType: "JSON",
        async: true,
        success:function(result)
        {
            $('#cboClie').html(result);
        },
        error: function(){
            alert('Error, No se puede autenticar por error');
        }
    });

    $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"cglobales/getanios",
        dataType: "JSON",
        async: true,
        success:function(result)
        {
            $('#cboAnio').html(result);
        },
        error: function(){
            alert('Error, No se puede autenticar por error');
        }
    });

    $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"cglobales/getmeses",
        dataType: "JSON",
        async: true,
        success:function(result)
        {
            $('#cboMes').html(result);
        },
        error: function(){
            alert('Error, No se puede autenticar por error');
        }
    });

    $('#carta-inspeccion-todos').click(function() {
    	const elItems = $('.carta-inspeccion-item');
		elItems.prop('checked', $(this).is(':checked'));
		agregarSeleccionados();
	});

    $(document).on('change', '.carta-inspeccion-item', function() {
		agregarSeleccionados();
	});

});

agregarSeleccionados = function() {
	seleccionados = [];
	$('.carta-inspeccion-item').each(function() {
		const el = $(this);
		if (el.is(':checked')) {
			const inspeccion = el.data('inspeccion');
			const fecha = el.data('fecha');
			seleccionados.push({ inspeccion: inspeccion, fecha: fecha });
		}
	});
};

listarCartas = function(){
	const estadoEvalProd = $('#chElvaprod').is(':checked') ? 1 : 0;
    oTableListcartas = $('#tblListcartas').DataTable({
		'bJQueryUI': true,
		'scrollY': '650px',
		'scrollX': true,
		'processing': true,
		'bDestroy': true,
		'paging': true,
		'info': true,
		'filter': true,
        'ajax'  : {
            "url"   : baseurl+"at/ctrlprov/ccartasprov/getbuscarcartas",
            "type"  : "POST", 
            "data": function ( d ) {
                d.anio      = $('#cboAnio').val();
                d.mes       = $('#cboMes').val();  
                d.ccliente  = $('#cboClie').val();  
                d.sevalprod  = estadoEvalProd;
            },
            dataSrc : ''        
        },
        'columns'     : [
            {
              "class"     :   "index",
              orderable   :   false,
              data        :   null,
              targets     :   0
            },
			{"orderable": false,
				render:function(data, type, row) {
				const id = 'carta-' + row.cauditoriainspeccion + '' + moment(row.fservicio, 'YYYY-MM-DD').format('YYYYMMDD');
					return '<div class="custom-control custom-checkbox">' +
						'<input type="checkbox" class="custom-control-input carta-inspeccion-item" id="' + id + '" name="' + id + '" data-inspeccion="' + row.cauditoriainspeccion + '" data-fecha="' + row.fservicio + '" >' +
						'<label class="custom-control-label" for="' + id + '">&nbsp;</label>' +
						'</div>';
				}
			},
			{"orderable": false,
				render:function(data, type, row) {
					var senviocarta = (row.senviocarta === 'S') ? 'text-danger' : '';
					return'<span class="' + senviocarta + '" >' + row.cauditoriainspeccion + '</span>';
				}
			},
			{"orderable": false,
				render:function(data, type, row) {
					var fservicio = row.fservicio;
					var senviocarta = (row.senviocarta === 'S') ? 'text-danger' : '';
					if (row.fservicio) {
						fservicio = moment(row.fservicio, 'YYYY-MM-DD').format('DD/MM/YYYY');
					}
					return'<span class="' + senviocarta + '" >' + fservicio + '</span>';
				}
			},
			{"orderable": false,
				render:function(data, type, row) {
					var fcreacion = row.fcreacion;
					var senviocarta = (row.senviocarta === 'S') ? 'text-danger' : '';
					if (row.fcreacion) {
						fcreacion = moment(row.fcreacion, 'YYYY-MM-DD').format('DD/MM/YYYY');
					}
					return'<span class="' + senviocarta + '" >' + fcreacion + '</span>';
				}
			},
			{"orderable": false,
				render:function(data, type, row) {
					var senviocarta = (row.senviocarta === 'S') ? 'text-danger' : '';
					return'<span class="' + senviocarta + '" >' + row.proveedor + '</span>';
				}
			},
            {data: 'ESTMAQ', targets: 6},
            {data: 'dlinea', targets: 7},
            {data: 'sischecklist', targets: 8},
			{"orderable": false,
				render:function(data, type, row) {
					var senviocarta = (row.senviocarta === 'S') ? '' : '';
					return'<span class="' + senviocarta + '" >' + row.cchecklist + ' - ' + row.dchecklist + '</span>';
				}
			},
			{"orderable": false,
				render:function(data, type, row) {
					var senviocarta = (row.senviocarta === 'S') ? 'checked' : '';
					const id = 'senvio-' + row.cauditoriainspeccion + '' + moment(row.fservicio, 'YYYY-MM-DD').format('YYYYMMDD');
					return '<div class="custom-control custom-checkbox">' +
						'<input type="checkbox" class="custom-control-input" ' + senviocarta + ' id="' + id + '" disabled >' +
						'<label class="custom-control-label" for="' + id + '">&nbsp;</label>' +
						'</div>';
				}
			},
            {data: 'dubigeo', targets: 11},
            {data: 'tipoempresa', targets: 12},
			{"orderable": false,
				render:function(data, type, row) {
					var sevalprod = '';
					switch(row.sevalprod)
					{
						case 1:
							sevalprod = 'Si';
							break;
						default:
						case 0:
							sevalprod = 'No';
							break;
					}
					return sevalprod;
				}
			},
            {data: 'costo', targets: 14},
        ], 
    });   
    // Enumeracion 
    oTableListcartas.on( 'order.dt search.dt', function () { 
        oTableListcartas.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i+1;
          } );
    }).draw();     
};

$('#btnBuscar').click(function(){
    listarCartas();
});

$('#btnPrint').click(function(){
	const el = $('#formCartasElegidas');
	var v_anio = $( "#cboAnio option:selected").attr("value");
	var v_mes = $( "#cboMes option:selected").attr("value");
	el.attr('action', baseurl + 'at/ctrlprov/ccartasprov/genpdfcartasprov_elegidos/' + v_anio + '/' + v_mes);
	$('#formCartasElegidas').submit();
    // var v_anio = $( "#cboAnio option:selected").attr("value");
    // var v_mes = $( "#cboMes option:selected").attr("value");
    // var v_clie = $( "#cboClie option:selected").attr("value");
    // window.open(baseurl+"at/ctrlprov/ccartasprov/genpdfcartasprov/"+v_anio+"/"+v_mes+"/"+v_clie);
    //
   /* var params = { 
        "anio"  : v_anio,
        "mes"   : v_mes,  
        "clie"  : v_clie 
    };  
    $.ajax({
      type: 'ajax',
      method: 'post',
      url: baseurl+"at/ctrlprov/ccartasprov/genpdfcartasprov",
      dataType: "JSON",
      async: true,
      data: params,
      success:function(result)
      {
          $('#cboDist').html(result);
      },
      error: function(){
        alert('Error, No se puede autenticar por error');
      }
    })*/
});
