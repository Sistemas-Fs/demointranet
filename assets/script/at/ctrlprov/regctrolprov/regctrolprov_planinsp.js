/*!
 *
 * @version 1.0.0
 */

const objPlanInsp = {};

$(function () {

	objPlanInsp.activarPlanInsp = function () {
		const chkPlanInsp = $("#chkplaninsp");
		const btnPlan = $("#Btnplan");
		if (chkPlanInsp.is(":checked")) {
			btnPlan.show();
		} else {
			btnPlan.hide();
		}
	};

	objPlanInsp.fechaActualplaninsp = function () {
		var fecha = new Date();
		var fechatring = ("0" + fecha.getDate()).slice(-2) + "/" + ("0" + (fecha.getMonth() + 1)).slice(-2) + "/" + fecha.getFullYear();
		$('#mtxtFplaninspeccion').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY'));
	};

	objPlanInsp.initBusquedaContacto = function(v_ccliente, v_cproveedor, v_cmaquilador) {
		$("#cbocontacplanins").select2({
			ajax: {
				url: baseurl + "at/ctrlprov/cregctrolprov/getcbocontacplanins",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						search: params.term,
						ccliente: v_ccliente,
						cproveedor: v_cproveedor,
						cmaquilador: v_cmaquilador,
					};
				},
				processResults: function (data) {
					return {results: data};
				},
				cache: true
			},
			escapeMarkup: function (markup) {
				return markup;
			}, // let our custom formatter work
			minimumInputLength: 0,
			// Aqi se muestran todos los resultados de la busqueda
			templateResult: function (state) {
				const dcargocontacto = (state.dcargocontacto) ? state.dcargocontacto : '';
				const dmail = (state.dmail) ? state.dmail : '';
				const dtelefono = (state.dtelefono) ? state.dtelefono : '';
				let result = '<table class="" >';
				result += '<tr>';
				result += '<td style="width: 200px; padding-right: 5px" >' + state.cliente + '</td>';
				result += '<td style="width: 200px; padding-right: 5px" >' + state.text + '</td>';
				result += '<td style="width: 150px; padding-right: 5px" >' + dcargocontacto + '</td>';
				// result += '<td style="width: 150px; padding-right: 5px" >' + dmail + '</td>';
				result += '<td style="width: 100px" >' + dtelefono + '</td>';
				result += '<tr/>';
				result += '<table>';
				return result;
			},
			templateSelection: function (state) {
				if (state.id !== "") {
					const dcargocontacto = (state.dcargocontacto) ? state.dcargocontacto : '';
					const dmail = (state.dmail) ? state.dmail : '';
					const dtelefono = (state.dtelefono) ? state.dtelefono : '';
					let result = '<table class="table-sm" >';
					result += '<tr>';
					result += '<td style="width: 330px; padding-right: 5px; border: none" >' + state.cliente + '</td>';
					result += '<td style="width: 220px; padding-right: 5px; border: none" >' + state.text + '</td>';
					result += '<td style="width: 200px; padding-right: 5px; border: none" >' + dcargocontacto + '</td>';
					// result += '<td style="width: 150px; padding-right: 5px; border: none" >' + dmail + '</td>';
					result += '<td style="width: 200px; border: none" >' + dtelefono + '</td>';
					result += '<tr/>';
					result += '<table>';
					return result;
				} else {
					return 'Elegir contacto';
				}
			},
			placeholder: "Elegir contacto",
			allowClear: true,
			width: '100%',
			dropdownParent: $('#modalPlaninsp'),
		});
	};

});

$(document).ready(function () {

	$("#chkplaninsp").on("change", objPlanInsp.activarPlanInsp);

	$('#modalPlaninsp').on('shown.bs.modal', function (e) {

		$('#frmPlaninsp').trigger("reset");

		const cbotiposerv = $('#cbotiposerv');

		if (cbotiposerv.val() === '013') {
			$('#divfplan').hide();
			document.querySelector('#htitleplan').innerText = 'Plan de Inspección';
		} else {
			$('#divfplan').show();
			document.querySelector('#htitleplan').innerText = 'Plan de Inspección - Inopinada';
		}

		$('#mtxtFplaninspeccion').datetimepicker({
			format: 'DD/MM/YYYY',
			daysOfWeekDisabled: [0],
			locale: 'es'
		});

		$('#mtxtHplaninspeccion').datetimepicker({
			format: 'LT'
		});

		var v_ccliente = $('#mhdnccliente').val();
		var v_cproveedor = $('#mhdncproveedor').val();
		var v_cmaquilador = $('#mhdncmaquilador').val();
		objPlanInsp.initBusquedaContacto(v_ccliente, v_cproveedor, v_cmaquilador);

		objPlanInsp.fechaActualplaninsp();

		var v_dcliente = $('#mhdndcliente').val();
		var v_dlineaproc = $('#mtxtinsplinea').val();

		$('#mhdnidinspplaninsp').val($('#mtxtidinsp').val());
		$('#mhdnfservicioplaninsp').val($('#mfechainsp').val());
		$('#mhdntiposervplaninsp').val(cbotiposerv.val());
		$('#mtxtobjeplaninsp').val('Verificar el cumplimiento de los criterios de inspección, que permita a ' + v_dcliente.toUpperCase() + ' asegurar la calidad sanitaria de los productos comercializados');
		$('#mtxtalcanplaninsp').val(v_dlineaproc);
		$('#mtxtrequeplaninsp').val('Estar Procesando');

	});

});
