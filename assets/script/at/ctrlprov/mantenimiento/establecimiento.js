/*!
 *
 * @version 1.0.0
 */

const objEstablecimiento = {};

$(function() {

	objEstablecimiento.mostrarRegEstable = function (ccliente, razonsocial, direccioncliente, dzip, cpais, dciudad, destado, cubigeo, dubigeo) {
		const boton = $('#btnAccionContenedorLista');
		const icon = boton.find('i');
		if (icon.hasClass('fa-plus')) icon.removeClass('fa-plus');
		icon.addClass('fa-minus');
		boton.click();

		$('#cardRegestable').hide();
		$('#cardLineaProc').hide();
		$('#cardListestable').show();
		$('#btnEstableNuevo').show();

		document.querySelector('#lblCliente').innerText = razonsocial;
		document.querySelector('#lblDirclie').innerText = direccioncliente;

		$('#mhdnIdClie').val(ccliente);

		$('#mhdnestCcliente').val(ccliente);
		$('#mhdnestDcliente').val(razonsocial);
		$('#mhdnestDdireccion').val(direccioncliente);
		$('#mhdnestDzid').val(dzip);
		$('#mhdnestCpais').val(cpais);
		$('#mhdnestDciudad').val(dciudad);
		$('#mhdnestDestado').val(destado);
		$('#mhdnestCubigeo').val(cubigeo);
		$('#mhdnestDubigeo').val(dubigeo);

		listEstable(ccliente);

		$('#contenedorRegestable').show();
		$('#contenedorBusqueda').hide();
		$('#contenedorRegprov').hide();
		$('#contenedorRegmaq').hide();
		$('#contenedorRegserv').hide();
		$('#contenedorRegcontacto').hide();
		$('#contenedorRegarea').hide();
	};

});

$(document).ready(function() {

	$('#btnEstableNuevo').click(function () {
		$('#cardRegestable').show();
		$('#cardListestable').hide();
		$('#cardLineaProc').hide();

		limpiarFormEstable();

		$('#mhdnIdClie').val($('#mhdnestCcliente').val());
		$('#txtestableCI').val('');
		$('#txtestabledireccion').val('');
		$('#txtestablezip').val($('#mhdnestDzid').val());
		$('#cboPaisEstable').val($('#mhdnestCpais').val()).trigger("change");
		$('#txtCiudadEstable').val('');
		$('#txtEstadoEstable').val('');
		$('#hdnidubigeoEstable').val($('#mhdnestCubigeo').val());
		$('#mtxtUbigeoEstable').val('');
		$('#txtestablecelu').val('');
		$('#txtreferenciadir').val('');
		$('#cboestableEstado').val('A').trigger("change");

		$('#mhdnAccionEstable').val('N');
	});

	$('#btnCerrarEstable').click(function () {
		$('#cardRegestable').hide();
		$('#cardListestable').show();
	});

	$('#btnRetornarLista').click(function () {
		objFormulario.mostrarBusqueda();
	});

	$('#btnBuscarUbigeoEstable').click(function () {
		$("#modalUbigeoest").modal();
	});

	$('#frmMantEstablecimiento').submit(function (event) {

		event.preventDefault();

		var request = $.ajax({
			url: $('#frmMantEstablecimiento').attr("action"),
			type: $('#frmMantEstablecimiento').attr("method"),
			data: $('#frmMantEstablecimiento').serialize(),
			error: function () {
				alert('Error, No se puede autenticar por error');
			}
		});
		request.done(function (respuesta) {

			Vtitle = 'Datos Guardados correctamente';
			Vtype = 'success';
			sweetalert(Vtitle, Vtype);
			limpiarFormEstable();
			var v_ccliente = $('#mhdnIdClie').val();
			listEstable(v_ccliente);

			$('#cardRegestable').hide();
			$('#cardLineaProc').hide();
			$('#cardListestable').show();
		});
	});

});

listEstable = function (ccliente) {

	tblEstablecimiento = $('#tblListEstablecimiento').DataTable({
		"processing": true,
		"bDestroy": true,
		"stateSave": true,
		"bJQueryUI": true,
		"scrollY": "500px",
		"scrollX": true,
		'AutoWidth': true,
		"paging": false,
		"info": true,
		"filter": true,
		"ordering": false,
		"responsive": false,
		"select": true,
		//'fixedColumns':{
		//  'leftColumns': false,// Fijo primera columna
		//  'rightColumns':1
		//},
		//'lengthMenu'  : [[10, 20, 30, -1], [10, 20, 30, "Todo"]],
		'ajax': {
			"url": baseurl + 'oi/ctrlprov/mantenimiento/cclientes/getbuscarestablecimiento',
			"type": "POST",
			"data": function (d) {
				d.IDCLIENTE = ccliente

			},
			dataSrc: ''
		},
		'columns': [

			{data: 'SPACE', "class": "col-xxs"},
			{
				"orderable": false, "class": "col-xxs",
				render: function (data, type, row) {
					return '<div class="dropdown" style="text-align: center;">' +
						'<a  data-toggle="dropdown" href="#"><span class="fas fa-bars"></span></a>' +
						'<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">' +
						'<li><a title="Agregar Maquilador" style="cursor:pointer; color:blue;" onClick="objEstablecimientoLinea.mostrarRegLinea(\'' + row.COD_CLIENTE + '\',\'' + row.DRAZONSOCIAL + '\',\'' + row.ddireccioncliente + '\',\'' + row.COD_ESTABLE + '\');"><span class="fas fa-user-plus" aria-hidden="true">&nbsp;</span>&nbsp;Agregar Linea de Proc.</a></li>' +

						'</ul>' +
						'</div>'

				}
			},
			{data: 'DESCRIPESTABLE', "class": "col-lm"},
			{data: 'DIRECCION', "class": "col-lm"},
		],
		"rowCallback": function (row, data) {
			if (data.ESTADO === 'I') {
				$('td', row).css('background-color', '#ff0000');
				$('td', row).css('color', '#ffffff');
			}
		}
	});
	// Enumeracion
	tblEstablecimiento.on('order.dt search.dt', function () {
		tblEstablecimiento.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
			cell.innerHTML = i + 1;
		});
	}).draw();
};

limpiarFormEstable = function () {
	$('#frmMantEstablecimiento').trigger("reset");
	$('#mhdnIdEstable').val('');
	$('#cboPaisEstable').val('').trigger("change");
	$('#mtxtUbigeoEstable').val('').trigger("change");
}
