/*!
 *
 * @version 1.0.0
 */

const objEstablecimientoLinea = {};

$(function () {

	objEstablecimientoLinea.mostrarRegLinea = function (ccliente, razonsocial, ddireccioncliente, COD_ESTABLE, COD_CLIENTE, DRAZONSOCIAL) {
		console.log('ccliente', ccliente)
		const boton = $('#btnAccionContenedorLista');
		const icon = boton.find('i');
		if (icon.hasClass('fa-plus')) icon.removeClass('fa-plus');
		icon.addClass('fa-minus');
		boton.click();
		$('#cardLineaProc').show();
		$('#cardListestable').hide();
		$('#cardRegestable').hide();
		$('#btnEstableNuevo').hide();

		// document.querySelector('#lblClienteContacto').innerText = razonsocial;
		// document.querySelector('#lblDirclieContacto').innerText = (ddireccioncliente !== '') ? ddireccioncliente : '';

		$('#mhdnIdContacto').val(ccliente);
		$('#hdnIdptClieLinea').val(ccliente);

		$('#mhdnconCcliente').val(ccliente);
		$('#mhdnconDcliente').val(razonsocial);
		$('#mhdnconDdireccion').val(ddireccioncliente);

		if (COD_ESTABLE) {
			//listContactoEstable(ccliente,COD_ESTABLE);
			$("#txtestablecimiento").val(COD_ESTABLE);
		} else {
			//listContacto(ccliente);
			$("#txtestablecimiento").val('');
		}
		listLineaEstablec(ccliente, COD_ESTABLE);

		$('#contenedorRegcontacto').hide();
		$('#contenedorRegserv').hide();
		$('#contenedorRegmaq').hide();
		$('#contenedorRegprov').hide();
		$('#contenedorBusqueda').hide();
		$('#contenedorRegarea').hide();
		$('#contenedorRegestable').show();
	};


});

$(document).ready(function () {

	$("#btnModalAddLinea").click(function () {
		var ccliente = $("#hdnIdptClieLinea").val();
		console.log('ccliente', ccliente)
		var COD_ESTABLE = $("#txtestablecimiento").val();
		console.log('COD_ESTABLE', COD_ESTABLE)
		$('#hdnAccionptclieLinea').val('N');
		$('#modalLineaAdd').modal('show');
	});

	$('#frmLineaAdd').submit(function (event) {
		event.preventDefault();
		var request = $.ajax({
			url: $('#frmLineaAdd').attr("action"),
			type: $('#frmLineaAdd').attr("method"),
			data: $('#frmLineaAdd').serialize(),
			error: function () {
				alert('Error, No se puede autenticar por error');
			}
		});
		request.done(function (respuesta) {
			Vtitle = 'Datos Guardados correctamente';
			Vtype = 'success';
			sweetalert(Vtitle, Vtype);
			$('#cardListestable').hide();
			$('#cardRegestable').hide();
			$('#cardLineaProc').show();
			$('#frmLineaAdd').trigger("reset");
			$('#modalLineaAdd').modal('hide');
			tblLineaEstable.ajax.reload();

		});
	});

});

listLineaEstablec = function (ccliente, COD_ESTABLE) {

	tblLineaEstable = $('#tblLineaEstable').DataTable({
		"processing": true,
		"bDestroy": true,
		"stateSave": true,
		"bJQueryUI": true,
		"scrollY": "500px",
		"scrollX": true,
		'AutoWidth': true,
		"paging": false,
		"info": true,
		"filter": true,
		"ordering": false,
		"responsive": false,
		"select": true,
		//'fixedColumns':{
		//  'leftColumns': false,// Fijo primera columna
		//  'rightColumns':1
		//},
		//'lengthMenu'  : [[10, 20, 30, -1], [10, 20, 30, "Todo"]],
		'ajax': {
			"url": baseurl + "at/ctrlprov/mantenimiento/cclientes/getbuscarlineaxestablec",
			"type": "POST",
			"data": function (d) {
				d.cliente = ccliente;
				d.cod_estable = COD_ESTABLE;
			},
			dataSrc: ''
		},
		'columns': [
			{data: 'CLINEAPROCESOCLIENTE', "class": "col-xxs"},

			{data: 'DLINEACLIENTEE', "class": "col-xxs"},
		],

	});
	// Enumeracion
	tblLineaEstable.on('order.dt search.dt', function () {
		tblLineaEstable.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
			cell.innerHTML = i + 1;
		});
	}).draw();
};

