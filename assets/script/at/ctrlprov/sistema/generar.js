/*!
 *
 * @version 1.0.0
 */

const objGenerar = {};

$(function() {

	/**
	 * Recursos del modulo de sistema
	 */
	objGenerar.recursos = function() {
		$.ajax({
			url: baseurl + 'at/ctrlprov/csistema/recursos',
			method: 'POST',
			data: {},
			dataType: 'json',
			beforeSend: function() {}
		}).done(function(res) {
			objGenerar.imprimirArea(res.areas);
			objGenerar.imprimirServicio(res.servicios);
		});
	};

	/**
	 * @param areas
	 */
	objGenerar.imprimirArea = function(areas) {
		let opciones = '<option value="" >::Elegir</option>';
		if (areas && Array.isArray(areas)) {
			areas.forEach(function(area) {
				opciones += '<option value="' + area.CAREA + '" >' + area.DAREA + '</option>';
			});
		}
		$('#sistema_area_id').html(opciones);
	};

	/**
	 * @param sistemas
	 */
	objGenerar.imprimirServicio = function(sistemas) {
		let opciones = '<option value="" >::Elegir</option>';
		if (sistemas && Array.isArray(sistemas)) {
			sistemas.forEach(function(sistema) {
				opciones += '<option value="' + sistema.CSERVICIO + '" >' + sistema.DSERVICIO + '</option>';
			});
		}
		$('#sistema_servicio_id').html(opciones);
	};

	/**
	 * Guarda el sistema
	 */
	objGenerar.guardar = function() {
		const boton = $('#btnGuardarSistema');
		$.ajax({
			url: baseurl + 'at/ctrlprov/csistema/generar',
			method: 'POST',
			data: $('#frmRegSistema').serialize(),
			dataType: 'json',
			beforeSend: function() {
				objPrincipal.botonCargando(boton);
			}
		}).done(function(res) {
			objPrincipal.notify('success', res.message);
			$('#btnBuscar').click();
			$('#modalGenerarSistema').modal('hide');
		}).fail(function(jhxr) {
			const message = (jhxr && jhxr.responseJSON && jhxr.responseJSON.message)
				? jhxr.responseJSON.message
				: 'Error en el proceso de ejecución.';
			objPrincipal.notify('error', message);
		}).always(function() {
			objPrincipal.liberarBoton(boton);
		});
	};

	/**
	 * @param id
	 * @param descripcion
	 * @param area_id
	 * @param servicio_id
	 * @param estado
	 */
	objGenerar.imprimirDatos = function(id, descripcion, area_id, servicio_id, estado) {
		$('#sistema_id').val(id);
		$('#sistema_descripcion').val(descripcion);
		$('#sistema_area_id').val(area_id);
		$('#sistema_servicio_id').val(servicio_id);
		$('#sistema_estado').val(estado).change();
	};

});

$(document).ready(function() {

	objGenerar.recursos();

	$('#btnGuardarSistema').click(objGenerar.guardar);

	$('#modalGenerarSistema').on('hidden.bs.modal', function () {
		// Se limpian los datos
		objGenerar.imprimirDatos(null, '', null, null, 'A');
	})

});
