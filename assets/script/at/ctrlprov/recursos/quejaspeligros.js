/*!
 *
 * @version 1.0.0
 */

const objQuejaPeligro = {};

$(function() {

	objQuejaPeligro.subirArchivo = function() {
		const form = $('form#frmGeneral');
		const datos = new FormData(form[0]);
		const botonGuardar = $('#btnSubirArchivos');
		$.ajax({
			url: BASE_URL + 'at/ctrlprov/inspctrolprov/cquejapeligros/cargar',
			method: 'POST',
			data: datos,
			dataType: 'json',
			cache: false,
			contentType: false,
			processData: false,
			beforeSend: function () {
				objPrincipal.botonCargando(botonGuardar);
			}
		}).done(function (response) {
			objPrincipal.notify('success', 'Archivo cargados correctamente.');
			objQuejaPeligro.descargarPeligroQuejas();
		}).fail(function (jqxhr) {
			sweetalert('Error en el proceso de ejecución', 'error');
		}).always(function () {
			objPrincipal.liberarBoton(botonGuardar);
		});
	};

	objQuejaPeligro.descargarPeligroQuejas = function() {
		$.ajax({
			method: 'post',
			url: baseurl + 'at/ctrlprov/inspctrolprov/cquejapeligros/descargar_peligro',
			dataType: 'json',
			async: true,
			data: {},
			beforeSend: function () {}
		}).done(function (resp) {
			var show = false;
			var nombre = 'Archivo...';
			if (resp.archivo) {
				nombre = resp.nombre;
				show = true;
			}
			$('#txtPeligro').html(nombre);
			$('.btm-eliminar-archivo').each(function() {
				const boton = $(this);
				const id = parseInt($(this).data('id'));
				if (id === 569) {
					if (show) {
						boton.show();
					} else {
						boton.hide();
					}
				}
			});
		}).fail(function (jqxhr) {
			const message = (jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la petición HTTP.';
			sweetalert(message, 'error');
		});
		$.ajax({
			method: 'post',
			url: baseurl + 'at/ctrlprov/inspctrolprov/cquejapeligros/descargar_queja',
			dataType: 'json',
			async: true,
			data: {},
			beforeSend: function () {}
		}).done(function (resp) {
			var show = false;
			var nombre = 'Archivo...';
			if (resp.archivo) {
				nombre = resp.nombre;
				show = true;
			}
			$('#txtQuejas').html(nombre);
			$('.btm-eliminar-archivo').each(function() {
				const boton = $(this);
				const id = parseInt($(this).data('id'));
				if (id === 568) {
					if (show) {
						boton.show();
					} else {
						boton.hide();
					}
				}
			});
		}).fail(function (jqxhr) {
			const message = (jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la petición HTTP.';
			sweetalert(message, 'error');
		});
	}

	objQuejaPeligro.eliminarArchivo = function() {
		Swal.fire({
			type: 'warning',
			title: 'Eliminar archivo',
			text: '¿Estas seguro(a) de eliminar?',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Si',
			cancelButtonText: 'Cancelar',
		}).then((result) => {
			if (result.value) {
				const boton = $(this);
				const id = parseInt(boton.data('id'));
				$.ajax({
					method: 'post',
					url: baseurl + 'at/ctrlprov/inspctrolprov/cquejapeligros/eliminar',
					dataType: 'json',
					async: true,
					data: {
						id: id
					},
					beforeSend: function () {
					}
				}).done(function (resp) {
					objQuejaPeligro.descargarPeligroQuejas();
				}).fail(function (jqxhr) {
					const message = (jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la petición HTTP.';
					sweetalert(message, 'error');
				});
			}
		});
	};

});

$(document).ready(function() {

	objQuejaPeligro.descargarPeligroQuejas();

	$('#btnSubirArchivos').click(objQuejaPeligro.subirArchivo);

	$(document).on('click', '.btm-eliminar-archivo', objQuejaPeligro.eliminarArchivo);

});
