/*!
 *
 * @version 1.0.0
 */

const objCLLAsociar = {};

$(function() {

	/**
	 * Agrega un nuevo cliente
	 */
	objCLLAsociar.agregar = function() {
		const tabla = $('#tblAsociar tbody');
		const position = tabla.find('tr').length;
		tabla.append(objCLLAsociar._agregar(position));
		objCLLAsociar.agregarBusqueda($(document.getElementById('checklist_asociado_id[' + position + ']')));
	};

	/**
	 * MEtodo para agregar un nuevo cliente
	 * @param position
	 * @returns {string}
	 */
	objCLLAsociar._agregar = function(position) {
		let fila = '<tr data-position="' + position + '" >';
		fila += '<td class="text-left" >';
		fila += '<select class="custom-select" id="checklist_asociado_id[' + position + ']" name="checklist_asociado_id[' + position + ']" ></select>';
		fila += '</td>';
		fila += '<td class="text-left" >';
		fila += '<buttton type="button" role="button" class="btn btn-danger btn-agregar-cliente" >';
		fila += '<i class="fa fa-save" ></i> Guardar';
		fila += '</buttton>';
		fila += '</td>';
		fila += '<td class="text-left" >';
		fila += '<buttton type="button" role="button" class="btn btn-secondary btn-quitar-cliente" >';
		fila += '<i class="fa fa-trash" ></i> Eliminar';
		fila += '</buttton>';
		fila += '</td>';
		fila += '</tr>';
		return fila;
	};

	/**
	 * MEtodo para agregar un nuevo cliente
	 * @param position
	 * @param idCliente
	 * @param cliente
	 * @returns {string}
	 */
	objCLLAsociar.imprimirLista = function(position, idCliente, cliente) {
		let fila = '<tr data-position="' + position + '" data-id="' + idCliente + '" >';
		fila += '<td class="text-left" >';
		fila += '<input type="text" class="form-control" maxlength="0" readonly value="' + cliente + '" >';
		fila += '</td>';
		fila += '<td class="text-left" colspan="2" >';
		fila += '<buttton type="button" role="button" class="btn btn-secondary btn-eliminar-cliente" >';
		fila += '<i class="fa fa-trash" ></i> Eliminar';
		fila += '</buttton>';
		fila += '</td>';
		fila += '</tr>';
		return fila;
	};

	objCLLAsociar.agregarBusqueda = function(objDOM) {
		objDOM.select2({
			ajax: {
				url: baseurl + 'at/ctrlprov/checklist/casociar/autocompletado',
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						busqueda: params.term,
					};
				},
				processResults: function (data) {
					return {results: data.items};
				},
				cache: true
			},
			escapeMarkup: function (markup) {
				return markup;
			}, // let our custom formatter work
			minimumInputLength: 0,
			// Aqi se muestran todos los resultados de la busqueda
			templateResult: function (res) {
				return res.text;
			},
			templateSelection: function (res) {
				if (res.id !== "") {
					if (typeof __elegirCliente == "function") {
						__elegirCliente(res);
					}
					return res.text;
				} else {
					return 'Elegir';
				}
			},
			placeholder: "Search",
			allowClear: true,
			width: '100%',
			dropdownParent: $('#asociadoCheckList'),
		});
	};

	/**
	 * Guarda el cliente asociado
	 */
	objCLLAsociar.guardar = function() {
		const boton = $(this);
		const fila = boton.parents('tr');
		const position = fila.data('position');
		const idCliente = document.getElementById('checklist_asociado_id[' + position + ']').value;
		if (!idCliente) {
			objPrincipal.notify('error', 'Debes elegir un cliente');
		} else {
			$.ajax({
				url: baseurl + 'at/ctrlprov/checklist/casociar/guardar',
				method: 'POST',
				data: {
					idCheckList: $('#checklist_id').val(),
					idCliente: idCliente,
				},
				dataType: 'json',
				beforeSend: function() {
					objPrincipal.botonCargando(boton);
				}
			}).done(function() {
				objCLLAsociar.listar();
			}).fail(function(jhxr) {
				const message = (jhxr && jhxr.responseJSON && jhxr.responseJSON.message)
					? jhxr.responseJSON.message
					: 'Error en el proceso de ejecución.';
				objPrincipal.notify('error', message);
			}).always(function() {
				objPrincipal.liberarBoton(boton);
			});
		}
	};

	objCLLAsociar.listar = function() {
		$.ajax({
			url: baseurl + 'at/ctrlprov/checklist/casociar/listar',
			method: 'POST',
			data: {
				idCheckList: $('#checklist_id').val(),
			},
			dataType: 'json',
			beforeSend: function() {}
		}).done(function(res) {
			const tabla = $('#tblAsociar tbody');
			let position = tabla.find('tr').length;
			let filas = '';
			if (res.items && Array.isArray(res.items)) {
				res.items.forEach(function(item) {
					filas += objCLLAsociar.imprimirLista(position, item.CCLIENTE, item.NRUC + ' ' + item.DRAZONSOCIAL);
					++position;
				});
			}
			tabla.html(filas);
		}).fail(function(jhxr) {
			const message = (jhxr && jhxr.responseJSON && jhxr.responseJSON.message)
				? jhxr.responseJSON.message
				: 'Error en el proceso de ejecución.';
			objPrincipal.notify('error', message);
		});
	};

	objCLLAsociar.quitar = function() {
		const boton = $(this);
		const fila = boton.parents('tr');
		fila.hide();
	};

	/**
	 * Elimina a los clientes
	 */
	objCLLAsociar.eliminar = function() {
		const boton = $(this);
		const fila = boton.parents('tr');
		const id = fila.data('id');
		$.ajax({
			url: baseurl + 'at/ctrlprov/checklist/casociar/eliminar',
			method: 'POST',
			data: {
				idCheckList: $('#checklist_id').val(),
				idCliente: id
			},
			dataType: 'json',
			beforeSend: function() {}
		}).done(function(res) {
			objCLLAsociar.listar();
		}).fail(function(jhxr) {
			const message = (jhxr && jhxr.responseJSON && jhxr.responseJSON.message)
				? jhxr.responseJSON.message
				: 'Error en el proceso de ejecución.';
			objPrincipal.notify('error', message);
		});
	};

});

$(document).ready(function() {

	$('#btnAsociar').click(objCLLAsociar.agregar);

	$('#asociadoCheckList').on('show.bs.modal', function () {
		if (parseInt($('#checklist_id').val()) <= 0) {
			$('#asociadoCheckList').modal('hide');
		} else {
			objCLLAsociar.listar();
		}
	});

	$(document).on('click', '.btn-agregar-cliente', objCLLAsociar.guardar);

	$(document).on('click', '.btn-quitar-cliente', objCLLAsociar.quitar);

	$(document).on('click', '.btn-eliminar-cliente', objCLLAsociar.eliminar);

});

function __elegirCliente(datos)
{
	// console.log(datos);
}
