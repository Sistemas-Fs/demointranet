/*!
 *
 * @version 1.0.0
 */

const objResumen = {};

$(function () {

	objResumen.guardar = function () {
		const form = $('#frmResumen');
		const datos = new FormData(form[0]);
		datos.append('cauditoria', $('#cauditoria').val());
		datos.append('fservicio', $('#fservicio').val());
		datos.append('resumen_informe_queja', $('#resumen_informe_queja').is(':checked'));
		const boton = $('.btnGuardarResumen');
		$.ajax({
			url: BASE_URL + 'at/ctrlprov/inspctrolprov/cresumen/guardar',
			method: 'POST',
			data: datos,
			dataType: 'json',
			cache: false,
			contentType: false,
			processData: false,
			beforeSend: function () {
				objPrincipal.botonCargando(boton);
			}
		}).done(function (res) {
			objPrincipal.notify('success', res.message);
		}).fail(function (jqxhr) {
			const message = (jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la petición HTTP.';
			sweetalert(message, 'error');
		}).always(function() {
			objPrincipal.liberarBoton(boton);
		});
	};

	objResumen.generarNroInforme = function () {
		const boton = $('#btnGenerarNroInforme');
		$.ajax({
			url: BASE_URL + 'at/ctrlprov/inspctrolprov/cresumen/nro_informe',
			method: 'POST',
			data: {
				cauditoria: $('#cauditoria').val(),
				fservicio: $('#fservicio').val(),
			},
			dataType: 'json',
			beforeSend: function () {
				objPrincipal.botonCargando(boton);
			}
		}).done(function (res) {
			$('#resumen_nro_informe').val(res.data);
		}).fail(function (jqxhr) {
			const message = (jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la petición HTTP.';
			sweetalert(message, 'error');
		}).always(function() {
			objPrincipal.liberarBoton(boton);
		});
	};

	objResumen.generarInforme = function() {
		const boton = $(this);
		Swal.fire({
			type: 'warning',
			title: 'Generar informe',
			text: 'Seguro(a) de generar el informe?',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Si',
			cancelButtonText: 'Cancelar',
		}).then((result) => {
			if (result.value) {
				$.ajax({
					url: BASE_URL + 'at/ctrlprov/inspctrolprov/cresumen/generar_informe',
					method: 'POST',
					data: {
						cauditoria: $('#cauditoria').val(),
						fservicio: $('#fservicio').val(),
					},
					dataType: 'json',
					beforeSend: function () {
						objPrincipal.botonCargando(boton);
					}
				}).done(function (resp) {
					objPrincipal.notify('success', resp.message);
					$.ajax({
						url: BASE_URL + 'at/ctrlprov/ccons_insp/close_download',
						method: 'POST',
						data: {
							codigo: $('#cauditoria').val(),
							fecha: $('#fservicio').val()
						},
						dataType: 'json',
					}).done(function(resp) {
						const download = window.open(BASE_URL + 'FTPfileserver/Archivos/' + resp.data.DUBICACIONFILESERVERPDF, '_blank');
						if (!download) {
							objPrincipal.alert('warning', 'Habilite la ventana emergente de su navegador.');
						}
						download.focus();
					}).always(function () {
						objPrincipal.liberarBoton(boton);
					});
				}).fail(function (jqxhr) {
					const message = (jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la petición HTTP.';
					objPrincipal.alert('error', message);
					objPrincipal.liberarBoton(boton);
				});
			}
		});
	};

	objResumen.cierreInspeccion = function() {
		const boton = $(this);
		Swal.fire({
			type: 'warning',
			title: 'Cierre del informe',
			text: 'Seguro(a) de cerrar el informe?',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Si',
			cancelButtonText: 'Cancelar',
		}).then((result) => {
			if (result.value) {
				$.ajax({
					url: BASE_URL + 'at/ctrlprov/inspctrolprov/cinspeccion/cerrar_informe',
					method: 'POST',
					data: {
						cauditoria: $('#cauditoria').val(),
						fservicio: $('#fservicio').val(),
					},
					dataType: 'json',
					beforeSend: function () {
						objPrincipal.botonCargando(boton);
					}
				}).done(function (resp) {
					window.close();
				}).fail(function (jqxhr) {
					const message = (jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la petición HTTP.';
					sweetalert(message, 'error');
				}).always(function () {
					objPrincipal.liberarBoton(boton);
				});
			}
		});
	};

	objResumen.subirArchivo = function() {
		const form = $('form#frmResumen');
		const datos = new FormData(form[0]);
		datos.append('cauditoria', $('#cauditoria').val());
		datos.append('fservicio', $('#fservicio').val());
		const botonGuardar = $('#btnCargarArchivoAC');
		$.ajax({
			url: BASE_URL + 'at/ctrlprov/inspctrolprov/cresumen/guardar_archivo',
			method: 'POST',
			data: datos,
			dataType: 'json',
			cache: false,
			contentType: false,
			processData: false,
			beforeSend: function () {
				objPrincipal.botonCargando(botonGuardar);
			}
		}).done(function (response) {
			objPrincipal.notify('sucess', response.message);
		}).fail(function (jqxhr) {
			sweetalert('Error en el proceso de ejecución', 'error');
		}).always(function () {
			objPrincipal.liberarBoton(botonGuardar);
		});
	};

	objResumen.guardarFecha = function() {
		const boton = $('#btnGenerarFecha');
		$.ajax({
			url: BASE_URL + 'at/ctrlprov/inspctrolprov/cresumen/generar_fecha',
			method: 'POST',
			data: {
				cauditoria: $('#cauditoria').val(),
				fservicio: $('#fservicio').val(),
			},
			dataType: 'json',
			beforeSend: function () {
				objPrincipal.botonCargando(boton);
			}
		}).done(function (response) {
			$('#resumen_fecha_terminada').val(moment(response.data, 'YYYY-MM-DD').format('DD/MM/YYYY'));
			objPrincipal.notify('success', response.message);
		}).fail(function (jqxhr) {
			const message = (jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la petición HTTP.';
			sweetalert(message, 'error');
		}).always(function () {
			objPrincipal.liberarBoton(boton);
		});
	};

	objResumen.recargarPagina = function() {
		location.href = window.location;
	};

});

$(document).ready(function () {

	$('.btnGuardarResumen').click(objResumen.guardar);

	$('#btnGenerarNroInforme').click(objResumen.generarNroInforme);

	$('#btnGenerarInforme').click(objResumen.generarInforme);

	$('#btnCierreInspeccion').click(objResumen.cierreInspeccion);

	$('#resumen_informe').change(objResumen.subirArchivo);

	$('#btnGenerarFecha').click(objResumen.guardarFecha);

	$('#btnReAperturaInspeccion').click(objResumen.reAperturaInspeccion);

});
