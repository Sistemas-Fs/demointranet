/**!
 *
 * @version 1.0.0
 */

const objResumenContacto = {};

$(function() {

	/**
	 * Lista de contactos
	 */
	objResumenContacto.lista = function() {
		$.ajax({
			method: 'post',
			url: baseurl + "at/ctrlprov/inspctrolprov/cresumen_contacto/lista",
			dataType: "JSON",
			data: {
				cauditoria: $('#cauditoria').val(),
				fservicio: $('#fservicio').val(),
			},
		}).done(function(res) {
			const tabla = $('#tblResumenContactos > tbody');
			tabla.html('');
			if (res.items) {
				res.items.forEach(function(item, pos) {
					const row = objResumenContacto._item(
						pos,
						item.ccliente,
						item.cliente,
						item.dapepat,
						item.dapemat,
						item.dnombre,
						item.dcargocontacto,
						item.dmail,
						item.dtelefono,
						item.ccontacto,
						1
					);
					tabla.append(row);
				});
			}
		}).fail(function() {
			//
		});
	};

	/**
	 * Nuevo item
	 */
	objResumenContacto.agregar = function(tipo) {
		const tabla = $('#tblResumenContactos > tbody');
		const posicion = tabla.find('tr').length;
		const item = objResumenContacto._item(posicion, null, '', '', '', '', '', '', '', 0, tipo);
		tabla.append(item);
		if (tipo === 2) {
			objResumenContacto.initBusqueda($(document.getElementById('res_contact_id[' + posicion + ']')));
		}
		if (tipo === 1) {
			objResumenContacto.initBusquedaCliente($(document.getElementById('res_contact_ccliente[' + posicion + ']')));
		}
	};

	/**
	 *
	 * @param posicion
	 * @param ccliente
	 * @param dcliente
	 * @param apepat
	 * @param apemat
	 * @param nombres
	 * @param cargo
	 * @param email
	 * @param telefono
	 * @param ccontacto
	 * @param tipo
	 * @returns {string}
	 * @private
	 */
	objResumenContacto._item = function(posicion, ccliente, dcliente, apepat, apemat, nombres, cargo, email, telefono, ccontacto, tipo) {
		ccontacto = (typeof ccontacto == "undefined") ? 0 : ccontacto;
		apepat = (typeof apepat != "undefined" && apepat) ? apepat : '';
		apemat = (typeof apemat != "undefined" && apemat) ? apemat : '';
		nombres = (typeof nombres != "undefined" && nombres) ? nombres : '';
		cargo = (typeof cargo != "undefined" && cargo) ? cargo : '';
		email = (typeof email != "undefined" && email) ? email : '';
		telefono = (typeof telefono != "undefined" && telefono) ? telefono : '';
		tipo = (typeof tipo == "undefined") ? 1 : tipo;
		let item = '<tr data-posicion="' + posicion + '" >';
		if (tipo === 1) {
			item += '<td class="text-left" style="width: 320px; min-width: 320px" >';
			item += '<select class="form-control form-control-sm" id="res_contact_ccliente[' + posicion + ']" >';
			if (ccliente) {
				item += '<option value="' + ccliente + '" >' + dcliente + '</option>';
			}
			item += '</select>';
			item += '</td>';
			item += '<td class="text-left" >';
			item += '<input type="text" class="form-control form-control-sm" id="res_contact_apepat[' + posicion + ']" value="' + apepat + '" >';
			item += '<input type="hidden" class="d-none" id="res_contact_id[' + posicion + ']" value="' + ccontacto + '" />';
			item += '</td>';
			item += '<td class="text-left" >';
			item += '<input type="text" class="form-control form-control-sm" id="res_contact_apemat[' + posicion + ']" value="' + apemat + '" >';
			item += '<input type="hidden" class="d-none" id="res_contact_id[' + posicion + ']" value="' + ccontacto + '" />';
			item += '</td>';
			item += '<td class="text-left" >';
			item += '<input type="text" class="form-control form-control-sm" id="res_contact_nombres[' + posicion + ']" value="' + nombres + '" >';
			item += '<input type="hidden" class="d-none" id="res_contact_id[' + posicion + ']" value="' + ccontacto + '" />';
			item += '</td>';
			item += '<td class="text-left" >';
			item += '<input type="text" class="form-control form-control-sm" id="res_contact_cargo[' + posicion + ']" value="' + cargo + '" >';
			item += '</td>';
			item += '<td class="text-left" >';
			item += '<input type="text" class="form-control form-control-sm" id="res_contact_email[' + posicion + ']" value="' + email + '" >';
			item += '</td>';
			item += '<td class="text-left" >';
			item += '<input type="text" class="form-control form-control-sm" id="res_contact_telefonos[' + posicion + ']" value="' + telefono + '" >';
			item += '</td>';
		} else {
			item += '<td class="text-left" colspan="7" >';
			item += '<select class="custom-select" id="res_contact_id[' + posicion + ']" style="width: 100% !important;" ></select>';
			item += '</td>';
		}
		item += '<td class="text-left" style="width: 210px" >';
		if (parseInt($('#inspeccion_abierto').val()) === 1) {
			item += '<div class="btn-group btn-group-sm" >';
			item += '<button type="button" role="button" class="btn btn-success btn-sm mr-2 btn-res-contacto-guardar" ><i class="fa fa-save" ></i> Guardar</button>';
			item += '<button type="button" role="button" class="btn btn-secondary btn-sm btn-res-contacto-eliminar" ><i class="fa fa-trash" ></i> Eliminar</button>';
			item += '<input type="hidden" class="d-none" id="res_contact_tipo[' + posicion + ']" value="' + tipo + '" />';
			item += '</div>';
		}
		item += '</td>';
		item += '</tr>';
		return item;
	};

	objResumenContacto.initBusquedaCliente = function(objDOM) {
		objDOM.select2({
			ajax: {
				url: BASE_URL + 'at/ctrlprov/inspctrolprov/cresumen_contacto/autocompletado_clientes',
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						search: params.term,
						cauditoria: $('#cauditoria').val(),
					};
				},
				processResults: function (data) {
					return {results: data.items};
				},
				cache: true
			},
			escapeMarkup: function (markup) {
				return markup;
			}, // let our custom formatter work
			minimumInputLength: 0,
			// Aqi se muestran todos los resultados de la busqueda
			templateResult: function (state) {
				return state.text;
			},
			templateSelection: function (state) {
				if (state.id !== "") {
					return state.text;
				} else {
					return 'Elegir empresa';
				}
			},
			placeholder: "Elegir empresa",
			allowClear: true,
			width: '100%',
			dropdownParent: null,
		});
	};

	objResumenContacto.initBusqueda = function(objDOM) {
		objDOM.select2({
			ajax: {
				url: BASE_URL + 'at/ctrlprov/inspctrolprov/cresumen_contacto/autocompletado',
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						search: params.term,
						cauditoria: $('#cauditoria').val(),
						fservicio: $('#fservicio').val(),
					};
				},
				processResults: function (data) {
					return {results: data.items};
				},
				cache: true
			},
			escapeMarkup: function (markup) {
				return markup;
			}, // let our custom formatter work
			minimumInputLength: 0,
			// Aqi se muestran todos los resultados de la busqueda
			templateResult: function (state) {
				const dcargocontacto = (state.dcargocontacto) ? state.dcargocontacto : '';
				const dmail = (state.dmail) ? state.dmail : '';
				const dtelefono = (state.dtelefono) ? state.dtelefono : '';
				let result = '<table class="" >';
				result += '<tr>';
				result += '<td style="width: 330px; padding-right: 5px" >' + state.cliente + '</td>';
				result += '<td style="width: 220px; padding-right: 5px" >' + state.text + '</td>';
				result += '<td style="width: 200px; padding-right: 5px" >' + dcargocontacto + '</td>';
				result += '<td style="width: 150px; padding-right: 5px" >' + dmail + '</td>';
				result += '<td style="width: 200px" >' + dtelefono + '</td>';
				result += '<tr/>';
				result += '<table>';
				return result;
			},
			templateSelection: function (state) {
				if (state.id !== "") {
					const dcargocontacto = (state.dcargocontacto) ? state.dcargocontacto : '';
					const dmail = (state.dmail) ? state.dmail : '';
					const dtelefono = (state.dtelefono) ? state.dtelefono : '';
					let result = '<table class="table-sm" >';
					result += '<tr>';
					result += '<td style="width: 330px; padding-right: 5px; border: none" >' + state.cliente + '</td>';
					result += '<td style="width: 220px; padding-right: 5px; border: none" >' + state.text + '</td>';
					result += '<td style="width: 200px; padding-right: 5px; border: none" >' + dcargocontacto + '</td>';
					result += '<td style="width: 150px; padding-right: 5px; border: none" >' + dmail + '</td>';
					result += '<td style="width: 200px; border: none" >' + dtelefono + '</td>';
					result += '<tr/>';
					result += '<table>';
					return result;
				} else {
					return 'Elegir contacto';
				}
			},
			placeholder: "Elegir contacto",
			allowClear: true,
			width: '100%',
			dropdownParent: null,
		});
	};

	/**
	 * Guardar contacto
	 */
	objResumenContacto.guardar = function() {
		const boton = $(this);
		const row = boton.parents('tr');
		const posicion = row.data('posicion');

		const id = (document.getElementById('res_contact_id[' + posicion + ']'))
			? document.getElementById('res_contact_id[' + posicion + ']').value
			: '';
		const apepat = (document.getElementById('res_contact_apepat[' + posicion + ']'))
			? document.getElementById('res_contact_apepat[' + posicion + ']').value
			: '';
		const apemat = (document.getElementById('res_contact_apemat[' + posicion + ']'))
			? document.getElementById('res_contact_apemat[' + posicion + ']').value
			: '';
		const nombres = (document.getElementById('res_contact_nombres[' + posicion + ']'))
			? document.getElementById('res_contact_nombres[' + posicion + ']').value
			: '';
		const cargo = (document.getElementById('res_contact_cargo[' + posicion + ']'))
			? document.getElementById('res_contact_cargo[' + posicion + ']').value
			: '';
		const email = (document.getElementById('res_contact_email[' + posicion + ']'))
			? document.getElementById('res_contact_email[' + posicion + ']').value
			: '';
		const telefonos = (document.getElementById('res_contact_telefonos[' + posicion + ']'))
			? document.getElementById('res_contact_telefonos[' + posicion + ']').value
			: '';
		const tipo = (document.getElementById('res_contact_tipo[' + posicion + ']'))
			? document.getElementById('res_contact_tipo[' + posicion + ']').value
			: 1;
		const ccliente = (document.getElementById('res_contact_ccliente[' + posicion + ']'))
			? document.getElementById('res_contact_ccliente[' + posicion + ']').value
			: 1;

		$.ajax({
			url: BASE_URL + 'at/ctrlprov/inspctrolprov/cresumen_contacto/guardar',
			method: 'POST',
			data: {
				cauditoria: $('#cauditoria').val(),
				fservicio: $('#fservicio').val(),
				apepat: apepat,
				apemat: apemat,
				nombres: nombres,
				cargo: cargo,
				email: email,
				telefonos: telefonos,
				id: id,
				tipo: tipo,
				ccliente: ccliente,
			},
			dataType: 'json',
			beforeSend: function() {
				objPrincipal.botonCargando(boton);
			}
		}).done(function(res) {
			objResumenContacto.lista();
			objPrincipal.notify('success', res.message);
		}).fail(function() {
			objPrincipal.liberarBoton(boton);
		});
	};

	/**
	 * Eliminar contacto
	 */
	objResumenContacto.eliminar = function() {
		const boton = $(this);
		const row = boton.parents('tr');
		const posicion = row.data('posicion');
		const contact_id = (document.getElementById('res_contact_id[' + posicion + ']'))
			? document.getElementById('res_contact_id[' + posicion + ']').value
			: '';
		if (contact_id <= 0) {
			row.hide();
		} else {
			Swal.fire({
				type: 'warning',
				title: 'Eliminar responsable',
				text: '¿Estas seguro(a) de eliminar?',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Si',
				cancelButtonText: 'Cancelar',
			}).then((result) => {
				if (result.value) {
					$.ajax({
						url: BASE_URL + 'at/ctrlprov/inspctrolprov/cresumen_contacto/eliminar',
						method: 'POST',
						data: {
							cauditoria: $('#cauditoria').val(),
							fservicio: $('#fservicio').val(),
							contact_id: contact_id
						},
						dataType: 'json',
						beforeSend: function () {
							objPrincipal.botonCargando(boton);
						}
					}).done(function (res) {
						objResumenContacto.lista();
						objPrincipal.notify('success', res.message);
					}).fail(function () {
						objPrincipal.liberarBoton(boton);
					});
				}
			});
		}
	};

});

$(document).ready(function() {

	objResumenContacto.lista();

	$('#btnAgregarContacto').click(function() {
		objResumenContacto.agregar(1);
	});

	$('#btnBuscarContacto').click(function() {
		objResumenContacto.agregar(2);
	});

	$(document).on('click', '.btn-res-contacto-guardar', objResumenContacto.guardar);

	$(document).on('click', '.btn-res-contacto-eliminar', objResumenContacto.eliminar);

});
