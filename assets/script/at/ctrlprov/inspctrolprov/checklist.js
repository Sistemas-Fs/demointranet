/*!
 *
 * @version 1.0.0
 */

const objCheckList = {};

$(function () {

	/**
	 * @type {void|jQuery|HTMLElement|*}
	 */
	objCheckList.modal = $('#ModalItemList');

	/**
	 * Lista de items de checklist
	 * @type {*[]}
	 */
	objCheckList.items = [];

	/**
	 * @param varcauditoria
	 * @param varfservicio
	 */
	objCheckList.lista = function (varcauditoria, varfservicio) {
		otblinspeccionprov = $('#tblinspeccionprov').DataTable({
			'responsive': false,
			'bJQueryUI': true,
			'scrollY': '600px',
			'scrollX': true,
			'paging': false,
			'processing': true,
			'bDestroy': true,
			'AutoWidth': true,
			'info': true,
			'filter': true,
			'ordering': false,
			'stateSave': true,
			'ajax': {
				"url": baseurl + "at/ctrlprov/inspctrolprov/cchecklist/lista",
				"type": "POST",
				"data": function (d) {
					d.cauditoriainspeccion = varcauditoria;
					d.fservicio = varfservicio;
				},
				dataSrc: function (data) {
					var totalNvalormax = 0;
					var totalNoconformidad = 0;
					objCheckList.items = [];
					data.forEach(function(row) {
						if (String(row.srequisito).toUpperCase() === 'H') {
							objCheckList.items.push({ id: row.crequisitochecklist, active: false, data: row });
						}
					});
					return data;
				},
			},
			'columns': [
				{
					"orderable": false,
					render: function (data, type, row) {
						if (String(row.sexcluyente).toUpperCase() === 'S') {
							return '<span class="text-danger" >' + row.dnumeradorpadre + '</span>';
						}
						return row.dnumeradorpadre;
					}
				},
				{
					"orderable": false,
					render: function (data, type, row) {
						if (String(row.sexcluyente).toUpperCase() === 'S') {
							return '<span class="text-danger" >' + row.dnumerador + '</span>';
						}
						return row.dnumerador;
					}
				},
				{
					"orderable": false,
					render: function (data, type, row) {
						let drequisito = String(row.drequisito).replace(/(\r\n|\r|\n)/g, '<br />');
						if (String(row.sexcluyente).toUpperCase() === 'S') {
							return '<span class="text-danger" >' + drequisito + '</span>';
						}
						return drequisito;
					}
				},
				{
					"orderable": false,
					render: function (data, type, row) {
						if (String(row.sexcluyente).toUpperCase() === 'S') {
							return '<span class="text-danger" >' + row.dnormativa + '</span>';
						}
						return row.dnormativa;
					}
				},
				{
					"orderable": false,
					render: function (data, type, row) {
						let nvalormaxrequisito = (row.nvalormaxrequisito) ? parseInt(row.nvalormaxrequisito) : '';
						if (parseFloat(row.nvalormaxrequisito) < 0) {
							nvalormaxrequisito = row.noconformidad;
						}
						if (String(row.sexcluyente).toUpperCase() === 'S') {
							return '<span class="text-danger" >' + nvalormaxrequisito + '</span>';
						}
						return nvalormaxrequisito;
					}
				},
				{
					"orderable": false,
					render: function (data, type, row) {
						let nvalorrequisito = (row.nvalorrequisito) ? parseInt(row.nvalorrequisito) : '';
						if (nvalorrequisito < 0) {
							nvalorrequisito = row.noconformidad;
						}
						if (String(row.sexcluyente).toUpperCase() === 'S') {
							return '<span class="text-danger" >' + nvalorrequisito + '</span>';
						}
						return nvalorrequisito;
					}
				},
				{
					"orderable": false,
					render: function (data, type, row) {
						let tipohallazgo = (row.tipohallazgo) ? row.tipohallazgo : '';
						if (String(row.sexcluyente).toUpperCase() === 'S') {
							return '<span class="text-danger" >' + tipohallazgo + '</span>';
						}
						return tipohallazgo;
					}
				},
				{
					"orderable": false,
					render: function (data, type, row) {
						let dhallazgotext = (row.dhallazgotext) ? row.dhallazgotext : '';
						dhallazgotext = String(dhallazgotext).replace(/(\r\n|\r|\n)/g, '<br />');
						if (String(row.sexcluyente).toUpperCase() === 'S') {
							return '<span class="text-danger" >' + dhallazgotext + '</span>';
						}
						return dhallazgotext;
					}
				},
				{
					responsivePriority: 1, "orderable": false,
					render: function (data, type, row) {
						if (parseInt($('#inspeccion_abierto').val()) === 1 && row.srequisito == 'H') {
							return '<div>' +
								'<button class="btn btn-primary" data-original-title="Registrar" onClick="objCheckList.abrirActual(\'' + row.crequisitochecklist + '\');">' +
								// '<button class="btn btn-primary" data-original-title="Registrar" onClick="objCheckList.abrir(\'' + row.cdetallevalornoconformidad + '\',\'' + row.cdetallevalorrequisito + '\',\'' + row.cchecklist + '\',\'' + row.crequisitochecklist + '\',\'' + row.cauditoriainspeccion + '\',\'' + row.cvalorrequisito + '\',\'' + row.cvalornoconformidad + '\',\'' + row.dhallazgo + '\',\'' + row.dnumerador + '\',\'' + row.drequisito + '\',\'' + row.sexcluyente + '\');">' +
								'<i class="fa fa-edit" data-original-title="Registrar" data-toggle="tooltip"></i>' +
								'</button>' +
								'</div>'
						} else {
							return '';
						}

					}
				}
			],
			"columnDefs": [
				{"targets": [0], "visible": true},
			],
		});

	};

	/**
	 * Recarga el plugin de la lista del checklist
	 */
	objCheckList.refrescarLista = function () {
		$("#tblinspeccionprov").DataTable().ajax.reload(null, false);
	};

	/**
	 * Muestra los datos del checklist
	 * @param cdetallevalornoconformidad
	 * @param cdetallevalorrequisito
	 * @param cchecklist
	 * @param crequisitochecklist
	 * @param cauditoriainspeccion
	 * @param cvalorrequisito
	 * @param cvalornoconformidad
	 * @param dhallazgo
	 * @param dnumerador
	 * @param drequisito
	 * @param sexcluyente
	 */
	objCheckList.abrir = function (cdetallevalornoconformidad, cdetallevalorrequisito, cchecklist, crequisitochecklist, cauditoriainspeccion, cvalorrequisito, cvalornoconformidad, dhallazgo, dnumerador, drequisito, sexcluyente) {
		$("#int_requisito_checklist").val(crequisitochecklist);
		$("#int_codigo_checklist").val(cchecklist);
		$("#int_auditoria_inspeccion").val(cauditoriainspeccion);
		$("#checklist_nro").html(dnumerador);
		drequisito = String(drequisito).replace(/(\r\n|\r|\n)/g, '<br />');
		$("#checklist_drequisito").html(drequisito);
		if (String(sexcluyente).toUpperCase() === "S") {
			$("#checklist_nro").addClass('text-danger');
			$("#checklist_drequisito").addClass('text-danger');
		} else {
			$("#checklist_nro").removeClass('text-danger');
			$("#checklist_drequisito").removeClass('text-danger');
		}

		cdetallevalorrequisito = (cdetallevalorrequisito) ? cdetallevalorrequisito : '001';

		$.ajax({
			method: 'post',
			url: baseurl + "at/ctrlprov/inspctrolprov/cchecklist/valores",
			dataType: "JSON",
			async: true,
			data: {
				"int_valor": cvalorrequisito,
			},
			success: function (result) {
				$('#cmbPuntuacionChecklist').html(result);
				$('#cmbPuntuacionChecklist').val(cdetallevalorrequisito);

			},
			error: function () {
				alert('Error, No se puede autenticar por error ');
			}
		});

		$.ajax({
			method: 'post',
			url: baseurl + "at/ctrlprov/inspctrolprov/cchecklist/criterioHallazgo",
			dataType: "JSON",
			async: true,
			data: {
				cauditoria: $('#cauditoria').val(),
				fservicio: $('#fservicio').val(),
			},
			success: function (result) {
				$('#cmbCriterioHallazgo').html(result);
				$('#cmbCriterioHallazgo').val(cdetallevalornoconformidad);
				objCheckList.elegirCriterioHallazgo();
			},
			error: function () {
				alert('Error, No se puede autenticar por error ');
			}
		});
		$("#txtHallazgo").val(dhallazgo);

		$("#ModalItemList").modal('show');
	};

	/**
	 * Guarda el checklist
	 */
	objCheckList.save = function (tipoBoton) {
		const from = $("#frmChecklist");
		var datos = from.serialize()
			+ '&cauditoria=' + $('#cauditoria').val()
			+ '&fservicio=' + $('#fservicio').val();
		const botonSiguiente = $("#btnGuardarChecklist");
		const botonAtras = $("#btnAnteriorChecklist");
		const botonCerrar = $("#btnCerrarChecklist");
		// Solo si elige un criterio y decide avanzar
		$.ajax({
			type: 'ajax',
			method: 'post',
			url: baseurl + 'at/ctrlprov/inspctrolprov/cchecklist/guardar',
			dataType: 'json',
			async: true,
			data: datos,
			beforeSend: function () {
				botonAtras.prop('disabled', true);
				objPrincipal.botonCargando(botonSiguiente);
				objPrincipal.botonCargando(botonCerrar);
			}
		}).done(function (resp) {
			sweetalert(resp.message, 'success');
			if (tipoBoton === 2) {
				objCheckList.modal.modal('hide');
			} else {
				objCheckList.siguiente();
			}
			$('#resumen_resultado_checklist').val(resp.data.resultado_porcentaje);
			$('#resumen_resultado').val(resp.data.resultado);
		}).fail(function (jqxhr) {
			const message = (jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la petición HTTP.';
			sweetalert(message, 'error');
		}).always(function () {
			botonAtras.prop('disabled', false);
			objPrincipal.liberarBoton(botonSiguiente);
			objPrincipal.liberarBoton(botonCerrar);
		});
	};

	/**
	 * Criterio de hallazgo elegido
	 */
	objCheckList.elegirCriterioHallazgo = function() {
		const valor = $('#cmbCriterioHallazgo option:selected').data('valor');
		$('#criterio_hallazgo_valor').val(valor);
	};

	/**
	 * @param crequisitochecklist
	 */
	objCheckList.abrirActual = function(crequisitochecklist) {
		const index = objCheckList.items.findIndex(function(row) {
			return (row.id === crequisitochecklist);
		});
		objCheckList.items[index].active = true;
		const row = objCheckList.items[index].data;
		objCheckList._abrirActual(row);
	};

	/**
	 * @param row
	 * @private
	 */
	objCheckList._abrirActual = function(row) {
		objCheckList.abrir(
			row.cdetallevalornoconformidad,
			row.cdetallevalorrequisito,
			row.cchecklist,
			row.crequisitochecklist,
			row.cauditoriainspeccion,
			row.cvalorrequisito,
			row.cvalornoconformidad,
			row.dhallazgo,
			row.dnumerador,
			row.drequisito,
			row.sexcluyente,
		);
	};

	/**
	 * Item siguiente
	 */
	objCheckList.siguiente = function() {
		const index = objCheckList.obtenerItemActual();
		const nextIndex = index + 1;
		if (typeof objCheckList.items[nextIndex] === 'undefined') {
			objPrincipal.notify('info', 'No existen mas checklist');
		} else {
			// Se guardan los datos en el item actual
			objCheckList.items[index].data.cdetallevalorrequisito = $('#cmbPuntuacionChecklist').val();
			objCheckList.items[index].data.cdetallevalornoconformidad = $('#cmbCriterioHallazgo').val();
			objCheckList.items[index].data.dhallazgo = $('#txtHallazgo').val();
			// Se pasa al siguiente item
			objCheckList.limpiarItems();
			objCheckList.items[nextIndex].active = true;
			console.log(nextIndex);
			console.log(objCheckList.items[nextIndex].data.dhallazgo);
			objCheckList._abrirActual(objCheckList.items[nextIndex].data);
		}
	};

	/**
	 * Limpia todos los items active
	 */
	objCheckList.limpiarItems = function() {
		objCheckList.items.forEach(function(row) {
			row.active = false;
		});
	};

	/**
	 * @returns {*}
	 */
	objCheckList.obtenerItemActual = function() {
		return objCheckList.items.findIndex(function(row) {
			return (row.active);
		});
	};

	/**
	 * Busca inspeccion anterior
	 */
	objCheckList.anterior = function() {
		const index = objCheckList.obtenerItemActual();
		if (typeof objCheckList.items[index - 1] === 'undefined') {
			objPrincipal.notify('info', 'No existen más checklist');
		} else {
			const itemAnterior = objCheckList.items[index - 1];
			objCheckList.limpiarItems();
			objCheckList.items[index - 1].active = true;
			objCheckList._abrirActual(itemAnterior.data);
		}
	};

});

$(document).ready(function () {

	objCheckList.lista($('#cauditoria').val(), $('#fservicio').val());

	$("#btnGuardarChecklist").click(function() {
		objCheckList.save(1);
	});
	$("#btnCerrarChecklist").click(function() {
		objCheckList.save(2);
	});
	$("#btnAnteriorChecklist").click(objCheckList.anterior);

	$('#cmbCriterioHallazgo').change(function() {
		objCheckList.elegirCriterioHallazgo();
	});

	objCheckList.modal.on('hidden.bs.modal', function () {
		objCheckList.limpiarItems();
		objCheckList.refrescarLista();
		objACC.refrescarLista();
	});

});
