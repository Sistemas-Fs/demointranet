var otblListinspctrlprov, otblinspeccionprov;
var varfdesde = '%', varfhasta = '%';

$(document).ready(function () {
	$('#tabctrlprov a[href="#tabctrlprov-list-tab"]').attr('class', 'disabled');
	$('#tabctrlprov a[href="#tabctrlprov-det-tab"]').attr('class', 'disabled active');

	$('#tabctrlprov a[href="#tabctrlprov-list-tab"]').not('#store-tab.disabled').click(function (event) {
		$('#tabctrlprov a[href="#tabctrlprov-list"]').attr('class', 'active');
		$('#tabctrlprov a[href="#tabctrlprov-det"]').attr('class', '');
		return true;
	});
	$('#tabctrlprov a[href="#tabctrlprov-det-tab"]').not('#bank-tab.disabled').click(function (event) {
		$('#tabctrlprov a[href="#tabctrlprov-det"]').attr('class', 'active');
		$('#tabctrlprov a[href="#tabctrlprov-list"]').attr('class', '');
		return true;
	});

	$('#tabctrlprov a[href="#tabctrlprov-list"]').click(function (event) {
		return false;
	});
	$('#tabctrlprov a[href="#tabctrlprov-det"]').click(function (event) {
		return false;
	});

	$('#txtFDesde,#txtFHasta,#txtFInspeccion').datetimepicker({
		format: 'DD/MM/YYYY',
		daysOfWeekDisabled: [0],
		locale: 'es',
		autoclose: true,
		todayBtn: true
	});
	fechaActual();

	/*LLENADO DE COMBOS*/
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "at/ctrlprov/cinspctrolprov/getcboclieserv",
		dataType: "JSON",
		async: true,
		success: function (result) {
			$('#cboclieserv').html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboclieserv');
		}
	});
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "at/ctrlprov/cinspctrolprov/getcboestado",
		dataType: "JSON",
		async: true,
		success: function (result) {
			$('#cboestado').html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboestado');
		}
	})
	var params = {"sregistro": '%'};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "at/ctrlprov/cinspctrolprov/getcboinspector",
		dataType: "JSON",
		async: true,
		data: params,
		success: function (result) {
			$('#cboinspector').html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboinspector');
		}
	})

	//
	$('#frmCreactrlprov').validate({
		rules: {
			cboregClie: {
				required: true,
			},
			cboregprovclie: {
				required: true,
			},
			cboregestable: {
				required: true,
			},
			cboregareaclie: {
				required: true,
			},
		},
		messages: {
			cboregClie: {
				required: "Por Favor escoja un Cliente"
			},
			cboregprovclie: {
				required: "Por Favor escoja un Proveedor"
			},
			cboregestable: {
				required: "Por Favor escoja un Establecimiento"
			},
			cboregareaclie: {
				required: "Por Favor escoja un Area"
			},
		},
		errorElement: 'span',
		errorPlacement: function (error, element) {
			error.addClass('invalid-feedback');
			element.closest('.form-group').append(error);
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass('is-invalid');
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).removeClass('is-invalid');
		},
		submitHandler: function (form) {
			const botonEvaluar = $('#mbtnGCreactrl');
			var request = $.ajax({
				url: $('#frmCreactrlprov').attr("action"),
				type: $('#frmCreactrlprov').attr("method"),
				data: $('#frmCreactrlprov').serialize(),
				error: function () {
					Vtitle = 'Error en Guardar!!!';
					Vtype = 'error';
					sweetalert(Vtitle, Vtype);
					objPrincipal.liberarBoton(botonEvaluar);
				},
				beforeSend: function () {
					objPrincipal.botonCargando(botonEvaluar);
				}
			});
			request.done(function (respuesta) {
				var posts = JSON.parse(respuesta);

				$.each(posts, function () {
					$('#mhdnregIdinsp').val(this.cauditoriainspeccion);

					Vtitle = 'Inspección Guardada!!!';
					Vtype = 'success';
					sweetalert(Vtitle, Vtype);
					otblListinspctrlprovv.ajax.reload(null, false);
					objPrincipal.liberarBoton(botonEvaluar);
				});
			});
			return false;
		}
	});
	$('#frmRegInsp').validate({
		rules: {},
		messages: {},
		errorElement: 'span',
		errorPlacement: function (error, element) {
			error.addClass('invalid-feedback');
			element.closest('.form-group').append(error);
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass('is-invalid');
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).removeClass('is-invalid');
		},
		submitHandler: function (form) {
			const botonEvaluar = $('#btnRegistrar');
			var request = $.ajax({
				url: $('#frmRegInsp').attr("action"),
				type: $('#frmRegInsp').attr("method"),
				data: $('#frmRegInsp').serialize(),
				error: function () {
					Vtitle = 'Error en Guardar!!!';
					Vtype = 'error';
					sweetalert(Vtitle, Vtype);
					objPrincipal.liberarBoton(botonEvaluar);
				},
				beforeSend: function () {
					objPrincipal.botonCargando(botonEvaluar);
				}
			});
			request.done(function (respuesta) {
				var posts = JSON.parse(respuesta);

				$.each(posts, function () {
					//$('#mhdnregIdinsp').val(this.cauditoriainspeccion);

					Vtitle = 'Inspección Guardada!!!';
					Vtype = 'success';
					sweetalert(Vtitle, Vtype);
					objPrincipal.liberarBoton(botonEvaluar);
				});
			});
			return false;
		}
	});

	descargarPeligroQuejas();

	$(document).on('click', '.option-re-apertura', function() {
		const boton = $(this);
		Swal.fire({
			type: 'warning',
			title: 'Re apertura de la inspección',
			text: 'Seguro(a) de abrir la inspección?',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Si',
			cancelButtonText: 'Cancelar',
		}).then((result) => {
			if (result.value) {
				$.ajax({
					url: BASE_URL + 'at/ctrlprov/inspctrolprov/cinspeccion/reapertura',
					method: 'POST',
					data: {
						cauditoria: $('#cauditoria').val(),
						fservicio: $('#fservicio').val(),
					},
					dataType: 'json',
					beforeSend: function () {
						objPrincipal.botonCargando(boton);
					}
				}).done(function (res) {
					console.log(res);
				}).fail(function (jqxhr) {
					const message = (jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la petición HTTP.';
					sweetalert(message, 'error');
				}).always(function () {
					objPrincipal.liberarBoton(boton);
				});
			}
		});
	});

	$(document).on('click', '.operacion-descargar-acc', function() {
		const boton = $(this);
		objPrincipal.botonCargando(boton);
		$('#acc_cauditoria').val(boton.data('cauditoria'));
		$('#acc_fservicio').val(boton.data('fservicio'));
		const otblAACC = $('#tblAcciónCorrectiva').DataTable({
			"processing"  	: true,
			"bDestroy"    	: true,
			"stateSave"     : true,
			"bJQueryUI"     : true,
			"scrollY"     	: "540px",
			"scrollX"     	: true,
			'AutoWidth'     : true,
			"paging"      	: false,
			"info"        	: true,
			"filter"      	: true,
			"ordering"		: false,
			"responsive"    : false,
			"select"        : true,
			'ajax': {
				"url": BASE_URL + 'at/ctrlprov/inspctrolprov/caccion_correctiva/descargar_archivo',
				"type": "POST",
				"data": function (d) {
					d.cauditoriainspeccion = boton.data('cauditoria');
					d.fservicio = boton.data('fservicio');
				},
				dataSrc: function (data) {
					objPrincipal.liberarBoton(boton);
					$('#modalAccionCorrectiva').modal('show');
					const formACCArchivo = $('#formACCArchivo');
					if (data.data.path_file) {
						$('#download-excel').show();
						$('#btnDownloadAccionCorrectiva').attr('href', data.data.path_file);
						formACCArchivo.hide();
					} else {
						$('#download-excel').hide();
						formACCArchivo.show();
					}
					return data.data.result;
				},
			},
			'columns': [
				{
					"orderable": false,
					render: function (data, type, row) {
						let texto = '';
						if (row.saceptaraccioncorrectiva !== 'S') {
							texto = '<button class="btn btn-primary opcion-editar-acc" data-tipohallazgo="' + row.tipohallazgo + '" data-crequisitochecklist="' + row.crequisitochecklist + '" data-cchecklist="' + row.cchecklist + '" data-cauditoria="' + row.cauditoriainspeccion + '" data-fservicio="' + row.fservicio + '" ><i class="fa fa-edit" ></i></button>';
						}
						return texto;
					}
				},
				{"orderable": false, data: 'dnumerador', targets: 0},
				{
					"orderable": false,
					render: function (data, type, row) {
						return String(row.drequisito).replace(/(\r\n|\r|\n)/g, '<br />');
					},
					"class": "col-m"
				},
				{"orderable": false, data: 'sexcluyente', targets: 2},
				{"orderable": false, data: 'tipohallazgo', targets: 3},
				{"orderable": false, data: 'dhallazgo', targets: 5},
				{"orderable": false, data: 'daccioncorrectiva', targets: 6},
				{
					"orderable": false,
					render: function (data, type, row) {
						return (row.fcorrectiva)
							? moment(row.fcorrectiva, 'YYY-MM-DD').format('DD/MM/YYYY')
							: '';
					}
				},
				{"orderable": false, data: 'dresponsablecliente', targets: 8},
				{"orderable": false, data: 'saceptaraccioncorrectiva', targets: 9},
				{"orderable": false, data: 'dobservacion', targets: 10},
			],
			"columnDefs": [
				{"targets": [0], "visible": true},
			],
		});
	});

	$('#btnSubirArchivoACC').click(function() {
		const form = $('form#frmFileACC');
		Swal.fire({
			type: 'warning',
			title: 'El proceso de subir el archivo deja sin efecto lo registro manual de la AA.CC.',
			text: '¿Seguro(a) de subir el archivo?',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Si',
			cancelButtonText: 'Cancelar',
		}).then((result) => {
			if (result.value) {
				const datos = new FormData(form[0]);
				datos.append('cauditoria', $('#acc_cauditoria').val());
				datos.append('fservicio', $('#acc_fservicio').val());
				$.ajax({
					url: BASE_URL + 'at/ctrlprov/inspctrolprov/caccion_correctiva/guardar_archivo',
					method: 'POST',
					data: datos,
					dataType: 'json',
					cache: false,
					contentType: false,
					processData: false,
					beforeSend: function () {
					}
				}).done(function (response) {
					$('#modalAccionCorrectiva').modal('hide');
					sweetalert(response.message, 'success');
				}).fail(function (jqxhr) {
					sweetalert('Error en el proceso de ejecución', 'error');
				}).always(function () {
				});
			}
		});
	});

	$(document).on('click', '.opcion-editar-acc', function() {
		const boton = $(this);
		$.ajax({
			method: 'POST',
			url: baseurl + "at/ctrlprov/inspctrolprov/caccion_correctiva/buscar",
			dataType: "JSON",
			async: true,
			data: {
				cauditoria: boton.data('cauditoria'),
				fservicio: boton.data('fservicio'),
				cchecklist: boton.data('cchecklist'),
				crequisitochecklist: boton.data('crequisitochecklist'),
			},
			beforeSend: function() {
				if (boton) {
					objPrincipal.botonCargando(boton);
				}
			}
		}).done(function(resp) {
			$('#acc_modal_cchecklist_id').val(boton.data('cchecklist'));
			$('#acc_modal_requisito_cchecklist_id').val(boton.data('crequisitochecklist'));
			$('#acc_modal_requisito').val(resp.drequisito);
			$('#acc_modal_hallazgo').val(resp.dhallazgo);
			$('#acc_modal_acc').val(resp.daccioncorrectiva);
			$('#acc_modal_observaciones').val(resp.dobservacion);
			$('#acc_modal_acepta_acc').prop('checked', resp.saceptaraccioncorrectiva.toUpperCase() === "S");
			$('#acc_modal_tipo_hallazgo').val(boton.data('tipohallazgo'));
			$('#acc_modal_responsable').val(resp.dresponsablecliente);
			if (resp.fcorrectiva) {
				let fecha = moment(resp.fcorrectiva, 'YYYY-MM-DD').format('DD/MM/YYYY');
				$('#acc_modal_fecha').val(fecha);
			}
			$('#ModalAccionCorrectiva').modal('show');
		}).fail(function() {
			objPrincipal.notify('error', 'Error al cargar la acción correctiva.')
		}).always(function() {
			if (boton) {
				objPrincipal.liberarBoton(boton);
			}
		});
	});

	$('#btnCerrarACC').click(function() {
		const botonCerrar = $('#btnCerrarACC');
		var datos = $('#frmAccionCorrectiva').serialize()
			+ '&cauditoria=' + $('#acc_cauditoria').val()
			+ '&fservicio=' + $('#acc_fservicio').val();
		$.ajax({
			url: baseurl + 'at/ctrlprov/inspctrolprov/caccion_correctiva/guardar',
			method: 'POST',
			dataType: "JSON",
			async: true,
			data: datos,
			beforeSend: function() {
				objPrincipal.botonCargando(botonCerrar);
			}
		}).done(function(resp) {
			objPrincipal.notify('success', resp.message);
			$('#ModalAccionCorrectiva').modal('hide');
			$("#tblAcciónCorrectiva").DataTable().ajax.reload(null, false);
		}).fail(function() {
			objPrincipal.notify('error', 'Error HTTP al actualizar la acción correctiva.');
		}).always(function() {
			objPrincipal.liberarBoton(botonCerrar);
		});
	});

	$('#modalAccionCorrectiva').on('hidden.bs.modal', function (event) {
		$('#tblListinspctrlprov').DataTable().ajax.reload(null, false);
	});

	$(document).on('click', '.abrir-historico', function() {
		const boton = $(this)
		const cauditoria = boton.data('cauditoria');
		const fservicio = boton.data('fservicio');
		const areacli = boton.data('areacli');
		$('#textoInspHistorico').html(areacli);
		abrirHistorico(cauditoria, fservicio);
		$('#modalInspecciónHistorico').modal('show');
	});

});

fechaAnioActual = function () {
	$("#txtFIni").prop("disabled", false);
	$("#txtFFin").prop("disabled", false);

	varfdesde = '';
	varfhasta = '';

	var fecha = new Date();
	var fechatring1 = "01/01/" + (fecha.getFullYear());
	var fechatring2 = ("0" + fecha.getDate()).slice(-2) + "/" + ("0" + (fecha.getMonth() + 1)).slice(-2) + "/" + fecha.getFullYear();

	$('#txtFDesde').datetimepicker('date', moment(fechatring1, 'DD/MM/YYYY'));
	$('#txtFHasta').datetimepicker('date', moment(fechatring2, 'DD/MM/YYYY'));
};

fechaActual = function () {
	var fecha = new Date();
	var fechatring = ("0" + fecha.getDate()).slice(-2) + "/" + ("0" + (fecha.getMonth() + 1)).slice(-2) + "/" + fecha.getFullYear();

	$('#txtFDesde').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY'));
	$('#txtFHasta').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY'));
	$('#mtxtFCoti').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY'));

};

// $('#txtFDesde').on('change.datetimepicker', function (e) {
// 	$('#txtFHasta').datetimepicker({
// 		format: 'DD/MM/YYYY',
// 		daysOfWeekDisabled: [0],
// 		locale: 'es'
// 	});
// 	var fecha = moment(e.date).format('DD/MM/YYYY');
// 	$('#txtFHasta').datetimepicker('minDate', fecha);
// 	$('#txtFHasta').datetimepicker('date', fecha);
// });

$("#chkFreg").on("change", function () {
	if ($("#chkFreg").is(":checked") == true) {
		$("#txtFIni").prop("disabled", false);
		$("#txtFFin").prop("disabled", false);

		varfdesde = '';
		varfhasta = '';

		var fecha = new Date();
		var fechatring1 = "01/01/" + (fecha.getFullYear());
		var fechatring2 = ("0" + fecha.getDate()).slice(-2) + "/" + ("0" + (fecha.getMonth() + 1)).slice(-2) + "/" + fecha.getFullYear();

		$('#txtFDesde').datetimepicker('date', fechatring1);
		$('#txtFHasta').datetimepicker('date', fechatring2);
	} else if ($("#chkFreg").is(":checked") == false) {
		$("#txtFIni").prop("disabled", true);
		$("#txtFFin").prop("disabled", true);

		varfdesde = '%';
		varfhasta = '%';

		fechaActual();
	}
	;
});

$("#cboclieserv").change(function () {

	var select = document.getElementById("cboclieserv"), //El <select>
		value = select.value, //El valor seleccionado
		text = select.options[select.selectedIndex].innerText;
	document.querySelector('#lblCliente').innerText = text;

	var v_cboclieserv = $('#cboclieserv').val();
	var params = {"ccliente": v_cboclieserv};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/cinspctrolprov/getcboprovxclie",
		dataType: "JSON",
		async: true,
		data: params,
		success: function (result) {
			$("#cboprovxclie").html(result);
			$('#cboprovxclie').val('').trigger("change");
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboprovxclie');
		}
	});

	/*if(v_cboclieserv != 0){
		$("#btnNuevo").prop("disabled",false);
	}else{
		$("#btnNuevo").prop("disabled",true);
	}*/
});

$("#cboprovxclie").change(function () {

	var v_cboprov = $('#cboprovxclie').val();
	var params = {"cproveedor": v_cboprov};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/cinspctrolprov/getcbomaqxprov",
		dataType: "JSON",
		async: true,
		data: params,
		success: function (result) {
			$("#cbomaqxprov").html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cbomaqxprov');
		}
	});
});

$("#btnBuscar").click(function () {
	let fdesde = '%';
	let fhasta = '%';
	if ($('#chkFreg').is(':checked')) {
		fdesde = $('#txtFIni').val();
		fhasta = $('#txtFFin').val();
	}
	let filtroPeligro = '';
	if ($('#filtro_peligro_1').is(':checked')) {
		filtroPeligro = 'S';
	}
	if ($('#filtro_peligro_2').is(':checked')) {
		filtroPeligro = 'N';
	}
	const boton = $("#btnBuscar");
	var groupColumn = 0;
	otblListinspctrlprov = $('#tblListinspctrlprov').DataTable({
		'responsive': false,
		'bJQueryUI': true,
		'scrollY': '650px',
		'scrollX': true,
		'paging': true,
		'processing': true,
		'bDestroy': true,
		'AutoWidth': true,
		'info': true,
		'filter': true,
		'ordering': false,
		'stateSave': true,
		'ajax': {
			"url": baseurl + "at/ctrlprov/inspctrolprov/cinspeccion/getbuscarctrlprov",
			"type": "POST",
			"data": function (d) {
				d.ccliente = $('#cboclieserv').val();
				d.fdesde = fdesde;
				d.fhasta = fhasta;
				d.dclienteprovmaq = $('#txtprovmaq').val();
				d.inspector = $('#cboinspector').val();
				d.cboestado = $('#cboestado').val();
				d.speligro = filtroPeligro;
			},
			dataSrc: function (data) {
				objPrincipal.liberarBoton(boton);
				return data;
			},
		},
		'columns': [
			{"orderable": false, data: 'desc_gral', targets: 0, "visible": false},
			{"orderable": false, data: 'areacli', targets: 1, "visible": false},
			{"orderable": false, data: 'lineaproc', targets: 2, "visible": false},

			{"orderable": false, data: 'periodo', "class": "col-s", targets: 3},
			{"orderable": false, data: 'destado', "class": "col-s", targets: 4},
			{
				"orderable": false,
				render: function(data, type, row) {
					let vfechaip = '';
					if (row.finspeccion !== '01/01/1900') {
						vfechaip = row.finspeccion;
					}
					return '<div class="text-left" style="width: 80px" >' + vfechaip + '</div>';
				}
			},
			{
				"orderable": false,
				render: function(data, type, row) {
					let dinforme = row.dinformefinal;
					if (String(row.destado).toLowerCase() === 'concluido ok' && row.DUBICACIONFILESERVERPDF) {
						dinforme = '<a href="' + baseurl + 'FTPfileserver/Archivos/' + row.DUBICACIONFILESERVERPDF + '" target="_blank" class="btn btn-link p-0" >'
							+ row.dinformefinal +
							'</a>';
					}
					return '<div class="text-left" style="width: 115px" >' + dinforme + '</div>';
				}
			},
			{
				"orderable": false,
				render: function(data, type, row) {
					const EMPRESAINSPECTORA = (row.EMPRESAINSPECTORA)
						? ' - ' + String(row.EMPRESAINSPECTORA).toUpperCase().trim()
						: '';
					return row.resultado + EMPRESAINSPECTORA;
				}
			},
			// {
			// 	"orderable": false,
			// 	render: function(data, type, row) {
			// 		return '<div class="text-left" style="width: 130px" >' + row.inspector + '</div>';
			// 	}
			// },
			{
				"orderable": false, // Se edito el procedimiento usp_oi_ctrlprov_getbuscarctrlprov
				"class": "col-xs",
				render: function (data, type, row) {
					const checked = (row.finalizado === 'S') ? 'checked' : '';
					return '<div class="text-center" >' +
						'<div class="custom-control custom-checkbox">' +
						'<input type="checkbox" class="custom-control-input" disabled id="fina-' + row.periodo + '" ' + checked + ' >' +
						'<label class="custom-control-label" for="customCheck1"></label>' +
						'</div>' +
						'</div>';
				}
			},
			{
				"orderable": false, // Se edito el procedimiento usp_oi_ctrlprov_getbuscarctrlprov
				render: function (data, type, row) {
					const checked = (row.peligros === 'S') ? 'checked' : '';
					return '<div class="text-center" >' +
						'<div class="custom-control custom-checkbox">' +
						'<input type="checkbox" class="custom-control-input" disabled id="peli-' + row.periodo + '" ' + checked + ' >' +
						'<label class="custom-control-label" for="customCheck1"></label>' +
						'</div>' +
						'</div>';
				}
			},
			{
				"orderable": false, // Se edito el procedimiento usp_oi_ctrlprov_getbuscarctrlprov
				render: function (data, type, row) {
					const checked = (row.queja === 'S') ? 'checked' : '';
					let boton =
						'<div class="custom-control custom-checkbox">' +
						'<input type="checkbox" class="custom-control-input" disabled id="que-' + row.periodo + '" ' + checked + ' >' +
						'<label class="custom-control-label" for="customCheck1"></label>' +
						'</div>';
					if (row.DUBICACIONFILESERVERQUEJA) {
						boton += '<a class="btn btn-link btn-sm" href="' + BASE_URL + 'FTPfileserver/Archivos/' + row.DUBICACIONFILESERVERQUEJA + '" target="_blank" >Descargar</a>';
					}
					return '<div class="text-center" >' + boton + '</div>';
				}
			},
			{
				"orderable": false, // Se edito el procedimiento usp_oi_ctrlprov_getbuscarctrlprov
				render: function (data, type, row) {
					const checked = (row.accion_cor === 'S') ? 'checked' : '';
					let boton =
						'<div class="custom-control custom-checkbox">' +
						'<input type="checkbox" class="custom-control-input" disabled id="AA-CC-' + row.periodo + '" ' + checked + ' >' +
						'<label class="custom-control-label" for="customCheck1"></label>' +
						'</div>';
					const txtAACC = (checked) ? 'Descargar' : 'Llenar AACC';
					if (row.totalAC > 0) {
						boton += '<button type="button" class="btn btn-link btn-sm operacion-descargar-acc" data-cauditoria="' + row.cauditoriainspeccion + '" data-fservicio="' + row.fservicio + '" >' + txtAACC + '</button>';
					}
					return '<div class="text-center" >' + boton + '</div>';
				}
			},
			{
				responsivePriority: 1,
				"orderable": false, // Se edito el procedimiento usp_oi_ctrlprov_getbuscarctrlprov
				render: function (data, type, row) {
					let desc_gral = (row.desc_gral) ? String(row.desc_gral).replace(/['"]+/g, '') : '';
					let result = '<div class="btn-group" >';
					switch (row.zctipoestadoservicio) {
						case '031': // EN PROCESO
							result += '<a title="Checklist" style="cursor:pointer; color:#3c763d;" href="' + BASE_URL + 'at/ctrlprov/inspctrolprov/cinspeccion/editar?cauditoria=' + row.cauditoriainspeccion + '&fservicio=' + row.fservicio + '" target="_blank" ><span class="fas fa-th-list fa-2x" aria-hidden="true"> </span> </a>';
							break;
						case '029': // INSPECTOR ASIGNADO
							result += '<a title="Iniciar checklist" style="cursor:pointer; color:#0069d9;" onClick="javascript:iniciarChecklist(\'' + desc_gral + '\',\'' + row.areacli + '\',\'' + row.lineaproc + '\',\'' + row.cauditoriainspeccion + '\',\'' + row.fservicio + '\',\'' + row.finspeccion + '\',\'' + row.cchecklist + '\',\'' + row.cmodeloinforme + '\',\'' + row.cgrupocriterio + '\',\'' + row.cvalornoconformidad + '\');"><span class="fas fa-check fa-2x" aria-hidden="true"> </span> </a>';
							break;
					}
					// if (row.sultimo === 'S') {
						result += '<button type="button" role="button" class="ml-2 btn btn-light btn-sm abrir-historico" data-areacli="' + desc_gral + '" data-cauditoria="' + row.cauditoriainspeccion + '" data-fservicio="' + row.fservicio + '" title="Historico" ><i class="fa fa-list fa-2x" ></i></button>';
					// }
					return result + '</div>';
				}
			},
			{data: 'espeligro'},
		],
		"columnDefs": [
			{"targets": [0], "visible": false},
		],
		"drawCallback": function (settings) {
			var api = this.api();
			var rows = api.rows({page: 'current'}).nodes();
			var last = null;
			var grupo, grupo01;

			api.column([0], {}).data().each(function (ctra, i) {
				grupo = api.column(1).data()[i];
				grupo01 = api.column(2).data()[i];
				var speligro = api.column(13).data()[i];
				const varcolor = (speligro === "S") ? '#FF0000' : '#000000';
				if (last !== ctra) {
					$(rows).eq(i).before(
						'<tr class="group"><td colspan="14" class="subgroup"><strong>' + ctra.toUpperCase() + '</strong></td></tr>' +
						'<tr class="group"><td colspan="3">Area : ' + grupo + '</td><td colspan="10" style="color: ' + varcolor + '" >Linea : ' + grupo01 + '</td></tr>'
					);
					last = ctra;
				}
			});
		}
	});
	otblListinspctrlprov.column(1).visible(false);
	otblListinspctrlprov.column(2).visible(false);
	otblListinspctrlprov.column(13).visible(false);
});

function abrirHistorico(cauditoria, fservicio) {
	const otblListinspctrlprovHist = $('#tblListinspctrlprovHistorico').DataTable({
		'responsive': false,
		'bJQueryUI': true,
		'scrollY': '650px',
		'scrollX': true,
		'paging': true,
		'processing': true,
		'bDestroy': true,
		'AutoWidth': true,
		'info': true,
		'filter': true,
		'ordering': false,
		'stateSave': true,
		'ajax': {
			"url": baseurl + "at/ctrlprov/inspctrolprov/cinspeccion/getbuscarctrlprov_historico",
			"type": "POST",
			"data": function (d) {
				d.cauditoria = cauditoria;
				d.fservicio = fservicio;
			},
			dataSrc: function (data) {
				return data;
			},
		},
		'columns': [
			{"orderable": false, data: 'desc_gral', targets: 0, "visible": false},
			{"orderable": false, data: 'areacli', targets: 1, "visible": false},
			{"orderable": false, data: 'lineaproc', targets: 2, "visible": false},

			{"orderable": false, data: 'periodo', "class": "col-s", targets: 3},
			{"orderable": false, data: 'destado', "class": "col-s", targets: 4},
			{
				"orderable": false,
				render: function(data, type, row) {
					let vfechaip = '';
					if (row.finspeccion !== '01/01/1900') {
						vfechaip = row.finspeccion;
					}
					return '<div class="text-left" style="width: 80px" >' + vfechaip + '</div>';
				}
			},
			{
				"orderable": false,
				render: function(data, type, row) {
					let dinforme = row.dinformefinal;
					if (String(row.destado).toLowerCase() === 'concluido ok' && row.DUBICACIONFILESERVERPDF) {
						dinforme = '<a href="' + baseurl + 'FTPfileserver/Archivos/' + row.DUBICACIONFILESERVERPDF + '" target="_blank" class="btn btn-link p-0" >'
							+ row.dinformefinal +
							'</a>';
					}
					return '<div class="text-left" style="width: 115px" >' + dinforme + '</div>';
				}
			},
			{
				"orderable": false,
				render: function(data, type, row) {
					const EMPRESAINSPECTORA = (row.EMPRESAINSPECTORA)
						? ' - ' + String(row.EMPRESAINSPECTORA).toUpperCase().trim()
						: '';
					return row.resultado + EMPRESAINSPECTORA;
				}
			},
			{
				"orderable": false,
				render: function(data, type, row) {
					return '<div class="text-left" style="width: 130px" >' + row.inspector + '</div>';
				}
			},
			{
				"orderable": false, // Se edito el procedimiento usp_oi_ctrlprov_getbuscarctrlprov
				"class": "col-xs",
				render: function (data, type, row) {
					const checked = (row.finalizado === 'S') ? 'checked' : '';
					return '<div class="text-center" >' +
						'<div class="custom-control custom-checkbox">' +
						'<input type="checkbox" class="custom-control-input" disabled id="fina-' + row.periodo + '" ' + checked + ' >' +
						'<label class="custom-control-label" for="customCheck1"></label>' +
						'</div>' +
						'</div>';
				}
			},
			{
				"orderable": false, // Se edito el procedimiento usp_oi_ctrlprov_getbuscarctrlprov
				render: function (data, type, row) {
					const checked = (row.peligros === 'S') ? 'checked' : '';
					return '<div class="text-center" >' +
						'<div class="custom-control custom-checkbox">' +
						'<input type="checkbox" class="custom-control-input" disabled id="peli-' + row.periodo + '" ' + checked + ' >' +
						'<label class="custom-control-label" for="customCheck1"></label>' +
						'</div>' +
						'</div>';
				}
			},
			{
				"orderable": false, // Se edito el procedimiento usp_oi_ctrlprov_getbuscarctrlprov
				render: function (data, type, row) {
					const checked = (row.queja === 'S') ? 'checked' : '';
					let boton =
						'<div class="custom-control custom-checkbox">' +
						'<input type="checkbox" class="custom-control-input" disabled id="que-' + row.periodo + '" ' + checked + ' >' +
						'<label class="custom-control-label" for="customCheck1"></label>' +
						'</div>';
					if (row.DUBICACIONFILESERVERQUEJA) {
						boton += '<a class="btn btn-link btn-sm" href="' + BASE_URL + 'FTPfileserver/Archivos/' + row.DUBICACIONFILESERVERQUEJA + '" target="_blank" >Descargar</a>';
					}
					return '<div class="text-center" >' + boton + '</div>';
				}
			},
			{
				"orderable": false, // Se edito el procedimiento usp_oi_ctrlprov_getbuscarctrlprov
				render: function (data, type, row) {
					const checked = (row.accion_cor === 'S') ? 'checked' : '';
					let boton =
						'<div class="custom-control custom-checkbox">' +
						'<input type="checkbox" class="custom-control-input" disabled id="AA.CC-' + row.periodo + '" ' + checked + ' >' +
						'<label class="custom-control-label" for="customCheck1"></label>' +
						'</div>';
					const txtAACC = (checked) ? 'Descargar' : 'Llenar AACC';
					if (row.totalAC > 0) {
						boton += '<button type="button" class="btn btn-link btn-sm operacion-descargar-acc" data-cauditoria="' + row.cauditoriainspeccion + '" data-fservicio="' + row.fservicio + '" >' + txtAACC + '</button>';
					}
					return '<div class="text-center" >' + boton + '</div>';
				}
			},
			{data: 'espeligro'},
		],
		"columnDefs": [
			{"targets": [0], "visible": false},
		],
		"drawCallback": function (settings) {
			var api = this.api();
			var rows = api.rows({page: 'current'}).nodes();
			var last = null;
			var grupo, grupo01;

			api.column([0], {}).data().each(function (ctra, i) {
				grupo = api.column(1).data()[i];
				grupo01 = api.column(2).data()[i];
				var speligro = api.column(13).data()[i];
				const varcolor = (speligro === "S") ? '#FF0000' : '#000000';
				if (last !== ctra) {
					$(rows).eq(i).before(
						'<tr class="group"><td colspan="14" class="subgroup"><strong>' + ctra.toUpperCase() + '</strong></td></tr>' +
						'<tr class="group"><td colspan="3">Area : ' + grupo + '</td><td colspan="10" style="color: ' + varcolor + '" >Linea : ' + grupo01 + '</td></tr>'
					);
					last = ctra;
				}
			});
		}
	});
	otblListinspctrlprovHist.column(1).visible(false);
	otblListinspctrlprovHist.column(2).visible(false);
	otblListinspctrlprovHist.column(13).visible(false);
}

//selInspe = function(cauditoriainspeccion,desc_gral,areacli,lineaproc,cusuarioconsultor,periodo,fservicio,cnorma,csubnorma,cchecklist,cmodeloinforme,cvalornoconformidad,cformulaevaluacion,ccriterioresultado,dcomentario,zctipoestadoservicio,destado,ccliente){
selInspe = function (desc_gral, areacli, lineaproc, cauditoriainspeccion, fservicio) {
	$('#tabctrlprov a[href="#tabctrlprov-det"]').tab('show');

	$('#frmRegInsp').trigger("reset");

	var proveedor = 'PROVEEDOR: ' + desc_gral;
	var area = 'AREA: ' + areacli;
	var linea = 'LINEA: ' + lineaproc;
	$('#mtxtidinsp').val(cauditoriainspeccion);
	$("#mtxtinspdatos").text(proveedor);
	$("#mtxtinsparea").text(area);
	$("#mtxtinsplinea").text(linea);

	$('#mhdnAccioninsp').val('A');
	//$('#mhdnccliente').val(ccliente);

	iniInspe(cusuarioconsultor,'T',cnorma,csubnorma,cchecklist,ccliente,cvalornoconformidad,cmodeloinforme,cformulaevaluacion,ccriterioresultado);

	/*$('#mtxtinspdatos').val(desc_gral);
	$('#mtxtinsparea').val('AREÁ : '+areacli);
	$('#mtxtinsplinea').val('LINEA : '+lineaproc);

	$('#cboinspperiodo').val(periodo);
	$('#mtxtinspcoment').val(dcomentario);
	$('#mhdnzctipoestado').val(zctipoestadoservicio);
	$('#txtinspestado').val(destado);*/

	listChecklist(cauditoriainspeccion, fservicio);

	$('#txtFInspeccion').datetimepicker({
		format: 'DD/MM/YYYY',
		daysOfWeekDisabled: [0],
		locale: 'es',
		autoclose: true,
		todayBtn: true
	});
	if (fservicio == '01/01/1900') {
		$('#txtFInsp').val('Sin Fecha');
	} else {
		$('#txtFInsp').val(fservicio);
	}

};

$("#cboinspsistema").change(function () {
	var v_cnorma = $("#cboinspsistema option:selected").attr("value");

	var paramsrubro = {"cnorma": v_cnorma};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/cinspctrolprov/getcborubroip",
		dataType: "JSON",
		async: true,
		data: paramsrubro,
		success: function (result) {
			$('#cboinsprubro').html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboinsprubro');
		}
	});
});

$("#cboinsprubro").change(function () {
	var v_csubnorma = $("#cboinsprubro option:selected").attr("value");
	var v_cnorma = $('#cboinspsistema').val();
	var v_ccliente = $('#mhdnccliente').val();
	var paramschecklist = {
		"cnorma": v_cnorma,
		"csubnorma": v_csubnorma,
		"ccliente": v_ccliente,
	};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/cinspctrolprov/getcbochecklist",
		dataType: "JSON",
		async: true,
		data: paramschecklist,
		success: function (result) {
			$('#cboinspcchecklist').html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboinspcchecklist');
		}
	});
});

$("#cboinspcchecklist").change(function () {
	var v_cchecklist = $("#cboinspcchecklist option:selected").attr("value");
	var paramsform = {"cchecklist": v_cchecklist};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/cinspctrolprov/getcboinspformula",
		dataType: "JSON",
		async: true,
		data: paramsform,
		success: function (result) {
			$('#cboinspformula').html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboinspformula');
		}
	});
});


$('#btnRetornarLista').click(function () {
	$('#tabctrlprov a[href="#tabctrlprov-list"]').tab('show');
	$('#btnBuscar').click();
});

iniciarChecklist = function (desc_gral, areacli, lineaproc, cauditoriainspeccion, fservicio, finspeccion, cchecklist, cmodeloinforme, cgrupocriterio, cvalornoconformidad) {
	var cod_usuario = $("#cusuario").val();
	Swal.fire({
		title: 'Inciar Checklist',
		text: "¿Está seguro de iniciar el registro de checklist?",
		icon: 'question',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Si, iniciar!'
	}).then(function(result) {
		if (result.value) {
			var params = {
				"cauditoriainspeccion": cauditoriainspeccion,
				"fservicio": fservicio,
				"cchecklist": cchecklist,
				"ls_cgrucrieval": cgrupocriterio,
				"ls_cvalnoconf": cvalornoconformidad,
				"str_cod_usuario": cod_usuario
			};

			$.ajax({
				type: 'ajax',
				method: 'post',
				url: baseurl + "at/ctrlprov/inspctrolprov/cinspeccion/cambioestadochecklist",
				dataType: "JSON",
				async: true,
				data: params,
				success: function (result) {
					console.log('result', result)
					if (result == 1) {
						$('#tblListinspctrlprov').DataTable().ajax.reload(); // verificar si al actualizar necesita parametros
						Vtitle = 'Checklist iniciado!';
						Vtype = 'success';
						sweetalert(Vtitle, Vtype);
						window.open(
							BASE_URL + 'at/ctrlprov/inspctrolprov/cinspeccion/editar?cauditoria=' + cauditoriainspeccion + '&fservicio=' + fservicio,
							'_blank'
						)
					} else {
						Vtitle = 'Ups,error con el registro!';
						Vtype = 'error';
						sweetalert(Vtitle, Vtype);
					}
				},
				error: function () {
					alert('Error, No se puede autenticar por error ');
				}
			});
		}
	})

}

function descargarPeligroQuejas() {
	$.ajax({
		method: 'post',
		url: baseurl + 'at/ctrlprov/inspctrolprov/cquejapeligros/descargar_peligro',
		dataType: 'json',
		async: true,
		data: {},
		beforeSend: function () {}
	}).done(function (resp) {
		if (resp.archivo) {
			const el = $('#descargarPeligro');
			el.attr('href', resp.archivo);
			el.attr('download', resp.nombre);
			el.show();
		}
	}).fail(function (jqxhr) {
		const message = (jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la petición HTTP.';
		sweetalert(message, 'error');
	});
	$.ajax({
		method: 'post',
		url: baseurl + 'at/ctrlprov/inspctrolprov/cquejapeligros/descargar_queja',
		dataType: 'json',
		async: true,
		data: {},
		beforeSend: function () {}
	}).done(function (resp) {
		if (resp.archivo) {
			const el = $('#descargarQuejas');
			el.attr('href', resp.archivo);
			el.attr('download', resp.nombre);
			el.show();
		}
	}).fail(function (jqxhr) {
		const message = (jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la petición HTTP.';
		sweetalert(message, 'error');
	});
}
