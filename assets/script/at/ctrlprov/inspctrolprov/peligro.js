/*!
 *
 * @versión 1.0.0
 */

const objPeligro = {};

$(function() {

	/**
	 * Lista de peligros
	 */
	objPeligro.lista = function() {
		otblinspeccionprov = $('#tblPeligros').DataTable({
			'responsive': false,
			'bJQueryUI': true,
			'scrollY': '600px',
			'scrollX': true,
			'paging': false,
			'processing': true,
			'bDestroy': true,
			'AutoWidth': true,
			'info': true,
			'filter': true,
			'ordering': false,
			'stateSave': true,
			'ajax': {
				"url": baseurl + "at/ctrlprov/inspctrolprov/cpeligros/lista",
				"type": "POST",
				"data": function (d) {
					d.cauditoria = $('#cauditoria').val();
					d.fservicio = $('#fservicio').val();
				},
				dataSrc: function (data) {
					return data;
				},
			},
			'columns': [
				{"orderable": false, data: 'NORDENPELIGRO', targets: 0},
				{"orderable": false, data: 'DPRODUCTO', targets: 1},
				{"orderable": false, data: 'DPELIGROCLIENTE', targets: 2},
				{"orderable": false, data: 'DPELIGROPROVEEDOR', targets: 3},
				{"orderable": false, data: 'DPELIGROINSPECCION', targets: 4},
				{"orderable": false, data: 'DOBSERVACION', targets: 5},
				{
					responsivePriority: 1, "orderable": false,
					render: function (data, type, row) {
						if (parseInt($('#inspeccion_abierto').val()) === 1) {
							return '<div>' +
								'<button type="button" role="button" class="btn btn-secondary btn-sm btn-eliminar-peligro" data-orden="' + row.NORDENPELIGRO + '" >' +
								'<i class="fa fa-trash" ></i> Eliminar' +
								'</button>' +
								'</div>';
						} else {
							return '';
						}
					}
				}
			],
			"columnDefs": [
				{"targets": [0], "visible": true},
			],
		});
	};

	objPeligro.refrescarLista = function() {
		$("#tblPeligros").DataTable().ajax.reload();
	};

	/**
	 * Guarda el peligro
	 */
	objPeligro.guardar = function() {
		const from = $("#frmPeligro");
		var datos = from.serialize()
			+ '&cauditoria=' + $('#cauditoria').val()
			+ '&fservicio=' + $('#fservicio').val();
		const boton = $("#btnGuardarPeligro");
		$.ajax({
			type: 'ajax',
			method: 'post',
			url: baseurl + 'at/ctrlprov/inspctrolprov/cpeligros/guardar',
			dataType: 'json',
			async: true,
			data: datos,
			beforeSend: function () {
				objPrincipal.botonCargando(boton);
			}
		}).done(function (resp) {
			sweetalert(resp.message, 'success');
			objPeligro.refrescarLista();
		}).fail(function (jqxhr) {
			const message = (jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la petición HTTP.';
			sweetalert(message, 'error');
		}).always(function () {
			objPrincipal.liberarBoton(boton);
		});
	};

	/**
	 * Elimina un peligro
	 */
	objPeligro.eliminar = function() {
		const boton = $(this);
		Swal.fire({
			type: 'warning',
			title: 'Eliminar peligro',
			text: '¿Estas seguro(a) de eliminar?',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Si',
			cancelButtonText: 'Cancelar',
		}).then((result) => {
			if (result.value) {
				$.ajax({
					method: 'post',
					url: baseurl + 'at/ctrlprov/inspctrolprov/cpeligros/eliminar',
					dataType: 'json',
					async: true,
					data: {
						cauditoria: $('#cauditoria').val(),
						fservicio: $('#fservicio').val(),
						norden: boton.data('orden'),
					},
					beforeSend: function () {
						objPrincipal.botonCargando(boton);
					}
				}).done(function (resp) {
					sweetalert(resp.message, 'success');
					objPeligro.refrescarLista();
				}).fail(function (jqxhr) {
					const message = (jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la petición HTTP.';
					sweetalert(message, 'error');
				}).always(function () {
					objPrincipal.liberarBoton(boton);
				});
			}
		});
	};

	objPeligro.descargarPeligroQuejas = function() {
		$.ajax({
			method: 'post',
			url: baseurl + 'at/ctrlprov/inspctrolprov/cquejapeligros/descargar_peligro',
			dataType: 'json',
			async: true,
			data: {},
			beforeSend: function () {}
		}).done(function (resp) {
			if (resp.archivo) {
				const el = $('#descargarPeligro');
				el.attr('href', resp.archivo);
				el.attr('download', resp.nombre);
				el.show();
			}
		}).fail(function (jqxhr) {
			const message = (jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la petición HTTP.';
			sweetalert(message, 'error');
		});
		$.ajax({
			method: 'post',
			url: baseurl + 'at/ctrlprov/inspctrolprov/cquejapeligros/descargar_queja',
			dataType: 'json',
			async: true,
			data: {},
			beforeSend: function () {}
		}).done(function (resp) {
			if (resp.archivo) {
				const el = $('#descargarQuejas');
				el.attr('href', resp.archivo);
				el.attr('download', resp.nombre);
				el.show();
			}
		}).fail(function (jqxhr) {
			const message = (jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la petición HTTP.';
			sweetalert(message, 'error');
		});
	}

});

$(document).ready(function() {

	objPeligro.lista();

	objPeligro.descargarPeligroQuejas();

	$('#btnGuardarPeligro').click(objPeligro.guardar);

	$(document).on('click', '.btn-eliminar-peligro', objPeligro.eliminar);

	$('#alertaPeligro').click(function() {
		$('#tabctrlprov-peligro-tab').click();
	});

});
