var otblListinspaudi;
var varfdesde = '%', varfhasta = '%';

$(document).ready(function () {
	$('#tabinspaudi a[href="#tabinspaudi-list-tab"]').attr('class', 'disabled');
	$('#tabinspaudi a[href="#tabinspaudi-det-tab"]').attr('class', 'disabled active');

	$('#tabinspaudi a[href="#tabinspaudi-list-tab"]').not('#store-tab.disabled').click(function (event) {
		$('#tabinspaudi a[href="#tabinspaudi-list"]').attr('class', 'active');
		$('#tabinspaudi a[href="#tabinspaudi-det"]').attr('class', '');
		return true;
	});
	$('#tabinspaudi a[href="#tabinspaudi-det-tab"]').not('#bank-tab.disabled').click(function (event) {
		$('#tabinspaudi a[href="#tabinspaudi-det"]').attr('class', 'active');
		$('#tabinspaudi a[href="#tabinspaudi-list"]').attr('class', '');
		return true;
	});

	$('#tabinspaudi a[href="#tabinspaudi-list"]').click(function (event) {
		return false;
	});
	$('#tabinspaudi a[href="#tabinspaudi-det"]').click(function (event) {
		return false;
	});

	$('#txtFDesde,#txtFHasta').datetimepicker({
		format: 'DD/MM/YYYY',
		daysOfWeekDisabled: [0],
		locale: 'es',
		autoclose: true,
		todayBtn: true
	});
	fechaActual();

	/*LLENADO DE COMBOS*/

	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "at/inspauditoria/creginspaudi/getcboclieserv",
		dataType: "JSON",
		async: true,
		success: function (result) {
			$('#cboclieserv').html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboclieserv');
		}
	});
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "at/inspauditoria/creginspaudi/getcboestado",
		dataType: "JSON",
		async: true,
		success: function (result) {
			$('#cboestado').html(result);
			$("#cboestado option[value='']").remove();
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboestado');
		}
	})
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "at/inspauditoria/creginspaudi/getcboinspector",
		dataType: "JSON",
		async: true,
		data: {
			"sregistro": 'A',
		},
		success: function (result) {
			$('#cboinspector').html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboinspector');
		}
	});

	//
	$('#frmCreactrlprov').validate({
		rules: {
			cboregClie: {
				required: true,
			},
			cboregprovclie: {
				required: true,
			},
			cboregestable: {
				required: true,
			},
			cboregareaclie: {
				required: true,
			},
		},
		messages: {
			cboregClie: {
				required: "Por Favor escoja un Cliente"
			},
			cboregprovclie: {
				required: "Por Favor escoja un Proveedor"
			},
			cboregestable: {
				required: "Por Favor escoja un Establecimiento"
			},
			cboregareaclie: {
				required: "Por Favor escoja un Area"
			},
		},
		errorElement: 'span',
		errorPlacement: function (error, element) {
			error.addClass('invalid-feedback');
			element.closest('.form-group').append(error);
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass('is-invalid');
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).removeClass('is-invalid');
		},
		submitHandler: function (form) {
			const botonEvaluar = $('#mbtnGCreactrl');
			var request = $.ajax({
				url: $('#frmCreactrlprov').attr("action"),
				type: $('#frmCreactrlprov').attr("method"),
				data: $('#frmCreactrlprov').serialize(),
				error: function () {
					Vtitle = 'Error en Guardar!!!';
					Vtype = 'error';
					sweetalert(Vtitle, Vtype);
					objPrincipal.liberarBoton(botonEvaluar);
				},
				beforeSend: function () {
					objPrincipal.botonCargando(botonEvaluar);
				}
			});
			request.done(function (respuesta) {
				var posts = JSON.parse(respuesta);

				$.each(posts, function () {
					$('#mhdnregIdinsp').val(this.cauditoriainspeccion);

					Vtitle = 'Inspección Guardada!!!';
					Vtype = 'success';
					sweetalert(Vtitle, Vtype);
					$('#btnBuscar').click();
					objPrincipal.liberarBoton(botonEvaluar);
				});
			});
			return false;
		}
	});
	$('#frmRegInsp').validate({
		rules: {},
		messages: {},
		errorElement: 'span',
		errorPlacement: function (error, element) {
			error.addClass('invalid-feedback');
			element.closest('.form-group').append(error);
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass('is-invalid');
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).removeClass('is-invalid');
		},
		submitHandler: function (form) {
			const botonEvaluar = $('#btnRegistrar');
			var request = $.ajax({
				url: $('#frmRegInsp').attr("action"),
				type: $('#frmRegInsp').attr("method"),
				data: $('#frmRegInsp').serialize(),
				error: function () {
					Vtitle = 'Error en Guardar!!!';
					Vtype = 'error';
					sweetalert(Vtitle, Vtype);
					objPrincipal.liberarBoton(botonEvaluar);
				},
				beforeSend: function () {
					objPrincipal.botonCargando(botonEvaluar);
				}
			});
			request.done(function (respuesta) {
				var posts = JSON.parse(respuesta);

				$.each(posts, function () {
					//$('#mhdnregIdinsp').val(this.cauditoriainspeccion);

					Vtitle = 'Inspección Guardada!!!';
					Vtype = 'success';
					sweetalert(Vtitle, Vtype);
					objPrincipal.liberarBoton(botonEvaluar);
				});
			});
			return false;
		}
	});
	$('#frmPlaninsp').validate({
		rules: {},
		messages: {},
		errorElement: 'span',
		errorPlacement: function (error, element) {
			error.addClass('invalid-feedback');
			element.closest('.form-group').append(error);
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass('is-invalid');
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).removeClass('is-invalid');
		},
		submitHandler: function (form) {
			const botonEvaluar = $('#mbtnGplaninsp');
			var request = $.ajax({
				url: $('#frmPlaninsp').attr("action"),
				type: $('#frmPlaninsp').attr("method"),
				data: $('#frmPlaninsp').serialize(),
				error: function () {
					Vtitle = 'Error en Guardar!!!';
					Vtype = 'error';
					sweetalert(Vtitle, Vtype);
					objPrincipal.liberarBoton(botonEvaluar);
				},
				beforeSend: function () {
					objPrincipal.botonCargando(botonEvaluar);
				}
			});
			request.done(function (respuesta) {
				var posts = JSON.parse(respuesta);

				$.each(posts, function () {
					Vtitle = 'Plan Creado!!!';
					Vtype = 'success';
					sweetalert(Vtitle, Vtype);
					objPrincipal.liberarBoton(botonEvaluar);
					$('#mbtnCplaninsp').click();
				});
			});
			return false;
		}
	});
	$('#frmCierreespecial').validate({
		rules: {},
		messages: {},
		errorElement: 'span',
		errorPlacement: function (error, element) {
			error.addClass('invalid-feedback');
			element.closest('.form-group').append(error);
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass('is-invalid');
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).removeClass('is-invalid');
		},
		submitHandler: function (form) {
			const botonEvaluar = $('#mbtnGCierreesp');
			var request = $.ajax({
				url: $('#frmCierreespecial').attr("action"),
				type: $('#frmCierreespecial').attr("method"),
				data: $('#frmCierreespecial').serialize(),
				error: function () {
					Vtitle = 'Error en Guardar!!!';
					Vtype = 'error';
					sweetalert(Vtitle, Vtype);
					objPrincipal.liberarBoton(botonEvaluar);
				},
				beforeSend: function () {
					objPrincipal.botonCargando(botonEvaluar);
				}
			});
			request.done(function (respuesta) {
				var posts = JSON.parse(respuesta);

				$.each(posts, function () {
					//$('#mhdnregIdinsp').val(this.cauditoriainspeccion);

					Vtitle = 'Inspección Cerrada!!!';
					Vtype = 'success';
					sweetalert(Vtitle, Vtype);
					objPrincipal.liberarBoton(botonEvaluar);
					$('#mbtnCCierreesp').click();
					$('#btnBuscar').click();
				});
			});
			return false;
		}
	});
	$('#frmConvalidacion').validate({
		rules: {},
		messages: {},
		errorElement: 'span',
		errorPlacement: function (error, element) {
			error.addClass('invalid-feedback');
			element.closest('.form-group').append(error);
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass('is-invalid');
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).removeClass('is-invalid');
		},
		submitHandler: function (form) {
			const botonEvaluar = $('#mbtnGconvali');
			var request = $.ajax({
				url: $('#frmConvalidacion').attr("action"),
				type: $('#frmConvalidacion').attr("method"),
				data: $('#frmConvalidacion').serialize(),
				error: function () {
					Vtitle = 'Error en Guardar!!!';
					Vtype = 'error';
					sweetalert(Vtitle, Vtype);
				},
				beforeSend: function () {
					objPrincipal.botonCargando(botonEvaluar);
				}
			});
			request.done(function (respuesta) {
				var posts = JSON.parse(respuesta);

				$.each(posts, function () {
					objPrincipal.liberarBoton(botonEvaluar);
					//$('#mhdnIdPropu').val(this.id_propuesta);
					if ($('#sArchivo').val() == 'S') {
						subirArchivoconval();
					} else {
						$('#btnBuscar').click();
						Vtitle = 'Propuesta Guardada!!!';//this.respuesta;
						Vtype = 'success';
						sweetalert(Vtitle, Vtype);
						$('#mbtnCconvali').click();
					}
					$('#sArchivo').val('N');
				});
			});
			return false;
		}
	});
});

fechaActual = function () {
	var fecha = new Date();
	var fechatring = ("0" + fecha.getDate()).slice(-2) + "/" + ("0" + (fecha.getMonth() + 1)).slice(-2) + "/" + fecha.getFullYear();

	$('#txtFDesde').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY'));
	$('#txtFHasta').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY'));

};

$('#txtFDesde').on('change.datetimepicker', function (e) {

	$('#txtFHasta').datetimepicker({
		format: 'DD/MM/YYYY',
		daysOfWeekDisabled: [0],
		locale: 'es'
	});

	var fecha = moment(e.date).format('DD/MM/YYYY');

	$('#txtFHasta').datetimepicker('minDate', fecha);
	$('#txtFHasta').datetimepicker('date', fecha);

});

$("#chkFreg").on("change", function () {
	if ($("#chkFreg").is(":checked") == true) {
		$("#txtFIni").prop("disabled", false);
		$("#txtFFin").prop("disabled", false);

		varfdesde = '';
		varfhasta = '';
	} else if ($("#chkFreg").is(":checked") == false) {
		$("#txtFIni").prop("disabled", true);
		$("#txtFFin").prop("disabled", true);

		varfdesde = '%';
		varfhasta = '%';
	}
});

$("#cboclieserv").change(function () {

	var select = document.getElementById("cboclieserv"), //El <select>
		value = select.value, //El valor seleccionado
		valor = select.valor,
		text = select.options[select.selectedIndex].innerText;
	document.querySelector('#lblCliente').innerText = text;
});

$("#btnBuscar").click(function () {
	let fdesde = '%';
	let fhasta = '%';
	if ($('#chkFreg').is(':checked')) {
		fdesde = $('#txtFIni').val();
		fhasta = $('#txtFFin').val();
	}
	var groupColumn = 1;
	const boton = $("#btnBuscar");
	objPrincipal.botonCargando(boton);
	otblListinspaudi = $('#tblListinspaudi').DataTable({
		"processing": true,
		"bDestroy": true,
		"stateSave": true,
		"bJQueryUI": true,
		"scrollY": "540px",
		"scrollX": true,
		'AutoWidth': true,
		"paging": false,
		"info": true,
		"filter": false,
		"ordering": false,
		"responsive": false,
		"select": true,
		"dom": 'lfTrtip',
		'ajax': {
			"url": baseurl + "at/inspauditoria/creginspaudi/getbuscarinspaudi/",
			"type": "POST",
			"data": function (d) {
				d.ccliente = $('#cboclieserv').val();
				d.fdesde = fdesde;
				d.fhasta = fhasta;
				d.dclientetienesta = $('#txttienesta').val();
				d.inspector = $('#cboinspector').val();
				d.cboestado = $('#cboestado').val();
			},
			dataSrc: function (data) {
				objPrincipal.liberarBoton(boton);
				return data;
			},
		},
		'columns': [
			{data: 'blanco', "class": "col-xs"},
			{data: 'desc_gral'},
			{data: 'finspeccion', "class": "col-s"},
			{data: 'destado', "class": "col-s"},
			{data: 'inspector', "class": "col-xm"},
			{data: 'dinformefinal', "class": "col-sm"},
			{data: 'resultado', "class": "col-sm"},
			{data: 'blanco', "class": "col-xm"},
			{data: 'blanco', "class": "col-xm"},
			/*{
				"orderable": false, "class": "col-xxs",
				render: function (data, type, row) {
					if (row.zctipoestadoservicio == '028') {
						return '<div class="dropdown" style="text-align: center;">' +
							'<a  data-toggle="dropdown" href="#"><span class="fas fa-bars"></span></a>' +
							'<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">' +
							'<li><a title="Programar" style="cursor:pointer; color:#3c763d;" onClick="javascript:fprogramar(\'' + row.cauditoriainspeccion + '\',\'' + row.desc_gral + '\',\'' + row.areacli + '\',\'' + row.lineaproc + '\',\'' + row.cusuarioconsultor + '\',\'' + row.periodo + '\',\'' + row.finspeccion + '\',\'' + row.zctipoestadoservicio + '\',\'' + row.destado + '\',\'' + row.ccliente + '\',\'' + row.spermitircorrectivas + '\',\'' + row.sincluyeplan + '\',\'' + row.zctiposervicio + '\',\'' + row.cproveedorcliente + '\',\'' + row.cmaquiladorcliente + '\',\'' + row.dcliente + '\',\'' + row.splan + '\',\'' + row.fservicio + '\');"><span class="far fa-calendar-alt" aria-hidden="true">&nbsp;</span>&nbsp;Programar</a></li>' +
							'<li><a data-toggle="modal" title="Covalidar" style="cursor:pointer; color:#3c763d;" data-target="#modalConvalidacion" onClick="javascript:fconvalidar(\'' + row.cauditoriainspeccion + '\',\'' + row.finspeccion + '\',\'' + row.ccriterioresultado + '\');"><span class="fas fa-file-signature" aria-hidden="true">&nbsp;</span>&nbsp;Convalidar</a></li>' +
							'<li><a data-toggle="modal" title="Cierre" style="cursor:pointer; color:#3c763d;" data-target="#modalCierreespecial" onClick="javascript:fcierreespecial(\'' + row.cauditoriainspeccion + '\',\'' + row.desc_gral + '\',\'' + row.areacli + '\',\'' + row.lineaproc + '\',\'' + row.finspeccion + '\',\'' + row.zctipoestadoservicio + '\',\'' + row.destado + '\',\'' + row.ccliente + '\');"><span class="far fa-window-close" aria-hidden="true">&nbsp;</span>&nbsp;Cierres Especiales</a></li>' +
							'</ul>' +
							'</div>'
					} else if (row.valorestado == '0' && row.sultimo == 'S') {
						return '<div class="dropdown" style="text-align: center;">' +
							'<a  data-toggle="dropdown" href="#"><span class="fas fa-bars"></span></a>' +
							'<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">' +
							'<li><a id="aReaperturar" href="' + row.cauditoriainspeccion + '" fservicio="' + row.finspeccion + '" title="Reaperturar" style="cursor:pointer; color:#3c763d;"><span class="far fa-folder-open" aria-hidden="true"> </span>&nbsp;Reaperturar</a></li>' +
							'</ul>' +
							'</div>'
					} else {
						return '<div></div>';
					}
				}
			},
			{
				"orderable": false, "class": "col-s",
				render: function (data, type, row) {
					if (row.finspeccion == '01/01/1900') {
						vfechaip = '';
					} else {
						vfechaip = row.finspeccion;
					}
					return '<div>' + vfechaip + '</div>'
				}
			},
			{data: 'destado', "class": "col-sm"},
			{data: 'inspector', "class": "col-xm"},
			{data: 'dinformefinal', "class": "col-sm"},
			{data: 'resultado', "class": "col-xm"},
			{
				responsivePriority: 1, "orderable": false, "class": "col-s",
				render: function (data, type, row) {
					return '<div>' +
						'<a title="Registro" style="cursor:pointer; color:#3c763d;" onClick="javascript:selInspe(\'' + row.cauditoriainspeccion + '\',\'' + row.desc_gral + '\',\'' + row.areacli + '\',\'' + row.lineaproc + '\',\'' + row.cusuarioconsultor + '\',\'' + row.periodo + '\',\'' + row.finspeccion + '\',\'' + row.cnorma + '\',\'' + row.csubnorma + '\',\'' + row.cchecklist + '\',\'' + row.cmodeloinforme + '\',\'' + row.cvalornoconformidad + '\',\'' + row.cformulaevaluacion + '\',\'' + row.ccriterioresultado + '\',\'' + row.dcomentario + '\',\'' + row.zctipoestadoservicio + '\',\'' + row.destado + '\',\'' + row.ccliente + '\',\'' + row.spermitircorrectivas + '\',\'' + row.sincluyeplan + '\',\'' + row.zctiposervicio + '\',\'' + row.splan + '\',\'' + row.fservicio + '\');">' +
						'<span class="fas fa-eye fa-2x" aria-hidden="true"></span>' +
						'</a>';
				}
			}*/
		],
		"columnDefs": [
			//{"targets": [0], "visible": true},
		],
		"rowCallback": function (row, data) {
			/*if(data.espeligro == "S"){
				$(row).css("color","red");
			}*/
		},
		"drawCallback": function (settings) {
			var api = this.api();
			var rows = api.rows({page: 'current'}).nodes();
			var last = null;
			var grupo;

			api.column([1], {}).data().each(function (ctra, i) {
				grupo = api.column(1).data()[i];

				if (last !== ctra) {
					$(rows).eq(i).before(
						'<tr class="group"><td colspan="8" class="subgroup">' + ctra.toUpperCase() + '</td></tr>'
					);
					last = ctra;
				}
			});
		}
	});
	otblListinspaudi.column(1).visible(false);
});

$('#tblListinspaudi tbody').on('dblclick', 'tr.group', function () {
	var rowsCollapse = $(this).nextUntil('.group');
	var id = $(this).find("td.subgroup:first-child").text().substr(1, 8);

	verInspeccion(id);
});

$('#tblListinspaudi tbody').on('click', 'tr.group', function () {
	var rowsCollapse = $(this).nextUntil('.group');
	$(rowsCollapse).toggleClass('hidden');
});

$('#btn-show-all-children').on('click', function () {
	otblListinspaudi.rows().every(function () {
		var rowsCollapse = $('.group').nextUntil('.group');
		$(rowsCollapse).removeClass('hidden');
	});
});

$('#btn-hide-all-children').on('click', function () {
	otblListinspaudi.rows().every(function () {
		var rowsCollapse = $('.group').nextUntil('.group');
		$(rowsCollapse).removeClass('hidden');
		$(rowsCollapse).toggleClass('hidden');
	});
});

$("#btnNuevo").click(function () {
	$("#modalCreactrlprov").modal('show');

	$('#frmCreactrlprov').trigger("reset");
	$('#mhdnAccionctrlprov').val('N');

	var vnfecha = new Date();
	var vnano = vnfecha.getFullYear();
	var vnmes = ('00' + vnfecha.getMonth());
	$('#mtxtregPeriodo').val(vnano + '-' + vnmes.substr(-2));

	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "at/ctrlprov/cregctrolprov/getcboclieserv",
		dataType: "JSON",
		async: true,
		success: function (result) {
			$('#cboregClie').html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboregClie');
		}
	});

	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "at/ctrlprov/cregctrolprov/getcbotipoestable",
		dataType: "JSON",
		async: true,
		success: function (result) {
			$('#cbotipoestable').html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cbotipoestable');
		}
	});

});

$("#cboregClie").change(function () {
	var v_cboregClie = $('#cboregClie').val();
	var params = {"ccliente": v_cboregClie};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "at/ctrlprov/cregctrolprov/getcboprovxclie",
		dataType: "JSON",
		async: true,
		data: params,
		success: function (result) {
			$("#cboregprovclie").html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboregprovclie');
		}
	});
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "at/ctrlprov/cregctrolprov/getcboareaclie",
		dataType: "JSON",
		async: true,
		data: params,
		success: function (result) {
			$("#cboregareaclie").html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboregareaclie');
		}
	});
});

$("#cboregprovclie").change(function () {

	var v_cboclie = $('#cboregClie').val();
	var v_cboprov = $('#cboregprovclie').val();

	var params = {"cproveedor": v_cboprov};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "at/ctrlprov/cregctrolprov/getcbomaqxprov",
		dataType: "JSON",
		async: true,
		data: params,
		success: function (result) {
			$("#cboregmaquiprov").html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboregmaquiprov');
		}
	});
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "at/ctrlprov/cregctrolprov/getcbocontacprinc",
		dataType: "JSON",
		async: true,
		data: params,
		success: function (result) {
			$("#cbocontacprinc").html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cbocontacprinc');
		}
	});
	cboEstablecimiento(v_cboclie, v_cboprov, '', 'P');
	$('#mtxtregdirestable').val('');
});

$("#cboregmaquiprov").change(function () {

	var v_cboclie = $('#cboregClie').val();
	var v_cboprov = $('#cboregprovclie').val();
	var v_cbomaqui = $('#cboregmaquiprov').val();
	var v_tipo = 'M'

	if (v_cbomaqui == '') {
		v_tipo = 'P'
	}

	cboEstablecimiento(v_cboclie, v_cboprov, v_cbomaqui, v_tipo);
	$('#mtxtregdirestable').val('');
});

$("#cboregestable").change(function () {
	var v_cboclie = $('#cboregClie').val();
	var v_cboestable = $('#cboregestable').val();

	var params = {
		"cestablecimiento": v_cboestable
	};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "at/ctrlprov/cregctrolprov/getcbolineaproc",
		dataType: "JSON",
		async: true,
		data: params,
		success: function (result) {
			$("#cboreglineaproc").html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboreglineaproc');
		}
	});

	var parametros = {
		"cestablecimiento": v_cboestable
	};
	var request = $.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "at/ctrlprov/cregctrolprov/getdirestable",
		dataType: "JSON",
		async: true,
		data: parametros,
		error: function () {
			alert('Error, No se puede autenticar por error = getdirestable');
		}
	});
	request.done(function (respuesta) {
		$.each(respuesta, function () {
			$('#mtxtregdirestable').val(this.DIRESTABLE);
		});
	});
});

$("#cbotipoestable").change(function () {

	var v_cbotipoestable = $('#cbotipoestable').val();

	var params = {"ctipoestable": v_cbotipoestable};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "at/ctrlprov/cregctrolprov/getmontotipoestable",
		dataType: "JSON",
		async: true,
		data: params,
		success: function (result) {
			$('#txtcostoestable').val(result);
		},
		error: function () {
			alert('Error, No se puede recuperar Monto del Costo');
		}
	});
});

cboEstablecimiento = function (cboclie, cboprov, cbomaqui, vtipo) {

	var params = {
		"ccliente": cboclie,
		"cproveedor": cboprov,
		"cmaquilador": cbomaqui,
		"tipo": vtipo

	};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "at/ctrlprov/cregctrolprov/getcboregestable",
		dataType: "JSON",
		async: true,
		data: params,
		success: function (result) {
			$("#cboregestable").html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = getcboregestable');
		}
	});
};

$('#mbtnCCreactrl').click(function () {
	$('#btnBuscar').click();
});

iniInspe = function (cusuarioconsultor, consultsreg, cnorma, csubnorma, cchecklist, ccliente, cvalornoconformidad, cmodeloinforme, cformulaevaluacion, ccriterioresultado) {
	var params = {"sregistro": consultsreg};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "at/ctrlprov/cregctrolprov/getcboinspector",
		dataType: "JSON",
		async: true,
		data: params,
		success: function (result) {
			$('#cboinspinspector').html(result);
			$('#cboinspinspector').val(cusuarioconsultor).trigger("change");
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboinspinspector');
		}
	});
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "at/ctrlprov/cregctrolprov/getcbosistemaip",
		dataType: "JSON",
		async: true,
		success: function (result) {
			$('#cboinspsistema').html(result);
			//$('#cboinspsistema').val(cnorma).trigger("change");
			document.getElementById("cboinspsistema").value = cnorma;
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboinspsistema');
		}
	});
	var paramsrubro = {"cnorma": cnorma};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "at/ctrlprov/cregctrolprov/getcborubroip",
		dataType: "JSON",
		async: true,
		data: paramsrubro,
		success: function (result) {
			$('#cboinsprubro').html(result);
			document.getElementById("cboinsprubro").value = csubnorma;
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboinsprubro');
		}
	});


	var paramschecklist = {
		"cnorma": cnorma,
		"csubnorma": csubnorma,
		"ccliente": ccliente,
	};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "at/ctrlprov/cregctrolprov/getcbochecklist",
		dataType: "JSON",
		async: true,
		data: paramschecklist,
		success: function (result) {
			$('#cboinspcchecklist').html(result);
			document.getElementById("cboinspcchecklist").value = cchecklist;
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboinspcchecklist');
		}
	});
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "at/ctrlprov/cregctrolprov/getcbomodinforme",
		dataType: "JSON",
		async: true,
		success: function (result) {
			$('#cboinspmodeloinfo').html(result);
			$('#cboinspmodeloinfo').val(cmodeloinforme).trigger("change");
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboinspmodeloinfo');
		}
	});
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "at/ctrlprov/cregctrolprov/getcboinspvalconf",
		dataType: "JSON",
		async: true,
		success: function (result) {
			$('#cboinspvalconf').html(result);
			$('#cboinspvalconf').val(cvalornoconformidad).trigger("change");
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboinspvalconf');
		}
	});
	var paramsform = {"cchecklist": cchecklist};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "at/ctrlprov/cregctrolprov/getcboinspformula",
		dataType: "JSON",
		async: true,
		data: paramsform,
		success: function (result) {
			$('#cboinspformula').html(result);
			$('#cboinspformula').val(cformulaevaluacion).trigger("change");
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboinspformula');
		}
	});
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "at/ctrlprov/cregctrolprov/getcboinspcritresul",
		dataType: "JSON",
		async: true,
		success: function (result) {
			$('#cboinspcritresul').html(result);
			$('#cboinspcritresul').val(ccriterioresultado).trigger("change");
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboinspcritresul');
		}
	});
};

quitarFecha = function () {
	$('#txtFInsp').val('Sin Fecha');
};

$("#cboinspsistema").change(function () {
	var v_cnorma = $("#cboinspsistema option:selected").attr("value");

	var paramsrubro = {"cnorma": v_cnorma};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "at/ctrlprov/cregctrolprov/getcborubroip",
		dataType: "JSON",
		async: true,
		data: paramsrubro,
		success: function (result) {
			$('#cboinsprubro').html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboinsprubro');
		}
	});
});

$("#cboinsprubro").change(function () {
	var v_csubnorma = $("#cboinsprubro option:selected").attr("value");
	var v_cnorma = $('#cboinspsistema').val();
	var v_ccliente = $('#mhdnccliente').val();
	var paramschecklist = {
		"cnorma": v_cnorma,
		"csubnorma": v_csubnorma,
		"ccliente": v_ccliente,
	};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "at/ctrlprov/cregctrolprov/getcbochecklist",
		dataType: "JSON",
		async: true,
		data: paramschecklist,
		success: function (result) {
			$('#cboinspcchecklist').html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboinspcchecklist');
		}
	});
});

$("#cboinspcchecklist").change(function () {
	var v_cchecklist = $("#cboinspcchecklist option:selected").attr("value");
	var paramsform = {"cchecklist": v_cchecklist};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "at/ctrlprov/cregctrolprov/getcboinspformula",
		dataType: "JSON",
		async: true,
		data: paramsform,
		success: function (result) {
			$('#cboinspformula').html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboinspformula');
		}
	});
});

selInspe = function (cauditoriainspeccion, desc_gral, areacli, lineaproc, cusuarioconsultor, periodo, fservicio, cnorma, csubnorma, cchecklist, cmodeloinforme, cvalornoconformidad, cformulaevaluacion, ccriterioresultado, dcomentario, zctipoestadoservicio, destado, ccliente, spermitircorrectivas, sincluyeplan, zctiposervicio, splan) {
	$('#tabctrlprov a[href="#tabctrlprov-det"]').tab('show');

	$('#frmRegInsp').trigger("reset");

	$('#mtxtidinsp').val(cauditoriainspeccion);
	$('#mfechainsp').val(fservicio);
	$('#mfservicio').val(fechaservicio);
	$('#mhdnAccioninsp').val('A');
	$('#mhdnccliente').val(ccliente);

	iniInspe(cusuarioconsultor, 'T', cnorma, csubnorma, cchecklist, ccliente, cvalornoconformidad, cmodeloinforme, cformulaevaluacion, ccriterioresultado);

	$('#btnGrabar').hide();

	if (splan == 0) {
		$('#mbtnnewplaninsp').show();
		$('#mbtnverplaninsp').hide();
	} else {
		$('#mbtnnewplaninsp').hide();
		$('#mbtnverplaninsp').show();
	}

	$('#mtxtinspdatos').val(desc_gral);
	$('#mtxtinsparea').val('AREÁ : ' + areacli);
	$('#mtxtinsplinea').val('LINEA : ' + lineaproc);

	$('#cboinspperiodo').val(periodo);
	$('#mtxtinspcoment').val(dcomentario);
	$('#mhdnzctipoestado').val(zctipoestadoservicio);
	$('#txtinspestado').val(destado);

	$('#txtFInspeccion').datetimepicker({
		format: 'DD/MM/YYYY',
		daysOfWeekDisabled: [0],
		locale: 'es',
		autoclose: true,
		todayBtn: true
	});
	if (fservicio == '01/01/1900') {
		$('#txtFInsp').val('Sin Fecha');
	} else {
		$('#txtFInsp').val(fservicio);
	}

};
fechaActualinsp = function (fservicio) {
	var fecha = new Date();
	var fechatring = ("0" + fecha.getDate()).slice(-2) + "/" + ("0" + (fecha.getMonth() + 1)).slice(-2) + "/" + fecha.getFullYear();
	$('#txtFInspeccion').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY'));

};

fprogramar = function (cauditoriainspeccion, desc_gral, areacli, lineaproc, cusuarioconsultor, periodo, fservicio, zctipoestadoservicio, destado, ccliente, spermitircorrectivas, sincluyeplan, zctiposervicio, cproveedor, cmaquilador, dcliente, splan, fechaservicio) {
	$('#tabctrlprov a[href="#tabctrlprov-det"]').tab('show');

	$('#frmRegInsp').trigger("reset");

	$('#mtxtidinsp').val(cauditoriainspeccion);
	$('#mfechainsp').val(fservicio);
	$('#mfservicio').val(fechaservicio);
	$('#mhdnAccioninsp').val('A');
	$('#mhdnccliente').val(ccliente);
	$('#mhdncproveedor').val(cproveedor);
	$('#mhdncmaquilador').val(cmaquilador);

	$('#mhdndcliente').val(dcliente);

	if (splan == 0) {
		$('#mbtnnewplaninsp').show();
		$('#mbtnverplaninsp').hide();
	} else {
		$('#mbtnnewplaninsp').hide();
		$('#mbtnverplaninsp').show();
	}

	iniInspe(cusuarioconsultor, 'T', '', '', '', ccliente, '', '', '', '');

	$('#mtxtinspdatos').val(desc_gral);
	$('#mtxtinsparea').val('AREÁ : ' + areacli);
	$('#mtxtinsplinea').val('LINEA : ' + lineaproc);

	$('#cboinspperiodo').val(periodo);
	$('#mhdnzctipoestado').val(zctipoestadoservicio);
	$('#txtinspestado').val(destado);
	$('#cbotiposerv').val(zctiposervicio).trigger("change");

	if (spermitircorrectivas == 'S') {
		$("#chkaacc").prop("checked", true);
	} else {
		$("#chkaacc").prop("checked", false);
	}
	if (sincluyeplan == 'S') {
		$("#chkplaninsp").prop("checked", true);
		$('#Btnplan').show();
	} else {
		$("#chkplaninsp").prop("checked", false);
		$('#Btnplan').hide();
	}

	$('#txtFInspeccion').datetimepicker({
		format: 'DD/MM/YYYY',
		daysOfWeekDisabled: [0],
		locale: 'es',
		autoclose: true,
		todayBtn: true
	});
	if (fservicio == '01/01/1900') {
		$('#txtFInsp').val('Sin Fecha');
	} else {
		$('#txtFInsp').val(fservicio);
	}

	$('#mtxtHsalidaperm').datetimepicker({
		format: 'hh:mm A',
		locale: 'es',
		stepping: 15
	});
	$('#mtxtHsalidaperm').datetimepicker('minDate', moment('08:00 AM', 'hh:mm A'));
	$('#mtxtHsalidaperm').datetimepicker('maxDate', moment('05:45 PM', 'hh:mm A'));
	$('#mtxtHsalidaperm').datetimepicker('date', moment('08:00 AM', 'hh:mm A'));


};

$('#modalPlaninsp').on('shown.bs.modal', function (e) {

	$('#frmPlaninsp').trigger("reset");

	if ($('#cbotiposerv').val() == '013') {
		$('#divfplan').hide();
		document.querySelector('#htitleplan').innerText = 'Plan de Inspección';
	} else {
		$('#divfplan').show();
		document.querySelector('#htitleplan').innerText = 'Plan de Inspección - Inopinada';
	}


	$('#mtxtFplaninspeccion').datetimepicker({
		format: 'DD/MM/YYYY',
		daysOfWeekDisabled: [0],
		locale: 'es'
	});

	$('#mtxtHplaninspeccion').datetimepicker({
		format: 'LT'
	});

	var v_ccliente = $('#mhdnccliente').val();
	var v_cproveedor = $('#mhdncproveedor').val();
	var v_cmaquilador = $('#mhdncmaquilador').val();
	var params = {
		"ccliente": v_ccliente,
		"cproveedor": v_cproveedor,
		"cmaquilador": v_cmaquilador,
	};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "at/ctrlprov/cregctrolprov/getcbocontacplanins",
		dataType: "JSON",
		async: true,
		data: params,
		success: function (result) {
			$("#cbocontacplanins").html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cbocontacplanins');
		}
	})

	fechaActualplaninsp();

	var v_dcliente = $('#mhdndcliente').val();
	var v_dlineaproc = $('#mtxtinsplinea').val();

	$('#mhdnidinspplaninsp').val($('#mtxtidinsp').val());
	$('#mhdnfservicioplaninsp').val($('#mfechainsp').val());
	$('#mhdntiposervplaninsp').val($('#cbotiposerv').val());
	$('#mtxtobjeplaninsp').val('Verificar el cumplimiento de los criterios de inspección, que permita a ' + v_dcliente.toUpperCase() + ' asegurar la calidad sanitaria de los productos comercializados');
	$('#mtxtalcanplaninsp').val(v_dlineaproc);
	$('#mtxtrequeplaninsp').val('Estar Procesando');

});
fechaActualplaninsp = function () {
	var fecha = new Date();
	var fechatring = ("0" + fecha.getDate()).slice(-2) + "/" + ("0" + (fecha.getMonth() + 1)).slice(-2) + "/" + fecha.getFullYear();
	$('#mtxtFplaninspeccion').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY'));

};
$('#mbtnverplaninsp').click(function () {
	var v_idinspeccion = $('#mtxtidinsp').val();
	var v_fservicio = $('#mfservicio').val();
	window.open(baseurl + "at/ctrlprov/cregctrolprov/genpdfplaninsp/" + v_idinspeccion + "/" + v_fservicio);
});

$('#modalConvalidacion').on('shown.bs.modal', function (e) {
	$("#txtconvaliidinsp").prop({readonly: true});
	$("#txtconvalifservicio").prop({readonly: true});

	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "at/ctrlprov/cregctrolprov/getcbocertificadora",
		dataType: "JSON",
		async: true,
		success: function (result) {
			$("#cbocertificadora").html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cbocertificadora');
		}
	})


});
fconvalidar = function (cauditoriainspeccion, fservicio, ccriterioresultado) {
	$('#frmConvalidacion').trigger("reset");

	$('#txtconvaliidinsp').val(cauditoriainspeccion);
	$('#txtconvalifservicio').val(fservicio);
	$('#mhdncritresultconvali').val(ccriterioresultado);
	$('#mhdnAccionconvali').val('N');

	$('#txtcierreFConvalidacion').datetimepicker({
		format: 'DD/MM/YYYY',
		daysOfWeekDisabled: [0],
		locale: 'es',
		autoclose: true,
		todayBtn: true
	});
	fechaActualconval(fservicio);
};
fechaActualconval = function (fservicio) {
	var fecha = new Date();
	var fechatring = ("0" + (fecha.getDate() + 1)).slice(-2) + "/" + ("0" + (fecha.getMonth() + 1)).slice(-2) + "/" + fecha.getFullYear();
	$('#txtcierreFConvalidacion').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY'));
	$('#txtcierreFConvalidacion').datetimepicker('minDate', moment(fservicio, 'DD/MM/YYYY'));
};
$("#cbocertificadora").change(function () {
	var idcertificadora = $('#cbocertificadora').val();
	var parametros = {
		"ccertificadora": idcertificadora
	};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "at/ctrlprov/cregctrolprov/getcbocertificacion",
		dataType: "JSON",
		async: true,
		data: parametros,
		success: function (result) {
			$("#cbocertificacion").html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cbocertificacion');
		}
	})
});
txtconvaliresul.oninput = function () {
	var v_resul = txtconvaliresul.value;
	var v_ccriterio = $('#mhdncritresultconvali').val();

	var parametros = {
		"resultado": v_resul,
		"ccriterio": v_ccriterio
	};

	$.ajax({
		type: 'ajax',
		url: baseurl + "at/ctrlprov/cregctrolprov/calculocriterio",
		type: 'post',
		dataType: "JSON",
		data: parametros,
		success: function (result) {
			$.each(result, function (key, value) {
				v_nr = value.nr;
				v_ng = value.ng;
				v_nb = value.nb;
				document.getElementById("divcolor").style.backgroundColor = 'rgb(' + v_nr + ',' + v_ng + ',' + v_nb + ')';
				$('#txtconvalivalor').val(value.ddetallecriterioresultado);
				$('#txtconvalimes').val(value.nmes);
				$('#mhdncritdetresultconvali').val(value.cdetallecriterioresultado);
			});
		}
	});
};
escogerArchivo = function () {
	var archivoInput = document.getElementById('mtxtArchivoconvali');
	var archivoRuta = archivoInput.value;
	var extPermitidas = /(.pdf|.docx|.xlsx|.doc|.xls)$/i;

	var filename = $('#mtxtArchivoconvali').val().replace(/.*(\/|\\)/, '');
	$('#mtxtNomarchconvali').val(filename);

	if (!extPermitidas.exec(archivoRuta)) {
		alert('Asegurese de haber seleccionado un PDF, DOCX, XSLX');
		archivoInput.value = '';
		$('#mtxtNomarchconvali').val('');
		return false;
	}
	$('#sArchivo').val('S');
};
subirArchivoconval = function () {
	var parametrotxt = new FormData($("#frmCreaPropu")[0]);
	var request = $.ajax({
		data: parametrotxt,
		method: 'post',
		url: baseurl + "pt/cpropuesta/subirArchivoconval/",
		dataType: "JSON",
		async: true,
		contentType: false,
		processData: false,
		error: function () {
			alert('Error, no se cargó el archivo');
		}
	});
	request.done(function (respuesta) {
		$('#btnBuscar').click();
		Vtitle = 'Guardo Correctamente';
		Vtype = 'success';
		sweetalert(Vtitle, Vtype);
		$('#mbtnCCreaPropu').click();
	});
};


$('#modalCierreespecial').on('shown.bs.modal', function (e) {
	$("#txtcierreidinsp").prop({readonly: true});
	$("#txtcierrefservicio").prop({readonly: true});

	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "at/ctrlprov/cregctrolprov/getcbocierreTipo",
		dataType: "JSON",
		async: true,
		success: function (result) {
			$("#cbocierreTipo").html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cbocierreTipo');
		}
	})
});
fcierreespecial = function (cauditoriainspeccion, desc_gral, areacli, lineaproc, fservicio, zctipoestadoservicio, destado, ccliente) {

	$('#frmCierreespecial').trigger("reset");

	$('#txtcierreidinsp').val(cauditoriainspeccion);
	$('#mhdncierrefservicio').val(fservicio);
	$('#mhdnAccioncierre').val('N');
	$('#txtcierreFProgramado,#txtcierrefservicio').datetimepicker({
		format: 'DD/MM/YYYY',
		daysOfWeekDisabled: [0],
		locale: 'es',
		autoclose: true,
		todayBtn: true
	});
	fechaActualcierre();
	$('#divcierreprograma').hide();
	$('#divcierremonto').hide();
	$('#divcierreviatico').hide();

};

$("#cbocierreTipo").change(function () {
	var idcierretipo = $('#cbocierreTipo').val();
	var valor = $("option:selected", this).attr("data-lat");
	if (valor == '0') {
		$('#divcierreprograma').hide();
		if (idcierretipo == '874') {
			$('#divcierremonto').show();
			$('#divcierreviatico').show();
		} else {
			$('#divcierremonto').hide();
			$('#divcierreviatico').hide();
		}
		$('#txtcierreFProg').val('');
		$('#mhdnfprog').val('0');
	} else {
		$('#divcierreprograma').show();
		if (idcierretipo == '515') {
			$('#divcierremonto').show();
			$('#divcierreviatico').show();
		} else {
			$('#divcierremonto').hide();
			$('#divcierreviatico').hide();
		}
		fechaActualcierreprog();
		$('#mhdnfprog').val('1');
	}
});

fechaActualcierre = function (fservicio) {
	var fecha = new Date();
	var fechatring = ("0" + fecha.getDate()).slice(-2) + "/" + ("0" + (fecha.getMonth() + 1)).slice(-2) + "/" + fecha.getFullYear();
	$('#txtcierrefservicio').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY'));
};
fechaActualcierreprog = function (fservicio) {
	var fecha = new Date();
	var fechatring = ("0" + (fecha.getDate() + 1)).slice(-2) + "/" + ("0" + (fecha.getMonth() + 1)).slice(-2) + "/" + fecha.getFullYear();
	$('#txtcierreFProgramado').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY'));
	$('#txtcierreFProgramado').datetimepicker('minDate', moment(fechatring, 'DD/MM/YYYY'));
};


$("body").on("click", "#aReaperturar", function (event) {
	event.preventDefault();

	cauditoriainspeccion = $(this).attr("href");
	fservicio = $(this).attr("fservicio");
	cusuario = $('#hdncusuario').val();

	var params = {
		"cauditoriainspeccion": cauditoriainspeccion,
		"fservicio": fservicio,
		"cusuario": cusuario,
	};

	Swal.fire({
		title: 'Confirmar Reaperturar Inspeccion',
		text: "¿Está seguro de Reaperturar Inspeccion?",
		type: 'error',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Si, Reaperturar!'
	}).then((result) => {
		if (result.value) {
			var request = $.ajax({
				type: 'POST',
				url: baseurl + "at/ctrlprov/cregctrolprov/setreaperturar/",
				async: true,
				data: params,
				error: function () {
					Vtitle = 'Error en Guardar!!!';
					Vtype = 'error';
					sweetalert(Vtitle, Vtype);
				}
			});
			request.done(function (respuesta) {
				var posts = JSON.parse(respuesta);

				$.each(posts, function () {
					Vtitle = 'Se Re-aperturo Correctamente!!!';
					Vtype = 'success';
					sweetalert(Vtitle, Vtype);
					$('#btnBuscar').click();
				});
			});
		}
	})

});


$("#chkplaninsp").on("change", function () {
	if ($("#chkplaninsp").is(":checked") == true) {
		$('#Btnplan').show();
	} else if ($("#chkplaninsp").is(":checked") == false) {
		$('#Btnplan').hide();
	}
	;
});


$('#btnRetornarLista').click(function () {
	$('#tabctrlprov a[href="#tabctrlprov-list"]').tab('show');
	$('#btnBuscar').click();
});

verInspeccion = function (id) {
	$("#modalCreactrlprov").modal('show');

	var parametros = {
		"idinspeccion": id
	};
	var request = $.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "at/ctrlprov/cregctrolprov/getrecuperainsp",
		dataType: "JSON",
		async: true,
		data: parametros,
		error: function () {
			alert('Error, no se puede cargar la lista desplegable de establecimiento');
		}
	});
	request.done(function (respuesta) {
		$.each(respuesta, function () {
			var $idtipoproducto = this.id_tipoproducto;
			var $idsiparticula = this.id_siparticula;

			$('#txtNombprodReg06').val(this.nombre_producto);
			$('#txtPHmatprimaReg06').val(this.ph_materia_prima);
			$('#txtPHprodfinReg06').val(this.ph_producto_final);

			$('#cbollevapartReg06').val(this.particulas).trigger("change");


		});
	});
};

cargarInspeccion = function (id, idclie, idprov, idmaq, idestable, idarea, idlinea) {
	var params = {
		"idptregestudio": v_RegEstu
	};

	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "pt/cinforme/getTipoproducto",
		dataType: "JSON",
		async: true,
		data: params,
		success: function (result) {
			$("#cboTipoprodReg06").html(result);
			$('#cboTipoprodReg06').val($idtipoproducto).trigger("change");
		},
		error: function () {
			alert('Error, no se puede cargar la lista desplegable de establecimiento');
		}
	});
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "pt/cinforme/getParticulas",
		dataType: "JSON",
		async: true,
		data: params,
		success: function (result) {
			$("#cboParticulasReg06").html(result);
			$('#cboParticulasReg06').val($idsiparticula).trigger("change");
		},
		error: function () {
			alert('Error, no se puede cargar la lista desplegable de establecimiento');
		}
	})
};
