const objDocumento = {};

$(function () {
    objDocumento.data = {};

    objDocumento.guardarArchivo = function(){
        const button = $(this);
        var codigoEval = $('#id_eval').val();
        var formData = new FormData($('#form_cargar_doc')[0]);
        $.ajax({
            url: 'ar/evalcenco/cconteval/cargar_archivo',
            method: 'POST',
            data: formData,
            cache:false,
            contentType:false,
            processData:false,
            beforeSend: function() {
                
            },
        }).done(function(res) {
            objPrincipal.notify('success', res.message);
            filtroEval(codigoEval);
            $('#cerrarMdlArchivos').click();
        }).fail(function(jqxhr) {
            const message = (jqxhr && jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la solicitud del servidor.';
            objPrincipal.notify('error', message);
        }).always(function() {

        });
    };

    objDocumento.abrirModal = function(){
        var button = $(this);
        var idbutton = button.attr('id');
        var aD = idbutton.split('-');
        var numeroTramite = aD[3];
        var ideval = $('#id_eval').val();
        var idProducto = $('#id_producto').val();
        $('#id_tramite').val(numeroTramite);
        $('#id_eval_modal').val(ideval);
        $('#id_producto_modal').val(idProducto);
        estadodoc = $('#estado_'+ numeroTramite+' option:selected').val();
        $('#estado_documento').val(estadodoc);
        $('#inputdocumento').val('');
    };

    objDocumento.abrirOpciones = function(){
        var button = $(this);
        var idbutton = button.attr('id');
        var ideval = $('#id_eval').val();
        var aD = idbutton.split('-');
        var numeroTramite = aD[3];

        $.ajax({
            url: 'ar/evalcenco/cconteval/opciones_documentos',
            method: 'POST',
            data: {
                numeroTramite:numeroTramite,
                ideval:ideval
            },
            dataType: 'json',
            beforeSend: function() {
                
            },
        }).done(function(res) {
            objPrincipal.notify('success', res.message);
            $('#numero_tramite').val(numeroTramite);
            var documentos = res.data.documentos;
            verDocumentos(documentos);
        }).fail(function(jqxhr) {
            const message = (jqxhr && jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la solicitud del servidor.';
            objPrincipal.notify('error', message);
        }).always(function() {

        });
    };

    verDocumentos = function(documentos){
        var row = '';
        var rutaDoc = "http://plataforma.grupofs.com:82/intranet";
        //var rutaDoc = "http://localhost/demointranet";
        if(documentos.DOC1){
            nombreDoc = '<a href="'+rutaDoc+documentos.RUTA1+'" target="_blank"><i class="fas fa-cloud-download-alt" data-toggle="tooltip"></i>'+documentos.DOC1+'</a>';
            row += '<tr>';
            row += '<td class="text-center">1</td><td>'+nombreDoc+'</td>';
            row += '<td class="text-center">'+
            '<button type="button" role="button" class="btn btn-danger btnEliminar" id="btnEliminar-1"><i class="fa fa-fw fa-times"></i></button>'+
            '</td>';
            row += '</tr>';
        }
        if(documentos.DOC2){
            nombreDoc2 = '<a href="'+rutaDoc+documentos.RUTA2+'" target="_blank"><i class="fas fa-cloud-download-alt" data-toggle="tooltip"></i>'+documentos.DOC2+'</a>';
            row += '<tr>';
            row += '<td class="text-center">2</td><td>'+nombreDoc2+'</td>';
            row += '<td class="text-center">'+
            '<button type="button" role="button" class="btn btn-danger btnEliminar" id="btnEliminar-2"><i class="fa fa-fw fa-times"></i></button>'+
            '</td>';
            row += '</tr>';
        }
        if(documentos.DOC3){
            nombreDoc3 = '<a href="'+rutaDoc+documentos.RUTA3+'" target="_blank"><i class="fas fa-cloud-download-alt" data-toggle="tooltip"></i>'+documentos.DOC3+'</a>';
            row += '<tr>';
            row += '<td class="text-center">3</td><td>'+nombreDoc3+'</td>';
            row += '<td class="text-center">'+
            '<button type="button" role="button" class="btn btn-danger btnEliminar" id="btnEliminar-3"><i class="fa fa-fw fa-times"></i></button>'+
            '</td>';
            row += '</tr>';
        }
        if(documentos.DOC4){
            nombreDoc4 = '<a href="'+rutaDoc+documentos.RUTA4+'" target="_blank"><i class="fas fa-cloud-download-alt" data-toggle="tooltip"></i>'+documentos.DOC4+'</a>';
            row += '<tr>';
            row += '<td class="text-center">4</td><td>'+nombreDoc4+'</td>';
            row += '<td class="text-center">'+
            '<button type="button" role="button" class="btn btn-danger btnEliminar" id="btnEliminar-4"><i class="fa fa-fw fa-times"></i></button>'+
            '</td>';
            row += '</tr>';
        }
        if(documentos.DOC5){
            nombreDoc5 = '<a href="'+rutaDoc+documentos.RUTA5+'" target="_blank"><i class="fas fa-cloud-download-alt" data-toggle="tooltip"></i>'+documentos.DOC5+'</a>';
            row += '<tr>';
            row += '<td class="text-center">5</td><td>'+nombreDoc5+'</td>';
            row += '<td class="text-center">'+
            '<button type="button" role="button" class="btn btn-danger btnEliminar" id="btnEliminar-5"><i class="fa fa-fw fa-times"></i></button>'+
            '</td>';
            row += '</tr>';
        }
        $('#listaDocumentosTramite tbody').html(row);
        $('.btnEliminar').click(objDocumento.eliminarDocumento);
    };

    objDocumento.eliminarDocumento = function(){
        var button = $(this);
        var idbutton = button.attr('id');
        var ideval = $('#id_eval').val();
        var numeroTramite = $('#numero_tramite').val();
        var aD = idbutton.split('-');
        var numeroDocumento = aD[1];

        $.ajax({
            url: 'ar/evalcenco/cconteval/eliminar_documento',
            method: 'POST',
            data: {
                numeroTramite:numeroTramite,
                ideval:ideval,
                numeroDocumento:numeroDocumento
            },
            dataType: 'json',
            beforeSend: function() {
                
            },
        }).done(function(res) {
            objPrincipal.notify('success', res.message);
            var documentos = res.data.documentos;
            verDocumentos(documentos);
        }).fail(function(jqxhr) {
            const message = (jqxhr && jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la solicitud del servidor.';
            objPrincipal.notify('error', message);
        }).always(function() {

        });

    };

    objDocumento.cerrarOpciones = function(){
        var ideval = $('#id_eval').val();
        filtroEval(ideval);
        $('#cerrarMdlOpciones').click();
    };
});

$(document).ready(function() {
    $('#btnSaveUploadFile').click(objDocumento.guardarArchivo);
    $('.btn-cargar-documento').click(objDocumento.abrirModal);
    $('.btn-opciones-documento').click(objDocumento.abrirOpciones);
    $('.btnEliminar').click(objDocumento.eliminarDocumento);
    $('#btnCerrarOpciones').click(objDocumento.cerrarOpciones);
});