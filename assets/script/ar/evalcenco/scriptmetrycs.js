const objMetrycs = {};

$(function () {
    objMetrycs.data = {};
    var myChart;
    var myChartS;

    function cargarDataDistri(datos){
        var total = datos['Aprobados']+datos['Observados']+datos['Truncos'];
        var row = '<tr><td>Aprobados</td><td>'+ datos['Aprobados'] +'</td><td>'+ (datos['Aprobados']*100/total).toFixed(2) +'%</td></tr>'+
        '<tr><td>Observados</td><td>'+ datos['Observados'] +'</td><td>'+ (datos['Observados']*100/total).toFixed(2) +'%</td></tr>'+
        '<tr><td>Truncos</td><td>'+ datos['Truncos'] +'</td><td>'+ (datos['Truncos']*100/total).toFixed(2) +'%</td></tr>';
        $('#tbltendenciaanualdistri tbody').html(row);

        var ctx = document.getElementById('graftendenciaanualdistri').getContext('2d');
        if(myChart){
            myChart.destroy();
        }
        myChart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: ['Aprobados', 'Observados', 'Truncos'],
                datasets: [{
                    label: '# of Evaluaciones',
                    data: [datos['Aprobados'], datos['Observados'], datos['Truncos']],
                    backgroundColor: [
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(255, 206, 86, 0.2)'
                    ],
                    borderColor: [
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 99, 132, 1)',
                        'rgba(255, 206, 86, 1)'
                    ],
                    borderWidth: 1,
                    hoverOffset:4
                }]
            }
        });
    }

    objMetrycs.cargarDistri = function(){
        var parametros = {
			"anio" : $('#cboAnio').val(),
			"mes" : $('#cboMes').val(),
		};

		$.ajax({
			type: 'ajax',
			url:  baseurl+"ar/evalcenco/cmetrycs/getDistri",
			type:  'POST',
			dataType: "JSON",
			data: parametros,
			success: function(result){
				cargarDataDistri(result);
			}
		});
    }

    function cargarDataSoli(datos){
        var meses = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Setiembre','Octubre','Noviembre','Diciembre'];
        var row='';
        var total=[];
        for(var i=1;i<=12;i++){
            var dato = datos[i];
            total[i-1] = dato['A']+dato['O']+dato['T'];
            row += '<tr><td>'+meses[i-1]+'</td><td>'+ dato['A'] +'</td><td>'+ dato['O'] +'</td><td>'+ dato['T'] +'</td><td>'+ total[i-1] +'</td></tr>';
        }
        $('#tbltendenciaanualsoli tbody').html(row);

        var ctx = document.getElementById('graftendenciaanualsoli').getContext('2d');
        if(myChartS){
            myChartS.destroy();
        }
        myChartS = new Chart(ctx, {
            data: {
                labels: meses,
                datasets: [{
                    type: 'bar',
                    label: 'Solicitudes por Mes',
                    data: total,
                    backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(255, 99, 132, 0.2)'
                    ],
                    borderColor: [
                    'rgb(255, 99, 132)',
                    'rgb(255, 99, 132)',
                    'rgb(255, 99, 132)',
                    'rgb(255, 99, 132)',
                    'rgb(255, 99, 132)',
                    'rgb(255, 99, 132)',
                    'rgb(255, 99, 132)',
                    'rgb(255, 99, 132)',
                    'rgb(255, 99, 132)',
                    'rgb(255, 99, 132)',
                    'rgb(255, 99, 132)',
                    'rgb(255, 99, 132)'
                    ],
                    borderWidth: 1
                },{
                    type: 'line',
                    label: 'Solicitudes por Mes',
                    data: total,
                    backgroundColor: [
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(54, 162, 235, 0.2)'
                    ],
                    borderColor: [
                    'rgb(54, 162, 235)',
                    'rgb(54, 162, 235)',
                    'rgb(54, 162, 235)',
                    'rgb(54, 162, 235)',
                    'rgb(54, 162, 235)',
                    'rgb(54, 162, 235)',
                    'rgb(54, 162, 235)',
                    'rgb(54, 162, 235)',
                    'rgb(54, 162, 235)',
                    'rgb(54, 162, 235)',
                    'rgb(54, 162, 235)',
                    'rgb(54, 162, 235)'
                    ],
                    borderWidth: 1
                }]
            }
        });
    }

    objMetrycs.cargarSoli = function(){
        var parametros = {
			"anio" : $('#cboAnioSoli').val(),
		};

		$.ajax({
			type: 'ajax',
			url:  baseurl+"ar/evalcenco/cmetrycs/getSoli",
			type:  'POST',
			dataType: "JSON",
			data: parametros,
			success: function(result){
				cargarDataSoli(result);
			}
		});
    }

    
});

$(document).ready(function() {
    objMetrycs.cargarDistri();
    $('#btnBuscarDistri').click(objMetrycs.cargarDistri);
    objMetrycs.cargarSoli();
    $('#btnBuscarSoli').click(objMetrycs.cargarSoli);
});

