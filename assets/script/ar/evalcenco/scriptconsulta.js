const objFormularioBusqueda = {};

$(function () {
    objFormularioBusqueda.data = {};

    objFormularioBusqueda.cargarResultados = function(dataResultados) {
        row='';
        dataResultados.forEach(function(item, pos) {
            row += '<tr>';
            row += '<td>'+ item.CODIGOEVAL+'</td>';
            row += '<td>'+ item.NOMBREPRODUCTO+'</td>';
            row += '<td>'+ item.REGSANITARIO+'</td>';
            row += '<td>'+ item.VIGENCIARS+'</td>';
            if(item.ESTADOEVAL == 1){
                row += '<td>APROBADO</td>';
            }else if(item.ESTADOEVAL == 2){
                row += '<td>OBSERVADO</td>';
            }else if(item.ESTADOEVAL == 3){
                row += '<td>TRUNCO</td>';
            }
            row += '<td>'+ item.FECHAINICIO+'</td>';
            if(item.FECHACIERRE!=null){
                row += '<td>'+ item.FECHACIERRE+'</td>';
            }else{
                row += '<td></td>';
            }
            row += '<td>'+ '<div>'+
            '<a data-original-title="Listar Documentos" data-toggle="modal" style="cursor:pointer;color:#3c763d;" data-target="#modalListdocumentos" onClick="javascript:buscarTramites('+ item.CODIGOEVAL +');"><i class="far fa-folder-open fa-2x" data-original-title="Listar Documentos" data-toggle="tooltip"></i></a>'+
            '</div>'+'</td>';
            
            row += '</tr>';
            
        });
        $('#tblResultadosEval tbody').html(row);
    };

    buscarTramites = function(codigoEval){
        $.ajax({
            url: 'ar/evalcenco/cconseval/buscarDocumentos',
            method: 'POST',
            data: {codigoeval:codigoEval},
            dataType: 'json',
            beforeSend: function() {
                
            },
        }).done(function(res) {
            objPrincipal.notify('success', res.message);
            var datos = res.data.dataresultados;
            var numero = 1;
            datos.forEach(function(item,pos){
                var rutaDoc = "http://plataforma.grupofs.com:82/intranet";
                //var rutaDoc = "http://localhost/demointranet";
                var tr = $('tr#tramite_'+numero);
                var span = tr.find('span#doc_adjunto_'+numero);
                var nombreDoc = '';
                if(item.NOMBREDOCUMENTO){
                    nombreDoc += '<a href="'+rutaDoc+item.RUTADOCUMENTO+'" target="_blank" id="download_'+numero+'_1"><i class="fas fa-cloud-download-alt" data-toggle="tooltip"></i>'+item.NOMBREDOCUMENTO+'</a>';
                }
                if(item.NOMBREDOCUMENTO2){
                    nombreDoc += "<br>"+'<a href="'+rutaDoc+item.RUTADOCUMENTO2+'" target="_blank" id="download_'+numero+'_2"><i class="fas fa-cloud-download-alt" data-toggle="tooltip"></i>'+item.NOMBREDOCUMENTO2+'</a>';
                }
                if(item.NOMBREDOCUMENTO3){
                    nombreDoc += "<br>"+'<a href="'+rutaDoc+item.RUTADOCUMENTO3+'" target="_blank" id="download_'+numero+'_3"><i class="fas fa-cloud-download-alt" data-toggle="tooltip"></i>'+item.NOMBREDOCUMENTO3+'</a>';
                }
                if(item.NOMBREDOCUMENTO4){
                    nombreDoc += "<br>"+'<a href="'+rutaDoc+item.RUTADOCUMENTO4+'" target="_blank" id="download_'+numero+'_4"><i class="fas fa-cloud-download-alt" data-toggle="tooltip"></i>'+item.NOMBREDOCUMENTO4+'</a>';
                }
                if(item.NOMBREDOCUMENTO5){
                    nombreDoc += "<br>"+'<a href="'+rutaDoc+item.RUTADOCUMENTO5+'" target="_blank" id="download_'+numero+'_5"><i class="fas fa-cloud-download-alt" data-toggle="tooltip"></i>'+item.NOMBREDOCUMENTO5+'</a>';
                }
                span.html(nombreDoc);
                var estado = tr.find('span.estado_tramite_'+numero);
                if(item.ESTADOTRAMITE == '0'){
                    estado.html("Pendiente");
                }else if(item.ESTADOTRAMITE == '1'){
                    estado.html("Aprobado");
                }else if(item.ESTADOTRAMITE == '2'){
                    estado.html("Observado");
                }else if(item.ESTADOTRAMITE == '3'){
                    estado.html("No aplica");
                }
                //var btnDescargar = tr.find('a.download_'+numero);
                ++numero;
            });
        }).fail(function(jqxhr) {
            const message = (jqxhr && jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la solicitud del servidor.';
            objPrincipal.notify('error', message);
        }).always(function() {

        });
    };

    objFormularioBusqueda.buscar = function() {
        const button = $(this);
        const form = $('#frmConsultaEval');
        $.ajax({
            url: form.attr('action'),
            method: 'POST',
            data: form.serialize() +
                '&slc_fabricante=' + $('#slc_fabricante option:selected').val()+
                '&estado_eval=' + $('#estado_eval option:selected').val(),
            dataType: 'json',
            beforeSend: function() {
                
            },
        }).done(function(res) {
            objPrincipal.notify('success', res.message);
            datos = res.data.dataresultados;
            objFormularioBusqueda.cargarResultados(datos);
        }).fail(function(jqxhr) {
            const message = (jqxhr && jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la solicitud del servidor.';
            objPrincipal.notify('error', message);
        }).always(function() {

        });
    };

    objFormularioBusqueda.cargarFabricantes = function(){
        $.ajax({
            url: 'ar/evalcenco/cconseval/cargarFabricantes',
            method: 'POST',
            data: {},
            dataType: 'json',
            beforeSend: function() {
                
            },
        }).done(function(res) {
            objPrincipal.notify('success', res.message);
            fabricantes = res.data.dataFabricantes;
            options = '<option value="0" selected="selected">Todos</option>';
            fabricantes.forEach(function(item,pos){
                options += '<option value="'+item.IDFABRICANTE+'">'+item.NOMBREFABRICANTE+'</option>'
            });
            $('#slc_fabricante').html(options);
        }).fail(function(jqxhr) {
            const message = (jqxhr && jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la solicitud del servidor.';
            objPrincipal.notify('error', message);
        }).always(function() {

        });
    };
});


$(document).ready(function() {
    $('#btnBuscar').click(objFormularioBusqueda.buscar);
    objFormularioBusqueda.cargarFabricantes();
    objFormularioBusqueda.buscar();

});