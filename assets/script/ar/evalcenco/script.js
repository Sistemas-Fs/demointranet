const objFormularioEval = {};

$(function () {

	objFormularioEval.data = {};

    objFormularioEval.imprData = function(datos) {
        const currentDate = moment(datos.FECHACIERRE, 'YYYY-MM-DD HH:mm:ss').format('DD/MM/YYYY');
		let endDate = '';
		if (datos.FECHACIERRE) {
			endDate = moment(datos.FECHACIERRE, 'YYYY-MM-DD HH:mm:ss').format('DD/MM/YYYY');
		}

        $('#codigo_eval').val(datos.IDEVAL);
        $('#id_eval').val(datos.IDEVAL);
        $('#id_eval_modal').val(datos.IDEVAL);
        $('#fecha_cierre').val(endDate);
        $('#btnGuardarEval').hide();
        $('#btnActualizarEval').show();
        objFormularioEval.btnCerrarEval();
    };

    objFormularioEval.guardarEvaluacion = function() {
        const button = $(this);
        const form = $('#formEvalu');
        var cerrar = 0;
        $.ajax({
            url: form.attr('action'),
            method: 'POST',
            data: form.serialize() +
                '&estado_eval=' + $('#estado_eval option:selected').val()+
                '&cerrar=' + cerrar,
            dataType: 'json',
            beforeSend: function() {
                
            },
        }).done(function(res) {
            objPrincipal.notify('success', res.message);
            datos = res.data.dataEval;    
            objFormularioEval.imprData(datos);
        }).fail(function(jqxhr) {
            const message = (jqxhr && jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la solicitud del servidor.';
            objPrincipal.notify('error', message);
        }).always(function() {

        });
    };

    objFormularioEval.cerrarEvaluacion = function(){
        const button = $(this);
        const form = $('#formEvalu');
        var cerrar = 1;
        $.ajax({
            url: form.attr('action'),
            method: 'POST',
            data: form.serialize() +
                '&estado_eval=' + $('#estado_eval option:selected').val()+
                '&cerrar=' + cerrar,
            dataType: 'json',
            beforeSend: function() {
                
            },
        }).done(function(res) {
            objPrincipal.notify('success', res.message);
            datos = res.data.dataEval;    
            objFormularioEval.imprData(datos);
        }).fail(function(jqxhr) {
            const message = (jqxhr && jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la solicitud del servidor.';
            objPrincipal.notify('error', message);
        }).always(function() {

        });
    };

    objFormularioEval.cargarProducto = function(){
        const button = $(this);
        const form = $('#frmFiltroProducto');
        $.ajax({
            url: form.attr('action'),
            method: 'POST',
            data: form.serialize(),
            dataType: 'json',
            beforeSend: function() {
                
            },
        }).done(function(res) {
            objPrincipal.notify('success', res.message);
            dataProducto = res.data.dataProducto;    
            $('#id_producto').val(dataProducto.IDPRODUCTO);
            $('#codigo_producto').val(dataProducto.CODIGOPRODUCTO);
            
            $('#producto_rs').val(dataProducto.REGSANITARIO);
            Date1 = moment(dataProducto.FECHAINICIO, 'YYYY-MM-DD HH:mm:ss').format('DD/MM/YYYY');
            $('#producto_fecha_inicio').val(Date1);
            Date2 = moment(dataProducto.FECHAFIN, 'YYYY-MM-DD HH:mm:ss').format('DD/MM/YYYY');
            $('#producto_fecha_vencimiento').val(Date2);
            $('#producto_descripcion').val(dataProducto.DESCRIPCION);
            $('#producto_nombre').val(dataProducto.NOMBREPRODUCTO);
            $('#producto_presentacion').val(dataProducto.PRESENTACION);
            $('#direccion_fabricante').val(dataProducto.DIRECCIONFABRICANTE);
            $('#vida_util').val(dataProducto.VIDAUTIL);

            $('#categoria_id').html('<option value="'+dataProducto.IDCATEGORIA+'" selected>'+dataProducto.CATEGORIA+'</option>');
            $('#categoria_id option[value=' + dataProducto.IDCATEGORIA + ']').prop('selected', true);

            $('#marca_id').html('<option value="'+dataProducto.IDMARCA+'" selected>'+dataProducto.MARCA+'</option>');
            $('#marca_id option[value=' + dataProducto.IDMARCA + ']').prop('selected', true);

            $('#fabricante_id').html('<option value="'+dataProducto.IDFABRICANTE+'" selected>'+dataProducto.NOMBREFABRICANTE+'</option>');
            $('#fabricante_id option[value=' + dataProducto.IDFABRICANTE + ']').prop('selected', true);

            $('#producto_pais').html('<option value="'+dataProducto.IDPAIS+'" selected>'+dataProducto.PAIS+'</option>');
            $('#producto_pais option[value=' + dataProducto.IDPAIS + ']').prop('selected', true);
            if(dataProducto.ESTADO=='I'){
                $('#producto_estado option[value=0]').prop('selected', true);
            }else{
                $('#producto_estado option[value=1]').prop('selected', true);
            }
        }).fail(function(jqxhr) {
            const message = (jqxhr && jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la solicitud del servidor.';
            objPrincipal.notify('error', message);
        }).always(function() {

        });
    };

    objFormularioEval.guardarProducto = function() {
        const button = $(this);
        const form = $('#frmProducto');
        $.ajax({
            url: form.attr('action'),
            method: 'POST',
            data: form.serialize() +
                '&categoria_id=' + $('#categoria_id option:selected').val() +
                '&marca_id=' + $('#marca_id option:selected').val() +
                '&fabricante_id=' + $('#fabricante_id option:selected').val() +
                '&producto_estado=' + $('#producto_estado option:selected').val() +
                '&producto_pais=' + $('#producto_pais option:selected').val(),
            dataType: 'json',
            beforeSend: function() {
                
            },
        }).done(function(res) {
            objPrincipal.notify('success', res.message);
            datos = res.data.dataProducto;    
            $('#id_producto').val(datos.IDPRODUCTO);
            $('#id_producto_modal').val(datos.IDPRODUCTO);
        }).fail(function(jqxhr) {
            const message = (jqxhr && jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la solicitud del servidor.';
            objPrincipal.notify('error', message);
        }).always(function() {

        });
    };

    objFormularioEval.cargarEvaluacion = function(dataEval,dataDocumentos){
        Date1 = moment(dataEval.FECHAINICIOEVAL, 'YYYY-MM-DD HH:mm:ss').format('DD/MM/YYYY');
        $('#fecha_inicio').val(Date1);
        if(dataEval.FECHACIERREEVAL){
            Date2 = moment(dataEval.FECHACIERREEVAL, 'YYYY-MM-DD HH:mm:ss').format('DD/MM/YYYY');
            $('#fecha_cierre').val(Date2);
        }
        $('#codigo_eval').val(dataEval.IDEVAL);
        $('#estado_eval option[value=' + dataEval.ESTADOEVAL + ']').prop('selected', true);
        $('#btnGuardarEval').hide();
        $('#btnActualizarEval').show();
        objFormularioEval.btnCerrarEval();

        $('#id_producto').val(dataEval.IDPRODUCTO);
        $('#id_eval').val(dataEval.IDEVAL);
        $('#codigo_producto').val(dataEval.CODIGOPRODUCTO);
        $('#categoria_id').html('<option value="'+dataEval.CATEGORIA+'" selected>'+dataEval.NOMBRECATEGORIA+'</option>');
        $('#categoria_id option[value=' + dataEval.CATEGORIA + ']').prop('selected', true);
        $('#producto_rs').val(dataEval.REGSANITARIO);
        Date3 = moment(dataEval.FECHAEMISION, 'YYYY-MM-DD HH:mm:ss').format('DD/MM/YYYY');
        Date4 = moment(dataEval.FECHAVENCIMIENTO, 'YYYY-MM-DD HH:mm:ss').format('DD/MM/YYYY');
        $('#producto_fecha_inicio').val(Date3);
        $('#producto_fecha_vencimiento').val(Date4);
        $('#producto_descripcion').val(dataEval.DESCRIPCION);
        $('#direccion_fabricante').val(dataEval.DIRECCIONFABRICANTE);
        $('#producto_nombre').val(dataEval.NOMBREPRODUCTO);
        $('#marca_id').html('<option value="'+dataEval.MARCA+'" selected>'+dataEval.NOMBREMARCA+'</option>');
        $('#marca_id option[value=' + dataEval.MARCA + ']').prop('selected', true);
        $('#producto_presentacion').val(dataEval.PRESENTACION);
        $('#fabricante_id').html('<option value="'+dataEval.FABRICANTE+'" selected>'+dataEval.NOMBREFABRICANTE+'</option>');
        $('#fabricante_id option[value=' + dataEval.FABRICANTE + ']').prop('selected', true);
        $('#producto_pais').html('<option value="'+dataEval.PAISFABRICANTE+'" selected>'+dataEval.NOMBREPAIS+'</option>');
        $('#producto_pais option[value=' + dataEval.PAISFABRICANTE + ']').prop('selected', true);
        $('#vida_util').val(dataEval.VIDAUTIL);
        $('#producto_estado option[value=' + dataEval.ESTADOPRODUCTO + ']').prop('selected', true);
        
        dataDocumentos.forEach(function(item,pos){
            var nombreDoc = '';
            if(item.NOMBREDOCUMENTO){nombreDoc += item.NOMBREDOCUMENTO;}
            if(item.NOMBREDOCUMENTO2){nombreDoc += "<br>"+item.NOMBREDOCUMENTO2;}
            if(item.NOMBREDOCUMENTO3){nombreDoc += "<br>"+item.NOMBREDOCUMENTO3;}
            if(item.NOMBREDOCUMENTO4){nombreDoc += "<br>"+item.NOMBREDOCUMENTO4;}
            if(item.NOMBREDOCUMENTO5){nombreDoc += "<br>"+item.NOMBREDOCUMENTO5;}
            $('#nombre_doc_tramite_'+item.IDTRAMITE).html(nombreDoc);
            $('#estado_'+item.IDTRAMITE+' option[value=' + item.ESTADOTRAMITE + ']').prop('selected', true);
        });
    };

    filtroEval = function(codigoEval){
        $.ajax({
            url: 'ar/evalcenco/cconteval/cargar_Evaluacion',
            method: 'POST',
            data: {codigoEval:codigoEval},
            dataType: 'json',
            beforeSend: function() {
                
            },
        }).done(function(res) {
            objPrincipal.notify('success', res.message);
            var data = res.data.dataEval;
            var dataDoc = res.data.dataDocumentos;
            objFormularioEval.cargarEvaluacion(data,dataDoc);
            $('#tab-list-registro').click();
        }).fail(function(jqxhr) {
            const message = (jqxhr && jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la solicitud del servidor.';
            objPrincipal.notify('error', message);
        }).always(function() {

        });
    };

    objFormularioEval.listarResultados = function(dataResultados) {
        row='';
        dataResultados.forEach(function(item, pos) {
            row += '<tr>';
            row += '<td>'+ item.IDEVAL+'</td>';
            row += '<td>'+ item.NOMBREPRODUCTO+'</td>';
            row += '<td>'+ item.CODIGORS+'</td>';
            if(item.ESTADOEVAL == 1){
                row += '<td>APROBADO</td>';
            }else if(item.ESTADOEVAL == 2){
                row += '<td>OBSERVADO</td>';
            }else if(item.ESTADOEVAL == 3){
                row += '<td>TRUNCO</td>';
            }
            row += '<td>'+ '<div>'+
            '<a data-original-title="Cargar Evaluación" style="cursor:pointer;color:#3c763d;" '+
            'onClick="javascript:filtroEval('+ item.IDEVAL +');"><i class="fa fa-chevron-circle-right fa-2x" data-original-title="Cargar Evaluación" data-toggle="tooltip"></i></a>'+
            '</div>'+'</td>';
            
            row += '</tr>';
            
        });
        $('#tblFiltroEval tbody').html(row);
    };

    objFormularioEval.buscarEvaluaciones = function(){
        const button = $(this);
        const form = $('#frmFiltro');
        $.ajax({
            url: form.attr('action'),
            method: 'POST',
            data: form.serialize() +
                '&filtro_estado_eval=' + $('#filtro_estado_eval option:selected').val(),
            dataType: 'json',
            beforeSend: function() {
                
            },
        }).done(function(res) {
            objPrincipal.notify('success', res.message);
            dataResultados = res.data.listadoEval;
            objFormularioEval.listarResultados(dataResultados);
        }).fail(function(jqxhr) {
            const message = (jqxhr && jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la solicitud del servidor.';
            objPrincipal.notify('error', message);
        }).always(function() {

        });
    };

    objFormularioEval.btnCerrarEval = function(){
        var estadobtn = $('#estado_eval option:selected').val();
        if(estadobtn==2){
            $('#btnCerrarEval').hide();
        }else{
            $('#btnCerrarEval').show();
        }
    };

    objFormularioEval.cambiarEstadoDoc = function(){
        var button = $(this);
        var idbutton = button.attr('id');
        var ideval = $('#id_eval').val();
        var aD = idbutton.split('_');
        var numeroTramite = aD[1];
        var estado = $('#estado_'+numeroTramite+' option:selected').val();

        console.log(numeroTramite);

        $.ajax({
            url: 'ar/evalcenco/cconteval/cambiar_estado',
            method: 'POST',
            data: {
                numeroTramite:numeroTramite,
                ideval:ideval,
                estado:estado
            },
            dataType: 'json',
            beforeSend: function() {
                
            },
        }).done(function(res) {
            objPrincipal.notify('success', res.message);
            console.log(res.data.documentos);
        }).fail(function(jqxhr) {
            const message = (jqxhr && jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la solicitud del servidor.';
            objPrincipal.notify('error', message);
        }).always(function() {

        });
    };
     
});

$(document).ready(function() {

    objFormularioEval.buscarEvaluaciones();

    $('#btnGuardarEval').click(objFormularioEval.guardarEvaluacion);
    $('#btnActualizarEval').click(objFormularioEval.guardarEvaluacion);
    $('#btnCerrarEval').click(objFormularioEval.cerrarEvaluacion);
    $('#btnActualizarEval').hide();
    $('#btnCerrarEval').hide();

    $('#btnCargarProducto').click(objFormularioEval.cargarProducto);
    
    $('.btn-producto-guardar').click(objFormularioEval.guardarProducto);
    $('#filtroBuscarEval').click(objFormularioEval.buscarEvaluaciones);

    $('#estado_eval').change(objFormularioEval.btnCerrarEval);
    $('.select_estado_doc').change(objFormularioEval.cambiarEstadoDoc);

    $('#fecha_inicio').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        autoclose: true,
        theme: 'bootstrap4',
        locale: {
            format: 'DD/MM/YYYY',
            daysOfWeek: [
                'Do',
                'Lu',
                'Ma',
                'Mi',
                'Ju',
                'Vi',
                'Sa'
            ],
            monthNames: [
                'Enero',
                'Febrero',
                'Marzo',
                'Abril',
                'Mayo',
                'Junio',
                'Julio',
                'Agosto',
                'Setiembre',
                'Octubre',
                'Noviembre',
                'Diciembre'
            ]
        }
    });

    $('#producto_fecha_inicio').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        autoclose: true,
        theme: 'bootstrap4',
        locale: {
            format: 'DD/MM/YYYY',
            daysOfWeek: [
                'Do',
                'Lu',
                'Ma',
                'Mi',
                'Ju',
                'Vi',
                'Sa'
            ],
            monthNames: [
                'Enero',
                'Febrero',
                'Marzo',
                'Abril',
                'Mayo',
                'Junio',
                'Julio',
                'Agosto',
                'Setiembre',
                'Octubre',
                'Noviembre',
                'Diciembre'
            ]
        }
    });

    $('#producto_fecha_vencimiento').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        autoclose: true,
        theme: 'bootstrap4',
        locale: {
            format: 'DD/MM/YYYY',
            daysOfWeek: [
                'Do',
                'Lu',
                'Ma',
                'Mi',
                'Ju',
                'Vi',
                'Sa'
            ],
            monthNames: [
                'Enero',
                'Febrero',
                'Marzo',
                'Abril',
                'Mayo',
                'Junio',
                'Julio',
                'Agosto',
                'Setiembre',
                'Octubre',
                'Noviembre',
                'Diciembre'
            ]
        }
    });

});
