

var otblListaextras, otblListapermisos, otblListavacaciones; 

$(document).ready(function() {
    
    vccia = $('#hdnccia').val();

    $('#modalCuadroPermisos > .modal-body').css({width:'auto',height:'auto', 'max-height':'100%'});
    $('#modalCuadroVacaciones > .modal-body').css({width:'auto',height:'auto', 'max-height':'100%'});  
    
    datosPermisos();
});

datosPermisos = function(){
    var v_idempleado = $('#hdidempleado').val();

    var parametros = { 
        'idempleado' : v_idempleado,
    };
    
    $.ajax({
        type: 'ajax',
        url:  baseurl+"adm/rrhh/cctrlpermisos/getdatospermisos/",
        type:  'post',
        dataType: "JSON",
        data: parametros,
        success: function(result){
            $.each(result,function(key, value){
                document.querySelector('#pdatosPerm').innerText = value.horaspendientes+' Horas a '+value.permOpcion;
                document.querySelector('#pdatosVaca').innerText = value.diaspendientes+' Dias '+value.vacaOpcion;
                document.querySelector('#pdatosHextra').innerText = value.nro_horasextras+' Horas adicionales';

                document.querySelector('#spanhoraacum').innerText = value.nro_horasextras;
                document.querySelector('#spanpermacum').innerText = value.nro_permisos;
                document.querySelector('#spanpermest').innerText = value.permOpcion.toUpperCase();
                document.querySelector('#spanhorapend').innerText = value.horaspendientes;
                document.querySelector('#spandiavaca').innerText = value.diasvaca;
                document.querySelector('#spandiaxvaca').innerText = value.diasxvacaciones;
                document.querySelector('#spanvacaest').innerText = value.vacaOpcion.toUpperCase();
                document.querySelector('#spandiapend').innerText = value.diaspendientes;
            });
        }
    });
};

$(document).on('shown.bs.modal', function (e) {
    $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
});

// PERMISOS
$('#modalCuadroPermisos').on('shown.bs.modal', function (e) {	

    otblPermisos = $('#tblPermisos').DataTable({
        "processing"  	: true,
        "bDestroy"    	: true,
        "stateSave"     : true,
        "bJQueryUI"     : true,
        "scrollY"     	: "350px",
        "scrollX"     	: true, 
        'AutoWidth'     : true,
        "paging"      	: false,
        "info"        	: false,
        "filter"      	: true, 
        "ordering"		: false,
        "responsive"    : false,
        "select"        : true, 
        'ajax'        : {
            "url"   : baseurl+"adm/rrhh/cctrlpermisos/getlistpermisos/",
            "type"  : "POST", 
            "data": function ( d ) { 
                d.id_empleado = $('#hdidempleado').val();   
            },     
            dataSrc : ''        
        },
        'columns'     : [
            {data: 'fsalida', "class": "dt-body-center col-s"},
            {data: 'horapermisos', "class": "dt-body-center col-s"},
            {data: 'horasuso', "class": "dt-body-center col-s"},
            {data: 'desc_motivo'},   
            {data: 'fundamentacion'}        
        ],
    });
    
});

$('#modalRegPermisos').on('shown.bs.modal', function (e) {
    $('#frmMantPermisos').trigger("reset");
    $('#mhdnIdPerm').val('');
    $('#mhdnAccionPerm').val('N');
    $('#mhdnEmpPerm').val($('#hdidempleado').val());

    $('#mtxtFsalidaperm').datetimepicker({
        format: 'DD/MM/YYYY',
        daysOfWeekDisabled: [0],
        locale:'es'
    });	

    $('#mtxtHsalidaperm, #mtxtHretornoperm').datetimepicker({
        format: 'hh:mm A',
        locale:'es',
        stepping: 15
    });	
        
    $('#mtxtHsalidaperm').datetimepicker('minDate', moment('08:00 AM', 'hh:mm A') );
    $('#mtxtHsalidaperm').datetimepicker('maxDate', moment('05:45 PM', 'hh:mm A') );
    $('#mtxtHsalidaperm').datetimepicker('date', moment('08:00 AM', 'hh:mm A') );

    $('#mtxtHretornoperm').datetimepicker('date', moment('08:15 AM', 'hh:mm A') );  
    
    $('#frmMantPermisos').validate({
        rules: {
        },
        messages: {
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        },        
        submitHandler: function (form) {
            const botonEvaluar = $('#mbtnGPerm');
            var request = $.ajax({
                url:$('#frmMantPermisos').attr("action"),
                type:$('#frmMantPermisos').attr("method"),
                data:$('#frmMantPermisos').serialize(),
                error: function(){
                    Vtitle = 'Error en Guardar!!!';
                    Vtype = 'error';
                    sweetalert(Vtitle,Vtype); 
                    objPrincipal.liberarBoton(botonEvaluar);
                },
                beforeSend: function() {
                    objPrincipal.botonCargando(botonEvaluar);
                }
            });
            request.done(function( respuesta ) {
                var posts = JSON.parse(respuesta);
                
                $.each(posts, function() {
                    Vtitle = 'Se Grabo Correctamente!!!';
                    Vtype = 'success';
                    sweetalert(Vtitle,Vtype);  
                    objPrincipal.liberarBoton(botonEvaluar);
                    //emailValidar(this.emailrespomsable,this.tipo,this.token,this.idusuario,this.idempleado,this.idpermisosvacaciones,this.ccia);       
                    $('#btnRetornarPerm').click();    
                });
            });
            return false;
        }
    });	

    fechaActualperm();
});

$('#mtxtHsalidaperm').on('change.datetimepicker',function(e){
    $('#mtxtHretornoperm').datetimepicker({
        format: 'hh:mm A',
        locale:'es',
        stepping: 15
    });	 
    $('#mtxtHretornoperm').datetimepicker('minDate', e.date.add(15, "minute"));
    $('#mtxtHretornoperm').datetimepicker('maxDate', moment('06:00 PM', 'hh:mm A') );
    $('#mtxtHretornoperm').datetimepicker('date', moment(e.date, 'hh:mm A'));    
});

fechaActualperm = function(){
    var fecha = new Date();		
    var fechatring = ("0" + fecha.getDate()).slice(-2) + "/" + ("0"+(fecha.getMonth()+1)).slice(-2) + "/" +fecha.getFullYear() ;

    $('#mtxtFregistroperm').val(fechatring);
    $('#mtxtFsalidaperm').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY') );

    $('#mtxtHsalidaperm').datetimepicker('minDate', moment('08:00 AM', 'hh:mm A') );
    $('#mtxtHsalidaperm').datetimepicker('maxDate', moment('05:45 PM', 'hh:mm A') );
    $('#mtxtHsalidaperm').datetimepicker('date', moment('08:00 AM', 'hh:mm A') );
    
    $('#mtxtHretornoperm').datetimepicker('date', moment('08:15 AM', 'hh:mm A') );
    
    $("#mtxtFsalidaperm").trigger("change.datetimepicker");
};

$('#cboRecuperahora').change(function( event ){ 
    var v_recuhora = $( "#cboRecuperahora option:selected").attr("value");
    if(v_recuhora == 'V'){
        $('#mtxtFrecuperm').val(''); 
    }else{
        var fecharecu = new Date();		
        var fechatringrecu = ("0" + fecharecu.getDate()).slice(-2) + "/" + ("0"+(fecharecu.getMonth()+1)).slice(-2) + "/" +fecharecu.getFullYear() ;
        $('#mtxtFrecuperm').datetimepicker('date', moment(fechatringrecu, 'DD/MM/YYYY') );
    }
});

$('#modalRegPermisos').on('hidden.bs.modal', function (e) {
    datosPermisos();
});
// 


// VACACIONES	
$('#modalCuadroVacaciones').on('shown.bs.modal', function (e) {		
    
    otblListavacaciones = $('#tblVacaciones').DataTable({  
        "processing"  	: true,
        "bDestroy"    	: true,
        "stateSave"     : true,
        "bJQueryUI"     : true,
        "scrollY"     	: "350px",
        "scrollX"     	: true, 
        'AutoWidth'     : true,
        "paging"      	: false,
        "info"        	: true,
        "filter"      	: true, 
        "ordering"		: false,
        "responsive"    : false,
        "select"        : true, 
        'ajax'        : {
            "url"   : baseurl+"adm/rrhh/cctrlpermisos/getlistvacaciones",
            "type"  : "POST", 
            "data": function ( d ) { 
                d.id_empleado = $('#hdidempleado').val(); 
            },     
            dataSrc : ''        
        },
        "columns"     : [
            {data: 'fsalida', "class": "dt-body-center col-s"},
            {data: 'fecha_retorno', "class": "dt-body-center col-s"},
            {data: 'diastomados', "class": "dt-body-right col-s"},
            {data: 'fundamentacion'},            
        ],       
    });
});

$('#modalRegVacaciones').on('shown.bs.modal', function (e) {
    $('#frmMantVacaciones').trigger("reset");
    $('#mhdnIdVaca').val('');
    $('#mhdnAccionVaca').val('N');
    $('#mhdnEmpVaca').val($('#mhdnIdEmpleado').val());
    fechaActualvaca();    

    $('#mtxtFsalidavaca,mtxtFretornovaca').datetimepicker({
        format: 'DD/MM/YYYY',
        daysOfWeekDisabled: [0],
        locale:'es'
    });	

    $('#frmMantVacaciones').validate({
        rules: {
        },
        messages: {
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        },        
        submitHandler: function (form) {
            const botonEvaluar = $('#mbtnGVaca');
            var request = $.ajax({
                url:$('#frmMantVacaciones').attr("action"),
                type:$('#frmMantVacaciones').attr("method"),
                data:$('#frmMantVacaciones').serialize(),
                error: function(){
                    Vtitle = 'Error en Guardar!!!';
                    Vtype = 'error';
                    sweetalert(Vtitle,Vtype); 
                    objPrincipal.liberarBoton(botonEvaluar);
                },
                beforeSend: function() {
                    objPrincipal.botonCargando(botonEvaluar);
                }
            });
            request.done(function( respuesta ) {
                var posts = JSON.parse(respuesta);
                
                $.each(posts, function() {
                    Vtitle = 'Se Grabo Correctamente!!!';
                    Vtype = 'success';
                    sweetalert(Vtitle,Vtype);  
                    objPrincipal.liberarBoton(botonEvaluar);
                    //emailValidar(this.emailrespomsable,this.tipo,this.token,this.idusuario,this.idempleado,this.idpermisosvacaciones,this.ccia);     
                    $('#btnRetornarVaca').click();  
                });
            });
            return false;
        }
    });

});

$('#mtxtFsalidavaca').on('change.datetimepicker',function(e){  
        
    $('#mtxtFretornovaca').datetimepicker({
        format: 'DD/MM/YYYY',
        daysOfWeekDisabled: [0],
        locale:'es'
    });

    var fecha = moment(e.date).format('DD/MM/YYYY');
    var newDate = moment(e.date); 
           
    newDate.add(1, 'days');
    nfecha = moment(newDate).format('DD/MM/YYYY');

    $('#mtxtFretornovaca').datetimepicker('minDate', fecha);
    $('#mtxtFretornovaca').datetimepicker('date', nfecha);
    
});

fechaActualvaca= function(){
    var fecha = new Date();		
    var fechatring = ("0" + fecha.getDate()).slice(-2) + "/" + ("0"+(fecha.getMonth()+1)).slice(-2) + "/" +fecha.getFullYear() ;
   
    $('#mtxtFregistrovaca').val(fechatring);
   
    $('#mtxtFsalidavaca').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY') );		
    $("#mtxtFsalidavaca").trigger("change.datetimepicker");
};

$('#modalRegVacaciones').on('hidden.bs.modal', function (e) {
    datosPermisos();
});
// 


// EXTRAS
$('#modalCuadroExtras').on('shown.bs.modal', function (e) {	

    otblExtras = $('#tblExtras').DataTable({
        "processing"  	: true,
        "bDestroy"    	: true,
        "stateSave"     : true,
        "bJQueryUI"     : true,
        "scrollY"     	: "350px",
        "scrollX"     	: true, 
        'AutoWidth'     : true,
        "paging"      	: false,
        "info"        	: false,
        "filter"      	: true, 
        "ordering"		: false,
        "responsive"    : false,
        "select"        : true, 
        'ajax'        : {
            "url"   : baseurl+"adm/rrhh/cctrlpermisos/getlisthorasextras/",
            "type"  : "POST", 
            "data": function ( d ) { 
                d.id_empleado = $('#hdidempleado').val();   
            },     
            dataSrc : ''        
        },
        'columns'     : [
            {data: 'fhoraextra', "class": "dt-body-center col-s"},
            {data: 'hora_entrada', "class": "dt-body-center col-s"},
            {data: 'hora_salida', "class": "dt-body-right col-s"},
            {data: 'fundamentacion'},       
        ],
    });
    
});	

$('#modalRegExtras').on('shown.bs.modal', function (e) {
    $('#frmMantHorasextras').trigger("reset");
    $('#mhdnIdHextra').val('');
    $('#mhdnAccionHextra').val('N');
    $('#mhdnEmpHextra').val($('#mhdnIdEmpleado').val());

    $('#mtxtFsalidahextras').datetimepicker({
        format: 'DD/MM/YYYY',
        daysOfWeekDisabled: [0],
        locale:'es'
    });	

    $('#mtxtHsalidahextras, #mtxtHretornohextras').datetimepicker({
        format: 'hh:mm A',
        locale:'es',
        stepping: 15
    });	
        
    $('#mtxtHsalidahextras').datetimepicker('minDate', moment('08:00 AM', 'hh:mm A') );
    $('#mtxtHsalidahextras').datetimepicker('maxDate', moment('05:45 PM', 'hh:mm A') );
    $('#mtxtFsalidahextras').datetimepicker('date', moment('08:00 AM', 'hh:mm A') );

    $('#mtxtHretornohextras').datetimepicker('date', moment('08:15 AM', 'hh:mm A') );  

    $('#frmMantHorasextras').validate({
        rules: {
        },
        messages: {
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        },        
        submitHandler: function (form) {
            const botonEvaluar = $('#mbtnGHextra');
            var request = $.ajax({
                url:$('#frmMantHorasextras').attr("action"),
                type:$('#frmMantHorasextras').attr("method"),
                data:$('#frmMantHorasextras').serialize(),
                error: function(){
                    Vtitle = 'Error en Guardar!!!';
                    Vtype = 'error';
                    sweetalert(Vtitle,Vtype); 
                    objPrincipal.liberarBoton(botonEvaluar);
                },
                beforeSend: function() {
                    objPrincipal.botonCargando(botonEvaluar);
                }
            });
            request.done(function( respuesta ) {
                var posts = JSON.parse(respuesta);
                
                $.each(posts, function() {
                    Vtitle = 'Se Grabo Correctamente!!!';
                    Vtype = 'success';
                    sweetalert(Vtitle,Vtype);   
                    objPrincipal.liberarBoton(botonEvaluar); 
                    //emailValidar(this.emailrespomsable,this.tipo,this.token,this.idusuario,this.idempleado,this.idpermisosvacaciones,this.ccia);   
                    $('#btnRetornarHextra').click();    
                });
            });
            return false;
        }
    });	

    fechaActualhextras();

});

$('#mtxtHsalidaperm').on('change.datetimepicker',function(e){
    $('#mtxtHretornohextras').datetimepicker({
        format: 'hh:mm A',
        locale:'es',
        stepping: 15
    });	 
    $('#mtxtHretornohextras').datetimepicker('minDate', e.date.add(15, "minute"));
    $('#mtxtHretornohextras').datetimepicker('maxDate', moment('11:00 PM', 'hh:mm A') );
    $('#mtxtHretornohextras').datetimepicker('date', moment(e.date, 'hh:mm A'));     
});

fechaActualhextras = function(){
    var fecha = new Date();		
    var fechatring = ("0" + fecha.getDate()).slice(-2) + "/" + ("0"+(fecha.getMonth()+1)).slice(-2) + "/" +fecha.getFullYear() ;

    $('#mtxtFregistrohextras').val(fechatring);
    $('#mtxtFsalidahextras').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY') );

    $('#mtxtHsalidahextras').datetimepicker('minDate', moment('08:00 AM', 'hh:mm A') );
    $('#mtxtHsalidahextras').datetimepicker('maxDate', moment('10:45 PM', 'hh:mm A') );
    $('#mtxtHsalidahextras').datetimepicker('date', moment('08:00 AM', 'hh:mm A') );
    
    $('#mtxtHretornohextras').datetimepicker('date', moment('08:15 AM', 'hh:mm A') );
    
    $("#mtxtFsalidahextras").trigger("change.datetimepicker");
};

$('#modalRegExtras').on('hidden.bs.modal', function (e) {
    datosPermisos();
});
// 


   
excelResumen = function(id_empleado){
    window.open(baseurl+"adm/rrhh/cctrlpermisos/excelresumenperm/"+id_empleado);
};

emailValidar = function(emailrespomsable,tipo,token,idusuario,idempleado,idpermisosvacaciones,ccia){    
    var parametros = { 
        'emailrespomsable'		:	emailrespomsable,
        'tipo'					:   tipo,
        'token'					:   token,
        'idusuario'				:   idusuario,
        'idempleado' 			:   idempleado,
        'idpermisosvacaciones' 	:   idpermisosvacaciones,
        'ccia'					:	ccia
    };
    
    $.ajax({
        type: 'ajax',
        url:  baseurl+"adm/rrhh/cctrlpermisos/sendEmailValidar/",
        type:  'post',
        dataType: "JSON",
        data: parametros,
        success: function(result){
            $.each(result,function(key, value){
                Vtitle = 'Conforme, Se envio el Email correctamente!!!';
                Vtype = 'success';
                sweetalert(Vtitle,Vtype); 
            });
        }
    });
};


