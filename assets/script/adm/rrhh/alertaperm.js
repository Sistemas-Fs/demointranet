
var otblListRegInforme, otblListRegitro, otblListReg03equipo, otblListReg06equipo, otblListReg08equipo;
var varfdesde = '%', varfhasta = '%';
var iduser = $('#mtxtidusuinfor').val();

$(document).ready(function() { 
    
   
    parametros = paramListalertaperm();
    getlistalertaperm(parametros);  
    
});

paramListalertaperm = function (){    
       
    var param = {
        "idempleado"  : $('#hdidempleado').val(),
    }; 
    return param;    
};

getlistalertaperm = function(parametros){

    otblListRegInforme = $('#tblListalertaPerm').DataTable({  
        "processing"  	: true,
        "bDestroy"    	: true,
        "stateSave"     : true,
        "bJQueryUI"     : true,
        "scrollY"     	: "400px",
        "scrollX"     	: true, 
        'AutoWidth'     : true,
        "paging"      	: false,
        "info"        	: false,
        "filter"      	: true, 
        "ordering"		: false,
        "responsive"    : false,
        "select"        : true, 
        'ajax'	: {
            "url"   : baseurl+"adm/rrhh/cctrlpermisos/getlistalertaperm/", 
            "type"  : "POST",         
            "data"  : parametros, 
            dataSrc : ''        
        },
        'columns'	: [
            {data : 'nro', "class" : "col-xxxs", orderable : false},
            {data: 'area',"class": "col-m"},
            {data: 'empleado',"class": "col-m"},
            {data: 'fingreso', "class": "dt-body-center col-s"},
            {data: 'fcumplevaca', "class": "dt-body-center col-s"}, 
            {data: 'periodovaca', "class": "dt-body-right col-s"}, 
            {data: 'diasvaca', "class": "dt-body-right col-s"}, 
            {data: 'diastomados', "class": "dt-body-right col-s"}, 
            {data: 'nro_permcuentavaca', "class": "dt-body-right col-s"}, 
            {data: 'nro_vacaciones', "class": "dt-body-right col-s"}, 
            {data: 'diaspendientes', "class": "dt-body-right col-s"}, 
        ]
    }); 
};
