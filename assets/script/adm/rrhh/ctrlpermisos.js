
var otblListCtrlpermiso, oTable_listavacaciones, oTable_listapermisos, oTable_listahorasextras, oTable_listadescansomedico;

$(document).ready(function() {
    
    cargarEmpleados('0','0');
        
});
  
$('#cboCia').change(function(){ 
    var v_ccia = $( "#cboCia option:selected").attr("value");
    var params = { 
        "ccia" : v_ccia
    }; 
    $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"cglobales/getareacia",
        dataType: "JSON",
        async: true,
        data: params,
        success:function(result)
        {
            $('#cboArea').html(result);
        },
        error: function(){
          alert('Error, No se puede autenticar por error');
        }
    });
    cargarEmpleados(v_ccia,'0');
});

$('#cboArea').change(function(){ 
    var v_ccia = $( "#cboCia option:selected").attr("value");
    var v_carea = $( "#cboArea option:selected").attr("value");

    cargarEmpleados(v_ccia,v_carea);
    parametros = paramListCtrlpermiso();
    getListCtrlpermiso(parametros);
});

$('#cboEmpleado').change(function(){ 
    parametros = paramListCtrlpermiso();
    getListCtrlpermiso(parametros);
});

cargarEmpleados = function(v_ccia,v_carea){
    
    var params = { 
        "ccia" : v_ccia,
        "carea" : v_carea
    }; 
    $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"adm/rrhh/cctrlpermisos/getempleados",
        dataType: "JSON",
        async: true,
        data: params,
        success:function(result)
        {
            $('#cboEmpleado').html(result);
        },
        error: function(){
          alert('Error, No se puede autenticar por error');
        }
    });
}

paramListCtrlpermiso = function(){      
    var v_idempleado = $( "#cboEmpleado option:selected").attr("value");
    var v_ccia = $( "#cboCia option:selected").attr("value");
    var v_carea = $( "#cboArea option:selected").attr("value");
     
    var parametros = {
        "id_empleado"   : v_idempleado,
        "ccia"          : v_ccia,
        "carea"         : v_carea
    };  

    return parametros;
}

getListCtrlpermiso = function(parametros){
    var groupColumn = 0; 

    otblListCtrlpermiso = $('#tblListCtrlPermisos').DataTable({
        "processing"  	: true,
        "bDestroy"    	: true,
        "stateSave"     : true,
        "bJQueryUI"     : true,
        "scrollY"     	: "540px",
        "scrollX"     	: true, 
        'AutoWidth'     : true,
        "paging"      	: false,
        "info"        	: true,
        "filter"      	: true, 
        "ordering"		: false,
        "responsive"    : false,
        "select"        : true,
        'ajax'        : {
            "url"   : baseurl+"adm/rrhh/cctrlpermisos/getlistempleadosperm/",
            "type"  : "POST", 
            "data"  : parametros,     
            dataSrc : ''          
        },
        'columns'     : [
            {data: 'area', "class": "col-m"},
            {data: null, "class": "col-xxs"},
            {data: 'empleado', "class": "col-m"},  
            {data: 'fingreso', "class": "dt-body-center col-s"},
            {data: 'fcumplevaca', "class": "dt-body-center col-s"}, 
            {data: 'periodovaca', "class": "dt-body-right col-s"}, 
            {data: 'diasvaca', "class": "dt-body-right col-s"}, 
            {data: 'diastomados', "class": "dt-body-right col-s"}, 
            {data: 'nro_permcuentavaca', "class": "dt-body-right col-s"}, 
            {data: 'nro_vacaciones', "class": "dt-body-right col-s"}, 
            {data: 'diaspendientes', "class": "dt-body-right col-s"}, 
            {data: 'nro_horasextras', "class": "dt-body-right col-xs"}, 
            {data: 'nro_permisos', "class": "dt-body-right col-xs"},  
            {data: 'horaspendientes', "class": "dt-body-right col-xs"},
            {data: 'nro_descansomedico', "class": "dt-body-right col-s"},                        
            {"orderable": false, "class": "dt-body-center col-s", 
                render:function(data, type, row){
                    return '<div>' +
                            '    <a onclick="excelResumen(\''+row.id_empleado+'\');" class="btn btn-outline-primary btn-sm hidden-sm"><span class="fas fa-file-excel fa-2x" aria-hidden="true"> </span> REPORTE</a>' +
                            '</div>' ; 
                }
            },                        
            {"orderable": false, "class": "dt-body-center col-s", 
                render:function(data, type, row){
                    return '<div>' +
                            '    <a  data-original-title="Listar" data-toggle="modal" data-target="#modalvacaciones" onclick="javascript:regVacaciones(\''+row.id_empleado+'\');" class="btn btn-outline-primary btn-sm hidden-sm"><span class="fa fa-briefcase fa-2x" aria-hidden="true"> </span> VACACIONES</a>' +
                            '</div>' ; 
                }
            },                        
            {"orderable": false, "class": "dt-body-center col-s", 
                render:function(data, type, row){
                    return '<div>' +
                            '    <a data-original-title="Listar" data-toggle="modal" data-target="#modalpermisos" onclick="javascript:regPermisos(\''+row.id_empleado+'\');" class="btn btn-outline-primary btn-sm hidden-sm"><span class="fas fa-ticket-alt fa-2x" aria-hidden="true"> </span> PERMISOS</a>' +
                            '</div>' ; 
                }
            },                        
            {"orderable": false, "class": "dt-body-center col-s", 
                render:function(data, type, row){
                    return '<div>' +
                            '    <a data-original-title="Listar" data-toggle="modal" data-target="#modalhorasextras" onclick="javascript:regHorasextras(\''+row.id_empleado+'\');" class="btn btn-outline-primary btn-sm hidden-sm"><span class="fab fa-safari fa-2x" aria-hidden="true"> </span> HORAS-EXT</a>' +
                            '</div>' ; 
                }
            },                        
            {"orderable": false, "class": "dt-body-center col-s", 
                render:function(data, type, row){
                    return '<div>' +
                            '    <a data-original-title="Listar" data-toggle="modal" data-target="#modaldescansomedico" onclick="javascript:regDescansomedico(\''+row.id_empleado+'\');" class="btn btn-outline-primary btn-sm hidden-sm"><span class="fa fa-ambulance fa-2x" aria-hidden="true"> </span> DES-MEDIC</a>' +
                            '</div>' ; 
                }
            }
        ],  
        "columnDefs": [
            { "visible": false, "targets": groupColumn },
        ],
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
 
            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="13">'+group+'</td></tr>'
                    );
 
                    last = group;
                }
            } );
        },
        'order' : [[0, "asc"],[2, "asc"]] 
    });
    otblListCtrlpermiso.column(0).visible( false );

    // Enumeracion 
    otblListCtrlpermiso.on( 'order.dt search.dt', function () { 
        otblListCtrlpermiso.column(1, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i+1;
        } );
    }).draw();
}
    
excelResumen = function(id_empleado){
    window.open(baseurl+"adm/rrhh/cctrlpermisos/excelresumenperm/"+id_empleado);
};

/*VACACIONES*/
regVacaciones = function(id_empleado){    
	$('#mhdnIdEmpleado').val(id_empleado);
};

$('#modalvacaciones').on('show.bs.modal', function (e) {

    $('#tabvacaciones a[href="#tab_listavacacionestab"]').attr('class', 'disabled');
    $('#tabvacaciones a[href="#tab_newvacacionestab"]').attr('class', 'disabled active');

    $('#tabvacaciones a[href="#tab_listavacacionestab"]').not('#store-tab.disabled').click(function(event){
        $('#tabvacaciones a[href="#tabinforme-list"]').attr('class', 'active');
        $('#tabvacaciones a[href="#tab_newvacaciones"]').attr('class', '');
        return true;
    });
    $('#tabvacaciones a[href="#tab_newvacacionestab"]').not('#bank-tab.disabled').click(function(event){
        $('#tabvacaciones a[href="#tab_newvacaciones"]').attr('class' ,'active');
        $('#tabvacaciones a[href="#tab_listavacaciones"]').attr('class', '');
        return true;
    });
    
    $('#tabvacaciones a[href="#tab_listavacaciones"]').click(function(event){return false;});
    $('#tabvacaciones a[href="#tab_newvacaciones"]').click(function(event){return false;});
	
    $('#tabvacaciones a[href="#tab_listavacaciones"]').tab('show');
    
    listarVacaciones();

    $('#mtxtFsalidavaca,mtxtFretornovaca').datetimepicker({
        format: 'DD/MM/YYYY',
        daysOfWeekDisabled: [0],
        locale:'es'
    });	

    $('#frmMantVacaciones').validate({
        rules: {
        },
        messages: {
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        },        
        submitHandler: function (form) {
            const botonEvaluar = $('#mbtnGVaca');
            var request = $.ajax({
                url:$('#frmMantVacaciones').attr("action"),
                type:$('#frmMantVacaciones').attr("method"),
                data:$('#frmMantVacaciones').serialize(),
                error: function(){
                    Vtitle = 'Error en Guardar!!!';
                    Vtype = 'error';
                    sweetalert(Vtitle,Vtype); 
                    objPrincipal.liberarBoton(botonEvaluar);
                },
                beforeSend: function() {
                    objPrincipal.botonCargando(botonEvaluar);
                }
            });
            request.done(function( respuesta ) {
                var posts = JSON.parse(respuesta);
                
                $.each(posts, function() {
                    Vtitle = 'Se Grabo Correctamente!!!';
                    Vtype = 'success';
                    sweetalert(Vtitle,Vtype); 
                    listarVacaciones();   
                    objPrincipal.liberarBoton(botonEvaluar);    
                    $('#btnRetornarVaca').click();    
                });
            });
            return false;
        }
    });
	
});

$('#mtxtFsalidavaca').on('change.datetimepicker',function(e){  
        
    $('#mtxtFretornovaca').datetimepicker({
        format: 'DD/MM/YYYY',
        daysOfWeekDisabled: [0],
        locale:'es'
    });

    var fecha = moment(e.date).format('DD/MM/YYYY');
    var newDate = moment(e.date); 
           
    newDate.add(1, 'days');
    nfecha = moment(newDate).format('DD/MM/YYYY');

    $('#mtxtFretornovaca').datetimepicker('minDate', fecha);
    $('#mtxtFretornovaca').datetimepicker('date', nfecha);
    
});

$('#btnNuevoVaca').click(function(){		
    $('#tabvacaciones a[href="#tab_newvacaciones"]').tab('show');
    $('#frmMantVacaciones').trigger("reset");
    $('#mhdnIdVaca').val('');
    $('#mhdnAccionVaca').val('N');
    $('#mhdnEmpVaca').val($('#mhdnIdEmpleado').val());
    fechaActualvaca();

});

$('#btnRetornarVaca').click(function(){
    $('#tabvacaciones a[href="#tab_listavacaciones"]').tab('show');  
});

fechaActualvaca= function(){
    var fecha = new Date();		
    var fechatring = ("0" + fecha.getDate()).slice(-2) + "/" + ("0"+(fecha.getMonth()+1)).slice(-2) + "/" +fecha.getFullYear() ;
   
    $('#mtxtFregistrovaca').val(fechatring);
   
    $('#mtxtFsalidavaca').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY') );		
    $("#mtxtFsalidavaca").trigger("change.datetimepicker");
};

listarVacaciones = function(){        
    oTable_listavacaciones = $('#tblVacaciones').DataTable({
        "processing"  	: true,
        "bDestroy"    	: true,
        "stateSave"     : true,
        "bJQueryUI"     : true,
        "scrollY"     	: "500px",
        "scrollX"     	: true, 
        'AutoWidth'     : true,
        "paging"      	: false,
        "info"        	: true,
        "filter"      	: true, 
        "ordering"		: false,
        "responsive"    : false,
        "select"        : true, 
        'ajax'        : {
            "url"   : baseurl+"adm/rrhh/cctrlpermisos/getlistvacaciones",
            "type"  : "POST", 
            "data": function ( d ) { 
                d.id_empleado     = $('#mhdnIdEmpleado').val(); 
            },     
            dataSrc : ''        
        },
        "columns"     : [
            {data: 'fsalida', "class": "dt-body-center col-s"},
            {data: 'fecha_retorno', "class": "dt-body-center col-s"},
            {data: 'diastomados', "class": "dt-body-right col-s"},
            {data: 'fundamentacion'},                        
            {"orderable": false, 
                render:function(data, type, row){
                    return '<div>' +
                            '<a title="Editar" style="cursor:pointer; color:#3c763d;" onclick="javascript:seleVacaciones(\''+row.id_permisosvacaciones+'\',\''+row.id_empleado+'\',\''+row.fecha_registro+'\',\''+row.fecha_salida+'\',\''+row.fecha_retorno+'\',\''+row.fundamentacion+'\');"><span class="fas fa-edit fa-2x" aria-hidden="true"> </span> </a>' +
                            '</div>' ; 
                }
            },                        
            {"orderable": false, 
                render:function(data, type, row){
                    return '<div>' +
                            '<a id="aDelVaca" href="'+row.id_permisosvacaciones+'" title="Eliminar" style="cursor:pointer; color:#FF0000;"><span class="fas fa-trash-alt fa-2x" aria-hidden="true"> </span></a>'+
                            '</div>' ; 
                }
            }            
        ],
        columnDefs: [ {
            targets: 0,
            render: $.fn.dataTable.render.moment( 'YYYY-MM-DD', 'DD/MM/YYYY' )
          } ]        
    });
};

seleVacaciones = function(id_permisosvacaciones,id_empleado,fecha_registro,fecha_salida,fecha_retorno,fundamentacion){
    $('#tabvacaciones a[href="#tab_newvacaciones"]').tab('show'); 

    $('#mhdnIdVaca').val(id_permisosvacaciones);
    $('#mhdnAccionVaca').val('A'); 
    $('#mhdnEmpVaca').val(id_empleado);

    $('#mtxtFregistrovaca').val(fecha_registro);         
    $('#mtxtFsalvaca').val(fecha_salida);        
    $('#mtxtFretovaca').val(fecha_retorno); 

    $('#mtxtFundamentovaca').val(fundamentacion); 
};
   
$("body").on("click","#aDelVaca",function(event){
    event.preventDefault();
    idvacaciones = $(this).attr("href");
    
    
    var params = { 
        "vidpermisosvacaciones"   : idvacaciones,
    };
    
    Swal.fire({
        title: 'Confirmar Eliminar Registro',
        text: "¿Está seguro de Eliminar Registro?",
        type: 'error',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Eliminar!'
    }).then((result) => {
        if (result.value) {
            var request = $.ajax({
                type: 'POST',
                url: baseurl+"adm/rrhh/cctrlpermisos/delpermisosvacaciones/",
                async: true,
                data: params,
                error: function(){
                    Vtitle = 'Error en Guardar!!!';
                    Vtype = 'error';
                    sweetalert(Vtitle,Vtype); 
                }
            });
            request.done(function( respuesta ) {
                var posts = JSON.parse(respuesta);
                
                $.each(posts, function() {
                    Vtitle = 'Se Elimino Correctamente!!!';
                    Vtype = 'success';
                    sweetalert(Vtitle,Vtype);         
                    oTable_listavacaciones.ajax.reload(null,false);  
                });
            });
        }
    }) 

}); 

$('#modalvacaciones').on('hidden.bs.modal', function (e) {
    otblListCtrlpermiso.ajax.reload(null,false);
});

/*PERMISOS*/
regPermisos = function(id_empleado){ 
	$('#mhdnIdEmpleado').val(id_empleado);
};

$('#modalpermisos').on('show.bs.modal', function (e) {

    $('#tabpermisos a[href="#tab_listapermisostab"]').attr('class', 'disabled');
    $('#tabpermisos a[href="#tab_newpermisostab"]').attr('class', 'disabled active');

    $('#tabpermisos a[href="#tab_listapermisostab"]').not('#store-tab.disabled').click(function(event){
        $('#tabpermisos a[href="#tabinforme-list"]').attr('class', 'active');
        $('#tabpermisos a[href="#tab_newpermisos"]').attr('class', '');
        return true;
    });
    $('#tabpermisos a[href="#tab_newpermisostab"]').not('#bank-tab.disabled').click(function(event){
        $('#tabpermisos a[href="#tab_newpermisos"]').attr('class' ,'active');
        $('#tabpermisos a[href="#tab_listapermisos"]').attr('class', '');
        return true;
    });
    
    $('#tabpermisos a[href="#tab_listapermisos"]').click(function(event){return false;});
    $('#tabpermisos a[href="#tab_newpermisos"]').click(function(event){return false;});
	
    $('#tabpermisos a[href="#tab_listapermisos"]').tab('show');
    
    listarPermisos();

    $('#mtxtFsalidaperm').datetimepicker({
        format: 'DD/MM/YYYY',
        daysOfWeekDisabled: [0],
        locale:'es'
    });	

    $('#mtxtHsalidaperm, #mtxtHretornoperm').datetimepicker({
        format: 'hh:mm A',
        locale:'es',
        stepping: 15
    });	
        
    $('#mtxtHsalidaperm').datetimepicker('minDate', moment('08:00 AM', 'hh:mm A') );
    $('#mtxtHsalidaperm').datetimepicker('maxDate', moment('05:45 PM', 'hh:mm A') );
    $('#mtxtHsalidaperm').datetimepicker('date', moment('08:00 AM', 'hh:mm A') );

    $('#mtxtHretornoperm').datetimepicker('date', moment('08:15 AM', 'hh:mm A') );  
    
    $('#frmMantPermisos').validate({
        rules: {
        },
        messages: {
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        },        
        submitHandler: function (form) {
            const botonEvaluar = $('#mbtnGPerm');
            var request = $.ajax({
                url:$('#frmMantPermisos').attr("action"),
                type:$('#frmMantPermisos').attr("method"),
                data:$('#frmMantPermisos').serialize(),
                error: function(){
                    Vtitle = 'Error en Guardar!!!';
                    Vtype = 'error';
                    sweetalert(Vtitle,Vtype); 
                    objPrincipal.liberarBoton(botonEvaluar);
                },
                beforeSend: function() {
                    objPrincipal.botonCargando(botonEvaluar);
                }
            });
            request.done(function( respuesta ) {
                var posts = JSON.parse(respuesta);
                
                $.each(posts, function() {
                    Vtitle = 'Se Grabo Correctamente!!!';
                    Vtype = 'success';
                    sweetalert(Vtitle,Vtype); 
                    listarPermisos();   
                    objPrincipal.liberarBoton(botonEvaluar);    
                    $('#btnRetornarPerm').click();    
                });
            });
            return false;
        }
    });
	
});

$('#mtxtHsalidaperm').on('change.datetimepicker',function(e){
    $('#mtxtHretornoperm').datetimepicker({
        format: 'hh:mm A',
        locale:'es',
        stepping: 15
    });	 
    $('#mtxtHretornoperm').datetimepicker('minDate', e.date.add(15, "minute"));
    $('#mtxtHretornoperm').datetimepicker('maxDate', moment('06:00 PM', 'hh:mm A') );
    $('#mtxtHretornoperm').datetimepicker('date', moment(e.date, 'hh:mm A'));    
});

$('#btnNuevoPerm').click(function(){		
    $('#tabpermisos a[href="#tab_newpermisos"]').tab('show');
    $('#frmMantPermisos').trigger("reset");
    $('#mhdnIdPerm').val('');
    $('#mhdnAccionPerm').val('N');
    $('#mhdnEmpPerm').val($('#mhdnIdEmpleado').val());
    fechaActualperm();

});

$('#btnRetornarPerm').click(function(){
    $('#tabpermisos a[href="#tab_listapermisos"]').tab('show');  
});

fechaActualperm = function(){
    var fecha = new Date();		
    var fechatring = ("0" + fecha.getDate()).slice(-2) + "/" + ("0"+(fecha.getMonth()+1)).slice(-2) + "/" +fecha.getFullYear() ;

    $('#mtxtFregistroperm').val(fechatring);
    $('#mtxtFsalidaperm').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY') );

    $('#mtxtHsalidaperm').datetimepicker('minDate', moment('08:00 AM', 'hh:mm A') );
    $('#mtxtHsalidaperm').datetimepicker('maxDate', moment('05:45 PM', 'hh:mm A') );
    $('#mtxtHsalidaperm').datetimepicker('date', moment('08:00 AM', 'hh:mm A') );
    
    $('#mtxtHretornoperm').datetimepicker('date', moment('08:15 AM', 'hh:mm A') );
    
    $("#mtxtFsalidaperm").trigger("change.datetimepicker");
};

listarPermisos = function(){        
    oTable_listapermisos = $('#tblPermisos').DataTable({
        "processing"  	: true,
        "bDestroy"    	: true,
        "stateSave"     : true,
        "bJQueryUI"     : true,
        "scrollY"     	: "400px",
        "scrollX"     	: true, 
        'AutoWidth'     : true,
        "paging"      	: false,
        "info"        	: false,
        "filter"      	: true, 
        "ordering"		: false,
        "responsive"    : false,
        "select"        : true, 
        'ajax'        : {
            "url"   : baseurl+"adm/rrhh/cctrlpermisos/getlistpermisos/",
            "type"  : "POST", 
            "data": function ( d ) { 
                d.id_empleado     = $('#mhdnIdEmpleado').val(); 
            },     
            dataSrc : ''        
        },
        'columns'     : [
            {data: 'fsalida', "class": "dt-body-center col-s"},
            {data: 'horapermisos', "class": "dt-body-center col-s"},
            {data: 'horasuso', "class": "dt-body-center col-s"},
            {data: 'desc_motivo'},   
            {data: 'fundamentacion'},   
            {"orderable": false, 
                render:function(data, type, row){
                    return '<div>' +
                            '<a title="Editar" style="cursor:pointer; color:#3c763d;" onclick="javascript:selePermisos(\''+row.id_permisosvacaciones+'\',\''+row.id_empleado+'\',\''+row.fecha_registro+'\',\''+row.fsalida+'\',\''+row.hora_salida+'\',\''+row.hora_retorno+'\',\''+row.motivo+'\',\''+row.fundamentacion+'\',\''+row.recupera_hora+'\',\''+row.fecha_recuperacion+'\');"><span class="fas fa-edit fa-2x" aria-hidden="true"> </span> </a>' +
                            '</div>' ; 
                }
            },                        
            {"orderable": false, 
                render:function(data, type, row){
                    return '<div>' +
                            '<a id="aDelPerm" href="'+row.id_permisosvacaciones+'" title="Eliminar" style="cursor:pointer; color:#FF0000;"><span class="fas fa-trash-alt fa-2x" aria-hidden="true"> </span></a>'+
                            '</div>' ; 
                }
            }            
        ],      
    });
};

selePermisos = function(id_permisosvacaciones,id_empleado,fecha_registro, fecha_salida, hora_salida, hora_retorno, motivo, fundamentacion, recupera_hora, fecha_recuperacion){
    $('#tabpermisos a[href="#tab_newpermisos"]').tab('show'); 

    $('#mhdnIdPerm').val(id_permisosvacaciones)
    $('#mhdnAccionPerm').val('A'); 
    $('#mhdnEmpPerm').val(id_empleado);

    $('#mtxtFregistroperm').val(fecha_registro);
	$('#mtxtFsalperm').val(fecha_salida);
	$('#mtxtHsalperm').val(hora_salida);
	$('#mtxtHretorperm').val(hora_retorno);
    $('#mcboMotivo').val(motivo).trigger("change");
    
    $('#mtxtFundamentoperm').val(fundamentacion);
	$('#cboRecuperahora').val(recupera_hora).trigger("change");
    if(fecha_recuperacion == 'null'){
        $('#mtxtFrecuperm').val('');   
    }else{
        $('#mtxtFrecuperm').val(fecha_recuperacion);   
    }
};
    
$("body").on("click","#aDelPerm",function(event){
    event.preventDefault();
    idpermiso = $(this).attr("href");
    
    var params = { 
        "vidpermisosvacaciones"   : idpermiso,
    };

    Swal.fire({
        title: 'Confirmar Eliminar Registro',
        text: "¿Está seguro de Eliminar Registro?",
        type: 'error',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Eliminar!'
    }).then((result) => {
        if (result.value) {
            var request = $.ajax({
                type: 'POST',
                url: baseurl+"adm/rrhh/cctrlpermisos/delpermisosvacaciones/",
                async: true,
                data: params,
                error: function(){
                    Vtitle = 'Error en Guardar!!!';
                    Vtype = 'error';
                    sweetalert(Vtitle,Vtype); 
                }
            });
            request.done(function( respuesta ) {
                var posts = JSON.parse(respuesta);
                
                $.each(posts, function() {
                    Vtitle = 'Se Elimino Correctamente!!!';
                    Vtype = 'success';
                    sweetalert(Vtitle,Vtype);         
                    oTable_listapermisos.ajax.reload(null,false);  
                });
            });
        }
    }) 
}); 

$('#cboRecuperahora').change(function( event ){ 
    var v_recuhora = $( "#cboRecuperahora option:selected").attr("value");
    if(v_recuhora == 'V'){
        $('#mtxtFrecuperm').val(''); 
    }else{
        var fecharecu = new Date();		
        var fechatringrecu = ("0" + fecharecu.getDate()).slice(-2) + "/" + ("0"+(fecharecu.getMonth()+1)).slice(-2) + "/" +fecharecu.getFullYear() ;
        $('#mtxtFrecuperm').datetimepicker('date', moment(fechatringrecu, 'DD/MM/YYYY') );
    }
});

$('#modalpermisos').on('hidden.bs.modal', function (e) {
    otblListCtrlpermiso.ajax.reload(null,false);
});

/*HORAS EXTRAS*/
regHorasextras = function(id_empleado){    
	$('#mhdnIdEmpleado').val(id_empleado);
};

$('#modalhorasextras').on('show.bs.modal', function (e) {

    $('#tabhorasextras a[href="#tab_listahorasextrastab"]').attr('class', 'disabled');
    $('#tabhorasextras a[href="#tab_newhorasextrastab"]').attr('class', 'disabled active');

    $('#tabhorasextras a[href="#tab_listahorasextrastab"]').not('#store-tab.disabled').click(function(event){
        $('#tabhorasextras a[href="#tabinforme-list"]').attr('class', 'active');
        $('#tabhorasextras a[href="#tab_newhorasextras"]').attr('class', '');
        return true;
    });
    $('#tabhorasextras a[href="#tab_newhorasextrastab"]').not('#bank-tab.disabled').click(function(event){
        $('#tabhorasextras a[href="#tab_newhorasextras"]').attr('class' ,'active');
        $('#tabhorasextras a[href="#tab_listahorasextras"]').attr('class', '');
        return true;
    });
    
    $('#tabhorasextras a[href="#tab_listahorasextras"]').click(function(event){return false;});
    $('#tabhorasextras a[href="#tab_newhorasextras"]').click(function(event){return false;});
	
    $('#tabhorasextras a[href="#tab_listahorasextras"]').tab('show');
    
    listarHorasextras();

    $('#mtxtFsalidahextras').datetimepicker({
        format: 'DD/MM/YYYY',
        daysOfWeekDisabled: [0],
        locale:'es'
    });	

    $('#mtxtHsalidahextras, #mtxtHretornohextras').datetimepicker({
        format: 'hh:mm A',
        locale:'es',
        stepping: 15
    });	
        
    $('#mtxtHsalidahextras').datetimepicker('minDate', moment('08:00 AM', 'hh:mm A') );
    $('#mtxtHsalidahextras').datetimepicker('maxDate', moment('05:45 PM', 'hh:mm A') );
    $('#mtxtFsalidahextras').datetimepicker('date', moment('08:00 AM', 'hh:mm A') );

    $('#mtxtHretornohextras').datetimepicker('date', moment('08:15 AM', 'hh:mm A') );  

    $('#frmMantHorasextras').validate({
        rules: {
        },
        messages: {
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        },        
        submitHandler: function (form) {
            const botonEvaluar = $('#mbtnGHextra');
            var request = $.ajax({
                url:$('#frmMantHorasextras').attr("action"),
                type:$('#frmMantHorasextras').attr("method"),
                data:$('#frmMantHorasextras').serialize(),
                error: function(){
                    Vtitle = 'Error en Guardar!!!';
                    Vtype = 'error';
                    sweetalert(Vtitle,Vtype); 
                    objPrincipal.liberarBoton(botonEvaluar);
                },
                beforeSend: function() {
                    objPrincipal.botonCargando(botonEvaluar);
                }
            });
            request.done(function( respuesta ) {
                var posts = JSON.parse(respuesta);
                
                $.each(posts, function() {
                    Vtitle = 'Se Grabo Correctamente!!!';
                    Vtype = 'success';
                    sweetalert(Vtitle,Vtype); 
                    listarHorasextras();   
                    objPrincipal.liberarBoton(botonEvaluar);    
                    $('#btnRetornarHextra').click();    
                });
            });
            return false;
        }
    });
	
});

$('#mtxtHsalidahextras').on('change.datetimepicker',function(e){
    $('#mtxtHretornohextras').datetimepicker({
        format: 'hh:mm A',
        locale:'es',
        stepping: 15
    });	 
    $('#mtxtHretornohextras').datetimepicker('minDate', e.date.add(15, "minute"));
    $('#mtxtHretornohextras').datetimepicker('maxDate', moment('11:00 PM', 'hh:mm A') );
    $('#mtxtHretornohextras').datetimepicker('date', moment(e.date, 'hh:mm A'));    
});

$('#btnNuevohextras').click(function(){		
    $('#tabhorasextras a[href="#tab_newhorasextras"]').tab('show');
    $('#frmMantHorasextras').trigger("reset");
    $('#mhdnIdHextra').val('');
    $('#mhdnAccionHextra').val('N');
    $('#mhdnEmpHextra').val($('#mhdnIdEmpleado').val());
    fechaActualhextras();

});

$('#btnRetornarHextra').click(function(){
    $('#tabhorasextras a[href="#tab_listahorasextras"]').tab('show');  
});

fechaActualhextras= function(){
    var fecha = new Date();		
    var fechatring = ("0" + fecha.getDate()).slice(-2) + "/" + ("0"+(fecha.getMonth()+1)).slice(-2) + "/" +fecha.getFullYear() ;

    $('#mtxtFregistrohextras').val(fechatring);
    $('#mtxtFsalidahextras').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY') );

    $('#mtxtHsalidahextras').datetimepicker('minDate', moment('08:00 AM', 'hh:mm A') );
    $('#mtxtHsalidahextras').datetimepicker('maxDate', moment('10:45 PM', 'hh:mm A') );
    $('#mtxtHsalidahextras').datetimepicker('date', moment('08:00 AM', 'hh:mm A') );
    
    $('#mtxtHretornohextras').datetimepicker('date', moment('08:15 AM', 'hh:mm A') );
    
    $("#mtxtFsalidahextras").trigger("change.datetimepicker");
};

listarHorasextras = function(){        
    oTable_listahorasextras = $('#tblHorasextras').DataTable({
        "processing"  	: true,
        "bDestroy"    	: true,
        "stateSave"     : true,
        "bJQueryUI"     : true,
        "scrollY"     	: "400px",
        "scrollX"     	: true, 
        'AutoWidth'     : true,
        "paging"      	: false,
        "info"        	: true,
        "filter"      	: true, 
        "ordering"		: false,
        "responsive"    : false,
        "select"        : true, 
        'ajax'        : {
            "url"   : baseurl+"adm/rrhh/cctrlpermisos/getlisthorasextras",
            "type"  : "POST", 
            "data": function ( d ) { 
                d.id_empleado     = $('#mhdnIdEmpleado').val(); 
            },     
            dataSrc : ''        
        },
        "columns"     : [
            {data: 'fhoraextra', "class": "dt-body-center col-s"},
            {data: 'hora_entrada', "class": "dt-body-center col-s"},
            {data: 'hora_salida', "class": "dt-body-right col-s"},
            {data: 'fundamentacion'},                        
            {"orderable": false, 
                render:function(data, type, row){
                    return '<div>' +
                            '<a title="Editar" style="cursor:pointer; color:#3c763d;" onclick="javascript:seleHorasextras(\''+row.id_permisosvacaciones+'\',\''+row.id_empleado+'\',\''+row.fecha_registro+'\',\''+row.fhoraextra+'\',\''+row.hora_entrada+'\',\''+row.hora_salida+'\',\''+row.fundamentacion+'\');"><span class="fas fa-edit fa-2x" aria-hidden="true"> </span> </a>' +
                            '</div>' ; 
                }
            },                        
            {"orderable": false, 
                render:function(data, type, row){
                    return '<div>' +
                            '<a id="aDelHextras" href="'+row.id_permisosvacaciones+'" title="Eliminar" style="cursor:pointer; color:#FF0000;"><span class="fas fa-trash-alt fa-2x" aria-hidden="true"> </span></a>'+
                            '</div>' ; 
                }
            }            
        ]       
    });
};

seleHorasextras = function(id_permisosvacaciones,id_empleado,fecha_registro, fhoraextra, hora_entrada, hora_salida, fundamentacion){
    $('#tabhorasextras a[href="#tab_newhorasextras"]').tab('show'); 

    $('#mhdnIdHextra').val(id_permisosvacaciones);
    $('#mhdnAccionHextra').val('A'); 
    $('#mhdnEmpHextra').val(id_empleado);

    $('#mtxtFregistrohextras').val(fecha_registro);
	$('#mtxtFsalhextras').val(fhoraextra);
	$('#mtxtHsalhextras').val(hora_entrada);
	$('#mtxtHretorhextras').val(hora_salida);
    
    $('#mtxtMotivohextras').val(fundamentacion);
};
   
$("body").on("click","#aDelHextras",function(event){
    event.preventDefault();
    idhorasextras = $(this).attr("href");
    
    var params = { 
        "vidpermisosvacaciones"   : idhorasextras,
    };

    Swal.fire({
        title: 'Confirmar Eliminar Registro',
        text: "¿Está seguro de Eliminar Registro?",
        type: 'error',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Eliminar!'
    }).then((result) => {
        if (result.value) {
            var request = $.ajax({
                type: 'POST',
                url: baseurl+"adm/rrhh/cctrlpermisos/delpermisosvacaciones/",
                async: true,
                data: params,
                error: function(){
                    Vtitle = 'Error en Guardar!!!';
                    Vtype = 'error';
                    sweetalert(Vtitle,Vtype); 
                }
            });
            request.done(function( respuesta ) {
                var posts = JSON.parse(respuesta);
                
                $.each(posts, function() {
                    Vtitle = 'Se Elimino Correctamente!!!';
                    Vtype = 'success';
                    sweetalert(Vtitle,Vtype);         
                    oTable_listahorasextras.ajax.reload(null,false);  
                });
            });
        }
    }) 
}); 

$('#modalhorasextras').on('hidden.bs.modal', function (e) {
    otblListCtrlpermiso.ajax.reload(null,false);
});

/*DESCANSO MEDICO*/
regDescansomedico = function(id_empleado){    
	$('#mhdnIdEmpleado').val(id_empleado);
};

$('#modaldescansomedico').on('show.bs.modal', function (e) {

    $('#tabdescan a[href="#tab_listadescantab"]').attr('class', 'disabled');
    $('#tabdescan a[href="#tab_newdescantab"]').attr('class', 'disabled active');

    $('#tabdescan a[href="#tab_listadescantab"]').not('#store-tab.disabled').click(function(event){
        $('#tabdescan a[href="#tabinforme-list"]').attr('class', 'active');
        $('#tabdescan a[href="#tab_newdescan"]').attr('class', '');
        return true;
    });
    $('#tabdescan a[href="#tab_newdescantab"]').not('#bank-tab.disabled').click(function(event){
        $('#tabdescan a[href="#tab_newdescan"]').attr('class' ,'active');
        $('#tabdescan a[href="#tab_listadescan"]').attr('class', '');
        return true;
    });
    
    $('#tabdescan a[href="#tab_listadescan"]').click(function(event){return false;});
    $('#tabdescan a[href="#tab_newdescan"]').click(function(event){return false;});
	
    $('#tabdescan a[href="#tab_listadescan"]').tab('show');
    
    listarDescansomedico();

    $('#mtxtFsalidadescan,mtxtFretornodescan').datetimepicker({
        format: 'DD/MM/YYYY',
        daysOfWeekDisabled: [0],
        locale:'es'
    });	

    $('#frmMantDescansomedico').validate({
        rules: {
        },
        messages: {
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        },        
        submitHandler: function (form) {
            const botonEvaluar = $('#mbtnGDescan');
            var request = $.ajax({
                url:$('#frmMantDescansomedico').attr("action"),
                type:$('#frmMantDescansomedico').attr("method"),
                data:$('#frmMantDescansomedico').serialize(),
                error: function(){
                    Vtitle = 'Error en Guardar!!!';
                    Vtype = 'error';
                    sweetalert(Vtitle,Vtype); 
                    objPrincipal.liberarBoton(botonEvaluar);
                },
                beforeSend: function() {
                    objPrincipal.botonCargando(botonEvaluar);
                }
            });
            request.done(function( respuesta ) {
                var posts = JSON.parse(respuesta);
                
                $.each(posts, function() {
                    Vtitle = 'Se Grabo Correctamente!!!';
                    Vtype = 'success';
                    sweetalert(Vtitle,Vtype); 
                    listarDescansomedico();   
                    objPrincipal.liberarBoton(botonEvaluar);    
                    $('#btnRetornarDescan').click();    
                });
            });
            return false;
        }
    });
	
});

$('#mtxtFsalidadescan').on('change.datetimepicker',function(e){  
        
    $('#mtxtFretornodescan').datetimepicker({
        format: 'DD/MM/YYYY',
        daysOfWeekDisabled: [0],
        locale:'es'
    });

    var fecha = moment(e.date).format('DD/MM/YYYY');
    var newDate = moment(e.date); 
           
    newDate.add(1, 'days');
    nfecha = moment(newDate).format('DD/MM/YYYY');

    $('#mtxtFretornodescan').datetimepicker('minDate', fecha);
    $('#mtxtFretornodescan').datetimepicker('date', nfecha);
    
});

$('#btnNuevoDescan').click(function(){		
    $('#tabdescan a[href="#tab_newdescan"]').tab('show');
    $('#frmMantDescansomedico').trigger("reset");
    $('#mhdnIdDescan').val('');
    $('#mhdnAccionDescan').val('N');
    $('#mhdnEmpDescan').val($('#mhdnIdEmpleado').val());
    fechaActualdescan();

});

$('#btnRetornarDescan').click(function(){
    $('#tabdescan a[href="#tab_listadescan"]').tab('show');  
});

fechaActualdescan= function(){
    var fecha = new Date();		
    var fechatring = ("0" + fecha.getDate()).slice(-2) + "/" + ("0"+(fecha.getMonth()+1)).slice(-2) + "/" +fecha.getFullYear() ;
   
    $('#mtxtFregistrodescan').val(fechatring);
   
    $('#mtxtFsalidadescan').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY') );		
    $("#mtxtFsalidadescan").trigger("change.datetimepicker");
};

listarDescansomedico = function(){        
    oTable_listadescansomedico = $('#tblDescansomedico').DataTable({
        "processing"  	: true,
        "bDestroy"    	: true,
        "stateSave"     : true,
        "bJQueryUI"     : true,
        "scrollY"     	: "500px",
        "scrollX"     	: true, 
        'AutoWidth'     : true,
        "paging"      	: false,
        "info"        	: true,
        "filter"      	: false, 
        "ordering"		: false,
        "responsive"    : false,
        "select"        : true, 
        'ajax'        : {
            "url"   : baseurl+"adm/rrhh/cctrlpermisos/getlistdescansomedico",
            "type"  : "POST", 
            "data": function ( d ) { 
                d.id_empleado     = $('#mhdnIdEmpleado').val(); 
            },     
            dataSrc : ''        
        },
        "columns"     : [
            {data: 'fsalida', "class": "dt-body-center col-s"},
            {data: 'fretorno', "class": "dt-body-center col-s"},
            {data: 'diastomados', "class": "dt-body-right col-s"},
            {data: 'fundamentacion'},                        
            {"orderable": false, 
                render:function(data, type, row){
                    return '<div>' +
                            '<a title="Editar" style="cursor:pointer; color:#3c763d;" onclick="javascript:seleDescansomedico(\''+row.id_permisosvacaciones+'\',\''+row.id_empleado+'\',\''+row.fecha_registro+'\',\''+row.fsalida+'\',\''+row.fretorno+'\',\''+row.fundamentacion+'\');"><span class="fas fa-edit fa-2x" aria-hidden="true"> </span> </a>' +
                            '</div>' ; 
                }
            },                        
            {"orderable": false, 
                render:function(data, type, row){
                    return '<div>' +
                            '<a id="aDelDescan" href="'+row.id_permisosvacaciones+'" title="Eliminar" style="cursor:pointer; color:#FF0000;"><span class="fas fa-trash-alt fa-2x" aria-hidden="true"> </span></a>'+
                            '</div>' ; 
                }
            }            
        ]       
    });
};

seleDescansomedico = function(id_permisosvacaciones,id_empleado,fecha_registro,fecha_salida,fecha_retorno,fundamentacion){
    $('#tabdescan a[href="#tab_newdescan"]').tab('show'); 

    $('#mhdnIdDescan').val(id_permisosvacaciones);
    $('#mhdnAccionDescan').val('A'); 
    $('#mhdnEmpDescan').val(id_empleado);

    $('#mtxtFregistrodescan').val(fecha_registro);         
    $('#mtxtFsaldescan').val(fecha_salida);        
    $('#mtxtFretodescan').val(fecha_retorno); 

    $('#mtxtFundamentodescan').val(fundamentacion); 
};
    
$("body").on("click","#aDelDescan",function(event){
    event.preventDefault();
    iddescansomedico = $(this).attr("href");
    
    $.post(baseurl+"adm/rrhh/cctrlpermisos/delpermisosvacaciones", 
    {
        vidpermisosvacaciones   : iddescansomedico,
    },      
    function(data){   	
        oTable_listadescansomedico.ajax.reload(null,false); 	
    });
}); 

$('#modaldescansomedico').on('hidden.bs.modal', function (e) {
    otblListCtrlpermiso.ajax.reload(null,false);
});

