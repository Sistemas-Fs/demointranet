/*!
 *
 * @version 1.0.0
 */

const objFormulario = {};

$(function() {

	objFormulario.grupoEmpresarial = function() {
		$.ajax({
			url: BASE_URL + 'oi/homologaciones/chomologaciones/lista_grupo_empresarial',
			method: 'GET',
			data: {},
			dataType: 'json'
		}).done(function(resp) {
			let opciones = '<option value="" >::Elegir::</option>';
			if (resp.items && Array.isArray(resp.items)) {
				resp.items.forEach(function(item) {
					opciones += '<option value="' + item.id + '" >';
					opciones += item.text;
					opciones += '</option>';
				});
			}
			$('#cboGrupoEmp').html(opciones);
		});
	};

	objFormulario.guardar = function() {
		const form = $('#fmrNewExpediente');
		const boton = $('#btnGuardarForm');
		const botonSalir = $('#btnSalirForm');
		$.ajax({
			url: BASE_URL + 'oi/homologaciones/chomologaciones/guardar_evaluacion',
			method: 'POST',
			data: form.serialize(),
			dataType: 'json',
			beforeSend: function() {
				objPrincipal.botonCargando(boton);
				botonSalir.prop('disabled', true);
			},
		}).done(function(resp) {
			objPrincipal.notify('success', resp.message);
			$('#evaluacion_id').val(resp.data);
			$('#txtIdExpediente').val(resp.data);
			$('#txtExp').val(resp.data);
			$("#tabExped-eval").attr('exp', resp.data);
			ListDetProducto(resp.data);
			console.log(resp);
		}).fail(function(jqxhr) {
			const mensaje = (jqxhr && jqxhr.responseJSON) ? jqxhr.responseJSON.message : 'Error en la petición HTTP';
			objPrincipal.notify('error', mensaje);
		}).always(function() {
			objPrincipal.liberarBoton(boton);
			botonSalir.prop('disabled', false);
		});
	};

});

$(document).ready(function() {

	$('#btnVistaPrevia').click(function() {
		const exp = $('#evaluacion_id').val();
		const url = BASE_URL + 'oi/homologaciones/chomologaciones/vista_previa/' + exp;
		const download = window.open(url, '_blank');
		if (!download) {
			objPrincipal.alert('warning', 'Habilite la ventana emergente de su navegador.');
		}
		download.focus();
	});

	$('#btnNuevoExp').click(function() {
		objFormulario.grupoEmpresarial();
		$('#evaluacion_id').val('');
		$("#cboCliente01").html('');
		$("#cboArea").html('');
		$("#cboTipoequipoReg05").html('');
		$("#cboEstadoProveedor").val('A');
		$("#cboProveedor").html('');
		$("#cboContacto1").html('');
		$("#txtEmail1").val('');
		$("#cboContacto2").html('');
		$("#txtEmail2").val('');

		$('#txtIdExpediente').val('');
		$('#txtExp').val('');

		$('#proveedor_nuevo').prop('checked', false);

		$("#tabExped-eval").attr('exp', '');

		$('#tblProdProveedor').DataTable().clear().draw();
		$('#tblDetProductoProveedor').DataTable().clear().draw();
		$("#tblDetProducto").DataTable().clear().draw();

		$('#tabExped a[href="#tabExped-new"]').tab('show');
	});

	$("#cboGrupoEmp").change(function () {
		var idGrupoEmp = $("#cboGrupoEmp").val();
		console.log(idGrupoEmp);
		$.ajax({
			url: BASE_URL + "oi/homologaciones/chomologaciones/lista_clientes",
			method: 'GET',
			data: {
				cgrupoemp: idGrupoEmp,
			},
			dataType: "JSON",
		}).done(function(resp) {
			let opciones = '<option value="" >::Elegir::</option>';
			if (resp.items && Array.isArray(resp.items)) {
				resp.items.forEach(function(item) {
					opciones += '<option value="' + item.CODIGO + '" >';
					opciones += item.CLIENTE;
					opciones += '</option>';
				});
			}
			$('#cboCliente01').html(opciones);
		});
	});

	$("#cboCliente01").change(function () {
		var cliente = $("#cboCliente01").val();
		var params = {
			"cliente": cliente
		}
		$.ajax({
			type: 'ajax',
			method: 'post',
			url: baseurl + "oi/homologaciones/chomologaciones/get_areaxcliente",
			dataType: "JSON",
			async: true,
			data: params,
			success: function (result) {
				$('#cboArea').html(result);
			},
			error: function () {
				alert('Error, No se puede autenticar por error');
			}
		});
		$.ajax({
			method: 'get',
			url: baseurl + "oi/homologaciones/chomologaciones/lista_proveedores",
			dataType: "JSON",
			data: params,
		}).done(function(resp) {
			let opciones = '<option value="" >::Elegir::</option>';
			if (resp.items && Array.isArray(resp.items)) {
				resp.items.forEach(function(item) {
					opciones += '<option value="' + item.CODIGO + '" >';
					opciones += item.CLIENTE;
					opciones += '</option>';
				});
			}
			$('#cboProveedor').html(opciones);
		});
	});

	// obtener contactos segun proveedor
	$("#cboProveedor").change(function () {
		var idproveedor = $("#cboProveedor").val();
		console.log('idproveedor', idproveedor)

		var parametros = {
			'idproveedor': idproveedor
		}
		$.ajax({
			type: 'ajax',
			method: 'post',
			url: baseurl + "oi/homologaciones/chomologaciones/getContactoProveedor",
			dataType: "JSON",
			async: true,
			data: parametros,
			success: function (result) {
				$('#cboContacto1,#cboContacto2').html(result).trigger("change");
			},
			error: function () {
				alert('Error, no se encontraron los datos');
			}
		})
	});

	$('#btnSalirForm').click(function() {
		$('#tabExped-list-tab').click();
	});

	$('#btnGuardarForm').click(objFormulario.guardar);

	$('#btnDescargarProductos').click(function() {
		var content = BASE_URL + 'oi/homologaciones/chomologaciones/descargar_producto/' + $('#evaluacion_id').val();
		var download = window.open(content, '_blank');
		if (download == null || typeof download == "undefined") {
			objApp.notify("Ventana emergente", "Habilite la ventana emergente de su navegador. Para continuar la descarga!", "warning");
		} else {
			download.focus();
		}
	});

});
