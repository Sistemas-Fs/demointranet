/*!
 *
 * @version 1.0.0
 */

const objReqHomoLista = {};

$(function() {

	objReqHomoLista.listaArea = function() {
		$.ajax({
			url: BASE_URL + 'oi/homologaciones/creqhomologacion/lista_area',
			method: 'POST',
			data: {},
			dataType: 'json',
			beforeSend: function() {}
		}).done(function(resp) {
			const elArea = $('#cboArea');
			let opciones = '';
			if (resp) {
				resp.forEach(function(item) {
					opciones += '<option value="' + item.CTIPO + '" >' + item.DREGISTRO + '</option>';
				});
			}
			elArea.html(opciones);
		});
	}

	objReqHomoLista.listaTipo = function() {
		const idArea = $('#cboArea').val();
		$.ajax({
			url: BASE_URL + 'oi/homologaciones/creqhomologacion/lista_tipo',
			method: 'POST',
			data: {
				idArea: idArea
			},
			dataType: 'json',
			beforeSend: function() {}
		}).done(function(resp) {
			const elArea = $('#cboTipo');
			let opciones = '';
			if (resp) {
				resp.forEach(function(item) {
					opciones += '<option value="' + item.CAREACLIENTE + '" >' + item.DAREACLIENTE + '</option>';
				});
			}
			elArea.html(opciones);
		});
	};

	objReqHomoLista.buscar = function() {
		const boton = $('#btnBuscar');
		const idTipo = $('#cboTipo').val();
		const idMarca = $('#cboMarca').val();
		objPrincipal.botonCargando(boton);
		const tblLista = $("#tblListado").DataTable({
			'responsive': true,
			'bJQueryUI': true,
			'scrollY': '650px',
			'scrollX': true,
			'paging': true,
			'processing': true,
			'bDestroy': true,
			"AutoWidth": false,
			'info': true,
			'filter': true,
			"ordering": false,
			'stateSave': true,
			"ajax": {
				"url": BASE_URL + 'oi/homologaciones/creqhomologacion/lista',
				"type": "POST",
				"data": function (d) {
					d.idTipo = idTipo;
					d.idMarca = idMarca;
				},
				dataSrc: function(data) {
					objPrincipal.liberarBoton(boton);
					return data;
				}
			},
			'columns': [
				{"orderable": false, data: 'norden', targets: 0},
				{"orderable": false, data: 'ddocumento', targets: 1},
				{
					"orderable": false,
					render: function (data, type, row) {
						return (row.stipocampo == 'D') ? 'Descripción' : 'Fecha sin Alarma';
					}
				},
				{
					"orderable": false,
					render: function (data, type, row) {
						return $('#cboArea option:selected').text();

					}
				},
				{"orderable": false, data: 'dareacliente', targets: 4},
				{
					"orderable": false,
					render: function (data, type, row) {
						return (row.zctipomarca == 'OM') ? 'Otras Marcas' : 'Marca Propia';
					}
				},
				{
					"orderable": false,
					render: function (data, type, row) {
						return (row.stitulo == 'S') ? 'Si' : 'No';
					}
				},
				{
					"orderable": false,
					render: function (data, type, row) {
						return (row.sregistro == 'A') ? 'Activo' : 'Inactivo';
					}
				},
			]
		});

		// Enumeracion
		tblLista.on('order.dt search.dt', function () {
			tblLista.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
				cell.innerHTML = i + 1;
			});
		}).draw();
	};

});

$(document).ready(function() {

	objReqHomoLista.listaArea();

	$('#cboArea').change(objReqHomoLista.listaTipo);

	$('#btnBuscar').click(objReqHomoLista.buscar);

});
