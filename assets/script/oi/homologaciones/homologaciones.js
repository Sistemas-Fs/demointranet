$(document).ready(function () {
	mostrarAdjuntoText(false, '');
	/* TRAER CLIENTES AL COMBOBOX*/
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/homologaciones/chomologaciones/getClientes",
		dataType: "JSON",
		async: true,
		success: function (result) {
			$('#cboCliente').html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error');
		}
	});

	/* TRAER LOS ESTADOS AL COMBOBOX*/

	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/homologaciones/chomologaciones/getEstadoExp",
		dataType: "JSON",
		async: true,
		success: function (result) {
			$('#cboEstado,#cboEstadoProducto').html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error');
		}
	});


	/* TRAER TIPO REQUISITO*/

	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/homologaciones/chomologaciones/getArea",
		dataType: "JSON",
		async: true,
		success: function (result) {
			$('#cboTipRequisito').html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error');
		}
	})

	/* FORMATO DE FECHAS */
	$('#txtFDesde,#txtFFinal,#txtFCobro,#txtFecRegRequ,#txtFecRecDoc,#txtFecPrimEval,#txtFechaLevObs,#txtFechaTerminos').datetimepicker({
		format: 'YYYY-MM-DD',
		daysOfWeekDisabled: [0]
	});

	$('#FechaInicial').val('2018-01-01');

	/* TRAER INFO SEGUN EL COMBOBOX CLIENTE */

	$("#btnBuscarExp").click(function () {
		var clienteprincipal = $("#cboCliente").val();
		var fi = $("#FechaInicial").val();
		var ff = $("#FechaTermino").val();
		var estado = $("#cboEstado").val();

		$("#tblDetalleExp").addClass("d-none");//OCUALTAR TABLA DETALLE EXP

		var cliente = $("#select2-cboCliente-container").attr("title"); //OBTENER NOMBRE DE EMPRESA
		$("#clienteExp").text(cliente); //ASIGNAR EL NOMBRE DE LA EMPRESA A MOSTRAR

		$("#tblListado").DataTable({
			'responsive': true,
			'bJQueryUI': true,
			'scrollY': '300px',
			'scrollX': true,
			'paging': true,
			'processing': true,
			'bDestroy': true,
			"AutoWidth": false,
			'info': true,
			'filter': true,
			"ordering": false,
			'stateSave': true,
			"ajax": {
				"url": baseurl + "oi/homologaciones/chomologaciones/getbuscarexpediente",
				"type": "POST",
				"data": function (d) {
					d.cliente = clienteprincipal,
						d.fecinicio = fi,
						d.fecfin = ff,
						d.estado = estado
				},
				dataSrc: ''

			},
			'columns': [

				{"orderable": false, data: 'EXPEDIENTE', targets: 0},
				{"orderable": false, data: 'FECREGISTRO', targets: 1},
				{"orderable": false, data: 'PROVEEDOR', targets: 2},
				{"orderable": false, data: 'RUC', targets: 3},
				{
					"orderable": false,
					render: function (data, type, row) {
						return '<button class="btn btn-secundary" onClick="javascript:ListarDetalleExp(\'' + row.EXPEDIENTE + '\')";><i class="fas fa-eye" data-original-title="listar"></i></button>';

					}
				}

			]

		});


	})


	ListarDetalleExp = function (expediente) {
		$("#tblDetalleExp").removeClass("d-none");

		var oTblDetExp = $("#tblListadoDetalle").DataTable({
			'responsive': true,
			'bJQueryUI': true,
			'scrollY': '300px',
			'scrollX': true,
			'paging': true,
			'processing': true,
			'bDestroy': true,
			"AutoWidth": false,
			'info': true,
			'filter': true,
			"ordering": false,
			'stateSave': true,
			"ajax": {
				"url": baseurl + "oi/homologaciones/chomologaciones/getbuscarproductoxespediente",
				"type": "POST",
				"data": function (d) {
					d.expediente = expediente
				},
				dataSrc: ''

			},
			'columns': [
				{
					"orderable": false,
					render: function (data, type, row) {
						return '<div>' +
							' <a onClick="javascript:detallExpediente(\'' + expediente + '\');"><i class="fas fa-eye" style="color:#088A08; cursor:pointer;"> </i> </a>' +
							'</div>'
					}
				},
				{"orderable": false, data: 'PRODUCTO', targets: 1},
				{"orderable": false, data: 'MARCA', targets: 2},
				{"orderable": false, data: 'SANITARIO', targets: 3},
				{"orderable": false, data: 'EMISION', targets: 4},
				{"orderable": false, data: 'VENCE', targets: 5},
				{"orderable": false, data: 'ESTADO', targets: 6},
				{"orderable": false, data: 'FINICIO', targets: 7},
				{"orderable": false, data: 'FFIN', targets: 8}


			]

		})

		$('#tblListadoDetalle tbody').on('click', 'tr', function () {
			if ($(this).hasClass('selected')) {
				$(this).removeClass('selected');
			} else {
				oTblDetExp.$('tr.selected').removeClass('selected');
				$(this).addClass('selected');
			}
		});


	}

	// PASAR AL SEGUNTO TAB CON DATOS
	detallExpediente = function (expediente) {
		$.ajax({
			type: 'ajax',
			method: 'post',
			url: baseurl + "oi/homologaciones/chomologaciones/buscar_expediente",
			dataType: "JSON",
			data: {
				expediente: expediente
			},
		}).done(function(resp) {
			$('#evaluacion_id').val(expediente);
			$("#tabExped-eval").attr('exp', expediente);
			const evalprod = resp.data.evalprod;
			const area = resp.data.area;
			const contactoprov1 = resp.data.contactoprov1;
			const contactoprov2 = resp.data.contactoprov2;

			// se imprimen los datos
			$('#cboCliente01').html('<option selected value="' + evalprod.CCLIENTEPRINCIPAL + '" >' + evalprod.DCLIENTEPRINCIPAL + '</option>');
			if (area) {
				$('#cboArea').html('<option selected value="' + area.CAREACLIENTE + '" >' + area.DAREACLIENTE + '</option>');
			} else {
				$.ajax({
					type: 'ajax',
					method: 'post',
					url: baseurl + "oi/homologaciones/chomologaciones/get_areaxcliente",
					dataType: "JSON",
					async: true,
					data: { "cliente": evalprod.CCLIENTEPRINCIPAL },
					success: function (result) {
						$('#cboArea').html(result);
					},
					error: function () {
						alert('Error, No se puede autenticar por error');
					}
				});
			}
			$('#cboProveedor').html('<option selected value="' + evalprod.CPROVEEDORCLIENTE + '" >' + evalprod.DPROVEEDORCLIENTE + '</option>');
			if (contactoprov1) {
				$('#cboContacto1').html('<option selected value="' + contactoprov1.CCONTACTO + '" >' + contactoprov1.DNOMBRE+' '+contactoprov1.DAPEPAT + '</option>');
				$('#txtEmail1').val(contactoprov1.DMAIL);
			} else {
				$('#txtEmail1').val('');
				$.ajax({
					type: 'ajax',
					method: 'post',
					url: baseurl + "oi/homologaciones/chomologaciones/getContactoProveedor",
					dataType: "JSON",
					async: true,
					data: { 'idproveedor': evalprod.CPROVEEDORCLIENTE },
					success: function (result) {
						$('#cboContacto1').html(result).trigger("change");
					},
					error: function () {
						alert('Error, no se encontraron los datos');
					}
				})
			}
			if (contactoprov2) {
				$('#cboContacto2').html('<option selected value="' + contactoprov2.CCONTACTO + '" >' + contactoprov2.DNOMBRE+' '+contactoprov2.DAPEPAT + '</option>');
				$('#txtEmail2').val(contactoprov2.DMAIL);
			} else {
				$('#txtEmail2').val('');
				$.ajax({
					type: 'ajax',
					method: 'post',
					url: baseurl + "oi/homologaciones/chomologaciones/getContactoProveedor",
					dataType: "JSON",
					async: true,
					data: { 'idproveedor': evalprod.CPROVEEDORCLIENTE },
					success: function (result) {
						$('#cboContacto2').html(result).trigger("change");
					},
					error: function () {
						alert('Error, no se encontraron los datos');
					}
				})
			}

			$('#proveedor_nuevo').prop('checked', (evalprod.SNUEVOPROVEEDOR == 'S'));

			// Se lsita el producto elegido
			ListDetProducto(expediente);
			//  $("#tblDetProducto").DataTable();

			// LLENAMOS EL INPUT HIDDEN CON EL QUE SE AGREGARA EL PRODUCTO EN EL 2DO TAB
			$("#txtIdExpediente").val(expediente);

			$('#tabExped a[href="#tabExped-new"]').tab('show');
		});
	}
	/* TABLA DETALLES DE PRODUCTO 2DO TAB */

	ListDetProducto = function (exp) {
		$("#tblDetProducto").DataTable({
			'responsive': true,
			'bJQueryUI': true,
			'scrollY': '300px',
			'scrollX': true,
			'paging': true,
			'processing': true,
			'bDestroy': true,
			"AutoWidth": false,
			'info': true,
			'filter': true,
			"ordering": false,
			'stateSave': true,
			'ajax': {
				"url": baseurl + "oi/homologaciones/chomologaciones/getbuscarproductoxespediente",
				"type": "POST",
				"data": function (d) {
					d.expediente = exp
				},
				dataSrc: ''
			},
			"columnDefs": [{
				"targets": [7],
				"data": null,
				"render": function (data, type, row) {

					return '<button class="btn btn-secundary"><i class="fas fa-trash" data-original-title="Eliminar Producto"></i></button> <button class="btn btn-secundary" data-toggle="modal" data-target="#ModalEditarProducto"><i class="fas fa-edit" data-original-title="Editar Producto"></i></button>';

				}
			}],

			'columns': [
				{
					"orderable": false,
					render: function (data, type, row) {
						return '<div> </div>'
					}

				},
				{"orderable": false, data: 'TIPOREQUISITO', targets: 1},
				{"orderable": false, data: 'PRODUCTO', targets: 2},
				{"orderable": false, data: 'MARCA', targets: 3},
				{"orderable": false, data: 'ENVASEPRIM', targets: 4},
				{"orderable": false, data: 'ENVASESECU', targets: 5},
				{"orderable": false, data: 'ESTADO', targets: 6},
				{
					"orderable": false, "class": "col-s",
					render: function (data, type, row) {
						let ORIGEN = (row.ORIGEN) ? row.ORIGEN : '';
						let CONDICIONALM = (row.CONDICIONALM) ? row.CONDICIONALM : '';
						let MARCA = (row.MARCA) ? row.MARCA : '';
						let VIDA_UTIL = (row.VIDA_UTIL) ? row.VIDA_UTIL : '';
						let ENVASEPRIM = (row.ENVASEPRIM) ? row.ENVASEPRIM : '';
						let ENVASESECU = (row.ENVASESECU) ? row.ENVASESECU : '';
						let FABRICANTE = (row.FABRICANTE) ? row.FABRICANTE : '';
						let FABRICADIREC = (row.FABRICADIREC) ? row.FABRICADIREC : '';
						let ALMACEN = (row.ALMACEN) ? row.ALMACEN : '';
						let DIRECCIONALM = (row.DIRECCIONALM) ? row.DIRECCIONALM : '';
						let DPLANTAORIGEN = (row.DPLANTAORIGEN) ? row.DPLANTAORIGEN : '';
						let DPAISORIGEN = (row.DPAISORIGEN) ? row.DPAISORIGEN : '';
						let DCODIGOSAP = (row.DCODIGOSAP) ? row.DCODIGOSAP : '';
						return '<div class="btn-group" >' +
							'<button type="button" role="button" class="btn btn-success btn-sm mr-2" data-toggle="modal" title="Editar" style="cursor:pointer;" data-target="#ModalEditarProducto" onClick="javascript:selProducto(\'' + exp + '\',\'' + row.IDPROD + '\',\'' + row.PRODUCTO + '\',\'' + row.IDTIPREQUI + '\',\'' + row.TIPOMARCA + '\',\'' + ORIGEN + '\',\'' + CONDICIONALM + '\',\'' + MARCA + '\',\'' + VIDA_UTIL + '\',\'' + ENVASEPRIM + '\',\'' + ENVASESECU + '\',\'' + FABRICANTE + '\',\'' + FABRICADIREC + '\',\'' + ALMACEN + '\',\'' + DIRECCIONALM + '\',\'' + DPLANTAORIGEN + '\',\'' + DPAISORIGEN + '\',\'' + DCODIGOSAP + '\');"><span class="fas fa-edit" aria-hidden="true"> </span> </button>' +
							'<button type="button" role="button" class="btn btn-danger btn-sm mr-2" onClick="javascript:SelDeleteProd(\'' + exp + '\',\'' + row.IDPROD + '\');" title="Eliminar" style="cursor:pointer;"><span class="fas fa-trash-alt" aria-hidden="true"> </span></button>' +
							'<a target="_blank" class="btn btn-info btn-sm" href="' + BASE_URL + 'oi/homologaciones/chomologaciones/vista_previa/' + exp + '/' + row.IDPROD + '" title="Vista Previa" style="cursor:pointer;"><span class="fas fa-file-pdf" aria-hidden="true"> </span></a>' +
							'</div>'
					}
				},

			]
		})

		$("#tabExped-eval").attr('exp', exp);
	}

	/* tabla lista de productos del proveedor */
	$("#tabExped-eval-tab").click(function () {

		var expediente = $("#tabExped-eval").attr('exp');
		if (expediente) {
			var params = {
				'expediente': expediente
			}

			$("#tblProdProveedor").DataTable({
				'responsive': true,
				'bJQueryUI': true,
				'scrollY': '300px',
				'scrollX': true,
				'paging': true,
				'processing': true,
				'bDestroy': true,
				"AutoWidth": true,
				'info': true,
				'filter': true,
				"ordering": false,
				'stateSave': true,
				"ajax": {
					"url": baseurl + "oi/homologaciones/chomologaciones/getbuscarproductoxespediente",
					"type": "POST",
					"data": params,
					dataSrc: ''

				},
				'columns': [
					{"orderable": false, data: 'NUM', targets: 0},
					{"orderable": false, data: 'ESTADO', targets: 1},
					{"orderable": false, data: 'PRODUCTO', targets: 2},
					{"orderable": false, data: 'MONTO', targets: 3},
					{"orderable": false, data: 'PAGAR', targets: 4},
					{"orderable": false, data: 'FCOBRO', targets: 5}

				],
				"columnDefs": [{
					"targets": [6],
					"data": null,
					"class": 'text-center',
					"render": function (data, type, row) {

						return '<div class="btn-group" >' +
							'<button type="button" role="button" class="btn btn-secondary btn-sm mr-2" onClick="javascript:verRequisitos(\'' + row.IDPROD + '\',\'' + expediente + '\',\'' + row.PRODUCTO + '\',\'' + row.IDTIPOREQU + '\'); "><i class="fas fa-eye" data-original-title="Ver Requisitos"></i></button>' +
							'<button type="button" role="button" class="btn btn-success btn-sm" data-toggle="modal" title="Editar" style="cursor:pointer;" data-target="#ModalEditarProductoProveedor" onClick="javascript:selProductoProveedor(\'' + expediente + '\',\'' + row.IDPROD + '\',\'' + row.IDESTADO + '\',\'' + row.PRODUCTO + '\',\'' + row.MONTO + '\',\'' + row.PAGAR + '\',\'' + row.FCOBRO + '\');"><span class="fas fa-edit" aria-hidden="true"> </span> </button>' +
							'</div>';

					}
				}, {
					"targets": [4],
					"data": null,
					"class": 'text-center',
					"render": function (data, type, row) {
						if (row.PAGAR == 'S') {
							return '<div class="custom-control custom-checkbox">' +
								'<input  checked  type="checkbox" disabled>' +
								'</div>';
						} else {
							return '-';
						}


					}
				}, {
					"targets": [3],
					"data": null,
					"class": 'text-center',
					"render": function (data, type, row) {
						if (row.MONTO == null) {
							return '<p class="text-muted">S./ 0.00</p>';
						} else {
							return 'S./ ' + row.MONTO;
						}


					}
				}, {
					"targets": [6],
					"data": null,
					"class": 'text-center',
					"render": function (data, type, row) {
						if (row.FCOBRO == 'null') {
							return '-';
						} else {
							return row.FCOBRO;
						}


					}
				}


				]
			});
		}
	});

})

verRequisitos = function (idprod, exp, producto, IDTIPOREQU) {
	$("#cardObservaciones").removeClass("invisible");
	$("#cardObservaciones").addClass("visible");

	$("#btnAddRequisito").removeClass("d-none");

	$("#txtIdRequisito").val(IDTIPOREQU);

	$("#ProductoNombre").text(producto);
	$("#ProductoNombre1").text(producto);

	/* COLOCAR ID Y EXP DEL PRODUCTO PARA GRTABAR Y EDITAR LOS REQUISITOS DE CADA PRODUCTO */

	$("#txtExpRequisito").val(exp);
	$("#txtIdProducto").val(idprod);


	var params = {
		'expediente': exp,
		'idprod': idprod
	}

	$("#tblDetProductoProveedor").DataTable({
		'responsive': true,
		'bJQueryUI': true,
		'scrollY': '300px',
		'scrollX': true,
		'paging': true,
		'processing': true,
		'bDestroy': true,
		"AutoWidth": false,
		'info': true,
		'filter': true,
		"ordering": false,
		'stateSave': true,
		"ajax": {
			"url": baseurl + "oi/homologaciones/chomologaciones/getbuscarequisitoxproducto",
			"type": "POST",
			"data": params,

			dataSrc: ''

		},
		'columns': [
			{"orderable": false, data: 'REQUISITO', targets: 0},
			{
				"orderable": false,
				render: function (data, type, row) {
					if (row.NOTA == null) {
						return '<p class="text-muted">N/A</p>';
					} else {
						return row.NOTA;
					}
				}
			},
			{
				"orderable": false,
				render: function (data, type, row) {
					if (row.DT_FECHA == null) {
						return '<p class="text-muted">Sin Fec.</p>';
					} else {
						return row.DT_FECHA;
					}
				}
			},
			{
				"orderable": false,
				render: function (data, type, row) {
					if (row.DESCRIPCION == null) {
						return '<p class="text-muted">N/A</p>';
					} else {
						return row.DESCRIPCION;
					}
				}
			},
			{
				"orderable": false,
				render: function (data, type, row) {
					switch (row.CONFORMIDAD) {
						case 'C':
							return 'Cumple';
							break;
						case 'P':
							return 'Pendiente';
							break;
						case 'A':
							return 'No Aplica';
							break;
						case 'N':
							return 'No Cumple';
							break;
						default:
							return '-';

					}
				}
			},
			{
				"orderable": false,
				render: function (data, type, row) {
					if (row.RUTA == null || row.RUTA == '') {
						return '<p class="text-muted">NO HAY ARCHIVO</p>';
					} else {
						return '<a href="' + baseurl + 'FTPfileserver/Archivos/' + row.RUTA + '" target="_blank"><img src="' + baseurl + 'assets/images/pdf.png" class="img-fluid"/></a>'
					}
				}
			},
			{
				"orderable": false,
				render: function (data, type, row) {
					if (row.TIPO == 'D') {
						return 'Descripcion';
					} else if (row.TIPO == 'F') {
						return 'Fecha sin Alarma';
					} else if (row.TIPO == 'A') {
						return 'Fecha con Alarma';
					}
				}
			}


		],
		"columnDefs": [{
			"targets": [7],
			"data": null,
			"render": function (data, type, row) {
				let nota = (row.NOTA) ? row.NOTA : '';
				let requisito = (row.REQUISITO) ? row.REQUISITO : '';
				let DESCRIPCION = (row.DESCRIPCION) ? row.DESCRIPCION : '';
				let fecha = (row.DT_FECHA) ? row.DT_FECHA : row.FECHA;
				fecha = (fecha) ? fecha : '';
				return '<div class="btn-group" >' +
					'<button type="button" role="button" class="btn btn-success btn-sm mr-2" data-toggle="modal" title="Editar" style="cursor:pointer;" data-target="#ModalAgregarRequisitoProducto" onClick="javascript:selRequisitoProducto(\'' + requisito + '\',\'' + nota + '\',\'' + fecha + '\',\'' + DESCRIPCION + '\',\'' + row.CONFORMIDAD + '\',\'' + row.RUTA + '\',\'' + row.TIPO + '\',\'' + row.IDREQUISITO + '\',\'' + row.IDREQU + '\');"><span class="fas fa-edit" aria-hidden="true"> </span> </button>' +
					'<button type="button" role="button" class="btn btn-danger btn-sm" onClick="javascript:SelDeleteReqProd(\'' + exp + '\',\'' + idprod + '\',\'' + row.IDREQU + '\');" title="Eliminar" style="cursor:pointer;"><span class="fas fa-trash-alt" aria-hidden="true"> </span></button>' +
					'</div>';
			}
		}
		]
	});


	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/homologaciones/chomologaciones/getbuscarobservacionxproducto",
		dataType: 'JSON',
		async: true,
		data: params,
		success: function (response) {


			$.each(response, function () {

				if (this.OBSERVACION == null) {
					$("#mtxtObservacion").text('');
					$("#mtxtObservacion").val('');
				} else {
					$("#mtxtObservacion").text(this.OBSERVACION);
					$("#mtxtObservacion").val(this.OBSERVACION);
				}

				if (this.ACUERDOS == null) {
					$("#mtxtAcuerdo").text('');
					$("#mtxtAcuerdo").val('');
				} else {
					$("#mtxtAcuerdo").text(this.ACUERDOS);
					$("#mtxtAcuerdo").val(this.ACUERDOS);
				}

				(this.RECEPDOC == null) ? $("#FechaRecepDoc").val('1900-01-01') : $("#FechaRecepDoc").val(this.RECEPDOC);
				(this.TIEMRESPROV == null) ? $("#tmpRespProv").val('') : $("#tmpRespProv").val(this.TIEMRESPROV);
				(this.PRIMEVAL == null) ? $("#FecPrimEval").val('1900-01-01') : $("#FecPrimEval").val(this.PRIMEVAL);
				(this.TIEMRESPFSC == null) ? $("#tmpRespFsc").val('') : $("#tmpRespFsc").val(this.TIEMRESPFSC);
				(this.LEVAOBSERV == null) ? $("#FechaLevObs").val('1900-01-01') : $("#FechaLevObs").val(this.LEVAOBSERV);
				(this.FINPROCESO == null) ? $("#FechaTerminos").val('1900-01-01') : $("#FechaTerminos").val(this.FINPROCESO);
				(this.TIMEDURACION == null) ? $("#tmpDuracion").val('') : $("#tmpDuracion").val(this.TIMEDURACION);


				//$("#txtmpPrimEval").val(this.RECEPDOC);

			});

			/* ASIGNAR ID Y EXPEDIENTE PARA EDITAR LAS OBSERVACIONES */

			if (idprod != null && exp != null) {
				$("#txtIdProdObservacion").val(idprod);
				$("#txtExpedienteObservacion").val(exp);
			} else {

				$("#txtIdProdObservacion").val('');
				$("#txtExpedienteObservacion").val('');
			}


		}
	});


}

// BUSCAR AREA SEGUN CLIENTE

$("#cboCliente").change(function () {

	var cliente = $("#cboCliente").val();
	var params = {
		"cliente": cliente
	}

	// $.ajax({
	// 	type: 'ajax',
	// 	method: 'post',
	// 	url: baseurl + "oi/homologaciones/chomologaciones/getProveedorxCliente",
	// 	dataType: "JSON",
	// 	async: true,
	// 	data: params,
	// 	success: function (result) {
	// 		$('#cboProveedor').html(result);
	// 	},
	// 	error: function () {
	// 		alert('Error, No se puede autenticar por error');
	// 	}
	// })

	// $.ajax({
	// 	type: 'ajax',
	// 	method: 'post',
	// 	url: baseurl + "oi/homologaciones/chomologaciones/getArea",
	// 	dataType: "JSON",
	// 	async: true,
	// 	data: params,
	// 	success: function (result) {
	// 		$('#cboArea').html(result);
	// 	},
	// 	error: function () {
	// 		alert('Error, No se puede autenticar por error');
	// 	}
	// })

})

//obtener email por contacto
$("#cboContacto1").change(function () {
	var emailContacto = $("#cboContacto1>option:selected").attr("email");
	$("#txtEmail1").val(emailContacto);
})

$("#cboContacto2").change(function () {
	var emailContacto2 = $("#cboContacto2>option:selected").attr("email");
	$("#txtEmail2").val(emailContacto2);
})
/* ACTIVAR/DESACTIVAR ESTADO DEL EXPEDIENTE CON UN CHECKBOX */

$("#checkTodos").change(function () {
	if ($("#checkTodos").is(":checked") == true) {
		$("#cboEstado").prop("disabled", true);
	} else if ($("#checkTodos").is(":checked") == false) {
		$("#cboEstado").prop("disabled", false);
	}
	;
})


// GUARDAR PRODUCTO DEL TAB 2

$("#btnGuardarProductos").click(function () {
	var datos = $("#frmProductosDos").serialize();

	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + 'oi/homologaciones/chomologaciones/insertarProducto',
		dataType: 'json',
		async: true,
		data: datos,
		success: function (result) {
			if (result == 1) {
				$("#frmProductosDos")[0].reset();
				$('#ModalEditarProducto').modal('hide');
				$("#tblDetProducto").DataTable().ajax.reload();
				Vtitle = 'Se agrego correctamente';
				Vtype = 'success';
				sweetalert(Vtitle, Vtype);

			} else if (result == 2) {
				Vtitle = 'Llene los campos requeridos.';
				Vtype = 'warning';
				sweetalert(Vtitle, Vtype);
			} else {
				Vtitle = 'Problemas al Agregar';
				Vtype = 'error';
				sweetalert(Vtitle, Vtype);
			}
		},
		error: function () {


			Vtitle = 'Problemas con el Servidor, solicita ayuda!';
			Vtype = 'error';
			sweetalert(Vtitle, Vtype);

		}
	})
})

/* TRAER DATOS PARA EDITAR PRODUCTOS EN EL TEB 2 */

selProducto = function (EXP, IDPROD, PRODUCTO, IDTIPREQUI, TIPOMARCA, ORIGEN, CONDICIONALM, MARCA, VIDAUTIL, ENVASEPRIM, ENVASESECU, FABRICANTE, FABRICADIREC, ALMACEN, DIRECCIONALM, DPLANTAORIGEN, DPAISORIGEN, DCODIGOSAP) {

	PRODUCTO = (PRODUCTO) ? PRODUCTO : '';
	CONDICIONALM = (CONDICIONALM) ? CONDICIONALM : '';
	MARCA = (MARCA) ? MARCA : '';
	VIDAUTIL = (VIDAUTIL) ? VIDAUTIL : '';
	ENVASEPRIM = (ENVASEPRIM) ? ENVASEPRIM : '';
	ENVASESECU = (ENVASESECU) ? ENVASESECU : '';
	FABRICANTE = (FABRICANTE) ? FABRICANTE : '';
	FABRICADIREC = (FABRICADIREC) ? FABRICADIREC : '';
	ALMACEN = (ALMACEN) ? ALMACEN : '';
	DIRECCIONALM = (DIRECCIONALM) ? DIRECCIONALM : '';
	DPLANTAORIGEN = (DPLANTAORIGEN) ? DPLANTAORIGEN : '';
	DPAISORIGEN = (DPAISORIGEN) ? DPAISORIGEN : '';
	DCODIGOSAP = (DCODIGOSAP) ? DCODIGOSAP : '';

	$("#txtIdExpediente").val(EXP);
	$("#idProductoEdit").val(IDPROD);
	$("#txtProducto").val(PRODUCTO);
	$("#cboTipRequisito").val(IDTIPREQUI).trigger("change");
	$("#cboTipoMarca").val(TIPOMARCA).trigger("change");
	$("#cboOrigenProd").val(ORIGEN).trigger("change");
	$("#txtCondicionAlmacen").val(CONDICIONALM);
	$("#txtMarca").val(MARCA);
	$("#txtVidautil").val(VIDAUTIL);
	$("#txtEnvPrimario").val(ENVASEPRIM);
	$("#txtEnvSecundario").val(ENVASESECU);
	$("#txtFabricante").val(FABRICANTE);
	$("#txtDirFabricante").val(FABRICADIREC);
	$("#txtAlmacen").val(ALMACEN);
	$("#txtDirecAlmacen").val(DIRECCIONALM);
	$("#txtplantaorigen").val(DPLANTAORIGEN);
	$("#txtpaisorigen").val(DPAISORIGEN);
	$("#txtcodigosap").val(DCODIGOSAP);

}

$("#ModalProductoDos").click(function () {
	const idExpediente = $('#txtIdExpediente').val();
	if (idExpediente) {
		$("#frmProductosDos")[0].reset();
		$("#idProductoEdit").val('');
		$("#cboTipRequisito").val('').trigger('change');
		$("#cboTipoMarca").val('').trigger('change');
		$("#cboOrigenProd").val('').trigger('change');
		$('#ModalEditarProducto').modal('show');
	} else {
		objPrincipal.notify('info', 'Por favor, guarde primero el registro de evaluación');
	}
})


//  ELIMINAR/DESACTIVAR PRODUCTO

SelDeleteProd = function (exp, idprod) {


	var datos = {
		expediente: exp,
		idprod: idprod
	};


	Swal.fire({
		title: 'Confirmar Eliminación',
		text: "¿Está seguro de eliminar el Producto?",
		icon: 'error',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Si, bórralo!'
	}).then((result) => {
		if (result.value) {

			$.ajax({
				type: 'ajax',
				method: 'post',
				url: baseurl + 'oi/homologaciones/chomologaciones/deleteProducto',
				dataType: 'JSON',
				async: true,
				data: datos,
				success: function (result) {
					if (result == 1) {
						$("#tblDetProducto").DataTable().ajax.reload();
						Vtitle = 'Se Elimino Correctamente';
						Vtype = 'success';
						sweetalert(Vtitle, Vtype);

					} else {
						Vtitle = 'Ups,no se puede Eliminar!';
						Vtype = 'error';
						sweetalert(Vtitle, Vtype);
					}

				},
				error: function () {
					Vtitle = 'Problemas con el Servidor!';
					Vtype = 'error';
					sweetalert(Vtitle, Vtype);
				}
			})
		}
	})

}

//EDITAR PRODUCTOS PROVEEDOR

selProductoProveedor = function (exp, idprod, estado, producto, monto, pago, fcobro) {
	$("#txtExp").val(exp);
	$("#txtidProduc").val(idprod);
	$("#cboEstadoProducto").val(estado).trigger("change");
	$("#txtProductoTab3").val(producto);

	if (monto == 'null') {
		$("#txtMonto").val('0.00');
	} else {
		$("#txtMonto").val(monto);
	}

	if (pago == 'null') {
		$("#cboPagoCliente").val('').trigger("change");
	} else {
		$("#cboPagoCliente").val(pago).trigger("change");
	}

	if (fcobro == 'null') {
		$("#FechaCobro").val('');
	} else {
		$("#FechaCobro").val(fcobro);
	}

	//TRAER PRODUCTOS CON SEGUN EL TIPO PRODUCTO PROCEDURE CREADO....

}


/* GUARDAR DATOS Y OBSERVACIONES EN REUISITOS DE PRODUCTOS */
$("#btnSaveObservacionReq").click(function () {
	var datos = $("#frmRequisitoProdObs").serialize();

	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + 'oi/homologaciones/chomologaciones/insertarObsRequisito',
		dataType: 'json',
		async: true,
		data: datos,
		success: function (result) {
			if (result == 1) {
				$("#tblProdProveedor").DataTable().ajax.reload();
				Vtitle = 'Se actualizó correctamente';
				Vtype = 'success';
				sweetalert(Vtitle, Vtype);

			} else if (result == 2) {
				Vtitle = 'Ingrese los campos requeridos';
				Vtype = 'warning';
				sweetalert(Vtitle, Vtype);
			}
		},
		error: function () {


			Vtitle = 'Problemas con el Servidor, solicita ayuda!';
			Vtype = 'error';
			sweetalert(Vtitle, Vtype);

		}
	})
});

$("#btnSaveProductoTabTres").click(function () {
	var datos = $("#frmProductoProveeddor").serialize();

	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + 'oi/homologaciones/chomologaciones/insertarProductoProveedor',
		dataType: 'json',
		async: true,
		data: datos,
		success: function (result) {
			if (result == 1) {

				$("#tblProdProveedor").DataTable().ajax.reload();
				$("#ModalEditarProductoProveedor").modal('hide');
				Vtitle = 'Se agrego correctamente';
				Vtype = 'success';
				sweetalert(Vtitle, Vtype);

			} else if (result == 2) {
				Vtitle = 'Ingrese los campos requeridos';
				Vtype = 'warning';
				sweetalert(Vtitle, Vtype);
			}
		},
		error: function () {


			Vtitle = 'Problemas con el Servidor, solicita ayuda!';
			Vtype = 'error';
			sweetalert(Vtitle, Vtype);

		}
	})
})


$("#cboPagoCliente").change(function () {
	var pagar = $("#cboPagoCliente").val();
	console.log('cboPagar', pagar)

	if (pagar == 'S') {
		$("#txtMonto").attr("readonly");
	}
})

function selRequisitoProducto(REQUISITO, NOTA, FECHA, DESCRIPCION, CONFORMIDAD, RUTA, TIPO, IDREQUISITO, IDREQU) {
	$('#fileRequisito').fileinput('clear');

	var params = {
		'tipoProd': IDREQUISITO
	}

	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/homologaciones/chomologaciones/getTipoRequisito",
		dataType: 'JSON',
		async: true,
		data: params,
		success: function (result) {
			$('#cboRequisito').html(result);
			$("#cboRequisito").val(IDREQU).trigger("change");
		},
		error: function () {
			alert('Error, No se puede autenticar por error');
		}
	});

	$("#txtAccion").val('U');
	$("#txtNotaReq").val(NOTA);
	$("#FecRegistroRequ").val(FECHA);
	$("#cboConformidad").val(CONFORMIDAD).trigger("change");
	$("#txtDescripcionRequisito").val(DESCRIPCION);
	$("#cboTipo").val(TIPO).trigger("change");

	if (RUTA != '' || RUTA != null) {
		console.log('RUTA', RUTA)
		$("#txtFileRequisito").val(RUTA);
		mostrarAdjuntoText(true, RUTA);
	} else {
		mostrarAdjuntoText(false, '');
	}


}

function mostrarAdjuntoText(status, file) { //FUNCION PARA MOSTRAR EL ADJUNTO EXISTENTE
	if (status) {
		$("#txtFileRequisitohtml").show();
		$("#txtFileRequisitohtml").text(file);
	} else {
		$("#txtFileRequisitohtml").hide();
		$("#txtFileRequisitohtml").text('');
	}
}

$("#btnCerrarRequisitos").click(function () {
	$("#txtNotaReq").val('');
	$("#FecRegistroRequ").val('');
	$("#cboConformidad").val('').trigger("change");
	$("#txtDescripcionRequisito").val('');
	$("#cboTipo").val('').trigger("change");
	$("#txtFileRequisito").val('');
	$("#cboRequisito").val('').trigger("change");
	$("#txtAccion").val('');
})

$("#btnSaveReqiusitoProd").click(function () {
	var cliente_id = $("#cboCliente01").val();
	var datos = $("#frmSaveRequisitoProducto").serialize() + '&cboCliente=' + cliente_id;

	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + 'oi/homologaciones/chomologaciones/insertarRequisitoProducto',
		dataType: 'json',
		async: true,
		data: datos,
		success: function (result) {
			if (result == 1) {
				$("#tblDetProductoProveedor").DataTable().ajax.reload();
				$("#btnCerrarRequisitos").click();
				Vtitle = 'Se agrego correctamente';
				Vtype = 'success';
				sweetalert(Vtitle, Vtype);

			} else if (result == 2) {
				Vtitle = 'Ingrese los campos requeridos';
				Vtype = 'warning';
				sweetalert(Vtitle, Vtype);
			}
		},
		error: function () {


			Vtitle = 'Problemas con el Servidor, solicita ayuda!';
			Vtype = 'error';
			sweetalert(Vtitle, Vtype);

		}
	})
})

function SelDeleteReqProd(exp, idprod, idreq) {
	var datos = {
		expediente: exp,
		idprod: idprod,
		requisito: idreq
	};


	Swal.fire({
		title: 'Confirmar Eliminación',
		text: "¿Está seguro de eliminar el Requisito?",
		icon: 'error',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Si, bórralo!'
	}).then((result) => {
		if (result.value) {

			$.ajax({
				type: 'ajax',
				method: 'post',
				url: baseurl + 'oi/homologaciones/chomologaciones/deleteRequisitoProd',
				dataType: 'JSON',
				async: true,
				data: datos,
				success: function (result) {
					if (result == 1) {
						$("#tblDetProductoProveedor").DataTable().ajax.reload();
						Vtitle = 'Se Elimino Correctamente';
						Vtype = 'success';
						sweetalert(Vtitle, Vtype);

					} else {
						Vtitle = 'Ups,no se puede Eliminar!';
						Vtype = 'error';
						sweetalert(Vtitle, Vtype);
					}

				},
				error: function () {
					Vtitle = 'Problemas con el Servidor!';
					Vtype = 'error';
					sweetalert(Vtitle, Vtype);
				}
			})
		}
	})
}

$("#btnAddRequisito").click(function () {
	$('#fileRequisito,#txtFileRequisito').val('');
	mostrarAdjuntoText(false, '');
	var params = {
		'tipoProd': $("#txtIdRequisito").val()

	}
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/homologaciones/chomologaciones/getTipoRequisito",
		dataType: 'JSON',
		async: true,
		data: params,
		success: function (result) {
			$('#cboRequisito').html(result);

		},
		error: function () {
			alert('Error, No se puede autenticar por error');
		}
	});

	$("#txtAccion").val('I'); // "I" = insertar
})

$("#fileRequisito").fileinput({
	uploadAsync: false,
	minFileCount: 1,
	maxFileCount: 60000,
	showUpload: false,
	showRemove: true,
	overwriteInitial: true,
	maxFileSize: 60000,
	showClose: false,
	showCaption: false,
	removeIcon: '<i class="fa fa-remove"></i>',
	removeTitle: 'Eliminar Archivo',
	elErrorContainer: '#kv-avatar-errors-1',
	msgErrorClass: 'alert alert-block alert-danger'
});

$('#fileRequisito').on('filecleared', function (event) {
	$('#txtFileRequisito').val('');
	$('#fileRequisito').val('');
});

function registrar_archivo() {
	var idCliente = $("#cboCliente01").val();
	var expediente = $("#txtExpRequisito").val();
	var idProducto = $("#txtIdProducto").val();

	var archivoInput = document.getElementById('fileRequisito');
	var archivoRuta = archivoInput.value;
	var extPermitidas = /(.pdf|.docx|.xlsx|.doc|.xls|.rar|.zip)$/i;

	if (!extPermitidas.exec(archivoRuta)) {
		Vtitle = 'Asegurese de haber seleccionado un PDF, DOCX, XSLX, RAR, ZIP';
		Vtype = 'error';
		sweetalert(Vtitle, Vtype);
		archivoInput.value = '';
		return false;
	} else {
		var parametrotxt = new FormData($("#frmSaveRequisitoProducto")[0]);
		parametrotxt.append('cboCliente', idCliente);
		parametrotxt.append('txtExpRequisito', expediente);
		parametrotxt.append('txtIdProducto', idProducto);
		console.log('parametrotxt', parametrotxt)
		const botonCancelar = $('#btnCerrarRequisitos');
		const botonGuardar = $('#btnSaveReqiusitoProd');

		$.ajax({
			data: parametrotxt,
			method: 'post',
			url: baseurl + "oi/homologaciones/chomologaciones/subirArchivo",
			dataType: "JSON",
			async: true,
			contentType: false,
			processData: false,
			beforeSend: function () {
				botonCancelar.prop('disabled', true);
				objPrincipal.botonCargando(botonGuardar);
			},
		}).done(function(response) {
			folder = response[0];
			$("#txtFileRequisito").val(folder); //ASIGNAMOS EL NOMBRE AL ARCHIVO PARA GUARDAR
			rutaarchivo = response[1];
		}).fail(function() {
			alert('Error, no se cargó el archivo');
		}).always(function() {
			botonCancelar.prop('disabled', false);
			objPrincipal.liberarBoton(botonGuardar);
		});
	}

}
