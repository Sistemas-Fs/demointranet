/*!
 *
 * @versión 1.0.0
 */

const objPeligro = {};

$(function() {

	/**
	 * Lista de peligros
	 */
	objPeligro.lista = function() {
		otblinspeccionprov = $('#tblPeligros').DataTable({
			'responsive': false,
			'bJQueryUI': true,
			'scrollY': '420px',
			'scrollX': true,
			'paging': false,
			'processing': true,
			'bDestroy': true,
			'AutoWidth': true,
			'info': true,
			'filter': true,
			'ordering': false,
			'stateSave': true,
			'ajax': {
				"url": baseurl + "oi/ctrlprov/inspctrolprov/cpeligros/lista",
				"type": "POST",
				"data": function (d) {
					d.cauditoria = $('#cauditoria').val();
					d.fservicio = $('#fservicio').val();
				},
				dataSrc: function (data) {
					return data;
				},
			},
			'columns': [
				// {"orderable": false, data: 'norden', targets: 0},
				{"orderable": false, data: 'dproductopeligro', targets: 0},
				{"orderable": false, data: 'ctipopeligro', targets: 1, "searchable": false},
				{"orderable": false, data: 'dpeligro', targets: 2},
				{"orderable": false, data: 'ddetallepeligro', targets: 3},
				{
					responsivePriority: 1, "orderable": false, "searchable": false,
					render: function (data, type, row) {
						return '<div>' +
							'<button type="button" role="button" class="btn btn-secondary btn-sm btn-elegir-peligro" data-cproducto="' + row.cproductopeligro + '" data-ctipopeligro="' + row.ctipopeligro + '" data-norden="' + row.norden + '" >' +
							'<i class="fa fa-eye" ></i> Detalle' +
							'</button>' +
							'</div>';
					}
				}
			],
			"columnDefs": [
				{"targets": [0], "visible": true},
			],
		});
	};

	objPeligro.refrescarLista = function() {
		$("#tblPeligros").DataTable().ajax.reload();
	};

	objPeligro.elegirProducto = function() {
		const button = $(this);
		const cproducto = button.data('cproducto');
		const ctipopeligro = button.data('ctipopeligro');
		const norden = button.data('norden');
		$.ajax({
			method: 'post',
			url: baseurl + 'oi/ctrlprov/inspctrolprov/cpeligros/buscar',
			dataType: 'json',
			async: true,
			data: {
				cauditoria: $('#cauditoria').val(),
				fservicio: $('#fservicio').val(),
				cproducto: cproducto,
				ctipopeligro: ctipopeligro,
				norden: norden,
			},
			beforeSend: function () {
				objPrincipal.botonCargando(button);
			}
		}).done(function (resp) {
			console.log(resp);
			$('#cproducto_peligro').val(resp.cproductopeligro);
			$('#dproducto_peligro').val(resp.dproductopeligro);
			$('#dproducto_peligro_tipo').val(resp.ctipopeligro);
			$('#dproducto_peligro_peligro').val(resp.dpeligro);
			$('#dproducto_peligro_dtexto').val(resp.ddetallepeligro);
			$('#contenidoDetallePeligro').show();
		}).fail(function (jqxhr) {
			const message = (jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la petición HTTP.';
			sweetalert(message, 'error');
		}).always(function () {
			objPrincipal.liberarBoton(button);
		});
	};

	/**
	 * Guarda el peligro
	 */
	objPeligro.guardar = function() {
		const boton = $('#btnGuardarDetallePeligro');
		$.ajax({
			type: 'ajax',
			method: 'post',
			url: baseurl + 'oi/ctrlprov/inspctrolprov/cpeligros/guardar',
			dataType: 'json',
			async: true,
			data: {
				cauditoria: $('#cauditoria').val(),
				fservicio: $('#fservicio').val(),
				cproducto: $('#cproducto_peligro').val(),
				ctipopeligro: $('#dproducto_peligro_tipo').val(),
				ddetalle: $('#dproducto_peligro_dtexto').val(),
				norden: 1,
			},
			beforeSend: function () {
				objPrincipal.botonCargando(boton);
			}
		}).done(function (resp) {
			sweetalert(resp.message, 'success');
			objPeligro.refrescarLista();
		}).fail(function (jqxhr) {
			const message = (jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la petición HTTP.';
			sweetalert(message, 'error');
		}).always(function () {
			objPrincipal.liberarBoton(boton);
		});
	};

	objPeligro.eliminar = function() {
		const boton = $('#btnEliminarDetallePeligro');
		Swal.fire({
			type: 'warning',
			title: 'Eliminar peligro',
			text: '¿Estas seguro(a) de eliminar?',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Si',
			cancelButtonText: 'Cancelar',
		}).then((result) => {
			if (result.value) {
				$.ajax({
					type: 'ajax',
					method: 'post',
					url: baseurl + 'oi/ctrlprov/inspctrolprov/cpeligros/eliminar',
					dataType: 'json',
					async: true,
					data: {
						cauditoria: $('#cauditoria').val(),
						fservicio: $('#fservicio').val(),
						cproducto: $('#cproducto_peligro').val(),
						ctipopeligro: $('#dproducto_peligro_tipo').val(),
						norden: 1,
					},
					beforeSend: function () {
						objPrincipal.botonCargando(boton);
					}
				}).done(function (resp) {
					sweetalert(resp.message, 'success');
					objPeligro.refrescarLista();
				}).fail(function (jqxhr) {
					const message = (jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la petición HTTP.';
					sweetalert(message, 'error');
				}).always(function () {
					objPrincipal.liberarBoton(boton);
				});
			}
		});
	};

	objPeligro.listaPeligros = function() {
		$('#tblListaPeligros').DataTable({
			'responsive': false,
			'bJQueryUI': true,
			'scrollY': '420px',
			'scrollX': true,
			'paging': false,
			'processing': true,
			'bDestroy': true,
			'AutoWidth': true,
			'info': true,
			'filter': true,
			'ordering': false,
			'stateSave': true,
			'ajax': {
				"url": baseurl + "oi/ctrlprov/inspctrolprov/cpeligros/lista_peligros",
				"type": "POST",
				"data": function (d) {
					d.cauditoria = $('#cauditoria').val();
				},
				dataSrc: function (data) {
					return data;
				},
			},
			'columns': [
				// {"orderable": false, data: 'norden', targets: 0},
				{"orderable": false, data: 'cproductopeligro', targets: 0},
				{"orderable": false, data: 'dproductopeligro', targets: 1},
				{
					responsivePriority: 1, "orderable": false,
					render: function (data, type, row) {
						return '<div>' +
							'<button type="button" role="button" class="btn btn-secondary btn-sm btn-guardar-peligro" data-cproducto="' + row.cproductopeligro + '" >' +
							'<i class="fa fa-save" ></i> Elegir' +
							'</button>' +
							'</div>';
					}
				}
			],
			"columnDefs": [
				{"targets": [0], "visible": true},
			],
		});
	};

	objPeligro.guardarPeligro = function() {
		const boton = $(this);
		let cproducto = boton.data('cproducto');
		$.ajax({
			method: 'post',
			url: baseurl + 'oi/ctrlprov/inspctrolprov/cpeligros/guardar_peligro',
			dataType: 'json',
			data: {
				cauditoria: $('#cauditoria').val(),
				fservicio: $('#fservicio').val(),
				cproducto: cproducto,
			},
			beforeSend: function () {
				objPrincipal.botonCargando(boton);
			}
		}).done(function (resp) {
			sweetalert(resp.message, 'success');
			objPeligro.refrescarLista();
			$('#modalPeligros').modal('hide');
		}).fail(function (jqxhr) {
			const message = (jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la petición HTTP.';
			sweetalert(message, 'error');
		}).always(function () {
			objPrincipal.liberarBoton(boton);
		});
	};

});

$(document).ready(function() {

	objPeligro.lista();

	$(document).on('click', '.btn-elegir-peligro', objPeligro.elegirProducto);
	$(document).on('click', '.btn-guardar-peligro', objPeligro.guardarPeligro);

	$('#btnGuardarDetallePeligro').click(objPeligro.guardar);

	$('#btnEliminarDetallePeligro').click(objPeligro.eliminar);

	$('#modalPeligros').on('show.bs.modal', function (event) {
		objPeligro.listaPeligros();
	})

});
