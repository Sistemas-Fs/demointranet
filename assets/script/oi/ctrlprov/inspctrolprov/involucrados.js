/*!
 *
 * @version 1.0.0
 */

const objInvolucrado = {};

$(function () {

	objInvolucrado.lista = function () {
		$.ajax({
			url: baseurl + 'oi/ctrlprov/inspctrolprov/cinvolucrados/lista_reuniones',
			method: 'POST',
			data: {
				cauditoria: $('#cauditoria').val(),
				fservicio: $('#fservicio').val(),
			},
			dataType: 'json',
		}).done(function (items) {
			const table = $('#tblReuniones tbody');
			let rows = '';
			if (items) {
				items.forEach(function (item, pos) {
					rows += '<tr>';
					rows += '<td class="text-center" >' + (pos + 1) + '</td>';
					rows += '<td class="text-left" >';
					rows += '<button type="button" role="button" class="btn btn-light btn-block text-left btn-cargar-reunion" data-zctiporeunion="' + item.ZCTIPOREUNION + '" >' + item.dregistro + '</button>';
					rows += '</td>';
					rows += '<td class="text-center" >';
					rows += '<button type="button" role="button" class="btn btn-secondary text-center btn-eliminar-reunion" data-zctiporeunion="' + item.ZCTIPOREUNION + '" ><i class="fa fa-trash" ></i> Eliminar</button>';
					rows += '</td>';
					rows += '</tr>';
				});
			}
			table.html(rows);
		}).fail(function (resp) {
			const message = (jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la petición HTTP.';
			sweetalert(message, 'error');
		});
	};

	objInvolucrado.agregarLinea = function () {
		$.ajax({
			url: baseurl + 'oi/ctrlprov/inspctrolprov/cinvolucrados/lista_tipo',
			method: 'POST',
			data: {
				cauditoria: $('#cauditoria').val(),
				fservicio: $('#fservicio').val(),
			},
			dataType: 'json',
		}).done(function (items) {
			const table = $('#tblReuniones tbody');
			let pos = table.find('tr').length + 1;
			let rows = '<tr data-position="' + pos + '" >';
			rows += '<td class="text-center" >' + pos + '</td>';
			rows += '<td class="text-left" >';
			rows += '<select class="custom-select" style="width: 100% !important;" id="zctiporeunion[' + pos + ']" >';
			if (items) {
				items.forEach(function(item) {
					rows += '<option value="' + item.CTIPO + '" >' + item.DREGISTRO + '</option>';
				});
			}
			rows += '</select>';
			rows += '</td>';
			rows += '<td class="text-center" >';
			rows += '<button type="button" role="button" class="btn btn-primary text-center btn-guardar-reunion" data-position="' + pos + '" ><i class="fa fa-save" ></i> Guardar</button>';
			rows += '</td>';
			rows += '</tr>';
			table.append(rows);
		}).fail(function (jqxhr) {
			const message = (jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la petición HTTP.';
			sweetalert(message, 'error');
		});
	};

	objInvolucrado.guardarReunion = function() {
		const boton = $(this);
		const position = boton.data('position');
		const zctiporeunion = document.getElementById('zctiporeunion[' + position + ']').value;
		$.ajax({
			url: baseurl + 'oi/ctrlprov/inspctrolprov/cinvolucrados/guardar_reunion',
			method: 'POST',
			data: {
				cauditoria: $('#cauditoria').val(),
				fservicio: $('#fservicio').val(),
				zctiporeunion: zctiporeunion,
			},
			dataType: 'json',
			beforeSend: function() {
				objPrincipal.botonCargando(boton);
			}
		}).done(function (resp) {
			sweetalert(resp.message, 'success');
			objInvolucrado.lista();
			objInvolucrado._cargarReunion(null, zctiporeunion);
		}).fail(function(jqxhr) {
			const message = (jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la petición HTTP.';
			sweetalert(message, 'error');
			objPrincipal.liberarBoton(boton);
		});
	};

	objInvolucrado.eliminarReunion = function() {
		const boton = $(this);
		const zctiporeunion = boton.data('zctiporeunion');
		Swal.fire({
			type: 'warning',
			title: 'Eliminar la reunion',
			text: '¿Estas seguro(a) de reunion?',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Si',
			cancelButtonText: 'Cancelar',
		}).then((result) => {
			if (result.value) {
				$.ajax({
					url: baseurl + 'oi/ctrlprov/inspctrolprov/cinvolucrados/eliminar_reunion',
					method: 'POST',
					data: {
						cauditoria: $('#cauditoria').val(),
						fservicio: $('#fservicio').val(),
						zctiporeunion: zctiporeunion,
					},
					dataType: 'json',
					beforeSend: function () {
						objPrincipal.botonCargando(boton);
					}
				}).done(function (resp) {
					sweetalert(resp.message, 'success');
					objInvolucrado.lista();
					objInvolucrado._cargarReunion(null, zctiporeunion);
				}).fail(function (jqxhr) {
					const message = (jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la petición HTTP.';
					sweetalert(message, 'error');
					objPrincipal.liberarBoton(boton);
				});
			}
		});
	};

	objInvolucrado.cargarReunion = function() {
		const boton = $(this);
		const zctiporeunion = boton.data('zctiporeunion');
		$('#asistente_zctiporeunion').val(zctiporeunion);
		objInvolucrado._cargarReunion(boton, zctiporeunion);
	};

	objInvolucrado._cargarReunion = function(boton, zctiporeunion) {
		$.ajax({
			url: baseurl + 'oi/ctrlprov/inspctrolprov/cinvolucrados/lista',
			method: 'POST',
			data: {
				cauditoria: $('#cauditoria').val(),
				fservicio: $('#fservicio').val(),
				zctiporeunion: zctiporeunion,
			},
			dataType: 'json',
			beforeSend: function () {
				if (boton) {
					objPrincipal.botonCargando(boton);
				}
			}
		}).done(function (resp) {
			console.log(resp);
			// Lista de reunion
			const tblReuniones = $('#tblListaReuniones tbody');
			let rows = '';
			if (resp.resultado) {
				resp.resultado.forEach(function(item) {
					rows += '<tr>';
					rows += '<td class="text-left bg-light" >' + item.dregistro + '</td>';
					rows += '</tr>';
					if (item.asistentes) {
						item.asistentes.forEach(function(asistente) {
							rows += '<tr>';
							rows += '<td class="text-left" >';
							rows += '<span class="ml-4" >' + asistente.DNOMBREASISTENTE + '</span>';
							rows += '</td>';
							rows += '</tr>';
						});
					}
				});
			}
			tblReuniones.html(rows);
			// Lista de aistentes
			const tblAsistentes = $('#tblAsistentes tbody');
			rows = '';
			if (resp.asistente) {
				resp.asistente.forEach(function(item, pos) {
					rows += '<tr>';
					rows += '<td class="text-center" >' + item.NASISTENTE + '</td>';
					rows += '<td class="text-left" >' + item.DNOMBREASISTENTE + '</td>';
					rows += '<td class="text-left" >' + item.DCARGOASISTENTE + '</td>';
					rows += '<td class="text-center" >';
					rows += '<button type="button" role="button" class="btn btn-secondary text-center btn-eliminar-asistente" data-zctiporeunion="' + item.ZCTIPOREUNION + '" data-nasistente="' + item.NASISTENTE + '" ><i class="fa fa-trash" ></i> Eliminar</button>';
					rows += '</td>';
					rows += '</tr>';
				});
			}
			tblAsistentes.html(rows);
		}).fail(function (jqxhr) {
			const message = (jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la petición HTTP.';
			sweetalert(message, 'error');
		}).always(function() {
			if (boton) {
				objPrincipal.liberarBoton(boton);
			}
		});
	};

	objInvolucrado.agregarAsistente = function() {
		const table = $('#tblAsistentes tbody');
		let pos = table.find('tr').length + 1;
		let rows = '<tr data-position="' + pos + '" >';
		rows += '<td class="text-center" >' + pos + '</td>';
		rows += '<td class="text-left" >';
		rows += '<input type="text" class="form-control" style="width: 100% !important;" id="asistente_nombres[' + pos + ']" />';
		rows += '</td>';
		rows += '<td class="text-left" >';
		rows += '<input type="text" class="form-control" style="width: 100% !important;" id="asistente_cargo[' + pos + ']" />';
		rows += '</td>';
		rows += '<td class="text-center" >';
		rows += '<button type="button" role="button" class="btn btn-primary text-center btn-guardar-asistente" data-position="' + pos + '" ><i class="fa fa-save" ></i> Guardar</button>';
		rows += '</td>';
		rows += '</tr>';
		table.append(rows);
	};

	objInvolucrado.buscarAsistente = function() {
		const table = $('#tblAsistentes tbody');
		let pos = table.find('tr').length + 1;
		let rows = '<tr data-position="' + pos + '" >';
		rows += '<td class="text-center" >' + pos + '</td>';
		rows += '<td class="text-left" colspan="2" >';
		rows += '<select class="custom-select" id="asistente_buscar[' + pos + ']" ></select>';
		rows += '</td>';
		rows += '<td class="text-center" >';
		rows += '<button type="button" role="button" class="btn btn-primary text-center btn-guardar-asistente" data-position="' + pos + '" ><i class="fa fa-save" ></i> Guardar</button>';
		rows += '<input type="hidden" class="d-none" style="" id="asistente_nombres[' + pos + ']" value="" />';
		rows += '<input type="hidden" class="d-none" style="" id="asistente_cargo[' + pos + ']" value="" />';
		rows += '</td>';
		rows += '</tr>';
		table.append(rows);
		// select2
		const elContacto = $(document.getElementById('asistente_buscar[' + pos + ']'));
		elContacto.select2({
			ajax: {
				url: baseurl + "oi/ctrlprov/cregctrolprov/getcbocontacplanins",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						search: params.term,
						ccliente: $('#mhdnccliente').val(),
						cproveedor: $('#mhdncproveedor').val(),
						cmaquilador: $('#mhdncmaquilador').val(),
					};
				},
				processResults: function (data) {
					return {results: data};
				},
				cache: true
			},
			escapeMarkup: function (markup) {
				return markup;
			}, // let our custom formatter work
			minimumInputLength: 0,
			// Aqi se muestran todos los resultados de la busqueda
			templateResult: function (state) {
				const dcargocontacto = (state.dcargocontacto) ? state.dcargocontacto : '';
				const dmail = (state.dmail) ? state.dmail : '';
				const dtelefono = (state.dtelefono) ? state.dtelefono : '';
				let result = '<table class="" >';
				result += '<tr>';
				result += '<td style="width: 200px; padding-right: 5px" >' + state.cliente + '</td>';
				result += '<td style="width: 200px; padding-right: 5px" >' + state.text + '</td>';
				result += '<td style="width: 150px; padding-right: 5px" >' + dcargocontacto + '</td>';
				// result += '<td style="width: 150px; padding-right: 5px" >' + dmail + '</td>';
				result += '<td style="width: 100px" >' + dtelefono + '</td>';
				result += '<tr/>';
				result += '<table>';
				return result;
			},
			templateSelection: function (state) {
				if (state.id !== "") {
					const dcargocontacto = (state.dcargocontacto) ? state.dcargocontacto : '';
					const dmail = (state.dmail) ? state.dmail : '';
					const dtelefono = (state.dtelefono) ? state.dtelefono : '';
					let result = '<table class="table-sm" >';
					result += '<tr>';
					result += '<td style="width: 220px; padding-right: 5px; border: none" >' + state.text + '</td>';
					result += '<td style="width: 200px; padding-right: 5px; border: none" >' + dcargocontacto + '</td>';
					// result += '<td style="width: 150px; padding-right: 5px; border: none" >' + dmail + '</td>';
					result += '<td style="width: 200px; border: none" >' + dtelefono + '</td>';
					result += '<tr/>';
					result += '<table>';
					if (typeof __selectAsistenteContacto == "function") {
						__selectAsistenteContacto(pos, state);
					}
					return result;
				} else {
					return 'Elegir contacto';
				}
			},
			placeholder: "Elegir contacto",
			allowClear: true,
			width: '100%',
		});
	};

	objInvolucrado.guardarAsistente = function() {
		const boton = $(this);
		const position = boton.data('position');
		const zctiporeunion = $('#asistente_zctiporeunion').val();
		const nombres = document.getElementById('asistente_nombres[' + position + ']').value;
		const cargo = document.getElementById('asistente_cargo[' + position + ']').value;
		$.ajax({
			url: baseurl + 'oi/ctrlprov/inspctrolprov/cinvolucrados/guardar_asistente',
			method: 'POST',
			data: {
				cauditoria: $('#cauditoria').val(),
				fservicio: $('#fservicio').val(),
				zctiporeunion: zctiporeunion,
				nombres: nombres,
				cargo: cargo,
			},
			dataType: 'json',
			beforeSend: function() {
				objPrincipal.botonCargando(boton);
			}
		}).done(function (resp) {
			sweetalert(resp.message, 'success');
			objInvolucrado.lista();
			objInvolucrado._cargarReunion(null, zctiporeunion);
		}).fail(function(jqxhr) {
			const message = (jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la petición HTTP.';
			sweetalert(message, 'error');
			objPrincipal.liberarBoton(boton);
		});
	};

	objInvolucrado.eliminarAsistente = function() {
		const boton = $(this);
		const zctiporeunion = $('#asistente_zctiporeunion').val();
		const nasistente = boton.data('nasistente');
		Swal.fire({
			type: 'warning',
			title: 'Eliminar la asistente',
			text: '¿Estas seguro(a) de asistente?',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Si',
			cancelButtonText: 'Cancelar',
		}).then((result) => {
			if (result.value) {
				$.ajax({
					url: baseurl + 'oi/ctrlprov/inspctrolprov/cinvolucrados/eliminar_asistente',
					method: 'POST',
					data: {
						cauditoria: $('#cauditoria').val(),
						fservicio: $('#fservicio').val(),
						zctiporeunion: zctiporeunion,
						nasistente: nasistente,
					},
					dataType: 'json',
					beforeSend: function () {
						objPrincipal.botonCargando(boton);
					}
				}).done(function (resp) {
					sweetalert(resp.message, 'success');
					objInvolucrado._cargarReunion(null, zctiporeunion);
				}).fail(function (jqxhr) {
					const message = (jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la petición HTTP.';
					sweetalert(message, 'error');
					objPrincipal.liberarBoton(boton);
				});
			}
		});
	};

});

$(document).ready(function () {

	objInvolucrado.lista();

	$('#btnAgregarReunion').click(objInvolucrado.agregarLinea);
	$(document).on('click', '.btn-guardar-reunion', objInvolucrado.guardarReunion);

	$(document).on('click', '.btn-eliminar-reunion', objInvolucrado.eliminarReunion);

	$(document).on('click', '.btn-cargar-reunion', objInvolucrado.cargarReunion);

	$('#btnAgregarAsistente').click(objInvolucrado.agregarAsistente);

	$('#btnBuscarAsistente').click(objInvolucrado.buscarAsistente);

	$(document).on('click', '.btn-guardar-asistente', objInvolucrado.guardarAsistente);

	$(document).on('click', '.btn-eliminar-asistente', objInvolucrado.eliminarAsistente);

});

function __selectAsistenteContacto(pos, data)
{
	console.log(data);
	document.getElementById('asistente_nombres[' + pos + ']').value = data.text;
	document.getElementById('asistente_cargo[' + pos + ']').value = data.dcargocontacto;
}
