/*!
 *
 * @version 1.0.0
 */

const objACC = {};

$(function() {

	/**
	 * Acciones
	 * @type {*[]}
	 */
	objACC.items = [];

	/**
	 * Lista de acciones correctivas
	 * @param varcauditoria
	 * @param varfservicio
	 */
	objACC.lista = function (varcauditoria, varfservicio) {
		otblinspeccionprov = $('#tblACC').DataTable({
			'responsive': false,
			'bJQueryUI': true,
			'scrollY': '680px',
			'scrollX': true,
			'paging': false,
			'processing': true,
			'bDestroy': true,
			'AutoWidth': true,
			'info': true,
			'filter': true,
			'ordering': false,
			'stateSave': true,
			'ajax': {
				"url": baseurl + "oi/ctrlprov/cinspctrolprovacc/lista/",
				"type": "POST",
				"data": function (d) {
					d.cauditoriainspeccion = varcauditoria;
					d.fservicio = varfservicio;
				},
				dataSrc: function (data) {
					const totalRegistros = data.length;
					var totalLlenado = 0;
					data.forEach(function(row) {
						objACC.items.push({ id: row.crequisitochecklist, active: false, data: row });
						if (row.saceptaraccioncorrectiva === 'S') {
							++totalLlenado;
						}
					});
					$('#acc_total').val("Total de AA. CC. generados: " + totalRegistros + " ==> " + totalLlenado + " de " + totalRegistros + " ingresadas.");
					return data;
				},
			},
			'columns': [
				{"orderable": false, data: 'dnumerador', targets: 0},
				// {"orderable": false, data: 'drequisito', targets: 1},
				{
					"orderable": false,
					render: function (data, type, row) {
						return String(row.drequisito).replace(/(\r\n|\r|\n)/g, '<br />');
					}
				},
				{"orderable": false, data: 'sexcluyente', targets: 2},
				{"orderable": false, data: 'tipohallazgo', targets: 3},
				{"orderable": false, data: 'dhallazgo', targets: 5},
				{"orderable": false, data: 'daccioncorrectiva', targets: 6},
				// {"orderable": false, data: 'fcorrectiva', targets: 7},
				{
					"orderable": false,
					render: function (data, type, row) {
						return (row.fcorrectiva)
							? moment(row.fcorrectiva, 'YYY-MM-DD').format('DD/MM/YYYY')
							: '';
					}
				},
				{"orderable": false, data: 'dresponsablecliente', targets: 8},
				{"orderable": false, data: 'saceptaraccioncorrectiva', targets: 9},
				{"orderable": false, data: 'dobservacion', targets: 10},
				{
					responsivePriority: 1, "orderable": false,
					render: function (data, type, row) {
						if (parseInt($('#inspeccion_abierto').val()) === 1) {
							return '<div>' +
								// '<button type="button" role="button" class="btn btn-link btn-sm btn-ver-acc" data-cauditoria="' + row.cauditoriainspeccion + '" data-fservicio="' + row.fservicio + '" data-cchecklist="' + row.cchecklist + '" data-crequisitochecklist="' + row.crequisitochecklist + '" data-tipohallazgo="' + row.tipohallazgo + '" >' +
								'<button type="button" role="button" class="btn btn-link btn-sm btn-editar-acc" onClick="objACC.abrirActual(\'' + row.crequisitochecklist + '\');" >' +
								'<i class="fa fa-edit fa-3x" data-original-title="Registrar" data-toggle="tooltip"></i>' +
								'</button>' +
								'</div>';
						} else {
							return '';
						}
					}
				}
			],
			"columnDefs": [
				{"targets": [0], "visible": true},
			],
		});
	};

	objACC.refrescarLista = function() {
		$("#tblACC").DataTable().ajax.reload();
	};

	/**
	 * Abrir la Accion correctiva
	 */
	objACC.editar = function() {
		const boton = $(this);
		const cauditoria = boton.data('cauditoria');
		const fservicio = boton.data('fservicio');
		const cchecklist = boton.data('cchecklist');
		const crequisitochecklist = boton.data('crequisitochecklist');
		const tipohallazgo = boton.data('tipohallazgo');
		objACC._editar(boton, cauditoria, fservicio, cchecklist, crequisitochecklist, tipohallazgo)
	};

	/**
	 * @param boton
	 * @param cauditoria
	 * @param fservicio
	 * @param cchecklist
	 * @param crequisitochecklist
	 * @param tipohallazgo
	 * @private
	 */
	objACC._editar = function(boton, cauditoria, fservicio, cchecklist, crequisitochecklist, tipohallazgo) {
		$.ajax({
			method: 'POST',
			url: baseurl + "oi/ctrlprov/inspctrolprov/caccion_correctiva/buscar",
			dataType: "JSON",
			async: true,
			data: {
				cauditoria: cauditoria,
				fservicio: fservicio,
				cchecklist: cchecklist,
				crequisitochecklist: crequisitochecklist,
			},
			beforeSend: function() {
				if (boton) {
					objPrincipal.botonCargando(boton);
				}
			}
		}).done(function(resp) {
			objACC.imprimirDatos(cchecklist, crequisitochecklist, resp.drequisito, resp.dhallazgo, resp.daccioncorrectiva, resp.saceptaraccioncorrectiva, tipohallazgo, resp.dobservacion, resp.fcorrectiva, resp.dresponsablecliente);
			objACC.abrirModal();
		}).fail(function() {
			objPrincipal.notify('error', 'Error al cargar la acción correctiva.')
		}).always(function() {
			if (boton) {
				objPrincipal.liberarBoton(boton);
			}
		});
	};

	/**
	 * Habre el modal de ACC
	 */
	objACC.abrirModal = function() {
		$('#ModalAccionCorrectiva').modal('show');
	};

	/**
	 * Habre el modal de ACC
	 */
	objACC.cerrarModal = function() {
		$('#ModalAccionCorrectiva').modal('hide');
	};

	/**
	 * Impresión de datos del ACC
	 * @param cchecklistId
	 * @param requisitoCheckListId
	 * @param requisito
	 * @param hallazgo
	 * @param acc
	 * @param aceptaAcc
	 * @param tipoAllazgo
	 * @param dobservacion
	 * @param fecha
	 * @param responsable
	 */
	objACC.imprimirDatos = function(cchecklistId, requisitoCheckListId, requisito, hallazgo, acc, aceptaAcc, tipoAllazgo, dobservacion, fecha, responsable) {
		$('#acc_modal_cchecklist_id').val(cchecklistId);
		$('#acc_modal_requisito_cchecklist_id').val(requisitoCheckListId);
		$('#acc_modal_requisito').val(requisito);
		$('#acc_modal_hallazgo').val(hallazgo);
		$('#acc_modal_acc').val(acc);
		$('#acc_modal_observaciones').val(dobservacion);
		$('#acc_modal_acepta_acc').prop('checked', aceptaAcc.toUpperCase() === "S");
		$('#acc_modal_tipo_hallazgo').val(tipoAllazgo);
		$('#acc_modal_responsable').val(responsable);
		if (fecha) {
			fecha = moment(fecha, 'YYYY-MM-DD').format('DD/MM/YYYY');
			$('#acc_modal_fecha').val(fecha);
		}
		$('#ModalAccionCorrectiva').modal('show');
	};

	/**
	 * Actualiza el checklist
	 */
	objACC.guardar = function(tipoBoton) {
		const botonContinuar = $('#btnGuardarACC');
		const botonCerrar = $('#btnCerrarACC');
		const botonAnterior = $('#btnAnteriorACC');
		var datos = $('#frmAccionCorrectiva').serialize()
			+ '&cauditoria=' + $('#cauditoria').val()
			+ '&fservicio=' + $('#fservicio').val();
		$.ajax({
			url: baseurl + 'oi/ctrlprov/inspctrolprov/caccion_correctiva/guardar',
			method: 'POST',
			dataType: "JSON",
			async: true,
			data: datos,
			beforeSend: function() {
				botonAnterior.prop('disabled', true);
				objPrincipal.botonCargando(botonContinuar);
				objPrincipal.botonCargando(botonCerrar);
			}
		}).done(function(resp) {
			objPrincipal.notify('success', resp.message);
			if (tipoBoton == 2) {
				objACC.cerrarModal();
			} else {
				objACC.siguiente();
			}
		}).fail(function() {
			objPrincipal.notify('error', 'Error HTTP al actualizar la acción correctiva.');
		}).always(function() {
			botonAnterior.prop('disabled', false);
			objPrincipal.liberarBoton(botonContinuar);
			objPrincipal.liberarBoton(botonCerrar);
		});
	};

	objACC.activarBotonArchivo = function() {
		$('.btn-existe-archivo').show();
	};

	objACC.desActivarBotonArchivo = function() {
		$('.btn-existe-archivo').hide();
	};

	/**
	 * Subir archivo
	 */
	objACC.subirArchivo = function() {
		const form = $('form#frmFileACC');
		const datos = new FormData(form[0]);
		datos.append('cauditoria', $('#cauditoria').val());
		datos.append('fservicio', $('#fservicio').val());
		const botonGuardar = $('#btnCargarArchivoAC');
		$.ajax({
			url: BASE_URL + 'oi/ctrlprov/inspctrolprov/caccion_correctiva/guardar_archivo',
			method: 'POST',
			data: datos,
			dataType: 'json',
			cache: false,
			contentType: false,
			processData: false,
			beforeSend: function () {
				objPrincipal.botonCargando(botonGuardar);
			}
		}).done(function (response) {
			sweetalert(response.message, 'success');
			$('#acc_archivo').val(response.data.filename);
			objACC.activarBotonArchivo();
			objACC.refrescarLista();
		}).fail(function (jqxhr) {
			sweetalert('Error en el proceso de ejecución', 'error');
		}).always(function () {
			objPrincipal.liberarBoton(botonGuardar);
		});
	};

	objACC.descargarArchivo = function() {
		var content = BASE_URL + 'oi/ctrlprov/inspctrolprov/cinspeccion/descargar_acc?cauditoria=' + $('#cauditoria').val() + '&fservicio=' + $('#fservicio').val();
		var download = window.open(content, '_blank');
		if (download == null || typeof download == "undefined") {
			objApp.notify("Ventana emergente", "Habilite la ventana emergente de su navegador. Para continuar la descarga!", "warning");
		} else {
			download.focus();
		}
	};

	objACC.eliminarArchivo = function() {
		const boton = $('#btnEliminarArchivoAC');
		$.ajax({
			url: BASE_URL + 'oi/ctrlprov/inspctrolprov/caccion_correctiva/eliminar_archivo',
			method: 'POST',
			data: {
				cauditoria: $('#cauditoria').val(),
				fservicio: $('#fservicio').val(),
			},
			dataType: 'json',
			beforeSend: function () {
				objPrincipal.botonCargando(boton);
			}
		}).done(function (response) {
			sweetalert(response.message, 'success');
			$('#acc_archivo').val('');
			objACC.desActivarBotonArchivo();
			objACC.refrescarLista();
		}).fail(function (jqxhr) {
			sweetalert('Error en el proceso de ejecución', 'error');
		}).always(function () {
			objPrincipal.liberarBoton(boton);
		});
	};

	/**
	 * @param crequisitochecklist
	 */
	objACC.abrirActual = function(crequisitochecklist) {
		const index = objACC.items.findIndex(function(row) {
			return (row.id === crequisitochecklist);
		});
		objACC.items[index].active = true;
		const row = objACC.items[index].data;
		objACC._abrirActual(row);
	};

	/**
	 * @param row
	 * @private
	 */
	objACC._abrirActual = function(row) {
		objACC._editar(
			null,
			row.cauditoriainspeccion,
			row.fservicio,
			row.cchecklist,
			row.crequisitochecklist,
			row.tipohallazgo,
		);
	};

	/**
	 * Item siguiente
	 */
	objACC.siguiente = function() {
		const index = objACC.obtenerItemActual();
		const nextIndex = index + 1;
		if (typeof objACC.items[nextIndex] === 'undefined') {
			objPrincipal.notify('info', 'No existen mas Acciones correctivas');
		} else {
			// Se pasa al siguiente item
			objACC.limpiarItems();
			objACC.items[nextIndex].active = true;
			objACC._abrirActual(objACC.items[nextIndex].data);
		}
	};

	/**
	 * Limpia todos los items active
	 */
	objACC.limpiarItems = function() {
		objACC.items.forEach(function(row) {
			row.active = false;
		});
	};

	/**
	 * @returns {*}
	 */
	objACC.obtenerItemActual = function() {
		return objACC.items.findIndex(function(row) {
			return (row.active);
		});
	};

	/**
	 * Busca inspeccion anterior
	 */
	objACC.anterior = function() {
		const index = objACC.obtenerItemActual();
		if (typeof objACC.items[index - 1] === 'undefined') {
			objPrincipal.notify('info', 'No existen más acciones correctivas');
		} else {
			const itemAnterior = objACC.items[index - 1];
			objACC.limpiarItems();
			objACC.items[index - 1].active = true;
			objACC._abrirActual(itemAnterior.data);
		}
	};

	objACC.descargarPlantilla = function() {
		var content = BASE_URL + 'oi/ctrlprov/inspctrolprov/caccion_correctiva/descargar_plantilla?cauditoria=' + $('#cauditoria').val() + '&fservicio=' + $('#fservicio').val();
		var download = window.open(content, '_blank');
		if (download == null || typeof download == "undefined") {
			objApp.notify("Ventana emergente", "Habilite la ventana emergente de su navegador. Para continuar la descarga!", "warning");
		} else {
			download.focus();
		}
	};

});

$(document).ready(function() {

	objACC.lista($('#cauditoria').val(), $('#fservicio').val());

	// $(document).on('click', '.btn-ver-acc', objACC.editar);

	$('#btnGuardarACC').click(function() {
		objACC.guardar(1);
	});

	$('#btnCerrarACC').click(function() {
		objACC.guardar(2);
	});

	$('#btnAnteriorACC').click(function() {
		objACC.anterior();
	});

	$("#btnAnteriorChecklist").click(objACC.anterior);

	objPrincipal.addPluginCalendar($('#acc_modal_fecha'));

	$('#btnCargarArchivoAC').click(function() {
		const boton = $(this);
		Swal.fire({
			type: 'warning',
			title: 'El proceso de subir el archivo deja sin efecto lo registro manual de la AA.CC.',
			text: '¿Seguro(a) de subir el archivo?',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Si',
			cancelButtonText: 'Cancelar',
		}).then((result) => {
			if (result.value) {
				$('#file_acc').click();
			}
		});
	});

	$('#file_acc').change(objACC.subirArchivo);

	$('#btnDescargarArchivoAC').click(objACC.descargarArchivo);

	$('#btnEliminarArchivoAC').click(objACC.eliminarArchivo);

	$('#ModalAccionCorrectiva').on('hidden.bs.modal', function () {
		objACC.limpiarItems();
		objACC.refrescarLista();
	});

	$('#btnDescargarPlantillaAC').click(objACC.descargarPlantilla);

});
