/*!
 *
 * @version 1.0.0
 */

const objResumenTema = {};

$(function() {

	objResumenTema.modal = $('#resumenTemaModal');

	/**
	 * Almacena la lista de temas
	 * @type {*[]}
	 */
	objResumenTema.temas = [];

	objResumenTema.abrir = function() {
		objResumenTema.modal.modal('show');
	};

	objResumenTema.cerrar = function() {
		objResumenTema.modal.modal('hide');
	};

	/**
	 * Ejecuta la busqueda de recursos para los temas
	 */
	objResumenTema.obtenerRecursos = function() {
		$.ajax({
			url: BASE_URL + 'oi/ctrlprov/inspctrolprov/cresumen_tema/recursos',
			method: 'POST',
			data: {
				cauditoria: $('#cauditoria').val(),
				fservicio: $('#fservicio').val()
			},
			dataType: 'json',
			beforeSend: function () {}
		}).done(function (res) {
			objResumenTema.temas = res.temas;
			objResumenTema.imprimirRecursos();
		}).fail(function (jqxhr) {
			//
		});
	};

	objResumenTema.imprimirRecursos = function() {
		const opcion = $('#res_tema_value');
		const tabla = $('#tblResumenTema tbody');
		let rows = '';
		let opciones = '';
		if (objResumenTema.temas) {
			objResumenTema.temas.forEach(function(item) {
				opciones += '<option value="' + item.CTIPO + '" >' + item.DREGISTRO + '</option>';
				rows += '<tr>';
				rows += '<td class="text-left position-relative" >';
				rows += '<button type="button" role="button" class="btn btn-link btn-resumen-tema text-left w-100" data-id="' + item.CTIPO + '" data-descripcion="' + item.DREGISTRO + '" >';
				rows += item.DREGISTRO;
				rows += '</button>';
				if (item.ZCINFOADICIONAL) {
					rows += '<button class="btn btn-secondary btn-sm position-absolute btn-resumen-tema-eliminar" data-id="' + item.CTIPO + '" style="right: 5px" ><i class="fa fa-trash" ></i></button>';
				}
				rows += '</td>';
				rows += '</tr>';
			});
		}
		opcion.html(opciones);
		tabla.html(rows);
		$('#res_tema_descripcion').val('');
	};

	objResumenTema.guardar = function() {
		const frm = $('#frmResumenTema');
		const botonGuardar = $('#btnResumenTemaGuardar');
		const botonCerrar = $('#btnResumenTemaCerrar');
		$.ajax({
			url: BASE_URL + 'oi/ctrlprov/inspctrolprov/cresumen_tema/guardar',
			method: 'POST',
			data: frm.serialize() + '&cauditoria=' + $('#cauditoria').val() + '&fservicio=' + $('#fservicio').val(),
			dataType: 'json',
			beforeSend: function () {
				objPrincipal.botonCargando(botonGuardar);
				botonCerrar.prop('disabled', true);
			}
		}).done(function (res) {
			objResumenTema.obtenerRecursos();
			objResumenTema.cerrar();
			objPrincipal.notify('success', res.message);
		}).fail(function (jqxhr) {
			const message = (jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la petición HTTP.';
			sweetalert(message, 'error');
		}).always(function() {
			objPrincipal.liberarBoton(botonGuardar);
			botonCerrar.prop('disabled', false);
		});
	};

	objResumenTema.cargar = function() {
		const boton = $(this);
		const ctipo = boton.data('id');
		$.ajax({
			url: BASE_URL + 'oi/ctrlprov/inspctrolprov/cresumen_tema/tema',
			method: 'POST',
			data: {
				ctipo: ctipo,
				cauditoria: $('#cauditoria').val(),
				fservicio: $('#fservicio').val()
			},
			dataType: 'json',
			beforeSend: function () {
				objPrincipal.botonCargando(boton);
			}
		}).done(function (res) {
			$('#res_tema_id').val(ctipo);
			$('#res_tema_value').val(ctipo).change();
			let descripcion = '';
			if (res) {
				descripcion = res.DINFOADICIONAL;
			}
			$('#res_tema_descripcion').val(descripcion);
			objResumenTema.abrir();
		}).always(function (jqxhr) {
			objPrincipal.liberarBoton(boton);
		});
	};

	objResumenTema.eliminar = function() {
		Swal.fire({
			type: 'warning',
			title: 'Eliminar tema',
			text: '¿Estas seguro(a) de eliminar?',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Si',
			cancelButtonText: 'Cancelar',
		}).then((result) => {
			if (result.value) {
				const boton = $(this);
				const ctipo = boton.data('id');
				$.ajax({
					url: BASE_URL + 'oi/ctrlprov/inspctrolprov/cresumen_tema/eliminar',
					method: 'POST',
					data: {
						ctipo: ctipo,
						cauditoria: $('#cauditoria').val(),
						fservicio: $('#fservicio').val()
					},
					dataType: 'json',
					beforeSend: function () {
						objPrincipal.botonCargando(boton);
					}
				}).done(function (res) {
					objPrincipal.notify('success', res.message);
					objResumenTema.obtenerRecursos();
				}).fail(function (jqxhr) {
					const message = (jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la petición HTTP.';
					sweetalert(message, 'error');
				}).always(function (jqxhr) {
					objPrincipal.liberarBoton(boton);
				});
			}
		});
	};

});

$(document).ready(function() {

	objResumenTema.obtenerRecursos();

	$('#btnResumenTemaGuardar').click(objResumenTema.guardar);

	$(document).on('click', '.btn-resumen-tema', objResumenTema.cargar);

	$(document).on('click', '.btn-resumen-tema-eliminar', objResumenTema.eliminar);

});
