/*!
 *
 * @version 1.0.0
 */

const objArea = {};
var tblListAreaCliente;

$(function () {

	objArea.mostrarRegArea = function (ccliente, razonsocial, ddireccioncliente) {
		console.log('ccliente', ccliente)
		const boton = $('#btnAccionContenedorLista');
		const icon = boton.find('i');
		if (icon.hasClass('fa-plus')) icon.removeClass('fa-plus');
		icon.addClass('fa-minus');
		boton.click();
		$('#cardListArea').show();
		document.querySelector('#lblClienteArea').innerText = razonsocial;
		document.querySelector('#lblDirclieArea').innerText = (ddireccioncliente !== '') ? ddireccioncliente : '';
		$('#mhdnIdClieArea').val(ccliente);
		$('#hdnIdptClieLinea').val(ccliente);
		$('#mhdnconCClienteArea').val(0);
		$('#modalAddArea').val(0);
		listAreaCliente(ccliente);
		$('#contenedorRegcontacto').hide();
		$('#contenedorRegserv').hide();
		$('#contenedorRegmaq').hide();
		$('#contenedorRegprov').hide();
		$('#contenedorBusqueda').hide();
		$('#contenedorRegestable').hide();
		$('#contenedorRegarea').show();
	};

	objArea.editar = function() {
		var tr = $(this).parents('tr');
		var row = tblListAreaCliente.row(tr);
		var rowData = row.data();
		objArea.mostrarDatos(rowData.CAREACLIENTE, rowData.DAREACLIENTE, rowData.DJERARQUIA, rowData.ESCONCESIONARIO, rowData.CTIPO, rowData.SREGISTRO);
	};

	objArea.mostrarDatos = function(carea, darea, djerarquia, consesionario, tipo, estado) {
		$('#mhdnIdArea').val(carea);
		$('#str_descripcion_area').val(darea);
		$('#str_jerarquia_area').val(djerarquia);
		$('#cboConsecionario').val(consesionario).change();
		$('#cboTipoArea').val(tipo).change();
		$('#cboEstado').val(estado).change();
		$('#modalAddArea').modal('show');
	};

});

$(document).ready(function () {

	$("#btnguardararea").click(function () {
		var data = $("#frmMantArea").serialize();
		console.log('data', data)
		var descripcion = $("#str_descripcion_area").val();
		if (descripcion === "") {
			Vtitle = 'Completar los campos requeridos';
			Vtype = 'error';
			sweetalert(Vtitle, Vtype);
		} else {
			$.ajax({
				type: 'ajax',
				data: data,
				method: 'post',
				url: baseurl + "oi/ctrlprov/cliente/carea/guardar",
				dataType: "JSON",
				async: true,
				success: function (result) {
					Vtitle = 'Se guardo correctamente!';
					Vtype = 'success';
					sweetalert(Vtitle, Vtype);
					$("#modalAddArea").modal('hide');
					tblListAreaCliente.ajax.reload();
					$('#mhdnIdArea').val(0);
					$('#str_descripcion_area').val('');
					$('#str_jerarquia_area').val('');
					$('#cboEstado').val('A').change();
				},
				error: function () {
					Vtitle = 'Error en el proceso!';
					Vtype = 'error';
					sweetalert(Vtitle, Vtype);
				}
			});
		}
	});

	$(document).on('click', '.opcion-editar-area', objArea.editar);

});

listAreaCliente = function (ccliente) {
	tblListAreaCliente = $('#tblListArea').DataTable({
		"processing": true,
		"bDestroy": true,
		"stateSave": true,
		"bJQueryUI": true,
		'bStateSave': true,
		"scrollY": "500px",
		"scrollX": true,
		'AutoWidth': true,
		"paging": false,
		"info": true,
		"filter": true,
		"ordering": false,
		"responsive": false,
		"select": true,
		//'fixedColumns':{
		//  'leftColumns': false,// Fijo primera columna
		//  'rightColumns':1
		//},
		//'lengthMenu'  : [[10, 20, 30, -1], [10, 20, 30, "Todo"]],
		'ajax': {
			"url": baseurl + "oi/ctrlprov/cliente/carea/lista",
			"type": "POST",
			"data": function (d) {
				d.ccliente = ccliente
			},
			dataSrc: ''
		},
		'columns': [
			{data: 'CAREACLIENTE', "class": "col-xxs"},
			{
				"orderable": false, "class": "col-xxs",
				render: function (data, type, row) {
					return '<div class="dropdown" style="text-align: center;">' +
						'<a  data-toggle="dropdown" href="#"><span class="fas fa-bars"></span></a>' +
						'<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">' +
						'<li><a title="Editar" style="cursor:pointer; color:blue;" class="opcion-editar-area" ><span class="fa fa-edit" aria-hidden="true">&nbsp;</span>&nbsp;Editar</a></li>' +
						'</ul>' +
						'</div>'
				}

			},
			{data: 'DAREACLIENTE', "class": "col-xxs"},
			{data: 'DJERARQUIA', "class": "col-xs"},
			{
				"orderable": false, "class": "col-xxs",
				render: function (data, type, row) {
					if (row.SREGISTRO === 'A') {
						return 'ACTIVO';
					} else {
						return 'INACTIVO';
					}
				}
			},
			{data: 'CTIPO', "class": "col-xxs"}
		],

	});
	// Enumeracion
	tblListAreaCliente.on('order.dt search.dt', function () {
		tblListAreaCliente.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
			cell.innerHTML = i + 1;
		});
	}).draw();
};
