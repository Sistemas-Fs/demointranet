/*!
 *
 * @version 1.0.0
 */

const objEstablecimientoLinea = {};
let tblLineaEstable;

$(function () {

	objEstablecimientoLinea.mostrarRegLinea = function (ccliente, razonsocial, ddireccioncliente, COD_ESTABLE, COD_CLIENTE, DRAZONSOCIAL) {
		console.log('ccliente', ccliente)
		const boton = $('#btnAccionContenedorLista');
		const icon = boton.find('i');
		if (icon.hasClass('fa-plus')) icon.removeClass('fa-plus');
		icon.addClass('fa-minus');
		boton.click();
		$('#cardLineaProc').show();
		$('#cardListestable').hide();
		$('#cardRegestable').hide();
		$('#btnEstableNuevo').hide();

		// document.querySelector('#lblClienteContacto').innerText = razonsocial;
		// document.querySelector('#lblDirclieContacto').innerText = (ddireccioncliente !== '') ? ddireccioncliente : '';

		$('#mhdnIdContacto').val(ccliente);
		$('#hdnIdptClieLinea').val(ccliente);

		$('#mhdnconCcliente').val(ccliente);
		$('#mhdnconDcliente').val(razonsocial);
		$('#mhdnconDdireccion').val(ddireccioncliente);

		if (COD_ESTABLE) {
			//listContactoEstable(ccliente,COD_ESTABLE);
			$("#txtestablecimiento").val(COD_ESTABLE);
		} else {
			//listContacto(ccliente);
			$("#txtestablecimiento").val('');
		}
		listLineaEstablec(ccliente, COD_ESTABLE);

		$('#contenedorRegcontacto').hide();
		$('#contenedorRegserv').hide();
		$('#contenedorRegmaq').hide();
		$('#contenedorRegprov').hide();
		$('#contenedorBusqueda').hide();
		$('#contenedorRegarea').hide();
		$('#contenedorRegestable').show();
	};

	objEstablecimientoLinea.editar = function() {
		var tr = $(this).parents('tr');
		var row = tblLineaEstable.row(tr);
		var rowData = row.data();
		objEstablecimientoLinea.mostrarEditarLinea(rowData.CLINEAPROCESOCLIENTE, rowData.CCLIENTE, rowData.CESTABLECIMIENTO, rowData.DLINEACLIENTEE, rowData.SPELIGRO);
	};

	objEstablecimientoLinea.mostrarEditarLinea = function(clineaprocesocliente, ccliente, cestablecimiento, dlineaclientee, speligro) {
		$('#hdnIdLinea').val(clineaprocesocliente);
		$('#hdnIdptClieLinea').val(ccliente);
		$('#txtestablecimiento').val(cestablecimiento);
		$('#txtLineaProc').val(dlineaclientee);
		$('#chxPeligro').prop('checked', (speligro === 'S'));
		$('#modalLineaAdd').modal('show');
	};

});

$(document).ready(function () {

	$("#btnModalAddLinea").click(function () {
		var ccliente = $("#hdnIdptClieLinea").val();
		console.log('ccliente', ccliente)
		var COD_ESTABLE = $("#txtestablecimiento").val();
		console.log('COD_ESTABLE', COD_ESTABLE)
		$('#hdnAccionptclieLinea').val('N');
		$('#hdnIdLinea').val('');
		$('#modalLineaAdd').modal('show');
	});

	$('#frmLineaAdd').submit(function (event) {
		event.preventDefault();
		var request = $.ajax({
			url: $('#frmLineaAdd').attr("action"),
			type: $('#frmLineaAdd').attr("method"),
			data: $('#frmLineaAdd').serialize(),
			error: function () {
				alert('Error, No se puede autenticar por error');
			}
		});
		request.done(function (respuesta) {
			$('#hdnIdLinea').val('');
			sweetalert('Datos Guardados correctamente', 'success');
			$('#cardListestable').hide();
			$('#cardRegestable').hide();
			$('#cardLineaProc').show();
			$('#frmLineaAdd').trigger("reset");
			$('#modalLineaAdd').modal('hide');
			tblLineaEstable.ajax.reload();
		});
	});

	$(document).on('click', '.opcion-editar-linea', objEstablecimientoLinea.editar);

});

listLineaEstablec = function (ccliente, cestablecimiento) {
	tblLineaEstable = $('#tblLineaEstable').DataTable({
		"processing": true,
		"bDestroy": true,
		"stateSave": true,
		"bJQueryUI": true,
		"scrollY": "500px",
		"scrollX": true,
		'AutoWidth': true,
		"paging": false,
		"info": true,
		"filter": true,
		"ordering": false,
		"responsive": false,
		"select": true,
		//'fixedColumns':{
		//  'leftColumns': false,// Fijo primera columna
		//  'rightColumns':1
		//},
		//'lengthMenu'  : [[10, 20, 30, -1], [10, 20, 30, "Todo"]],
		'ajax': {
			"url": baseurl + "at/ctrlprov/cliente/cestablecimiento_linea/lista",
			"type": "POST",
			"data": function (d) {
				d.ccliente = ccliente;
				d.cestablecimiento = cestablecimiento;
			},
			dataSrc: ''
		},
		'columns': [
			{
				"orderable": false, "class": "col-xs",
				render: function (data, type, row) {
					return '<div class="dropdown" style="text-align: center;">' +
						'<a  data-toggle="dropdown" href="#"><span class="fas fa-bars"></span></a>' +
						'<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">' +
						'<li><a title="Editar establecimiento" style="cursor:pointer; color:blue;" class="opcion-editar-linea" ><span class="fa fa-edit" aria-hidden="true">&nbsp;</span>&nbsp;Editar</a></li>' +
						'</ul>' +
						'</div>'

				}
			},
			{data: 'CLINEAPROCESOCLIENTE', "class": "col-xs"},
			{
				"orderable": false, "class": "col-xxs",
				render: function (data, type, row) {
					const color = (row.Peligro === 'Si') ? 'text-danger' : '';
					let res = '<div class="dropdown ' + color + '" style="text-align: left;">';
					res += row.DLINEACLIENTEE;
					res += '</div>';
					return res;
				}
			},
			{
				"orderable": false, "class": "col-xxs",
				render: function (data, type, row) {
					const color = (row.Peligro === 'Si') ? 'text-danger' : '';
					let res = '<div class="dropdown ' + color + '" style="text-align: left;">';
					res += row.Peligro;
					res += '</div>';
					return res;
				}
			},
		],
	});
	// Enumeracion
	tblLineaEstable.on('order.dt search.dt', function () {
		tblLineaEstable.column(1, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
			cell.innerHTML = i + 1;
		});
	}).draw();
};

