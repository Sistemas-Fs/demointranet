/*!
 *
 * @version 1.0.0
 */

const objClienteLista = {};
let oTableCliente, tblEstablecimiento, folderimage;

$(function() {

	/**
	 * Muestra la lista ocultando el formulario
	 */
	objClienteLista.mostrarBusqueda = function () {
		const boton = $('#btnAccionContenedorLista');
		const icon = boton.find('i');
		if (icon.hasClass('fa-minus')) icon.removeClass('fa-minus');
		icon.addClass('fa-plus');
		boton.click();
		$('#contenedorRegestable').hide();
		$('#contenedorRegprov').hide();
		$('#contenedorRegmaq').hide();
		$('#contenedorBusqueda').show();
		$('#contenedorRegserv').hide();
		$('#contenedorRegcontacto').hide();
		$('#contenedorRegarea').hide();
		$('#btnRetornarLista').show();
		//objFiltro.buscar()
	};

	objClienteLista.refrescarLista = function() {
		$("#tblinspeccionprov").DataTable().ajax.reload(null, false);
	};

	objClienteLista.listar = function () {
		const boton = $('#btnBuscar');
		objPrincipal.botonCargando(boton);
		oTableCliente = $('#tblListPtcliente').DataTable({
			"processing": true,
			"bDestroy": true,
			"stateSave": true,
			"bJQueryUI": true,
			"scrollY": "650px",
			"scrollX": true,
			'AutoWidth': true,
			"paging": false,
			"info": true,
			"filter": true,
			"ordering": false,
			"responsive": false,
			"select": true,
			'ajax': {
				"url": baseurl + "oi/ctrlprov/cliente/ccliente/lista",
				"type": "POST",
				"data": function (d) {
					d.cliente = $('#txtCliente').val();
				},
				dataSrc: function(data) {
					objPrincipal.liberarBoton(boton);
					return data;
				},
			},
			'columns': [
				// {data: 'SPACE', "class": "col-xxs"},
				{
					"orderable": false, "class": "col-xxs",
					render: function (data, type, row) {
						let DRAZONSOCIAL = row.DRAZONSOCIAL.replace(/['"]+/g, '');
						return '<div class="dropdown" style="text-align: center;">' +
							'<a  data-toggle="dropdown" href="#"><span class="fas fa-bars"></span></a>' +
							'<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">' +
							'<li><a title="Editar" style="cursor:pointer; color:blue;" class="opcion-editar" ><span class="fa fa-edit" aria-hidden="true">&nbsp;</span>&nbsp;Editar</a></li>' +
							'<li><div class="dropdown-divider" ></div></li>' +
							'<li><a title="Establecimientos" style="cursor:pointer; color:blue;" onClick="objEstablecimiento.mostrarRegEstable(\'' + row.CCLIENTE + '\',\'' + DRAZONSOCIAL + '\',\'' + row.DDIRECCIONCLIENTE + '\',\'' + row.dzip + '\',\'' + row.cpais + '\',\'' + row.dciudad + '\',\'' + row.destado + '\',\'' + row.CUBIGEO + '\',\'' + row.DUBIGEO + '\');"><span class="fas fa-map-marked-alt" aria-hidden="true">&nbsp;</span>&nbsp;Establecimientos</a></li>' +
							'<li><a title="Contactos" style="cursor:pointer; color:blue;" onClick="objContacto.mostrarRegContacto2(\'' + row.CCLIENTE + '\',\'' + 0 + '\',\'' + DRAZONSOCIAL + '\',\'' + row.DDIRECCIONCLIENTE + '\');"><span class="fas fa-user-plus" aria-hidden="true">&nbsp;</span>&nbsp;Contactos</a></li>' +
							'<li><a title="Proveedores" style="cursor:pointer; color:blue;" onClick="objProveedor.mostrarRegProv(\'' + row.CCLIENTE + '\',\'' + DRAZONSOCIAL + '\',\'' + row.DDIRECCIONCLIENTE + '\',\'' + row.dzip + '\',\'' + row.cpais + '\',\'' + row.dciudad + '\',\'' + row.destado + '\',\'' + row.CUBIGEO + '\',\'' + row.DUBIGEO + '\');"><span class="fas fa-users" aria-hidden="true">&nbsp;</span>&nbsp;Proveedores</a></li>' +
							// '<li><a title="Servicios" style="cursor:pointer; color:blue;" onClick="objServicio.mostrarRegServ(\'' + row.CCLIENTE + '\',\'' + DRAZONSOCIAL + '\',\'' + row.DDIRECCIONCLIENTE + '\',\'' + row.dzip + '\',\'' + row.cpais + '\',\'' + row.dciudad + '\',\'' + row.destado + '\',\'' + row.CUBIGEO + '\',\'' + row.DUBIGEO + '\');"><span class="fas fa-bars" aria-hidden="true">&nbsp;</span>&nbsp;Servicios</a></li>' +
							'<li><a title="Area del cliente" style="cursor:pointer; color:blue;" onClick="objArea.mostrarRegArea(\'' + row.CCLIENTE + '\',\'' + DRAZONSOCIAL + '\',\'' + row.DDIRECCIONCLIENTE + '\');"><span class="fas fa-user-plus" aria-hidden="true">&nbsp;</span>&nbsp;Categoría</a></li>' +
							'</ul>' +
							'</div>'
					}
				},
				{
					"orderable": false, "class": "col-l",
					render: function (data, type, row) {
						if (row.DRUTA == '') {
							return '<div class="user-block" style="float: none;">' +
								'<img src="' + baseurl + 'FTPfileserver/Imagenes/clientes/unknown.png"  width="64" height="64" class="img-circle img-bordered-sm">&nbsp;&nbsp;&nbsp;' +
								'<span class="username"  style="vertical-align: middle; margin-top: -25px;">' + row.DRAZONSOCIAL + '</span><span class="description"><h6>' + row.NRUC + '</h6></span>' +
								'</div>';
						} else {
							return '<div class="user-block" style="float: none;">' +
								'<img src="' + baseurl + 'FTPfileserver/Imagenes/clientes/' + row.DRUTA + '"  width="64" height="64" class="img-circle img-bordered-sm">&nbsp;&nbsp;&nbsp;' +
								'<span class="username" style="vertical-align: middle; margin-top: -25px;">' + row.DRAZONSOCIAL + '</span><span class="description"><h6>' + row.NRUC + '</h6></span>' +
								'</div>';
						}
					}
				},
				{
					"orderable": false, "class": "col-lm",
					render: function (data, type, row) {
						if (row.DRUTA == '') {
							return '<div>' +
								'<span class="username"  style="vertical-align: middle;">' + row.DDIRECCIONCLIENTE + '</span><br><span class="description"><small>' + row.DUBIGEO + '</small></span>' +
								'</div>';
						} else {
							return '<div>' +
								'<span class="username" style="vertical-align: middle;">' + row.DDIRECCIONCLIENTE + '</span><br><span class="description"><small>' + row.DUBIGEO + '</small></span>' +
								'</div>';
						}
					}
				},
				{data: 'DTELEFONO', "class": "col-sm"},
				{data: 'DREPRESENTANTE', "class": "col-m"}
			],
		});
		// Enumeracion
		// oTableCliente.on( 'order.dt search.dt', function () {
		//   oTableCliente.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
		//       cell.innerHTML = i+1;
		//       } );
		// }).draw();
	};

	objClienteLista.initialLoad = function() {
		$.ajax({ //tamaño de empresa
			type: 'ajax',
			method: 'post',
			url: baseurl + "oi/ctrlprov/cliente/ccliente/getsizeempresa",
			dataType: "JSON",
			async: true,
			success: function (result) {
				$('#cboSizeEmp').html(result);
			},
			error: function () {
				alert('Error, No se puede autenticar por error');
			}
		});

		$.ajax({ //GRUPO EMPRESARIAL
			type: 'ajax',
			method: 'post',
			url: baseurl + "oi/ctrlprov/cliente/ccliente/getgrupoempresarial",
			dataType: "JSON",
			async: true,
			success: function (result) {
				$('#cboGrupEmp').html(result);
			},
			error: function () {
				alert('Error, No se puede autenticar por error');
			}
		});

		objClienteLista.editar = function() {
			var tr = $(this).parents('tr');
			var row = oTableCliente.row(tr);
			var rowData = row.data();
			objClienteGenerar.mostrarDatos(rowData.CCLIENTE, rowData.NRUC, rowData.DRAZONSOCIAL, rowData.cpais, rowData.dciudad,
				rowData.destado, rowData.dzip, rowData.CUBIGEO, rowData.DDIRECCIONCLIENTE, rowData.DTELEFONO, rowData.DFAX,
				rowData.dweb, rowData.ZCTIPOTAMANOEMPRESA, rowData.NTRABAJADOR, rowData.DREPRESENTANTE,
				rowData.DCARGOREPRESENTANTE, rowData.DEMAILREPRESENTANTE, rowData.DRUTA, rowData.TIPODOC,
				rowData.DUBIGEO);
		};

	};

});

$(document).ready(function() {

	objClienteLista.initialLoad();

	$('#btnBuscar').click(objClienteLista.listar);

	$('#btnRetornarListaProv,#btnRetornarListaMaq,#btnRetornarListaContacto').click(function () {
		objClienteLista.mostrarBusqueda();
	});

	// $('#tblListPtcliente tbody').on('dblclick', 'td', objClienteGenerar.editar);

	$('#tabptcliente a[href="#tabptcliente-list-tab"]').attr('class', 'disabled');
	$('#tabptcliente a[href="#tabptcliente-reg-tab"]').attr('class', 'disabled active');

	$('#tabptcliente a[href="#tabptcliente-list-tab"]').not('#store-tab.disabled').click(function (event) {
		$('#tabptcliente a[href="#tabptcliente-list"]').attr('class', 'active');
		$('#tabptcliente a[href="#tabptcliente-reg"]').attr('class', '');
		return true;
	});
	$('#tabptcliente a[href="#tabptcliente-reg-tab"]').not('#bank-tab.disabled').click(function (event) {
		$('#tabptcliente a[href="#tabptcliente-reg"]').attr('class', 'active');
		$('#tabptcliente a[href="#tabptcliente-list"]').attr('class', '');
		return true;
	});

	$('#tabptcliente a[href="#tabptcliente-list"]').click(function (event) {
		return false;
	});
	$('#tabptcliente a[href="#tabptcliente-reg"]').click(function (event) {
		return false;
	});
	$('#tabptcliente a[href="#tabptcliente-prov"]').click(function (event) {
		return false;
	});

	$("#boxDeparEstable").hide();
	$("#boxProvEstable").hide();
	$("#boxDistEstable").hide();
	$("#boxUbigeo").hide();

	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "cglobales/getpaises",
		dataType: "JSON",
		async: true,
		success: function (result) {
			$('#cboPais,#cboPaisEstable,#cboPaisProv,#cboPaisMaq').html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error');
		}
	});
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "cglobales/getdepartamentos",
		dataType: "JSON",
		async: true,
		success: function (result) {
			$('#cboDepa,#cboDepaEsta,#cboDepaProv,#cboDepaMaq').html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error');
		}
	});

	var btnCust = '';

	$("#file-input").fileinput({
		overwriteInitial: true,
		maxFileSize: 1500,
		showClose: false,
		showCaption: false,
		browseLabel: '',
		removeLabel: '',
		browseIcon: '<i class="fas fa-file-image"></i>',
		removeIcon: '<i class="fas fa-times-circle"></i>',
		removeTitle: 'Cancelar o remover',
		elErrorContainer: '#kv-avatar-errors-1',
		msgErrorClass: 'alert alert-block alert-danger',
		layoutTemplates: {main2: '{preview} ' + btnCust + ' {remove} {browse}'},
		allowedFileExtensions: ["jpeg", "jpg", "png", "gif"]
	});

	$(document).on('click', '.opcion-editar', objClienteLista.editar);

});
