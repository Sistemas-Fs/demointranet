/*!
 *
 * @version 1.0.0
 */

const objMaquilador = {};
var tblMaquilador;

$(function () {

	objMaquilador.mostrarRegMaq = function (ccliente, razonsocial, ddireccioncliente) {
		const boton = $('#btnAccionContenedorLista');
		const icon = boton.find('i');
		if (icon.hasClass('fa-plus')) icon.removeClass('fa-plus');
		icon.addClass('fa-minus');
		boton.click();

		$('#cardRegmaq').hide();
		$('#cardListmaq').show();

		document.querySelector('#lblClienteMaq').innerText = razonsocial;
		document.querySelector('#lblDirclieMaq').innerText = ddireccioncliente;

		$('#mhdnIdClie').val(ccliente);
		$('#hdnIdptcliemaq').val(ccliente);
		// ID DEL MAQUILADOR PARA EDITAR O CREAR
		$('#maqhdnIdptcliemaq').val(ccliente);

		$('#mhdnprovCcliente').val(ccliente);
		$('#mhdnprovDcliente').val(razonsocial);
		$('#mhdnprovDdireccion').val(ddireccioncliente);

		console.log('entro');

		listMaquilador(ccliente);

		$('#contenedorRegmaq').show();
		$('#contenedorRegprov').hide();
		$('#contenedorBusqueda').hide();
		$('#contenedorRegestable').hide();
		$('#contenedorRegserv').hide();
		$('#contenedorRegcontacto').hide();
		$('#contenedorRegarea').hide();
	};

	objMaquilador.editar = function() {
		var tr = $(this).parents('tr');
		var row = tblMaquilador.row(tr);
		var rowData = row.data();
		objMaquilador.mostrarDatos(rowData.cmaquilador, rowData.cproveedor, rowData.NRUC, rowData.DRAZONSOCIAL, rowData.cpais, rowData.dciudad,
			rowData.destado, rowData.dzip, rowData.CUBIGEO, rowData.DDIRECCIONCLIENTE, rowData.DTELEFONO, rowData.DFAX,
			rowData.dweb, rowData.ZCTIPOTAMANOEMPRESA, rowData.NTRABAJADOR, rowData.DREPRESENTANTE,
			rowData.DCARGOREPRESENTANTE, rowData.DEMAILREPRESENTANTE, rowData.DRUTA, rowData.tipodoc,
			rowData.DUBIGEO);
	};

	objMaquilador.mostrarDatos = function(cmaquilador, ccliente, nruc, drazonsocial, cpais, dciudad, destado, dzip, cubigeo, ddireccioncliente, dtelefono,
											  dfax, dweb, zctipotamanoempresa, ntrabajador, drepresentante, dcargorepresentante, demailrepresentante,
											  druta, tipodoc, dubigeo) {
		var ruta_imagen;
		ruta_imagen = baseurl + 'FTPfileserver/Imagenes/clientes/' + druta;
		$('#maqhdnIdptprovmaq').val(cmaquilador);
		$('#maqhdnIdptcliemaq').val(ccliente);
		$('#maqtxtnrodoc').val(nruc);
		$('#maqtxtrazonsocial').val(drazonsocial);
		$('#maqcboPais').val(cpais).trigger("change");
		$('#maqtxtCiudad').val(dciudad);
		$('#maqtxtEstado').val(destado);
		$('#maqhdnidubigeo').val(cubigeo);
		$('#maqmtxtUbigeo').val(dubigeo);
		$('#maqtxtCodigopostal').val(dzip);
		$('#maqtxtDireccion').val(ddireccioncliente);
		$('#maqtxtTelefono').val(dtelefono);
		$('#maqtxtFax').val(dfax);
		$('#maqtxtWeb').val(dweb);
		$('#maqtxtTipoempresa').val(zctipotamanoempresa);
		$('#maqtxtNroTrab').val(ntrabajador);
		$('#maqtxtRepresentante').val(drepresentante);
		$('#maqtxtCargorep').val(dcargorepresentante);
		$('#maqtxtEmailrep').val(demailrepresentante);
		$('#hdnAccionptclieMaq').val('E');
		$('#maqutxtlogo').val(druta);
		$('#maqcboTipoDoc').val(tipodoc);
		document.getElementById("maqimage_previa").src = ruta_imagen;
		$('#maqtabptcliente a[href="#tabptcliente-reg"]').tab('show');
		$('#maqdivlogo').hide();
		$("#mbtnsavecliente").prop('disabled', false);
		// $('#maqhdnCCliente').val(ccliente);
		$('#cardRegmaq').show();
		$('#cardListmaq').hide();
	};

});

$(document).ready(function () {

	$('#btnMaqNuevo').click(function () {
		$('#cardRegmaq').show();
		$('#cardListmaq').hide();
		$('#hdnAccionptclieMaq').val('N');
		$('#maqhdnIdptprovmaq').val(0);
	});

	$('#btnRetornarMaq').click(function () {
		$('#cardRegmaq').hide();
		$('#cardListmaq').show();
	});

	$('#frmMantptClieMaq').submit(function (event) {
		event.preventDefault();

		var request = $.ajax({
			url: $('#frmMantptClieMaq').attr("action"),
			type: $('#frmMantptClieMaq').attr("method"),
			data: $('#frmMantptClieMaq').serialize()
		});
		request.done(function (respuesta) {
			Vtitle = 'Datos Guardados correctamente';
			Vtype = 'success';
			sweetalert(Vtitle, Vtype);
			$('#cardRegmaq').hide();
			$('#cardListmaq').show();
			limpiarFormMaq();
			tblMaquilador.ajax.reload();
		});
		request.fail(function (jqxhr) {
			const message = (jqxhr.responseJSON && jqxhr.responseJSON.message)
				? jqxhr.responseJSON.message
				: 'Error en el proceso de ejecución.';
			sweetalert(message, 'error');
		});
	});

	$(document).on('click', '.opcion-editar-maquilador', objMaquilador.editar);

	$('#btnAddMaquiladorModal').click(function () {
		listarclienteEnMaq();
	});

});

limpiarFormMaq = function () {
	$('#frmMantptClieMaq').trigger("reset");
	$('#maqhdnIdptprovmaq').val(0);
	$('#cboPaisMaq').val('').trigger("change");
	$('#cboUbigeo').val('').trigger("change");
}

listMaquilador = function (ccliente) {
	tblMaquilador = $('#tblListMaquiladoresInicio').DataTable({
		"processing": true,
		"bDestroy": true,
		"stateSave": true,
		"bJQueryUI": true,
		"scrollY": "650px",
		"scrollX": true,
		'AutoWidth': true,
		"paging": false,
		"info": true,
		"filter": true,
		"ordering": false,
		"responsive": false,
		"select": true,
		//'fixedColumns':{
		//  'leftColumns': false,// Fijo primera columna
		//  'rightColumns':1
		//},
		//'lengthMenu'  : [[10, 20, 30, -1], [10, 20, 30, "Todo"]],
		'ajax': {
			"url": baseurl + "at/ctrlprov/cliente/cmaquilador/lista",
			"type": "POST",
			"data": function (d) {
				d.ccliente = ccliente
			},
			dataSrc: ''
		},
		'columns': [
			{data: 'SPACE', "class": "col-xxs"},
			{
				"orderable": false, "class": "col-xxs",
				render: function (data, type, row) {
					var drazonsocial = (row.drazonsocial) ? String(row.drazonsocial).replace('\'', '') : '';
					var ddireccioncliente = (row.ddireccioncliente) ? String(row.ddireccioncliente).replace('\'', '') : '';
					return '<div class="dropdown" style="text-align: center;">' +
						'<a  data-toggle="dropdown" href="#"><span class="fas fa-bars"></span></a>' +
						'<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">' +
						'<li><a title="Editar" style="cursor:pointer; color:blue;" class="opcion-editar-maquilador" ><span class="fa fa-edit" aria-hidden="true">&nbsp;</span>&nbsp;Editar</a></li>' +
						'<li><div class="dropdown-divider" ></div></li>' +
						'<li><a title="Establecimientos" style="cursor:pointer; color:blue;" onClick="objEstablecimiento.mostrarRegEstable(\'' + row.cmaquilador + '\',\'' + drazonsocial + '\',\'' + ddireccioncliente + '\');"><span class="fas fa-map-marked-alt" aria-hidden="true">&nbsp;</span>&nbsp;Establecimientos</a></li>' +
						'<li><a title="Contactos" style="cursor:pointer; color:blue;" onClick="objProveedor.mostrarRegProv(\'' + row.cmaquilador + '\',\'' + drazonsocial + '\',\'' + ddireccioncliente + '\');"><span class="fas fa-user-plus" aria-hidden="true">&nbsp;</span>&nbsp;Contactos</a></li>' +
						'</ul>' +
						'</div>'
				}

			},
			{data: 'cmaquilador', "class": "col-xxs"},
			{data: 'DRAZONSOCIAL', "class": "col-lm"},
			{data: 'NRUC', "class": "col-lm"},
		],

	});
	// Enumeracion
	tblMaquilador.on('order.dt search.dt', function () {
		tblMaquilador.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
			cell.innerHTML = i + 1;
		});
	}).draw();
};

addMaquiladorProveedor = function (ccliente, drazonsocial, ddireccioncliente) {
	var cclientePrincipal = $("#maqhdnIdptcliemaq").val();

	var params = {"ccliente": cclientePrincipal, "cproveedor": ccliente};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "at/ctrlprov/cliente/cmaquilador/agregar",
		dataType: "JSON",
		async: true,
		data: params,
	}).done(function(resp) {
		sweetalert(resp.message, 'success');
	}).fail(function() {
		const message = (jqxhr.responseJSON && jqxhr.responseJSON.message)
			? jqxhr.responseJSON.message
			: 'Error en el proceso de ejecución.';
		sweetalert(message, 'error');
	});

	$("#tblListMaquiladoresInicio").DataTable().ajax.reload(null, false);
}

listarclienteEnMaq = function () {
	oTableAddMaq = $('#tblListMaquiladores').DataTable({
		"processing": true,
		"bDestroy": true,
		"stateSave": true,
		"bJQueryUI": true,
		"scrollY": "500px",
		"scrollX": true,
		'AutoWidth': true,
		"paging": true,
		"info": true,
		"filter": true,
		"ordering": false,
		"responsive": true,
		"select": true,
		"serverSide": false,
		'ajax': {
			"url": baseurl + "at/ctrlprov/cliente/cmaquilador/lista_prov",
			"type": "POST",
			"data": function (d) {
				d.cliente = $('#maqhdnIdptcliemaq').val();
			},
			dataSrc: ''
		},
		'columns': [
			{data: 'SPACE', "class": "col-xxs"},
			{
				"orderable": false, "class": "col-xxs",
				render: function (data, type, row) {
					return '<div class="dropdown" style="text-align: center;">' +
						'<a  data-toggle="dropdown" href="#"><span class="fas fa-bars"></span></a>' +
						'<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">' +
						'<li><a title="Agregar Maquilador" style="cursor:pointer; color:blue;" onClick="addMaquiladorProveedor(\'' + row.CCLIENTE + '\',\'' + row.DRAZONSOCIAL + '\',\'' + row.ddireccioncliente + '\');"><span class="fas fa-user-plus" aria-hidden="true">&nbsp;</span>&nbsp;Agregar Maquilador</a></li>' +

						'</ul>' +
						'</div>'
				}
			},
			{data: 'CCLIENTE', "class": "col-xxs"},
			{data: 'DRAZONSOCIAL', "class": "col-md"},
			{data: 'DDIRECCIONCLIENTE', "class": "col-md"},
			{data: 'DREPRESENTANTE', "class": "col-md"},
			{data: 'NRUC', "class": "col-md"},
		],
	});
	// Enumeracion
	oTableAddMaq.on('order.dt search.dt', function () {
		oTableAddMaq.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
			cell.innerHTML = i + 1;
		});
	}).draw();
};


