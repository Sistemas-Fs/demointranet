/*!
 *
 * @version 1.0.0
 */

const objContacto = {};
var tblContactos;

$(function () {

	objContacto.mostrarRegContacto2 = function (ccliente, cestablecimiento, razonsocial, ddireccioncliente) {
		const boton = $('#btnAccionContenedorLista');
		const icon = boton.find('i');
		if (icon.hasClass('fa-plus')) icon.removeClass('fa-plus');
		icon.addClass('fa-minus');
		boton.click();

		$('#cardRegContacto').hide();
		$('#cardListContacto').show();

		document.querySelector('#lblClienteContacto').innerText = razonsocial;
		document.querySelector('#lblDirclieContacto').innerText = (ddireccioncliente !== '') ? ddireccioncliente : '';

		$('#mhdnIdContacto').val(0);
		$('#id_cliente_contacto').val(ccliente);
		$('#hdnIdptclieserv').val(ccliente);
		$('#mhdnconCestable').val(cestablecimiento);

		$('#mhdnconCcliente').val(ccliente);
		$('#mhdnconDcliente').val(razonsocial);
		$('#mhdnconDdireccion').val(ddireccioncliente);
		$('#id_cliente_establecimiento').val(cestablecimiento);
		if (cestablecimiento) {
			listContacto(ccliente, cestablecimiento);
			$("#btnAddContacto").show();
		} else {
			listContacto(ccliente);
			$("#btnAddContacto").hide();
		}
		$('#contenedorRegcontacto').show();
		$('#contenedorRegserv').hide();
		$('#contenedorRegmaq').hide();
		$('#contenedorRegprov').hide();
		$('#contenedorBusqueda').hide();
		$('#contenedorRegestable').hide();
		$('#contenedorRegarea').hide();
	};

	objContacto.editar = function () {
		var tr = $(this).parents('tr');
		var row = null;
		// var id_estable = $("#txtestablecimiento").val();
		row = tblContactos.row(tr);
		var rowData = row.data();
		$('#mhdnIdContacto').val(rowData.CCONTACTO);
		// $('#txtestablecimiento').val(rowData.CESTABLECIMIENTO);
		$('#id_cliente_establecimiento').val(rowData.CESTABLECIMIENTO);
		$('#id_cliente_contacto').val(rowData.CCLIENTE);
		$('#txtNombresContacto').val(rowData.DNOMBRE);
		$('#txtApePaterno').val(rowData.DAPEPAT);
		$('#txtApeMaterno').val(rowData.DAPEMAT);
		$('#txtCargoEmp').val(rowData.DCARGOCONTACTO);
		$('#txtEmailCon').val(rowData.DMAIL);
		$('#txtTelefonoCon').val(rowData.DTELEFONO);
		$('#cardRegContacto').show();
		$('#cardListContacto').hide();
	};

	objContacto.addContactoEstable = function (CCONTACTO, COD_ESTABLE, CCLIENTE) {
		var params = {ccontacto: CCONTACTO, cod_estable: COD_ESTABLE, ccliente: CCLIENTE};
		$.ajax({
			type: 'ajax',
			method: 'post',
			url: baseurl + "oi/ctrlprov/cliente/ccontacto/addcontactoxestablecimiento",
			dataType: "JSON",
			async: true,
			data: params,
			success: function (result) {
				Vtitle = 'Se agrego correctamente el contacto al establecimiento';
				Vtype = 'success';
				sweetalert(Vtitle, Vtype);
				listContacto(CCLIENTE, COD_ESTABLE);
			},
			error: function () {
				Vtitle = 'Error en el proceso addContacto!';
				Vtype = 'error';
				sweetalert(Vtitle, Vtype);
			}

		});
	}

});

$(document).ready(function () {

	$('#btnCerrarContacto').click(function () {
		$('#cardRegContacto').hide();
		$('#cardListContacto').show();
	});

	$('#frmMantContacto').submit(function (event) {
		event.preventDefault();
		var id_cliente = $("#id_cliente_contacto").val();
		var id_estable = $("#id_cliente_establecimiento").val();
		$.ajax({
			url: $('#frmMantContacto').attr("action"),
			type: $('#frmMantContacto').attr("method"),
			data: $('#frmMantContacto').serialize(),
		}).done(function (respuesta) {
			Vtitle = 'Datos Guardados correctamente';
			Vtype = 'success';
			sweetalert(Vtitle, Vtype);
			$('#cardRegContacto').hide();
			limpiarFormContacto();
			listContacto(id_cliente, id_estable);
			$('#cardListContacto').show();
		}).fail(function () {
			alert('Error, No se puede autenticar por error');
		});
	});

	$('#btnContactoNuevo').click(function () {
		$('#cardRegContacto').show();
		$('#cardListContacto').hide();
		limpiarFormContacto();
		$('#mhdnAccionContacto').val('N');
		$('#mhdnIdContacto').val(0);
	});

	$(document).on('click', '.opcion-editar-contacto', objContacto.editar);

	$("#btnAddContacto").click(function () {
		var ccliente = $("#id_cliente_contacto").val();
		var COD_ESTABLE = $("#id_cliente_establecimiento").val();
		listAddContactoEstable(ccliente, COD_ESTABLE);
		$('#modalAddContacto').modal('show');
	});

});

limpiarFormContacto = function () {
	$('#frmMantContacto').trigger("reset");
}

listContacto = function (ccliente, cestable) {
	tblContactos = $('#tblListContactos').DataTable({
		"processing": true,
		"bDestroy": true,
		"stateSave": true,
		"bJQueryUI": true,
		"scrollY": "500px",
		"scrollX": true,
		'AutoWidth': true,
		"paging": false,
		"info": true,
		"filter": true,
		"ordering": false,
		"responsive": false,
		"select": true,
		//'fixedColumns':{
		//  'leftColumns': false,// Fijo primera columna
		//  'rightColumns':1
		//},
		//'lengthMenu'  : [[10, 20, 30, -1], [10, 20, 30, "Todo"]],
		'ajax': {
			"url": baseurl + "oi/ctrlprov/cliente/ccontacto/lista",
			"type": "POST",
			"data": function (d) {
				d.ccliente = ccliente;
				d.cestable = cestable;
			},
			dataSrc: ''
		},
		'columns': [
			{
				"orderable": false, "class": "col-xxs",
				render: function (data, type, row) {
					return '<div class="dropdown" style="text-align: center;">' +
						'<a  data-toggle="dropdown" href="#"><span class="fas fa-bars"></span></a>' +
						'<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">' +
						'<li><a title="Editar" style="cursor:pointer; color:blue;" class="opcion-editar-contacto" ><span class="fa fa-edit" aria-hidden="true">&nbsp;</span>&nbsp;Editar</a></li>' +
						'</ul>' +
						'</div>';
				}
			},
			{data: 'CCLIENTE', "class": "col-xxs"},
			{
				"class": "col-xxs",
				render: function (data, type, row) {
					return row.DNOMBRE + ' ' + row.DAPEPAT + ' ' + row.DAPEMAT
				}
			},
			{data: 'DCARGOCONTACTO', "class": "col-xxs"},
			{data: 'DMAIL', "class": "col-xxs"},
			{data: 'DTELEFONO', "class": "col-xxs"},
		],
	});
	// Enumeracion
	tblContactos.on('order.dt search.dt', function () {
		tblContactos.column(1, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
			cell.innerHTML = i + 1;
		});
	}).draw();
};

listAddContactoEstable = function (ccliente, COD_ESTABLE) {
	var id_cliente = $("#id_cliente_contacto").val();
	tblContactosAdd = $('#tblListAddContactos').DataTable({
		"processing": true,
		"bDestroy": true,
		"stateSave": true,
		"bJQueryUI": true,
		'bStateSave': true,
		"scrollY": "500px",
		"scrollX": true,
		'scrollCollapse': true,
		'AutoWidth': true,
		"paging": false,
		"info": true,
		"filter": true,
		"ordering": false,
		"responsive": false,
		"select": true,
		"overflow": "none",
		//'fixedColumns':{
		//  'leftColumns': false,// Fijo primera columna
		//  'rightColumns':1
		//},
		//'lengthMenu'  : [[10, 20, 30, -1], [10, 20, 30, "Todo"]],
		'ajax': {
			"url": baseurl + "oi/ctrlprov/cliente/ccontacto/listacontactoxestablecimiento",
			"type": "POST",
			"data": function (d) {
				d.cliente = ccliente;
			},
			dataSrc: ''
		},
		'columns': [
			{data: 'CCLIENTE', "class": "col-xxs"},
			{
				"orderable": false, "class": "col-xxs",
				render: function (data, type, row) {
					return '<div class="dropdown" style="text-align: center;">' +
						'<a  data-toggle="dropdown" href="#"><span class="fas fa-bars"></span></a>' +
						'<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">' +
						'<li><a title="Agregar Contacto" style="cursor:pointer; color:blue;" onClick="objContacto.addContactoEstable(\'' + row.CCONTACTO + '\',\'' + COD_ESTABLE + '\',\'' + id_cliente + '\');"><span class="fas fa-user-plus" aria-hidden="true">&nbsp;</span>&nbsp;Agregar Proveedor</a></li>' +

						'</ul>' +
						'</div>'
				}
			},
			{
				"class": "col-xxs",
				render: function (data, type, row) {
					return row.DNOMBRE + ' ' + row.DAPEPAT + ' ' + row.DAPEMAT
				}
			},
			{data: 'DCARGOCONTACTO', "class": "col-xxs"},
			{data: 'DMAIL', "class": "col-xxs"},
			{data: 'DTELEFONO', "class": "col-xxs"},
		],

	});
	// Enumeracion
	tblContactosAdd.on('order.dt search.dt', function () {
		tblContactosAdd.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
			cell.innerHTML = i + 1;
		});
	}).draw();
};
