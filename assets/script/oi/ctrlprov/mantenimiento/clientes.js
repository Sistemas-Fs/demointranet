const objFormulario = {};
$(function () {
	/**
	 * Muestra la lista ocultando el formulario
	 */
	objFormulario.mostrarBusqueda = function () {
		const boton = $('#btnAccionContenedorLista');
		const icon = boton.find('i');
		if (icon.hasClass('fa-minus')) icon.removeClass('fa-minus');
		icon.addClass('fa-plus');
		boton.click();
		$('#contenedorRegestable').hide();
		$('#contenedorRegprov').hide();
		$('#contenedorRegmaq').hide();
		$('#contenedorBusqueda').show();
		$('#contenedorRegserv').hide();
		$('#contenedorRegcontacto').hide();
		$('#contenedorRegarea').hide();
		$('#btnRetornarLista').show();
		//objFiltro.buscar()
	};

	objFormulario.mostrarRegServ = function (ccliente, razonsocial, ddireccioncliente) {
		console.log('ccliente', ccliente)

		const boton = $('#btnAccionContenedorLista');
		const icon = boton.find('i');
		if (icon.hasClass('fa-plus')) icon.removeClass('fa-plus');
		icon.addClass('fa-minus');
		boton.click();

		$('#cardRegServ').hide();
		$('#cardListServ').show();

		document.querySelector('#lblClienteServ').innerText = razonsocial;
		document.querySelector('#lblDirclieServ').innerText = ddireccioncliente;

		$('#mhdnIdClie').val(ccliente);
		$('#hdnIdptclieserv').val(ccliente);

		$('#mhdnprovCcliente').val(ccliente);
		$('#mhdnprovDcliente').val(razonsocial);
		$('#mhdnprovDdireccion').val(ddireccioncliente);

		$("#idCLienteServ").val(ccliente);

		listServicios(ccliente);
		$('#contenedorRegserv').show();
		$('#contenedorRegmaq').hide();
		$('#contenedorRegprov').hide();
		$('#contenedorBusqueda').hide();
		$('#contenedorRegestable').hide();
		$('#contenedorRegcontacto').hide();
		$('#contenedorRegarea').hide();
	};

	objFormulario.mostrarRegContacto = function (ccliente, razonsocial, ddireccioncliente, COD_ESTABLE, COD_CLIENTE, DRAZONSOCIAL) {

		console.log('ccliente', ccliente)
		const boton = $('#btnAccionContenedorLista');
		const icon = boton.find('i');
		if (icon.hasClass('fa-plus')) icon.removeClass('fa-plus');
		icon.addClass('fa-minus');
		boton.click();

		$('#cardRegContacto').hide();
		$('#cardListContacto').show();

		document.querySelector('#lblClienteContacto').innerText = razonsocial;
		document.querySelector('#lblDirclieContacto').innerText = (ddireccioncliente !== '') ? ddireccioncliente : '';

		$('#mhdnIdContacto').val(ccliente);
		$('#hdnIdptclieserv').val(ccliente);

		$('#mhdnconCcliente').val(ccliente);
		$('#mhdnconDcliente').val(razonsocial);
		$('#mhdnconDdireccion').val(ddireccioncliente);

		if (COD_ESTABLE) {
			listContactoEstable(ccliente, COD_ESTABLE);
			$("#txtestablecimiento").val(COD_ESTABLE);
			$("#btnAddContacto").show();
		} else {
			listContacto(ccliente);
			$("#txtestablecimiento").val('');
			$("#btnAddContacto").hide();
		}


		$('#contenedorRegcontacto').show();
		$('#contenedorRegserv').hide();
		$('#contenedorRegmaq').hide();
		$('#contenedorRegprov').hide();
		$('#contenedorBusqueda').hide();
		$('#contenedorRegestable').hide();
		$('#contenedorRegarea').hide();
	};

});

$('#btnRetornarProv').click(function () {
	$('#cardRegprov').hide();
	$('#cardListProv').show();
});

$(document).ready(function () {

});

$('#btnNuevo').click(function () {

	$('#tabptcliente a[href="#tabptcliente-reg"]').tab('show');
	objClienteGenerar.limpiarForm();
	$('#divlogo').hide();
	$('#btnGrabar').show();
	$('#hdnAccionptclie').val('N');
});

$('#cboPais').change(function () {
	var v_cpais = $("#cboPais option:selected").attr("value");
	$('#mtxtUbigeo').val('');
	$('#hdnidubigeo').val('');
	if (v_cpais == '290') {
		$("#boxCiudad").hide();
		$("#boxEstado").hide();
		$("#boxUbigeo").show();
	} else {
		$("#boxCiudad").show();
		$("#boxEstado").show();
		$("#boxUbigeo").hide();
	}
});

$('#cboPaisProv').change(function () {
	var v_cpais = $("#cboPaisProv option:selected").attr("value");
	$('#mtxtUbigeoProv').val('');
	$('#hdnidubigeoprov').val('');
	if (v_cpais == '290') {
		$("#boxCiudadProv").hide();
		$("#boxEstadoProv").hide();
		$("#boxUbigeoProv").show();
	} else {
		$("#boxCiudadProv").show();
		$("#boxEstadoProv").show();
		$("#boxUbigeoProv").hide();
	}
});

$('#cboPaisMaq').change(function () {
	var v_cpais = $("#cboPaisMaq option:selected").attr("value");
	$('#mtxtUbigeoMaq').val('');
	$('#hdnidubigeomaq').val('');
	if (v_cpais == '290') {
		$("#boxCiudadMaq").hide();
		$("#boxEstadoMaq").hide();
		$("#boxUbigeoMaq").show();
	} else {
		$("#boxCiudadMaq").show();
		$("#boxEstadoMaq").show();
		$("#boxUbigeoMaq").hide();
	}
});

$('#btnBuscarUbigeo').click(function () {
	$("#modalUbigeo").modal();
});

$('#cboDepa').change(function () {
	var v_cdepa = $("#cboDepa option:selected").attr("value");
	var params = {"cdepa": v_cdepa};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "cglobales/getprovincias",
		dataType: "JSON",
		async: true,
		data: params,
		success: function (result) {
			$('#cboProv').html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error');
		}
	});
});

$('#cboProv').change(function () {
	var v_cdepa = $("#cboDepa option:selected").attr("value");
	var v_cprov = $("#cboProv option:selected").attr("value");
	var params = {
		"cdepa": v_cdepa,
		"cprov": v_cprov
	};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "cglobales/getdistritos",
		dataType: "JSON",
		async: true,
		data: params,
		success: function (result) {
			$('#cboDist').html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error');
		}
	});
});

$('#btnSelUbigeo').click(function () {
	var v_cubigeo = $("#cboDist option:selected").attr("value");
	var v_depa = $("#cboDepa").find('option:selected').text();
	var v_prov = $("#cboProv").find('option:selected').text();
	var v_dist = $("#cboDist").find('option:selected').text();
	$('#mtxtUbigeo').val(v_depa + ' - ' + v_prov + ' - ' + v_dist);
	$('#hdnidubigeo').val(v_cubigeo);
	$("#btncerrarUbigeo").click();

});

registrar_imagen = function () {
	var archivoInput = document.getElementById('file-input').files[0];
	console.log('archivoInput', archivoInput)
	var archivoRuta = archivoInput.name;
	var extPermitidas = /(.gif|.jpg|.png|.jpeg)$/i;

	if (!extPermitidas.exec(archivoRuta)) {
		alert('Asegurese de haber seleccionado un GIF, JPG, PNG, JPEG');
		archivoInput.value = '';
		return false;
	} else {
		var parametrotxt = new FormData($("#frmFileinputLogoclie1")[0]);
		console.log('parametrotxt', parametrotxt)
		$.ajax({
			data: parametrotxt,
			type: 'post',
			enctype: 'multipart/form-data',
			url: baseurl + "oi/ctrlprov/mantenimiento/cclientes/upload_image",
			dataType: "JSON",
			async: true,
			cache: false,
			contentType: false,
			processData: false,
			success: function (response) {
				console.log('response', response)
				$('#divlogo').show();
				folderimage = response.nombreArch;
				$('#utxtlogo').val(folderimage);
			},
			error: function (error) {
				console.log('error', error)
				alert('Error, no se cargó el archivo');
			}

		});
	}
}


registrar_imagen_prov = function () {
	var archivoInput = document.getElementById('file-input').files[0].name;
	var archivoRuta = archivoInput;
	var extPermitidas = /(.gif|.jpg|.png|.jpeg)$/i;

	if (!extPermitidas.exec(archivoRuta)) {
		alert('Asegurese de haber seleccionado un GIF, JPG, PNG, JPEG');
		archivoInput.value = '';
		return false;
	} else {
		var parametrotxt = new FormData($("#frmFileinputLogoclieProv")[0]);
		$.ajax({
			data: parametrotxt,
			method: 'post',
			url: baseurl + "oi/ctrlprov/mantenimiento/cclientes/upload_image",
			dataType: "JSON",
			async: true,
			contentType: false,
			processData: false,
			cache: false,
			success: function (response) {
				$('#divlogoprov').show();
				folderimage = response.nombreArch;
				$('#utxtlogoprov1').val(folderimage);
				var ruta_imagen = baseurl + 'FTPfileserver/Imagenes/clientes/' + folderimage;
				$("#image_previaprov").val(ruta_imagen);
			},
			error: function (error) {
				console.log('error', error)
				alert('Error, no se cargó el archivo');
			}

		});
	}
}

limpiarFormProv = function () {
	$('#frmMantptClieProv').trigger("reset");
	$('#hdnIdptclieprov').val('');
	$('#cboPaisProv').val('').trigger("change");
	$('#cboUbigeo').val('').trigger("change");
}

// $('#frmMantptClieProv').submit(function (event) {
// 	event.preventDefault();
//
// 	var request = $.ajax({
// 		url: $('#frmMantptClieProv').attr("action"),
// 		type: $('#frmMantptClieProv').attr("method"),
// 		data: $('#frmMantptClieProv').serialize(),
// 		error: function () {
// 			alert('Error, No se puede autenticar por error');
// 		}
// 	});
// 	request.done(function (respuesta) {
//
// 		Vtitle = 'Datos Guardados correctamente';
// 		Vtype = 'success';
// 		sweetalert(Vtitle, Vtype);
// 		$('#cardRegprov').hide();
// 		$('#cardListProv').show();
// 		limpiarFormProv();
// 		tblProveedores.ajax.reload();
// 	});
// });

$('#cboPaisEstable').change(function () {
	var v_cpais = $("#cboPaisEstable option:selected").attr("value");

	$('#mtxtUbigeoEstable').val('');
	$('#hdnidubigeoEstable').val('');

	if (v_cpais == '290') {
		$("#boxCiudadEstable").hide();
		$("#boxEstadoEstable").hide();
		$("#boxUbigeoEstable").show();
	} else {
		$("#boxCiudadEstable").show();
		$("#boxEstadoEstable").show();
		$("#boxUbigeoEstable").hide();
	}
});

$('#btnBuscarUbigeoProv').click(function () {
	$("#modalUbigeoProv").modal();
});

$('#btnBuscarUbigeoMaq').click(function () {
	$("#modalUbigeoMaq").modal();
});

$('#cboDepaEsta').change(function () {
	var v_cdepa = $("#cboDepaEsta option:selected").attr("value");
	var params = {"cdepa": v_cdepa};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "cglobales/getprovincias",
		dataType: "JSON",
		async: true,
		data: params,
		success: function (result) {
			$('#cboProvEsta').html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error');
		}
	});
});

$('#cboProvEsta').change(function () {
	var v_cdepa = $("#cboDepaEsta option:selected").attr("value");
	var v_cprov = $("#cboProvEsta option:selected").attr("value");
	var params = {
		"cdepa": v_cdepa,
		"cprov": v_cprov
	};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "cglobales/getdistritos",
		dataType: "JSON",
		async: true,
		data: params,
		success: function (result) {
			$('#cboDistEsta').html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error');
		}
	});
});

$('#cboDistEsta').change(function () {
	var v_cubigeo = $("#cboDistEsta option:selected").attr("value");
	$('#hdnidubigeoEstable').val(v_cubigeo);
});


$('#cboDepaProv').change(function () {
	var v_cdepa = $("#cboDepaProv option:selected").attr("value");
	var params = {"cdepa": v_cdepa};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "cglobales/getprovincias",
		dataType: "JSON",
		async: true,
		data: params,
		success: function (result) {
			$('#cboProvProv').html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error');
		}
	});
});

$('#cboProvProv').change(function () {
	var v_cdepa = $("#cboDepaProv option:selected").attr("value");
	var v_cprov = $("#cboProvProv option:selected").attr("value");
	var params = {
		"cdepa": v_cdepa,
		"cprov": v_cprov
	};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "cglobales/getdistritos",
		dataType: "JSON",
		async: true,
		data: params,
		success: function (result) {
			$('#cboDistProv').html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error');
		}
	});
});

$('#cboDistProv').change(function () {
	var v_cubigeo = $("#cboDistProv option:selected").attr("value");
	$('#hdnidubigeoprov').val(v_cubigeo);
});


$('#cboDepaMaq').change(function () {
	var v_cdepa = $("#cboDepaMaq option:selected").attr("value");
	var params = {"cdepa": v_cdepa};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "cglobales/getprovincias",
		dataType: "JSON",
		async: true,
		data: params,
		success: function (result) {
			$('#cboProvMaq').html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error');
		}
	});
});

$('#cboProvMaq').change(function () {
	var v_cdepa = $("#cboDepaMaq option:selected").attr("value");
	var v_cprov = $("#cboProvMaq option:selected").attr("value");
	var params = {
		"cdepa": v_cdepa,
		"cprov": v_cprov
	};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "cglobales/getdistritos",
		dataType: "JSON",
		async: true,
		data: params,
		success: function (result) {
			$('#cboDistMaq').html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error');
		}
	});
});

$('#cboDistMaq').change(function () {
	var v_cubigeo = $("#cboDistMaq option:selected").attr("value");
	$('#hdnidubigeomaq').val(v_cubigeo);
});


$('#btnSelUbigeoEsta').click(function () {
	var v_cubigeo = $("#cboDistEsta option:selected").attr("value");
	var v_depa = $("#cboDepaEsta").find('option:selected').text();
	var v_prov = $("#cboProvEsta").find('option:selected').text();
	var v_dist = $("#cboDistEsta").find('option:selected').text();
	$('#mtxtUbigeoEstable').val(v_depa + ' - ' + v_prov + ' - ' + v_dist);
	$('#hdnidubigeoEstable').val(v_cubigeo);
	$("#btncerrarUbigeoEsta").click();

});

$('#btnSelUbigeoProv').click(function () {
	var v_cubigeo = $("#cboDistProv option:selected").attr("value");
	var v_depa = $("#cboDepaProv").find('option:selected').text();
	var v_prov = $("#cboProvProv").find('option:selected').text();
	var v_dist = $("#cboDistProv").find('option:selected').text();
	$('#mtxtUbigeoProv').val(v_depa + ' - ' + v_prov + ' - ' + v_dist);
	$('#hdnidubigeoprov').val(v_cubigeo);
	$("#btncerrarUbigeoProv").click();

});


$('#btnSelUbigeoPMaq').click(function () {
	var v_cubigeo = $("#cboDistMaq option:selected").attr("value");
	var v_depa = $("#cboDepaMaq").find('option:selected').text();
	var v_prov = $("#cboProvMaq").find('option:selected').text();
	var v_dist = $("#cboDistMaq").find('option:selected').text();
	$('#mtxtUbigeoMaq').val(v_depa + ' - ' + v_prov + ' - ' + v_dist);
	$('#hdnidubigeomaq').val(v_cubigeo);
	$("#btncerrarUbigeoMaq").click();

});

newProveedorCliente = function (ccliente, razonsocial, direccioncliente, dzip, cpais, dciudad, destado, cubigeo, dubigeo) {
	console.log('dubigeo', dubigeo)
	console.log('cubigeo', cubigeo)
	console.log('destado', destado)
	console.log('dciudad', dciudad)
	console.log('cpais', cpais)
	console.log('dzip', dzip)
	console.log('direccioncliente', direccioncliente)
	console.log('razonsocial', razonsocial)
	console.log('ccliente', ccliente)
	$('#tabptcliente a[href="#tabptcliente-prov"]').tab('show');
}

$('#btnProvNuevo').click(function () {
	$('#cardRegprov').show();
	$('#cardListProv').hide();


	$('#mhdnIdClie').val($('#mhdnestCcliente').val());
	$('#txtestableCI').val($('#mhdnestDcliente').val());
	$('#txtestabledireccion').val($('#mhdnestDdireccion').val());
	$('#txtestablezip').val($('#mhdnestDzid').val());
	$('#cboPaisEstable').val($('#mhdnestCpais').val()).trigger("change");
	$('#txtCiudadEstable').val($('#mhdnestDciudad').val());
	$('#txtEstadoEstable').val($('#mhdnestDestado').val());
	$('#hdnidubigeoEstable').val($('#mhdnestCubigeo').val());
	$('#mtxtUbigeoEstable').val($('#mhdnestDubigeo').val());

	$('#hdnAccionptclieProv').val('N');
});

$('#btnAddProveedorModal').click(function () {
	listarclienteEnProv();

});

$('#btnLineaProceso').click(function () {
	$('#cardRegestable').hide();
	$('#cardLineaProc').show();
	$('#cardListestable').hide()

	// $.ajax({
	//   type: 'ajax',
	//   method: 'post',
	//   url: baseurl+"oi/ctrlprov/mantenimiento/cclientes/getservicios",
	//   dataType: "JSON",
	//   async: true,
	//   success:function(result){
	//    $("#cboServicio").html(result);
	//   },
	//   error: function(){
	//     Vtitle = 'Error en el proceso!';
	//     Vtype = 'error';
	//     sweetalert(Vtitle,Vtype);
	//   }

	// });

});

/**/
(function ($, window) {
	'use strict';

	var MultiModal = function (element) {
		this.$element = $(element);
		this.modalCount = 0;
	};

	MultiModal.BASE_ZINDEX = 1040;

	MultiModal.prototype.show = function (target) {
		var that = this;
		var $target = $(target);
		var modalIndex = that.modalCount++;

		$target.css('z-index', MultiModal.BASE_ZINDEX + (modalIndex * 20) + 10);

		window.setTimeout(function () {
			if (modalIndex > 0)
				$('.modal-backdrop').not(':first').addClass('hidden');

			that.adjustBackdrop();
		});
	};

	MultiModal.prototype.hidden = function (target) {
		this.modalCount--;

		if (this.modalCount) {
			this.adjustBackdrop();
			$('body').addClass('modal-open');
		}
	};

	MultiModal.prototype.adjustBackdrop = function () {
		var modalIndex = this.modalCount - 1;
		$('.modal-backdrop:first').css('z-index', MultiModal.BASE_ZINDEX + (modalIndex * 20));
	};

	function Plugin(method, target) {
		return this.each(function () {
			var $this = $(this);
			var data = $this.data('multi-modal-plugin');

			if (!data)
				$this.data('multi-modal-plugin', (data = new MultiModal(this)));

			if (method)
				data[method](target);
		});
	}

	$.fn.multiModal = Plugin;
	$.fn.multiModal.Constructor = MultiModal;

	$(document).on('show.bs.modal', function (e) {
		$(document).multiModal('show', e.target);
	});

	$(document).on('hidden.bs.modal', function (e) {
		$(document).multiModal('hidden', e.target);
	});
}(jQuery, window));
