/*!
 *
 * @version 1.0.0
 */

const objValorChecklist = {};

$(function() {

	objValorChecklist.lista = function() {
		const boton = $('#btnBuscar');
		objPrincipal.botonCargando(boton);
		$('#tblLista').DataTable({
			'bJQueryUI': true,
			'scrollY': '650px',
			'scrollX': true,
			'processing': true,
			'bDestroy': true,
			'paging': true,
			'info': true,
			'filter': true,
			'ajax': {
				"url": baseurl + 'oi/ctrlprov/csistema/lista',
				"type": "POST",
				"data": function (d) {
					d.busqueda = $('#filtro_texto').val();
				},
				dataSrc: function (data) {
					objPrincipal.liberarBoton(boton);
					return data.items;
				},
				error: function () {
					objPrincipal.alert('warning', 'Error en el proceso de ejecución.', 'Vuelva a intentarlo más tarde.');
					objPrincipal.liberarBoton(boton);
				}
			},
			'columns': [
				{
					"orderable": false,
					render: function (data, type, row) {
						const rowId = 'dropdown-' + row.csistema;
						let htmlRow = '<div class="dropdown">';
						htmlRow += '<button type="button" class="btn btn-secondary btn-sm dropdown-toggle" role="button" id="' + rowId + '" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
						htmlRow += '<i class="fa fa-bars" ></i> Opciones';
						htmlRow += '</button>';
						htmlRow += '<div class="dropdown-menu" aria-labelledby="' + rowId + '">';
						htmlRow += '<button type="button" class="btn btn-sm dropdown-item editar-sistema" data-codigo="' + row.cvalor + '" ><i class="fa fa-edit" ></i> Editar</button>';
						htmlRow += '<button type="button" class="btn btn-sm dropdown-item eliminar-sistema" data-codigo="' + row.cvalor + '" ><i class="fa fa-trash" ></i> Eliminar</button>';
						htmlRow += '</div>';
						htmlRow += '</div>';
						return htmlRow;
					},
				},
				{data: 'dsistema', orderable: false, targets: 1},
				{data: 'darea', orderable: false, targets: 2},
				{data: 'dservicio', orderable: false, targets: 3},
				{
					"orderable": false,
					render: function (data, type, row) {
						return (row.sregistro === 'A') ? 'ACTIVO' : 'INACTIVO';
					},
				},
			],
			"columnDefs": [
				{
					"defaultContent": " ",
					"targets": "_all"
				},
			],
		});
	};

});

$(document).ready(function() {

	$('#btnBuscar').click(objValorChecklist.lista);

});

