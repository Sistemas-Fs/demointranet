/*!
 *
 * @version 1.0.0
 */

const objLista = {};

$(function () {

	/**
	 * Busqueda de los sistemas
	 */
	objLista.lista = function () {
		const boton = $('#btnBuscar');
		objPrincipal.botonCargando(boton);
		$('#tblSistema').DataTable({
			'bJQueryUI': true,
			'scrollY': '650px',
			'scrollX': true,
			'processing': true,
			'bDestroy': true,
			'paging': true,
			'info': true,
			'filter': true,
			'ajax': {
				"url": baseurl + 'oi/ctrlprov/csistema/lista',
				"type": "POST",
				"data": function (d) {
					d.busqueda = $('#filtro_texto').val();
				},
				dataSrc: function (data) {
					objPrincipal.liberarBoton(boton);
					return data.items;
				},
				error: function () {
					objPrincipal.alert('warning', 'Error en el proceso de ejecución.', 'Vuelva a intentarlo más tarde.');
					objPrincipal.liberarBoton(boton);
				}
			},
			'columns': [
				{
					"orderable": false,
					render: function (data, type, row) {
						const rowId = 'dropdown-' + row.csistema;
						let htmlRow = '<div class="dropdown">';
						htmlRow += '<button type="button" class="btn btn-secondary btn-sm dropdown-toggle" role="button" id="' + rowId + '" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
						htmlRow += '<i class="fa fa-bars" ></i> Opciones';
						htmlRow += '</button>';
						htmlRow += '<div class="dropdown-menu" aria-labelledby="' + rowId + '">';
						htmlRow += '<button type="button" class="btn btn-sm dropdown-item editar-sistema" data-codigo="' + row.csistema + '" ><i class="fa fa-edit" ></i> Editar</button>';
						htmlRow += '<button type="button" class="btn btn-sm dropdown-item eliminar-sistema" data-codigo="' + row.csistema + '" ><i class="fa fa-trash" ></i> Eliminar</button>';
						htmlRow += '</div>';
						htmlRow += '</div>';
						return htmlRow;
					},
				},
				{data: 'dsistema', orderable: false, targets: 1},
				{data: 'darea', orderable: false, targets: 2},
				{data: 'dservicio', orderable: false, targets: 3},
				{
					"orderable": false,
					render: function (data, type, row) {
						return (row.sregistro === 'A') ? 'ACTIVO' : 'INACTIVO';
					},
				},
			],
			"columnDefs": [
				{
					"defaultContent": " ",
					"targets": "_all"
				},
			],
		});
	};

	/**
	 * SE ejecuta el boton editar
	 */
	objLista.editar = function () {
		const boton = $(this);
		const codigo = boton.data('codigo');
		$.ajax({
			url: baseurl + 'oi/ctrlprov/csistema/buscar',
			method: 'POST',
			data: {
				codigo: codigo,
			},
			dataType: 'json',
			beforeSend: function () {
				objPrincipal.botonCargando(boton);
			}
		}).done(function (res) {
			if (!res) {
				objPrincipal.notify('error', 'El Sistema no pudo ser encontrado.');
			} else {
				objGenerar.imprimirDatos(
					res.CSISTEMA,
					res.DSISTEMA,
					res.CAREA,
					res.CSERVICIO,
					res.SREGISTRO,
				);
				$('#modalGenerarSistema').modal('show');
			}
		});
	};

	/**
	 * Elimina el sistema
	 */
	objLista.eliminar = function () {
		Swal.fire({
			type: 'warning',
			title: 'Eliminar Sistema',
			text: '¿Estas seguro(a) de eliminar?',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Si',
			cancelButtonText: 'Cancelar',
		}).then((result) => {
			if (result.value) {
				const boton = $(this);
				const codigo = boton.data('codigo');
				$.ajax({
					url: baseurl + 'oi/ctrlprov/csistema/eliminar',
					method: 'POST',
					data: {
						codigo: codigo,
					},
					dataType: 'json',
					beforeSend: function () {
						objPrincipal.botonCargando(boton);
					}
				}).done(function (res) {
					objPrincipal.notify('success', res.message);
					$('#btnBuscar').click();
				}).fail(function(jhxr) {
					const message = (jhxr && jhxr.responseJSON && jhxr.responseJSON.message)
						? jhxr.responseJSON.message
						: 'Error en el proceso de ejecución.';
					objPrincipal.notify('error', message);
				}).always(function() {

				});
			}
		});
	};

});

$(document).ready(function () {

	objLista.lista();

	$('#btnBuscar').click(objLista.lista);

	$(document).on('click', '.editar-sistema', objLista.editar);

	$(document).on('click', '.eliminar-sistema', objLista.eliminar);

});
