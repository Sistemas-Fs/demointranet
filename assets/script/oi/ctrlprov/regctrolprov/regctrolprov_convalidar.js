/*
 + @version 1.0.0
 */

const objRegCtrlProvConvalidar = {};

$(function() {

	/**
	 * Cambiar resultado
	 */
	objRegCtrlProvConvalidar.cambioResultado = function() {
		var v_resul = $(this).val();
		var v_ccriterio = $('#mhdncritresultconvali').val();
		$.ajax({
			url: baseurl + "at/ctrlprov/cregctrolprov/calculocriterio",
			method: 'post',
			dataType: "JSON",
			data: {
				resultado: v_resul,
				ccriterio: v_ccriterio
			},
		}).done(function(result) {
			$.each(result, function (key, value) {
				v_nr = value.nr;
				v_ng = value.ng;
				v_nb = value.nb;
				document.getElementById("divcolor").style.backgroundColor = 'rgb(' + v_nr + ',' + v_ng + ',' + v_nb + ')';
				$('#txtconvalivalor').val(value.ddetallecriterioresultado);
				$('#txtconvalimes').val(value.nmes);
				$('#mhdncritdetresultconvali').val(value.cdetallecriterioresultado);
			});
		});
	};

	/**
	 * Proceso al cambiar la certificadora
	 */
	objRegCtrlProvConvalidar.cambiarCertificadora = function() {
		var idcertificadora = $(this).val();
		$.ajax({
			url: baseurl + "at/ctrlprov/cregctrolprov/getcbocertificacion",
			method: 'post',
			async: true,
			data: {
				ccertificadora: idcertificadora
			},
			dataType: "JSON",
		}).done(function(result) {
			$("#cbocertificacion").html(result);
		}).fail(function() {
			objPrincipal.notify('error', 'No se puede autenticar por error = cbocertificacion');
		});
		// Resultado predeterminado
		if (idcertificadora === "10" || idcertificadora === "33" || idcertificadora === "22" || idcertificadora === "22") {
			$('#txtconvaliresul').val(86).trigger('keyup');
			$('#txtconvalimes').val(12);
		}
	};

	/**
	 * Proceso para guardar la convalidación.
	 */
	objRegCtrlProvConvalidar.guardar = function() {
		const botonGuardar = $('#mbtnGconvali');
		const botonCancelar = $('#mbtnCconvali');
		var data = new FormData($("#frmConvalidacion")[0]);
		data.append('cboclieserv', $('#cboclieserv').val());
		$.ajax({
			url: baseurl + "at/ctrlprov/cregctrolprov/setconvalidacion",
			type: 'POST',
			data: data,
			dataType: "JSON",
			async: true,
			contentType: false,
			processData: false,
			beforeSend: function () {
				objPrincipal.botonCargando(botonGuardar);
				botonCancelar.prop('disabled', true);
			}
		}).done(function (res) {
			objPrincipal.notify('success', res.message);
			$('#modalConvalidacion').modal('hide');
			$('#btnBuscar').click();
		}).fail(function(jqxhr) {
			const message = (jqxhr && jqxhr.responseJSON && jqxhr.responseJSON.message)
				? jqxhr.responseJSON.message
				: 'Error en el proceso de ejecución. Vuelva a inentarlo más tarde.';
			objPrincipal.notify('error', message);
		}).always(function() {
			objPrincipal.liberarBoton(botonGuardar);
			botonCancelar.prop('disabled', false);
		});
	};

});

$(document).ready(function() {

	$('#txtconvaliresul').keyup(objRegCtrlProvConvalidar.cambioResultado);

	$('#mbtnGconvali').click(objRegCtrlProvConvalidar.guardar);

	$("#cbocertificadora").change(objRegCtrlProvConvalidar.cambiarCertificadora);

	objPrincipal.addPluginCalendar($('#txtFConva'));

});

fconvalidar = function (cauditoriainspeccion, fservicio, ccriterioresultado) {
	$('#frmConvalidacion').trigger("reset");

	$('#txtconvaliidinsp').val(cauditoriainspeccion);
	$('#txtconvalifservicio').val(fservicio);
	$('#mhdncritresultconvali').val(ccriterioresultado);
	$('#mhdnAccionconvali').val('N');
	$('#cbocertificacion').val(null).change();

	$('#txtcierreFConvalidacion').datetimepicker({
		format: 'DD/MM/YYYY',
		daysOfWeekDisabled: [0],
		locale: 'es',
		autoclose: true,
		todayBtn: true
	});
	fechaActualconval(fservicio);

	// Validar criterio
	if (!ccriterioresultado) {
		objPrincipal.notify('info', 'La inspección no cuenta con un criterio de resultado.');
	}
};

escogerArchivo = function () {
	var archivoInput = document.getElementById('mtxtArchivoconvali');
	var archivoRuta = archivoInput.value;
	var extPermitidas = /(.pdf)$/i;

	var filename = $('#mtxtArchivoconvali').val().replace(/.*(\/|\\)/, '');
	$('#mtxtNomarchconvali').val(filename);

	if (!extPermitidas.exec(archivoRuta)) {
		alert('Asegurese de haber seleccionado un PDF, DOCX, XSLX');
		archivoInput.value = '';
		$('#mtxtNomarchconvali').val('');
		return false;
	}
	$('#sArchivo').val('S');
};
