/*!
 *
 * @version 1.0.0
 */

const objRegCtrolProvContacto = {};

$(function () {

	objRegCtrolProvContacto.lista = function() {
		$.ajax({
			url: BASE_URL + 'at/ctrlprov/regctrolprov/cregcontrolprov_contacto/lista',
			method: 'POST',
			data: {
				cauditoria: $('#mtxtidinsp').val(),
			},
			dataType: 'json',
		}).done(function (resp) {
			let rows = '';
			const data = resp.items;
			if (data && Array.isArray(data)) {
				data.forEach(function(item, posicion) {
					let dapepat = (item.dapepat) ? item.dapepat : '';
					let dapemat = (item.dapemat) ? item.dapemat : '';
					let dnombre = (item.dnombre) ? item.dnombre : '';
					let dcargocontacto = (item.dcargocontacto) ? item.dcargocontacto : '';
					let dtelefono = (item.dtelefono) ? item.dtelefono : '';
					let dmail = (item.dmail) ? item.dmail : '';
					rows += '<tr data-posicion="' + posicion + '" >';
					rows += '<td class="text-left" >' + (posicion + 1) + '</td>';
					rows += '<td class="text-left" >' + item.cliente + '</td>';
					rows += '<td class="text-left" >' + item.DESTABLECIMIENTO + '</td>';
					rows += '<td class="text-left" >' + dapepat + '</td>';
					rows += '<td class="text-left" >' + dapemat + '</td>';
					rows += '<td class="text-left" >' + dnombre + '</td>';
					rows += '<td class="text-left" >' + dcargocontacto + '</td>';
					rows += '<td class="text-left" >' + dtelefono + '</td>';
					rows += '<td class="text-left" >' + dmail + '</td>';
					rows += '<td class="text-left" style="width: 210px" >';
					rows += '<div class="btn-group btn-group-sm" >';
					rows += '<button type="button" role="button" class="btn btn-secondary btn-sm btn-contacto-eliminar" ><i class="fa fa-trash" ></i> Eliminar</button>';
					rows += '<input type="hidden" class="d-none" id="tbl_contacto_id[' + posicion + ']" value="' + item.id + '" />';
					rows += '<input type="hidden" class="d-none" id="tbl_contacto_tipo[' + posicion + ']" value="3" />';
					rows += '</div>';
					rows += '</td>';
					rows += '</tr>';
				});
			}
			$('#tblRegCtrolProvContacto tbody').html(rows);
		}).fail(function (jqxhr) {
			const message = (jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la petición HTTP.';
			objPrincipal.notify('error', message);
		});
	};

	objRegCtrolProvContacto.agregar = function (tipo) {
		const tabla = $('#tblRegCtrolProvContacto > tbody');
		const posicion = tabla.find('tr').length;
		const item = objRegCtrolProvContacto._item(posicion, tipo);
		tabla.append(item);
		objRegCtrolProvContacto.initBusquedaCliente($(document.getElementById('tbl_contacto_ccliente[' + posicion + ']')), posicion);
		$(document.getElementById('tbl_contacto_cestablecimiento[' + posicion + ']')).select2();
		if (tipo === 2) {
			objRegCtrolProvContacto.initBusqueda($(document.getElementById('tbl_contacto_id[' + posicion + ']')));
		}
	};

	objRegCtrolProvContacto._item = function (posicion, tipo, ccontacto, ccliente, dcliente, cestablecimiento, destablecimiento, dapepate, dapemate, dnombre, dcargo, dtelefono, demail) {
		const nro = (posicion + 1);
		ccontacto = (ccontacto && typeof ccontacto !== 'undefined') ? ccontacto : '';
		ccliente = (ccliente && typeof ccliente !== 'undefined') ? ccliente : '';
		dcliente = (dcliente && typeof dcliente !== 'undefined') ? dcliente : '';
		cestablecimiento = (cestablecimiento && typeof cestablecimiento !== 'undefined') ? cestablecimiento : '';
		destablecimiento = (destablecimiento && typeof destablecimiento !== 'undefined') ? destablecimiento : '';
		dapepate = (dapepate && typeof dapepate !== 'undefined') ? dapepate : '';
		dapemate = (dapemate && typeof dapemate !== 'undefined') ? dapemate : '';
		dnombre = (dnombre && typeof dnombre !== 'undefined') ? dnombre : '';
		dcargo = (dcargo && typeof dcargo !== 'undefined') ? dcargo : '';
		dtelefono = (dtelefono && typeof dtelefono !== 'undefined') ? dtelefono : '';
		demail = (demail && typeof demail !== 'undefined') ? demail : '';
		let row = '<tr data-posicion="' + posicion + '" >';
		row += '<td class="text-center" >' + nro + '</td>';
		row += '<td class="text-left" style="width: 320px; min-width: 320px;" >';
		row += '<select class="custom-select" id="tbl_contacto_ccliente[' + posicion + ']" style="width: 100% !important;" >';
		if (ccliente) {
			row += '<option value="' + ccliente + '" >' + dcliente + '</option>';
		}
		row += '</select>';
		row += '</td>';
		row += '<td class="text-left" style="width: 320px; min-width: 320px;" >';
		row += '<select class="custom-select" id="tbl_contacto_cestablecimiento[' + posicion + ']" style="width: 100% !important;" >';
		if (cestablecimiento) {
			row += '<option value="' + cestablecimiento + '" >' + destablecimiento + '</option>';
		}
		row += '</select>';
		row += '</td>';
		if (tipo === 1) {
			row += '<td class="text-left" >';
			row += '<input type="text" class="form-control" id="tbl_contacto_dapepate[' + posicion + ']" value="' + dapepate + '" >';
			row += '</td>';
			row += '<td class="text-left" >';
			row += '<input type="text" class="form-control" id="tbl_contacto_dapemate[' + posicion + ']" value="' + dapemate + '" >';
			row += '</td>';
			row += '<td class="text-left" >';
			row += '<input type="text" class="form-control" id="tbl_contacto_dnombre[' + posicion + ']" value="' + dnombre + '" >';
			row += '</td>';
			row += '<td class="text-left" >';
			row += '<input type="text" class="form-control" id="tbl_contacto_dcargo[' + posicion + ']" value="' + dcargo + '" >';
			row += '</td>';
			row += '<td class="text-left" >';
			row += '<input type="text" class="form-control" id="tbl_contacto_dtelefono[' + posicion + ']" value="' + dtelefono + '" >';
			row += '</td>';
			row += '<td class="text-left" >';
			row += '<input type="text" class="form-control" id="tbl_contacto_demail[' + posicion + ']" value="' + demail + '" >';
			row += '</td>';
		} else {
			row += '<td class="text-left" colspan="6" >';
			row += '<select class="custom-select" id="tbl_contacto_id[' + posicion + ']" style="width: 100% !important;" >';
			if (ccontacto) {
				row += '<option value="' + ccontacto + '" >' + dapepate + ' ' + dapemate + ' ' + dnombre + '</option>';
			}
			row += '</select>';
			row += '</td>';
		}
		row += '<td class="text-left" style="width: 210px" >';
		row += '<div class="btn-group btn-group-sm" >';
		row += '<button type="button" role="button" class="btn btn-success btn-sm mr-2 btn-contacto-guardar" ><i class="fa fa-save" ></i> Guardar</button>';
		row += '<button type="button" role="button" class="btn btn-secondary btn-sm btn-contacto-eliminar" ><i class="fa fa-trash" ></i> Eliminar</button>';
		row += '<input type="hidden" class="d-none" id="tbl_contacto_id[' + posicion + ']" value="' + ccontacto + '" />';
		row += '<input type="hidden" class="d-none" id="tbl_contacto_tipo[' + posicion + ']" value="' + tipo + '" />';
		row += '</div>';
		row += '</td>';
		row += '</tr>';
		return row;
	};

	objRegCtrolProvContacto.buscarEstablecimientos = function (posicion, ccliente) {
		$.ajax({
			url: BASE_URL + 'at/ctrlprov/regctrolprov/cregcontrolprov_contacto/get_establecimientos',
			method: 'POST',
			data: {
				ccliente: ccliente
			},
			dataType: 'json',
			beforeSend: function() {}
		}).done(function(resp) {
			const elEstablecimiento = $(document.getElementById('tbl_contacto_cestablecimiento[' + posicion + ']'));
			if (elEstablecimiento.length > 0 && resp.items && Array.isArray(resp.items)) {
				let establecimientos = '';
				resp.items.forEach(function(item) {
					establecimientos += '<option value="' + item.COD_ESTABLE + '" >' + item.DESCRIPESTABLE +'</option>';
				});
				elEstablecimiento.html(establecimientos);
			}
		});
	};

	objRegCtrolProvContacto.initBusqueda = function (objDOM) {
		objDOM.select2({
			ajax: {
				url: BASE_URL + 'at/ctrlprov/regctrolprov/cregcontrolprov_contacto/autocompletado',
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						search: params.term,
						cauditoria: $('#mtxtidinsp').val(),
					};
				},
				processResults: function (data) {
					return {results: data.items};
				},
				cache: true
			},
			escapeMarkup: function (markup) {
				return markup;
			}, // let our custom formatter work
			minimumInputLength: 0,
			// Aqi se muestran todos los resultados de la busqueda
			templateResult: function (state) {
				const dcargocontacto = (state.dcargocontacto) ? state.dcargocontacto : '';
				const dmail = (state.dmail) ? state.dmail : '';
				const dtelefono = (state.dtelefono) ? state.dtelefono : '';
				let result = '<table class="" >';
				result += '<tr>';
				result += '<td style="width: 330px; padding-right: 5px" >' + state.cliente + '</td>';
				result += '<td style="width: 220px; padding-right: 5px" >' + state.text + '</td>';
				result += '<td style="width: 200px; padding-right: 5px" >' + dcargocontacto + '</td>';
				result += '<td style="width: 150px; padding-right: 5px" >' + dmail + '</td>';
				result += '<td style="width: 200px" >' + dtelefono + '</td>';
				result += '<tr/>';
				result += '<table>';
				return result;
			},
			templateSelection: function (state) {
				if (state.id !== "") {
					const dcargocontacto = (state.dcargocontacto) ? state.dcargocontacto : '';
					const dmail = (state.dmail) ? state.dmail : '';
					const dtelefono = (state.dtelefono) ? state.dtelefono : '';
					let result = '<table class="table-sm" >';
					result += '<tr>';
					result += '<td style="width: 330px; padding-right: 5px; border: none" >' + state.cliente + '</td>';
					result += '<td style="width: 220px; padding-right: 5px; border: none" >' + state.text + '</td>';
					result += '<td style="width: 200px; padding-right: 5px; border: none" >' + dcargocontacto + '</td>';
					result += '<td style="width: 150px; padding-right: 5px; border: none" >' + dmail + '</td>';
					result += '<td style="width: 200px; border: none" >' + dtelefono + '</td>';
					result += '<tr/>';
					result += '<table>';
					return result;
				} else {
					return 'Elegir contacto';
				}
			},
			placeholder: "Elegir contacto",
			allowClear: true,
			width: '100%',
			dropdownParent: null,
		});
	};

	objRegCtrolProvContacto.initBusquedaCliente = function (objDOM, posicion) {
		objDOM.select2({
			ajax: {
				url: BASE_URL + 'at/ctrlprov/inspctrolprov/cresumen_contacto/autocompletado_clientes',
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						search: params.term,
						cauditoria: $('#mtxtidinsp').val(),
					};
				},
				processResults: function (data) {
					return {results: data.items};
				},
				cache: true
			},
			escapeMarkup: function (markup) {
				return markup;
			}, // let our custom formatter work
			minimumInputLength: 0,
			// Aqi se muestran todos los resultados de la busqueda
			templateResult: function (state) {
				return state.text;
			},
			templateSelection: function (state) {
				if (state.id !== "") {
					objRegCtrolProvContacto.buscarEstablecimientos(posicion, state.id);
					return state.text;
				} else {
					return 'Elegir empresa';
				}
			},
			placeholder: "Elegir empresa",
			allowClear: true,
			width: '100%',
			dropdownParent: null,
		});
	};

	/**
	 * Guardar contacto
	 */
	objRegCtrolProvContacto.guardar = function() {
		const boton = $(this);
		const row = boton.parents('tr');
		const posicion = row.data('posicion');

		const id = (document.getElementById('tbl_contacto_id[' + posicion + ']'))
			? document.getElementById('tbl_contacto_id[' + posicion + ']').value
			: '';
		const ccliente = (document.getElementById('tbl_contacto_ccliente[' + posicion + ']'))
			? document.getElementById('tbl_contacto_ccliente[' + posicion + ']').value
			: 0;
		const cestablecimiento = (document.getElementById('tbl_contacto_cestablecimiento[' + posicion + ']'))
			? document.getElementById('tbl_contacto_cestablecimiento[' + posicion + ']').value
			: 0;
		const apepat = (document.getElementById('tbl_contacto_dapepate[' + posicion + ']'))
			? document.getElementById('tbl_contacto_dapepate[' + posicion + ']').value
			: '';
		const apemat = (document.getElementById('tbl_contacto_dapemate[' + posicion + ']'))
			? document.getElementById('tbl_contacto_dapemate[' + posicion + ']').value
			: '';
		const nombres = (document.getElementById('tbl_contacto_dnombre[' + posicion + ']'))
			? document.getElementById('tbl_contacto_dnombre[' + posicion + ']').value
			: '';
		const cargo = (document.getElementById('tbl_contacto_dcargo[' + posicion + ']'))
			? document.getElementById('tbl_contacto_dcargo[' + posicion + ']').value
			: '';
		const email = (document.getElementById('tbl_contacto_demail[' + posicion + ']'))
			? document.getElementById('tbl_contacto_demail[' + posicion + ']').value
			: '';
		const telefonos = (document.getElementById('tbl_contacto_dtelefono[' + posicion + ']'))
			? document.getElementById('tbl_contacto_dtelefono[' + posicion + ']').value
			: '';
		const tipo = (document.getElementById('tbl_contacto_tipo[' + posicion + ']'))
			? document.getElementById('tbl_contacto_tipo[' + posicion + ']').value
			: 1;

		$.ajax({
			url: BASE_URL + 'at/ctrlprov/regctrolprov/cregcontrolprov_contacto/guardar',
			method: 'POST',
			data: {
				cauditoria: $('#mtxtidinsp').val(),
				apepat: apepat,
				apemat: apemat,
				nombres: nombres,
				cargo: cargo,
				email: email,
				telefonos: telefonos,
				ccontacto: id,
				tipo: tipo,
				ccliente: ccliente,
				cestablecimiento: cestablecimiento,
			},
			dataType: 'json',
			beforeSend: function() {
				objPrincipal.botonCargando(boton);
			}
		}).done(function(res) {
			// objResumenContacto.lista();
			objPrincipal.notify('success', res.message);
			document.getElementById('tbl_contacto_id[' + posicion + ']').value = res.data;
		}).fail(function(jqxhr) {
			const message = (jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la petición HTTP.';
			objPrincipal.notify('error', message);
		}).always(function() {
			objPrincipal.liberarBoton(boton);
		});
	};

	/**
	 * Eliminar contacto
	 */
	objRegCtrolProvContacto.eliminar = function() {
		const boton = $(this);
		const row = boton.parents('tr');
		const posicion = row.data('posicion');
		const contact_id = (document.getElementById('tbl_contacto_id[' + posicion + ']'))
			? document.getElementById('tbl_contacto_id[' + posicion + ']').value
			: 0;
		if (contact_id > 0) {
			Swal.fire({
				type: 'warning',
				title: 'Eliminar contacto',
				text: '¿Estas seguro(a) de eliminar?',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Si',
				cancelButtonText: 'Cancelar',
			}).then((result) => {
				if (result.value) {
					$.ajax({
						url: BASE_URL + 'at/ctrlprov/regctrolprov/cregcontrolprov_contacto/eliminar',
						method: 'POST',
						data: {
							cauditoria: $('#mtxtidinsp').val(),
							contact_id: contact_id
						},
						dataType: 'json',
						beforeSend: function () {
							objPrincipal.botonCargando(boton);
						}
					}).done(function (res) {
						row.hide();
						document.getElementById('tbl_contacto_id[' + posicion + ']').value = 0;
						// objResumenContacto.lista();
						objPrincipal.notify('success', res.message);
					}).fail(function (jqxhr) {
						const message = (jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la petición HTTP.';
						objPrincipal.notify('error', message);
					}).always(function() {
						objPrincipal.liberarBoton(boton);
					});
				}
			});
		} else {
			row.hide();
		}
	};

});

$(document).ready(function () {

	$('#btnRegCtrolProvAgregarContacto').click(function () {
		objRegCtrolProvContacto.agregar(1);
	});

	$('#btnRegCtrolProvBuscarContacto').click(function () {
		objRegCtrolProvContacto.agregar(2);
	});

	$(document).on('click', '.btn-contacto-guardar', objRegCtrolProvContacto.guardar);

	$(document).on('click', '.btn-contacto-eliminar', objRegCtrolProvContacto.eliminar);

});
