var otblListctrlprov;
var varfdesde = '%', varfhasta = '%';

$(document).ready(function () {
	$('#tabctrlprov a[href="#tabctrlprov-list-tab"]').attr('class', 'disabled');
	$('#tabctrlprov a[href="#tabctrlprov-det-tab"]').attr('class', 'disabled active');

	$('#tabctrlprov a[href="#tabctrlprov-list-tab"]').not('#store-tab.disabled').click(function (event) {
		$('#tabctrlprov a[href="#tabctrlprov-list"]').attr('class', 'active');
		$('#tabctrlprov a[href="#tabctrlprov-det"]').attr('class', '');
		return true;
	});
	$('#tabctrlprov a[href="#tabctrlprov-det-tab"]').not('#bank-tab.disabled').click(function (event) {
		$('#tabctrlprov a[href="#tabctrlprov-det"]').attr('class', 'active');
		$('#tabctrlprov a[href="#tabctrlprov-list"]').attr('class', '');
		return true;
	});

	$('#tabctrlprov a[href="#tabctrlprov-list"]').click(function (event) {
		return false;
	});
	$('#tabctrlprov a[href="#tabctrlprov-det"]').click(function (event) {
		return false;
	});

	$('#txtFDesde,#txtFHasta').datetimepicker({
		format: 'DD/MM/YYYY',
		daysOfWeekDisabled: [0],
		locale: 'es',
		autoclose: true,
		todayBtn: true
	});
	fechaActual();

	/*LLENADO DE COMBOS*/

	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/cregctrolprov/getcboclieserv",
		dataType: "JSON",
		async: true,
		success: function (result) {
			$('#cboclieserv').html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboclieserv');
		}
	});
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/cregctrolprov/getcboestado",
		dataType: "JSON",
		async: true,
		success: function (result) {
			$('#cboestado').html(result);
			$("#cboestado option[value='']").remove();
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboestado');
		}
	})
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/cregctrolprov/getcboinspector",
		dataType: "JSON",
		async: true,
		data: {
			"sregistro": 'A',
		},
		success: function (result) {
			$('#cboinspector').html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboinspector');
		}
	});

	//
	$('#frmCreactrlprov').validate({
		rules: {
			cboregClie: {
				required: true,
			},
			cboregprovclie: {
				required: true,
			},
			cboregestable: {
				required: true,
			},
			cboregareaclie: {
				required: true,
			},
		},
		messages: {
			cboregClie: {
				required: "Por Favor escoja un Cliente"
			},
			cboregprovclie: {
				required: "Por Favor escoja un Proveedor"
			},
			cboregestable: {
				required: "Por Favor escoja un Establecimiento"
			},
			cboregareaclie: {
				required: "Por Favor escoja un Area"
			},
		},
		errorElement: 'span',
		errorPlacement: function (error, element) {
			error.addClass('invalid-feedback');
			element.closest('.form-group').append(error);
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass('is-invalid');
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).removeClass('is-invalid');
		},
		submitHandler: function (form) {
			const botonEvaluar = $('#mbtnGCreactrl');
			var request = $.ajax({
				url: $('#frmCreactrlprov').attr("action"),
				type: $('#frmCreactrlprov').attr("method"),
				data: $('#frmCreactrlprov').serialize(),
				dataType: 'json',
				error: function () {
					Vtitle = 'Error en Guardar!!!';
					Vtype = 'error';
					sweetalert(Vtitle, Vtype);
					objPrincipal.liberarBoton(botonEvaluar);
				},
				beforeSend: function () {
					objPrincipal.botonCargando(botonEvaluar);
				}
			});
			request.done(function (respuesta) {
				$('#mhdnregIdinsp').val(respuesta[0].cauditoriainspeccion);
				Vtitle = 'Inspección Guardada!!!';
				Vtype = 'success';
				sweetalert(Vtitle, Vtype);
				objPrincipal.liberarBoton(botonEvaluar);
				// cerrar modal
				$('#mbtnCCreactrl').click();
				// Refresca la lista
				// $("#tblListctrlprov").DataTable().ajax.reload(null, false);
			});
			return false;
		}
	});
	$('#frmRegInsp').validate({
		rules: {},
		messages: {},
		errorElement: 'span',
		errorPlacement: function (error, element) {
			error.addClass('invalid-feedback');
			element.closest('.form-group').append(error);
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass('is-invalid');
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).removeClass('is-invalid');
		},
		submitHandler: function (form) {
			const botonEvaluar = $('#btnGrabar');
			var request = $.ajax({
				url: $('#frmRegInsp').attr("action"),
				type: $('#frmRegInsp').attr("method"),
				data: $('#frmRegInsp').serialize(),
				error: function () {
					Vtitle = 'Error en Guardar!!!';
					Vtype = 'error';
					sweetalert(Vtitle, Vtype);
					objPrincipal.liberarBoton(botonEvaluar);
				},
				beforeSend: function () {
					objPrincipal.botonCargando(botonEvaluar);
				}
			});
			request.done(function (respuesta) {
				var posts = JSON.parse(respuesta);

				$.each(posts, function () {
					sweetalert('Inspección Guardada!!!', 'success');
					objPrincipal.liberarBoton(botonEvaluar);
					$('#mfechainsp').val($('#txtFInsp').val());
					$('#mfservicio').val(moment($('#txtFInsp').val(), 'DD/MM/YYYY').format('YYYY-MM-DD'));
					// Solo si se elige en inspector
					if ($('#cboinspinspector').val()) {
						$('#contentActualizar').show();
						$('#txtinspestado').val('Inspector Asignado');
					} else {
						$('#contentActualizar').hide();
						$('#txtinspestado').val('Asignar fecha y/o inspector');
					}
				});
			});
			return false;
		}
	});
	$('#frmPlaninsp').validate({
		rules: {
			mtxtobjeplaninsp: { required: true},
			mtxtalcanplaninsp: { required: true},
			mtxtHplanins: { required: true},
			cbocontacplanins: { required: true},
			mtxtrequeplaninsp: { required: true},
		},
		messages: {},
		errorElement: 'span',
		errorPlacement: function (error, element) {
			error.addClass('invalid-feedback');
			element.closest('.form-group').append(error);
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass('is-invalid');
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).removeClass('is-invalid');
		},
		submitHandler: function (form) {
			const botonEvaluar = $('#mbtnGplaninsp');
			var request = $.ajax({
				url: $('#frmPlaninsp').attr("action"),
				type: $('#frmPlaninsp').attr("method"),
				data: $('#frmPlaninsp').serialize(),
				error: function () {
					Vtitle = 'Error en Guardar!!!';
					Vtype = 'error';
					sweetalert(Vtitle, Vtype);
					objPrincipal.liberarBoton(botonEvaluar);
				},
				beforeSend: function () {
					objPrincipal.botonCargando(botonEvaluar);
				}
			});
			request.done(function (respuesta) {
				var posts = JSON.parse(respuesta);

				$.each(posts, function () {
					Vtitle = 'Plan Creado!!!';
					Vtype = 'success';
					sweetalert(Vtitle, Vtype);
					objPrincipal.liberarBoton(botonEvaluar);
					$('#mbtnCplaninsp').click();
					// Plan de inspección
					$('#mhdnAccionplaninsp').val('E');
					$('#mbtnnewplaninsp').show();
					$('#mbtnverplaninsp').show();
				});
			});
			return false;
		}
	});
	$('#frmCierreespecial').validate({
		rules: {},
		messages: {},
		errorElement: 'span',
		errorPlacement: function (error, element) {
			error.addClass('invalid-feedback');
			element.closest('.form-group').append(error);
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass('is-invalid');
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).removeClass('is-invalid');
		},
		submitHandler: function (form) {
			const botonEvaluar = $('#mbtnGCierreesp');
			var request = $.ajax({
				url: $('#frmCierreespecial').attr("action"),
				type: $('#frmCierreespecial').attr("method"),
				data: $('#frmCierreespecial').serialize(),
				error: function () {
					Vtitle = 'Error en Guardar!!!';
					Vtype = 'error';
					sweetalert(Vtitle, Vtype);
					objPrincipal.liberarBoton(botonEvaluar);
				},
				beforeSend: function () {
					objPrincipal.botonCargando(botonEvaluar);
				}
			});
			request.done(function (respuesta) {
				var posts = JSON.parse(respuesta);

				$.each(posts, function () {
					//$('#mhdnregIdinsp').val(this.cauditoriainspeccion);

					Vtitle = 'Inspección Cerrada!!!';
					Vtype = 'success';
					sweetalert(Vtitle, Vtype);
					objPrincipal.liberarBoton(botonEvaluar);
					$('#mbtnCCierreesp').click();
					$('#btnBuscar').click();
				});
			});
			return false;
		}
	});
	// $('#frmConvalidacion').validate({
	// 	rules: {},
	// 	messages: {},
	// 	errorElement: 'span',
	// 	errorPlacement: function (error, element) {
	// 		error.addClass('invalid-feedback');
	// 		element.closest('.form-group').append(error);
	// 	},
	// 	highlight: function (element, errorClass, validClass) {
	// 		$(element).addClass('is-invalid');
	// 	},
	// 	unhighlight: function (element, errorClass, validClass) {
	// 		$(element).removeClass('is-invalid');
	// 	},
	// 	submitHandler: function (form) {
	// 		const botonEvaluar = $('#mbtnGconvali');
	// 		var request = $.ajax({
	// 			url: $('#frmConvalidacion').attr("action"),
	// 			type: $('#frmConvalidacion').attr("method"),
	// 			data: $('#frmConvalidacion').serialize(),
	// 			error: function () {
	// 				Vtitle = 'Error en Guardar!!!';
	// 				Vtype = 'error';
	// 				sweetalert(Vtitle, Vtype);
	// 				objPrincipal.liberarBoton(botonEvaluar);
	// 			},
	// 			beforeSend: function () {
	// 				objPrincipal.botonCargando(botonEvaluar);
	// 			}
	// 		});
	// 		request.done(function (respuesta) {
	// 			var posts = JSON.parse(respuesta);
	//
	// 			$.each(posts, function () {
	// 				objPrincipal.liberarBoton(botonEvaluar);
	// 				//$('#mhdnIdPropu').val(this.id_propuesta);
	// 				if ($('#sArchivo').val() == 'S') {
	// 					subirArchivoconval();
	// 				} else {
	// 					$('#btnBuscar').click();
	// 					Vtitle = 'Propuesta Guardada!!!';//this.respuesta;
	// 					Vtype = 'success';
	// 					sweetalert(Vtitle, Vtype);
	// 					$('#mbtnCconvali').click();
	// 				}
	// 				$('#sArchivo').val('N');
	// 			});
	// 		});
	// 		return false;
	// 	}
	// });

	$(document).on('click', '.opcion-reapertura', function() {
		const boton = $(this);
		Swal.fire({
			type: 'warning',
			title: 'Re apertura de la inspección',
			text: 'Seguro(a) de abrir la inspección?',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Si',
			cancelButtonText: 'Cancelar',
		}).then((result) => {
			if (result.value) {
				$.ajax({
					url: BASE_URL + 'oi/ctrlprov/inspctrolprov/cresumen/re_apertura',
					method: 'POST',
					data: {
						cauditoria: $('#cauditoria').val(),
						fservicio: $('#fservicio').val(),
					},
					dataType: 'json',
					beforeSend: function () {
						objPrincipal.botonCargando(boton);
					}
				}).done(function (resp) {
					$("#tblListctrlprov").DataTable().ajax.reload(null, false);
				}).fail(function (jqxhr) {
					const message = (jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la petición HTTP.';
					sweetalert(message, 'error');
				}).always(function () {
					objPrincipal.liberarBoton(boton);
				});
			}
		});
	});

	$('#btnActualizar').click(function() {
		const boton = $('#btnActualizar');
		const nuevo_estado_insp = $('#nuevo_estado_insp').val();
		const txtTitle = (nuevo_estado_insp === '029') ? 'Proceso para restablecer a Asignar fecha y/o inspector' : 'Proceso para restablecer a En proceso';
		Swal.fire({
			type: 'warning',
			title: txtTitle,
			text: '¿Seguro(a) deseas continuar?',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Si',
			cancelButtonText: 'Cancelar',
		}).then((result) => {
			if (result.value) {
				$.ajax({
					url: BASE_URL + 'oi/ctrlprov/cregctrolprov/actualizar_reg_inspeccion',
					method: 'POST',
					data: {
						cauditoria: $('#mtxtidinsp').val(),
						fservicio: $('#mfservicio').val(),
						nuevo_estado_insp: nuevo_estado_insp,
					},
					dataType: 'json',
					beforeSend: function () {
						objPrincipal.botonCargando(boton);
						$('#btnRetornarLista').prop('disabled', true);
					}
				}).done(function (resp) {
					// Se actualiza el estado
					$('#contentActualizar').hide();
					if (nuevo_estado_insp == '028') {
						$('#txtinspestado').val('Asignar fecha y/o inspector');
						$('#btnGrabar').show();
					} else {
						$('#txtinspestado').val('En proceso');
						$('#btnGrabar').hide();
					}
					console.log(resp);
				}).fail(function (jqxhr) {
					const message = (jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la petición HTTP.';
					sweetalert(message, 'error');
				}).always(function () {
					objPrincipal.liberarBoton(boton);
					$('#btnRetornarLista').prop('disabled', false);
				});
			}
		});
	});

	$(document).on('click', '.operacion-descargar-acc', function() {
		const boton = $(this);
		objPrincipal.botonCargando(boton);
		$('#acc_cauditoria').val(boton.data('cauditoria'));
		$('#acc_fservicio').val(boton.data('fservicio'));
		const otblAACC = $('#tblAcciónCorrectiva').DataTable({
			"processing"  	: true,
			"bDestroy"    	: true,
			"stateSave"     : true,
			"bJQueryUI"     : true,
			"scrollY"     	: "540px",
			"scrollX"     	: true,
			'AutoWidth'     : true,
			"paging"      	: false,
			"info"        	: true,
			"filter"      	: true,
			"ordering"		: false,
			"responsive"    : false,
			"select"        : true,
			'ajax': {
				"url": BASE_URL + 'oi/ctrlprov/inspctrolprov/caccion_correctiva/descargar_archivo',
				"type": "POST",
				"data": function (d) {
					d.cauditoriainspeccion = boton.data('cauditoria');
					d.fservicio = boton.data('fservicio');
				},
				dataSrc: function (data) {
					objPrincipal.liberarBoton(boton);
					$('#modalAccionCorrectiva').modal('show');
					const formACCArchivo = $('#formACCArchivo');
					if (data.data.path_file) {
						$('#download-excel').show();
						$('#btnDownloadAccionCorrectiva').attr('href', data.data.path_file);
						formACCArchivo.hide();
					} else {
						$('#download-excel').hide();
						formACCArchivo.show();
					}
					return data.data.result;
				},
			},
			'columns': [
				{
					"orderable": false,
					render: function (data, type, row) {
						let texto = '';
						if (row.saceptaraccioncorrectiva !== 'S') {
							texto = '<button class="btn btn-primary opcion-editar-acc" data-tipohallazgo="' + row.tipohallazgo + '" data-crequisitochecklist="' + row.crequisitochecklist + '" data-cchecklist="' + row.cchecklist + '" data-cauditoria="' + row.cauditoriainspeccion + '" data-fservicio="' + row.fservicio + '" ><i class="fa fa-edit" ></i></button>';
						}
						return texto;
					}
				},
				{"orderable": false, data: 'dnumerador', targets: 0},
				{
					"orderable": false,
					render: function (data, type, row) {
						return String(row.drequisito).replace(/(\r\n|\r|\n)/g, '<br />');
					},
					"class": "col-m"
				},
				{"orderable": false, data: 'sexcluyente', targets: 2},
				{"orderable": false, data: 'tipohallazgo', targets: 3},
				{"orderable": false, data: 'dhallazgo', targets: 5},
				{"orderable": false, data: 'daccioncorrectiva', targets: 6},
				{
					"orderable": false,
					render: function (data, type, row) {
						return (row.fcorrectiva)
							? moment(row.fcorrectiva, 'YYY-MM-DD').format('DD/MM/YYYY')
							: '';
					}
				},
				{"orderable": false, data: 'dresponsablecliente', targets: 8},
				{"orderable": false, data: 'saceptaraccioncorrectiva', targets: 9},
				{"orderable": false, data: 'dobservacion', targets: 10},
			],
			"columnDefs": [
				{"targets": [0], "visible": true},
			],
		});
	});

	$('#btnSubirArchivoACC').click(function() {
		const form = $('form#frmFileACC');
		Swal.fire({
			type: 'warning',
			title: 'El proceso de subir el archivo deja sin efecto lo registro manual de la AA.CC.',
			text: '¿Seguro(a) de subir el archivo?',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Si',
			cancelButtonText: 'Cancelar',
		}).then((result) => {
			if (result.value) {
				const datos = new FormData(form[0]);
				datos.append('cauditoria', $('#acc_cauditoria').val());
				datos.append('fservicio', $('#acc_fservicio').val());
				$.ajax({
					url: BASE_URL + 'oi/ctrlprov/inspctrolprov/caccion_correctiva/guardar_archivo',
					method: 'POST',
					data: datos,
					dataType: 'json',
					cache: false,
					contentType: false,
					processData: false,
					beforeSend: function () {
					}
				}).done(function (response) {
					$('#modalAccionCorrectiva').modal('hide');
					sweetalert(response.message, 'success');
				}).fail(function (jqxhr) {
					sweetalert('Error en el proceso de ejecución', 'error');
				}).always(function () {
				});
			}
		});
	});

	$(document).on('click', '.opcion-editar-acc', function() {
		const boton = $(this);
		$.ajax({
			method: 'POST',
			url: baseurl + "oi/ctrlprov/inspctrolprov/caccion_correctiva/buscar",
			dataType: "JSON",
			async: true,
			data: {
				cauditoria: boton.data('cauditoria'),
				fservicio: boton.data('fservicio'),
				cchecklist: boton.data('cchecklist'),
				crequisitochecklist: boton.data('crequisitochecklist'),
			},
			beforeSend: function() {
				if (boton) {
					objPrincipal.botonCargando(boton);
				}
			}
		}).done(function(resp) {
			$('#acc_modal_cchecklist_id').val(boton.data('cchecklist'));
			$('#acc_modal_requisito_cchecklist_id').val(boton.data('crequisitochecklist'));
			$('#acc_modal_requisito').val(resp.drequisito);
			$('#acc_modal_hallazgo').val(resp.dhallazgo);
			$('#acc_modal_acc').val(resp.daccioncorrectiva);
			$('#acc_modal_observaciones').val(resp.dobservacion);
			$('#acc_modal_acepta_acc').prop('checked', resp.saceptaraccioncorrectiva.toUpperCase() === "S");
			$('#acc_modal_tipo_hallazgo').val(boton.data('tipohallazgo'));
			$('#acc_modal_responsable').val(resp.dresponsablecliente);
			if (resp.fcorrectiva) {
				let fecha = moment(resp.fcorrectiva, 'YYYY-MM-DD').format('DD/MM/YYYY');
				$('#acc_modal_fecha').val(fecha);
			}
			$('#ModalAccionCorrectiva').modal('show');
		}).fail(function() {
			objPrincipal.notify('error', 'Error al cargar la acción correctiva.')
		}).always(function() {
			if (boton) {
				objPrincipal.liberarBoton(boton);
			}
		});
	});

	$('#verDirecciones').click(function() {
		$('#destab_maq').toggle();
		$('#destab_prov').toggle();
	});

	$("#cboregareaclie").change(function() {
		const el = $('#cboregareaclie option:selected');
		let adiv = el.data('adiv');
		adiv = (adiv) ? adiv : '';
		$('#cboareadiv').val(adiv);
	});
	
});

fechaActual = function () {
	var fecha = new Date();
	var fechatring = ("0" + fecha.getDate()).slice(-2) + "/" + ("0" + (fecha.getMonth() + 1)).slice(-2) + "/" + fecha.getFullYear();

	$('#txtFDesde').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY'));
	$('#txtFHasta').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY'));

};

// $('#txtFDesde').on('change.datetimepicker', function (e) {
//
// 	$('#txtFHasta').datetimepicker({
// 		format: 'DD/MM/YYYY',
// 		daysOfWeekDisabled: [0],
// 		locale: 'es'
// 	});
//
// 	var fecha = moment(e.date).format('DD/MM/YYYY');
//
// 	$('#txtFHasta').datetimepicker('minDate', fecha);
// 	$('#txtFHasta').datetimepicker('date', fecha);
// });

$("#chkFreg").on("change", function () {
	if ($("#chkFreg").is(":checked") == true) {
		$("#txtFIni").prop("disabled", false);
		$("#txtFFin").prop("disabled", false);

		varfdesde = '';
		varfhasta = '';
	} else if ($("#chkFreg").is(":checked") == false) {
		$("#txtFIni").prop("disabled", true);
		$("#txtFFin").prop("disabled", true);

		varfdesde = '%';
		varfhasta = '%';
	}
});

$("#cboclieserv").change(function () {

	var select = document.getElementById("cboclieserv"), //El <select>
		value = select.value, //El valor seleccionado
		valor = select.valor,
		text = select.options[select.selectedIndex].innerText;
	document.querySelector('#lblCliente').innerText = text;

	/*var v_cboclieserv = $('#cboclieserv').val();
	var params = { "ccliente":v_cboclieserv };
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl+"oi/ctrlprov/cregctrolprov/getcboprovxclie",
		dataType: "JSON",
		async: true,
		data: params,
		success:function(result){
			$("#cboprovxclie").html(result);
			$('#cboprovxclie').val('').trigger("change");
		},
		error: function(){
			alert('Error, No se puede autenticar por error = cboprovxclie');
		}
	});

	if(v_cboclieserv != 0){
		$("#btnNuevo").prop("disabled",false);
	}else{
		$("#btnNuevo").prop("disabled",true);
	}*/
});

$("#btnBuscar").click(function () {
	let fdesde = '%';
	let fhasta = '%';
	if ($('#chkFreg').is(':checked')) {
		fdesde = $('#txtFIni').val();
		fhasta = $('#txtFFin').val();
	}
	let filtroPeligro = '';
	if ($('#filtro_peligro_1').is(':checked')) {
		filtroPeligro = 'S';
	}
	if ($('#filtro_peligro_2').is(':checked')) {
		filtroPeligro = 'N';
	}
	var groupColumn = 0;
	const boton = $("#btnBuscar");
	objPrincipal.botonCargando(boton);
	otblListctrlprov = $('#tblListctrlprov').DataTable({
		"processing": true,
		"bDestroy": true,
		"stateSave": true,
		"bJQueryUI": true,
		"scrollY": "700px",
		"scrollX": true,
		'AutoWidth': true,
		"paging": false,
		"info": true,
		"filter": false,
		"ordering": false,
		"responsive": false,
		"select": true,
		"dom": 'lfTrtip',
		'ajax': {
			"url": baseurl + "oi/ctrlprov/cregctrolprov/getbuscarctrlprov/",
			"type": "POST",
			"data": function (d) {
				d.ccliente = $('#cboclieserv').val();
				d.fdesde = fdesde;
				d.fhasta = fhasta;
				d.dclienteprovmaq = $('#txtprovmaq').val();
				d.inspector = $('#cboinspector').val();
				d.cboestado = $('#cboestado').val();
				d.codigo = $('#txtcodigo').val();
				d.speligro = filtroPeligro;
			},
			dataSrc: function (data) {
				objPrincipal.liberarBoton(boton);
				return data;
			},
		},
		'columns': [
			{data: 'desc_gral', targets: 0, "visible": false},
			{data: 'areacli', targets: 1, "visible": false},
			{data: 'lineaproc', targets: 2, "visible": false},
			{
				"orderable": false, "class": "col-xxs",
				render: function (data, type, row) {
					let desc_gral = (row.desc_gral) ? String(row.desc_gral).replace(/['"]+/g, '') : '';
					let destab_prov = (row.dir_prov) ? row.dir_prov : '';
					let destab_maq = (row.dir_maq) ? row.dir_maq : '';
					let opciones = '<div class="dropdown" style="text-align: center;">';
					opciones += '<a  data-toggle="dropdown" href="#"><span class="fas fa-bars"></span></a>';
					opciones += '<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">';
					if (row.zctipoestadoservicio == '028') {
						opciones += '<li><a title="Programar" style="cursor:pointer; color:#3c763d;" onClick="javascript:fprogramar(\'' + row.cauditoriainspeccion + '\',\'' + desc_gral + '\',\'' + row.areacli + '\',\'' + row.lineaproc + '\',\'' + row.cusuarioconsultor + '\',\'' + row.periodo + '\',\'' + row.finspeccion + '\',\'' + row.zctipoestadoservicio + '\',\'' + row.destado + '\',\'' + row.ccliente + '\',\'' + row.spermitircorrectivas + '\',\'' + row.sincluyeplan + '\',\'' + row.zctiposervicio + '\',\'' + row.cproveedorcliente + '\',\'' + row.cmaquiladorcliente + '\',\'' + row.dcliente + '\',\'' + row.splan + '\',\'' + row.cnorma + '\',\'' + row.csubnorma + '\',\'' + row.cchecklist + '\',\'' + row.cmodeloinforme + '\',\'' + row.cformulaevaluacion + '\',\'' + row.ccriterioresultado + '\',\'' + row.cvalornoconformidad + '\',\'' + destab_prov + '\',\'' + destab_maq + '\',\'' + row.fservicio + '\');"><span class="far fa-calendar-alt" aria-hidden="true">&nbsp;</span>&nbsp;Programar</a></li>';
						opciones += '<li><a data-toggle="modal" title="Covalidar" style="cursor:pointer; color:#3c763d;" data-target="#modalConvalidacion" onClick="javascript:fconvalidar(\'' + row.cauditoriainspeccion + '\',\'' + row.finspeccion + '\',\'' + row.ccriterioresultado + '\');"><span class="fas fa-file-signature" aria-hidden="true">&nbsp;</span>&nbsp;Convalidar</a></li>';
						opciones += '<li><a data-toggle="modal" title="Cierre" style="cursor:pointer; color:#3c763d;" data-target="#modalCierreespecial" onClick="javascript:fcierreespecial(\'' + row.cauditoriainspeccion + '\',\'' + row.finspeccion + '\',\'' + row.zctipoestadoservicio + '\',\'' + row.destado + '\',\'' + row.ccliente + '\');"><span class="far fa-window-close" aria-hidden="true">&nbsp;</span>&nbsp;Cierres Especiales</a></li>';
					} else if (row.valorestado == '0' && row.sultimo == 'S') {
						opciones += '<li><button type="button" role="button" class="btn btn-link opcion-reapertura" data-cauditoria="' + row.cauditoriainspeccion + '" data-fservicio="' + row.finspeccion + '" title="Reaperturar" style="cursor:pointer; color:#3c763d;"><span class="far fa-folder-open" aria-hidden="true"> </span>&nbsp;Reaperturar</button></li>';
					}
					opciones += '<li><a href="' + BASE_URL + 'oi/ctrlprov/inspctrolprov/cinspeccion/editar?cauditoria=' + row.cauditoriainspeccion + '&fservicio=' + row.fservicio + '" target="_blank" title="Ver Inspección" style="cursor:pointer; color:#3c763d;"><span class="far fa-folder-open" aria-hidden="true"> </span>&nbsp;Ver Inspección</a></li>';
					opciones +='</ul>';
					opciones += '</div>';
					return opciones;
				}
			},
			{data: 'periodo', "class": "col-s"},
			{data: 'destado', "class": "col-s"},
			{
				"orderable": false, "class": "col-s",
				render: function (data, type, row) {
					let vfechaip = '';
					if (row.finspeccion !== '01/01/1900') {
						vfechaip = row.finspeccion;
					}
					return '<div class="text-left" style="width: 80px">' + vfechaip + '</div>'
				}
			},
			{
				"orderable": false,
				render: function(data, type, row) {
					let dinforme = row.dinformefinal;
					// Solo cuando es convalidado
					if (row.DUBICACIONFILESERVERCONVA && row.zctipoestadoservicio == '519') {
						dinforme = '<a href="' + baseurl + 'FTPfileserver/Archivos/' + row.DUBICACIONFILESERVERCONVA + '" target="_blank" class="btn btn-link p-0" >Archivo Conva.</a>';
					} else {
						if (row.DUBICACIONFILESERVERPDF) {
							dinforme = '<a href="' + baseurl + 'FTPfileserver/Archivos/' + row.DUBICACIONFILESERVERPDF + '" target="_blank" class="btn btn-link p-0" >'
								+ row.dinformefinal +
								'</a>';
						}
					}
					return '<div class="text-left" style="width: 115px" >' + dinforme + '</div>';
				}
			},
			{
				"orderable": false,
				render: function(data, type, row) {
					const EMPRESAINSPECTORA = (row.EMPRESAINSPECTORA)
						? ' - ' + String(row.EMPRESAINSPECTORA).toUpperCase().trim()
						: '';
					return row.resultado + EMPRESAINSPECTORA;
				}
			},
			{
				"orderable": false,
				render: function(data, type, row) {
					return '<div class="text-left" style="width: 130px" >' + row.inspector + '</div>';
				}
			},
			{
				"orderable": false, // Se edito el procedimiento usp_oi_ctrlprov_getbuscarctrlprov
				"class": "col-xs",
				render: function (data, type, row) {
					const checked = (row.finalizado === 'S') ? 'checked' : '';
					return '<div class="text-center" >' +
						'<div class="custom-control custom-checkbox">' +
						'<input type="checkbox" class="custom-control-input" disabled id="fina-' + row.periodo + '" ' + checked + ' >' +
						'<label class="custom-control-label" for="customCheck1"></label>' +
						'</div>' +
						'</div>';
				}
			},
			{
				"orderable": false, // Se edito el procedimiento usp_oi_ctrlprov_getbuscarctrlprov
				render: function (data, type, row) {
					const checked = (row.peligros === 'S') ? 'checked' : '';
					return '<div class="text-center" >' +
						'<div class="custom-control custom-checkbox">' +
						'<input type="checkbox" class="custom-control-input" disabled id="peli-' + row.periodo + '" ' + checked + ' >' +
						'<label class="custom-control-label" for="customCheck1"></label>' +
						'</div>' +
						'</div>';
				}
			},
			{
				"orderable": false, // Se edito el procedimiento usp_oi_ctrlprov_getbuscarctrlprov
				render: function (data, type, row) {
					const checked = (row.queja === 'S') ? 'checked' : '';
					let boton =
						'<div class="custom-control custom-checkbox">' +
						'<input type="checkbox" class="custom-control-input" disabled id="que-' + row.periodo + '" ' + checked + ' >' +
						'<label class="custom-control-label" for="customCheck1"></label>' +
						'</div>';
					if (row.DUBICACIONFILESERVERQUEJA) {
						boton += '<a class="btn btn-link btn-sm" href="' + BASE_URL + 'FTPfileserver/Archivos/' + row.DUBICACIONFILESERVERQUEJA + '" target="_blank" >Descargar</a>';
					}
					return '<div class="text-center" >' + boton + '</div>';
				}
			},
			{
				"orderable": false, // Se edito el procedimiento usp_oi_ctrlprov_getbuscarctrlprov
				render: function (data, type, row) {
					const checked = (row.accion_cor === 'S') ? 'checked' : '';
					let boton =
						'<div class="custom-control custom-checkbox">' +
						'<input type="checkbox" class="custom-control-input" disabled id="AA-CC-' + row.periodo + '" ' + checked + ' >' +
						'<label class="custom-control-label" for="customCheck1"></label>' +
						'</div>';
					const txtAACC = (checked) ? 'Descargar' : 'Llenar AACC';
					if (row.totalAC > 0) {
						boton += '<button type="button" class="btn btn-link btn-sm operacion-descargar-acc" data-cauditoria="' + row.cauditoriainspeccion + '" data-fservicio="' + row.fservicio + '" >' + txtAACC + '</button>';
					}
					return '<div class="text-center" >' + boton + '</div>';
				}
			},
			{
				responsivePriority: 1, "orderable": false, "class": "col-s",
				render: function (data, type, row) {
					let desc_gral = (row.desc_gral) ? String(row.desc_gral).replace(/['"]+/g, '') : '';
					if (row.zctipoestadoservicio != '028') {
						let destab_prov = (row.dir_prov) ? row.dir_prov : '';
						let destab_maq = (row.dir_maq) ? row.dir_maq : '';
						return '<div class="text-center" >' +
							'<a title="Registro" style="cursor:pointer; color:#3c763d;" onClick="javascript:selInspe(\'' + row.cauditoriainspeccion + '\',\'' + desc_gral + '\',\'' + row.areacli + '\',\'' + row.lineaproc + '\',\'' + row.cusuarioconsultor + '\',\'' + row.periodo + '\',\'' + row.finspeccion + '\',\'' + row.cnorma + '\',\'' + row.csubnorma + '\',\'' + row.cchecklist + '\',\'' + row.cmodeloinforme + '\',\'' + row.cvalornoconformidad + '\',\'' + row.cformulaevaluacion + '\',\'' + row.ccriterioresultado + '\',\'' + row.dcomentario + '\',\'' + row.zctipoestadoservicio + '\',\'' + row.destado + '\',\'' + row.ccliente + '\',\'' + row.spermitircorrectivas + '\',\'' + row.sincluyeplan + '\',\'' + row.zctiposervicio + '\',\'' + row.splan + '\',\'' + row.cproveedorcliente + '\',\'' + row.cmaquiladorcliente + '\',\'' + destab_prov + '\',\'' + destab_maq + '\',\'' + row.fservicio + '\');">' +
							'<span class="fas fa-eye fa-2x" aria-hidden="true"></span>' +
							'</a>';
					} else {
						return '';
					}
				}
			},
			{data: 'espeligro'},
		],
		"columnDefs": [
			{"targets": [0], "visible": false},
		],
		"rowCallback": function (row, data) {
			/*if(data.espeligro == "S"){
				$(row).css("color","red");
			}*/
		},
		"drawCallback": function (settings) {
			var api = this.api();
			var rows = api.rows({page: 'current'}).nodes();
			var last = null;
			var grupo, grupo01;

			api.column([0], {}).data().each(function (ctra, i) {
				grupo = api.column(1).data()[i];
				grupo01 = api.column(2).data()[i];

				var speligro = api.column(15).data()[i];
				const varcolor = (speligro === "S") ? '#FF0000' : '#000000';
				if (last !== ctra) {
					$(rows).eq(i).before(
						'<tr class="group"><td colspan="15" class="subgroup">' + ctra.toUpperCase() + '</td></tr>' +
						'<tr class="group"><td colspan="4">Area : ' + grupo + '</td><td colspan="10" style="color:' + varcolor + '">Linea : ' + grupo01 + '</td></tr>'
					);
					last = ctra;
				}
			});
		}
	});
	otblListctrlprov.column(1).visible(false);
	otblListctrlprov.column(2).visible(false);
	otblListctrlprov.column(15).visible(false);


});

$('#tblListctrlprov tbody').on('dblclick', 'tr.group', function () {
	var rowsCollapse = $(this).nextUntil('.group');
	var id = $(this).find("td.subgroup:first-child").text().substr(1, 8);

	verInspeccion(id);
});

$('#tblListctrlprov tbody').on('click', 'tr.group', function () {
	var rowsCollapse = $(this).nextUntil('.group');
	$(rowsCollapse).toggleClass('hidden');
});

$('#btn-show-all-children').on('click', function () {
	otblListctrlprov.rows().every(function () {
		var rowsCollapse = $('.group').nextUntil('.group');
		$(rowsCollapse).removeClass('hidden');
	});
});

$('#btn-hide-all-children').on('click', function () {
	otblListctrlprov.rows().every(function () {
		var rowsCollapse = $('.group').nextUntil('.group');
		$(rowsCollapse).removeClass('hidden');
		$(rowsCollapse).toggleClass('hidden');
	});
});

$("#btnNuevo").click(function () {
	$("#modalCreactrlprov").modal('show');

	$('#frmCreactrlprov').trigger("reset");
	$('#mhdnAccionctrlprov').val('N');

	var vnfecha = new Date();
	var vnano = vnfecha.getFullYear();
	var vnmes = ('00' + vnfecha.getMonth());
	$('#mtxtregPeriodo').val(vnano + '-' + vnmes.substr(-2));

	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/cregctrolprov/getcboclieserv",
		dataType: "JSON",
		async: true,
		success: function (result) {
			$('#cboregClie').html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboregClie');
		}
	});

	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/cregctrolprov/getcbotipoestable",
		dataType: "JSON",
		async: true,
		success: function (result) {
			$('#cbotipoestable').html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cbotipoestable');
		}
	});

});

$("#cboregClie").change(function () {
	var v_cboregClie = $('#cboregClie').val();
	var params = {"ccliente": v_cboregClie};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/cregctrolprov/getcboprovxclie",
		dataType: "JSON",
		async: true,
		data: params,
		success: function (result) {
			$("#cboregprovclie").html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboregprovclie');
		}
	});
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/cregctrolprov/getcboareaclie",
		dataType: "JSON",
		async: true,
		data: params,
		success: function (result) {
			$("#cboregareaclie").html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboregareaclie');
		}
	});
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/cregctrolprov/getcbocartafirmante",
		dataType: "JSON",
		async: true,
		data: params,
		success: function (result) {
			$("#cbocartafirmante").html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboregareaclie');
		}
	});
});

$("#cboregprovclie").change(function () {

	var v_cboclie = $('#cboregClie').val();
	var v_cboprov = $('#cboregprovclie').val();

	var params = {"cproveedor": v_cboprov};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/cregctrolprov/getcbomaqxprov",
		dataType: "JSON",
		async: true,
		data: params,
		success: function (result) {
			$("#cboregmaquiprov").html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboregmaquiprov');
		}
	});
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/cregctrolprov/getcbocontacprinc",
		dataType: "JSON",
		async: true,
		data: params,
		success: function (result) {
			$("#cbocontacprinc").html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cbocontacprinc');
		}
	});
	// $.ajax({
	// 	type: 'ajax',
	// 	method: 'post',
	// 	url: baseurl + "oi/ctrlprov/cregctrolprov/getcbocartadestinatario",
	// 	dataType: "JSON",
	// 	async: true,
	// 	data: params,
	// 	success: function (result) {
	// 		$("#cbocartadestinatario").html(result);
	// 	},
	// 	error: function () {
	// 		alert('Error, No se puede autenticar por error = cbocontacprinc');
	// 	}
	// });
	cboEstablecimiento(v_cboclie, v_cboprov, '', 'P');
	$('#mtxtregdirestable').val('');
});

$("#cboregmaquiprov").change(function () {

	var v_cboclie = $('#cboregClie').val();
	var v_cboprov = $('#cboregprovclie').val();
	var v_cbomaqui = $('#cboregmaquiprov').val();
	var v_tipo = 'M'

	if (v_cbomaqui == '') {
		v_tipo = 'P'
	}

	cboEstablecimiento(v_cboclie, v_cboprov, v_cbomaqui, v_tipo);
	$('#mtxtregdirestable').val('');
});

$("#cboregestable").change(function () {
	var v_cboclie = $('#cboregClie').val();
	var v_cboestable = $('#cboregestable').val();

	var params = {
		"cestablecimiento": v_cboestable
	};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/cregctrolprov/getcbolineaproc",
		dataType: "JSON",
		async: true,
		data: params,
		success: function (result) {
			$("#cboreglineaproc").html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboreglineaproc');
		}
	});

	var parametros = {
		"cestablecimiento": v_cboestable
	};
	var request = $.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/cregctrolprov/getdirestable",
		dataType: "JSON",
		async: true,
		data: parametros,
		error: function () {
			alert('Error, No se puede autenticar por error = getdirestable');
		}
	});
	request.done(function (respuesta) {
		$.each(respuesta, function () {
			$('#mtxtregdirestable').val(this.DIRESTABLE);
		});
	});
});

$("#cbotipoestable").change(function () {

	var v_cbotipoestable = $('#cbotipoestable').val();

	var params = {"ctipoestable": v_cbotipoestable};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/cregctrolprov/getmontotipoestable",
		dataType: "JSON",
		async: true,
		data: params,
		success: function (result) {
			$('#txtcostoestable').val(result);
		},
		error: function () {
			alert('Error, No se puede recuperar Monto del Costo');
		}
	});
});

cboEstablecimiento = function (cboclie, cboprov, cbomaqui, vtipo) {

	var params = {
		"ccliente": cboclie,
		"cproveedor": cboprov,
		"cmaquilador": cbomaqui,
		"tipo": vtipo

	};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/cregctrolprov/getcboregestable",
		dataType: "JSON",
		async: true,
		data: params,
		success: function (result) {
			$("#cboregestable").html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = getcboregestable');
		}
	});
};

$('#mbtnCCreactrl').click(function () {
	$('#btnBuscar').click();
});

iniInspe = function (cusuarioconsultor, consultsreg, cnorma, csubnorma, cchecklist, ccliente, cvalornoconformidad, cmodeloinforme, cformulaevaluacion, ccriterioresultado, regPlanInsp) {
	var params = {"sregistro": consultsreg};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/cregctrolprov/getcboinspector",
		dataType: "JSON",
		async: true,
		data: params,
		success: function (result) {
			$('#cboinspinspector').html(result);
			$('#cboinspinspector').val(cusuarioconsultor).trigger("change");
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboinspinspector');
		}
	});
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/cregctrolprov/getcbosistemaip",
		dataType: "JSON",
		async: true,
		success: function (result) {
			$('#cboinspsistema').html(result);
			//$('#cboinspsistema').val(cnorma).trigger("change");
			document.getElementById("cboinspsistema").value = cnorma;
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboinspsistema');
		}
	});
	var paramsrubro = {"cnorma": cnorma};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/cregctrolprov/getcborubroip",
		dataType: "JSON",
		async: true,
		data: paramsrubro,
		success: function (result) {
			$('#cboinsprubro').html(result);
			document.getElementById("cboinsprubro").value = csubnorma;
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboinsprubro');
		}
	});


	var paramschecklist = {
		"cnorma": cnorma,
		"csubnorma": csubnorma,
		"ccliente": ccliente,
	};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/cregctrolprov/getcbochecklist",
		dataType: "JSON",
		async: true,
		data: paramschecklist,
		success: function (result) {
			$('#cboinspcchecklist').html(result);
			document.getElementById("cboinspcchecklist").value = cchecklist;
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboinspcchecklist');
		}
	});
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/cregctrolprov/getcbomodinforme",
		dataType: "JSON",
		async: true,
		success: function (result) {
			$('#cboinspmodeloinfo').html(result);
			$('#cboinspmodeloinfo').val(cmodeloinforme).trigger("change");
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboinspmodeloinfo');
		}
	});
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/cregctrolprov/getcboinspvalconf",
		dataType: "JSON",
		async: true,
		success: function (result) {
			$('#cboinspvalconf').html(result);
			$('#cboinspvalconf').val(cvalornoconformidad).trigger("change");
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboinspvalconf');
		}
	});
	var paramsform = {"cchecklist": cchecklist};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/cregctrolprov/getcboinspformula",
		dataType: "JSON",
		async: true,
		data: paramsform,
		success: function (result) {
			$('#cboinspformula').html(result);
			$('#cboinspformula').val(cformulaevaluacion).trigger("change");
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboinspformula');
		}
	});
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/cregctrolprov/getcboinspcritresul",
		dataType: "JSON",
		async: true,
		success: function (result) {
			$('#cboinspcritresul').html(result);
			$('#cboinspcritresul').val(ccriterioresultado).trigger("change");
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboinspcritresul');
		}
	});
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/cregctrolprov/getcostos",
		data: {
			cauditoria: $('#mtxtidinsp').val(),
		},
		dataType: "JSON",
		async: true,
		success: function (result) {
			if (result) {
				$('#insp_tipo_estable_costo').html('<option value="' + result.CTIPOESTABLECIMIENTO + '" >' + result.DTIPOESTABLECIMIENTO + '</option>');
				$('#insp_costo').val(result.costo);
			} else {
				$('#insp_tipo_estable_costo').html('');
				$('#insp_costo').val(0.00);
			}
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboinspcritresul');
		}
	});
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/regctrolprov/cregcontrolprov_contacto/lista_cartas",
		data: {
			cauditoria: $('#mtxtidinsp').val(),
		},
		dataType: "JSON",
		async: true,
		success: function (result) {
			let rows = '';
			if (result && result.items) {
				result.items.forEach(function(item, pos) {
					const textoRol = (item.SACCIONCONTACTO == 'E') ? 'CONTACTO DESTINO' : 'FIRMANTE';
					const dmail = (item.dmail) ? item.dmail : '';
					const dtelefono = (item.dtelefono) ? item.dtelefono : '';
					rows += '<tr>';
					rows += '<td class="text-left" >' + (pos + 1) + '</td>';
					rows += '<td class="text-left" >' + textoRol + '</td>';
					rows += '<td class="text-left" >' + item.dapemat + ' ' + item.dapepat + ' ' + item.dnombre + '</td>';
					rows += '<td class="text-left" >' + item.dcargocontacto + '</td>';
					rows += '<td class="text-left" >' + item.DESTABLECIMIENTO + '</td>';
					rows += '<td class="text-left" >' + dtelefono + '</td>';
					rows += '<td class="text-left" >' + dmail + '</td>';
					rows += '<td class="text-left" >' + item.cliente + '</td>';
					rows += '<td class="text-left" ><button class="btn btn-danger" ><i class="fa fa-edit" ></i> Cambiar</button></td>';
					rows += '</tr>';
				});
			}
			$('#tblRegCtrolProvContactoCarta tbody').html(rows);
		},
		error: function () {
			alert('Error, No se puede autenticar por error cartas');
		}
	});

	// N: NUEVO, E: EDITAR el plan de inspección
	$('#mhdnAccionplaninsp').val(regPlanInsp);
};

quitarFecha = function () {
	$('#txtFInsp').val('Sin Fecha');
};

$("#cboinspsistema").change(function () {
	var v_cnorma = $("#cboinspsistema option:selected").attr("value");

	var paramsrubro = {"cnorma": v_cnorma};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/cregctrolprov/getcborubroip",
		dataType: "JSON",
		async: true,
		data: paramsrubro,
		success: function (result) {
			$('#cboinsprubro').html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboinsprubro');
		}
	});
});

$("#cboinsprubro").change(function () {
	var v_csubnorma = $("#cboinsprubro option:selected").attr("value");
	var v_cnorma = $('#cboinspsistema').val();
	var v_ccliente = $('#mhdnccliente').val();
	var paramschecklist = {
		"cnorma": v_cnorma,
		"csubnorma": v_csubnorma,
		"ccliente": v_ccliente,
	};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/cregctrolprov/getcbochecklist",
		dataType: "JSON",
		async: true,
		data: paramschecklist,
		success: function (result) {
			$('#cboinspcchecklist').html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboinspcchecklist');
		}
	});
});

$("#cboinspcchecklist").change(function () {
	var v_cchecklist = $("#cboinspcchecklist option:selected").attr("value");
	var paramsform = {"cchecklist": v_cchecklist};
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/cregctrolprov/getcboinspformula",
		dataType: "JSON",
		async: true,
		data: paramsform,
		success: function (result) {
			$('#cboinspformula').html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cboinspformula');
		}
	});
});

selInspe = function (cauditoriainspeccion, desc_gral, areacli, lineaproc, cusuarioconsultor, periodo, fservicio, cnorma, csubnorma, cchecklist, cmodeloinforme, cvalornoconformidad, cformulaevaluacion, ccriterioresultado, dcomentario, zctipoestadoservicio, destado, ccliente, spermitircorrectivas, sincluyeplan, zctiposervicio, splan, cproveedorcliente, cmaquilador, destab_prov, destab_maq, fechaservicio) {
	$('#tabctrlprov a[href="#tabctrlprov-det"]').tab('show');

	$('#frmRegInsp').trigger("reset");

	$('#mtxtidinsp').val(cauditoriainspeccion);
	$('#mfechainsp').val(fservicio);
	$('#mfservicio').val(fechaservicio);
	$('#mhdnAccioninsp').val('A');
	$('#mhdnccliente').val(ccliente);
	$('#mhdncproveedor').val(cproveedorcliente);
	$('#mhdncmaquilador').val(cmaquilador);

	$('#destab_prov').val('DIR. PROVEEDOR: ' + destab_prov);
	$('#destab_maq').val('DIR. MAQUILADOR: ' + destab_maq);

	let refPlanInsp = 'N';
	if (splan == 0) {
		refPlanInsp = 'N';
		$('#mbtnnewplaninsp').show();
		$('#mbtnverplaninsp').hide();
	} else {
		refPlanInsp = 'E';
		$('#mbtnnewplaninsp').show();
		$('#mbtnverplaninsp').show();
	}

	iniInspe(cusuarioconsultor, 'A', cnorma, csubnorma, cchecklist, ccliente, cvalornoconformidad, cmodeloinforme, cformulaevaluacion, ccriterioresultado, refPlanInsp);

	$('#btnGrabar').hide();

	$('#mtxtinspdatos').val(desc_gral);
	$('#mtxtinsparea').val('AREÁ : ' + areacli);
	$('#mtxtinsplinea').val('LINEA : ' + lineaproc);

	$('#cboinspperiodo').val(periodo);
	$('#mtxtinspcoment').val(dcomentario);
	$('#mhdnzctipoestado').val(zctipoestadoservicio);
	$('#txtinspestado').val(destado);

	$('#txtFInspeccion').datetimepicker({
		format: 'DD/MM/YYYY',
		daysOfWeekDisabled: [0],
		locale: 'es',
		autoclose: true,
		todayBtn: true
	});
	if (fservicio == '01/01/1900') {
		$('#txtFInsp').val('Sin Fecha');
	} else {
		$('#txtFInsp').val(fservicio);
	}

	objRegCtrolProvContacto.lista();

	$('#contentActualizar').show();
};
fechaActualinsp = function (fservicio) {
	var fecha = new Date();
	var fechatring = ("0" + fecha.getDate()).slice(-2) + "/" + ("0" + (fecha.getMonth() + 1)).slice(-2) + "/" + fecha.getFullYear();
	$('#txtFInspeccion').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY'));

};

fprogramar = function (cauditoriainspeccion, desc_gral, areacli, lineaproc, cusuarioconsultor, periodo, fservicio, zctipoestadoservicio, destado, ccliente, spermitircorrectivas, sincluyeplan, zctiposervicio, cproveedor, cmaquilador, dcliente, splan, cnorma, csubnorma, cchecklist, cmodeloinforme, cformulaevaluacion, ccriterioresultado, cvalornoconformidad, destab_prov, destab_maq, fechaservicio) {
	$('#tabctrlprov a[href="#tabctrlprov-det"]').tab('show');

	$('#frmRegInsp').trigger("reset");

	$('#destab_prov').val('DIR. PROVEEDOR: ' + destab_prov);
	$('#destab_maq').val('DIR. MAQUILADOR: ' + destab_maq);

	$('#mtxtidinsp').val(cauditoriainspeccion);
	$('#mfechainsp').val(fservicio);
	$('#mfservicio').val(fechaservicio);
	$('#mhdnAccioninsp').val('A');
	$('#mhdnccliente').val(ccliente);
	$('#mhdncproveedor').val(cproveedor);
	$('#mhdncmaquilador').val(cmaquilador);

	$('#mhdndcliente').val(dcliente);

	if (splan == 0) {
		$('#mbtnnewplaninsp').show();
		$('#mbtnverplaninsp').hide();
	} else {
		$('#mbtnnewplaninsp').show();
		$('#mbtnverplaninsp').show();
	}

	iniInspe(cusuarioconsultor, 'A', cnorma, csubnorma, cchecklist, ccliente, cvalornoconformidad, cmodeloinforme, cformulaevaluacion, ccriterioresultado, 'N');

	$('#btnGrabar').show();

	$('#mtxtinspdatos').val(desc_gral);
	$('#mtxtinsparea').val('AREÁ : ' + areacli);
	$('#mtxtinsplinea').val('LINEA : ' + lineaproc);

	$('#cboinspperiodo').val(periodo);
	$('#mhdnzctipoestado').val(zctipoestadoservicio);
	$('#txtinspestado').val(destado);
	$('#cbotiposerv').val(zctiposervicio).trigger("change");

	if (spermitircorrectivas == 'S') {
		$("#chkaacc").prop("checked", true);
	} else {
		$("#chkaacc").prop("checked", false);
	}
	if (sincluyeplan == 'S') {
		$("#chkplaninsp").prop("checked", true);
		$('#Btnplan').show();
	} else {
		$("#chkplaninsp").prop("checked", false);
		$('#Btnplan').hide();
	}

	$('#txtFInspeccion').datetimepicker({
		format: 'DD/MM/YYYY',
		daysOfWeekDisabled: [0],
		locale: 'es',
		autoclose: true,
		todayBtn: true
	});
	if (fservicio == '01/01/1900') {
		$('#txtFInsp').val('Sin Fecha');
	} else {
		$('#txtFInsp').val(fservicio);
	}

	$('#mtxtHsalidaperm').datetimepicker({
		format: 'hh:mm A',
		locale: 'es',
		stepping: 15
	});
	$('#mtxtHsalidaperm').datetimepicker('minDate', moment('08:00 AM', 'hh:mm A'));
	$('#mtxtHsalidaperm').datetimepicker('maxDate', moment('05:45 PM', 'hh:mm A'));
	$('#mtxtHsalidaperm').datetimepicker('date', moment('08:00 AM', 'hh:mm A'));

	objRegCtrolProvContacto.lista();

	$('#contentActualizar').hide();
};

$('#mbtnverplaninsp').click(function () {
	var v_idinspeccion = $('#mtxtidinsp').val();
	var v_fservicio = $('#mfservicio').val();
	window.open(baseurl + "oi/ctrlprov/cregctrolprov/genpdfplaninsp/" + v_idinspeccion + "/" + v_fservicio);
});

$('#modalConvalidacion').on('shown.bs.modal', function (e) {
	$("#txtconvaliidinsp").prop({readonly: true});
	$("#txtconvalifservicio").prop({readonly: true});

	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/cregctrolprov/getcbocertificadora",
		dataType: "JSON",
		async: true,
		success: function (result) {
			$("#cbocertificadora").html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cbocertificadora');
		}
	})


});

// fconvalidar = function (cauditoriainspeccion, fservicio, ccriterioresultado) {
// 	$('#frmConvalidacion').trigger("reset");
//
// 	$('#txtconvaliidinsp').val(cauditoriainspeccion);
// 	$('#txtconvalifservicio').val(fservicio);
// 	$('#mhdncritresultconvali').val(ccriterioresultado);
// 	$('#mhdnAccionconvali').val('N');
//
// 	$('#txtcierreFConvalidacion').datetimepicker({
// 		format: 'DD/MM/YYYY',
// 		daysOfWeekDisabled: [0],
// 		locale: 'es',
// 		autoclose: true,
// 		todayBtn: true
// 	});
// 	fechaActualconval(fservicio);
// };

fechaActualconval = function (fservicio) {
	var fecha = new Date();
	var fechatring = ("0" + (fecha.getDate() + 1)).slice(-2) + "/" + ("0" + (fecha.getMonth() + 1)).slice(-2) + "/" + fecha.getFullYear();
	$('#txtcierreFConvalidacion').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY'));
	$('#txtcierreFConvalidacion').datetimepicker('minDate', moment(fservicio, 'DD/MM/YYYY'));
};
// $("#cbocertificadora").change(function () {
// 	var idcertificadora = $('#cbocertificadora').val();
// 	var parametros = {
// 		"ccertificadora": idcertificadora
// 	};
// 	$.ajax({
// 		type: 'ajax',
// 		method: 'post',
// 		url: baseurl + "oi/ctrlprov/cregctrolprov/getcbocertificacion",
// 		dataType: "JSON",
// 		async: true,
// 		data: parametros,
// 		success: function (result) {
// 			$("#cbocertificacion").html(result);
// 		},
// 		error: function () {
// 			alert('Error, No se puede autenticar por error = cbocertificacion');
// 		}
// 	})
// });
// txtconvaliresul.oninput = function () {
// 	var v_resul = txtconvaliresul.value;
// 	var v_ccriterio = $('#mhdncritresultconvali').val();
//
// 	var parametros = {
// 		"resultado": v_resul,
// 		"ccriterio": v_ccriterio
// 	};
//
// 	$.ajax({
// 		type: 'ajax',
// 		url: baseurl + "oi/ctrlprov/cregctrolprov/calculocriterio",
// 		type: 'post',
// 		dataType: "JSON",
// 		data: parametros,
// 		success: function (result) {
// 			$.each(result, function (key, value) {
// 				v_nr = value.nr;
// 				v_ng = value.ng;
// 				v_nb = value.nb;
// 				document.getElementById("divcolor").style.backgroundColor = 'rgb(' + v_nr + ',' + v_ng + ',' + v_nb + ')';
// 				$('#txtconvalivalor').val(value.ddetallecriterioresultado);
// 				$('#txtconvalimes').val(value.nmes);
// 				$('#mhdncritdetresultconvali').val(value.cdetallecriterioresultado);
// 			});
// 		}
// 	});
// };

// escogerArchivo = function () {
// 	var archivoInput = document.getElementById('mtxtArchivoconvali');
// 	var archivoRuta = archivoInput.value;
// 	var extPermitidas = /(.pdf|.docx|.xlsx|.doc|.xls)$/i;
//
// 	var filename = $('#mtxtArchivoconvali').val().replace(/.*(\/|\\)/, '');
// 	$('#mtxtNomarchconvali').val(filename);
//
// 	if (!extPermitidas.exec(archivoRuta)) {
// 		alert('Asegurese de haber seleccionado un PDF, DOCX, XSLX');
// 		archivoInput.value = '';
// 		$('#mtxtNomarchconvali').val('');
// 		return false;
// 	}
// 	$('#sArchivo').val('S');
// };

subirArchivoconval = function () {
	var parametrotxt = new FormData($("#frmCreaPropu")[0]);
	var request = $.ajax({
		data: parametrotxt,
		method: 'post',
		url: baseurl + "pt/cpropuesta/subirArchivoconval/",
		dataType: "JSON",
		async: true,
		contentType: false,
		processData: false,
		error: function () {
			alert('Error, no se cargó el archivo');
		}
	});
	request.done(function (respuesta) {
		$('#btnBuscar').click();
		Vtitle = 'Guardo Correctamente';
		Vtype = 'success';
		sweetalert(Vtitle, Vtype);
		$('#mbtnCCreaPropu').click();
	});
};


$('#modalCierreespecial').on('shown.bs.modal', function (e) {
	$("#txtcierreidinsp").prop({readonly: true});
	$("#txtcierrefservicio").prop({readonly: true});

	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/cregctrolprov/getcbocierreTipo",
		dataType: "JSON",
		async: true,
		success: function (result) {
			$("#cbocierreTipo").html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error = cbocierreTipo');
		}
	})
});

fcierreespecial = function (cauditoriainspeccion, fservicio, zctipoestadoservicio, destado, ccliente) {

	$('#frmCierreespecial').trigger("reset");

	$('#txtcierreidinsp').val(cauditoriainspeccion);
	$('#mhdncierrefservicio').val(fservicio);
	$('#mhdnAccioncierre').val('N');
	$('#txtcierreFProgramado,#txtcierrefservicio').datetimepicker({
		format: 'DD/MM/YYYY',
		daysOfWeekDisabled: [0],
		locale: 'es',
		autoclose: true,
		todayBtn: true
	});
	fechaActualcierre();
	$('#divcierreprograma').hide();
	$('#divcierremonto').hide();
	$('#divcierreviatico').hide();

};

$("#cbocierreTipo").change(function () {
	var idcierretipo = $('#cbocierreTipo').val();
	var valor = $("option:selected", this).attr("data-lat");
	if (valor == '0') {
		$('#divcierreprograma').hide();
		if (idcierretipo == '874') {
			$('#divcierremonto').show();
			$('#divcierreviatico').show();
		} else {
			$('#divcierremonto').hide();
			$('#divcierreviatico').hide();
		}
		$('#txtcierreFProg').val('');
		$('#mhdnfprog').val('0');
	} else {
		$('#divcierreprograma').show();
		if (idcierretipo == '515') {
			$('#divcierremonto').show();
			$('#divcierreviatico').show();
		} else {
			$('#divcierremonto').hide();
			$('#divcierreviatico').hide();
		}
		fechaActualcierreprog();
		$('#mhdnfprog').val('1');
	}
});

fechaActualcierre = function (fservicio) {
	var fecha = new Date();
	var fechatring = ("0" + fecha.getDate()).slice(-2) + "/" + ("0" + (fecha.getMonth() + 1)).slice(-2) + "/" + fecha.getFullYear();
	$('#txtcierrefservicio').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY'));
};
fechaActualcierreprog = function (fservicio) {
	var fecha = new Date();
	var fechatring = ("0" + (fecha.getDate() + 1)).slice(-2) + "/" + ("0" + (fecha.getMonth() + 1)).slice(-2) + "/" + fecha.getFullYear();
	$('#txtcierreFProgramado').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY'));
	$('#txtcierreFProgramado').datetimepicker('minDate', moment(fechatring, 'DD/MM/YYYY'));
};


$("body").on("click", "#aReaperturar", function (event) {
	event.preventDefault();

	cauditoriainspeccion = $(this).attr("href");
	fservicio = $(this).attr("fservicio");
	cusuario = $('#hdncusuario').val();

	var params = {
		"cauditoriainspeccion": cauditoriainspeccion,
		"fservicio": fservicio,
		"cusuario": cusuario,
	};

	Swal.fire({
		title: 'Confirmar Reaperturar Inspeccion',
		text: "¿Está seguro de Reaperturar Inspeccion?",
		type: 'error',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Si, Reaperturar!'
	}).then((result) => {
		if (result.value) {
			var request = $.ajax({
				type: 'POST',
				url: baseurl + "oi/ctrlprov/cregctrolprov/setreaperturar/",
				async: true,
				data: params,
				error: function () {
					Vtitle = 'Error en Guardar!!!';
					Vtype = 'error';
					sweetalert(Vtitle, Vtype);
				}
			});
			request.done(function (respuesta) {
				var posts = JSON.parse(respuesta);

				$.each(posts, function () {
					Vtitle = 'Se Re-aperturo Correctamente!!!';
					Vtype = 'success';
					sweetalert(Vtitle, Vtype);
					$('#btnBuscar').click();
				});
			});
		}
	})

});

$('#btnRetornarLista').click(function () {
	$('#tabctrlprov a[href="#tabctrlprov-list"]').tab('show');
	$("#tblListctrlprov").DataTable().ajax.reload(null, false);
});

verInspeccion = function (id) {
	$("#modalCreactrlprov").modal('show');

	var parametros = {
		"idinspeccion": id
	};
	var request = $.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/cregctrolprov/getrecuperainsp",
		dataType: "JSON",
		async: true,
		data: parametros,
		error: function () {
			alert('Error, no se puede cargar la lista desplegable de establecimiento');
		}
	});
	request.done(function (respuesta) {
		$.each(respuesta, function () {
			var $idtipoproducto = this.id_tipoproducto;
			var $idsiparticula = this.id_siparticula;

			$('#txtNombprodReg06').val(this.nombre_producto);
			$('#txtPHmatprimaReg06').val(this.ph_materia_prima);
			$('#txtPHprodfinReg06').val(this.ph_producto_final);

			$('#cbollevapartReg06').val(this.particulas).trigger("change");


		});
	});
};

cargarInspeccion = function (id, idclie, idprov, idmaq, idestable, idarea, idlinea) {
	var params = {
		"idptregestudio": v_RegEstu
	};

	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "pt/cinforme/getTipoproducto",
		dataType: "JSON",
		async: true,
		data: params,
		success: function (result) {
			$("#cboTipoprodReg06").html(result);
			$('#cboTipoprodReg06').val($idtipoproducto).trigger("change");
		},
		error: function () {
			alert('Error, no se puede cargar la lista desplegable de establecimiento');
		}
	});
	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "pt/cinforme/getParticulas",
		dataType: "JSON",
		async: true,
		data: params,
		success: function (result) {
			$("#cboParticulasReg06").html(result);
			$('#cboParticulasReg06').val($idsiparticula).trigger("change");
		},
		error: function () {
			alert('Error, no se puede cargar la lista desplegable de establecimiento');
		}
	})
};
