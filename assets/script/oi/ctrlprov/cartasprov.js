var oTableListcartas;

var seleccionados = [];

$(document).ready(function () {

	$('#btnContactos').click(function () {

	});

	$('#modalContactos').on('show.bs.modal', function (event) {
		buscarContactos();
	});

	$('#downloadContactos').click(function() {
		descargarContactos();
	});

	$('#btnDescarga').click(function() {
		descargarCartas();
	});

	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "oi/ctrlprov/ccartasprov/getclientes",
		dataType: "JSON",
		async: true,
		success: function (result) {
			$('#cboClie').html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error');
		}
	});

	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "cglobales/getanios",
		dataType: "JSON",
		async: true,
		success: function (result) {
			$('#cboAnio').html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error');
		}
	});

	$.ajax({
		type: 'ajax',
		method: 'post',
		url: baseurl + "cglobales/getmeses",
		dataType: "JSON",
		async: true,
		success: function (result) {
			$('#cboMes').html(result);
		},
		error: function () {
			alert('Error, No se puede autenticar por error');
		}
	});

	$('#carta-inspeccion-todos').click(function () {
		const elItems = $('.carta-inspeccion-item');
		elItems.prop('checked', $(this).is(':checked'));
		agregarSeleccionados();
	});

	$(document).on('change', '.carta-inspeccion-item', function () {
		agregarSeleccionados();
	});

});

/**
 * Realiza el filtro de las inspecciones
 */
buscarContactos = function () {
	const estadoEvalProd = $('#chElvaprod').is(':checked') ? 1 : 0;
	const oTableListaContacto = $('#tblContacto').DataTable({
		'bJQueryUI': true,
		'scrollY': '650px',
		'scrollX': true,
		'processing': true,
		'bDestroy': true,
		'paging': true,
		'info': true,
		'filter': true,
		'ajax': {
			"url": BASE_URL + 'oi/ctrlprov/ccartasprov/buscar_contactos',
			"type": "POST",
			"data": function (d) {
				d.anio = $('#cboAnio').val();
				d.mes = $('#cboMes').val();
				d.ccliente = $('#cboClie').val();
				d.sevalprod = estadoEvalProd;
			},
			dataSrc: ''
		},
		'columns': [
			{
				"class": "index",
				orderable: false,
				data: null,
				targets: 0
			},
			{
				"orderable": false,
				render: function (data, type, row) {
					var senviocarta = (row.senviocarta === 'S') ? 'text-danger' : '';
					return '<span class="' + senviocarta + '" >' + row.cauditoriainspeccion + '</span>';
				}
			},
			{
				"orderable": false,
				render: function (data, type, row) {
					var fservicio = row.fservicio;
					var senviocarta = (row.senviocarta === 'S') ? 'text-danger' : '';
					if (row.fservicio) {
						fservicio = moment(row.fservicio, 'YYYY-MM-DD').format('DD/MM/YYYY');
					}
					return '<span class="' + senviocarta + '" >' + fservicio + '</span>';
				}
			},
			{
				"orderable": false,
				render: function (data, type, row) {
					var fcreacion = row.fcreacion;
					var senviocarta = (row.senviocarta === 'S') ? 'text-danger' : '';
					if (row.fcreacion) {
						fcreacion = moment(row.fcreacion, 'YYYY-MM-DD').format('DD/MM/YYYY');
					}
					return '<span class="' + senviocarta + '" >' + fcreacion + '</span>';
				}
			},
			{
				"orderable": false,
				render: function (data, type, row) {
					var senviocarta = (row.senviocarta === 'S') ? 'text-danger' : '';
					return '<span class="' + senviocarta + '" >' + row.nruc + '</span>';
				}
			},
			{
				"orderable": false,
				render: function (data, type, row) {
					var senviocarta = (row.senviocarta === 'S') ? 'text-danger' : '';
					return '<span class="' + senviocarta + '" >' + row.proveedor + '</span>';
				}
			},
			{data: 'ESTMAQ', targets: 5},
			{data: 'dlinea', targets: 6},
			{data: 'sischecklist', targets: 7},
			{
				"orderable": false,
				render: function (data, type, row) {
					var senviocarta = (row.senviocarta === 'S') ? '' : '';
					return '<span class="' + senviocarta + '" >' + row.cchecklist + ' - ' + row.dchecklist + '</span>';
				}
			},
			{
				"orderable": false,
				render: function (data, type, row) {
					var senviocarta = (row.senviocarta === 'S') ? 'checked' : '';
					const id = 'senvio-' + row.cauditoriainspeccion + '' + moment(row.fservicio, 'YYYY-MM-DD').format('YYYYMMDD');
					return '<div class="custom-control custom-checkbox">' +
						'<input type="checkbox" class="custom-control-input" ' + senviocarta + ' id="' + id + '" disabled >' +
						'<label class="custom-control-label" for="' + id + '">&nbsp;</label>' +
						'</div>';
				}
			},
			{data: 'dubigeo', targets: 10},
			{data: 'tipoempresa', targets: 11},
			{
				"orderable": false,
				render: function (data, type, row) {
					var sevalprod = '';
					switch (row.sevalprod) {
						case 1:
							sevalprod = 'Si';
							break;
						default:
						case 0:
							sevalprod = 'No';
							break;
					}
					return sevalprod;
				}
			},
			{data: 'costo', targets: 13},
			{data: 'DAREACLIENTE', targets: 14},
			{data: 'DJERARQUIA', targets: 15},
			{data: 'NOMBCONTACTO', targets: 16},
			{data: 'CARCONTACTO', targets: 17},
			{data: 'MAILCONTACTO', targets: 18},
			{data: 'FONOCONTACTO', targets: 19},
		],
		"columnDefs": [
			{
				"defaultContent": " ",
				"targets": "_all"
			}
		]
	});
	// Enumeracion
	oTableListaContacto.on('order.dt search.dt', function () {
		oTableListaContacto.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
			cell.innerHTML = i + 1;
		});
	}).draw();
};

descargarContactos = function() {
	const button = $('#downloadContactos');
	const estadoEvalProd = $('#chElvaprod').is(':checked') ? 1 : 0;
	$.ajax({
		url: baseurl + 'oi/ctrlprov/ccartasprov/descargar_contactos',
		method: 'POST',
		data: {
			anio: $('#cboAnio').val(),
			mes: $('#cboMes').val(),
			ccliente: $('#cboClie').val(),
			sevalprod: estadoEvalProd,
		},
		dataType: 'json',
		beforeSend: function() {
			objPrincipal.botonCargando(button);
		}
	}).done(function(res) {
		objPrincipal.notify('success', 'Descarga del reporte correctamente.');
		const url = baseurl + 'ar/tramites/cexcelExport/download?filename=' + res.data;
		const download = window.open(url, '_blank');
		if (!download) {
			objPrincipal.alert('warning', 'Habilite la ventana emergente de su navegador.');
		}
		download.focus();
	}).fail(function() {
		objPrincipal.notify('error', 'Error en la descarga del reporte');
	}).always(function() {
		objPrincipal.liberarBoton(button);
	});
};

descargarCartas = function() {
	const button = $('#btnDescarga');
	const estadoEvalProd = $('#chElvaprod').is(':checked') ? 1 : 0;
	$.ajax({
		url: baseurl + 'oi/ctrlprov/ccartasprov/descargar_cartas',
		method: 'POST',
		data: {
			anio: $('#cboAnio').val(),
			mes: $('#cboMes').val(),
			ccliente: $('#cboClie').val(),
			sevalprod: estadoEvalProd,
		},
		dataType: 'json',
		beforeSend: function() {
			objPrincipal.botonCargando(button);
		}
	}).done(function(res) {
		objPrincipal.notify('success', 'Descarga del reporte correctamente.');
		const url = baseurl + 'ar/tramites/cexcelExport/download?filename=' + res.data;
		const download = window.open(url, '_blank');
		if (!download) {
			objPrincipal.alert('warning', 'Habilite la ventana emergente de su navegador.');
		}
		download.focus();
	}).fail(function() {
		objPrincipal.notify('error', 'Error en la descarga del reporte');
	}).always(function() {
		objPrincipal.liberarBoton(button);
	});
};

agregarSeleccionados = function () {
	seleccionados = [];
	$('.carta-inspeccion-item').each(function () {
		const el = $(this);
		if (el.is(':checked')) {
			const inspeccion = el.data('inspeccion');
			const fecha = el.data('fecha');
			seleccionados.push({inspeccion: inspeccion, fecha: fecha});
		}
	});
};

listarCartas = function () {
	const estadoEvalProd = $('#chElvaprod').is(':checked') ? 1 : 0;
	oTableListcartas = $('#tblListcartas').DataTable({
		'bJQueryUI': true,
		'scrollY': '650px',
		'scrollX': true,
		'processing': true,
		'bDestroy': true,
		'paging': true,
		'info': true,
		'filter': true,
		'ajax': {
			"url": baseurl + "oi/ctrlprov/ccartasprov/getbuscarcartas",
			"type": "POST",
			"data": function (d) {
				d.anio = $('#cboAnio').val();
				d.mes = $('#cboMes').val();
				d.ccliente = $('#cboClie').val();
				d.sevalprod = estadoEvalProd;
			},
			dataSrc: ''
		},
		'columns': [
			{
				"class": "index",
				orderable: false,
				data: null,
				targets: 0
			},
			{
				"orderable": false,
				render: function (data, type, row) {
					const id = 'carta-' + row.cauditoriainspeccion + '' + moment(row.fservicio, 'YYYY-MM-DD').format('YYYYMMDD');
					return '<div class="custom-control custom-checkbox">' +
						'<input type="checkbox" class="custom-control-input carta-inspeccion-item" id="' + id + '" name="' + id + '" data-inspeccion="' + row.cauditoriainspeccion + '" data-fecha="' + row.fservicio + '" >' +
						'<label class="custom-control-label" for="' + id + '">&nbsp;</label>' +
						'</div>';
				}
			},
			{
				"orderable": false,
				render: function (data, type, row) {
					var senviocarta = (row.senviocarta === 'S') ? 'text-danger' : '';
					return '<span class="' + senviocarta + '" >' + row.cauditoriainspeccion + '</span>';
				}
			},
			// {
			// 	"orderable": false,
			// 	render: function (data, type, row) {
			// 		var fservicio = row.fservicio;
			// 		var senviocarta = (row.senviocarta === 'S') ? 'text-danger' : '';
			// 		if (row.fservicio) {
			// 			fservicio = moment(row.fservicio, 'YYYY-MM-DD').format('DD/MM/YYYY');
			// 		}
			// 		return '<span class="' + senviocarta + '" >' + fservicio + '</span>';
			// 	}
			// },
			{
				"orderable": false,
				render: function (data, type, row) {
					var fcreacion = row.fcreacion;
					var senviocarta = (row.senviocarta === 'S') ? 'text-danger' : '';
					if (row.fcreacion) {
						fcreacion = moment(row.fcreacion, 'YYYY-MM-DD').format('DD/MM/YYYY');
					}
					return '<span class="' + senviocarta + '" >' + fcreacion + '</span>';
				}
			},
			{
				"orderable": false,
				render: function (data, type, row) {
					var senviocarta = (row.senviocarta === 'S') ? 'text-danger' : '';
					return '<span class="' + senviocarta + '" >' + row.nruc + '</span>';
				}
			},
			{
				"orderable": false,
				render: function (data, type, row) {
					var senviocarta = (row.senviocarta === 'S') ? 'text-danger' : '';
					return '<span class="' + senviocarta + '" >' + row.proveedor + '</span>';
				}
			},
			{data: 'ESTMAQ', targets: 6},
			{data: 'dlinea', targets: 7},
			// {data: 'sischecklist', targets: 8},
			// {
			// 	"orderable": false,
			// 	render: function (data, type, row) {
			// 		var senviocarta = (row.senviocarta === 'S') ? '' : '';
			// 		return '<span class="' + senviocarta + '" >' + row.cchecklist + ' - ' + row.dchecklist + '</span>';
			// 	}
			// },
			{
				"orderable": false,
				render: function (data, type, row) {
					var senviocarta = (row.senviocarta === 'S') ? 'checked' : '';
					const id = 'senvio-' + row.cauditoriainspeccion + '' + moment(row.fservicio, 'YYYY-MM-DD').format('YYYYMMDD');
					return '<div class="custom-control custom-checkbox">' +
						'<input type="checkbox" class="custom-control-input" ' + senviocarta + ' id="' + id + '" disabled >' +
						'<label class="custom-control-label" for="' + id + '">&nbsp;</label>' +
						'</div>';
				}
			},
			// {data: 'dubigeo', targets: 11},
			// {data: 'tipoempresa', targets: 12},
			// {
			// 	"orderable": false,
			// 	render: function (data, type, row) {
			// 		var sevalprod = '';
			// 		switch (row.sevalprod) {
			// 			case 1:
			// 				sevalprod = 'Si';
			// 				break;
			// 			default:
			// 			case 0:
			// 				sevalprod = 'No';
			// 				break;
			// 		}
			// 		return sevalprod;
			// 	}
			// },
			{data: 'costo', targets: 14},
			// {data: 'DAREACLIENTE', targets: 15},
			// {data: 'DJERARQUIA', targets: 16},
		],
	});
	// Enumeracion
	oTableListcartas.on('order.dt search.dt', function () {
		oTableListcartas.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
			cell.innerHTML = i + 1;
		});
	}).draw();
};

$('#btnBuscar').click(function () {
	listarCartas();
});

$('#btnPrint').click(function () {
	const el = $('#formCartasElegidas');
	var v_anio = $("#cboAnio option:selected").attr("value");
	var v_mes = $("#cboMes option:selected").attr("value");
	el.attr('action', baseurl + 'oi/ctrlprov/ccartasprov/genpdfcartasprov_elegidos/' + v_anio + '/' + v_mes);
	$('#formCartasElegidas').submit();
	// var v_anio = $( "#cboAnio option:selected").attr("value");
	// var v_mes = $( "#cboMes option:selected").attr("value");
	// var v_clie = $( "#cboClie option:selected").attr("value");
	// window.open(baseurl+"oi/ctrlprov/ccartasprov/genpdfcartasprov/"+v_anio+"/"+v_mes+"/"+v_clie);
	//
	/* var params = {
		 "anio"  : v_anio,
		 "mes"   : v_mes,
		 "clie"  : v_clie
	 };
	 $.ajax({
	   type: 'ajax',
	   method: 'post',
	   url: baseurl+"oi/ctrlprov/ccartasprov/genpdfcartasprov",
	   dataType: "JSON",
	   async: true,
	   data: params,
	   success:function(result)
	   {
		   $('#cboDist').html(result);
	   },
	   error: function(){
		 alert('Error, No se puede autenticar por error');
	   }
	 })*/
});
