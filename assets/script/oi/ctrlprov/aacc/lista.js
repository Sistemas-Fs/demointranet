/*!
 *
 * @version 1.0.0
 */

const objAACCLista = {};

$(function() {

	objAACCLista.listaClientes = function() {
		$.ajax({
			type: 'ajax',
			method: 'post',
			url: baseurl + "oi/ctrlprov/cregctrolprov/getcboclieserv",
			dataType: "JSON",
			async: true,
			success: function (result) {
				$('#filtro_cliente').html(result);
			},
			error: function () {
				alert('Error, No se puede autenticar por error = cboclieserv');
			}
		});
	};

	objAACCLista.listar = function() {
		const button = $('#btnTblInsp');
		$.ajax({
			type: 'ajax',
			method: 'post',
			data: {
				ccliente: $('#filtro_cliente').val(),
				busqueda: $('#filter_insprov').val(),
			},
			url: baseurl + "oi/ctrlprov/aacc/caccion_correctiva/lista",
			dataType: "JSON",
			async: true,
			beforeSend: function() {
				objPrincipal.botonCargando(button);
			}
		}).done(function(result) {
			let rows = '';
			let pos = 1;
			result.forEach(function(item) {
				rows += '<tr class="" >';
				rows += '<td class="text-left" style="width: 120px; min-width: 120px" >';
				rows += '<button type="button" role="button" class="btn btn-primary elegir-inspeccion" data-fservicio="' + item.FSERVICIO + '" data-cauditoria="' + item.CAUDITORIAINSPECCION + '"  >';
				rows += '<i class="fa fa-external-link-alt" ></i> Elegir';
				rows += '</button>';
				rows += '</td>';
				rows += '<td class="text-left" >' + pos + '</td>';
				rows += '<td class="text-left" >' + item.DRAZONSOCIAL + '</td>';
				rows += '<td class="text-center" >' + item.CAUDITORIAINSPECCION + '</td>';
				rows += '<td class="text-center" >' + item.FINSPECCION + '</td>';
				rows += '<td class="text-center" >' + item.NRODEACCCORRECTIVAS + '</td>';
				rows += '<tr>';
				++pos;
			});
			$('#tblInspProv tbody').html(rows);
		}).always(function() {
			objPrincipal.liberarBoton(button);
		});
	};

	objAACCLista.elegirInsp = function() {
		const boton = $(this);
		$.ajax({
			url: baseurl + "oi/ctrlprov/aacc/caccion_correctiva/buscar",
			method: 'POST',
			data: {
				fservicio: boton.data('fservicio'),
				cauditoria: boton.data('cauditoria'),
			},
			dataType: "JSON",
			async: true,
		}).done(function(result) {
			let cauditoria = '';
			let fservicio = '';
			let dproveedor = '';
			let dubicacionfileserverac = '';
			if (result) {
				cauditoria = result.cauditoriainspeccion;
				fservicio = result.finspeccion;
				dproveedor = result.proveedor;
				dubicacionfileserverac = result.DUBICACIONFILESERVERAC;
			}
			$('#filtro_codigo').val(cauditoria);
			$('#filtro_proveedor').val(dproveedor);
			$('#filtro_fecha').val(fservicio);
			$('#modalInspecciones').modal('hide');
			if (dubicacionfileserverac) {
				objAACCLista.activarArchivo();
			} else {
				objAACCLista.desactivarArchivo();
			}
			objAACCLista.listaAACC();
		});
	};

	objAACCLista.listaAACC = function() {
		const boton = $('#btnBuscar');
		objPrincipal.botonCargando(boton);
		const table = $('#tblAccionCorrectiva tbody');
		$.ajax({
			url: baseurl + "oi/ctrlprov/aacc/caccion_correctiva/buscar_aacc",
			method: 'POST',
			data: {
				fservicio: $('#filtro_fecha').val(),
				cauditoria: $('#filtro_codigo').val(),
			},
			dataType: "JSON",
			async: true,
		}).done(function(result) {
			let rows = '';
			if (result) {
				result.forEach(function(item) {
					let daccioncorrectiva = (item.daccioncorrectiva) ? item.daccioncorrectiva : '';
					let fcorrectiva = (item.fcorrectiva) ? item.fcorrectiva : '';
					let dresponsablecliente = (item.dresponsablecliente) ? item.dresponsablecliente : '';
					let dhallazgo = (item.dhallazgo) ? item.dhallazgo : '';
					rows += '<tr>';
					rows += '<td class="text-center" style="width: 120px" >' + item.dnumerador + '</td>';
					rows += '<td class="text-left" style="width: 220px" >' + item.drequisito + '</td>';
					rows += '<td class="text-left" style="width: 50px" >' + item.sexcluyente + '</td>';
					rows += '<td class="text-left" style="width: 200px" >' + dhallazgo + '</td>';
					rows += '<td class="text-left" style="width: 200px" >' + daccioncorrectiva + '</td>';
					rows += '<td class="text-center" style="width: 80px" >' + fcorrectiva + '</td>';
					rows += '<td class="text-left" style="width: 200px" >' + dresponsablecliente + '</td>';
					rows += '</tr>';
				});
			}
			table.html(rows);
		}).always(function() {
			objPrincipal.liberarBoton(boton);
		});
	};

	objAACCLista.subirArchivo = function() {
		const form = $('form#frmFileACC');
		const datos = new FormData(form[0]);
		datos.append('cauditoria', $('#filtro_codigo').val());
		datos.append('fservicio', moment($('#filtro_fecha').val(), 'DD/MM/YYYY').format('YYYY-MM-DD'));
		const botonGuardar = $('#btnCargarArchivo');
		$.ajax({
			url: BASE_URL + 'oi/ctrlprov/inspctrolprov/caccion_correctiva/guardar_archivo',
			method: 'POST',
			data: datos,
			dataType: 'json',
			cache: false,
			contentType: false,
			processData: false,
			beforeSend: function () {
				objPrincipal.botonCargando(botonGuardar);
			}
		}).done(function (response) {
			sweetalert(response.message, 'success');
			objAACCLista.listaAACC();
			objAACCLista.activarArchivo();
		}).fail(function (jqxhr) {
			const message = (jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la petición HTTP.';
			sweetalert(message, 'error');
		}).always(function () {
			objPrincipal.liberarBoton(botonGuardar);
		});
	};

	objAACCLista.activarArchivo = function() {
		$('.btn-exist-file').show();
	};

	objAACCLista.desactivarArchivo = function() {
		$('.btn-exist-file').hide();
	};

	objAACCLista.descargarArchivo = function() {
		var content = BASE_URL + 'oi/ctrlprov/inspctrolprov/cinspeccion/descargar_acc?cauditoria=' + $('#filtro_codigo').val() + '&fservicio=' + moment($('#filtro_fecha').val(), 'DD/MM/YYYY').format('YYYY-MM-DD');
		var download = window.open(content, '_blank');
		if (download == null || typeof download == "undefined") {
			objApp.notify("Ventana emergente", "Habilite la ventana emergente de su navegador. Para continuar la descarga!", "warning");
		} else {
			download.focus();
		}
	};

	objAACCLista.eliminarArchivo = function () {
		const boton = $('#btnEliminarArchivo');
		$.ajax({
			url: BASE_URL + 'oi/ctrlprov/inspctrolprov/caccion_correctiva/eliminar_archivo',
			method: 'POST',
			data: {
				cauditoria: $('#filtro_codigo').val(),
				fservicio: moment($('#filtro_fecha').val(), 'DD/MM/YYYY').format('YYYY-MM-DD'),
			},
			dataType: 'json',
			beforeSend: function () {
				objPrincipal.botonCargando(boton);
			}
		}).done(function (response) {
			sweetalert(response.message, 'success');
			$('#acc_archivo').val('');
			objAACCLista.desactivarArchivo();
			objAACCLista.listaAACC();
		}).fail(function (jqxhr) {
			const message = (jqxhr.responseJSON && jqxhr.responseJSON.message) ? jqxhr.responseJSON.message : 'Error en la petición HTTP.';
			sweetalert(message, 'error');
		}).always(function () {
			objPrincipal.liberarBoton(boton);
		});
	};

	objAACCLista.descargarPlantilla = function() {
		var content = BASE_URL + 'oi/ctrlprov/inspctrolprov/caccion_correctiva/descargar_plantilla?cauditoria=' + $('#filtro_codigo').val() + '&fservicio=' + moment($('#filtro_fecha').val(), 'DD/MM/YYYY').format('YYYY-MM-DD');
		var download = window.open(content, '_blank');
		if (download == null || typeof download == "undefined") {
			objApp.notify("Ventana emergente", "Habilite la ventana emergente de su navegador. Para continuar la descarga!", "warning");
		} else {
			download.focus();
		}
	};

});

$(document).ready(function() {

	objAACCLista.listaClientes();

	$('#filtro_cliente').change(function() {
		$('#btnFiltroInsp').click();
	});

	$('#btnFiltroInsp').click(function() {
		$('#modalInspecciones').modal('show');
	});

	$('#modalInspecciones').on('show.bs.modal', function (event) {
		objAACCLista.listar();
	});

	$('#btnTblInsp').click(objAACCLista.listar);

	$(document).on('click', '.elegir-inspeccion', objAACCLista.elegirInsp);

	$('#btnBuscar').click(objAACCLista.listaAACC);

	$('#btnCargarArchivo').click(objAACCLista.subirArchivo);

	$('#btnDescargarArchivo').click(objAACCLista.descargarArchivo);

	$('#btnEliminarArchivo').click(objAACCLista.eliminarArchivo);

	$('#btnExportar').click(objAACCLista.descargarPlantilla);

});
