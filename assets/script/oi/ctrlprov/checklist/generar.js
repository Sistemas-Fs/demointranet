/*!
 *
 * @version 1.0.0
 */

const objCLGenerar = {};

$(function () {

	/**
	 * Habilita el formulario para poder crear un nuevo checklist
	 */
	objCLGenerar.nuevoFormulario = function () {
		$('#tabReg2-tab').click();
		objCLGenerar.cargarInformacion('oi/ctrlprov/checklist/cchecklist/recursos_crear')
			.done(function (res) {
				objCLGenerar.imprimirCombos($('#checklist_area'), res.data.areas);
				objCLGenerar.imprimirCombos($('#checklist_conformidad'), res.data.no_conformidad);
				objCLGenerar.imprimirCombos($('#checklist_servicio'), []);
				objCLGenerar.imprimirCombos($('#checklist_sistema'), []);
				objCLGenerar.imprimirCombos($('#checklist_organismo'), []);
				objCLGenerar.imprimirCombos($('#checklist_rubro'), []);
				$('#checklist_id').val('0');
				$('#checklist_descripcion').val('');
				$('#checklist_estado_1').prop('checked', false);
				$('#checklist_estado_2').prop('checked', true);
				$('#checklist_uso').prop('checked', false);
				$('#checklist_asignado').prop('checked', false);
				$('#checklist_pesos').val('N');
				$('#checklist_tipo').val('P');
				$('#checklist_estado').val('A');
				$('#contenidoRequisito').hide();
			});
	};

	/**
	 * Habila la edición del checklist
	 * @param id
	 */
	objCLGenerar.editarFormulario = function (id) {
		objCLGenerar.cargarInformacion('oi/ctrlprov/checklist/cchecklist/recursos_editar/' + id)
			.done(function (res) {
				const data = res.data;
				const checklist = res.data.checklist;
				const norma = res.data.norma;
				objCLGenerar.imprimirCombos($('#checklist_area'), data.areas);
				objCLGenerar.imprimirCombos($('#checklist_conformidad'), data.no_conformidad);
				objCLGenerar.imprimirCombos($('#checklist_servicio'), data.servicios);
				objCLGenerar.imprimirCombos($('#checklist_sistema'), data.sistemas);
				objCLGenerar.imprimirCombos($('#checklist_organismo'), data.organismos);
				objCLGenerar.imprimirCombos($('#checklist_rubro'), data.rubros);
				// Imprimesion del checklist
				$('#checklist_id').val(checklist.CCHECKLIST);
				$('#checklist_descripcion').val(checklist.DCHECKLIST);
				$('#checklist_area').val(checklist.CAREA);
				$('#checklist_servicio').val(checklist.CSERVICIO);
				$('#checklist_sistema').val(norma.CSISTEMA);
				$('#checklist_organismo').val(checklist.CNORMA);
				$('#checklist_rubro').val(checklist.CSUBNORMA);
				$('#checklist_conformidad').val(checklist.CVALORNOCONFORMIDAD);
				$('#checklist_tipo').val(checklist.SCHECKLIST);
				$('#checklist_pesos').val(checklist.SPESO);
				$('#checklist_estado').val(checklist.SREGISTRO);
				$('#checklist_estado_1').prop('checked', (checklist.SCERRADO !== 'A'));
				$('#checklist_estado_2').prop('checked', (checklist.SCERRADO === 'A'));
				$('#checklist_uso').prop('checked', (checklist.SUSO === 'S'));
				$('#checklist_asignado').prop('checked', (checklist.SPROPIEDAD === 'C'));
				$('#contenidoRequisito').show();
				objCLRequisito.obtenerRequisitos(checklist.CCHECKLIST);
				// Se genera el poder crear nuevo requisito
				objCLRequisito.nuevo();

				$('#tabReg2-tab').click();
			});
	};

	objCLGenerar.volverLista = function () {
		$('#tabReg1-tab').click();
	};

	/**
	 * Impresion de los combos del formulario
	 * @param el
	 * @param resultado
	 */
	objCLGenerar.imprimirCombos = function (el, resultado) {
		// Impresion de las areas
		let opcion = '<option value="" >::Elegir::</option>';
		if (resultado && Array.isArray(resultado)) {
			resultado.forEach(function (item) {
				opcion += '<option value="' + item.id + '" >' + item.text + '</option>';
			});
		}
		el.html(opcion);
	};

	objCLGenerar.cargarInformacion = function (url, data) {
		data = (data) ? data : {};
		return $.ajax({
			url: baseurl + url,
			method: 'POST',
			data: data,
			dataType: 'json',
			beforeSend: function () {
			}
		}).fail(function (jhxr) {
			const message = (jhxr && jhxr.responseJSON && jhxr.responseJSON.message)
				? jhxr.responseJSON.message
				: 'Error en el proceso de ejecución.';
			objPrincipal.notify('error', message);
		});
	};

	/**
	 * Carga de datos de los servicios
	 */
	objCLGenerar.cargarServicios = function () {
		objCLGenerar.cargarInformacion('oi/ctrlprov/checklist/cchecklist/recursos_servicio', {
			area: $('#checklist_area').val(),
		}).done(function (res) {
			objCLGenerar.imprimirCombos($('#checklist_servicio'), res);
		});
	};

	/**
	 * Carga de datos de los sistemas
	 */
	objCLGenerar.cargarSistema = function () {
		objCLGenerar.cargarInformacion('oi/ctrlprov/checklist/cchecklist/recursos_sistema', {
			id_area: $('#checklist_area').val(),
			id_servicio: $('#checklist_servicio').val(),
		}).done(function (res) {
			objCLGenerar.imprimirCombos($('#checklist_sistema'), res);
		});
	};

	/**
	 * Carga de datos de los organismo
	 */
	objCLGenerar.cargarOrganismo = function () {
		objCLGenerar.cargarInformacion('oi/ctrlprov/checklist/cchecklist/recursos_organismo', {
			id_sistema: $('#checklist_sistema').val(),
		}).done(function (res) {
			objCLGenerar.imprimirCombos($('#checklist_organismo'), res);
		});
	};

	/**
	 * Carga de datos de los rubro
	 */
	objCLGenerar.cargarRubro = function () {
		objCLGenerar.cargarInformacion('oi/ctrlprov/checklist/cchecklist/recursos_rubro', {
			id_organismo: $('#checklist_organismo').val(),
		}).done(function (res) {
			objCLGenerar.imprimirCombos($('#checklist_rubro'), res);
		});
	};

	/**
	 * Guarda el checklist
	 */
	objCLGenerar.guardar = function() {
		const boton = $('#btnGrabarCheckList');
		const form = $('#frmMantRegistro');
		$.ajax({
			url: form.attr('action'),
			method: 'POST',
			data: form.serialize(),
			dataType: 'json',
			beforeSend: function () {
				objPrincipal.botonCargando(boton);
			}
		}).done(function(res) {
			objPrincipal.notify('success', 'CheckList obtenido correctamente.');
			$('#checklist_id').val(res.data.CCHECKLIST);
			if (res.data.SREGISTRO === 'A') {
				$('#contenidoRequisito').show();
				objCLRequisito.obtenerRequisitos(res.data.CCHECKLIST);
				// Se genera el poder crear nuevo requisito
				objCLRequisito.nuevo();
			} else {
				// Como se guarda como inactivo, se refresa a la lista actualizada
				$('#btnRetornarLista').click();
				objCLLista.lista();
			}
		}).fail(function (jhxr) {
			const message = (jhxr && jhxr.responseJSON && jhxr.responseJSON.message)
				? jhxr.responseJSON.message
				: 'Error en el proceso de ejecución.';
			objPrincipal.notify('error', message);
		}).always(function() {
			objPrincipal.liberarBoton(boton);
		});
	};

});

$(document).ready(function () {

	$('#btnNuevoCheckList').click(objCLGenerar.nuevoFormulario);

	$('#btnRetornarLista').click(objCLGenerar.volverLista);

	$('#checklist_area').change(objCLGenerar.cargarServicios);

	$('#checklist_servicio').change(objCLGenerar.cargarSistema);

	$('#checklist_sistema').change(objCLGenerar.cargarOrganismo);

	$('#checklist_organismo').change(objCLGenerar.cargarRubro);

	$('#btnGrabarCheckList').click(objCLGenerar.guardar);

});
