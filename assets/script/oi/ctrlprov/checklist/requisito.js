/*!
 *
 * @version 1.0.0
 */

const objCLRequisito = {};

$(function () {

	/**
	 * @param idCheckList
	 */
	objCLRequisito.obtenerRequisitos = function (idCheckList) {
		const btnLoading = $('#btnLoadingRequisito');
		$.ajax({
			url: baseurl + 'oi/ctrlprov/checklist/cchecklist/obtener_requisitos',
			method: 'POST',
			data: {
				idCheckList: idCheckList,
			},
			dataType: 'json',
			beforeSend: function () {
				btnLoading.show();
			}
		}).done(function (res) {
			// requisitos
			objCLRequisito.imprimirRequisitos(res.items);
		}).fail(function (jhxr) {
			const message = (jhxr && jhxr.responseJSON && jhxr.responseJSON.message)
				? jhxr.responseJSON.message
				: 'Error en el proceso de ejecución.';
			objPrincipal.notify('error', message);
		}).always(function () {
			btnLoading.hide();
		})
	};

	/**
	 * @param items
	 */
	objCLRequisito.imprimirRequisitos = function (items) {
		const table = $('#tblRequisitos tbody');
		let filas = '';
		if (items && Array.isArray(items)) {
			items.forEach(function (item, pos) {
				let colorEscluyente = '';
				if (item.SEXCLUYENTE === 'S') {
					colorEscluyente = 'text-danger'
				}
				filas += '<tr data-position="' + pos + '" data-requisito="' + item.CREQUISITOCHECKLIST + '" class="' + colorEscluyente + ' elegir-requisito" style="cursor: pointer" id="lista-requisito-' + item.CREQUISITOCHECKLIST + '" >';
				filas += '<td class="text-left" style="width: 5rem; min-width: 5rem;" >' + item.DNUMERADOR + '</td>';
				filas += '<td class="text-left" style="width: 25rem; min-width: 25rem;" >' + item.DREQUISITO + '</td>';
				filas += '</tr>';
			});
		}
		table.html(filas);
	};

	/**
	 * @param items
	 */
	objCLRequisito.imprimirRequisitoPadres = function (items) {
		let opciones = '<option value="0" >::RAIZ::</option>';
		if (items && Array.isArray(items)) {
			items.forEach(function (item) {
				opciones += '<option value="' + item.crequisitochecklist + '" >' + item.dnumerador + ' ' + item.drequisito + '</option>';
			});
		}
		$('#checklist_padre_requisito').html(opciones);
	};

	/**
	 * @param items
	 */
	objCLRequisito.imprimirValores = function (items) {
		let opciones = '';
		if (items && Array.isArray(items)) {
			items.forEach(function (item) {
				opciones += '<option value="' + item.cvalor + '" >' + item.cvalor + '|' + item.dvalor + '|' + item.stipovalor + '|' + item.nvalor + '</option>';
			});
		}
		$('#checklist_requisito_valor').html(opciones);
	};

	/**
	 * Nuevo requisito
	 */
	objCLRequisito.nuevo = function () {
		const idCheckList = $('#checklist_id').val();
		$('.elegir-requisito').removeClass('active');
		const btnLoading = $('#btnLoadingRequisito');
		$.ajax({
			url: baseurl + 'oi/ctrlprov/checklist/crequisito/nuevo_requisito',
			method: 'POST',
			data: {
				idCheckList: idCheckList,
			},
			dataType: 'json',
			beforeSend: function () {
				btnLoading.show();
			}
		}).done(function (response) {
			$('.elegir-requisito').removeClass('table-active');
			objCLRequisito.imprimirDatos(
				response.data.padreRequisitos,
				response.data.valores
			);
			$('#btnGuardarRequisito').prop('disabled', false).html('<i class="fa fa-save" ></i> Guardar Requisito');
			$('#btnEliminarRequisito').prop('disabled', true);
			objCLRequisito.obtenerOrdenLista(0, 0);
		}).fail(function (jhxr) {
			const message = (jhxr && jhxr.responseJSON && jhxr.responseJSON.message)
				? jhxr.responseJSON.message
				: 'Error en el proceso de ejecución.';
			objPrincipal.notify('error', message);
		}).always(function () {
			btnLoading.hide();
		});
	};

	/**
	 * Se obtiene el orden de lista
	 * @param idRequisitoPadre
	 * @param idRequisito
	 */
	objCLRequisito.obtenerOrdenLista = function (idRequisitoPadre, idRequisito) {
		const idCheckList = $('#checklist_id').val();
		$.ajax({
			url: baseurl + 'oi/ctrlprov/checklist/crequisito/obtener_order_lista',
			method: 'POST',
			data: {
				idCheckList: idCheckList,
				idRequisitoPadre: idRequisitoPadre,
				idRequisitoId: idRequisito,
			},
			dataType: 'json',
			beforeSend: function () {
			}
		}).done(function (response) {
			let opciones = '';
			const data = response.data;
			if (data && Array.isArray(data)) {
				data.forEach(function (opcion) {
					opciones += '<option value="' + opcion.CREQUISITOCHECKLIST + '" data-nivel="' + opcion.NIVEL + '" data-orden="' + opcion.DNUMERADOR + '" data-lista="' + opcion.DORDENLISTA + '" >' + opcion.DNUMERADOR + '</option>';
				});
			}
			$('#checklist_requisito_orden').html(opciones);
			objCLRequisito._elegirOrdenLista(idRequisito);
		}).fail(function (jhxr) {
			const message = (jhxr && jhxr.responseJSON && jhxr.responseJSON.message)
				? jhxr.responseJSON.message
				: 'Error en el proceso de ejecución.';
			objPrincipal.notify('error', message);
		});
	};

	/**
	 *
	 */
	objCLRequisito.elegir = function () {
		const idRequisito = $(this).data('requisito');
		$('.elegir-requisito').removeClass('table-active');
		objCLRequisito.modificar(idRequisito);
	};

	/**
	 * Elección del requisito
	 */
	objCLRequisito.modificar = function (idRequisito) {
		const idCheckList = $('#checklist_id').val();
		const btnLoading = $('#btnLoadingRequisito');
		$('#lista-requisito-' + idRequisito).addClass('table-active');
		$.ajax({
			url: baseurl + 'oi/ctrlprov/checklist/crequisito/obtener_requisito',
			method: 'POST',
			data: {
				idCheckList: idCheckList,
				idRequisito: idRequisito,
			},
			dataType: 'json',
			beforeSend: function () {
				btnLoading.show();
			}
		}).done(function (response) {
			objCLRequisito.imprimirDatos(
				response.data.padreRequisitos,
				response.data.valores,
				response.data.requisito
			);
			$('#btnGuardarRequisito').prop('disabled', false).html('<i class="fa fa-edit" ></i> Actualizar Requisito');
			$('#btnEliminarRequisito').prop('disabled', false);
			objCLRequisito.obtenerOrdenLista(response.data.requisito.CREQUISITOCHECKLISTPADRE, response.data.requisito.CREQUISITOCHECKLIST);
		}).fail(function (jhxr) {
			const message = (jhxr && jhxr.responseJSON && jhxr.responseJSON.message)
				? jhxr.responseJSON.message
				: 'Error en el proceso de ejecución.';
			objPrincipal.notify('error', message);
		}).always(function () {
			btnLoading.hide();
		});
	};

	/**
	 * @param padreRequisitos
	 * @param valores
	 * @param requisito
	 */
	objCLRequisito.imprimirDatos = function (padreRequisitos, valores, requisito) {
		requisito = (typeof requisito === 'undefined') ? null : requisito;
		objCLRequisito.imprimirRequisitoPadres(padreRequisitos);
		// Impresión de los valores y su configuración visual
		objCLRequisito.imprimirValores(valores);
		$('#checklist_requisito_valor').select2({
			escapeMarkup: function (text) {
				return text;
			},
			templateResult: function (state) {
				const separate = String(state.text).split('|');
				let result = '<table class="" >';
				result += '<tr>';
				result += '<td style="width: 50px" >' + separate[0] + '</td>';
				result += '<td style="width: 200px" >' + separate[1] + '</td>';
				result += '<td style="width: 30px" >' + separate[2] + '</td>';
				result += '<td style="width: 30px" >' + separate[3] + '</td>';
				result += '<tr/>';
				result += '<table>';
				return result;
			},
			templateSelection: function (state) {
				return String(state.text).split('|')[1];
			},
		});
		// Impresión del requisito
		if (requisito) {
			$('#checklist_requisito_id').val(requisito.CREQUISITOCHECKLIST);
			$('#checklist_padre_requisito').val(requisito.CREQUISITOCHECKLISTPADRE)
				.prop('disabled', true);
			$('#checklist_requisito_valor').val(requisito.CVALOR).change();
			$('#checklist_requisito_descripcion').val(requisito.DREQUISITO);
			$('#checklist_normativa').val(requisito.DNORMATIVA);
			$('#checklist_requisito_valorado').prop('checked', (requisito.SVALOR === 'S'));
			$('#checklist_requisito_excluyente_1').prop('checked', (requisito.SEXCLUYENTE === 'S'));
			$('#checklist_requisito_excluyente_2').prop('checked', (requisito.SEXCLUYENTE !== 'S'));
			$('#checklist_requisito_factores_criticos').prop('checked', (parseInt(requisito.SDISMINUYECALIFICACION) === 1));
			$('#checklist_requisito_peso').val(requisito.NPESO);
			$('#checklist_requisito_numerador').val(requisito.DNUMERADOR);
			let arrayNumerador = (requisito.DNUMERADOR) ? String(requisito.DNUMERADOR).split('.') : [];
			$('#checklist_requisito_nivel').val('Nivel ' + arrayNumerador.length);
			// Inspecciones de tienda
			$('#checklist_inspeccion_peso').prop('checked', (parseInt(requisito.SPRODPESO) === 1));
			$('#checklist_inspeccion_venc').prop('checked', (parseInt(requisito.SPRODVENCE) === 1));
			$('#checklist_inspeccion_baja').prop('checked', (parseInt(requisito.BAJA_MITAD) === 1));
			$('#checklist_inspeccion_oc').prop('checked', (parseInt(requisito.SOC) === 1));
		} else {
			// Nuevo requisito
			$('#checklist_requisito_id').val('0').change();
			$('#checklist_padre_requisito').val('0')
				.prop('disabled', false);
			$('#checklist_requisito_valor').val('001').change();
			$('#checklist_requisito_descripcion').val('');
			$('#checklist_normativa').val('');
			$('#checklist_requisito_valorado').prop('checked', false);
			$('#checklist_requisito_excluyente_1').prop('checked', false);
			$('#checklist_requisito_excluyente_2').prop('checked', true);
			$('#checklist_requisito_factores_criticos').prop('checked', false);
			$('#checklist_requisito_peso').val(1);
			$('#checklist_requisito_numerador').val('');
			$('#checklist_requisito_nivel').val('');
			$('#checklist_inspeccion_peso').prop('checked', false);
			$('#checklist_inspeccion_venc').prop('checked', false);
			$('#checklist_inspeccion_baja').prop('checked', false);
			$('#checklist_inspeccion_oc').prop('checked', false);
		}
	};

	/**
	 * Guarda el requisito
	 */
	objCLRequisito.guardar = function () {
		const boton = $(this);
		const form = $('#frmMantRequisito');
		$.ajax({
			url: form.attr('action'),
			method: 'POST',
			data: form.serialize() + '&checklist_id=' + $('#checklist_id').val(),
			dataType: 'json',
			beforeSend: function () {
				objPrincipal.botonCargando(boton);
			}
		}).done(function (response) {
			objPrincipal.notify('success', response.message);
			objCLRequisito.obtenerRequisitos(response.data.CCHECKLIST);
			// Se limpia el formulario
			objCLRequisito.nuevo();
		}).fail(function (jhxr) {
			const message = (jhxr && jhxr.responseJSON && jhxr.responseJSON.message)
				? jhxr.responseJSON.message
				: 'Error en el proceso de ejecución.';
			objPrincipal.notify('error', message);
		}).always(function () {
			objPrincipal.liberarBoton(boton);
		});
	};

	/**
	 * Elimina el requisito del checklist
	 */
	objCLRequisito.eliminar = function () {
		Swal.fire({
			type: 'warning',
			title: 'Eliminar Requisito',
			text: '¿Estas seguro(a) de eliminar?',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Si',
			cancelButtonText: 'Cancelar',
		}).then((result) => {
			if (result.value) {
				const boton = $(this);
				$.ajax({
					url: baseurl + 'oi/ctrlprov/checklist/crequisito/eliminar',
					method: 'POST',
					data: {
						idCheckList: $('#checklist_id').val(),
						idRequisitoCheckList: $('#checklist_requisito_id').val(),
					},
					dataType: 'json',
					beforeSend: function () {
						objPrincipal.botonCargando(boton);
					}
				}).done(function (response) {
					objPrincipal.notify('success', response.message);
					objCLRequisito.obtenerRequisitos($('#checklist_id').val());
					// Se limpia el formulario
					objCLRequisito.nuevo();
				}).fail(function (jhxr) {
					const message = (jhxr && jhxr.responseJSON && jhxr.responseJSON.message)
						? jhxr.responseJSON.message
						: 'Error en el proceso de ejecución.';
					objPrincipal.notify('error', message);
				}).always(function () {
					objPrincipal.liberarBoton(boton);
				});
			}
		});
	};

	/**
	 * Se actualiza la orden de Lista
	 */
	objCLRequisito.actualizarOrdenLista = function () {
		objCLRequisito.obtenerOrdenLista($(this).val(), 0);
	};

	/**
	 * Elegir Orden
	 */
	objCLRequisito.elegirOrdenLista = function () {
		const id = $(this).val();
		objCLRequisito._elegirOrdenLista(id);
	};

	/**
	 * Impresion de la orden
	 * @param id
	 * @private
	 */
	objCLRequisito._elegirOrdenLista = function (id) {
		const opcion = $('#checklist_requisito_orden option[value=' + id + ']');
		$('#checklist_requisito_numerador').val(opcion.data('orden'));
		$('#checklist_requisito_nivel').val(opcion.data('nivel'));
		$('#checklist_requisito_ordenlista').val(opcion.data('lista'));
	};

});

$(document).ready(function () {

	$(document).on('click', '.elegir-requisito', objCLRequisito.elegir);

	$('#btnNuevoRequisito').click(objCLRequisito.nuevo);

	$('#btnGuardarRequisito').click(objCLRequisito.guardar);

	$('#btnEliminarRequisito').click(objCLRequisito.eliminar);

	$('#checklist_padre_requisito').change(objCLRequisito.actualizarOrdenLista);

	$('#checklist_requisito_orden').change(objCLRequisito.elegirOrdenLista);

});
