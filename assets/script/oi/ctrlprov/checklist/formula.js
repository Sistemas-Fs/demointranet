/*!
 *
 * @version 1.0.0
 */

const objCLFormula = {};

$(function() {

	/**
	 * Agrega un nuevo cliente
	 */
	objCLFormula.agregar = function() {
		const tabla = $('#tblFormula tbody');
		const position = tabla.find('tr').length;
		tabla.append(objCLFormula._agregar(position));
		objCLFormula.agregarBusqueda($(document.getElementById('checklist_formula_id[' + position + ']')));
	};

	/**
	 * MEtodo para agregar un nuevo cliente
	 * @param position
	 * @returns {string}
	 */
	objCLFormula._agregar = function(position) {
		let fila = '<tr data-position="' + position + '" >';
		fila += '<td class="text-left" >';
		fila += '<select class="custom-select" id="checklist_formula_id[' + position + ']" name="checklist_formula_id[' + position + ']" ></select>';
		fila += '</td>';
		fila += '<td class="text-left" >';
		fila += '<select class="custom-select" id="checklist_formula_estado[' + position + ']" name="checklist_formula_estado[' + position + ']" >';
		fila += '<option value="A" >Activo</option>';
		fila += '<option value="I" >Inactivo</option>';
		fila += '</select>';
		fila += '</td>';
		fila += '<td class="text-left" >';
		fila += '<buttton type="button" role="button" class="btn btn-danger btn-agregar-formula" >';
		fila += '<i class="fa fa-save" ></i> Guardar';
		fila += '</buttton>';
		fila += '</td>';
		fila += '<td class="text-left" >';
		fila += '<buttton type="button" role="button" class="btn btn-secondary btn-quitar-formula" >';
		fila += '<i class="fa fa-trash" ></i> Eliminar';
		fila += '</buttton>';
		fila += '</td>';
		fila += '</tr>';
		return fila;
	};

	/**
	 * MEtodo para agregar un nuevo cliente
	 * @param position
	 * @param id
	 * @param formula
	 * @param sregistro
	 * @returns {string}
	 */
	objCLFormula.imprimirLista = function(position, id, formula, sregistro) {
		const estado = (sregistro === 'A') ? 'Activo' : 'Inactivo';
		let fila = '<tr data-position="' + position + '" data-id="' + id + '" >';
		fila += '<td class="text-left" >';
		fila += '<input type="text" class="form-control" maxlength="0" readonly value="' + formula + '" >';
		fila += '</td>';
		fila += '<td class="text-left" >';
		fila += '<input type="text" class="form-control" maxlength="0" readonly value="' + estado + '" >';
		fila += '</td>';
		fila += '<td class="text-left" colspan="2" >';
		if (sregistro === 'A') {
			fila += '<buttton type="button" role="button" class="btn btn-secondary btn-cambiar-estado" data-estado="I" >';
			fila += '<i class="fa fa-sync-alt" ></i> Inactivar';
			fila += '</buttton>';
		} else {
			fila += '<buttton type="button" role="button" class="btn btn-secondary btn-cambiar-estado" data-estado="A" >';
			fila += '<i class="fa fa-sync-alt" ></i> Activar';
			fila += '</buttton>';
		}
		fila += '</td>';
		fila += '</tr>';
		return fila;
	};

	/**
	 * Elimina visualmente la formula
	 */
	objCLFormula.quitar = function() {
		const boton = $(this);
		const fila = boton.parents('tr');
		fila.hide();
	};

	/**
	 * Busqueda de formulas
	 * @param objDOM
	 */
	objCLFormula.agregarBusqueda = function(objDOM) {
		objDOM.select2({
			ajax: {
				url: baseurl + 'oi/ctrlprov/checklist/cformula/autocompletado',
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						busqueda: params.term,
					};
				},
				processResults: function (data) {
					return {results: data.items};
				},
				cache: true
			},
			escapeMarkup: function (markup) {
				return markup;
			}, // let our custom formatter work
			minimumInputLength: 0,
			// Aqi se muestran todos los resultados de la busqueda
			templateResult: function (res) {
				return res.text;
			},
			templateSelection: function (res) {
				if (res.id !== "") {
					if (typeof __elegirFormula == "function") {
						__elegirFormula(res);
					}
					return res.text;
				} else {
					return 'Elegir';
				}
			},
			placeholder: "Search",
			allowClear: true,
			width: '100%',
			dropdownParent: $('#formulaCheckList'),
		});
	};

	/**
	 * Guarda el cliente asociado
	 */
	objCLFormula.guardar = function() {
		const boton = $(this);
		const fila = boton.parents('tr');
		const position = fila.data('position');
		const idFormula = document.getElementById('checklist_formula_id[' + position + ']').value;
		const estado = document.getElementById('checklist_formula_estado[' + position + ']').value;
		if (!idFormula) {
			objPrincipal.notify('error', 'Debes elegir una formula');
		} else {
			$.ajax({
				url: baseurl + 'oi/ctrlprov/checklist/cformula/guardar',
				method: 'POST',
				data: {
					idCheckList: $('#checklist_id').val(),
					idFormula: idFormula,
					estado: estado,
				},
				dataType: 'json',
				beforeSend: function() {
					objPrincipal.botonCargando(boton);
				}
			}).done(function() {
				objCLFormula.listar();
			}).fail(function(jhxr) {
				const message = (jhxr && jhxr.responseJSON && jhxr.responseJSON.message)
					? jhxr.responseJSON.message
					: 'Error en el proceso de ejecución.';
				objPrincipal.notify('error', message);
			}).always(function() {
				objPrincipal.liberarBoton(boton);
			});
		}
	};

	/**
	 * Lista las formulas de un checklist
	 */
	objCLFormula.listar = function() {
		$.ajax({
			url: baseurl + 'oi/ctrlprov/checklist/cformula/listar',
			method: 'POST',
			data: {
				idCheckList: $('#checklist_id').val(),
			},
			dataType: 'json',
			beforeSend: function() {}
		}).done(function(res) {
			const tabla = $('#tblFormula tbody');
			let position = tabla.find('tr').length;
			let filas = '';
			console.log(res);
			if (res.items && Array.isArray(res.items)) {
				res.items.forEach(function(item) {
					filas += objCLFormula.imprimirLista(position, item.CFORMULAEVALUACION, item.DFORMULA, item.SREGISTRO);
					++position;
				});
			}
			tabla.html(filas);
		}).fail(function(jhxr) {
			const message = (jhxr && jhxr.responseJSON && jhxr.responseJSON.message)
				? jhxr.responseJSON.message
				: 'Error en el proceso de ejecución.';
			objPrincipal.notify('error', message);
		});
	};

	/**
	 * Cambiar estado de la formula
	 */
	objCLFormula.cambiarEstado = function () {
		const boton = $(this);
		const fila = boton.parents('tr');
		const idFormula = fila.data('id');
		const estado = boton.data('estado');
		$.ajax({
			url: baseurl + 'oi/ctrlprov/checklist/cformula/cambiar_estado',
			method: 'POST',
			data: {
				idCheckList: $('#checklist_id').val(),
				idFormula: idFormula,
				estado: estado,
			},
			dataType: 'json',
			beforeSend: function() {
				objPrincipal.botonCargando(boton);
			}
		}).done(function() {
			objCLFormula.listar();
		}).fail(function(jhxr) {
			const message = (jhxr && jhxr.responseJSON && jhxr.responseJSON.message)
				? jhxr.responseJSON.message
				: 'Error en el proceso de ejecución.';
			objPrincipal.notify('error', message);
		}).always(function() {
			objPrincipal.liberarBoton(boton);
		});
	};

});

$(document).ready(function() {

	$('#btnFormula').click(objCLFormula.agregar);

	$('#formulaCheckList').on('show.bs.modal', function () {
		if (parseInt($('#checklist_id').val()) <= 0) {
			$('#formulaCheckList').modal('hide');
		} else {
			$('#formula_checklist_codigo').val($('#checklist_id').val());
			$('#formula_checklist_descripcion').val($('#checklist_descripcion').val());
			objCLFormula.listar();
		}
	});

	$(document).on('click', '.btn-agregar-formula', objCLFormula.guardar);

	$(document).on('click', '.btn-quitar-formula', objCLFormula.quitar);

	$(document).on('click', '.btn-cambiar-estado', objCLFormula.cambiarEstado);

});
