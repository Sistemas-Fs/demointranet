/*!
 *
 * @version 1.0.0
 */

const objCLLista = {};

$(function () {

	objCLLista.obtenerArea = function () {
		$.ajax({
			url: baseurl + 'oi/ctrlprov/checklist/cchecklist/recursos_area',
			method: 'POST',
			data: {},
			dataType: 'json',
			beforeSend: function () {
			},
		}).done(function (res) {
			let opcion = '<option value="" >::Elegir::</option>';
			if (res && Array.isArray(res)) {
				res.forEach(function (item) {
					opcion += '<option value="' + item.id + '" >' + item.text + '</option>';
				});
			}
			$('#filtro_area').html(opcion);
		}).fail(function () {
			objPrincipal.notify('error', 'Error al buscar las areas.');
		});
	};

	objCLLista.cambiarArea = function () {
		$.ajax({
			url: baseurl + 'oi/ctrlprov/checklist/cchecklist/recursos_servicio',
			method: 'GET',
			data: {
				area: $(this).val(),
			},
			dataType: 'json',
			beforeSend: function () {
			},
		}).done(function (res) {
			let opcion = '<option value="" >::Elegir::</option>';
			if (res && Array.isArray(res)) {
				res.forEach(function (item) {
					opcion += '<option value="' + item.id + '" >' + item.text + '</option>';
				});
			}
			$('#filtro_servicio').html(opcion);
		}).fail(function () {
			objPrincipal.notify('error', 'Error al buscar los servicios.');
		});
	};

	objCLLista.lista = function () {
		const boton = $('#btnBuscar');
		objPrincipal.botonCargando(boton);
		$('#tblInspecciones').DataTable({
			'bJQueryUI': true,
			'scrollY': '650px',
			'scrollX': true,
			'processing': true,
			'bDestroy': true,
			'paging': true,
			'info': true,
			'filter': true,
			'ajax': {
				"url": baseurl + 'oi/ctrlprov/checklist/cchecklist/lista',
				"type": "POST",
				"data": function (d) {
					d.busqueda = $('#filtro_texto').val();
					d.carea = $('#filtro_area').val();
					d.cservicio = $('#filtro_servicio').val();
				},
				dataSrc: function (data) {
					objPrincipal.liberarBoton(boton);
					return data.data;
				},
				error: function () {
					objPrincipal.alert('warning', 'Error en el proceso de ejecución.', 'Vuelva a intentarlo más tarde.');
					objPrincipal.liberarBoton(boton);
				}
			},
			'columns': [
				{
					"orderable": false,
					render: function (data, type, row) {
						const rowId = 'dropdown-' + row.cchecklist;
						let htmlRow = '<div class="dropdown">';
						htmlRow += '<button type="button" class="btn btn-secondary btn-sm dropdown-toggle" role="button" id="' + rowId + '" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
						htmlRow += '<i class="fa fa-bars" ></i> Opciones';
						htmlRow += '</button>';
						htmlRow += '<div class="dropdown-menu" aria-labelledby="' + rowId + '">';
						htmlRow += '<button type="button" class="btn btn-sm dropdown-item editar-checklist" data-codigo="' + row.cchecklist + '" ><i class="fa fa-edit" ></i> Editar</button>';
						htmlRow += '<button type="button" class="btn btn-sm dropdown-item eliminar-checklist" data-codigo="' + row.cchecklist + '" ><i class="fa fa-trash" ></i> Eliminar</button>';
						htmlRow += '</div>';
						htmlRow += '</div>';
						return htmlRow;
					},
				},
				{data: 'cchecklist', orderable: false, targets: 1},
				{data: 'dchecklist', orderable: false, targets: 2},
				{data: 'darea', orderable: false, targets: 3},
				{data: 'dservicio', orderable: false, targets: 4},
				{data: 'dsistema', orderable: false, targets: 5},
				{data: 'dnorma', orderable: false, targets: 6},
				{data: 'dsubnorma', orderable: false, targets: 7},
				{data: 'scerrado', orderable: false, targets: 8},
				{data: 'scerrado', orderable: false, targets: 9},
				{data: 'suso', orderable: false, targets: 10},
				{data: 'speso', orderable: false, targets: 11},
				{
					"orderable": false,
					render: function (data, type, row) {
						return (row.sregistro === 'A') ? 'ACTIVO' : 'INACTIVO';
					},
				},
			],
			"columnDefs": [
				{
					"defaultContent": " ",
					"targets": "_all"
				},
			],
		});
	};

	objCLLista.editar = function () {
		const el = $(this);
		const id = el.data('codigo');
		objCLGenerar.editarFormulario(id);
	};

	/**
	 * Elimina el checklist
	 */
	objCLLista.eliminar = function() {
		const el = $(this);
		const idCheckList = el.data('codigo');
		Swal.fire({
			type: 'warning',
			title: 'Eliminar CheckList',
			text: '¿Estas seguro(a) de eliminar?',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Si',
			cancelButtonText: 'Cancelar',
		}).then((result) => {
			if (result.value) {
				$.ajax({
					url: baseurl + 'oi/ctrlprov/checklist/cchecklist/eliminar',
					method: 'POST',
					data: {
						idCheckList: idCheckList,
					},
					dataType: 'json',
					beforeSend: function () {}
				}).done(function (response) {
					objPrincipal.notify('success', response.message);
					objCLLista.lista();
				}).fail(function (jhxr) {
					const message = (jhxr && jhxr.responseJSON && jhxr.responseJSON.message)
						? jhxr.responseJSON.message
						: 'Error en el proceso de ejecución.';
					objPrincipal.notify('error', message);
				});
			}
		});
	};

});

$(document).ready(function () {

	objCLLista.obtenerArea();

	$('#filtro_area').change(objCLLista.cambiarArea);

	$('#btnBuscar').click(objCLLista.lista);

	objCLLista.lista();

	$(document).on('click', '.editar-checklist', objCLLista.editar);

	$(document).on('click', '.eliminar-checklist', objCLLista.eliminar);

});
