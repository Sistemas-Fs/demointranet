

var otblListCapacita, otblListRegcapadet, otblListRegprogr, otblListParticipantes, otblListAdjuntos;
var varfdesde = '%', varfhasta = '%';

$(document).ready(function() {    
    $('#tabcapa a[href="#tabcapa-list-tab"]').attr('class', 'disabled');
    $('#tabcapa a[href="#tabcapa-reg-tab"]').attr('class', 'disabled active');
    $('#tabcapa a[href="#tabcapa-parti-tab"]').attr('class', 'disabled active');

    $('#tabcapa a[href="#tabcapa-list-tab"]').not('#store-tab.disabled').click(function(event){
        $('#tabcapa a[href="#tabcapa-list"]').attr('class', 'active');
        $('#tabcapa a[href="#tabcapa-reg"]').attr('class', '');
        $('#tabcapa a[href="#tabcapa-parti"]').attr('class', '');
        return true;
    });
    $('#tabcapa a[href="#tabcapa-reg-tab"]').not('#bank-tab.disabled').click(function(event){
        $('#tabcapa a[href="#tabcapa-reg"]').attr('class' ,'active');
        $('#tabcapa a[href="#tabcapa-list"]').attr('class', '');
        $('#tabcapa a[href="#tabcapa-parti"]').attr('class', '');
        return true;
    });
    $('#tabcapa a[href="#tabcapa-parti-tab"]').not('#bank-tab.disabled').click(function(event){
        $('#tabcapa a[href="#tabcapa-parti"]').attr('class' ,'active');
        $('#tabcapa a[href="#tabcapa-list"]').attr('class', '');
        $('#tabcapa a[href="#tabcapa-reg"]').attr('class', '');
        return true;
    });
    
    $('#tabcapa a[href="#tabcapa-list"]').click(function(event){return false;});
    $('#tabcapa a[href="#tabcapa-reg"]').click(function(event){return false;});
    $('#tabcapa a[href="#tabcapa-parti"]').click(function(event){return false;});
    
    $('#txtFDesde,#txtFHasta,#mtxtFCapaini').datetimepicker({
        format: 'DD/MM/YYYY',
        daysOfWeekDisabled: [0],
        locale:'es'
    });

    fechaActual();

    /*LLENADO DE COMBOS*/

    $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"pt/cptcapacitacion/getclientecapa",
        dataType: "JSON",
        async: true,
        success:function(result)
        {
            $('#cboClie').html(result);
        },
        error: function(){
            alert('Error, No se puede autenticar por error');
        }
    });
    $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"pt/cptcapacitacion/getptcursos",
        dataType: "JSON",
        async: true,
        success:function(result)
        {
            $('#cboCursos').html(result);
        },
        error: function(){
            alert('Error, No se puede autenticar por error');
        }
    });

    
});

fechaActual = function(){
    var fecha = new Date();		
    var fechatring = ("0" + fecha.getDate()).slice(-2) + "/" + ("0"+(fecha.getMonth()+1)).slice(-2) + "/" +fecha.getFullYear() ;

    $('#txtFDesde').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY') );
    $('#txtFHasta').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY') );

};
	
$('#txtFDesde').on('change.datetimepicker',function(e){	
    
    $('#txtFHasta').datetimepicker({
        format: 'DD/MM/YYYY',
        daysOfWeekDisabled: [0],
        locale:'es'
    });	

    var fecha = moment(e.date).format('DD/MM/YYYY');		
    
    $('#txtFHasta').datetimepicker('minDate', fecha);
    $('#txtFHasta').datetimepicker('date', fecha);

});

$("#chkFreg").on("change", function () {
    if($("#chkFreg").is(":checked") == true){ 
        $("#txtFIni").prop("disabled",false);
        $("#txtFFin").prop("disabled",false);
        
        varfdesde = '';
        varfhasta = '';
    }else if($("#chkFreg").is(":checked") == false){ 
        $("#txtFIni").prop("disabled",true);
        $("#txtFFin").prop("disabled",true);
        
        varfdesde = '%';
        varfhasta = '%';
    }; 
});

$("#btnBuscar").click(function (){
    
    if(varfdesde != '%'){ varfdesde = $('#txtFIni').val(); }
    if(varfhasta != '%'){ varfhasta = $('#txtFFin').val(); } 
      
    otblListCapacita = $('#tblListCapacita').DataTable({ 
        'responsive'    : false,
        'bJQueryUI'     : true,
        'scrollY'     	: '400px',
        'scrollX'     	: true, 
        'paging'      	: true,
        'processing'  	: true,     
        'bDestroy'    	: true,
        'AutoWidth'     : false,
        'info'        	: false,
        'filter'      	: true, 
        'ordering'		: false,
        'stateSave'     : true,
        'ajax'	: {
            "url"   : baseurl+"pt/cptcapacitacion/getbuscarcapa/",
            "type"  : "POST", 
            "data": function ( d ) {
                d.ccliente      = $('#cboClie').val();
                d.fdesde        = varfdesde; 
                d.fhasta        = varfhasta;   
                d.idcurso       = $('#cboCursos').val(); 
            },     
            dataSrc : ''        
        },
        'columns'	: [
            {"class" : "col-xxs", orderable : false, data : null, targets : 0},
            {"orderable": false, data: 'drazonsocial', targets: 1, "class": "col-m"},   
            {"orderable": false, data: 'fcapacitacion', targets: 3},
            {"orderable": false, data: 'descripcion', targets: 4, "class": "col-m"},
            {"orderable": false, "class": "col-xs", 
              render:function(data, type, row){                
                  return  '<div>'+ 
                    '<a id="aDelCapa" href="'+row.idptcapacitacion+'" title="Eliminar" style="cursor:pointer; color:#FF0000;"><span class="fas fa-trash-alt" aria-hidden="true"> </span></a>'+      
                    '&nbsp;'+
                    ' <a data-toggle="modal" title="Editar" style="cursor:pointer; color:#3c763d;" onClick="javascript:selCapa(\''+row.idptcapacitacion +'\',\''+row.ccliente+'\',\''+row.cestablecimiento+'\',\''+row.fcapacitacion+'\',\''+row.hduracion+'\',\''+row.fduracion+'\',\''+row.idptcapacurso+'\',\''+row.comentarios+'\');"><span class="fas fa-external-link-alt" aria-hidden="true"> </span></a>'+
                    '</div>'
              }
            },  
        ],  
    });   
    // Enumeracion 
    otblListCapacita.on( 'order.dt search.dt', function () { 
        otblListCapacita.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i+1;
          } );
    }).draw();  
});

$('#btnNuevo').click(function(){
    
    $('#tabcapa a[href="#tabcapa-reg"]').tab('show'); 
    $('#frmRegCapa').trigger("reset");
    $('#hdnAccionregcapa').val('N'); 
    $('#mtxtidcapa').val(0); 
    
    iniRegCapa();

    fechaActualCapa();

    $('#btnParticiopantes').hide();    
    $('#btnAdjuntos').hide();      
    $('#regAdjuntos').hide();  
});

fechaActualCapa = function(){
    var fecha = new Date();		
    var fechatring = ("0" + fecha.getDate()).slice(-2) + "/" + ("0"+(fecha.getMonth()+1)).slice(-2) + "/" +fecha.getFullYear() ;

    $('#mtxtFCapaini').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY') );

};

iniRegCapa = function(ccliente,idcurso){
    
    $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"pt/cptcapacitacion/getclienteinternopt",
        dataType: "JSON",
        async: true,
        success:function(result)
        {
            $('#cboregClie').html(result);
            $('#cboregClie').val(ccliente).trigger("change");
        },
        error: function(){
            alert('Error, No se puede autenticar por error');
        }
    });    
   
    listcbocurso(idcurso);
    /*var params = { "ccliente":Ccliente };
    $.ajax({
            type: 'ajax',
            method: 'post',
            url: baseurl+"at/capa/cregcapa/buscar_establexcliente",
            dataType: "JSON",
            async: true,
            data: params,
            success:function(result)
            {
            $("#cboregEstab").html(result);           
            $('#cboregEstab').val(idestab).trigger("change");
            },
            error: function(){
            alert('Error, no se puede cargar la lista desplegable de establecimiento');
        }
    });*/
};

listcbocurso = function(idcurso){
    
    $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"pt/cptcapacitacion/getptcursos",
        dataType: "JSON",
        async: true,
        success:function(result)
        {
            $('#mcboCursos').html(result);
            $('#mcboCursos').val(idcurso).trigger("change");
        },
        error: function(){
            alert('Error, No se puede autenticar por error');
        }
    }); 
}

$("#cboregClie").change(function(){
    var v_mcboClie = $('#cboregClie').val();

    var params = { "ccliente":v_mcboClie };

        $.ajax({
            type: 'ajax',
            method: 'post',
            url: baseurl+"at/capa/cregcapa/buscar_establexcliente",
            dataType: "JSON",
            async: true,
            data: params,
            success:function(result)
            {
                $("#cboregEstab").html(result);           
            },
            error: function(){
                alert('Error, no se puede cargar la lista desplegable de establecimiento');
            }
        });    
});

$('#frmRegCapa').submit(function(event){
    event.preventDefault();
    
    var request = $.ajax({
        url:$('#frmRegCapa').attr("action"),
        type:$('#frmRegCapa').attr("method"),
        data:$('#frmRegCapa').serialize(),
        error: function(){
            Vtitle = 'No se puede registrar por error';
            Vtype = 'error';
            sweetalert(Vtitle,Vtype);
        }
    });
    request.done(function( respuesta ) {
        var posts = JSON.parse(respuesta);        
        $.each(posts, function() { 
            $('#mtxtidcapa').val(this.id_capacitacion);
            Vtitle = this.respuesta;
            Vtype = 'success';
            sweetalert(Vtitle,Vtype);
            recuperaListadjuntos();
            $('#btnParticiopantes').show();  
            $('#btnAdjuntos').show();               
            $('#regAdjuntos').show();  
        });
    });
});

$("#mbtnnewcurso").click(function (){
    $('#frmMantcurso').trigger("reset");

    $("#modalMantcurso").modal('show');

    $('#mhdnAccioncapacurso').val('N');
});
$('#modalMantcurso').on('show.bs.modal', function (e) {
    $('#frmMantcurso').validate({        
        rules: {
            txtdescripcion: {
              required: true,
            },
        },
        messages: {
            txtdescripcion: {
              required: "Por Favor ingrese Nombre del curso"
            },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        },        
        submitHandler: function (form) {
            const botonEvaluar = $('#mbtnGMantcurso');
            var request = $.ajax({
                url:$('#frmMantcurso').attr("action"),
                type:$('#frmMantcurso').attr("method"),
                data:$('#frmMantcurso').serialize(),
                error: function(){
                    Vtitle = 'Error en Guardar!!!';
                    Vtype = 'error';
                    sweetalert(Vtitle,Vtype); 
                    objPrincipal.liberarBoton(botonEvaluar);
                },
                beforeSend: function() {
                    objPrincipal.botonCargando(botonEvaluar);
                }
            });
            request.done(function( respuesta ) {
                var posts = JSON.parse(respuesta);
                
                $.each(posts, function() {
                    Vtitle = 'Se Grabo Correctamente!!!';
                    Vtype = 'success';
                    sweetalert(Vtitle,Vtype); 
                      
                    var vidcapacurso = this.id
                    listcbocurso(vidcapacurso);

                    objPrincipal.liberarBoton(botonEvaluar);    
                    $('#mbtnCMantcurso').click();    
                });
            });
            return false;
        }
    });
});

$('#btnAdjuntos').click(function(){
    $('#modalAdjunto').modal('show');
    $('#frmSubadjunto').trigger("reset");
    $('#mhdnIdCapaadjunto').val($('#mtxtidcapa').val()); 
    $('#mtxtFinicioadjunto').val($('#mtxtFinicio').val()); 
});

$('#btnRetornarLista').click(function(){
    $('#tabcapa a[href="#tabcapa-list"]').tab('show'); 
    $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"pt/cptcapacitacion/getclientecapa",
        dataType: "JSON",
        async: true,
        success:function(result)
        {
            $('#cboClie').html(result);
        },
        error: function(){
            alert('Error, No se puede autenticar por error');
        }
    });
    $('#btnBuscar').click();
});

adjArchivo=function(){
    var archivoInput = document.getElementById('stxtArchivoadj');
    var archivoRuta = archivoInput.value;
    var extPermitidas = /(.pdf|.docx|.xlsx|.doc|.xls)$/i;
    
    var filename = $('#stxtArchivoadj').val().replace(/.*(\/|\\)/, '');
    $('#stxtNomarchadj').val(filename);

    if(!extPermitidas.exec(archivoRuta)){
        alert('Asegurese de haber seleccionado un PDF, DOCX, XSLX');
        archivoInput.value = '';  
        $('#stxtNomarchadj').val('');
        return false;
    }      

    var parametrotxt = new FormData($("#frmSubadjunto")[0]);
    var request = $.ajax({
        data: parametrotxt,
        method: 'post',
        url: baseurl+"pt/cptcapacitacion/adjArchivo/",
        dataType: "JSON",
        async: true,
        contentType: false,
        processData: false,
        error: function(){
            alert('Error, no se cargó Archivo');
        }
    });
    request.done(function( respuesta ) {
        recuperaListadjuntos();
        $('#mbtnCSubadjunto').click();
    });
};

recuperaListadjuntos = function(){
    otblListAdjuntos = $('#tblListAdjuntos').DataTable({
        'responsive'    : true,
        'bJQueryUI'     : true,
        'scrollY'     	: '300px',
        'scrollX'     	: true, 
        'paging'      	: true,
        'processing'  	: true,     
        'bDestroy'    	: true,
        'AutoWidth'     : false,
        'info'        	: true,
        'filter'      	: true, 
        'ordering'		: false,  
        'stateSave'     : true,
        'ajax'	: {
            "url"   : baseurl+"pt/cptcapacitacion/getlistaradjuntos/",
            "type"  : "POST", 
            "data": function ( d ) {
                d.idptcapacitacion      = $('#mtxtidcapa').val();
            },     
            dataSrc : ''        
        },
        'columns'	: [
            {
              "class"     :   "col-xxs",
              orderable   :   false,
              data        :   null,
              targets     :   0,
            },
            {"orderable": false, data: 'tipoadjunto', targets: 1},   
            {"orderable": false, data: 'descripcion', targets: 3, "class": "col-xm"},
            {"orderable": false, data: 'archivo_capaadj', targets: 4, "class": "col-lm"}, 
            {"orderable": false, "class": "col-xs", 
              render:function(data, type, row){                
                  return  '<div>'+ 
                    '<a id="aDelCapaadj" href="'+row.idptcapacitacion+'" idcapaadj="'+row.idptcapaadjunto+'" title="Eliminar" style="cursor:pointer; color:#FF0000;"><span class="fas fa-trash-alt" aria-hidden="true"> </span></a>'+      
                    '</div>'
              }
            }, 
        ],  
    });
    // Enumeracion 
    otblListAdjuntos.on( 'order.dt search.dt', function () { 
        otblListAdjuntos.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
              cell.innerHTML = i+1;
              } );
    }).draw();
};


selCapa= function(id_capa,ccliente,cestablecimiento,fcapacitacion,hduracion,fduracion,idcurso,comentarios){  
    $('#tabcapa a[href="#tabcapa-reg"]').tab('show'); 
    $('#frmRegCapa').trigger("reset");

    $('#hdnAccionregcapa').val('A'); 
    $('#mtxtidcapa').val(id_capa); 

    $('#mtxtFinicio').val(fcapacitacion);  
    $('#mtxthduracion').val(hduracion); 
    $('#mtxtfduracion').val(fduracion); 
    $('#mtxtComentarios').val(comentarios);    
    
    iniRegCapa(ccliente,idcurso);
 
    $('#btnParticiopantes').show();    
    $('#btnAdjuntos').show();    
    $('#regAdjuntos').show(); 

    recuperaListadjuntos();
    
};
   
$("body").on("click","#aDelCapa",function(event){
    event.preventDefault();
    id_capa = $(this).attr("href");

    Swal.fire({
        title: 'Confirmar Eliminación',
        text: "¿Está seguro de eliminar la Capacitacion?",
        icon: 'error',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, bórralo!'
    }).then((result) => {
        if (result.value) {
            $.post(baseurl+"pt/cptcapacitacion/delcapa/", 
            {
                idptcapacitacion   : id_capa,
            },      
            function(data){     
                otblListCapacita.ajax.reload(null,false); 
                Vtitle = 'Se Elimino Correctamente';
                Vtype = 'success';
                sweetalert(Vtitle,Vtype);      
            });
        }
    }) 
});
   
$("body").on("click","#aDelCapaadj",function(event){
    event.preventDefault();
    id_capa = $(this).attr("href");
    idcapaadj = $(this).attr("idcapaadj");

    Swal.fire({
        title: 'Confirmar Eliminación',
        text: "¿Está seguro de eliminar al Adjunto?",
        icon: 'error',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, bórralo!'
    }).then((result) => {
        if (result.value) {
            $.post(baseurl+"pt/cptcapacitacion/delcapaadj/", 
            {
                idptcapacitacion   : id_capa,
                idptcapaadjunto   : idcapaadj,
            },      
            function(data){     
                otblListAdjuntos.ajax.reload(null,false); 
                Vtitle = 'Se Elimino Correctamente';
                Vtype = 'success';
                sweetalert(Vtitle,Vtype);      
            });
        }
    }) 
});

$('#btnParticiopantes').click(function(){
    $('#tabcapa a[href="#tabcapa-parti"]').tab('show');
    var vidcapa = $('#mtxtidcapa').val();
    recuperaListparti(vidcapa);
    $("#mtxtDniparti").prop({readonly:false});
    $("#mtxtPerparti").prop({readonly:true});
    $("#mtxtAppatparti").prop({readonly:true});
    $("#mtxtApmatparti").prop({readonly:true});
    $("#mtxtNotaparti").prop({readonly:false});
    $("#mtxtEmailparti").prop({readonly:true});
    $("#mtxtTelparti").prop({readonly:true}); 
});

recuperaListparti = function(idcapa){
    $('#mhdnIdcapacitaparti').val(idcapa);
    otblListParticipantes = $('#tblListParticipantes').DataTable({ 
        'responsive'    : true,
        'bJQueryUI'     : true,
        'scrollY'     	: '300px',
        'scrollX'     	: true, 
        'paging'      	: true,
        'processing'  	: true,     
        'bDestroy'    	: true,
        'AutoWidth'     : false,
        'info'        	: true,
        'filter'      	: true, 
        'ordering'		: false,  
        'stateSave'     : true,
        'ajax'	: {
            "url"   : baseurl+"pt/cptcapacitacion/getlistparticipante/",
            "type"  : "POST", 
            "data": function ( d ) {
                d.id_capa  = idcapa; 
            },     
            dataSrc : ''        
        },
        'columns'	: [
            {
                "class"     :   "col-xxs",
                orderable   :   false,
                data        :   null,
                targets     :   0
            },
            {"orderable": false, data: 'NRODNI', targets: 1},
            {"orderable": false, data: 'NOMBREPARTI', targets: 2},
            {"orderable": false, data: 'NOTA', targets: 3},
            {"orderable": false, 
              render:function(data, type, row){                
                  return  '<div>'+
                  '<a title="Editar" style="cursor:pointer; color:#3c763d;" onClick="javascript:selParti(\''+row.id_capa+'\',\''+row.id_capaparti+'\',\''+row.id_administrado+'\',\''+row.NOMBREPARTI+'\',\''+row.NRODNI+'\',\''+row.fono_celular+'\',\''+row.email+'\',\''+row.NOTA+'\');"><span class="fas fa-edit fa-2x" aria-hidden="true"> </span> </a>'+
                  '&nbsp;'+
                  '<a id="aDelParti" href="'+row.id_capaparti+'" title="Eliminar" style="cursor:pointer; color:#FF0000;"><span class="fas fa-trash-alt fa-2x" aria-hidden="true"> </span></a>'+      
                  '</div>'
              }
            },   
            {"orderable": false, 
              render:function(data, type, row){
                  var rexamen;
                  if(row.EXAMEN != null) {
                    rexamen = ' <a title="Examen" style="cursor:pointer; color:#1646ec;" href="'+baseurl+row.EXAMEN+'" target="_blank" class="btn btn-outline-secondary btn-sm hidden-xs hidden-sm"><span class="fas fa-cloud-download-alt" aria-hidden="true"> </span> Examen</a>'+
                    ' &nbsp; &nbsp;'
                  } else {
                    rexamen = ' <a data-toggle="modal" title="Examen" style="cursor:pointer; color:#3c763d;" data-target="#modalPresent" onClick="javascript:uploadPresent(\''+row.id_capa+'\',\''+row.id_capadet+'\',\''+row.ccliente+'\',\''+row.fini+'\');"class="btn btn-outline-secondary btn-sm hidden-xs hidden-sm"><span class="fas fa-cloud-upload-alt" aria-hidden="true"> </span> Examen</a>'+
                    ' &nbsp; &nbsp;'
                  }  
                    return  '<div>'+rexamen+
                    '</div>'   
                }
            }, 
            {"orderable": false, 
              render:function(data, type, row){
                  var rcerti;
                  if(row.CERTIFICADO != null) {
                    rcerti = ' <a title="Certificado" style="cursor:pointer; color:#1646ec;" href="'+baseurl+row.CERTIFICADO+'" target="_blank" class="btn btn-outline-secondary btn-sm hidden-xs hidden-sm"><span class="fas fa-cloud-download-alt" aria-hidden="true"> </span> Certificado</a>'+
                    ' &nbsp; &nbsp;'
                  } else {
                    rcerti = ' <a data-toggle="modal" title="Certificado" style="cursor:pointer; color:#3c763d;" data-target="#modalPresent" onClick="javascript:uploadPresent(\''+row.id_capa+'\',\''+row.id_capadet+'\',\''+row.ccliente+'\',\''+row.fini+'\');"class="btn btn-outline-secondary btn-sm hidden-xs hidden-sm"><span class="fas fa-cloud-upload-alt" aria-hidden="true"> </span> Certificado</a>'+
                    ' &nbsp; &nbsp;'
                  }  
                  return  '<div>'+rcerti+
                  '</div>'   
                }
            },       
        ]
    });  
    // Enumeracion 
    otblListParticipantes.on( 'order.dt search.dt', function () { 
        otblListParticipantes.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i+1;
          } );
    }).draw();
};

selParti = function(id_capa,id_capaparti,id_administrado,NOMBREPARTI,NRODNI,fono_celular,email, NOTA){
    $('#mhdnAccionParti').val('A'); 

    $('#mhdnIdparti').val(id_capaparti);
    $('#mhdnIdcapaParti').val(id_capa);
    $('#mhdnIdadmParti').val(id_administrado);
    $('#mtxtPerparti').val(NOMBREPARTI);
    $('#mtxtDniparti').val(NRODNI);
    $('#mtxtNotaparti').val(NOTA);
    $('#mtxtEmailparti').val(email);
    $('#mtxtTelparti').val(fono_celular);

}

$('#btnValidarAdmi').click(function(){
    var v_nrodni = $('#mtxtDniparti').val();
    const boton = $('#btnValidarAdmi');
  
    var parametros = { 
      "nrodni":v_nrodni 
    };
    var request = $.ajax({
      type: 'ajax',
      method: 'post',
      url: baseurl+"pt/cptcapacitacion/getvalidarnrodni",
      dataType: "JSON",
      async: true,
      data: parametros,
      error: function(){
          alert('Error, no se puede proceder con la funcion ValidarAdmi');
          objPrincipal.liberarBoton(boton); 
      },
      beforeSend: function () {
          objPrincipal.botonCargando(boton);
      }
    });      
    request.done(function( respuesta ) {            
        $.each(respuesta, function() {
            var valerta = this.alerta;
            var vflag = this.flag;
            var vidadmin = this.idadmin;
            
            if(vflag == "1"){
              sweetalert(valerta, 'warning'); 
            }else if(vflag == "0"){
              sweetalert(valerta, 'warning');
            }else if(vflag == "2"){
              recuperaradministrado(vidadmin);
            }
        });
        objPrincipal.liberarBoton(boton); 
    });
});

recuperaradministrado = function(a_idadmin){
    var parametros = { 
      "id_administrado":a_idadmin 
    };
    var request = $.ajax({
      type: 'ajax',
      method: 'post',
      url: baseurl+"pt/cptcapacitacion/getadministrado",
      dataType: "JSON",
      async: true,
      data: parametros,
      error: function(){
          alert('Error, no se puede proceder con la funcion recuperar administrado');
      }
    });      
    request.done(function( respuesta ) {            
        $.each(respuesta, function() {        
          adminValido(this.id_administrado, this.nombres, this.ape_paterno, this.ape_materno, this.email, this.fono_celular);
        });
    });
  
}

adminValido = function(id_administrado, nombres, ape_paterno, ape_materno, email, fono_celular){
  
  $('#hdnIdptclie').val(id_administrado);    
  $('#mtxtPerparti').val(nombres);    
  $('#mtxtAppatparti').val(ape_paterno);
  $('#mtxtApmatparti').val(ape_materno);
  $('#mtxtEmailparti').val(email);
  $('#mtxtTelparti').val(fono_celular);    
};
   
$("body").on("click","#aDelParti",function(event){
      event.preventDefault();
      id_capaparti = $(this).attr("href");
  
      Swal.fire({
          title: 'Confirmar Eliminación',
          text: "¿Está seguro de eliminar al participante?",
          icon: 'error',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, bórralo!'
      }).then((result) => {
          if (result.value) {
              $.post(baseurl+"pt/cptcapacitacion/delparti/", 
              {
                  id_capaparti   : id_capaparti,
              },      
              function(data){     
                  otblListParticipantes.ajax.reload(null,false); 
                  Vtitle = 'Se Elimino Correctamente';
                  Vtype = 'success';
                  sweetalert(Vtitle,Vtype);      
              });
          }
      }) 
});

$('#frmRegParti').submit(function(event){
      event.preventDefault();
      
      var request = $.ajax({
          url:$('#frmRegParti').attr("action"),
          type:$('#frmRegParti').attr("method"),
          data:$('#frmRegParti').serialize(),
          error: function(){
              Vtitle = 'No se puede registrar por error';
              Vtype = 'error';
              sweetalert(Vtitle,Vtype);
          }
      });
      request.done(function( respuesta ) {
          var posts = JSON.parse(respuesta);        
          $.each(posts, function() { 
              otblListParticipantes.ajax.reload(null,false);      
              Vtitle = this.respuesta;
              Vtype = 'success';
              sweetalert(Vtitle,Vtype);       
          });
      });
});

$('#btnRetornarReg').click(function(){
    $('#tabcapa a[href="#tabcapa-reg"]').tab('show');  
});



//////////////////////////////////////////////////////////////////////////

$('#btnVercertificados').click(function(){
    var idcapa      = $('#mtxtidcapa').val();
    var tipocerti   = $('#cbotipocerti').val();
    var modelcerti  = $('#cbomodelcerti').val();

    if(tipocerti == 'E' && modelcerti == 'A'){
        window.open(baseurl+"at/capa/cgendoccapa/pdfcertiEmpApro/"+idcapa);
    }else if(tipocerti == 'E' && modelcerti == 'P'){
        window.open(baseurl+"at/capa/cgendoccapa/pdfcertiEmpParti/"+idcapa);
    }else if(tipocerti == 'E' && modelcerti == 'AN'){
        window.open(baseurl+"at/capa/cgendoccapa/pdfcertiEmpAproNota/"+idcapa);
    }else if(tipocerti == 'E' && modelcerti == 'AD'){
        window.open(baseurl+"at/capa/cgendoccapa/pdfcertiEmpAproDni/"+idcapa);
    }else if(tipocerti == 'E' && modelcerti == 'AT'){
        window.open(baseurl+"at/capa/cgendoccapa/pdfcertiEmpAproNotadni/"+idcapa);
    }else if(tipocerti == 'E' && modelcerti == 'PD'){
        window.open(baseurl+"at/capa/cgendoccapa/pdfcertiEmpPartiDni/"+idcapa);
    }else if(tipocerti == 'IP' && modelcerti == 'A'){
        window.open(baseurl+"at/capa/cgendoccapa/pdfcertiIndivProgApro/"+idcapa);
    }else if(tipocerti == 'IP' && modelcerti == 'P'){
        window.open(baseurl+"at/capa/cgendoccapa/pdfcertiIndivProgParti/"+idcapa);
    }else if(tipocerti == 'IP' && modelcerti == 'AN'){
        window.open(baseurl+"at/capa/cgendoccapa/pdfcertiIndivProgAproNota/"+idcapa);
    }else if(tipocerti == 'IP' && modelcerti == 'AD'){
        window.open(baseurl+"at/capa/cgendoccapa/pdfcertiIndivProgAproDni/"+idcapa);
    }else if(tipocerti == 'IP' && modelcerti == 'AT'){
        window.open(baseurl+"at/capa/cgendoccapa/pdfcertiIndivProgAproNotaDni/"+idcapa);
    }else if(tipocerti == 'IP' && modelcerti == 'PD'){
        window.open(baseurl+"at/capa/cgendoccapa/pdfcertiIndivProgPartiDni/"+idcapa);
    }else if(tipocerti == 'IC' && modelcerti == 'A'){
        window.open(baseurl+"at/capa/cgendoccapa/pdfcertiIndivCurApro/"+idcapa);
    }else if(tipocerti == 'IC' && modelcerti == 'P'){
        window.open(baseurl+"at/capa/cgendoccapa/pdfcertiIndivCurParti/"+idcapa);
    }else if(tipocerti == 'IC' && modelcerti == 'AN'){
        window.open(baseurl+"at/capa/cgendoccapa/pdfcertiIndivCurAproNota/"+idcapa);
    }else if(tipocerti == 'IC' && modelcerti == 'AD'){
        window.open(baseurl+"at/capa/cgendoccapa/pdfcertiIndivCurAproDni/"+idcapa);
    }else if(tipocerti == 'IC' && modelcerti == 'AT'){
        window.open(baseurl+"at/capa/cgendoccapa/pdfcertiIndivCurAproNotaDni/"+idcapa);
    }else if(tipocerti == 'IC' && modelcerti == 'PD'){
        window.open(baseurl+"at/capa/cgendoccapa/pdfcertiIndivCurPartiDni/"+idcapa);
    }
});

/////////////////////////////////////////////////////////////////////////////////


$('#modalImportparti').on('shown.bs.modal', function (e) {
    var IdcapaParti = $('#mhdnIdcapacitaparti').val();
    $('#mhdnIdCapamigra').val(IdcapaParti);
});

adjFile=function(){
    var archivoInput = document.getElementById('fileMigra');
    var archivoRuta = archivoInput.value;
    var extPermitidas = /(.xls)$/i;
    
    var filename = $('#fileMigra').val().replace(/.*(\/|\\)/, '');
    $('#txtFile').val(filename);

    if(!extPermitidas.exec(archivoRuta)){
        alert('Asegurese de haber seleccionado un XLS');
        archivoInput.value = '';  
        $('#txtFile').val('');
        return false;
    }    
};

$('#mbtnGUpload').click(function(){    
    var parametrotxt = new FormData($("#frmImport")[0]);
    var request = $.ajax({
        data: parametrotxt,
        method: 'post',
        url: baseurl+"at/capa/cregcapa/import_parti",
        dataType: "JSON",
        async: true,
        contentType: false,
        processData: false,
        error: function(){
            alert('Error, no se cargó el archivo');
        }
    });
    request.done(function( respuesta ) {
        otblListParticipantes.ajax.reload(null,false);      
        Vtitle = this.respuesta;
        Vtype = 'success';
        sweetalert(Vtitle,Vtype);    
        $('#frmImport').trigger("reset");                 
        $('#mbtnCImportparti').click();
    });
});
