var otblListCarta, otblDetacarta;
var varfdesde = '%', varfhasta = '%';
var iduser = $('#mtxtidusucarta').val();

var dt = new Date();
var year = dt.getFullYear();


$(document).ready(function() {
    $('#txtFDesde,#txtFHasta').datetimepicker({
        format: 'DD/MM/YYYY',
        daysOfWeekDisabled: [0],
        locale:'es'
    });

    $('#mtxtFregcarta').datetimepicker({
        format: 'DD/MM/YYYY',
        daysOfWeekDisabled: [0],
        locale:'es'
    });

    fechaActual();

    /*LLENADO DE COMBOS*/         
    $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"pt/ccartas/getclientecarta",
        dataType: "JSON",
        async: true,
        success:function(result)
        {
            $('#cboClie').html(result);
        },
        error: function(){
            alert('Error, No se puede autenticar por error');
        }
    });
  

    $('#frmCreaCarta').validate({
        rules: {
            mcboClie: {
            required: true,
          },
        },
        messages: {
            mcboClie: {
            required: "Por Favor escoja un Cliente"
          },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        },        
        submitHandler: function (form) {
            const botonEvaluar = $('#mbtnGCreaCarta');
            var request = $.ajax({
                url:$('#frmCreaCarta').attr("action"),
                type:$('#frmCreaCarta').attr("method"),
                data:$('#frmCreaCarta').serialize(),
                error: function(){
                    Vtitle = 'Error en Guardar!!!';
                    Vtype = 'error';
                    sweetalert(Vtitle,Vtype); 
                },
                beforeSend: function() {
                    objPrincipal.botonCargando(botonEvaluar);
                }
            });
            request.done(function( respuesta ) {
                var posts = JSON.parse(respuesta);
                
                $.each(posts, function() {    
                    objPrincipal.liberarBoton(botonEvaluar);                 
                    $('#mhdnIdCarta').val(this.id_carta);
                    if($('#sArchivo').val() == 'S'){          
                        subirArchivo();
                    }else{      
                        Vtitle = 'Carta Guardada!!!';//this.respuesta;
                        Vtype = 'success';
                        sweetalert(Vtitle,Vtype);                                
                        $('#mbtnCCreaCarta').click();
                        $('#btnBuscar').click();  
                    } 
                    $('#sArchivo').val('N');      
                });
            });
            return false;
        }
    });
});

fechaActual = function(){
    var fecha = new Date();		
    var fechatring = ("0" + fecha.getDate()).slice(-2) + "/" + ("0"+(fecha.getMonth()+1)).slice(-2) + "/" +fecha.getFullYear() ;

    $('#txtFDesde').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY') );
    $('#txtFHasta').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY') );

};
	
$('#txtFDesde').on('change.datetimepicker',function(e){	
    
    $('#txtFHasta').datetimepicker({
        format: 'DD/MM/YYYY',
        daysOfWeekDisabled: [0],
        locale:'es'
    });	

    var fecha = moment(e.date).format('DD/MM/YYYY');		
    
    $('#txtFHasta').datetimepicker('minDate', fecha);
    $('#txtFHasta').datetimepicker('date', fecha);

});

$("#chkFreg").on("change", function () {
    if($("#chkFreg").is(":checked") == true){ 
        $("#txtFIni").prop("disabled",false);
        $("#txtFFin").prop("disabled",false);
        
        varfdesde = '';
        varfhasta = '';

        var fecha = new Date();		
        var fechatring1 = "01/01/" +fecha.getFullYear() ;
        var fechatring2 = ("0" + fecha.getDate()).slice(-2) + "/" + ("0"+(fecha.getMonth()+1)).slice(-2) + "/" +fecha.getFullYear() ;
        $('#txtFDesde').datetimepicker('date', fechatring1);
        $('#txtFHasta').datetimepicker('date', fechatring2);

    }else if($("#chkFreg").is(":checked") == false){ 
        $("#txtFIni").prop("disabled",true);
        $("#txtFFin").prop("disabled",true);
        
        varfdesde = '%';
        varfhasta = '%';

        fechaActual();
    }; 
});

$("#btnBuscar").click(function (){
    
    if(varfdesde != '%'){ varfdesde = $('#txtFIni').val(); }
    if(varfhasta != '%'){ varfhasta = $('#txtFFin').val(); }  


    otblListCarta = $('#tblListCarta').DataTable({           
        "processing"  	: true,
        "bDestroy"    	: true,
        "stateSave"     : true,
        "bJQueryUI"     : true,
        "scrollResize"  : true,
        "scrollY"     	: "400px",
        "scrollX"     	: true,
        "scrollCollapse": false, 
        'AutoWidth'     : false,
        "paging"      	: true,
        "info"        	: true,
        "filter"      	: true, 
        "ordering"		: false,
        "responsive"    : true,
        "select"        : true, 
        'ajax'	: {
            "url"   : baseurl+"pt/ccartas/getbuscarcarta/",
            "type"  : "POST", 
            "data": function ( d ) {
                d.ccliente      = $('#cboClie').val();
                d.fdesde        = varfdesde; 
                d.fhasta        = varfhasta;   
                d.dnrodet       = $('#txtnrodet').val(); 
            },     
            dataSrc : ''        
        },
        'columns'	: [
            {
              "class"     :   "col-xxs",
              orderable   :   false,
              data        :   null,
              targets     :   0
            },
            {"orderable": false, data: 'NROCARTA', targets: 1, "class": "col-s"},
            {"orderable": false, data: 'RAZONSOCIAL', targets: 2, "class": "col-m"},
            {"orderable": false, data: 'DETACARTA', targets: 3, "class": "col-lm"},
            {"orderable": false, data: 'FECHCARTA', targets: 4, "class": "col-s"},
            {responsivePriority: 1, "orderable": false, "class": "col-xs", 
                render:function(data, type, row){
                    return '<div>'+
                    '<a data-toggle="modal" title="Editar" style="cursor:pointer; color:#3c763d;" data-target="#modalCreaCarta" onClick="javascript:selCarta(\''+row.IDCARTA+'\',\''+row.ccliente+'\',\''+row.NROCARTA+'\',\''+row.FECHCARTA+'\',\''+row.DETACARTA+'\',\''+row.idresponsable+'\',\''+row.idptpropuesta+'\',\''+row.archivo_carta+'\',\''+row.ruta_carta+'\',\''+row.descripcion_archivo+'\');"><span class="fas fa-edit" aria-hidden="true"> </span> </a>'+
                    '&nbsp;'+
                    '<a id="aDelPropu" href="'+row.IDCARTA+'" title="Eliminar" style="cursor:pointer; color:#FF0000;"><span class="fas fa-trash-alt" aria-hidden="true"> </span></a>'+      
                    '</div>'
                }
            },            
            {"orderable": false, 
              render:function(data, type, row){
                bfind = true;   
                  return ' <div>'+
                    ' <a data-toggle="modal" title="Adjuntar" style="cursor:pointer; color:#3c763d;" data-target="#modalDetaCarta" onClick="javascript:listarDetCarta(\''+row.IDCARTA+'\',\''+row.FECHCARTA+'\',\''+row.NROCARTA+'\',\''+row.CANTDET+'\');"class="btn btn-outline-primary btn-sm hidden-xs hidden-sm"><span class="fas fa-folder-open" aria-hidden="true"> </span> DOCUMENTOS ADJUNTOS</a>'+
                  '</div>'  
                
              }
            }
        ],
        "columnDefs": [{
            "targets": [1], 
            "data": null, 
            "render": function(data, type, row) {                 
                var yearprop = row.FECHCARTA.substr(-4);
                if(row.archivo_carta != null) {
                    return '<p><a href="'+baseurl+row.ruta_carta+row.archivo_carta+'" target="_blank" class="pull-left">'+row.NROCARTA+'&nbsp;&nbsp;<i class="fas fa-cloud-download-alt" data-original-title="Descargar" data-toggle="tooltip"></i></a><p>';
                }else{
                    return '<p>'+row.NROCARTA+'</p>';
                }                      
            }
        }]
    });   
    // Enumeracion 
    otblListCarta.on( 'order.dt search.dt', function () { 
        otblListCarta.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i+1;
          } );
    }).draw();  
});

$('#modalCreaCarta').on('shown.bs.modal', function (e) { 
    $("#txtRegPropu").prop({readonly:true}); 
    $("#mtxtNrocarta").prop({readonly:true});     
});

$("#btnNuevo").click(function (){
    $('#frmCreaCarta').trigger("reset");
    $('#mhdnAccionCarta').val('N');

    iniModalCarta('0',0); 

    $('#txtRegPropu').show();
    $('#divRegPropu').hide();

    $('#mhdnIdCarta').val();

    fechaActualcarta();	
    nro_carta();
});

iniModalCarta = function(ccliente, idpropu){
    $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"pt/ccartas/getbuscarCliente",
        dataType: "JSON",
        async: true,
        success:function(result)
        {
            $('#mcboClie').html(result);
            $('#mcboClie').val(ccliente);
        },
        error: function(){
            alert('Error, No se puede autenticar por error');
        }
    });  
    
    var params = { 
        "ccliente":ccliente 
    };
    $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"pt/ccartas/getpropucarta",
        dataType: "JSON",
        async: true,
        data: params,
        success:function(result)
        {
          $("#cboRegPropu").html(result); 
          $('#cboRegPropu').val(idpropu); 	
        },
        error: function(){
          alert('Error, no se puede cargar la lista desplegable de propuesta');
        }
    });   
    
}

$("#chkPropu").on("change", function () {
    if($("#chkPropu").is(":checked") == true){ 
        $('#txtRegPropu').hide();
        $('#divRegPropu').show();
    }else if($("#chkPropu").is(":checked") == false){ 
        $('#txtRegPropu').show();
        $('#divRegPropu').hide();
    }; 
}); 

fechaActualcarta= function(){
    var fecha = new Date();	
    var fechatring = ("0" + fecha.getDate()).slice(-2) + "/" + ("0"+(fecha.getMonth()+1)).slice(-2) + "/" +fecha.getFullYear() ;
    
    $('#mtxtFregcarta').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY') );	
};

$("#chkNroAntiguo").on("change", function () {
    var v_fecha = $('#mtxtFcarta').val().substr(6,4);
    var v_fechaactual = new Date().getFullYear();

    if (v_fechaactual == v_fecha){
        alert("Debe de Seleccionar una fecha anterior");
        $(document.getElementById('chkNroAntiguo')).prop('checked', false);
    }else{
        if($("#chkNroAntiguo").is(":checked") == true){ 
            $("#mtxtNrocarta").prop({readonly:false}); 
        }else if($("#chkNroAntiguo").is(":checked") == false){ 
            $("#mtxtNrocarta").prop({readonly:true}); 
        }; 
        if ($('#mhdnAccionPropu').val()=='N'){
            nro_carta();
        }
    }
}); 

function nro_carta(){
    var vyearCarta = $('#mtxtFcarta').val().substr(6);
    var params = { 
        "yearCarta" : vyearCarta
    }; 

    $.ajax({
      type: 'ajax',
      method: 'post',
      url: baseurl+"pt/ccartas/getnrocarta",
      dataType: "JSON",
      async: true,
      data: params,
      success: function (result){
        var c = (result);
        $.each(c,function(i,item){
          $('#mtxtNrocarta').val(item.NRO_CARTA);
        })
      },
      error: function(){
        alert('Error, no se genero Nro. Carta');
      }
    })
};

escogerArchivo = function(){    
    var archivoInput = document.getElementById('mtxtArchivocarta');
    var archivoRuta = archivoInput.value;
    var extPermitidas = /(.pdf|.docx|.xlsx|.doc|.xls)$/i;
    
    var filename = $('#mtxtArchivocarta').val().replace(/.*(\/|\\)/, '');
    $('#mtxtNomarchcarta').val(filename);

    if(!extPermitidas.exec(archivoRuta)){
        alert('Asegurese de haber seleccionado un PDF, DOCX, XSLX');
        archivoInput.value = '';  
        $('#mtxtNomarchcarta').val('');
        return false;
    }      
    $('#sArchivo').val('S');
};

subirArchivo=function(){
    var parametrotxt = new FormData($("#frmCreaCarta")[0]);
    var request = $.ajax({
        data: parametrotxt,
        method: 'post',
        url: baseurl+"pt/ccartas/subirArchivo/",
        dataType: "JSON",
        async: true,
        contentType: false,
        processData: false,
        error: function(){
            alert('Error, no se cargó el archivo');
        }
    });
    request.done(function( respuesta ) {
        $('#btnBuscar').click(); 
        Vtitle = 'Guardo Correctamente';
        Vtype = 'success';
        sweetalert(Vtitle,Vtype);
        $('#mbtnCCreaCarta').click();
    });
};

selCarta= function(IDCARTA,CCLIENTE,NROCARTA,FECHCARTA,DETACARTA,IDRESPONSABLE,IDPROPU,ARCHIVO,RUTA,NOMBARCH){
    $('#frmCreaCarta').trigger("reset");

    $('#mhdnAccionCarta').val('A');
        
    $('#mhdnIdCarta').val(IDCARTA);
    $('#mtxtNrocarta').val(NROCARTA);
    $('#mtxtFcarta').val(FECHCARTA);
    $('#mtxtDetaCarta').val(DETACARTA);
    $('#mcboRespon').val(IDRESPONSABLE);

    if(IDPROPU == 0){
        $('#txtRegPropu').show();
        $('#divRegPropu').hide();
    }else{
        $('#txtRegPropu').hide();
        $('#divRegPropu').show();
    }
    
    //$('#mtxtClientePote').val(CLIPOTEN).trigger("change");
    $('#mtxtarchivo').val(ARCHIVO);
    $('#mtxtRutacarta').val(RUTA);
    $('#mtxtNomarchcarta').val(NOMBARCH);

    iniModalCarta(CCLIENTE,IDPROPU);
    
};

$("#mcboClie").change(function(){
    var v_mcboClie = $('#mcboClie').val();
    
    var params = { 
        "ccliente":v_mcboClie 
    };
    $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"pt/ccartas/getpropucarta",
        dataType: "JSON",
        async: true,
        data: params,
        success:function(result)
        {
          $("#cboRegPropu").html(result);  	
        },
        error: function(){
          alert('Error, no se puede cargar la lista desplegable de propuesta');
        }
    });  
    
});

$('#modalDetaCarta').on('shown.bs.modal', function (e) {
    //
});

listarDetCarta=function(idcarta,fechacarta,nrocarta,cantdet){
    $('#mtxtiddetacarta').val(idcarta);
    $('#mtxtfechadetacarta').val(fechacarta);
    $('#mtxtnrodetacarta').val(nrocarta);
    $('#mtxtcantdetacarta').val(cantdet);

    otblDetacarta = $('#tblDetacarta').DataTable({  
        'responsive'    : true,
        'bJQueryUI'     : true,
        'scrollY'     	: '250px',
        'scrollX'     	: true, 
        'paging'      	: false,
        'processing'  	: true,      
        'bDestroy'    	: true,
        "AutoWidth"     : false,
        'info'        	: true,
        'filter'      	: false, 
        "ordering"		: false,  
        'stateSave'     : true,
        'ajax'	: {
            "url"   : baseurl+"pt/ccartas/getbuscardetacarta/",
            "type"  : "POST", 
            "data": function ( d ) { 
                d.idptcarta  = idcarta; 
            },     
            dataSrc : ''        
        },
        'columns'	: [
            {orderable : false, data : 'POS', targets : 0 },
            {data: 'NOMARCH', targets: 1 },
            {"orderable": false, 
              render:function(data, type, row){
                return  '<div>'+  
                            '<a href="'+baseurl+row.RUTAFILE+row.ARCHIVO+'" target="_blank" class="btn btn-default btn-xs pull-left"><i class="fas fa-cloud-download-alt fa-2x" data-original-title="Descargar" data-toggle="tooltip"></i></a>'+
                        '</div>'   
              }
            },
            {"orderable": false, 
              render:function(data, type, row){
                return  '<div>'+  
                          '<a id="aDelDetCarta" href="'+row.ITEM+'" title="Eliminar" style="cursor:pointer; color:#FF0000;"><span class="fas fa-trash-alt fa-2x" aria-hidden="true"> </span></a>'+
                        '</div>'   
              }
            }
        ],
    });   
};
 
subirDetCarta=function(){
    var archivoInput = document.getElementById('mtxtDetArchivocarta');
    var archivoRuta = archivoInput.files;
    var i;
    var result = [];

    for(i = 0; i < archivoRuta.length; i++){
        result[i] = archivoRuta[i].name;  
    }

    var extPermitidas = /(.pdf|.docx|.xlsx|.doc|.xls)$/i;

    if(!extPermitidas.exec(result)){
        alert('Asegurese de haber seleccionado un PDF, DOCX, XSLX, MDB');
        archivoInput.value = '';
        return false;
    }
    else
    {
        var parametrotxt = new FormData($("#frmDetaCarta")[0]);
        var request = $.ajax({
            data: parametrotxt,
            method: 'post',
            url: baseurl+"pt/ccartas/archivo_detcarta/",
            dataType: "JSON",
            async: true,
            contentType: false,
            processData: false,
            error: function(){
                alert('Error, no se cargó el archivo');
            }
        });
        request.done(function( respuesta ) {      
            $('#frmDetaCarta').trigger("reset");
            otblDetacarta.ajax.reload(null,false);
        });
    }
};
   
$("body").on("click","#aDelDetCarta",function(event){
    event.preventDefault();
    item = $(this).attr("href");

    Swal.fire({
        title: 'Confirmar Eliminación',
        text: "¿Está seguro de eliminar el Archivo Adjunto?",
        icon: 'error',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, bórralo!'
    }).then((result) => {
        if (result.value) {
            $.post(baseurl+"pt/ccartas/deldetcarta/", 
            {
                item   : item,
            },      
            function(data){     
                otblDetacarta.ajax.reload(null,false); 
                Vtitle = 'Se Elimino Correctamente';
                Vtype = 'success';
                sweetalert(Vtitle,Vtype);      
            });
        }
    }) 
});













   
$("body").on("click","#aDelPropu",function(event){
    event.preventDefault();
    idptpropuesta = $(this).attr("href");

    Swal.fire({
        title: 'Confirmar Eliminación',
        text: "¿Está seguro de eliminar la Propuesta?",
        icon: 'error',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, bórralo!'
    }).then((result) => {
        if (result.value) {
            $.post(baseurl+"pt/cpropuesta/delpropuesta/", 
            {
                idptpropuesta   : idptpropuesta,
            },      
            function(data){     
                otblListPropuesta.ajax.reload(null,false); 
                Vtitle = 'Se Elimino Correctamente';
                Vtype = 'success';
                sweetalert(Vtitle,Vtype);      
            });
        }
    }) 
});

cambiaEstPropu = function(idpropu, estado){
    var params = { "idpropu":idpropu,"est":estado };
    $.ajax({
      type: 'ajax',
      method: 'post',
      url: baseurl+"pt/cpropuesta/updestadopropuesta",
      async: true,
      data: params,
      success:function(result)
      {
        otblListPropuesta.ajax.reload(null,false); 
        Vtitle = 'Se Actualizo correctamente el Estado';
        Vtype = 'success';
        sweetalert(Vtitle,Vtype);  
      },
      error: function(){          
        otblListPropuesta.ajax.reload(null,false); 
        Vtitle = 'No se puede actualizar el estado';
        Vtype = 'error';
        sweetalert(Vtitle,Vtype); 
      }
    });
  
};


SelExtenderpropu= function(IDPROPU,CODCLIENTE,NROPROPU,FECHPROPU,IDSERV,DETAPROPU,COSTOTOTAL,ESTPROPU,CONTACTO,OBSPROPU,SERVNEW,CLIPOTEN,IDUSUARIO,TIPOCOSTO,ARCHIVO,CODESTABLE,RUTA){
    $('#frmCreaPropu').trigger("reset");
    
    $('#mhdnAccionPropu').val('N');
        
    $('#mhdnIdPropu').val('');
    $('#mtxtidusupropu').val(IDUSUARIO);
    $('#mhdnEstadoPropu').val('2');
    $('#mtxtFpropu').val('');
    $('#mcboEstable').val(CODESTABLE);
    $('#mcboClie').val(CODCLIENTE);
    $('#mcboServPropu').val(IDSERV);
    $('#mtxtNropropuesta').val('');
    $('#mtxtservnew').val(SERVNEW).trigger("change");
    $('#mtxtClientePote').val(CLIPOTEN).trigger("change");
    $('#mtxtCostoPropu').val(COSTOTOTAL);
    $('#txtTipomoneda').val(TIPOCOSTO);
    $('#mtxtContacPropu').val($('#mtxtinfousuario').val());
    $('#mtxtNomarchpropu').val('');
    $('#mtxtRutapropu').val('');
    $('#mtxtDetaPropu').val(DETAPROPU);
    $('#mtxtObspropu').val(OBSPROPU);

    iniModalPropu(CODCLIENTE,IDSERV,CODESTABLE);

    if(TIPOCOSTO == '$'){       
        tcDolar(); 
    }else{
        tcSol();
    }

    $('#lbnropro').hide();
    $('#lbchkpro').show();
    $('#lbcontact').hide();
    $('#lbchkcontact').show();

    fechaActualpropu();	
    ext_nropropuesta(NROPROPU);
    
};

function ext_nropropuesta(NROPROPU){
    
    var params = { 
        "nropropuesta" : NROPROPU
    }; 
    $.ajax({
      type: 'ajax',
      method: 'post',
      url: baseurl+"pt/cpropuesta/getextnropropuesta",
      dataType: "JSON",
      async: true,
      data: params,
      success: function (result){
        var c = (result);
        $.each(c,function(i,item){
          $('#mtxtNropropuesta').val(item.NRO_PROPU);
        })
      },
      error: function(){
        alert('Error, no se exxtendio Nro. Propuesta');
      }
    })
}; 

SelDuplicarpropu= function(IDPROPU,CODCLIENTE,NROPROPU,FECHPROPU,IDSERV,DETAPROPU,COSTOTOTAL,ESTPROPU,CONTACTO,OBSPROPU,SERVNEW,CLIPOTEN,IDUSUARIO,TIPOCOSTO,ARCHIVO,CODESTABLE,RUTA){
    $('#frmCreaPropu').trigger("reset");
    
    $('#mhdnAccionPropu').val('N');
        
    $('#mhdnIdPropu').val('');
    $('#mtxtidusupropu').val(IDUSUARIO);
    $('#mhdnEstadoPropu').val('2');
    $('#mtxtFpropu').val('');
    $('#mcboEstable').val(CODESTABLE);
    $('#mcboClie').val(CODCLIENTE);
    $('#mcboServPropu').val(IDSERV);
    $('#mtxtNropropuesta').val('');
    $('#mtxtservnew').val(SERVNEW).trigger("change");
    $('#mtxtClientePote').val(CLIPOTEN).trigger("change");
    $('#mtxtCostoPropu').val(COSTOTOTAL);
    $('#txtTipomoneda').val(TIPOCOSTO);
    $('#mtxtContacPropu').val($('#mtxtinfousuario').val());
    $('#mtxtNomarchpropu').val('');
    $('#mtxtRutapropu').val('');
    $('#mtxtDetaPropu').val(DETAPROPU);
    $('#mtxtObspropu').val(OBSPROPU);

    iniModalPropu(CODCLIENTE,IDSERV,CODESTABLE);

    if(TIPOCOSTO == '$'){       
        tcDolar(); 
    }else{
        tcSol();
    }

    $('#lbnropro').hide();
    $('#lbchkpro').show();
    $('#lbcontact').hide();
    $('#lbchkcontact').show();

    fechaActualpropu();	
    nro_propuesta();
    
};



listcboservicio = function(vidservicio){ 
    $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"pt/cpropuesta/getServicio",
        dataType: "JSON",
        async: true,
        success:function(result)
        {
            $('#mcboServPropu').html(result);
            $('#mcboServPropu').val(vidservicio).trigger("change");
        },
        error: function(){
            alert('Error, No se puede autenticar por error');
        }
    });
};
$("#mbtnnewservicio").click(function (){
    $('#frmMantservicio').trigger("reset");

    $("#modalMantservicio").modal('show');

    $('#mhdnAccionservicio').val('N');
});
$('#modalMantservicio').on('show.bs.modal', function (e) {
    $('#frmMantservicio').validate({        
        rules: {
            txtdesservicio: {
              required: true,
            },
        },
        messages: {
            txtdesservicio: {
              required: "Por Favor ingrese Nombre del servicio"
            },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        },        
        submitHandler: function (form) {
            const botonEvaluar = $('#mbtnGMantservicio');
            var request = $.ajax({
                url:$('#frmMantservicio').attr("action"),
                type:$('#frmMantservicio').attr("method"),
                data:$('#frmMantservicio').serialize(),
                error: function(){
                    Vtitle = 'Error en Guardar!!!';
                    Vtype = 'error';
                    sweetalert(Vtitle,Vtype); 
                    objPrincipal.liberarBoton(botonEvaluar);
                },
                beforeSend: function() {
                    objPrincipal.botonCargando(botonEvaluar);
                }
            });
            request.done(function( respuesta ) {
                var posts = JSON.parse(respuesta);
                
                $.each(posts, function() {
                    Vtitle = 'Se Grabo Correctamente!!!';
                    Vtype = 'success';
                    sweetalert(Vtitle,Vtype); 
                      
                    var vidservicio = this.id
                    listcboservicio(vidservicio);

                    objPrincipal.liberarBoton(botonEvaluar);    
                    $('#mbtnCMantservicio').click();    
                });
            });
            return false;
        }
    });
});


/*
$('.addcliente')
.on('select2:open', () => {
    $(".select2-results:not(:has(a))").append('<a href="#" style="padding: 6px;height: 20px;display: inline-table;">Agregar Nuevo</a>');
})
*/

