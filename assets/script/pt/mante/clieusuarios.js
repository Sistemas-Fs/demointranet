
var otblListclieusuario;

$(document).ready(function() {

});


$("#btnBuscar").click(function (){    
    parametros = paramListarBusqueda();
    getListarBusqueda(parametros);    
});

paramListarBusqueda = function (){    
       
    var param = {
        "descripcion"     : '%'
    }; 

    return param;    
};

getListarBusqueda = function(param){
    

    otblListclieusuario = $('#tblListclieusuario').DataTable({
        "processing"  	: true,
        "bDestroy"    	: true,
        "stateSave"     : true,
        "bJQueryUI"     : true,
        "scrollY"     	: "540px",
        "scrollX"     	: true, 
        'AutoWidth'     : true,
        "paging"      	: false,
        "info"        	: true,
        "filter"      	: true, 
        "ordering"		: false,
        "responsive"    : false,
        "select"        : true,
        'ajax'	: {
            "url"   : baseurl+"pt/cclieusuarios/getbuscarclieusuario/",
            "type"  : "POST", 
            "data"  : param,      
            dataSrc : ''        
        },
        'columns'	: [
            {data: null, "class": "col-xxs"},
            {data: 'NOMBRE', "class": "col-lm"},
            {data: 'EMAIL', "class": "col-lm"},
            {data: 'CLIENTE', "class": "col-lm"},
            {"orderable": false, "class": "col-s", 
                render:function(data, type, row){
                    if(row.tipo_acceso != "B") {                    
                        return '<div>'+
                        '<a id="aBloquear" href="'+row.id_usuario+'" title="Bloquear" style="cursor:pointer; color:#FF0000;"><span class="fas fa-user-lock" aria-hidden="true"> </span></a>'+      
                        '</div>'
                    }else{                   
                        return '<div>'+
                        '<a id="aDesbloquear" href="'+row.id_usuario+'" title="Desboquear" style="cursor:pointer; color:#29B93F;"><span class="fas fa-user-lock" aria-hidden="true"> </span></a>'+      
                        '</div>'
                    }
                }
            },
            {"orderable": false, "class": "col-s", 
                render:function(data, type, row){
                    return '<div>'+
                        '<a id="aDelusu" href="'+row.id_usuario+'" title="Eliminar" style="cursor:pointer; color:#FF0000;"><span class="fas fa-trash-alt" aria-hidden="true"> </span></a>'+      
                        '</div>'
                }
            },
        ],
    });  
    // Enumeracion 
    otblListclieusuario.on( 'order.dt search.dt', function () { 
        otblListclieusuario.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i+1;
          } );
    }).draw(); 
};

$('#tblListclieusuario tbody').on('dblclick', 'td', function () {
    var tr = $(this).parents('tr');
    var row = otblListclieusuario.row(tr);
    var rowData = row.data();
    
    $("#modalUsuarios").modal('show');

    $('#frmMantclieusu').trigger("reset");

    $('#mhdnAccion').val('A'); 
    $('#mhdnidusuario').val(rowData.id_usuario);
    $('#mhdnidadministrado').val(rowData.id_administrado);
    $('#mhdntipodoc').val(rowData.id_tipodoc);
    $('#mtxtnrodoc').val(rowData.nrodoc);
    $('#mtxtapepat').val(rowData.ape_paterno);
    $('#mtxtapemat').val(rowData.ape_materno);
    $('#mtxtnombres').val(rowData.nombres);
    $('#mtxtemail').val(rowData.EMAIL);
    
    listcbocliente(rowData.ccliente);
    

});

$("#btnNuevo").click(function (){
    $('#frmMantclieusu').trigger("reset");

    $("#modalUsuarios").modal('show');
 

    $('#mhdnAccion').val('N');

    $('#mhdntipodoc').val(1);
    
    listcbocliente('');
    
});

$("body").on("click","#aDelusu",function(event){
    event.preventDefault();
    id_usuario = $(this).attr("href");
  
    Swal.fire({
        title: 'Confirmar Eliminación',
        text: "¿Está seguro de eliminar el Usuario?",
        icon: 'error',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, bórralo!'
    }).then((result) => {
        if (result.value) {
            $.post(baseurl+"pt/cclieusuarios/delUsuario/", 
            {
                id_usuario   : id_usuario,
            },      
            function(data){     
                otblListclieusuario.ajax.reload(null,false); 
                Vtitle = 'Se Elimino Correctamente';
                Vtype = 'success';
                sweetalert(Vtitle,Vtype);      
            });
        }
    }) 
  });

$('#modalUsuarios').on('show.bs.modal', function (e) {   

    $('#frmMantclieusu').validate({
        rules: {
            mcboClie: {
              required: true,
            },
            mtxtemail: {
              required: true,
            },
            mtxtapepat: {
              required: true,
            },
        },
        messages: {
            mcboClie: {
              required: "Por Favor escoja un Cliente"
            },
            mtxtemail: {
              required: "Por Favor ingrese Email de Acceso"
            },
            mtxtapepat: {
              required: "Por Favor ingrese Apellido del usuario"
            },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        },        
        submitHandler: function (form) {
            const botonEvaluar = $('#mbtnCMante');
            var request = $.ajax({
                url:$('#frmMantclieusu').attr("action"),
                type:$('#frmMantclieusu').attr("method"),
                data:$('#frmMantclieusu').serialize(),
                error: function(){
                    Vtitle = 'Error en Guardar!!!';
                    Vtype = 'error';
                    sweetalert(Vtitle,Vtype); 
                    objPrincipal.liberarBoton(botonEvaluar);
                },
                beforeSend: function() {
                    objPrincipal.botonCargando(botonEvaluar);
                }
            });
            request.done(function( respuesta ) {
                var posts = JSON.parse(respuesta);
                
                $.each(posts, function() {
                    Vtitle = 'Se Grabo Correctamente!!!';
                    Vtype = 'success';
                    sweetalert(Vtitle,Vtype); 
                      
                    parametros = paramListarBusqueda();
                    getListarBusqueda(parametros); 

                    objPrincipal.liberarBoton(botonEvaluar);    
                    $('#mbtnCMante').click();    
                });
            });
            return false;
        }
    });
});

DNI=function(){
    $('#btntipodoc').html("DNI");
    $('#mhdntipodoc').val(1);
};

EXT=function(){
    $('#btntipodoc').html("CExt");
    $('#mhdntipodoc').val(4);
};
    
listcbocliente = function(ccliente){  
    $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"pt/cpropuesta/getclientepropu",
        dataType: "JSON",
        async: true,
        success:function(result)
        {
            $('#mcboClie').html(result);
            $('#mcboClie').val(ccliente).trigger("change"); 
        },
        error: function(){
            alert('Error, No se puede autenticar por error');
        }
    }); 

};

$("body").on("click","#aBloquear",function(event){
    event.preventDefault();
    id = $(this).attr("href");

    Swal.fire({
        title: 'Confirmar Bloquear',
        text: "¿Está seguro de bloquear usuario?",
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, bloquearlo!'
    }).then((result) => {
        if (result.value) {
            $.post(baseurl+"pt/cclieusuarios/setbloquear/", 
            {
                id_usuario   : id,
            },      
            function(data){     
                otblListclieusuario.ajax.reload(null,false); 
                Vtitle = 'Se Bloqueo Correctamente';
                Vtype = 'success';
                sweetalert(Vtitle,Vtype);      
            });
        }
    }) 
});

$("body").on("click","#aDesbloquear",function(event){
    event.preventDefault();
    id = $(this).attr("href");

    Swal.fire({
        title: 'Confirmar Desbloquear',
        text: "¿Está seguro de desbloquear usuario?",
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, desbloquearlo!'
    }).then((result) => {
        if (result.value) {
            $.post(baseurl+"pt/cclieusuarios/setdesbloquear/", 
            {
                id_usuario   : id,
            },      
            function(data){     
                otblListclieusuario.ajax.reload(null,false); 
                Vtitle = 'Se Desbloqueo Correctamente';
                Vtype = 'success';
                sweetalert(Vtitle,Vtype);      
            });
        }
    }) 
});








$("body").on("click","#aCesecontrato",function(event){
    event.preventDefault();
    vidcontrato = $(this).attr("href");
    
    $.post(baseurl+"adm/rrhh/ccontratos/setcesarcontrato", 
    {
        idcontrato   : vidcontrato,
    },      
    function(data){           
        Vtitle = 'Se Ceso Correctamente!!!';
        Vtype = 'success';
        sweetalert(Vtitle,Vtype); 

        otblListContratos.ajax.reload(null,false); 	
    });
}); 

$('#mcbopension').change(function( event, videnti ){ 
    var v_cpension = $( "#mcbopension option:selected").attr("value");
    var v_cidentidad = videnti || 0;
    listcboEntidadpension(v_cpension,v_cidentidad);
});

listcboEntidadpension = function(v_cpension,videntidadpension){    
    var params = { 
        "idpension" : v_cpension
    }; 
    $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"adm/rrhh/ccontratos/getcbopensionentidad",
        dataType: "JSON",
        async: true,
        data: params,
        success:function(result)
        {
            $('#mcboentidadpension').html(result);
            $('#mcboentidadpension').val(videntidadpension).trigger("change"); 
        },
        error: function(){
            alert('Error, No se puede autenticar por error = mcboentidadpension');
        }
    });
};

$("#mbtnnewentidadpension").click(function (){
    $('#frmMantentidadpension').trigger("reset");

    $("#modalMantentidadpension").modal('show');

    $('#mhdnAccionentidadpension').val('N');
});

$('#modalMantentidadpension').on('show.bs.modal', function (e) {
    $('#frmMantentidadpension').validate({        
        rules: {
            txtdescripcion: {
              required: true,
            },
        },
        messages: {
            txtdescripcion: {
              required: "Por Favor ingrese Nombre de la entidad"
            },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        },        
        submitHandler: function (form) {
            const botonEvaluar = $('#mbtnGMantentidadpension');
            var request = $.ajax({
                url:$('#frmMantentidadpension').attr("action"),
                type:$('#frmMantentidadpension').attr("method"),
                data:$('#frmMantentidadpension').serialize(),
                error: function(){
                    Vtitle = 'Error en Guardar!!!';
                    Vtype = 'error';
                    sweetalert(Vtitle,Vtype); 
                    objPrincipal.liberarBoton(botonEvaluar);
                },
                beforeSend: function() {
                    objPrincipal.botonCargando(botonEvaluar);
                }
            });
            request.done(function( respuesta ) {
                var posts = JSON.parse(respuesta);
                
                $.each(posts, function() {
                    Vtitle = 'Se Grabo Correctamente!!!';
                    Vtype = 'success';
                    sweetalert(Vtitle,Vtype); 
                      
                    var vidpension = $('#mcbopension').val();
                    listcboEntidadpension(vidpension,0);

                    objPrincipal.liberarBoton(botonEvaluar);    
                    $('#mbtnCMantentidadpension').click();    
                });
            });
            return false;
        }
    });
});

listcboBanco = function(vidbanco){    
    $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"adm/rrhh/ccontratos/getcbobanco",
        dataType: "JSON",
        async: true,
        success:function(result)
        {
            $('#mcbobanco').html(result);
            $('#mcbobanco').val(vidbanco).trigger("change"); 
        },
        error: function(){
            alert('Error, No se puede autenticar por error = mcbobanco');
        }
    });
};

$("#mbtnnewbanco").click(function (){
    $('#frmMantbanco').trigger("reset");

    $("#modalMantbanco").modal('show');
    
    $('#mhdnAccionbanco').val('N');
});

$('#modalMantbanco').on('show.bs.modal', function (e) {
    $('#frmMantbanco').validate({        
        rules: {
            txtdesbanco: {
              required: true,
            },
        },
        messages: {
            txtdesbanco: {
              required: "Por Favor ingrese Nombre del Banco"
            },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        },        
        submitHandler: function (form) {
            const botonEvaluar = $('#mbtnGMantbanco');
            var request = $.ajax({
                url:$('#frmMantbanco').attr("action"),
                type:$('#frmMantbanco').attr("method"),
                data:$('#frmMantbanco').serialize(),
                error: function(){
                    Vtitle = 'Error en Guardar!!!';
                    Vtype = 'error';
                    sweetalert(Vtitle,Vtype); 
                    objPrincipal.liberarBoton(botonEvaluar);
                },
                beforeSend: function() {
                    objPrincipal.botonCargando(botonEvaluar);
                }
            });
            request.done(function( respuesta ) {
                var posts = JSON.parse(respuesta);
                
                $.each(posts, function() {
                    Vtitle = 'Se Grabo Correctamente!!!';
                    Vtype = 'success';
                    sweetalert(Vtitle,Vtype); 
                      
                    listcboBanco();

                    objPrincipal.liberarBoton(botonEvaluar);    
                    $('#mbtnCMantbanco').click();    
                });
            });
            return false;
        }
    });
});

listcboProfesion = function(idprofesion){    
    
    $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"adm/rrhh/ccontratos/getcboprofesion",
        dataType: "JSON",
        async: true,
        success:function(result)
        {
            $('#mcboprofesion').html(result);
            $('#mcboprofesion').val(idprofesion).trigger("change"); 
        },
        error: function(){
            alert('Error, No se puede autenticar por error = mcboprofesion');
        }
    });
};

$("#mbtnnewprofesion").click(function (){
    $('#frmMantprofesion').trigger("reset");

    $("#modalMantprofesion").modal('show');
    
    $('#mhdnAccionprofesion').val('N');
});

$('#modalMantprofesion').on('show.bs.modal', function (e) {
    $('#frmMantprofesion').validate({        
        rules: {
            txtdesprofesion: {
              required: true,
            },
        },
        messages: {
            txtdesprofesion: {
              required: "Por Favor ingrese Nombre de la Profesion"
            },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        },        
        submitHandler: function (form) {
            const botonEvaluar = $('#mbtnGMantprofesion');
            var request = $.ajax({
                url:$('#frmMantprofesion').attr("action"),
                type:$('#frmMantprofesion').attr("method"),
                data:$('#frmMantprofesion').serialize(),
                error: function(){
                    Vtitle = 'Error en Guardar!!!';
                    Vtype = 'error';
                    sweetalert(Vtitle,Vtype); 
                    objPrincipal.liberarBoton(botonEvaluar);
                },
                beforeSend: function() {
                    objPrincipal.botonCargando(botonEvaluar);
                }
            });
            request.done(function( respuesta ) {
                var posts = JSON.parse(respuesta);
                
                $.each(posts, function() {
                    Vtitle = 'Se Grabo Correctamente!!!';
                    Vtype = 'success';
                    sweetalert(Vtitle,Vtype); 
                      
                    listcboProfesion();

                    objPrincipal.liberarBoton(botonEvaluar);    
                    $('#mbtnCMantprofesion').click();    
                });
            });
            return false;
        }
    });
});

listcboArea = function(v_ccia){    
    var params = { 
        "ccia" : v_ccia
    }; 
    $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"adm/rrhh/ccontratos/getcboarea",
        dataType: "JSON",
        async: true,
        data: params,
        success:function(result)
        {
            $('#mcboarea').html(result);
        },
        error: function(){
            a
            lert('Error, No se puede autenticar por error = mcboarea');
        }
    });
};

$('#mcboarea').change(function(){    
    var vccia = $('#hrdcia').val();
    var vcarea = $('#mcboarea').val();
    listcboSubarea(vccia,vcarea);
});

listcboSubarea = function(v_ccia,v_carea){    
    var params = { 
        "ccia" : v_ccia,
        "carea" : v_carea
    }; 
    $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"adm/rrhh/ccontratos/getcbosubarea",
        dataType: "JSON",
        async: true,
        data: params,
        success:function(result)
        {
            $('#mcbosubarea').html(result);
        },
        error: function(){
            a
            lert('Error, No se puede autenticar por error = mcbosubarea');
        }
    });
};

listcboCargo = function(tipocargo){
    vtipocargo = tipocargo;    
    $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"adm/rrhh/ccontratos/getcbocargo",
        dataType: "JSON",
        async: true,
        success:function(result)
        {
            if(vtipocargo == 'E'){
                $('#mcbocargo').html(result);
            }else{
                $('#mcbocargo_cont').html(result);
            }            
        },
        error: function(){
            alert('Error, No se puede autenticar por error = mcbocargo');
        }
    });
};

$("#mbtnnewcargo").click(function (){
    $('#frmMantcargo').trigger("reset");

    $("#modalMantcargo").modal('show');
    
    $('#mhdnAccioncargo').val('N');
    $('#mhdncargotipo').val('E');
});

$('#modalMantcargo').on('show.bs.modal', function (e) {
    var vtipocargo = $('#mhdncargotipo').val();
    $('#frmMantcargo').validate({        
        rules: {
            txtdescargo: {
              required: true,
            },
        },
        messages: {
            txtdescargo: {
              required: "Por Favor ingrese Nombre del Cargo"
            },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        },        
        submitHandler: function (form) {
            const botonEvaluar = $('#mbtnGMantcargo');
            var request = $.ajax({
                url:$('#frmMantcargo').attr("action"),
                type:$('#frmMantcargo').attr("method"),
                data:$('#frmMantcargo').serialize(),
                error: function(){
                    Vtitle = 'Error en Guardar!!!';
                    Vtype = 'error';
                    sweetalert(Vtitle,Vtype); 
                    objPrincipal.liberarBoton(botonEvaluar);
                },
                beforeSend: function() {
                    objPrincipal.botonCargando(botonEvaluar);
                }
            });
            request.done(function( respuesta ) {
                var posts = JSON.parse(respuesta);
                
                $.each(posts, function() {
                    Vtitle = 'Se Grabo Correctamente!!!';
                    Vtype = 'success';
                    sweetalert(Vtitle,Vtype); 
                     
                    listcboCargo(vtipocargo);

                    objPrincipal.liberarBoton(botonEvaluar);    
                    $('#mbtnCMantcargo').click();    
                });
            });
            return false;
        }
    });
});

$('#txtFInicio').on('change.datetimepicker',function(e){	
    
    $('#txtFTermino').datetimepicker({
        format: 'DD/MM/YYYY',
        daysOfWeekDisabled: [0],
        locale:'es'
    });	
    
    var faño = moment(e.date).add(1, 'years');

    $('#txtFTermino').datetimepicker('date',moment(faño).format('DD/MM/YYYY') );
    

});


$('#modalMantcontratos').on('show.bs.modal', function (e) {
    /*
    $('#txtFInicio_cont').datetimepicker({
        format: 'DD/MM/YYYY',
        daysOfWeekDisabled: [0],
        locale:'es',
        autoclose: true
    });	
    var fecha = new Date();		
    var fechatring = ("0" + fecha.getDate()).slice(-2) + "/" + ("0"+(fecha.getMonth()+1)).slice(-2) + "/" +fecha.getFullYear() ;
    $('#txtFInicio_cont').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY') );*/
   

    $('#frmMantcontratos').validate({
        rules: {
        },
        messages: {
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        },        
        submitHandler: function (form) {
            const botonEvaluar = $('#mbtnGMantcontrato');
            var request = $.ajax({
                url:$('#frmMantcontratos').attr("action"),
                type:$('#frmMantcontratos').attr("method"),
                data:$('#frmMantcontratos').serialize(),
                error: function(){
                    Vtitle = 'Error en Guardar!!!';
                    Vtype = 'error';
                    sweetalert(Vtitle,Vtype); 
                    objPrincipal.liberarBoton(botonEvaluar);
                },
                beforeSend: function() {
                    objPrincipal.botonCargando(botonEvaluar);
                }
            });
            request.done(function( respuesta ) {
                var posts = JSON.parse(respuesta);
                
                $.each(posts, function() {
                    Vtitle = 'Se Grabo Correctamente!!!';
                    Vtype = 'success';
                    sweetalert(Vtitle,Vtype); 
                    
                    otblContratos.ajax.reload(null,false);  
                    //getlistContratos(); 

                    objPrincipal.liberarBoton(botonEvaluar);       
                });
            });
            return false;
        }
    });
});

selContratos = function(vidempleado,vempleado,vidcontrato,vccia,vfinicio,vffin,vcarea,vcsubarea,vmodalidad_contrato,vsueldo,vidcargo){
    
    $('#mhdnidempleado_cont').val(vidempleado);
    $('#mhdnAccioncontrato').val('A');
    document.querySelector('#nomempleado').innerText = vempleado;
    $('#txtFInicio_cont').datetimepicker({
        format: 'DD/MM/YYYY',
        daysOfWeekDisabled: [0],
        locale:'es',
        autoclose: true
    });	
    getlistContratos();    
    getContratos(vidcontrato,vccia,vfinicio,vffin,vcarea,vcsubarea,vmodalidad_contrato,vsueldo,vidcargo);
};

$('#txtFInicio_cont').on('change.datetimepicker',function(e){	
    
    $('#txtFTermino_cont').datetimepicker({
        format: 'DD/MM/YYYY',
        daysOfWeekDisabled: [0],
        locale:'es',
        autoclose: true,
        todayBtn: true
    });	
    
    var faño = moment(e.date).add(1, 'years');
    $('#txtFTermino_cont').datetimepicker('date',moment(faño).format('DD/MM/YYYY') );    

});

listcboArea_cont = function(v_ccia, v_carea, v_csubarea){    
    var params = { 
        "ccia" : v_ccia
    }; 
    $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"adm/rrhh/ccontratos/getcboarea",
        dataType: "JSON",
        async: true,
        data: params,
        success:function(result)
        {
            $('#mcboarea_cont').html(result);
            $('#mcboarea_cont').val(v_carea).trigger("change", [v_ccia,v_csubarea] ); 
        },
        error: function(){
            alert('Error, No se puede autenticar por error = mcboarea');
        }
    });
};

$('#mcboarea_cont').change(function( event, ccia, csubarea){ 
    var v_carea = $( "#mcboarea_cont option:selected").attr("value");
    listcboSubarea_cont(ccia,v_carea,csubarea);
});

listcboSubarea_cont = function(v_ccia,v_carea,v_subarea){    
    var params = { 
        "ccia" : v_ccia,
        "carea" : v_carea
    }; 
    $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"adm/rrhh/ccontratos/getcbosubarea",
        dataType: "JSON",
        async: true,
        data: params,
        success:function(result)
        {
            $('#mcbosubarea_cont').html(result);
            $('#mcbosubarea_cont').val(v_subarea).trigger("change"); 
        },
        error: function(){
            alert('Error, No se puede autenticar por error = mcbosubarea');
        }
    });
};

listcboCargo_cont = function(v_idcargo){    
    $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"adm/rrhh/ccontratos/getcbocargo",
        dataType: "JSON",
        async: true,
        success:function(result)
        {
            $('#mcbocargo_cont').html(result);
            $('#mcbocargo_cont').val(v_idcargo).trigger("change"); 
        },
        error: function(){
            alert('Error, No se puede autenticar por error = mcbocargo');
        }
    });
}

getlistContratos = function(){
    otblContratos = $('#tblContratos').DataTable({
        "processing"  	: true,
        "bDestroy"    	: true,
        "stateSave"     : true,
        "bJQueryUI"     : true,
        "scrollY"     	: "320px",
        "scrollX"     	: true, 
        'AutoWidth'     : true,
        "paging"      	: false,
        "info"        	: false,
        "filter"      	: true, 
        "ordering"		: false,
        "responsive"    : false,
        "select"        : true,
        'ajax'	: {
            "url"   : baseurl+"adm/rrhh/ccontratos/getcontratosxempleado/",
            "type"  : "POST", 
            "data": function ( d ) { 
                d.id_empleado     = $('#mhdnidempleado_cont').val(); 
            },      
            dataSrc : ''        
        },
        'columns'	: [
            {data: null, "class": "col-xxs"},
            {data: 'FINICIO', "class": "col-sm"},
            {data: 'FTERMINO', "class": "col-sm"},
            {data: 'ESTADO', "class": "col-xs"},
            {"orderable": false, "class": "col-xxs",
                render:function(data, type, row){                
                        if ( row.ESTADO == 'A' ){
                            return '<div>' +
                            '<a id="aRenovar" href="'+row.id_contrato+'" title="Renovar" style="cursor:pointer; color:cray;"><span class="fas fa-marker" aria-hidden="true"> </span></a>'
                            '</div>' ;
                        }else{
                            return '<div>' +
                            '<a id="aDelete" href="'+row.id_contrato+'" title="Eliminar" style="cursor:pointer; color:red;"><span class="fas fa-trash-alt" aria-hidden="true"> </span></a>'
                            '</div>' ;
                        }
                    
                } 
            }
        ]
    });      
    // Enumeracion 
    otblContratos.on( 'order.dt search.dt', function () { 
        otblContratos.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i+1;
          } );
    }).draw();  
};

getContratos = function(vidcontrato,vccia,vfinicio,vffin,vcarea,vcsubarea,vmodalidad_contrato,vsueldo,vidcargo){
    $('#mhdnidcontrato').val(vidcontrato);
    $('#hrdcia_cont').val(vccia);
    $('#txtFIni_cont').val(vfinicio);
    $('#txtFTerm_cont').val(vffin); 
    listcboArea_cont(vccia,vcarea,vcsubarea);
    $('#mcbomodalidad_cont').val(vmodalidad_contrato).trigger("change");
    $('#mtxtsueldo_cont').val(vsueldo);
    listcboCargo_cont(vidcargo);
};

$("body").on("click","#aDelete",function(event){
    event.preventDefault();
    
    id_contrato = $(this).attr("href");
    
    Swal.fire({
        title: 'Confirmar Eliminación',
        text: "¿Está seguro de eliminar el Contrato??",
        icon: 'error',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, bórralo!'
    }).then((result) => {
        if (result.value) {
            $.post(baseurl+"adm/rrhh/ccontratos/deletecontrato/", 
            {
                id_contrato   : id_contrato,
            },      
            function(data){     
                otblContratos.ajax.reload(null,false);
                Vtitle = 'Se Elimino Correctamente..';
                Vtype = 'success';
                sweetalert(Vtitle,Vtype);      
            });
        }
    }) 
});

$('#tblContratos tbody').on('click', 'td', function () {
    var tr = $(this).parents('tr');
    var row = otblContratos.row(tr);
    var rowData = row.data();

    $('#frmMantcontratos').trigger("reset");

    $('#mhdnAccioncontrato').val('A');
    getContratos(rowData.id_contrato,rowData.ccompania,rowData.FINICIO,rowData.FTERMINO,rowData.carea,rowData.csubarea,rowData.modalidad_contrato,rowData.sueldo,rowData.idcargo);
});

$("#mbtnnewcargo_cont").click(function (){
    $('#frmMantcargo').trigger("reset");

    $("#modalMantcargo").modal('show');
    
    $('#mhdnAccioncargo').val('N');
    $('#mhdncargotipo').val('C');
});
    
$("body").on("click","#aRenovar",function(event){
    event.preventDefault();
    vidcontrato = $(this).attr("href");
    
    $.post(baseurl+"adm/rrhh/ccontratos/setrenovarcontrato", 
    {
        idcontrato   : vidcontrato,
    },      
    function(data){           
        Vtitle = 'Se Renovo Correctamente!!!';
        Vtype = 'success';
        sweetalert(Vtitle,Vtype); 

        otblContratos.ajax.reload(null,false); 	
    });
}); 