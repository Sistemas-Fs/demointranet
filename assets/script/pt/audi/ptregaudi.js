
var otblListAudi;
var varfdesde = '%', varfhasta = '%';

$(document).ready(function() {
    $('#tabptaudi a[href="#tabptaudi-list-tab"]').attr('class', 'disabled');
    $('#tabptaudi a[href="#tabptaudi-det-tab"]').attr('class', 'disabled active');

    $('#tabptaudi a[href="#tabptaudi-list-tab"]').not('#store-tab.disabled').click(function(event){
        $('#tabptaudi a[href="#tabptaudi-list"]').attr('class', 'active');
        $('#tabptaudi a[href="#tabptaudi-det"]').attr('class', '');
        return true;
    });
    $('#tabptaudi a[href="#tabptaudi-det-tab"]').not('#bank-tab.disabled').click(function(event){
        $('#tabptaudi a[href="#tabptaudi-det"]').attr('class' ,'active');
        $('#tabptaudi a[href="#tabptaudi-list"]').attr('class', '');
        return true;
    });
    
    $('#tabptaudi a[href="#tabptaudi-list"]').click(function(event){return false;});
    $('#tabptaudi a[href="#tabptaudi-det"]').click(function(event){return false;});
    
    $('#txtFDesde,#txtFHasta').datetimepicker({
        format: 'DD/MM/YYYY',
        daysOfWeekDisabled: [0],
        locale:'es'
    });

    $('#mtxtFreginforme,#txtFAuditoria').datetimepicker({
        format: 'DD/MM/YYYY',
        daysOfWeekDisabled: [0],
        locale:'es'
    });	

    fechaActual();
    
    $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"pt/cinforme/getclienteinfor",
        dataType: "JSON",
        async: true,
        success:function(result)
        {
            $('#cboclieserv').html(result);
        },
        error: function(){
            alert('Error, No se puede autenticar por error');
        }
    });
        
    var params = { 
        "idptregestudio":15
    };
    $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"pt/cptregaudi/getServicioAudi",
        dataType: "JSON",
        async: true,
        data: params,
        success:function(result){
            $("#cboservicios").html(result);  
        },
        error: function(){
            alert('Error, no se puede cargar la lista desplegable de establecimiento');
        }
    });
      
    $('#frmRegAudi').validate({
        rules: {
			cboRegClie: {
				required: true,
			},
			cboRegPropu: {
				required: true,
			},
			cboregprovclie: {
				required: true,
			},
        },
        messages: {
			cboRegClie: {
				required: "Por Favor escoja un Cliente"
			},
			cboRegPropu: {
				required: "Por Favor escoja una Propuesta"
			},
			cboregprovclie: {
				required: "Por Favor escoja un Proveedor"
			},
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        },        
        submitHandler: function (form) {
            const botonEvaluar = $('#btnGrabar');
            var request = $.ajax({
                url:$('#frmRegAudi').attr("action"),
                type:$('#frmRegAudi').attr("method"),
                data:$('#frmRegAudi').serialize(),
                error: function(){
                    Vtitle = 'Error en Guardar!!!';
                    Vtype = 'error';
                    sweetalert(Vtitle,Vtype); 
                },
                beforeSend: function() {
                    objPrincipal.botonCargando(botonEvaluar);
                }
            });
            request.done(function( respuesta ) {
                var posts = JSON.parse(respuesta);
                
                $.each(posts, function() {    
                    objPrincipal.liberarBoton(botonEvaluar);      
                    $('#mhdnIdInfor').val(this.id_informe);
                    if($('#sArchivo').val() == 'S'){          
                        subirArchivoAudi();
                    }else{   
                        $('#btnBuscar').click();     
                        Vtitle = 'Propuesta Guardada!!!';
                        Vtype = 'success';
                        sweetalert(Vtitle,Vtype);
                        //recuperaListinforme();       
                    } 
                    $('#sArchivo').val('N');  
                });
            });
            return false;
        }
    });
});

fechaActual = function(){
    var fecha = new Date();		
    var fechatring = ("0" + fecha.getDate()).slice(-2) + "/" + ("0"+(fecha.getMonth()+1)).slice(-2) + "/" +fecha.getFullYear() ;

    $('#txtFDesde').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY') );
    $('#txtFHasta').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY') );

};
	
$('#txtFDesde').on('change.datetimepicker',function(e){	
    
    $('#txtFHasta').datetimepicker({
        format: 'DD/MM/YYYY',
        daysOfWeekDisabled: [0],
        locale:'es'
    });	

    var fecha = moment(e.date).format('DD/MM/YYYY');		
    
    $('#txtFHasta').datetimepicker('minDate', fecha);
    $('#txtFHasta').datetimepicker('date', fecha);

});

$("#chkFreg").on("change", function () {
    if($("#chkFreg").is(":checked") == true){ 
        $("#txtFIni").prop("disabled",false);
        $("#txtFFin").prop("disabled",false);
        
        varfdesde = '';
        varfhasta = '';

        var fecha = new Date();		
        var fechatring1 = "01/01/" +fecha.getFullYear() ;
        var fechatring2 = ("0" + fecha.getDate()).slice(-2) + "/" + ("0"+(fecha.getMonth()+1)).slice(-2) + "/" +fecha.getFullYear() ;
        $('#txtFDesde').datetimepicker('date', fechatring1);
        $('#txtFHasta').datetimepicker('date', fechatring2);
    }else if($("#chkFreg").is(":checked") == false){ 
        $("#txtFIni").prop("disabled",true);
        $("#txtFFin").prop("disabled",true);
        
        varfdesde = '%';
        varfhasta = '%';

        fechaActual();
    }; 
});

$("#btnBuscar").click(function (){

    if(varfdesde != '%'){ varfdesde = $('#txtFIni').val(); }
    if(varfhasta != '%'){ varfhasta = $('#txtFFin').val(); }  

    
    otblListAudi = $('#tblListAudi').DataTable({           
        "processing"  	: true,
        "bDestroy"    	: true,
        "stateSave"     : true,
        "bJQueryUI"     : true,
        "scrollResize"  : true,
        "scrollY"     	: "400px",
        "scrollX"     	: true,
        "scrollCollapse": false, 
        'AutoWidth'     : false,
        "paging"      	: true,
        "info"        	: true,
        "filter"      	: true, 
        "ordering"		: false,
        "responsive"    : true,
        "select"        : true, 
        'ajax'	: {
            "url"   : baseurl+"pt/cptregaudi/getbuscarauditoria/",
            "type"  : "POST", 
            "data": function ( d ) {
                d.ccliente      = $('#cboclieserv').val();
                d.fdesde        = varfdesde; 
                d.fhasta        = varfhasta;   
                d.dnropropu     = $('#txtnropropu').val(); 
                d.dnroinfor     = $('#txtnroinfor').val();
            },     
            dataSrc : ''        
        },  
        'columns'	: [
            {
              "class"     :   "col-xxs",
              orderable   :   false,
              data        :   null,
              targets     :   0
            },
            {data: 'NROINFOR',responsivePriority: 1,  "class": "col-xsm"},
            {data: 'CLIERAZONSOCIAL',"class": "col-xm"},
            {data: 'PROVRAZONSOCIAL',"class": "col-xm"},
            {data: 'DESTABLECIMIENTO',"class": "col-xm"},
            {data: 'FECHINFOR',responsivePriority: 1, "class": "col-s"},
            {data: 'NROPROPU', "class": "col-xm"},
            {responsivePriority: 3, "orderable": false, "class": "col-xs", 
              render:function(data, type, row){                
                  return  '<div>'+ 
                    '<a id="aDelAudi" href="'+row.idptregauditoria+'" idinfor="'+row.idptinforme+'" title="Eliminar" style="cursor:pointer; color:#FF0000;"><span class="fas fa-trash-alt" aria-hidden="true"> </span></a>'+      
                    '&nbsp;'+
                    ' <a data-toggle="modal" title="Editar" style="cursor:pointer; color:#3c763d;" onClick="javascript:selAuditoria(\''+row.idptregauditoria+'\',\''+row.idptinforme+'\',\''+row.idptregistro+'\',\''+row.idresponsable+'\',\''+row.ccliente+'\',\''+row.idptpropuesta+'\',\''+row.FECHINFOR+'\',\''+row.NROINFOR+'\',\''+row.cproveedor+'\',\''+row.cestablecimiento+'\',\''+row.DESTABLECIMIENTO+'\',\''+row.idptservicio+'\',\''+row.fauditoria+'\',\''+row.descripcion_equipo+'\',\''+row.descripcion_producto+'\',\''+row.comentarios+'\',\''+row.descripcion_archivo+'\',\''+row.ruta_informe+'\',\''+row.ARCHIVO+'\',\''+row.idptevaluacion+'\');"><span class="fas fa-external-link-alt" aria-hidden="true"> </span></a>'+
                    '</div>'
              }
            },            
            {"orderable": false, 
              render:function(data, type, row){ 
                bfind = true;   
                  return ' <div>'+
                  ' <a data-toggle="modal" title="Adjuntar" style="cursor:pointer; color:#3c763d;" data-target="#modalDetaInfo" onClick="javascript:listarDetInforme(\''+row.IDINFOR+'\',\''+row.FECHINFOR+'\',\''+row.NROINFOR+'\',\''+row.CANTDET+'\');"class="btn btn-outline-primary btn-sm hidden-xs hidden-sm"><span class="fas fa-folder-open" aria-hidden="true"> </span> DOCUMENTOS ADJUNTOS</a>'+
                    //' <a data-toggle="modal" title="Duplicar" style="cursor:pointer; color:#3c763d;" data-target="#modalCreaInfor" onClick="javascript:SelDuplicarpropu(\''+row.IDPROPU+'\',\''+row.CODCLIENTE+'\',\''+row.NROPROPU+'\',\''+row.FECHPROPU+'\',\''+row.IDSERV+'\',\''+row.DETAPROPU+'\',\''+row.COSTOTOTAL+'\',\''+row.ESTPROPU+'\',\''+row.CONTACTO+'\',\''+row.OBSPROPU+'\',\''+row.SERVNEW+'\',\''+row.CLIPOTEN+'\',\''+row.IDUSUARIO+'\',\''+row.TIPOCOSTO+'\',\''+row.ARCHIVO+'\',\''+row.CODESTABLE+'\',\''+row.RUTA+'\');"class="btn btn-outline-primary btn-sm hidden-xs hidden-sm"><span class="fas fa-clone" aria-hidden="true"> </span> DUPLICAR INFORME</a>'+
                  '</div>' 
              }
            }
        ],
        "columnDefs": [{
            "targets": [1], 
            "data": null, 
            "render": function(data, type, row) { 
                if(row.ARCHIVO != "") {
                    return '<p><a href="'+baseurl+row.ruta_informe+row.ARCHIVO+'" target="_blank" class="pull-left">'+row.NROINFOR+'&nbsp;<i class="fas fa-cloud-download-alt" data-original-title="Descargar" data-toggle="tooltip"></i></a><p>';
                }else{
                    return '<p>'+row.NROINFOR+'</p>';
                }                      
            }
        }],
        fnDrawCallback: function() {
            //$("[name='my-checkbox']").bootstrapSwitch();
        },
    });   
    // Enumeraci
    otblListAudi.on( 'order.dt search.dt', function () { 
        otblListAudi.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i+1;
          } );
    }).draw();  
});

selAuditoria= function(idptregauditoria,idptinforme,idptregistro,idresponsable,ccliente,idptpropuesta,fechinfor,nroinfor,cproveedor,cestablecimiento,destablecimiento,idptservicio,fauditoria,descripcion_equipo,descripcion_producto,comentarios,descripcion_archivo,ruta_informe,archivo_informe,idptevaluacion){
    
    $('#tabptaudi a[href="#tabptaudi-det"]').tab('show'); 
    $('#hdnAccionaudi').val('A'); 

    $('#hdnidauditoria').val(idptregauditoria);
    $('#mhdnIdInfor').val(idptinforme);
    $('#mhdnIdpteval').val(idptevaluacion);
    $('#mhdnIdptregistro').val(idptregistro);
    $('#mhdnIdresponsable').val(idresponsable);
    $('#mtxtFinfor').val(fechinfor);
    $('#mtxtNroinfor').val(nroinfor);   
    $('#cboservicios').val(idptservicio).trigger("change");
    $('#txtFAudi').val(fauditoria);
    $('#txtEquipos').val(descripcion_equipo);
    $('#txtProdLinea').val(descripcion_producto);
    $('#mtxtregcomentario').val(comentarios);
    $('#mcboContacInfor').val(idresponsable).trigger("change");
    $('#mtxtNomarchaudi').val(descripcion_archivo);
    $('#mtxtRutaarchiaudi').val(ruta_informe);
    $('#mtxtArchinfor').val(archivo_informe);
    
    
    $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"pt/cptregaudi/getclientepropu",
        dataType: "JSON",
        async: true,
        success:function(result)
        {
            $('#cboRegClie').html(result);
            $('#cboRegClie').val(ccliente).trigger("change");
        },
        error: function(){
            alert('Error, No se puede autenticar por error');
        }
    }); 
    
    var params = { 
        "ccliente":ccliente 
    };
    $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"pt/cptregaudi/getpropuevaluar",
        dataType: "JSON",
        async: true,
        data: params,
        success:function(result)
        {
          $("#cboRegPropu").html(result);  
          $('#cboRegPropu').val(idptpropuesta).trigger("change");
        },
        error: function(){
          alert('Error, no se puede cargar la lista desplegable de establecimiento');
        }
    });
    $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"pt/cptregaudi/getcboprovxclie",
        dataType: "JSON",
        async: true,
        data: params,
        success:function(result){
            $("#cboregprovclie").html(result);  
            $('#cboregprovclie').val(cproveedor);         
        },
        error: function(){
            alert('Error, No se puede autenticar por error = cboregprovclie');
        }
    });
    

    cboEstablecimiento(ccliente,cproveedor,cestablecimiento,destablecimiento);  
        
};
   
$("body").on("click","#aDelAudi",function(event){
    event.preventDefault();
    idptauditoria = $(this).attr("href");
    idptinforme = $(this).attr("idinfor");

    Swal.fire({
        title: 'Confirmar Eliminación',
        text: "¿Está seguro de eliminar el Informe?",
        icon: 'error',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, bórralo!'
    }).then((result) => {
        if (result.value) {
            $.post(baseurl+"pt/cptregaudi/delauditoria/", 
            {
                idptauditoria   : idptauditoria,
                idptinforme   : idptinforme,
            },      
            function(data){     
                otblListAudi.ajax.reload(null,false); 
                Vtitle = 'Se Elimino Correctamente';
                Vtype = 'success';
                sweetalert(Vtitle,Vtype);      
            });
        }
    }) 
});

$("#btnNuevo").click(function (){
    $('#tabptaudi a[href="#tabptaudi-det"]').tab('show'); 
    
    $('#frmRegAudi').trigger("reset");

    $('#hdnidauditoria').val('');     
    $('#hdnAccionaudi').val('N');
    
    $('#txtRegClie').hide();
    $('#divRegClie').show(); 
    $('#txtRegPropu').hide();
    $('#divRegPropu').show();

    $('#mtxtFreginforme,#txtFAuditoria').datetimepicker({
        format: 'DD/MM/YYYY',
        daysOfWeekDisabled: [0],
        locale:'es'
    });	

    fechaActualinfor();
    nro_informe(); 
    
    $('#lbchkinf').show();
    
    $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"pt/cptregaudi/getclientepropu",
        dataType: "JSON",
        async: true,
        success:function(result)
        {
            $('#cboRegClie').html(result);
        },
        error: function(){
            alert('Error, No se puede autenticar por error');
        }
    }); 
});

fechaActualinfor= function(){
    var fecha = new Date();	
    var fechatring = ("0" + fecha.getDate()).slice(-2) + "/" + ("0"+(fecha.getMonth()+1)).slice(-2) + "/" +fecha.getFullYear() ;
    
    $('#mtxtFreginforme').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY') );	
    $('#txtFAuditoria').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY') );
};

$("#chkNroAntiguo").on("change", function () {
    var v_fecha = $('#mtxtFinfor').val().substr(6);
    var v_fechaactual = new Date().getFullYear();
    if (v_fechaactual == v_fecha){
        alert("Debe de Seleccionar una fecha anterior");
        $(document.getElementById('chkNroAntiguo')).prop('checked', false);
    }else{
        if($("#chkNroAntiguo").is(":checked") == true){ 
            $("#mtxtNroinfor").prop({readonly:false}); 
        }else if($("#chkNroAntiguo").is(":checked") == false){ 
            $("#mtxtNroinfor").prop({readonly:true}); 
        }; 
        
        if ($('#mhdnAccionInfor').val()=='N'){
            nro_informe();
        }
    }    
}); 

function nro_informe(){
    var vyearPropu = $('#mtxtFinfor').val().substr(6);
    var params = { 
        "yearPropu" : vyearPropu
    }; 

    $.ajax({
      type: 'ajax',
      method: 'post',
      url: baseurl+"pt/cinforme/getnroinforme",
      dataType: "JSON",
      async: true,
      data: params,
      success: function (result){
        var c = (result);
        $.each(c,function(i,item){
          $('#mtxtNroinfor').val(item.NRO_INFO);
        })
      },
      error: function(){
        alert('Error, no se genero Nro. Propuesta');
      }
    })
};

$("#cboRegClie").change(function(){
    var_accion = $('#hdnAccionaudi').val();
    if (var_accion == 'N'){
        var v_RegClie = $('#cboRegClie').val();
        var params = { 
            "ccliente":v_RegClie 
        };
        $.ajax({
            type: 'ajax',
            method: 'post',
            url: baseurl+"pt/cptregaudi/getpropuevaluar",
            dataType: "JSON",
            async: true,
            data: params,
            success:function(result)
            {
            $("#cboRegPropu").html(result);  
            $('#txtservicio').val('');
            $('#hdnidserv').val('');	
            },
            error: function(){
            alert('Error, no se puede cargar la lista desplegable de establecimiento');
            }
        });

        var v_cboclieserv = $('#cboRegClie').val();
        var params1 = { "ccliente":v_cboclieserv };
        $.ajax({
            type: 'ajax',
            method: 'post',
            url: baseurl+"pt/cptregaudi/getcboprovxclie",
            dataType: "JSON",
            async: true,
            data: params1,
            success:function(result){
                $("#cboregprovclie").html(result);  
                $('#cboregprovclie').val('').trigger("change");         
            },
            error: function(){
                alert('Error, No se puede autenticar por error = cboregprovclie');
            }
        });
    }
    
}); 

$("#cboregprovclie").change(function(){
    
    var v_cboclie = $('#cboRegClie').val();
    var v_cboprov = $('#cboregprovclie').val();  

    cboEstablecimiento(v_cboclie,v_cboprov,'','');  
});

cboEstablecimiento = function(cboclie,cboprov,idestable,ddirec){

    var params = { 
        "ccliente":cboclie, 
        "cproveedor":cboprov,
    };
    $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"pt/cptregaudi/getcboregestable",
        dataType: "JSON",
        async: true,
        data: params,
        success:function(result){
            $("#cboregestable").html(result);
            $('#cboregestable').val(idestable).trigger("change"); 
            //$('#mtxtregdirestable').val(ddirec);           
        },
        error: function(){
            alert('Error, No se puede autenticar por error = getcboregestable');
        }
    }); 
};

$("#cboregestable").change(function(){
    var v_cboclie = $('#cboRegClie').val();
    var v_cboestable = $('#cboregestable').val();

    var parametros = { 
        "cestablecimiento":v_cboestable
    };
    var request = $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"pt/cptregaudi/getdirestable",
        dataType: "JSON",
        async: true,
        data: parametros,
        error: function(){
            alert('Error, No se puede autenticar por error = getdirestable');
        }
    });      
    request.done(function( respuesta ) {            
        $.each(respuesta, function() {
            $('#mtxtregdirestable').val(this.DIRESTABLE);   
        });
    });
});

escogerArchivoAudi = function(){    
    var archivoInput = document.getElementById('mtxtArchivoaudi');
    var archivoRuta = archivoInput.value;
    var extPermitidas = /(.pdf|.docx|.xlsx|.doc|.xls)$/i;
    
    var filename = $('#mtxtArchivoaudi').val().replace(/.*(\/|\\)/, '');
    $('#mtxtNomarchaudi').val(filename);

    if(!extPermitidas.exec(archivoRuta)){
        alert('Asegurese de haber seleccionado un PDF, DOCX, DOC, XSLX, XSL');
        archivoInput.value = '';  
        $('#mtxtNomarchaudi').val('');
        return false;
    }      
    $('#sArchivo').val('S');
};

subirArchivoAudi =function(){
    var parametrotxt = new FormData($("#frmRegAudi")[0]);
    var request = $.ajax({
        data: parametrotxt,
        method: 'post',
        url: baseurl+"pt/cptregaudi/subirArchivoAudi/",
        dataType: "JSON",
        async: true,
        contentType: false,
        processData: false,
        error: function(){
            alert('Error, no se cargó el archivo');
        }
    });
    request.done(function( respuesta ) {
        $('#btnBuscar').click(); 
        Vtitle = 'Guardo Correctamente';
        Vtype = 'success';
        sweetalert(Vtitle,Vtype);
    });
};

$('#btnRetornarLista').click(function(){
    $('#tabptaudi a[href="#tabptaudi-list"]').tab('show'); 
    //$('#btnBuscar').click(); 
});



$('#modalDetaInfo').on('shown.bs.modal', function (e) {
    //
});
listarDetInforme=function(idinfo,fechainfo,nroinfo,cantdet){
    $('#mtxtiddetainfo').val(idinfo);
    $('#mtxtfechadetainfo').val(fechainfo);
    $('#mtxtnrodetainfo').val(nroinfo);
    $('#mtxtcantdetainfo').val(cantdet);

    otblDetainfo = $('#tblDetainfo').DataTable({  
        'responsive'    : true,
        'bJQueryUI'     : true,
        'scrollY'     	: '250px',
        'scrollX'     	: true, 
        'paging'      	: false,
        'processing'  	: true,      
        'bDestroy'    	: true,
        "AutoWidth"     : false,
        'info'        	: true,
        'filter'      	: false, 
        "ordering"		: false,  
        'stateSave'     : true,
        'ajax'	: {
            "url"   : baseurl+"pt/cinforme/getbuscardetainfo/",
            "type"  : "POST", 
            "data": function ( d ) { 
                d.idptinforme  = idinfo; 
            },     
            dataSrc : ''        
        },
        'columns'	: [
            {orderable : false, data : 'POS', targets : 0 },
            {data: 'NOMARCH', targets: 1 },
            {"orderable": false, 
              render:function(data, type, row){
                return  '<div>'+  
                            '<a href="'+baseurl+row.RUTAFILE+row.ARCHIVO+'" target="_blank" class="btn btn-default btn-xs pull-left"><i class="fas fa-cloud-download-alt fa-2x" data-original-title="Descargar" data-toggle="tooltip"></i></a>'+
                        '</div>'   
              }
            },
            {"orderable": false, 
              render:function(data, type, row){
                return  '<div>'+  
                          '<a id="aDelDetInfo" href="'+row.ITEM+'" title="Eliminar" style="cursor:pointer; color:#FF0000;"><span class="fas fa-trash-alt fa-2x" aria-hidden="true"> </span></a>'+
                        '</div>'   
              }
            }
        ],
    });   
}; 
subirDetInforme=function(){
    var archivoInput = document.getElementById('mtxtDetArchivoinfo');
    var archivoRuta = archivoInput.files;
    var i;
    var result = [];

    for(i = 0; i < archivoRuta.length; i++){
        result[i] = archivoRuta[i].name;  
    }

    var extPermitidas = /(.pdf|.docx|.xlsx|.doc|.xls)$/i;

    if(!extPermitidas.exec(result)){
        alert('Asegurese de haber seleccionado un PDF, DOCX, XSLX, MDB');
        archivoInput.value = '';
        return false;
    }
    else
    {
        var parametrotxt = new FormData($("#frmDetaInfo")[0]);
        var request = $.ajax({
            data: parametrotxt,
            method: 'post',
            url: baseurl+"pt/cinforme/archivo_detinforme/",
            dataType: "JSON",
            async: true,
            contentType: false,
            processData: false,
            error: function(){
                alert('Error, no se cargó el archivo');
            }
        });
        request.done(function( respuesta ) {      
            $('#frmDetaInfo').trigger("reset");
            otblDetainfo.ajax.reload(null,false);
        });
    }
};
   
$("body").on("click","#aDelDetInfo",function(event){
    event.preventDefault();
    item = $(this).attr("href");

    Swal.fire({
        title: 'Confirmar Eliminación',
        text: "¿Está seguro de eliminar el Archivo Adjunto?",
        icon: 'error',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, bórralo!'
    }).then((result) => {
        if (result.value) {
            $.post(baseurl+"pt/cinforme/deldetinforme/", 
            {
                item   : item,
            },      
            function(data){     
                otblDetainfo.ajax.reload(null,false); 
                Vtitle = 'Se Elimino Correctamente';
                Vtype = 'success';
                sweetalert(Vtitle,Vtype);      
            });
        }
    }) 
});

