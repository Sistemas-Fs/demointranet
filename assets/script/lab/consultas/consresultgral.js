
var otblListconsresultgral, otblElementos;

$(document).ready(function() {
    
    $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"lab/coti/ccotizacion/getcboclieserv",
        dataType: "JSON",
        async: true,
        success:function(result){
            $('#cboclieserv').html(result);
        },
        error: function(){
            alert('Error, No se puede autenticar por error');
        }
    });

    $('#txtFDesde,#txtFHasta').datetimepicker({
        format: 'DD/MM/YYYY',
        daysOfWeekDisabled: [0],
        locale:'es'
    });    

    fechaAnioActual();
});

fechaActual = function(){
    var fecha = new Date();		
    var fechatring = ("0" + fecha.getDate()).slice(-2) + "/" + ("0"+(fecha.getMonth()+1)).slice(-2) + "/" +fecha.getFullYear() ;

    $('#txtFDesde').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY') );
    $('#txtFHasta').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY') );
    $('#mtxtFanalisis').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY') );

};

fechaAnioActual = function(){
    $("#txtFIni").prop("disabled",false);
    $("#txtFFin").prop("disabled",false);
        
    varfdesde = '';
    varfhasta = '';
    
    var fecha = new Date();		
    var fechatring1 = "01/01/" + (fecha.getFullYear()) ;
    var fechatring2 = ("0" + fecha.getDate()).slice(-2) + "/" + ("0"+(fecha.getMonth()+1)).slice(-2) + "/" +fecha.getFullYear() ;

    $('#txtFDesde').datetimepicker('date', moment(fechatring1, 'DD/MM/YYYY') );
    $('#txtFHasta').datetimepicker('date', moment(fechatring2, 'DD/MM/YYYY') );
};

	
$('#txtFDesde').on('change.datetimepicker',function(e){	
    
    $('#txtFHasta').datetimepicker({
        format: 'DD/MM/YYYY',
        daysOfWeekDisabled: [0],
        locale:'es'
    });	

    var fecha = moment(e.date).format('DD/MM/YYYY');		
    
    $('#txtFHasta').datetimepicker('minDate', fecha);
    $('#txtFHasta').datetimepicker('date', fecha);

});


$("#btnBuscar").click(function (){
    listarBusqueda();
});

listarBusqueda = function(){
    if(varfdesde != '%'){ varfdesde = $('#txtFIni').val(); }
    if(varfhasta != '%'){ varfhasta = $('#txtFFin').val(); } 
    $("#btnexcel").removeAttr("disabled");
    
    otblListconsresultgral = $('#tblListconsresultgral').DataTable({  
        'responsive'    : false,
        'bJQueryUI'     : true,
        'scrollY'     	: '500px',
        'scrollX'     	: true, 
        'paging'      	: true,
        'processing'  	: true,     
        'bDestroy'    	: true,
        'AutoWidth'     : false,
        'info'        	: true,
        'filter'      	: true, 
        'ordering'		: false,  
        'stateSave'     : true,
        'select'        : true,
        'ajax'	: {
            "url"   : baseurl+"lab/consultas/cconsresultgral/resultadosgeneral/",
            "type"  : "POST", 
            "data": function ( d ) {
                d.ccliente      = $('#cboclieserv').val();  
                d.dcotizacion   = $('#txtbuscar').val();
                d.fini          = varfdesde;
                d.ffin          = varfhasta;  
            },     
            dataSrc : ''        
        },
        'columns'	: [
            {data : null, "class" : "col-xxs"},         
            {data: 'DCOTIZACION', "class" : "col-sm"},
            {data: 'FEC_RECEP', "class" : "col-s"},
            {data: 'DREALPRODUCTO', "class" : "col-sm"},
            {data: 'DPRESENTACION', "class" : "col-sm"},
            {data: 'DCONDICION', "class" : "col-s"},
            {"orderable": false, "class" : "col-xm",
                render:function(data, type, row){
                    
                    if(row.ZCTIPOENSAYO == "068" || row.ZCTIPOENSAYO == "069" || row.ZCTIPOENSAYO == "A61" || row.ZCTIPOENSAYO == "070" || row.ZCTIPOENSAYO == "736"){ // normal                         
                        return '<div>'+row.DENSAYO+'</div>';
                    } else { // elementos
                        return '<div>'+row.DENSAYO+'&nbsp;&nbsp;&nbsp;<a title="Elementos" style="cursor:pointer; color:navy;" onClick="verElementos('+row.CINTERNOORDENSERVICIO+',\''+row.CINTERNOCOTIZACION+'\','+row.NORDENPRODUCTO+',\''+row.CMUESTRA+'\',\''+row.CENSAYO+'\','+row.NVIAUSADO+');"><span class="fas fa-folder-plus" aria-hidden="true"></span></a></div>'
                    }
                }
            },
            {data: 'NVIAUSADO', "class" : "col-xxs"},
            {data: 'DRESULTADO', "class" : "col-s"},
            {data: 'UNIDAD', "class" : "col-s"},
            {data: 'ESTADO', "class" : "col-s"},
        ],
    });    
    // Enumeracion 
    otblListconsresultgral.on( 'order.dt search.dt', function () { 
        otblListconsresultgral.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i+1;
          } );
    }).draw();  
}; 

verElementos = function(cinternoordenservicio,cinternocotizacion,nordenproducto,cmuestra,censayo,nviausado){
    $('#modalElementos').modal({show:true});

    $('#mhdecinternoordenservicio').val(cinternoordenservicio);
    $('#mhdecinternocotizacion').val(cinternocotizacion);
    $('#mhdenordenproducto').val(nordenproducto);
    $('#mhdecmuestra').val(cmuestra);
    $('#mhdecensayo').val(censayo);
    $('#mhdenviausado').val(nviausado);

    listarElementos(cinternoordenservicio,cinternocotizacion,nordenproducto,cmuestra,censayo,nviausado);           
    
};
listarElementos = function(cinternoordenservicio,cinternocotizacion,nordenproducto,cmuestra,censayo,nviausado){
   
    var parametros = {
        "cinternoordenservicio" : cinternoordenservicio,
        "cinternocotizacion" : cinternocotizacion,
        "nordenproducto" : nordenproducto,
        "cmuestra" : cmuestra,
        "censayo" : censayo,
        "nviausado" : nviausado,
    };    

    otblElementos = $('#tblElementos').DataTable({ 
        "processing"  	: true,
        "bDestroy"    	: true,
        "stateSave"     : true,
        "bJQueryUI"     : true,
        "scrollResize"  : true,
        "scrollY"     	: "350px",
        "scrollX"     	: true, 
        'AutoWidth'     : false,
        "paging"      	: false,
        "info"        	: false,
        "filter"      	: true, 
        "ordering"		: false,
        "responsive"    : false,
        "select"        : true,
        "ajax"	: {
            "url"   : baseurl+"lab/consultas/cconsresultgral/getlistelementos",
            "type"  : "POST", 
            "data"  : parametros,     
            dataSrc : ''      
        }, 
        'columns'	: [
            {data: 'BLANCO', "className":"col-xxxs"},
            {data: 'delemento'},
            {data: 'dunidad'},
            {data: 'DRESULTADO'},
        ]
    });
    // Enumeracion 
    otblElementos.on( 'order.dt search.dt', function () { 
        otblElementos.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i+1;
          } );
    }).draw();   
}