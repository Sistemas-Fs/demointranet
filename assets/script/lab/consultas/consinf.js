
var otblListconsinf, otblListinforme

$(document).ready(function() {
    
    $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"lab/coti/ccotizacion/getcboclieserv",
        dataType: "JSON",
        async: true,
        success:function(result){
            $('#cboclieserv').html(result);
        },
        error: function(){
            alert('Error, No se puede autenticar por error');
        }
    });

    $('#txtFDesde,#txtFHasta').datetimepicker({
        format: 'DD/MM/YYYY',
        daysOfWeekDisabled: [0],
        locale:'es'
    });    

    fechaAnioActual();
    $('#chkFreg').prop("checked", false);
    $('#chkFreg').trigger("change"); 
});

fechaActual = function(){
    var fecha = new Date();		
    var fechatring = ("0" + fecha.getDate()).slice(-2) + "/" + ("0"+(fecha.getMonth()+1)).slice(-2) + "/" +fecha.getFullYear() ;

    $('#txtFDesde').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY') );
    $('#txtFHasta').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY') );
    $('#mtxtFanalisis').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY') );

};

fechaAnioActual = function(){
    $("#txtFIni").prop("disabled",false);
    $("#txtFFin").prop("disabled",false);
        
    varfdesde = '';
    varfhasta = '';
    
    var fecha = new Date();		
    var fechatring1 = "01/01/" + (fecha.getFullYear()) ;
    var fechatring2 = ("0" + fecha.getDate()).slice(-2) + "/" + ("0"+(fecha.getMonth()+1)).slice(-2) + "/" +fecha.getFullYear() ;

    $('#txtFDesde').datetimepicker('date', moment(fechatring1, 'DD/MM/YYYY') );
    $('#txtFHasta').datetimepicker('date', moment(fechatring2, 'DD/MM/YYYY') );
};

	
$('#txtFDesde').on('change.datetimepicker',function(e){	
    
    $('#txtFHasta').datetimepicker({
        format: 'DD/MM/YYYY',
        daysOfWeekDisabled: [0],
        locale:'es'
    });	

    var fecha = moment(e.date).format('DD/MM/YYYY');		
    
    $('#txtFHasta').datetimepicker('minDate', fecha);
    $('#txtFHasta').datetimepicker('date', fecha);

});

$("#chkFreg").on("change", function () {
    if($("#chkFreg").is(":checked") == true){ 
        $("#txtFIni").prop("disabled",false);
        $("#txtFFin").prop("disabled",false);
        
        varfdesde = '';
        varfhasta = '';

        
        var fecha = new Date();		
        var fechatring1 = "01/01/" + (fecha.getFullYear()) ;
        var fechatring2 = ("0" + fecha.getDate()).slice(-2) + "/" + ("0"+(fecha.getMonth()+1)).slice(-2) + "/" +fecha.getFullYear() ;
        $('#txtFDesde').datetimepicker('date', fechatring1);
        $('#txtFHasta').datetimepicker('date', fechatring2);

    }else if($("#chkFreg").is(":checked") == false){ 
        $("#txtFIni").prop("disabled",true);
        $("#txtFFin").prop("disabled",true);
        
        varfdesde = '%';
        varfhasta = '%';

        fechaActual();
    }; 
});

$("#btnBuscar").click(function (){
    listarBusqueda();
});

listarBusqueda = function(){
    if(varfdesde != '%'){ varfdesde = $('#txtFIni').val(); }
    if(varfhasta != '%'){ varfhasta = $('#txtFFin').val(); } 
    
    otblListconsinf = $('#tblListconsinf').DataTable({  
        'responsive'    : false,
        'bJQueryUI'     : true,
        'scrollY'     	: '600px',
        'scrollX'     	: true, 
        'paging'      	: true,
        'processing'  	: true,     
        'bDestroy'    	: true,
        'AutoWidth'     : false,
        'info'        	: true,
        'filter'      	: true, 
        'ordering'		: false,  
        'stateSave'     : true,
        'select'        : true,
        'ajax'	: {
            "url"   : baseurl+"lab/consultas/cconsinf/consultaformatos/",
            "type"  : "POST", 
            "data": function ( d ) {
                d.ccliente      = $('#cboclieserv').val(); 
                d.tipobuscar    = $('#cbotipobuscar').val(); 
                d.descripcion   = $('#txtbuscar').val();
                d.fechabuscar    = $('#cbofechapor').val(); 
                d.fini          = varfdesde;
                d.ffin          = varfhasta;  
            },     
            dataSrc : ''        
        },
        'columns'	: [
            {data : null, "class" : "details-control col-xxs"},         
            {data: 'DCLIENTE'},
            {data: 'TIPOINF'},
            {data: 'dcotizacion'},
            {data: 'fcotizacion'},
            {data: 'nordenservicio'},
            {data: 'fordenservicio'},
        ],
        "columnDefs": [
            {
                "targets": [0], 
                "createdCell": function (td, cellData, rowData, row, col) {
                        if (rowData.TIENEOT == 'N') {
                            $(td).removeClass( 'details-control' );
                        }
                }
            },
            {
                "targets": [3], 
                "data": null, 
                "render": function(data, type, row) {
                    return '<div>'+
                    '    <p><a title="Cotizacion" style="cursor:pointer;" onclick="pdfCoti(\'' + row.cinternocotizacion + '\');"  class="pull-left">'+row.dcotizacion+'&nbsp;&nbsp;<i class="fas fa-file-pdf" style="color:#FF0000;"></i></a><p>' +
                    '</div>';
                }
            },
            {
                "targets": [5], 
                "data": null, 
                "render": function(data, type, row) {
                    return '<div>'+
                    '    <p><a title="OT" style="cursor:pointer;" onclick="pdfOT(\'' + row.cinternoordenservicio + '\');" class="pull-left">'+row.nordenservicio+'&nbsp;&nbsp;<i class="fas fa-file-pdf" style="color:#FF0000;"></i></a><p>' +
                    '</div>';
                }
            }
        ],
    });    
    // Enumeracion 
    otblListconsinf.on( 'order.dt search.dt', function () { 
        otblListconsinf.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i+1;
          } );
    }).draw();  
}; 
/* DETALLE TRAMITES */
$('#tblListconsinf tbody').on( 'click', 'td.details-control', function () {
            
    var tr = $(this).parents('tr');
    var row = otblListconsinf.row(tr);
    var rowData = row.data();
    
    if ( row.child.isShown() ) {                    
        row.child.hide();
        tr.removeClass( 'details' );
    }
    else {
        otblListconsinf.rows().every(function(){
            if(this.child.isShown()){
                this.child.hide();
                $(this.node()).removeClass('details');
            }
        })
        row.child( 
           '<table id="tblListinforme" class="display compact" style="width:100%;  background-color:#D3DADF; padding-top: -10px; border-bottom: 2px solid black;">'+
           '<thead style="background-color:#FFFFFF;"><tr><th>MUESTRAS</th><th>NRO INFORME</th><th>FECHA INFORME</th><th>NRO CERTIFICADO</th><th>FECHA CERTIFICADO</th></thead><tbody>' +
            '</tbody></table>').show();
            
            otblListinforme = $('#tblListinforme').DataTable({
                "bJQueryUI"     : true,
                'bStateSave'    : true,
                'scrollY'       : false,
                'scrollX'       : true,
                'scrollCollapse': false,
                'bDestroy'    : true,
                'paging'      : false,
                'info'        : false,
                'filter'      : false,   
                'stateSave'   : true,
                'ajax'        : {
                    "url"   : baseurl+"lab/consultas/cconsinf/detconsultaformatos/",
                    "type"  : "POST", 
                    "data": function ( d ) {
                        d.cinternoordenservicio = rowData.cinternoordenservicio;
                        d.zctipoinforme = rowData.zctipoinforme;
                    },     
                    dataSrc : ''        
                },
                'columns'     : [
                    {"orderable": false, data: 'cmuestra'},
                    {"orderable": false, data: 'dinfensayo'},
                    {"orderable": false, data: 'finfensayo'},
                    {"orderable": false, data: 'dnumeroinforme'},
                    {"orderable": false, data: 'fcertificado'},
                              
                ], 
                "columnDefs": [
                    {
                        "targets": [1], 
                        "data": null, 
                        "render": function(data, type, row) {
                            return '<div>'+
                            '    <p><a title="Informe" style="cursor:pointer;" onclick="pdfInfensayo(\'' + row.cinternoordenservicio + '\',\'' + row.tipoinforme + '\',\'' + row.cmuestra + '\',\'' + row.idlabinformes + '\');"  class="pull-left">'+row.dinfensayo+'&nbsp;&nbsp;<i class="fas fa-file-pdf" style="color:#FF0000;"></i></a>' +
                            '    <a title="Informe Sin Logo" style="cursor:pointer;" onclick="pdfInfensayoSinLogo(\'' + row.cinternoordenservicio + '\',\'' + row.tipoinforme + '\',\'' + row.cmuestra + '\',\'' + row.idlabinformes + '\');"  class="pull-left">&nbsp;<i class="fas fa-file-pdf" style="color:#4F8FF7;"></i></a><p>' +
                            '</div>';
                        }
                    },
                    {
                        "targets": [3], 
                        "data": null, 
                        "render": function(data, type, row) {
                            if(row.dnumeroinforme == ''){
                                return '<div></div>'
                            }else{
                                return '<div>'+
                                '    <p><a title="OT" style="cursor:pointer;" onclick="pdfCerticalidad(\'' + row.cinternoordenservicio + '\',\'' + row.tipoinforme + '\',\'' + row.cmuestra + '\',\'' + row.idlabinformes + '\');" class="pull-left">'+row.dnumeroinforme+'&nbsp;&nbsp;<i class="fas fa-file-pdf" style="color:#FF0000;"></i></a><p>' +
                                '</div>';
                            }
                        }
                    }
                ],
            });

        tr.addClass('details');
    }
});

pdfCoti = function(idcoti){
    var nversion = 0;
    window.open(baseurl+"lab/formatospdf/pdfcotizacion/pdfCoti/"+idcoti+"/"+nversion);
};
pdfOT = function(cinternoordenservicio){
    window.open(baseurl+"lab/formatospdf/pdfrecepcion/pdfOrderServ/"+cinternoordenservicio);
};

pdfInfensayo = function(cinternoordenservicio,tipoinforme,cmuestra, idlabinformes){    
    var_idot = cinternoordenservicio;
    var_tipoinf = tipoinforme;    
    var_idmuestra = cmuestra;
    var_idlabinformes = idlabinformes;

    if(var_tipoinf == '067'){ // muestra
        window.open(baseurl+"lab/formatospdf/pdfinformes/pdfInfensayoMuestra/"+var_idot+"/"+var_idmuestra);
    }else if(var_tipoinf == 'K50'){ // resultado-indv
        window.open(baseurl+"lab/formatospdf/pdfinformes/pdfInfensayoPersonalizado/"+var_idot+"/"+var_idmuestra+"/"+var_idlabinformes);
    }
}
pdfInfensayoSinLogo = function(cinternoordenservicio,tipoinforme,cmuestra, idlabinformes){    
    var_idot = cinternoordenservicio;
    var_tipoinf = tipoinforme;    
    var_idmuestra = cmuestra;
    var_idlabinformes = idlabinformes;
    var_firma = 'S';

    Swal.fire({
        title: 'INFORME CON FIRMA',
        text: "¿Está seguro de Emitir con Firma?",
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, con firma!',        
        cancelButtonText: 'sin firma!'
    }).then((result) => {
        if (result.value) {
            var_firma = 'S'
            if(var_tipoinf == '067'){ // muestra
                window.open(baseurl+"lab/formatospdf/pdfinformes/pdfInfensayoMuestraSinLogo/"+var_idot+"/"+var_idmuestra+"/"+var_firma);
            }else if(var_tipoinf == 'K50'){ // resultado-indv
                window.open(baseurl+"lab/formatospdf/pdfinformes/pdfInfensayoPersonalizadoSinLogo/"+var_idot+"/"+var_idmuestra+"/"+var_idlabinformes+"/"+var_firma);
            }
        }else{
            var_firma = 'N'
            if(var_tipoinf == '067'){ // muestra
                window.open(baseurl+"lab/formatospdf/pdfinformes/pdfInfensayoMuestraSinLogo/"+var_idot+"/"+var_idmuestra+"/"+var_firma);
            }else if(var_tipoinf == 'K50'){ // resultado-indv
                window.open(baseurl+"lab/formatospdf/pdfinformes/pdfInfensayoPersonalizadoSinLogo/"+var_idot+"/"+var_idmuestra+"/"+var_idlabinformes+"/"+var_firma);
            }
        }
    })    
    
}
pdfCerticalidad = function(cinternoordenservicio,tipoinforme,cmuestra,idlabinformes){    
    var_idot = cinternoordenservicio;
    var_tipocerti = tipoinforme;    
    var_idmuestra = cmuestra;
    var_idlabinformes = idlabinformes;

    if(var_tipocerti == '067'){ //consolidado
        window.open(baseurl+"lab/formatospdf/pdfinformes/pdfCertificadoIndiv/"+var_idot+"/"+var_idmuestra);
    }else if(var_tipocerti == 'K50'){ // bambas
        window.open(baseurl+"lab/formatospdf/pdfinformes/pdfCertificadoPersonalizado/"+var_idot+"/"+var_idmuestra+"/"+var_idlabinformes);
    }
    
}