var oTableListEnsayo, oTableListLaboratorio, oTableMatriz;

$(document).ready(function() {

    $('#tablabensayo a[href="#tablabensayo-list-tab"]').attr('class', 'disabled');
    $('#tablabensayo a[href="#tablabensayo-reg-tab"]').attr('class', 'disabled active');

    $('#tablabensayo a[href="#tablabensayo-list-tab"]').not('#store-tab.disabled').click(function(event){
        $('#tablabensayo a[href="#tablabensayo-list"]').attr('class', 'active');
        $('#tablabensayo a[href="#tablabensayo-reg"]').attr('class', '');
        return true;
    });
    $('#tablabensayo a[href="#tablabensayo-reg-tab"]').not('#bank-tab.disabled').click(function(event){
        $('#tablabensayo a[href="#tablabensayo-reg"]').attr('class' ,'active');
        $('#tablabensayo a[href="#tablabensayo-list"]').attr('class', '');
        return true;
    });
    
    $('#tablabensayo a[href="#tablabensayo-list"]').click(function(event){return false;});
    $('#tablabensayo a[href="#tablabensayo-reg"]').click(function(event){return false;});
    
    $('#divDetalle').hide();

    $('#frmMantensayo').validate({
        rules: {
          cboSTipoensayo: {
            required: true,
          },
          cboZTipoensayo: {
            required: true,
          },
          txtDensayo: {
            required: true,
          },
        },
        messages: {
          cboSTipoensayo: {
            required: "Seleccionar clase de ensayo"
          },
          cboZTipoensayo: {
            required: "Seleccionar el tipo de ensayo"
          },
          txtDensayo: {
            required: "Ingrese una descripcion"
          },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        },        
        submitHandler: function (form) {
            const botonEvaluar = $('#btnGrabar');
            var request = $.ajax({
                url:$('#frmMantensayo').attr("action"),
                type:$('#frmMantensayo').attr("method"),
                data:$('#frmMantensayo').serialize(),
                error: function(){
                    Vtitle = 'Error en Guardar!!!';
                    Vtype = 'error';
                    sweetalert(Vtitle,Vtype); 
                    objPrincipal.liberarBoton(botonEvaluar);  
                },
                beforeSend: function() {
                    objPrincipal.botonCargando(botonEvaluar);
                }
            });
            request.done(function( respuesta ) {
                var posts = JSON.parse(respuesta);
                
                $.each(posts, function() {
                    $('#divDetalle').show();
                    $('#hdnAccregensayo').val('A'); 
                    $('#hdnIdcensayo').val(this.censayo);
                    $('#txtCodigofs').val(this.censayofs);
                   
                    Vtitle = 'EL Ensayo se Guardo Correctamente!!!';
                    Vtype = 'success';
                    sweetalert(Vtitle,Vtype);   
                    objPrincipal.liberarBoton(botonEvaluar);    
                });
            });
            return false;
        }
    });
});


$('#btnBuscar').click(function(){
    listarensayo();
  });

listarensayo = function(){
    oTableListEnsayo = $('#tblListEnsayo').DataTable({
      "processing"  	: true,
      "bDestroy"    	: true,
      "stateSave"       : true,
      "bJQueryUI"       : true,
      "scrollY"     	: "500px",
      "scrollX"     	: true, 
      'AutoWidth'       : true,
      "paging"      	: true,
      "info"        	: true,
      "filter"      	: true, 
      "ordering"		: false,
      "responsive"      : false,
      "select"          : true, 
      'ajax'            : {
        "url"   : baseurl+"lab/mante/cmantensayo/getbuscarensayo",
        "type"  : "POST", 
        "data": function ( d ) {
            d.ensayo = $('#txtensayo').val();   
        },     
        dataSrc : ''        
      },
      'columns'     : [
          {data: 'SPACE', "class": "col-xxs"},
          {data: 'censayofs', "class": "col-xs"},
          {data: 'densayo'},
          {data: 'DTIPOENSAYO', "class": "col-s"},
          {data: 'ACREADITADO', "class": "col-xs"},
          {data: 'sregistro', "class": "col-xxs"}
      ], 
    });    
    // Enumeracion 
    oTableListEnsayo.on( 'order.dt search.dt', function () { 
        oTableListEnsayo.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i+1;
          } );
    }).draw();     
};
$('#tblListEnsayo tbody').on('dblclick', 'td', function () {
  var tr = $(this).parents('tr');
  var row = oTableListEnsayo.row(tr);
  var rowData = row.data();

  editarEnsayo(rowData.censayo,rowData.stipoensayo,rowData.censayofs,rowData.sversion,rowData.densayo,rowData.unidad_original,rowData.zctipoensayo,rowData.dtitulonorma,rowData.dnorma,rowData.naniopublicacion,rowData.sacnoac,rowData.sregistro,rowData.valorvr);
});

$('#btnNuevo').click(function(){    
    $('#tablabensayo a[href="#tablabensayo-reg"]').tab('show'); 

    limpiarfrmMantensayo(); 
    iniEnsayo('');
    listcboreglab();

    $("#cboSTipoensayo").prop({disabled:false}); 
    $("#txtCodigofs").prop({readonly:false});

    $('#divDetalle').hide();
    $('#divregLaboratorio').hide();
    $('#divregMatriz').hide();

    $('#btnaddLab').show();
    $('#btncancelLab').hide();
    $('#btnsaveLab').hide();

    $('#btnaddMatriz').show();
    $('#btncancelMatriz').hide();
    $('#btnsaveMatriz').hide();

    $('#hdnAccregensayo').val('N'); 
});

editarEnsayo = function(censayo,stipoensayo,censayofs,sversion,densayo,unidad_original,zctipoensayo,dtitulonorma,dnorma,naniopublicacion,sacnoac,sregistro,valorvr){
    limpiarfrmMantensayo();

    $('#hdnIdcensayo').val(censayo);    
    $('#cboSTipoensayo').val(stipoensayo).trigger("change");   
    $('#txtCodigofs').val(censayofs);    
    $('#cboSVersion').val(sversion).trigger("change"); 
    $('#txtDensayo').val(densayo);    
    $('#txtUM').val(unidad_original);    
    $('#txtTituloNorma').val(dtitulonorma);     
    $('#txtDnorma').val(dnorma);    
    $('#txtAniopubli').val(naniopublicacion);    
    $('#cbosacnoac').val(sacnoac).trigger("change");         
    $('#txtValorvr').val(valorvr); 

    $("#cboSTipoensayo").prop({disabled:true}); 
    $("#txtCodigofs").prop({readonly:true});   

    iniEnsayo(zctipoensayo);
    listcboreglab();

    $('#hdnAccregensayo').val('A'); 

    $('#divDetalle').show();
    $('#divregLaboratorio').hide();
    $('#divregMatriz').hide();

    $('#btnaddLab').show();
    $('#btncancelLab').hide();
    $('#btnsaveLab').hide();

    $('#btnaddMatriz').show();
    $('#btncancelMatriz').hide();
    $('#btnsaveMatriz').hide();

    $('#tablabensayo a[href="#tablabensayo-reg"]').tab('show');
    listarlaboratorio(censayo); 
    listmartriz(censayo); 
};

iniEnsayo = function(a_zctipoensayo){
    
    $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"lab/mante/cmantensayo/getcboztipoensayo",
        dataType: "JSON",
        async: true,
        success:function(result){
            $('#cboZTipoensayo').html(result);
            $('#cboZTipoensayo').val(a_zctipoensayo).trigger("change"); 
        },
        error: function(){
            alert('Error, No se puede autenticar por error');
        }
    });
}

listcboreglab = function(){
    
    $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"lab/mante/cmantensayo/getcboreglab",
        dataType: "JSON",
        async: true,
        success:function(result){
            $('#cboreglab').html(result);
        },
        error: function(){
            alert('Error, No se puede autenticar por error');
        }
    });
}

listarlaboratorio = function(a_censayo){
    oTableListLaboratorio = $('#tblListLaboratorio').DataTable({
        'bJQueryUI'     : true, 
        'scrollY'     	: '200px',
        'scrollX'     	: true, 
        'paging'      	: false,
        'processing'  	: true,      
        'bDestroy'    	: true,
        'info'        	: false,
        'filter'      	: false, 
        'stateSave'     : true,
        "ordering"		: false, 
      'ajax'            : {
        "url"   : baseurl+"lab/mante/cmantensayo/getlistlaboratorio",
        "type"  : "POST", 
        "data": function ( d ) {
            d.censayo = a_censayo;   
        },     
        dataSrc : ''        
      },
      'columns'     : [
          {data: 'dlaboratorio'},
          {data: 'icosto'},
          {"orderable": false, 
            render:function(data, type, row){
              return  '<div>'+  
                        '<a id="aDelDetPropu" href="'+row.claboratorio+'" title="Eliminar" style="cursor:pointer; color:#FF0000;"><span class="fas fa-trash-alt" aria-hidden="true"> </span></a>'+
                      '</div>'   
            }
          },
          {data: 'censayo'},
      ], 
    });
    $('#tblListLaboratorio').on('draw.dt', function(){
        $('#tblListLaboratorio').Tabledit({
            url:'lab/mante/cmantensayo/seteditcostolab',
            eventType: 'dblclick',
            editButton: false,
            deleteButton: false,
            saveButton: false,
            autoFocus: false,
            hideIdentifier: true,
            restoreButton: false,
            columns:{
                identifier : [3,'censayo'],
                editable:[[1,'icosto']]
            },
            restoreButton:false,
            onSuccess: function(data, textStatus, jqXHR) {
                oTableListLaboratorio.ajax.reload(null,false); 
            },
        });
    });  
    oTableListLaboratorio.column(2).visible( true ); 
    oTableListLaboratorio.column(3).visible( false );       
};

$("#btnnewlab").click(function (){
    $('#frmReglab').trigger("reset");

    $("#modalReglab").modal('show');

    $('#mhdnAccionlaboratorio').val('N');
});

$('#modalReglab').on('show.bs.modal', function (e) {
    $('#frmReglab').validate({        
        rules: {
            txtdescripcion: {
              required: true,
            },
        },
        messages: {
            txtdescripcion: {
              required: "Por Favor ingrese Nombre del laboratorio"
            },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        },        
        submitHandler: function (form) {
            const botonEvaluar = $('#mbtnGMantlaboratorio');
            var request = $.ajax({
                url:$('#frmReglab').attr("action"),
                type:$('#frmReglab').attr("method"),
                data:$('#frmReglab').serialize(),
                error: function(){
                    Vtitle = 'Error en Guardar!!!';
                    Vtype = 'error';
                    sweetalert(Vtitle,Vtype); 
                    objPrincipal.liberarBoton(botonEvaluar);
                },
                beforeSend: function() {
                    objPrincipal.botonCargando(botonEvaluar);
                }
            });
            request.done(function( respuesta ) {
                var posts = JSON.parse(respuesta);
                
                $.each(posts, function() {
                    Vtitle = 'Se Grabo Correctamente!!!';
                    Vtype = 'success';
                    sweetalert(Vtitle,Vtype); 
                      
                    var vidlaboratorio = this.id
                    listcboreglab();

                    objPrincipal.liberarBoton(botonEvaluar);    
                    $('#mbtnCMantlaboratorio').click();    
                });
            });
            return false;
        }
    });
});

$('#frmMantlab').submit(function(event){
    event.preventDefault();
    
    var request = $.ajax({
        url:$('#frmMantlab').attr("action"),
        type:$('#frmMantlab').attr("method"),
        data:$('#frmMantlab').serialize(),
        error: function(){
          alert('Error, No se puede autenticar por error');
        }
    });
    request.done(function( respuesta ) {
        var posts = JSON.parse(respuesta);        
        $.each(posts, function() {  
        
            Vtitle = 'Datos Guardados correctamente';
            Vtype = 'success';
            sweetalert(Vtitle,Vtype);

            vcensayo = this.censayo;

            $('#btncancelLab').click();
            $('#frmMantensayo').trigger("reset");
            listarlaboratorio(vcensayo);
        });
    });
});

listmartriz = function(a_censayo){
    oTableMatriz = $('#tblListMatriz').DataTable({
        'bJQueryUI'     : true, 
        'scrollY'     	: '200px',
        'scrollX'     	: true, 
        'paging'      	: false,
        'processing'  	: true,      
        'bDestroy'    	: true,
        'info'        	: false,
        'filter'      	: false, 
        'stateSave'     : true,
        "ordering"		: false, 
      'ajax'            : {
        "url"   : baseurl+"lab/mante/cmantensayo/getlistmatriz",
        "type"  : "POST", 
        "data": function ( d ) {
            d.censayo = a_censayo;   
        },     
        dataSrc : ''        
      },
      'columns'     : [
          {data: 'ddetalle'},
          {"orderable": false, 
            render:function(data, type, row){
              return  '<div>'+  
                        '<a id="aDelDetPropu" href="'+row.cdensayo+'" title="Eliminar" style="cursor:pointer; color:#FF0000;"><span class="fas fa-trash-alt" aria-hidden="true"> </span></a>'+
                      '</div>'   
            }
          },
          {data: 'cdensayo'},
      ], 
    }); 
    $('#tblListMatriz').on('draw.dt', function(){
        $('#tblListMatriz').Tabledit({
            url:'lab/mante/cmantensayo/seteditdetmatriz',
            eventType: 'dblclick',
            editButton: false,
            deleteButton: false,
            saveButton: false,
            autoFocus: false,
            hideIdentifier: true,
            restoreButton: false,
            columns:{
                identifier : [2,'cdensayo'],
                editable:[[0,'ddetalle']]
            },
            restoreButton:false,
            onSuccess: function(data, textStatus, jqXHR) {
                oTableMatriz.ajax.reload(null,false); 
            },
        });
    }); 
    oTableMatriz.column(0).visible( true );  
    oTableMatriz.column(2).visible( false );      
};

limpiarfrmMantensayo = function(){    
  $('#frmMantensayo').trigger("reset");
  
  $('#cboSTipoensayo').val('').trigger("change"); 
  $('#cboZTipoensayo').val('').trigger("change");
}

$('#frmMantmatriz').submit(function(event){
    event.preventDefault();
    
    var request = $.ajax({
        url:$('#frmMantmatriz').attr("action"),
        type:$('#frmMantmatriz').attr("method"),
        data:$('#frmMantmatriz').serialize(),
        error: function(){
          alert('Error, No se puede autenticar por error');
        }
    });
    request.done(function( respuesta ) {
        var posts = JSON.parse(respuesta);        
        $.each(posts, function() {  
        
            Vtitle = 'Datos Guardados correctamente';
            Vtype = 'success';
            sweetalert(Vtitle,Vtype);

            vcensayo = this.censayo;

            $('#btncancelMatriz').click();
            $('#frmMantmatriz').trigger("reset");
            listmartriz(vcensayo);
        });
    });
});

$('#btnaddLab').click(function(){
    $('#hdnlabccensayo').val($('#hdnIdcensayo').val());
    
    $('#btnaddLab').hide();
    $('#btncancelLab').show();
    $('#btnsaveLab').show();
    $('#divregLaboratorio').show();    
});

$('#btnaddMatriz').click(function(){
    $('#hdnmatrizccensayo').val($('#hdnIdcensayo').val());

    $('#btnaddMatriz').hide();
    $('#btncancelMatriz').show();
    $('#btnsaveMatriz').show();
    $('#divregMatriz').show();
});

$('#btncancelLab').click(function(){
    $('#btnaddLab').show();
    $('#btncancelLab').hide();
    $('#btnsaveLab').hide();
    $('#divregLaboratorio').hide();
});

$('#btncancelMatriz').click(function(){
    $('#btnaddMatriz').show();
    $('#btncancelMatriz').hide();
    $('#btnsaveMatriz').hide();
    $('#divregMatriz').hide();
});

$('#btnRetornarList').click(function(){
    $('#tablabensayo a[href="#tablabensayo-list"]').tab('show');  
    $('#btnBuscar').click();
    $('#divDetalle').hide();
    $('#divregLaboratorio').hide();
    $('#divregMatriz').hide();

    $('#btnaddLab').show();
    $('#btncancelLab').hide();
    $('#btnsaveLab').hide();

    $('#btnaddMatriz').show();
    $('#btncancelMatriz').hide();
    $('#btnsaveMatriz').hide();
});