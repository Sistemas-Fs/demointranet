const objListaresult = {};
const objListaresultold = {};
var otblListServiciolab, otblListServiciolabdet, otblListResultados, otblListResultadosold, otblElementos, otblElementosold, otblAddnormas, otblListnormas, otblObsmuestra, otblLCmuestra;
var varfdesde = '%', varfhasta = '%';
var collapsedGroupsEq = {}, collapsedGroupsEq1 = {}, collapsedGroupsEq2 = {};


const objFiltro = {
	result: [],
	valueLoading: false,
};

$(function () {
	
	objFiltro.searchResult = function(valini) {
        const value = $(this).val().trim();           
                    		
        var v_cinternoordenservicio = $('#mhdecinternoordenservicio').val();
        var v_cinternocotizacion = $('#mhdecinternocotizacion').val();
        var v_nordenproducto = $('#mhdenordenproducto').val();
        var v_cmuestra = $('#mhdecmuestra').val();
        var v_censayo = $('#mhdecensayo').val();
        var v_nviausado = $('#mhdenviausado').val();
        var v_zctipoensayo = $('#mhdezctipoensayo').val();

        var v_concc = $('#hdconcc').val();
        if (v_concc == 'S'){
            otblElementos.search( value ).draw();
            listarElementos(v_cinternoordenservicio,v_cinternocotizacion,v_nordenproducto,v_cmuestra,v_censayo,v_nviausado,v_zctipoensayo);
        }else{
            otblElementosold.search( value ).draw();
            listarElementos_old(v_cinternoordenservicio,v_cinternocotizacion,v_nordenproducto,v_cmuestra,v_censayo,v_nviausado,v_zctipoensayo);
        }
        
	};

});

$(document).ready(function() {
    $(document).on('keyup', '.fs--paginate-search', objFiltro.searchResult);

    $('#tablab a[href="#tablab-list-tab"]').attr('class', 'disabled');
    $('#tablab a[href="#tablab-reg-tab"]').attr('class', 'disabled active');

    $('#tablab a[href="#tablab-list-tab"]').not('#store-tab.disabled').click(function(event){
        $('#tablab a[href="#tablab-list"]').attr('class', 'active');
        $('#tablab a[href="#tablab-reg"]').attr('class', '');
        return true;
    });
    $('#tablab a[href="#tablab-reg-tab"]').not('#bank-tab.disabled').click(function(event){
        $('#tablab a[href="#tablab-reg"]').attr('class' ,'active');
        $('#tablab a[href="#tablab-list"]').attr('class', '');
        return true;
    });
    
    $('#tablab a[href="#tablab-list"]').click(function(event){return false;});
    $('#tablab a[href="#tablab-reg"]').click(function(event){return false;});

    $('#txtFDesde,#txtFHasta,#mtxtFRecep,#mtxtFanalisis').datetimepicker({
        format: 'DD/MM/YYYY',
        locale:'es'
    });    

    $('#mtxtHanalisis').datetimepicker({
        format: 'LT'
    });

    fechaAnioActual();
    $('#chkFreg').prop("checked", true);
    
    $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"lab/coti/ccotizacion/getcboclieserv",
        dataType: "JSON",
        async: true,
        success:function(result){
            $('#cboclieserv').html(result);
        },
        error: function(){
            alert('Error, No se puede autenticar por error');
        }
    });

    
    $('#frmIngresult').validate({
        rules: {
            mcboum: {
            required: true,
          },
        },
        messages: {
            mcboum: {
            required: "Por Favor escoja una UM"
          },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        },        
        submitHandler: function (form) {
            const botonEvaluar = $('#mbtnGIngresult');
            var request = $.ajax({
                url:$('#frmIngresult').attr("action"),
                type:$('#frmIngresult').attr("method"),
                data:$('#frmIngresult').serialize(),
                error: function(){
                    Vtitle = 'Error en Guardar!!!';
                    Vtype = 'error';
                    sweetalert(Vtitle,Vtype); 
                },
                beforeSend: function() {
                    objPrincipal.botonCargando(botonEvaluar);
                }
            });
            request.done(function( respuesta ) {
                var posts = JSON.parse(respuesta);
                
                $.each(posts, function() {                    
                    
                    Vtitle = 'Cotizacion Guardada!!!';
                    Vtype = 'success';
                    sweetalert(Vtitle,Vtype);   
                    objPrincipal.liberarBoton(botonEvaluar); 
                    
                    getListResultados(this.cinternoordenservicio);

                    $('#mbtnCIngresult').click();
                });
            });
            return false;
        }
    });
    
    $('#frmsetservicio').validate({
        rules: {
            mtxtFanali: {
            required: true,
          },
          mtxtHanali:{
            required: true,
          },
        },
        messages: {
            mtxtFanali: {
            required: "Por Favor ingrese una fecha de Analisis"
          },
            mtxtHanali: {
            required: "Por Favor ingrese una Hora de Analisis"
          },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        },        
        submitHandler: function (form) {
            const botonEvaluar = $('#btngrabarResult');
            var request = $.ajax({
                url:$('#frmsetservicio').attr("action"),
                type:$('#frmsetservicio').attr("method"),
                data:$('#frmsetservicio').serialize(),
                error: function(){
                    Vtitle = 'Error en Guardar!!!';
                    Vtype = 'error';
                    sweetalert(Vtitle,Vtype); 
                },
                beforeSend: function() {
                    objPrincipal.botonCargando(botonEvaluar);
                }
            });
            request.done(function( respuesta ) {
                var posts = JSON.parse(respuesta);
                if(respuesta == 'true') {   
                    Vtitle = 'Servicio Guardado!!!';
                    Vtype = 'success';
                    sweetalert(Vtitle,Vtype);   
                    objPrincipal.liberarBoton(botonEvaluar); 
                    //getListResultados(this.cinternoordenservicio);
                    //$('#mbtnCIngresult').click();
                };
            });
            return false;
        }
    });

    $('#frmRegEsterilidad').validate({
        rules: {
        },
        messages: {
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        },        
        submitHandler: function (form) {
            const botonEvaluar = $('#mbtngabareste');
            var request = $.ajax({
                url:$('#frmRegEsterilidad').attr("action"),
                type:$('#frmRegEsterilidad').attr("method"),
                data:$('#frmRegEsterilidad').serialize(),
                error: function(){
                    Vtitle = 'Error en Guardar!!!';
                    Vtype = 'error';
                    sweetalert(Vtitle,Vtype); 
                },
                beforeSend: function() {
                    objPrincipal.botonCargando(botonEvaluar);
                }
            });
            request.done(function( respuesta ) {
                var posts = JSON.parse(respuesta);
                
                $.each(posts, function() {                      
                    $('#btnupdate').click();  
                    Vtitle = 'Registro Guardado!!!';
                    Vtype = 'success';
                    sweetalert(Vtitle,Vtype);
                    objPrincipal.liberarBoton(botonEvaluar);
                    var v_tipoacidez = $('#mcbotipoacidez').val(); 
                    $('#mcbotipoacidez').val(v_tipoacidez).trigger("change");
                });    
            });
            return false;
        }
    });
});

fechaActual = function(){
    var fecha = new Date();		
    var fechatring = ("0" + fecha.getDate()).slice(-2) + "/" + ("0"+(fecha.getMonth()+1)).slice(-2) + "/" +fecha.getFullYear() ;

    $('#txtFDesde').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY') );
    $('#txtFHasta').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY') );
    $('#mtxtFanalisis').datetimepicker('date', moment(fechatring, 'DD/MM/YYYY') );

};

fechaAnioActual = function(){
    $("#txtFIni").prop("disabled",false);
    $("#txtFFin").prop("disabled",false);
        
    varfdesde = '';
    varfhasta = '';
    
    var fecha = new Date();		
    var fechatring1 = "01/01/" + (fecha.getFullYear()) ;
    var fechatring2 = ("0" + fecha.getDate()).slice(-2) + "/" + ("0"+(fecha.getMonth()+1)).slice(-2) + "/" +fecha.getFullYear() ;

    $('#txtFDesde').datetimepicker('date', moment(fechatring1, 'DD/MM/YYYY') );
    $('#txtFHasta').datetimepicker('date', moment(fechatring2, 'DD/MM/YYYY') );
};
	
$('#txtFDesde').on('change.datetimepicker',function(e){	
    
    $('#txtFHasta').datetimepicker({
        format: 'DD/MM/YYYY',
        daysOfWeekDisabled: [0],
        locale:'es'
    });	

    var fecha = moment(e.date).format('DD/MM/YYYY');		
    
    $('#txtFHasta').datetimepicker('minDate', fecha);
    $('#txtFHasta').datetimepicker('date', fecha);

});

$("#chkFreg").on("change", function () {
    if($("#chkFreg").is(":checked") == true){ 
        $("#txtFIni").prop("disabled",false);
        $("#txtFFin").prop("disabled",false);
        
        varfdesde = '';
        varfhasta = '';

        
        var fecha = new Date();		
        var fechatring1 = "01/01/" + (fecha.getFullYear()) ;
        var fechatring2 = ("0" + fecha.getDate()).slice(-2) + "/" + ("0"+(fecha.getMonth()+1)).slice(-2) + "/" +fecha.getFullYear() ;
        $('#txtFDesde').datetimepicker('date', fechatring1);
        $('#txtFHasta').datetimepicker('date', fechatring2);

    }else if($("#chkFreg").is(":checked") == false){ 
        $("#txtFIni").prop("disabled",true);
        $("#txtFFin").prop("disabled",true);
        
        varfdesde = '%';
        varfhasta = '%';

        fechaActual();
    }; 
});

$("#btnBuscar").click(function (){
    parametros = paramListarBusqueda();
    getListarBusqueda(parametros);
});

paramListarBusqueda = function (){    
    var v_mes, v_anio

    if(varfdesde != '%'){ varfdesde = $('#txtFIni').val(); }
    if(varfhasta != '%'){ varfhasta = $('#txtFFin').val(); } 
     
    var parametros = {
        "ccliente"      : $('#cboclieserv').val(),
        "buspor"        : $('#cbobuspor').val(), 
        "numero"        : $('#txtbuscarnro').val(), 
        "fini"          : varfdesde, 
        "ffin"          : varfhasta,   
        "prodmuestra"   : $('#txtdescri').val(), 
        "ensayo"        : $('#txtensayo').val(), 
        //"areaserv"      : $('#cboareaserv').val(),
    };  

    return parametros;
    
};

getListarBusqueda = function(){    

    otblListServiciolab = $('#tblListServiciolab').DataTable({ 
        "processing"  	: true,
        "bDestroy"    	: true,
        "stateSave"     : true,
        "bJQueryUI"     : true,
        "scrollResize"  : true,
        "scrollY"     	: "540px",
        "scrollX"     	: true, 
        'AutoWidth'     : true,
        "paging"      	: false,
        "info"        	: true,
        "filter"      	: true, 
        "ordering"		: false,
        "responsive"    : false,
        "select"        : true,
        "ajax"	: {
            "url"   : baseurl+"lab/resultados/cregresult/getbuscaringresoresult",
            "type"  : "POST", 
            "data"  : parametros,     
            dataSrc : ''      
        }, 
        'columns'	: [
            {data: 'DCLIENTE'},
            {data: 'BLANCO', "className":"col-xxxs"},
            {data: 'NROCOTI'},
            {data: 'BLANCO', defaultContent: '', "className":"details-control col-xxxs"},
            {data: 'BLANCO', "className":"col-xxxs"},
            {data: 'CODMUESTRA'},
            {data: 'REALPRODUCTO'},
        ],
        rowGroup: {
            startRender : function ( rows, group, level ) {
                
                if ( level == 0 ) {
                    var varcclie = rows
                        .data()
                        .reduce( function (a, b) {
                            return b.ccliente;
                        }, 0) ; 

                    var collapsed = !!collapsedGroupsEq[varcclie];
        
                    rows.nodes().each(function (r) {
                        r.style.display = collapsed ? 'none' : '';
                    });   
              
                    
                    return $('<tr/>')
                    .append('<td colspan="6" style="background-color:#D5D8DC;">' + group + '</td>')
                    .attr('data-name', varcclie)
                    .toggleClass('collapsed', collapsed);

                } else if ( level == 1 ){
                    var varid = rows
                        .data()
                        .reduce( function (a, b) {
                            return b.cinternoordenservicio;
                        }, 0) ;

                    var collapsedid = !!collapsedGroupsEq1[varid];        
                    rows.nodes().each(function (r) {
                        r.style.display = collapsedid ? '' : 'none';
                    }); 


                    return $('<tr/>')
                    .append('<td id="collapsed" class="details-control col-xxxs"></td><td colspan="3" style="cursor: pointer; background-color:#EEEBEB;">' + group + '</td><td class="col-s"></td>')
                    .attr('data-name', varid)
                    .toggleClass('collapsed', collapsedid)
                    .toggleClass('details', collapsedid);
                    
                }                 
            },
            dataSrc: ["DCLIENTE","NROCOTI"]
        },
    }); 
    otblListServiciolab.column(0).visible( false ); 
    otblListServiciolab.column(1).visible( true ); 
    otblListServiciolab.column(2).visible( false ); 
    otblListServiciolab.column(3).visible( true ); 
    otblListServiciolab.column(4).visible( false );
    otblListServiciolab.column(5).visible( false );   
    otblListServiciolab.column(6).visible( true );  

    $("#btnexcel").prop("disabled",false); 
};

/* DETALLE RECEPCION */
$('#tblListServiciolab tbody').on( 'click', 'td.details-control', function () {
    var tr = $(this).parents('tr');
    var row = otblListServiciolab.row(tr);
    var rowData = row.data();
    
        if ( row.child.isShown() ) {                    
            row.child.hide();
            tr.removeClass( 'details' );
        }
        else {
            otblListServiciolab.rows().every(function(){
                // If row has details expanded
                if(this.child.isShown()){
                    // Collapse row details
                    this.child.hide();
                    $(this.node()).removeClass('details');
                }
            })
            row.child( 
            '<table id="tblListServiciolabdet" class="display compact" style="width:100%; background-color:#D3DADF; border-bottom: 2px solid black;">'+
            '<thead style="background-color:#FFFFFF;"><tr><th></th><th></th><th></th><th>TIPO ENSAYO</th><th>CODIGO ENSAYO</th><th>ENSAYO</th></tr></thead><tbody>' +
                '</tbody></table>').show();
                otblListServiciolabdet = $('#tblListServiciolabdet').DataTable({
                    "processing"  	: true,
                    'stateSave'     : true,
                    'bDestroy'      : true,
                    "bJQueryUI"     : true,
                    'scrollY'       : false,
                    'scrollX'       : true,
                    'scrollCollapse': false,
                    'AutoWidth'     : true,
                    'paging'        : false,
                    'info'          : false,
                    'filter'        : false,
                    "select"        : false, 
                    'ajax'        : {
                        "url"   : baseurl+"lab/resultados/cregresult/getbuscaringresoresultdet",
                        "type"  : "POST", 
                        "data": function ( d ) {
                            d.cinternoordenservicio = rowData.cinternoordenservicio;
                            d.cmuestra = rowData.cmuestra;
                        },     
                        dataSrc : ''        
                    },
                    'columns'     : [
                        {orderable: false,data: "BLANCO", "class" : "col-xxxs"},
                        {orderable: false,data: "BLANCO", "class" : "col-xxxs"},
                        {orderable: false,data: "BLANCO", "class" : "col-xxxs"},
                        {orderable: false,data: "TIPOENSAYO"},
                        {orderable: false,data: "CODENSAYO"},
                        {orderable: false,data: "ENSAYO"},     
                    ], 
                });
            tr.addClass('details');
        }
});

$('#tblListServiciolab tbody').on('click', 'tr.dtrg-level-1 td', function () { 

    if($(this).attr('id') == 'collapsed'){
        otblListServiciolab.rows().every(function(){
            if(this.child.isShown()){
                this.child.hide();
                $(this.node()).removeClass('details');
            }
        });

        var tr = $(this).parents('tr');        
        var name = tr.data('name');
        collapsedGroupsEq1[name] = !collapsedGroupsEq1[name];        
        otblListServiciolab.draw(true);
    }
    
    
}); 


$('#tblListServiciolab tbody').on('dblclick', 'tr.dtrg-level-1', function () {
    var id = $(this).data('name');
    $('#tablab a[href="#tablab-reg"]').tab('show');
    verResultados(id);
} );


$('#btnupdate').click(function(){    
    v_idot = $('#txtidordenservicio').val();
    
    verResultados(v_idot)
});

verResultados = function(id){
    var parametros = { 
        "cinternoordenservicio":id 
    };
    var request = $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"lab/resultados/cregresult/getrecuperaservicio",
        dataType: "JSON",
        async: true,
        data: parametros,
        error: function(){
            alert('Error, no se puede cargar el servicio');
        }
    });      
    request.done(function( respuesta ) {            
        $.each(respuesta, function() { 

            $('#txtcliente').val(this.drazonsocial);
            $('#mtxtFanali').val(this.fanalisis);
            $('#mtxtHanali').val(this.hanalisis);
            $('#txtcotizacion').val(this.dcotizacion);
            $('#txtfcotizacion').val(this.fcotizacion);  
            $('#txtnroot').val(this.nordenservicio);  
            $('#txtfot').val(this.fordenservicio);  
            $('#txtidcotizacion').val(this.cinternocotizacion);  
            $('#txtnroversion').val(this.nversioncotizacion);  
            $('#txtidordenservicio').val(this.cinternoordenservicio); 
            $('#txtobserva').val(this.dobservacionresultados); 
            //$('#mcbotipoinforme').val(this.zctipoinforme).trigger("change"); 
            $('#txtzctipocerticalidad').val(this.zctipocerticalidad);            
            $('#txttipoingreso').val(this.conttipoprod);           
            $('#hdconcc').val(this.concc);
             
                                    
            if (this.concc == 'S'){                
                $('#divtblListResultados').show();           
                $('#divtblListResultadosold').hide();
                $('#btnaddNormas').show();
                $('#aprevcerti').show(); 
                objListaresult.viewList(id,'%','%','%')
            }else{           
                $('#divtblListResultados').hide();           
                $('#divtblListResultadosold').show();
                $('#btnaddNormas').hide(); 
                $('#aprevcerti').hide(); 
                objListaresultold.viewListold(id,'%','%','%')
            }

            cbotipoensayo(this.cinternoordenservicio);
                  
        });
    });
};

cbotipoensayo = function(vcinternoordenservicio){ 
    var params = { "cinternoordenservicio": vcinternoordenservicio};   
    $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"lab/resultados/cregresult/getcbotipoensayo",
        dataType: "JSON",
        async: true,
        data: params,
        success:function(result){
            $('#mcbotipoensayo').html(result);
        },
        error: function(){
            alert('Error, No se puede autenticar por error');
        }
    });
};

$("#cboareaserv").change(function(){
    var v_idot = $('#txtidordenservicio').val();
    var v_cboareaserv = $('#cboareaserv').val();
    var v_tipoensayo = $('#mcbotipoensayo').val();
    var v_acreditado = $('#mcboacreditado').val();
    var v_conttipoprod = $('#hdconcc').val();

    if (v_conttipoprod == 'S'){
        objListaresult.viewList(v_idot,v_tipoensayo,v_acreditado,v_cboareaserv)
    }else{           
        objListaresultold.viewListold(v_idot,v_tipoensayo,v_acreditado,v_cboareaserv)
    }    
});
$("#mcbotipoensayo").change(function(){
    var v_idot = $('#txtidordenservicio').val();
    var v_cboareaserv = $('#cboareaserv').val();
    var v_tipoensayo = $('#mcbotipoensayo').val();
    var v_acreditado = $('#mcboacreditado').val();
    var v_conttipoprod = $('#hdconcc').val();

    if (v_conttipoprod == 'S'){
        objListaresult.viewList(v_idot,v_tipoensayo,v_acreditado,v_cboareaserv)
    }else{           
        objListaresultold.viewListold(v_idot,v_tipoensayo,v_acreditado,v_cboareaserv)
    }    
});
$("#mcboacreditado").change(function(){
    var v_idot = $('#txtidordenservicio').val();
    var v_cboareaserv = $('#cboareaserv').val();
    var v_tipoensayo = $('#mcbotipoensayo').val();
    var v_acreditado = $('#mcboacreditado').val();

    var v_conttipoprod = $('#hdconcc').val();

    if (v_conttipoprod == 'S'){
        objListaresult.viewList(v_idot,v_tipoensayo,v_acreditado,v_cboareaserv)
    }else{           
        objListaresultold.viewListold(v_idot,v_tipoensayo,v_acreditado,v_cboareaserv)
    }    
});

/* ******** */
/**
* Listar de servicio
*/

objListaresult.viewList = function (id, tipoensayo, acreditado, areaserv) {   
    var parametros = {
        "cinternoordenservicio" : id,
        "zctipoensayo" : tipoensayo,
        "sacnoac" : acreditado,
        "areaserv" : areaserv,
    };

    otblListResultados = $('#tblListResultados').DataTable({ 
        "processing"  	: true,
        "bDestroy"    	: true,
        "stateSave"     : true,
        "bJQueryUI"     : true,
        "scrollResize"  : true,
        "scrollY"     	: "540px",
        "scrollX"     	: true, 
        'AutoWidth'     : false,
        "paging"      	: false,
        "info"        	: false,
        "filter"      	: false, 
        "ordering"		: false,
        "responsive"    : false,
        "select"        : true,
        "ajax"	: {
            "url"   : baseurl+"lab/resultados/cregresult/getlistresultados",
            "type"  : "POST", 
            "data"  : parametros,     
            dataSrc : ''      
        }, 
        'columns'	: [
            {data: 'BLANCO', "className":"col-xxxs"},
            {data: 'codmuestra'},
            {"orderable": false, 
                render:function(data, type, row){
                    if(row.zctipoensayo == "068" || row.zctipoensayo == "069" || row.zctipoensayo == "A61"){ // normal                         
                        return '<div>'+row.codensayo+'</div>';
                    } else if (row.zctipoensayo == "070"){ // sensorial
                        return '<div>'+row.codensayo+'&nbsp;&nbsp;&nbsp;<a title="Ingresar" style="cursor:pointer; color:navy;" onClick="regSensorial('+row.cinternoordenservicio+',\''+row.cinternocotizacion+'\','+row.nordenproducto+',\''+row.cmuestra+'\',\''+row.censayo+'\','+row.nviausado+',\''+row.concc+'\');"><span class="fas fa-folder-plus" aria-hidden="true"></span></a></div>'
                    } else if(row.zctipoensayo == "736"){ // esterilidad
                        return '<div>'+row.codensayo+'&nbsp;&nbsp;&nbsp;<a title="Ingresar" style="cursor:pointer; color:navy;" onClick="regEsterilidad('+row.cinternoordenservicio+',\''+row.cinternocotizacion+'\','+row.nordenproducto+',\''+row.cmuestra+'\',\''+row.censayo+'\','+row.nviausado+','+row.idaccion+');"><span class="fas fa-folder-plus" aria-hidden="true"></span></a></div>'
                    } else { // elementos
                        return '<div>'+row.codensayo+'&nbsp;&nbsp;&nbsp;<a title="Ingresar" style="cursor:pointer; color:navy;" onClick="regElementos('+row.cinternoordenservicio+',\''+row.cinternocotizacion+'\','+row.nordenproducto+',\''+row.cmuestra+'\',\''+row.censayo+'\','+row.nviausado+',\''+row.zctipoensayo+'\',\''+row.concc+'\');"><span class="fas fa-folder-plus" aria-hidden="true"></span></a></div>'
                    }
                }
            },
            {data: 'densayo'},
            {data: 'unidadmedida'},
            {data: 'condi_espe'},
            {data: 'valor_espe', render: $.fn.dataTable.render.number( ',', '.', 3) },
            {data: 'valexpo_espe', render: $.fn.dataTable.render.number( ',', '.', 0) },
            {data: 'condi_resul'},
            {data: 'valor_resul', render: $.fn.dataTable.render.number( ',', '.', 3) },
            {data: 'valexpo_resul', render: $.fn.dataTable.render.number( ',', '.', 0) },
            {data: 'dobservacion'},
            {data: 'sresultado'},
            {data: 'dselect'},
            {data: 'dvistobueno'},
            {data: 'ID', "className":"ocultar col-xxxs"},
        ],
        rowGroup: {
            startRender : function ( rows, group, level ) {                
                if ( level == 0 ) {
                    var varcodmuestra = rows
                        .data()
                        .reduce( function (a, b) {
                            return b.codmuestra;
                        }, 0) ; 

                    var collapsed = !!collapsedGroupsEq[varcodmuestra];
        
                    rows.nodes().each(function (r) {
                        r.style.display = collapsed ? 'none' : '';
                    });                 
                    
                    return $('<tr/>')
                    .append('<td colspan="13" style="background-color:#D5D8DC;">' + group + '</td>')
                    .attr('data-name', varcodmuestra)
                    .toggleClass('collapsed', collapsed);

                } else if ( level == 1 ){
                    var vartipoensayo = rows
                        .data()
                        .reduce( function (a, b) {
                            return b.tipoensayo;
                        }, 0) ;

                    var collapsedid = !!collapsedGroupsEq1[vartipoensayo];        
                    rows.nodes().each(function (r) {
                        r.style.display = collapsedid ? 'none' : '';
                    }); 

                    return $('<tr/>')
                    .append('<td colspan="13" style="background-color:#EEEBEB;">' + group + '</td>')
                    .attr('data-name', vartipoensayo)
                    .toggleClass('collapsed', collapsed);
                    
                } 
            },
            dataSrc: ["codmuestra","tipoensayo"],
        }, 
        "rowCallback":function(row,data){           
            if(data.sselect == "1"){
                $(row).css("background-color","#A7D1AE");
            }else{
                $(row).css("background-color","#FFFFFF"); 
            }
        },
    });
    // Enumeracion 
    otblListResultados.on( 'order.dt search.dt', function () { 
        otblListResultados.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i+1;
          } );
    }).draw();  

    otblListResultados.column(0).visible(true); 
    otblListResultados.column(1).visible(false); 
};
$('#tblListResultados').on('draw.dt', function(){
    $('#tblListResultados').Tabledit({
        url:'lab/resultados/cregresult/setresultados',
        dataType:'json',
        eventType: 'click',
        editButton: false,
        deleteButton: false,
        saveButton: false,
        autoFocus: false,
        hideIdentifier: true,
		restoreButton: false,
        buttons: {
            edit: {
                class: 'btn btn-sm btn-primary',
                html: '<span class="fas fa-pencil-alt"></span>',
                action: 'edit'
            }
        },
        columns:{
            identifier : [14,'ID'],
            editable:[[3, 'unidadmedida', '{"565":"-","561":"%","784":"% (Ac. acético)","776":"% (Ac. cítrico)","611":"% (Ac. Láctico)","A43":"% (Ác. málico)","612":"% (Ac. Sulfúrico)","781":"% (Al 15% Humedad)","B61":"% (Amoniaco)","B29":"% del extracto seco","E76":"% Vol.","743":"%(Ac. Oleico)","849":"%(Ac. Palmitico)","941":"%p/p","999":"%V","850":"(20ºC)","872":"(25º)","A44":"(g/100g) Base Seca","782":"(mg KOH)/g)","943":"\ Hg","938":"°D","940":"°Z","991":"µg retinol/100g","873":"µs/cm","880":"Ausencia / 100 ml","563":"Ausencia / Presencia","906":"Ausencia en 25g / Presencia en 25g","976":"Ausencia o Presencia / 25 g","977":"Ausencia o Presencia/100g","A57":"Ausencia o Presencia/1L ","A42":"B. cereus / superficie","989":"CIO2 - mg/L","988":"CIO3 - mg/L","773":"cm","982":"Coliform bacilli en 25 g","945":"Coliformes / 1 g","912":"cp","944":"CP (25ºC)","A82":"cps (20ºC)","A54":"cps (25ºC)","629":"cps (50ºC)","A55":"cps (65ºC)","A56":"cps (97ºC)","631":"cTs (100ºC)","J12":"dS/m","851":"E. coli / 1 g","968":"E. coli / 1 g","918":"E. coli / 100cm2","I67":"E. coli / 25 g","626":"E. coli / área","627":"E. coli / cm2","760":"E. coli / manos","837":"E. coli / superficie","613":"E. coli O 157:H7 / 25 g","A48":"E. coli O 157:H7 / 25 ml","E74":"E. coli O 157:H7/325 g","981":"Enterobacterias / 1 g","610":"Estéril / No Estéril","737":"g","961":"g / L","957":"g/100g","845":"g/100g (Límite de Cuantificación: 0.08)","974":"g/100ml","955":"g/115g","956":"g/130g","I91":"g/200mL","946":"g/Kg","B28":"g/L (Ác. acético)","A60":"g/m2","771":"g/ml","783":"g/ml (20ºC)","630":"g/ml (53ºC)","628":"g/ml (65ºC)","980":"g/paquete","954":"g/porción","993":"gr/dm3","A53":"gr/ml ( 97ºC)","A51":"gr/ml (25ºC)","A52":"gr/ml (65ºC)","775":"Kcal/100g","J55":"Kcal/100ml","I92":"Kcal/200mL","990":"Kg","E75":"Kg/m3","437":"Listeria / 100 Cm2","436":"Listeria / 25 g","I72":"Listeria / 25ml ","994":"Listeria / manos","438":"Listeria / superficie","E73":"Listeria monocytogenes/325 g","I79":"Listeria spp.","992":"Lt.","A58":"LUX","I65":"mcg/100g","I66":"mcgRE/g","744":"meq de Ácido/Kg","741":"meq Peróxido /Kg Aceite","934":"meq/Kg","J13":"meq/L","562":"meqPeroxido / Kg Grasa","939":"mg","960":"mg / 100 ml AA","888":"mg Ac Ascorbico/100g","J10":"mg Ac Ascorbico/100ml","I78":"mg Ácido sórbico","I68":"mg C3H6O3","886":"mg CaCO3/L","947":"mg Imidacloprid/Kg","772":"mg Nitrógeno Amoniacal/100g","887":"mg Nitrógeno Amoniacal/L","J16":"mg/100 gr (Ac. Sulfúrico)","963":"mg/100 mL","867":"mg/100g","I74":"mg/100ml","I88":"mg/200mL","I75":"mg/240ml","J15":"mg/dm2","625":"mg/Kg","618":"mg/L","619":"mgCaO3/L","J57":"miliequivalentes/Kg","972":"min. 10^7","774":"ml","A41":"ml/L","967":"mm","995":"mmHg","435":"N° / 100 Ml","785":"NBV/100g","J14":"ng/g","J11":"NMP / 100 g","054":"NMP / 100 ml","052":"NMP / g","053":"NMP / ml","620":"NTU","623":"NUO","624":"NUS","919":"ºBaumé","738":"ºBe","566":"ºBrix","840":"ºC","966":"ºHaugh","962":"Org / 100 mL","609":"Org/L","996":"ºZ","740":"pH (10ºBrix)","739":"pH Solución 50%","632":"Positivo/Negativo","839":"ppb","564":"ppm","E78":"ppm de Oleato de Sodio","848":"Presencia/Ausencia","969":"Pseudomonas aeruginosa / 1 g","I70":"Pseudomonas aeruginosa/ 1g","987":"Pseudomonas/100ml","I69":"Pulg-Hg","838":"S. aureus / superficie","060":"Salmonella / 100Cm2","A49":"Salmonella / 25 ml","058":"Salmonella / 25g","998":"Salmonella / 4 superficies","868":"Salmonella / 50 g","959":"Salmonella / 750 g","059":"Salmonella / área","B24":"Salmonella / cm2","745":"Salmonella / manos","780":"Salmonella / superficie","A59":"Salmonella sp./ 25g","E72":"Salmonella/325 g","878":"Samonella / 100 ml","846":"Shigella/25 g","970":"Staphylococcus aureus / 1 g","753":"Staphylococcus aureus / 100 cm2","752":"Staphylococcus aureus / área","879":"Staphylococcus aureus / manos","933":"Staphylococcus aureus / superficie ","971":"Streptococcus / 10 g","622":"UCV escala Pt/Co","434":"UFC / 15 Min","997":"UFC / 4 superficies","056":"UFC / área","055":"UFC / Cm2","049":"UFC / g ","983":"UFC / g (Est.)","057":"UFC / manos","050":"UFC / ml","985":"UFC / ml (Est.)","750":"UFC / placa 40 cm2","758":"UFC / placa 60 cm2","779":"UFC / superficie","051":"UFC/ 100 ml","A47":"UFC/ 100g","978":"UFC/ ml 35ºC/48 h R2A","608":"UFC/15","616":"UFP/ml","927":"ug/100g","I76":"ug/100ml","I73":"ug/200ml","I89":"ug/200mL","I77":"ug/240ml","964":"ug/g","770":"ug/Kg","I71":"ug/L","A84":"ug/mL","I64":"ugRE/g","742":"UI","913":"UI/100g","I90":"UI/200mL","621":"umho/cm","J56":"unidades kertesz","986":"V. cholerae/manos","942":"Vibrio cholerae / 100 cm2","617":"Vibrio cholerae / 25 g","909":"Vibrio parahaemolyticus / 25 g","K98":"% Alc. Vol","M50":"msv/año","M49":"Bq/L","M51":"Nº de esporas de bacilos MAEF/g","M52":"Nº de formadores de esporas anaerobias/g","M53":"Nº de esporas/10g"}'],
                      [4,'condi_espe', '{"":"", "=": "=", "<": "<", ">": ">", "<=": "<=", ">=": ">=","Ausencia": "Ausencia", "Presencia": "Presencia","Positivo": "Positivo","Negativo": "Negativo","ND": "ND","Estéril": "Estéril", "No Estéril": "No Estéril", "Aceptable": "Aceptable"}'],[5,'valor_espe'],[6,'valexpo_espe'],[7,'condi_resul', '{"":"", "=": "=", "<": "<", ">": ">", "<=": "<=", ">=": ">=","Ausencia": "Ausencia", "Presencia": "Presencia","Positivo": "Positivo","Negativo": "Negativo","ND": "ND","Estéril": "Estéril", "No Estéril": "No Estéril", "Aceptable": "Aceptable"}'],[8,'valor_resul'],[9,'valexpo_resul'],[10,'dobservacion'],[11,'sresultado', '{"": "", "N": "NO CONFORME", "C": "CONFORME", "NA": "NO APLICA", "AA": "ALTO EN AZUCAR", "AS": "ALTO EN SODIO", "GS": "ALTO EN GRASAS SATURADAS", "GT": "CONTIENE GRASAS TRANS"}']]
        },
        onSuccess: function(data, textStatus, jqXHR) {  
            otblListResultados.ajax.reload(null,false);         
            //var v_idot = $('#txtidordenservicio').val();
            //var v_tipoensayo = $('#mcbotipoensayo').val();
            //var v_acreditado = $('#mcboacreditado').val();
            //objListaresult.viewList(v_idot,v_tipoensayo,v_acreditado) // "≤", "≥"
        },
    });
});

objListaresultold.viewListold = function (id, tipoensayo, acreditado, areaserv) { 
    var parametros = {
        "cinternoordenservicio" : id,
        "zctipoensayo" : tipoensayo,
        "sacnoac" : acreditado,
        "areaserv" : areaserv,
    };

    otblListResultadosold = $('#tblListResultadosold').DataTable({ 
        "processing"  	: true,
        "bDestroy"    	: true,
        "stateSave"     : true,
        "bJQueryUI"     : true,
        "scrollResize"  : true,
        "scrollY"     	: "540px",
        "scrollX"     	: true, 
        'AutoWidth'     : false,
        "paging"      	: false,
        "info"        	: false,
        "filter"      	: false, 
        "ordering"		: false,
        "responsive"    : false,
        "select"        : true,
        "ajax"	: {
            "url"   : baseurl+"lab/resultados/cregresult/getlistresultadosold",
            "type"  : "POST", 
            "data"  : parametros,     
            dataSrc : ''      
        }, 
        'columns'	: [
            {data: 'BLANCO', "className":"col-xxxs"},
            {data: 'codmuestra'},
            {"orderable": false, 
                render:function(data, type, row){
                    /* CAMBIO MOMENTANEO  */
                     if(row.zctipoensayo == "068" || row.zctipoensayo == "069" || row.zctipoensayo == "A61"){ // normal
                        return '<div>'+row.codensayo+'</div>';
                    } else if (row.zctipoensayo == "070"){ // sensorial
                        return '<div>'+row.codensayo+'&nbsp;&nbsp;&nbsp;<a title="Ingresar" style="cursor:pointer; color:navy;" onClick="regSensorial('+row.cinternoordenservicio+',\''+row.cinternocotizacion+'\','+row.nordenproducto+',\''+row.cmuestra+'\',\''+row.censayo+'\','+row.nviausado+',\''+row.concc+'\');"><span class="fas fa-folder-plus" aria-hidden="true"></span></a></div>'
                    } else if(row.zctipoensayo == "736"){ // esterilidad
                        return '<div>'+row.codensayo+'&nbsp;&nbsp;&nbsp;<a title="Ingresar" style="cursor:pointer; color:navy;" onClick="regEsterilidad('+row.cinternoordenservicio+',\''+row.cinternocotizacion+'\','+row.nordenproducto+',\''+row.cmuestra+'\',\''+row.censayo+'\','+row.nviausado+','+row.idaccion+');"><span class="fas fa-folder-plus" aria-hidden="true"></span></a></div>'
                    } else { // elementos
                        return '<div>'+row.codensayo+'&nbsp;&nbsp;&nbsp;<a title="Ingresar" style="cursor:pointer; color:navy;" onClick="regElementos('+row.cinternoordenservicio+',\''+row.cinternocotizacion+'\','+row.nordenproducto+',\''+row.cmuestra+'\',\''+row.censayo+'\','+row.nviausado+',\''+row.zctipoensayo+'\',\''+row.concc+'\');"><span class="fas fa-folder-plus" aria-hidden="true"></span></a></div>'
                    }
                }
            },
            {data: 'densayo'},
            {data: 'unidadmedida'},
            {data: 'dresultado'},
            {data: 'dresultadoexp'},
            {data: 'dselect'},
            {data: 'dvistobueno'},
            {data: 'ID', "className":"ocultar col-xxxs"},
        ],
        rowGroup: {
            startRender : function ( rows, group, level ) {                
                if ( level == 0 ) {
                    var varcodmuestra = rows
                        .data()
                        .reduce( function (a, b) {
                            return b.codmuestra;
                        }, 0) ; 

                    var collapsed = !!collapsedGroupsEq[varcodmuestra];
        
                    rows.nodes().each(function (r) {
                        r.style.display = collapsed ? 'none' : '';
                    });                 
                    
                    return $('<tr/>')
                    .append('<td colspan="9" style="background-color:#D5D8DC;">' + group + '</td>')
                    .attr('data-name', varcodmuestra)
                    .toggleClass('collapsed', collapsed);

                } else if ( level == 1 ){
                    var vartipoensayo = rows
                        .data()
                        .reduce( function (a, b) {
                            return b.tipoensayo;
                        }, 0) ;

                    var collapsedid = !!collapsedGroupsEq1[vartipoensayo];        
                    rows.nodes().each(function (r) {
                        r.style.display = collapsedid ? 'none' : '';
                    }); 

                    return $('<tr/>')
                    .append('<td colspan="9" style="background-color:#EEEBEB;">' + group + '</td>')
                    .attr('data-name', vartipoensayo)
                    .toggleClass('collapsed', collapsed);
                    
                } 
            },
            dataSrc: ["codmuestra","tipoensayo"],
        }, 
        "rowCallback":function(row,data){           
            if(data.sselect == "1"){
                $(row).css("background-color","#A7D1AE");
            }else{
                $(row).css("background-color","#FFFFFF"); 
            }
        },
    });
    // Enumeracion 
    otblListResultadosold.on( 'order.dt search.dt', function () { 
        otblListResultadosold.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i+1;
          } );
    }).draw();  

    otblListResultadosold.column(0).visible(true); 
    otblListResultadosold.column(1).visible(false); 
};

$('#tblListResultadosold').on('draw.dt', function(){
    $('#tblListResultadosold').Tabledit({
        url:'lab/resultados/cregresult/setresultadosold',
        dataType:'json',
        eventType: 'click',
        editButton: false,
        deleteButton: false,
        saveButton: false,
        autoFocus: false,
        hideIdentifier: true,
		restoreButton: false,
        buttons: {
            edit: {
                class: 'btn btn-sm btn-primary',
                html: '<span class="fas fa-pencil-alt"></span>',
                action: 'edit'
            }
        },
        columns:{
            identifier : [8,'ID'],
<<<<<<< HEAD
            editable:[[3, 'unidadmedida', '{"565":"-","561":"%","784":"% (Ac. acético)","776":"% (Ac. cítrico)","611":"% (Ac. Láctico)","A43":"% (Ác. málico)","612":"% (Ac. Sulfúrico)","781":"% (Al 15% Humedad)","B61":"% (Amoniaco)","B29":"% del extracto seco","E76":"% Vol.","743":"%(Ac. Oleico)","849":"%(Ac. Palmitico)","941":"%p/p","999":"%V","850":"(20ºC)","872":"(25º)","A44":"(g/100g) Base Seca","782":"(mg KOH)/g)","943":"\ Hg","938":"°D","940":"°Z","991":"µg retinol/100g","873":"µs/cm","880":"Ausencia / 100 ml","563":"Ausencia / Presencia","906":"Ausencia en 25g / Presencia en 25g","976":"Ausencia o Presencia / 25 g","977":"Ausencia o Presencia/100g","A57":"Ausencia o Presencia/1L ","A42":"B. cereus / superficie","989":"CIO2 - mg/L","988":"CIO3 - mg/L","773":"cm","982":"Coliform bacilli en 25 g","945":"Coliformes / 1 g","912":"cp","944":"CP (25ºC)","A82":"cps (20ºC)","A54":"cps (25ºC)","629":"cps (50ºC)","A55":"cps (65ºC)","A56":"cps (97ºC)","631":"cTs (100ºC)","J12":"dS/m","851":"E. coli / 1 g","968":"E. coli / 1 g","918":"E. coli / 100cm2","I67":"E. coli / 25 g","626":"E. coli / área","627":"E. coli / cm2","760":"E. coli / manos","837":"E. coli / superficie","613":"E. coli O 157:H7 / 25 g","A48":"E. coli O 157:H7 / 25 ml","E74":"E. coli O 157:H7/325 g","981":"Enterobacterias / 1 g","610":"Estéril / No Estéril","737":"g","961":"g / L","957":"g/100g","845":"g/100g (Límite de Cuantificación: 0.08)","974":"g/100ml","955":"g/115g","956":"g/130g","I91":"g/200mL","946":"g/Kg","B28":"g/L (Ác. acético)","A60":"g/m2","771":"g/ml","783":"g/ml (20ºC)","630":"g/ml (53ºC)","628":"g/ml (65ºC)","980":"g/paquete","954":"g/porción","993":"gr/dm3","A53":"gr/ml ( 97ºC)","A51":"gr/ml (25ºC)","A52":"gr/ml (65ºC)","775":"Kcal/100g","J55":"Kcal/100ml","I92":"Kcal/200mL","990":"Kg","E75":"Kg/m3","437":"Listeria / 100 Cm2","436":"Listeria / 25 g","I72":"Listeria / 25ml ","994":"Listeria / manos","438":"Listeria / superficie","E73":"Listeria monocytogenes/325 g","I79":"Listeria spp.","992":"Lt.","A58":"LUX","I65":"mcg/100g","I66":"mcgRE/g","744":"meq de Ácido/Kg","741":"meq Peróxido /Kg Aceite","934":"meq/Kg","J13":"meq/L","562":"meqPeroxido / Kg Grasa","939":"mg","960":"mg / 100 ml AA","888":"mg Ac Ascorbico/100g","J10":"mg Ac Ascorbico/100ml","I78":"mg Ácido sórbico","I68":"mg C3H6O3","886":"mg CaCO3/L","947":"mg Imidacloprid/Kg","772":"mg Nitrógeno Amoniacal/100g","887":"mg Nitrógeno Amoniacal/L","J16":"mg/100 gr (Ac. Sulfúrico)","963":"mg/100 mL","867":"mg/100g","I74":"mg/100ml","I88":"mg/200mL","I75":"mg/240ml","J15":"mg/dm2","625":"mg/Kg","618":"mg/L","619":"mgCaO3/L","J57":"miliequivalentes/Kg","972":"min. 10^7","774":"ml","A41":"ml/L","967":"mm","995":"mmHg","435":"N° / 100 Ml","785":"NBV/100g","J14":"ng/g","J11":"NMP / 100 g","054":"NMP / 100 ml","052":"NMP / g","053":"NMP / ml","620":"NTU","623":"NUO","624":"NUS","919":"ºBaumé","738":"ºBe","566":"ºBrix","840":"ºC","966":"ºHaugh","962":"Org / 100 mL","609":"Org/L","996":"ºZ","740":"pH (10ºBrix)","739":"pH Solución 50%","632":"Positivo/Negativo","839":"ppb","564":"ppm","E78":"ppm de Oleato de Sodio","848":"Presencia/Ausencia","969":"Pseudomonas aeruginosa / 1 g","I70":"Pseudomonas aeruginosa/ 1g","987":"Pseudomonas/100ml","I69":"Pulg-Hg","838":"S. aureus / superficie","060":"Salmonella / 100Cm2","A49":"Salmonella / 25 ml","058":"Salmonella / 25g","998":"Salmonella / 4 superficies","868":"Salmonella / 50 g","959":"Salmonella / 750 g","059":"Salmonella / área","B24":"Salmonella / cm2","745":"Salmonella / manos","780":"Salmonella / superficie","A59":"Salmonella sp./ 25g","E72":"Salmonella/325 g","878":"Samonella / 100 ml","846":"Shigella/25 g","970":"Staphylococcus aureus / 1 g","753":"Staphylococcus aureus / 100 cm2","752":"Staphylococcus aureus / área","879":"Staphylococcus aureus / manos","933":"Staphylococcus aureus / superficie ","971":"Streptococcus / 10 g","622":"UCV escala Pt/Co","434":"UFC / 15 Min","997":"UFC / 4 superficies","056":"UFC / área","055":"UFC / Cm2","049":"UFC / g ","983":"UFC / g (Est.)","057":"UFC / manos","050":"UFC / ml","985":"UFC / ml (Est.)","750":"UFC / placa 40 cm2","758":"UFC / placa 60 cm2","779":"UFC / superficie","051":"UFC/ 100 ml","A47":"UFC/ 100g","978":"UFC/ ml 35ºC/48 h R2A","608":"UFC/15","616":"UFP/ml","927":"ug/100g","I76":"ug/100ml","I73":"ug/200ml","I89":"ug/200mL","I77":"ug/240ml","964":"ug/g","770":"ug/Kg","I71":"ug/L","A84":"ug/mL","I64":"ugRE/g","742":"UI","913":"UI/100g","I90":"UI/200mL","621":"umho/cm","J56":"unidades kertesz","986":"V. cholerae/manos","942":"Vibrio cholerae / 100 cm2","617":"Vibrio cholerae / 25 g","909":"Vibrio parahaemolyticus / 25 g","K98":"% Alc. Vol","M51":"Nº de esporas de bacilos MAEF/g","M52":"Nº de formadores de esporas anaerobias/g","M53":"Nº de esporas/10g"}'],
=======
            editable:[[3, 'unidadmedida', '{"565":"-","561":"%","784":"% (Ac. acético)","776":"% (Ac. cítrico)","611":"% (Ac. Láctico)","A43":"% (Ác. málico)","612":"% (Ac. Sulfúrico)","781":"% (Al 15% Humedad)","B61":"% (Amoniaco)","B29":"% del extracto seco","E76":"% Vol.","743":"%(Ac. Oleico)","849":"%(Ac. Palmitico)","941":"%p/p","999":"%V","850":"(20ºC)","872":"(25º)","A44":"(g/100g) Base Seca","782":"(mg KOH)/g)","943":"\ Hg","938":"°D","940":"°Z","991":"µg retinol/100g","873":"µs/cm","880":"Ausencia / 100 ml","563":"Ausencia / Presencia","906":"Ausencia en 25g / Presencia en 25g","976":"Ausencia o Presencia / 25 g","977":"Ausencia o Presencia/100g","A57":"Ausencia o Presencia/1L ","A42":"B. cereus / superficie","989":"CIO2 - mg/L","988":"CIO3 - mg/L","773":"cm","982":"Coliform bacilli en 25 g","945":"Coliformes / 1 g","912":"cp","944":"CP (25ºC)","A82":"cps (20ºC)","A54":"cps (25ºC)","629":"cps (50ºC)","A55":"cps (65ºC)","A56":"cps (97ºC)","631":"cTs (100ºC)","J12":"dS/m","851":"E. coli / 1 g","968":"E. coli / 1 g","918":"E. coli / 100cm2","I67":"E. coli / 25 g","626":"E. coli / área","627":"E. coli / cm2","760":"E. coli / manos","837":"E. coli / superficie","613":"E. coli O 157:H7 / 25 g","A48":"E. coli O 157:H7 / 25 ml","E74":"E. coli O 157:H7/325 g","981":"Enterobacterias / 1 g","610":"Estéril / No Estéril","737":"g","961":"g / L","957":"g/100g","845":"g/100g (Límite de Cuantificación: 0.08)","974":"g/100ml","955":"g/115g","956":"g/130g","I91":"g/200mL","946":"g/Kg","B28":"g/L (Ác. acético)","A60":"g/m2","771":"g/ml","783":"g/ml (20ºC)","630":"g/ml (53ºC)","628":"g/ml (65ºC)","980":"g/paquete","954":"g/porción","993":"gr/dm3","A53":"gr/ml ( 97ºC)","A51":"gr/ml (25ºC)","A52":"gr/ml (65ºC)","775":"Kcal/100g","J55":"Kcal/100ml","I92":"Kcal/200mL","990":"Kg","E75":"Kg/m3","437":"Listeria / 100 Cm2","436":"Listeria / 25 g","I72":"Listeria / 25ml ","994":"Listeria / manos","438":"Listeria / superficie","E73":"Listeria monocytogenes/325 g","I79":"Listeria spp.","992":"Lt.","A58":"LUX","I65":"mcg/100g","I66":"mcgRE/g","744":"meq de Ácido/Kg","741":"meq Peróxido /Kg Aceite","934":"meq/Kg","J13":"meq/L","562":"meqPeroxido / Kg Grasa","939":"mg","960":"mg / 100 ml AA","888":"mg Ac Ascorbico/100g","J10":"mg Ac Ascorbico/100ml","I78":"mg Ácido sórbico","I68":"mg C3H6O3","886":"mg CaCO3/L","947":"mg Imidacloprid/Kg","772":"mg Nitrógeno Amoniacal/100g","887":"mg Nitrógeno Amoniacal/L","J16":"mg/100 gr (Ac. Sulfúrico)","963":"mg/100 mL","867":"mg/100g","I74":"mg/100ml","I88":"mg/200mL","I75":"mg/240ml","J15":"mg/dm2","625":"mg/Kg","618":"mg/L","619":"mgCaO3/L","J57":"miliequivalentes/Kg","972":"min. 10^7","774":"ml","A41":"ml/L","967":"mm","995":"mmHg","435":"N° / 100 Ml","785":"NBV/100g","J14":"ng/g","J11":"NMP / 100 g","054":"NMP / 100 ml","052":"NMP / g","053":"NMP / ml","620":"NTU","623":"NUO","624":"NUS","919":"ºBaumé","738":"ºBe","566":"ºBrix","840":"ºC","966":"ºHaugh","962":"Org / 100 mL","609":"Org/L","996":"ºZ","740":"pH (10ºBrix)","739":"pH Solución 50%","632":"Positivo/Negativo","839":"ppb","564":"ppm","E78":"ppm de Oleato de Sodio","848":"Presencia/Ausencia","969":"Pseudomonas aeruginosa / 1 g","I70":"Pseudomonas aeruginosa/ 1g","987":"Pseudomonas/100ml","I69":"Pulg-Hg","838":"S. aureus / superficie","060":"Salmonella / 100Cm2","A49":"Salmonella / 25 ml","058":"Salmonella / 25g","998":"Salmonella / 4 superficies","868":"Salmonella / 50 g","959":"Salmonella / 750 g","059":"Salmonella / área","B24":"Salmonella / cm2","745":"Salmonella / manos","780":"Salmonella / superficie","A59":"Salmonella sp./ 25g","E72":"Salmonella/325 g","878":"Samonella / 100 ml","846":"Shigella/25 g","970":"Staphylococcus aureus / 1 g","753":"Staphylococcus aureus / 100 cm2","752":"Staphylococcus aureus / área","879":"Staphylococcus aureus / manos","933":"Staphylococcus aureus / superficie ","971":"Streptococcus / 10 g","622":"UCV escala Pt/Co","434":"UFC / 15 Min","997":"UFC / 4 superficies","056":"UFC / área","055":"UFC / Cm2","049":"UFC / g ","983":"UFC / g (Est.)","057":"UFC / manos","050":"UFC / ml","985":"UFC / ml (Est.)","750":"UFC / placa 40 cm2","758":"UFC / placa 60 cm2","779":"UFC / superficie","051":"UFC/ 100 ml","A47":"UFC/ 100g","978":"UFC/ ml 35ºC/48 h R2A","608":"UFC/15","616":"UFP/ml","927":"ug/100g","I76":"ug/100ml","I73":"ug/200ml","I89":"ug/200mL","I77":"ug/240ml","964":"ug/g","770":"ug/Kg","I71":"ug/L","A84":"ug/mL","I64":"ugRE/g","742":"UI","913":"UI/100g","I90":"UI/200mL","621":"umho/cm","J56":"unidades kertesz","986":"V. cholerae/manos","942":"Vibrio cholerae / 100 cm2","617":"Vibrio cholerae / 25 g","909":"Vibrio parahaemolyticus / 25 g","K98":"% Alc. Vol","M40":"mg de acetato de a-Tocoferol/g","M42":"UFC/L","M43":"g/100 g de grasa","M44":"g/cc (20°C)","M45":"Detectable/No Detectable","M47":"g de ácido láctico/100g","M48":"KJ/100g","M49":"Bq/L","M51":"Nº de esporas de bacilos MAEF/g","M52":"Nº de formadores de esporas anaerobias/g","M53":"Nº de esporas/10g"}'],
>>>>>>> 29845f69b1c39ac903d8b65cb5fe49bca6d1009e
                      [4,'dresultado'],[5,'dresultadoexp']]
        },
        onSuccess: function(data, textStatus, jqXHR) {
            otblListResultadosold.ajax.reload(null,false);
            //var v_idot = $('#txtidordenservicio').val();
            //var v_tipoensayo = $('#mcbotipoensayo').val();
            //var v_acreditado = $('#mcboacreditado').val();
            //objListaresultold.viewListold(v_idot,v_tipoensayo,v_acreditado)
        },
    });
});

checkSeleccion = function(IDORDENSERVICIO,IDCOTIZACION,IDPRODUCTO,CMUESTRA,CENSAYO,NVIAUSADO,SSELECT){
    
    var params = { 
        "cinternoordenservicio" : IDORDENSERVICIO,
        "cinternocotizacion"    : IDCOTIZACION,
        "nordenproducto"        : IDPRODUCTO,
        "cmuestra"              : CMUESTRA,
        "censayo"               : CENSAYO,
        "nviausado"            : NVIAUSADO,
        "sselect"            : SSELECT,
    };
    
    var request = $.ajax({
        type: 'POST',
        url: baseurl+"lab/resultados/cregresult/updseleccion",
        async: true,
        data: params,
        error: function(){
            Vtitle = 'Error en Guardar!!!';
            Vtype = 'error';
            sweetalert(Vtitle,Vtype); 
        }
    });
    request.done(function( respuesta ) {             
        var v_idot = $('#txtidordenservicio').val();
        var v_tipoensayo = $('#mcbotipoensayo').val();
        var v_acreditado = $('#mcboacreditado').val();
        var v_conttipoprod = $('#hdconcc').val();
        
        if (v_conttipoprod == 'S'){            
            otblListResultados.ajax.reload(null,false);
            //objListaresult.viewList(v_idot,v_tipoensayo,v_acreditado)
        }else{           
            otblListResultadosold.ajax.reload(null,false);
            //objListaresultold.viewListold(v_idot,v_tipoensayo,v_acreditado)
        } 
    });
};

checkVB = function(IDORDENSERVICIO,IDCOTIZACION,IDPRODUCTO,CMUESTRA,CENSAYO,NVIAUSADO,SVISTOBUENO){
    
    var params = { 
        "cinternoordenservicio" : IDORDENSERVICIO,
        "cinternocotizacion"    : IDCOTIZACION,
        "nordenproducto"        : IDPRODUCTO,
        "cmuestra"              : CMUESTRA,
        "censayo"               : CENSAYO,
        "nviausado"            : NVIAUSADO,
        "svistobueno"            : SVISTOBUENO,
    };
    
    var request = $.ajax({
        type: 'POST',
        url: baseurl+"lab/resultados/cregresult/updvistobueno",
        async: true,
        data: params,
        error: function(){
            Vtitle = 'Error en Guardar!!!';
            Vtype = 'error';
            sweetalert(Vtitle,Vtype); 
        }
    });
    request.done(function( respuesta ) {             
        var v_idot = $('#txtidordenservicio').val();
        var v_tipoensayo = $('#mcbotipoensayo').val();
        var v_acreditado = $('#mcboacreditado').val();
        var v_conttipoprod = $('#hdconcc').val();
        
        if (v_conttipoprod == 'S'){
            otblListResultados.ajax.reload(null,false);
            //objListaresult.viewList(v_idot,v_tipoensayo,v_acreditado)
        }else{ 
            otblListResultadosold.ajax.reload(null,false);          
            //objListaresultold.viewListold(v_idot,v_tipoensayo,v_acreditado)
        } 
    });
};

function pulsarListarCoti(e) {
    if (e.keyCode === 13 && !e.shiftKey) {
        e.preventDefault();
        
        parametros = paramListarBusqueda();
        getListarBusqueda(parametros);
    }
}  

pdfInfensayoPrevia = function(){    
    var_idot = $('#txtidordenservicio').val();
    var_idmuestra = 'T';
    window.open(baseurl+"lab/formatospdf/pdfingresult/pdfInfensayoPreviaMuestra/"+var_idot+"/"+var_idmuestra);

    //var_tipoinf = $('#mcbotipoinforme').val();    
    /*if(var_tipoinf == '064'){ //consolidado
        window.open(baseurl+"lab/formatospdf/pdfingresult/pdfInfensayoPreviaConsol/"+var_idot+"/"+var_idmuestra);
    }else if(var_tipoinf == '067'){ // muestra
        window.open(baseurl+"lab/formatospdf/pdfingresult/pdfInfensayoPreviaMuestra/"+var_idot+"/"+var_idmuestra);
    }else if(var_tipoinf == '066'){ // resultado
        window.open(baseurl+"lab/formatospdf/pdfingresult/pdfInfensayoPreviaResul/"+var_idot+"/"+var_idmuestra);
    }else if(var_tipoinf == '920'){ // resultado-indv
        window.open(baseurl+"lab/formatospdf/pdfingresult/pdfInfensayoPreviaResulindv/"+var_idot+"/"+var_idmuestra);
    }*/
    
}
pdfInfensayoPreliminar = function(){    
    var_idot = $('#txtidordenservicio').val();
    var_idmuestra = 'T';
    window.open(baseurl+"lab/formatospdf/pdfingresult/pdfInfensayoPreliminar/"+var_idot+"/"+var_idmuestra);
}
pdfCerticalidadPrevia = function(){    
    var_idot = $('#txtidordenservicio').val();
    var_idmuestra = 'T';
    window.open(baseurl+"lab/formatospdf/pdfingresult/pdfCertificadoPreviaIndiv/"+var_idot+"/"+var_idmuestra);


    /*var_tipocerti = $('#txtzctipocerticalidad').val();
    if(var_tipocerti == '762'){ //consolidado
        window.open(baseurl+"lab/formatospdf/pdfingresult/pdfCertificadoPreviaIndiv/"+var_idot+"/"+var_idmuestra);
    }else if(var_tipocerti == '763'){ // consolidado simple
        window.open(baseurl+"lab/formatospdf/pdfingresult/pdfCertificadoConsolsimple/"+var_idot+"/"+var_idmuestra);
    }else if(var_tipocerti == '764'){ // consolidado tienda
        window.open(baseurl+"lab/formatospdf/pdfingresult/pdfCertificadoConsoltienda/"+var_idot+"/"+var_idmuestra);
    }else if(var_tipocerti == 'A40'){ // bambas
        window.open(baseurl+"lab/formatospdf/pdfingresult/pdfCertificadoBambas/"+var_idot+"/"+var_idmuestra);
    }*/
    
}

$('#btnaddSenso').click(function(){
    v_cinternoordenservicio = $('#mhdncinternoordenservicio').val();
    v_cinternocotizacion = $('#mhdncinternocotizacion').val();
    v_nordenproducto = $('#mhdnnordenproducto').val();
    v_cmuestra = $('#mhdncmuestra').val();
    v_censayo = $('#mhdncensayo').val();
    v_nviausado = $('#mhdnnviausado').val();

    v_concc = $('#mhdsconcc').val();

    var parametros = {
        "cinternoordenservicio" : v_cinternoordenservicio,
        "cinternocotizacion" : v_cinternocotizacion,
        "nordenproducto" : v_nordenproducto,
        "cmuestra" : v_cmuestra,
        "censayo" : v_censayo,
        "nviausado" : v_nviausado,
    };
    $.ajax({
       url      :  baseurl+"lab/resultados/cregresult/setaddsensorial",
       method   : "GET",
       data     : parametros, 
    }).done(function(data){
        listarSensorial(v_cinternoordenservicio,v_cinternocotizacion,v_nordenproducto,v_cmuestra,v_censayo,v_nviausado);
    })
});
regSensorial = function(cinternoordenservicio,cinternocotizacion,nordenproducto,cmuestra,censayo,nviausado,concc){
    $('#modalSensorial').modal({show:true});

    $('#mhdncinternoordenservicio').val(cinternoordenservicio);
    $('#mhdncinternocotizacion').val(cinternocotizacion);
    $('#mhdnnordenproducto').val(nordenproducto);
    $('#mhdncmuestra').val(cmuestra);
    $('#mhdncensayo').val(censayo);
    $('#mhdnnviausado').val(nviausado);
         
    $('#mhdsconcc').val(concc);
    if (concc == 'N'){      
        $('#divtblSensorial').hide();           
        $('#divtblSensorialold').show();
        listarSensorial_old(cinternoordenservicio,cinternocotizacion,nordenproducto,cmuestra,censayo,nviausado);             
    }else{  
        $('#divtblSensorial').show();           
        $('#divtblSensorialold').hide();
        listarSensorial(cinternoordenservicio,cinternocotizacion,nordenproducto,cmuestra,censayo,nviausado);   
    }
    
};
listarSensorial = function(cinternoordenservicio,cinternocotizacion,nordenproducto,cmuestra,censayo,nviausado){
    var parametros = {
        "cinternoordenservicio" : cinternoordenservicio,
        "cinternocotizacion" : cinternocotizacion,
        "nordenproducto" : nordenproducto,
        "cmuestra" : cmuestra,
        "censayo" : censayo,
        "nviausado" : nviausado,
    };
    $.ajax({
       url      :  baseurl+"lab/resultados/cregresult/getlistresultsenso",
       method   : "GET",
       data     : parametros, 
    }).done(function(data){
        $('#tblSensorial tbody').html(data);
        editarSensorial();
    })
}
listarSensorial_old = function(cinternoordenservicio,cinternocotizacion,nordenproducto,cmuestra,censayo,nviausado){
    var parametros = {
        "cinternoordenservicio" : cinternoordenservicio,
        "cinternocotizacion" : cinternocotizacion,
        "nordenproducto" : nordenproducto,
        "cmuestra" : cmuestra,
        "censayo" : censayo,
        "nviausado" : nviausado,
    };
    $.ajax({
       url      :  baseurl+"lab/resultados/cregresult/getlistresultsenso_old",
       method   : "GET",
       data     : parametros, 
    }).done(function(data){
        $('#tblSensorialold tbody').html(data);
        editarSensorial_old();
    })
}
editarSensorial = function(){    
    $('#tblSensorial').Tabledit({
        url:'lab/resultados/cregresult/setresultsenso',
        eventType: 'click',
        editButton: false,
        deleteButton: false,
        saveButton: false,
        autoFocus: false,
        hideIdentifier: true,
		restoreButton: false,
        buttons: {
            edit: {
                class: 'btn btn-sm btn-primary',
                html: '<span class="fas fa-pencil-alt"></span>',
                action: 'edit'
            }
        },
        columns:{
            identifier : [0,'ID'],
            editable:[[1,'dnombreescala'],[2,'dlimite'],[3,'dresultado'],[4,'sconclusion', '{"N": "NO CONFORME", "C": "CONFORME"}']]
        },
        onSuccess: function(data, textStatus, jqXHR) {
        },
    });
}
editarSensorial_old = function(){    
    $('#tblSensorialold').Tabledit({
        url:'lab/resultados/cregresult/setresultsenso',
        eventType: 'click',
        editButton: false,
        deleteButton: false,
        saveButton: false,
        autoFocus: false,
        hideIdentifier: true,
		restoreButton: false,
        buttons: {
            edit: {
                class: 'btn btn-sm btn-primary',
                html: '<span class="fas fa-pencil-alt"></span>',
                action: 'edit'
            }
        },
        columns:{
            identifier : [0,'ID'],
            editable:[[1,'dnombreescala'],[2,'dresultado']]
        },
        onSuccess: function(data, textStatus, jqXHR) {
        },
    });
}
delSensorial = function(cinternoordenservicio,cinternocotizacion,nordenproducto,cmuestra,censayo,norden){

    Swal.fire({
        title: 'Confirmar Eliminación',
        text: "¿Está seguro de eliminar el Atributo?",
        icon: 'error',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, bórralo!'
    }).then((result) => {
        if (result.value) {
            $.post(baseurl+"lab/resultados/cregresult/delsensorial/", 
            {
                cinternoordenservicio   : cinternoordenservicio,
                cinternocotizacion   : cinternocotizacion,
                nordenproducto   : nordenproducto,
                cmuestra   : cmuestra,
                censayo   : censayo,
                norden   : norden,
            },      
            function(data){     
                
                var_nvia = $('#mhdnnviausado').val();
                listarSensorial(cinternoordenservicio,cinternocotizacion,nordenproducto,cmuestra,censayo,var_nvia); 
                Vtitle = 'Se Elimino Correctamente';
                Vtype = 'success';
                sweetalert(Vtitle,Vtype);      
            });
        }
    })     
}

$('#btnrecargar').click(function(){
    cinternoordenservicio = $('#mhdecinternoordenservicio').val();
    cinternocotizacion = $('#mhdecinternocotizacion').val();
    nordenproducto = $('#mhdenordenproducto').val();
    cmuestra = $('#mhdecmuestra').val();
    censayo = $('#mhdecensayo').val();
    nviausado = $('#mhdenviausado').val();
    zctipoensayo = $('#mhdezctipoensayo').val();      
    concc = $('#mhdeconcc').val();

    if (concc == 'N'){      
        $('#divtblElementos').hide();           
        $('#divtblElementosold').show();
        listarElementos_old(cinternoordenservicio,cinternocotizacion,nordenproducto,cmuestra,censayo,nviausado,zctipoensayo);              
    }else{  
        $('#divtblElementos').show();           
        $('#divtblElementosold').hide();
        listarElementos(cinternoordenservicio,cinternocotizacion,nordenproducto,cmuestra,censayo,nviausado,zctipoensayo);     
    }
})

regElementos = function(cinternoordenservicio,cinternocotizacion,nordenproducto,cmuestra,censayo,nviausado,zctipoensayo,concc){
    $('#modalElementos').modal({show:true});

    $('#mhdecinternoordenservicio').val(cinternoordenservicio);
    $('#mhdecinternocotizacion').val(cinternocotizacion);
    $('#mhdenordenproducto').val(nordenproducto);
    $('#mhdecmuestra').val(cmuestra);
    $('#mhdecensayo').val(censayo);
    $('#mhdenviausado').val(nviausado);
    $('#mhdezctipoensayo').val(zctipoensayo);      
    $('#mhdeconcc').val(concc);
    
    if (concc == 'N'){      
        $('#divtblElementos').hide();           
        $('#divtblElementosold').show();
        listarElementos_old(cinternoordenservicio,cinternocotizacion,nordenproducto,cmuestra,censayo,nviausado,zctipoensayo);              
    }else{  
        $('#divtblElementos').show();           
        $('#divtblElementosold').hide();
        listarElementos(cinternoordenservicio,cinternocotizacion,nordenproducto,cmuestra,censayo,nviausado,zctipoensayo);     
    }
    
};
listarElementos_old = function(cinternoordenservicio,cinternocotizacion,nordenproducto,cmuestra,censayo,nviausado,zctipoensayo){
   
    var parametros = {
        "cinternoordenservicio" : cinternoordenservicio,
        "cinternocotizacion" : cinternocotizacion,
        "nordenproducto" : nordenproducto,
        "cmuestra" : cmuestra,
        "censayo" : censayo,
        "nviausado" : nviausado,
        "zctipoensayo" : zctipoensayo,
    };    

    otblElementosold = $('#tblElementosold').DataTable({ 
        "processing"  	: true,
        "bDestroy"    	: true,
        "stateSave"     : true,
        "bJQueryUI"     : true,
        "scrollResize"  : true,
        "scrollY"     	: "540px",
        "scrollX"     	: true, 
        'AutoWidth'     : false,
        "paging"      	: true,
        "info"        	: false,
        "filter"      	: true, 
        "ordering"		: false,
        "responsive"    : false,
        "select"        : true,
        "ajax"	: {
            "url"   : baseurl+"lab/resultados/cregresult/getlistresultelementos_old",
            "type"  : "POST", 
            "data"  : parametros,     
            dataSrc : ''      
        }, 
        'columns'	: [
            {data: 'BLANCO', "className":"col-xxxs"},
            {data: 'delemento'},
            {data: 'dunidad'},
            {data: 'dresultado'},
            {data: 'sinfensayo'},
            {data: 'ID', "className":"ocultar col-xxxs"},
        ],
        "rowCallback":function(row,data){           
            if(data.sinfensayo == "1"){
                $(row).css("background-color","#A7D1AE");
            }else{
                $(row).css("background-color","#FFFFFF"); 
            }
        },
    });
    // Enumeracion 
    otblElementosold.on( 'order.dt search.dt', function () { 
        otblElementosold.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i+1;
          } );
    }).draw();  

    otblElementosold.column(0).visible(true); 
}
$('#tblElementosold').on('draw.dt', function(){ 
    $('#tblElementosold').Tabledit({
        url:'lab/resultados/cregresult/setresultelementos_old',
        dataType:'json',
        eventType: 'click',
        editButton: false,
        deleteButton: false,
        saveButton: false,
        autoFocus: false,
        hideIdentifier: true,
		restoreButton: false,
        buttons: {
            edit: {
                class: 'btn btn-sm btn-primary',
                html: '<span class="fas fa-pencil-alt"></span>',
                action: 'edit'
            }
        },
        columns:{
            identifier : [5,'ID'],
            editable:[[2,'dunidad', '{"565":"-","561":"%","784":"% (Ac. acético)","776":"% (Ac. cítrico)","611":"% (Ac. Láctico)","A43":"% (Ác. málico)","612":"% (Ac. Sulfúrico)","781":"% (Al 15% Humedad)","B61":"% (Amoniaco)","B29":"% del extracto seco","E76":"% Vol.","743":"%(Ac. Oleico)","849":"%(Ac. Palmitico)","941":"%p/p","999":"%V","850":"(20ºC)","872":"(25º)","A44":"(g/100g) Base Seca","782":"(mg KOH)/g)","943":"\ Hg","938":"°D","940":"°Z","991":"µg retinol/100g","873":"µs/cm","880":"Ausencia / 100 ml","563":"Ausencia / Presencia","906":"Ausencia en 25g / Presencia en 25g","976":"Ausencia o Presencia / 25 g","977":"Ausencia o Presencia/100g","A57":"Ausencia o Presencia/1L ","A42":"B. cereus / superficie","989":"CIO2 - mg/L","988":"CIO3 - mg/L","773":"cm","982":"Coliform bacilli en 25 g","945":"Coliformes / 1 g","912":"cp","944":"CP (25ºC)","A82":"cps (20ºC)","A54":"cps (25ºC)","629":"cps (50ºC)","A55":"cps (65ºC)","A56":"cps (97ºC)","631":"cTs (100ºC)","J12":"dS/m","851":"E. coli / 1 g","968":"E. coli / 1 g","918":"E. coli / 100cm2","I67":"E. coli / 25 g","626":"E. coli / área","627":"E. coli / cm2","760":"E. coli / manos","837":"E. coli / superficie","613":"E. coli O 157:H7 / 25 g","A48":"E. coli O 157:H7 / 25 ml","E74":"E. coli O 157:H7/325 g","981":"Enterobacterias / 1 g","610":"Estéril / No Estéril","737":"g","961":"g / L","957":"g/100g","845":"g/100g (Límite de Cuantificación: 0.08)","974":"g/100ml","955":"g/115g","956":"g/130g","I91":"g/200mL","946":"g/Kg","B28":"g/L (Ác. acético)","A60":"g/m2","771":"g/ml","783":"g/ml (20ºC)","630":"g/ml (53ºC)","628":"g/ml (65ºC)","980":"g/paquete","954":"g/porción","993":"gr/dm3","A53":"gr/ml ( 97ºC)","A51":"gr/ml (25ºC)","A52":"gr/ml (65ºC)","775":"Kcal/100g","J55":"Kcal/100ml","I92":"Kcal/200mL","990":"Kg","E75":"Kg/m3","437":"Listeria / 100 Cm2","436":"Listeria / 25 g","I72":"Listeria / 25ml ","994":"Listeria / manos","438":"Listeria / superficie","E73":"Listeria monocytogenes/325 g","I79":"Listeria spp.","992":"Lt.","A58":"LUX","I65":"mcg/100g","I66":"mcgRE/g","744":"meq de Ácido/Kg","741":"meq Peróxido /Kg Aceite","934":"meq/Kg","J13":"meq/L","562":"meqPeroxido / Kg Grasa","939":"mg","960":"mg / 100 ml AA","888":"mg Ac Ascorbico/100g","J10":"mg Ac Ascorbico/100ml","I78":"mg Ácido sórbico","I68":"mg C3H6O3","886":"mg CaCO3/L","947":"mg Imidacloprid/Kg","772":"mg Nitrógeno Amoniacal/100g","887":"mg Nitrógeno Amoniacal/L","J16":"mg/100 gr (Ac. Sulfúrico)","963":"mg/100 mL","867":"mg/100g","I74":"mg/100ml","I88":"mg/200mL","I75":"mg/240ml","J15":"mg/dm2","625":"mg/Kg","618":"mg/L","619":"mgCaO3/L","J57":"miliequivalentes/Kg","972":"min. 10^7","774":"ml","A41":"ml/L","967":"mm","995":"mmHg","435":"N° / 100 Ml","785":"NBV/100g","J14":"ng/g","J11":"NMP / 100 g","054":"NMP / 100 ml","052":"NMP / g","053":"NMP / ml","620":"NTU","623":"NUO","624":"NUS","919":"ºBaumé","738":"ºBe","566":"ºBrix","840":"ºC","966":"ºHaugh","962":"Org / 100 mL","609":"Org/L","996":"ºZ","740":"pH (10ºBrix)","739":"pH Solución 50%","632":"Positivo/Negativo","839":"ppb","564":"ppm","E78":"ppm de Oleato de Sodio","848":"Presencia/Ausencia","969":"Pseudomonas aeruginosa / 1 g","I70":"Pseudomonas aeruginosa/ 1g","987":"Pseudomonas/100ml","I69":"Pulg-Hg","838":"S. aureus / superficie","060":"Salmonella / 100Cm2","A49":"Salmonella / 25 ml","058":"Salmonella / 25g","998":"Salmonella / 4 superficies","868":"Salmonella / 50 g","959":"Salmonella / 750 g","059":"Salmonella / área","B24":"Salmonella / cm2","745":"Salmonella / manos","780":"Salmonella / superficie","A59":"Salmonella sp./ 25g","E72":"Salmonella/325 g","878":"Samonella / 100 ml","846":"Shigella/25 g","970":"Staphylococcus aureus / 1 g","753":"Staphylococcus aureus / 100 cm2","752":"Staphylococcus aureus / área","879":"Staphylococcus aureus / manos","933":"Staphylococcus aureus / superficie ","971":"Streptococcus / 10 g","622":"UCV escala Pt/Co","434":"UFC / 15 Min","997":"UFC / 4 superficies","056":"UFC / área","055":"UFC / Cm2","049":"UFC / g ","983":"UFC / g (Est.)","057":"UFC / manos","050":"UFC / ml","985":"UFC / ml (Est.)","750":"UFC / placa 40 cm2","758":"UFC / placa 60 cm2","779":"UFC / superficie","051":"UFC/ 100 ml","A47":"UFC/ 100g","978":"UFC/ ml 35ºC/48 h R2A","608":"UFC/15","616":"UFP/ml","927":"ug/100g","I76":"ug/100ml","I73":"ug/200ml","I89":"ug/200mL","I77":"ug/240ml","964":"ug/g","770":"ug/Kg","I71":"ug/L","A84":"ug/mL","I64":"ugRE/g","742":"UI","913":"UI/100g","I90":"UI/200mL","621":"umho/cm","J56":"unidades kertesz","986":"V. cholerae/manos","942":"Vibrio cholerae / 100 cm2","617":"Vibrio cholerae / 25 g","909":"Vibrio parahaemolyticus / 25 g","M50":"msv/año","M49":"Bq/L"}'],
                [3,'dresultado']]
        },
        onSuccess: function(data, textStatus, jqXHR) {
            
            otblElementosold.ajax.reload(null,false);

            /*var v_cinternoordenservicio = $('#mhdecinternoordenservicio').val();
            var v_cinternocotizacion = $('#mhdecinternocotizacion').val();
            var v_nordenproducto = $('#mhdenordenproducto').val();
            var v_cmuestra = $('#mhdecmuestra').val();
            var v_censayo = $('#mhdecensayo').val();
            var v_nviausado = $('#mhdenviausado').val();
            var v_zctipoensayo = $('#mhdezctipoensayo').val();
            
            listarElementos_old(v_cinternoordenservicio,v_cinternocotizacion,v_nordenproducto,v_cmuestra,v_censayo,v_nviausado,v_zctipoensayo);*/
        },
    });
});
listarElementos = function(cinternoordenservicio,cinternocotizacion,nordenproducto,cmuestra,censayo,nviausado,zctipoensayo){
    
    var parametros = {
        "cinternoordenservicio" : cinternoordenservicio,
        "cinternocotizacion" : cinternocotizacion,
        "nordenproducto" : nordenproducto,
        "cmuestra" : cmuestra,
        "censayo" : censayo,
        "nviausado" : nviausado,
        "zctipoensayo" : zctipoensayo,
    };

    otblElementos = $('#tblElementos').DataTable({ 
        "processing"  	: true,
        "bDestroy"    	: true,
        "stateSave"     : true,
        "bJQueryUI"     : true,
        "scrollResize"  : true,
        "scrollY"     	: "540px",
        "scrollX"     	: true, 
        'AutoWidth'     : false,
        "paging"      	: true,
        "info"        	: false,
        "filter"      	: true, 
        "ordering"		: false,
        "responsive"    : false,
        "select"        : true,
        "ajax"	: {
            "url"   : baseurl+"lab/resultados/cregresult/getlistresultelementos",
            "type"  : "POST", 
            "data"  : parametros,     
            dataSrc : ''      
        }, 
        'columns'	: [
            {data: 'BLANCO', "className":"col-xxxs"},
            {data: 'delemento'},
            {data: 'dunidad'},
            {data: 'condi_espe'},
            {data: 'valor_espe', render: $.fn.dataTable.render.number( ',', '.', 3) },
            {data: 'valexpo_espe', render: $.fn.dataTable.render.number( ',', '.', 0) },
            {data: 'condi_resul'},
            {data: 'valor_resul', render: $.fn.dataTable.render.number( ',', '.', 3) },
            {data: 'valexpo_resul', render: $.fn.dataTable.render.number( ',', '.', 0) },
            {data: 'sconclusion'},
            {data: 'sinfensayo'},
            {data: 'ID', "className":"ocultar col-xxxs"},
        ],
        "rowCallback":function(row,data){           
            if(data.sinfensayo == "1"){
                $(row).css("background-color","#A7D1AE");
            }else{
                $(row).css("background-color","#FFFFFF"); 
            }
        },
    });
    // Enumeracion 
    otblElementos.on( 'order.dt search.dt', function () { 
        otblElementos.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i+1;
          } );
    }).draw();  

    otblElementos.column(0).visible(true);  
}
$('#tblElementos').on('draw.dt', function(){  
    $('#tblElementos').Tabledit({
        url:'lab/resultados/cregresult/setresultelementos',
        dataType:'json',
        eventType: 'click',
        editButton: false,
        deleteButton: false,
        saveButton: false,
        autoFocus: false,
        hideIdentifier: true,
		restoreButton: false,
        buttons: {
            edit: {
                class: 'btn btn-sm btn-primary',
                html: '<span class="fas fa-pencil-alt"></span>',
                action: 'edit'
            }
        },
        columns:{
            identifier : [11,'ID'],
        editable:[[2,'dunidad', '{"565":"-","561":"%","784":"% (Ac. acético)","776":"% (Ac. cítrico)","611":"% (Ac. Láctico)","A43":"% (Ác. málico)","612":"% (Ac. Sulfúrico)","781":"% (Al 15% Humedad)","B61":"% (Amoniaco)","B29":"% del extracto seco","E76":"% Vol.","743":"%(Ac. Oleico)","849":"%(Ac. Palmitico)","941":"%p/p","999":"%V","850":"(20ºC)","872":"(25º)","A44":"(g/100g) Base Seca","782":"(mg KOH)/g)","943":"\ Hg","938":"°D","940":"°Z","991":"µg retinol/100g","873":"µs/cm","880":"Ausencia / 100 ml","563":"Ausencia / Presencia","906":"Ausencia en 25g / Presencia en 25g","976":"Ausencia o Presencia / 25 g","977":"Ausencia o Presencia/100g","A57":"Ausencia o Presencia/1L ","A42":"B. cereus / superficie","989":"CIO2 - mg/L","988":"CIO3 - mg/L","773":"cm","982":"Coliform bacilli en 25 g","945":"Coliformes / 1 g","912":"cp","944":"CP (25ºC)","A82":"cps (20ºC)","A54":"cps (25ºC)","629":"cps (50ºC)","A55":"cps (65ºC)","A56":"cps (97ºC)","631":"cTs (100ºC)","J12":"dS/m","851":"E. coli / 1 g","968":"E. coli / 1 g","918":"E. coli / 100cm2","I67":"E. coli / 25 g","626":"E. coli / área","627":"E. coli / cm2","760":"E. coli / manos","837":"E. coli / superficie","613":"E. coli O 157:H7 / 25 g","A48":"E. coli O 157:H7 / 25 ml","E74":"E. coli O 157:H7/325 g","981":"Enterobacterias / 1 g","610":"Estéril / No Estéril","737":"g","961":"g / L","957":"g/100g","845":"g/100g (Límite de Cuantificación: 0.08)","974":"g/100ml","955":"g/115g","956":"g/130g","I91":"g/200mL","946":"g/Kg","B28":"g/L (Ác. acético)","A60":"g/m2","771":"g/ml","783":"g/ml (20ºC)","630":"g/ml (53ºC)","628":"g/ml (65ºC)","980":"g/paquete","954":"g/porción","993":"gr/dm3","A53":"gr/ml ( 97ºC)","A51":"gr/ml (25ºC)","A52":"gr/ml (65ºC)","775":"Kcal/100g","J55":"Kcal/100ml","I92":"Kcal/200mL","990":"Kg","E75":"Kg/m3","437":"Listeria / 100 Cm2","436":"Listeria / 25 g","I72":"Listeria / 25ml ","994":"Listeria / manos","438":"Listeria / superficie","E73":"Listeria monocytogenes/325 g","I79":"Listeria spp.","992":"Lt.","A58":"LUX","I65":"mcg/100g","I66":"mcgRE/g","744":"meq de Ácido/Kg","741":"meq Peróxido /Kg Aceite","934":"meq/Kg","J13":"meq/L","562":"meqPeroxido / Kg Grasa","939":"mg","960":"mg / 100 ml AA","888":"mg Ac Ascorbico/100g","J10":"mg Ac Ascorbico/100ml","I78":"mg Ácido sórbico","I68":"mg C3H6O3","886":"mg CaCO3/L","947":"mg Imidacloprid/Kg","772":"mg Nitrógeno Amoniacal/100g","887":"mg Nitrógeno Amoniacal/L","J16":"mg/100 gr (Ac. Sulfúrico)","963":"mg/100 mL","867":"mg/100g","I74":"mg/100ml","I88":"mg/200mL","I75":"mg/240ml","J15":"mg/dm2","625":"mg/Kg","618":"mg/L","619":"mgCaO3/L","J57":"miliequivalentes/Kg","972":"min. 10^7","774":"ml","A41":"ml/L","967":"mm","995":"mmHg","435":"N° / 100 Ml","785":"NBV/100g","J14":"ng/g","J11":"NMP / 100 g","054":"NMP / 100 ml","052":"NMP / g","053":"NMP / ml","620":"NTU","623":"NUO","624":"NUS","919":"ºBaumé","738":"ºBe","566":"ºBrix","840":"ºC","966":"ºHaugh","962":"Org / 100 mL","609":"Org/L","996":"ºZ","740":"pH (10ºBrix)","739":"pH Solución 50%","632":"Positivo/Negativo","839":"ppb","564":"ppm","E78":"ppm de Oleato de Sodio","848":"Presencia/Ausencia","969":"Pseudomonas aeruginosa / 1 g","I70":"Pseudomonas aeruginosa/ 1g","987":"Pseudomonas/100ml","I69":"Pulg-Hg","838":"S. aureus / superficie","060":"Salmonella / 100Cm2","A49":"Salmonella / 25 ml","058":"Salmonella / 25g","998":"Salmonella / 4 superficies","868":"Salmonella / 50 g","959":"Salmonella / 750 g","059":"Salmonella / área","B24":"Salmonella / cm2","745":"Salmonella / manos","780":"Salmonella / superficie","A59":"Salmonella sp./ 25g","E72":"Salmonella/325 g","878":"Samonella / 100 ml","846":"Shigella/25 g","970":"Staphylococcus aureus / 1 g","753":"Staphylococcus aureus / 100 cm2","752":"Staphylococcus aureus / área","879":"Staphylococcus aureus / manos","933":"Staphylococcus aureus / superficie ","971":"Streptococcus / 10 g","622":"UCV escala Pt/Co","434":"UFC / 15 Min","997":"UFC / 4 superficies","056":"UFC / área","055":"UFC / Cm2","049":"UFC / g ","983":"UFC / g (Est.)","057":"UFC / manos","050":"UFC / ml","985":"UFC / ml (Est.)","750":"UFC / placa 40 cm2","758":"UFC / placa 60 cm2","779":"UFC / superficie","051":"UFC/ 100 ml","A47":"UFC/ 100g","978":"UFC/ ml 35ºC/48 h R2A","608":"UFC/15","616":"UFP/ml","927":"ug/100g","I76":"ug/100ml","I73":"ug/200ml","I89":"ug/200mL","I77":"ug/240ml","964":"ug/g","770":"ug/Kg","I71":"ug/L","A84":"ug/mL","I64":"ugRE/g","742":"UI","913":"UI/100g","I90":"UI/200mL","621":"umho/cm","J56":"unidades kertesz","986":"V. cholerae/manos","942":"Vibrio cholerae / 100 cm2","617":"Vibrio cholerae / 25 g","909":"Vibrio parahaemolyticus / 25 g","M50":"msv/año","M49":"Bq/L"}'], /**/
                [3,'condi_espe', '{"Ausencia": "Ausencia", "=": "=", "<": "<", ">": ">", "<=": "<=", ">=": ">="}'],
                [4,'valor_espe'],[5,'valexpo_espe'],
                [6,'condi_resul', '{"Ausencia": "Ausencia", "=": "=", "<": "<", ">": ">", "<=": "<=", ">=": ">="}'],
                [7,'valor_resul'],[8,'valexpo_resul'],
                [9,'sconclusion', '{"":"", "N": "NO CONFORME", "C": "CONFORME"}']
            ]
        },
        onSuccess: function(data, textStatus, jqXHR) {
            
            otblElementos.ajax.reload(null,false);

            /*var v_cinternoordenservicio = $('#mhdecinternoordenservicio').val();
            var v_cinternocotizacion = $('#mhdecinternocotizacion').val();
            var v_nordenproducto = $('#mhdenordenproducto').val();
            var v_cmuestra = $('#mhdecmuestra').val();
            var v_censayo = $('#mhdecensayo').val();
            var v_nviausado = $('#mhdenviausado').val();
            var v_zctipoensayo = $('#mhdezctipoensayo').val();
            
            listarElementos(v_cinternoordenservicio,v_cinternocotizacion,v_nordenproducto,v_cmuestra,v_censayo,v_nviausado,v_zctipoensayo);*/
        },
    });
});


regEsterilidad = function(cinternoordenservicio,cinternocotizacion,nordenproducto,cmuestra,censayo,nviausado,idaccion){        
    $('#frmRegEsterilidad').trigger("reset");

    $('#modalEsterilidad').modal({show:true});

    $('#mhdnscinternoordenservicio').val(cinternoordenservicio);
    $('#mhdnscinternocotizacion').val(cinternocotizacion);
    $('#mhdnsnordenproducto').val(nordenproducto);
    $('#mhdnscmuestra').val(cmuestra);
    $('#mhdnscensayo').val(censayo);
    $('#mhdnsnviausado').val(nviausado);
          
    if(censayo == '785' || censayo == '1661'){
        $('#divtblesterilB').show();           
        $('#divtblesterilA').hide();
        $('#mcbotipoacidez').val('B');
        $('#mtxttipoacidez').val('Baja acidez (pH>4.6)');  
    }   
    if(censayo == '744' || censayo == '106'){
        $('#divtblesterilB').hide();           
        $('#divtblesterilA').show(); 
        $('#mcbotipoacidez').val('A');
        $('#mtxttipoacidez').val('Alta acidez (pH<4.6)'); 
    }

    if(idaccion == 0){
        $('#mhdnsaccionEsteril').val('N');
    }else{
        $('#mhdnsaccionEsteril').val('A');
        Selectesteril(cinternoordenservicio,cinternocotizacion,nordenproducto,cmuestra,censayo,nviausado);
    }   
    
};

Selectesteril = function (cinternoordenservicio, cinternocotizacion, nordenproducto, cmuestra, censayo, nviausado) {        
    var parametros = {
        "cinternoordenservicio" : cinternoordenservicio,
        "cinternocotizacion" : cinternocotizacion,
        "nordenproducto" : nordenproducto,
        "cmuestra" : cmuestra,
        "censayo" : censayo,
        "nviausado" : nviausado,
    };
    $.ajax({
       url      :  baseurl+"lab/resultados/cregresult/getlistaesteril",
       method   : "POST",
       data     : parametros, 
    }).done(function(respuesta){
        var posts = JSON.parse(respuesta);
        
        $.each(posts, function() { 
            $('#mtxtdpreincubacion').val(this.DPREINCUBACION);
            $('#mtxtph').val(this.DPH);
            $('#mcbodbames35cp').val(this.DBAMES35CP).trigger("change");
            $('#mcbodbames35cc').val(this.DBAMES35CC).trigger("change");
            $('#mcbodbater55cp').val(this.DBATER55CP).trigger("change");
            $('#mcbodbater55cc').val(this.DBATER55CC).trigger("change");
            $('#mcbodacmes30ca').val(this.DACMES30CA).trigger("change");
            $('#mcbodacmes30cm').val(this.DACMES30CM).trigger("change");
            $('#mcbodacter55ca').val(this.DACTER55CA).trigger("change");
            $('#mcbosresultado').val(this.SRESULTADO).trigger("change");
            
        });
    })
};

checkVerinf = function(IDORDENSERVICIO,IDCOTIZACION,IDPRODUCTO,CMUESTRA,CENSAYO,ZCELEMENTO,SINFENSAYO){
    
    var params = { 
        "cinternoordenservicio" : IDORDENSERVICIO,
        "cinternocotizacion"    : IDCOTIZACION,
        "nordenproducto"        : IDPRODUCTO,
        "cmuestra"              : CMUESTRA,
        "censayo"               : CENSAYO,
        "zcelemento"            : ZCELEMENTO,
        "sinfensayo"            : SINFENSAYO,
    };
    
    var request = $.ajax({
        type: 'POST',
        url: baseurl+"lab/resultados/cregresult/updestadoelementos",
        async: true,
        data: params,
        error: function(){
            Vtitle = 'Error en Guardar!!!';
            Vtype = 'error';
            sweetalert(Vtitle,Vtype); 
        }
    });
    request.done(function( respuesta ) {
            
            var v_cinternoordenservicio = $('#mhdecinternoordenservicio').val();
            var v_cinternocotizacion = $('#mhdecinternocotizacion').val();
            var v_nordenproducto = $('#mhdenordenproducto').val();
            var v_cmuestra = $('#mhdecmuestra').val();
            var v_censayo = $('#mhdecensayo').val();
            var v_nviausado = $('#mhdenviausado').val();
            var v_zctipoensayo = $('#mhdezctipoensayo').val();  
            
            var v_conttipoprod = $('#mhdeconcc').val();      
            
            
        if (v_conttipoprod == 'S'){
            listarElementos(v_cinternoordenservicio,v_cinternocotizacion,v_nordenproducto,v_cmuestra,v_censayo,v_nviausado,v_zctipoensayo);
        }else{           
            listarElementos_old(v_cinternoordenservicio,v_cinternocotizacion,v_nordenproducto,v_cmuestra,v_censayo,v_nviausado,v_zctipoensayo);
        } 
            
            
    });
};

$('#btnaddNormas').click(function(){
    $('#modalAddnormas').modal({show:true});
    v_idot = $('#txtidordenservicio').val();
    $('#mhdnNormascordenservicio').val(v_idot);
              
    $('#divtblAddnormas').show();
    $('#divtblFindnormas').hide(); 
    
    listarAddnormas(v_idot)
});

listarAddnormas = function(cinternoordenservicio){
   
    var parametros = {
        "cinternoordenservicio" : cinternoordenservicio,
    };    

    otblAddnormas = $('#tblAddnormas').DataTable({ 
        "processing"  	: true,
        "bDestroy"    	: true,
        "stateSave"     : true,
        "bJQueryUI"     : true,
        "scrollResize"  : true,
        "scrollY"     	: "400px",
        "scrollX"     	: true, 
        'AutoWidth'     : false,
        "paging"      	: false,
        "info"        	: false,
        "filter"      	: true, 
        "ordering"		: false,
        "responsive"    : false,
        "select"        : true,
        "ajax"	: {
            "url"   : baseurl+"lab/resultados/cregresult/getlistaddnormas",
            "type"  : "POST", 
            "data"  : parametros,     
            dataSrc : ''      
        }, 
        'columns'	: [
            {data: 'cmuestra', "className":"col-xs"},
            {data: 'dregistro', "className":"col-sm"},
            {data: 'drealproducto', "className":"col-m"},
            {data: 'dnumeronorma', "className":"col-s"},
            {data: 'dnumerogrupo', "className":"col-s"},
            {data: 'blanco',"orderable": false, 
                render:function(data, type, row){
                    return '<div><a title="Asociar" style="cursor:pointer; color:navy;" onClick="asociarReg('+row.cinternoordenservicio+','+row.nordenproducto+',\''+row.cmuestra+'\',\''+row.zctipoinforme+'\',\''+row.zctipoensayo+'\','+row.ndorden+');"><span class="fas fa-search" aria-hidden="true"></span></a></div>'
                }
            },
            {data: 'blanco',"orderable": false, 
                render:function(data, type, row){
                    return '<div><a title="Agregar" style="cursor:pointer; color:green;" onClick="agregarReg('+row.cinternoordenservicio+','+row.nordenproducto+',\''+row.cmuestra+'\',\''+row.zctipoinforme+'\',\''+row.zctipoensayo+'\');"><span class="fas fa-plus-circle" aria-hidden="true"></span></a></div>'
                }
            },
            {data: 'blanco',"orderable": false, 
                render:function(data, type, row){
                    return '<div><a title="Eliminar" style="cursor:pointer; color:red;" onClick="eliminarReg('+row.cinternoordenservicio+','+row.nordenproducto+',\''+row.cmuestra+'\',\''+row.zctipoinforme+'\',\''+row.zctipoensayo+'\','+row.ndorden+');"><span class="far fa-window-close" aria-hidden="true"></span></a></div>'
                }
            },
        ]
    }); 
}
asociarReg = function(cinternoordenservicio,nordenproducto,cmuestra,zctipoinforme,zctipoensayo,ndorden){
    $('#frmaddnormas').trigger("reset");
    $('#hdnasocinternoordenservicio').val(cinternoordenservicio);
    $('#hdnasonordenproducto').val(nordenproducto);
    $('#hdnasocmuestra').val(cmuestra);
    $('#hdnasozctipoinforme').val(zctipoinforme);
    $('#hdnasozctipoensayo').val(zctipoensayo);
    $('#hdnasondorden').val(ndorden);
    
    $('#hdnasoaccion').val('N');

    listarnormas(zctipoensayo);
    $('#divtblAddnormas').hide();
    $('#divtblFindnormas').show();
    $('#mbtnCretornarnormas').show();
}

$('#mbtnCretornarnormas').click(function(){
    $('#divtblAddnormas').show();
    $('#divtblFindnormas').hide();
    $('#mbtnCretornarnormas').hide();
});

listarnormas = function(zctipoensayo){
   
    var parametros = {
        "zctipoensayo" : zctipoensayo,
    };    

    otblListnormas = $('#tblListnormas').DataTable({ 
        "processing"  	: true,
        "bDestroy"    	: true,
        "stateSave"     : true,
        "bJQueryUI"     : true,
        "scrollResize"  : true,
        "scrollY"     	: "400px",
        "scrollX"     	: true, 
        'AutoWidth'     : false,
        "paging"      	: false,
        "info"        	: false,
        "filter"      	: true, 
        "ordering"		: false,
        "responsive"    : false,
        "select"        : true,
        "ajax"	: {
            "url"   : baseurl+"lab/resultados/cregresult/getlistnormas",
            "type"  : "POST", 
            "data"  : parametros,     
            dataSrc : ''      
        }, 
        'columns'	: [
            {data: 'dnormalab'},
            {data: 'dnumerogrupo'},
            {data: 'dnombregrupo', "className":"col-l"},
            {data: 'blanco', "className":"col-xs","orderable": false, 
                render:function(data, type, row){
                    return '<div><a title="Seleccionar" style="cursor:pointer; color:green;" onClick="asociarNorma(\''+row.cnormalab+'\',\''+row.cgruponormalab+'\');"><span class="fas fa-check-double" aria-hidden="true"></span></a></div>'
                }
            }
        ],
        rowGroup: {
            startRender : function ( rows, group, level ) {                
                if ( level == 0 ) {
                    var varcodnorma = rows
                        .data()
                        .reduce( function (a, b) {
                            return b.cnormalab;
                        }, 0) ; 

                    var collapsed = !!collapsedGroupsEq2[varcodnorma];
        
                    rows.nodes().each(function (r) {
                        r.style.display = collapsed ? 'none' : '';
                    });                 
                    
                    return $('<tr/>')
                    .append('<td colspan="3" style="background-color:#D5D8DC;">' + group + '</td>')
                    .attr('data-name', varcodnorma)
                    .toggleClass('collapsed', collapsed);

                }
            },
            dataSrc: ["dnormalab"],
        }, 
    }); 
    otblListnormas.column(0).visible(false); 
}
asociarNorma = function(var_cnormalab,var_cgruponormalab){
 
    var_cinternoordenservicio = $('#hdnasocinternoordenservicio').val();
    var_nordenproducto = $('#hdnasonordenproducto').val();
    var_cmuestra = $('#hdnasocmuestra').val();
    var_zctipoinforme = $('#hdnasozctipoinforme').val();
    var_zctipoensayo = $('#hdnasozctipoensayo').val();
    var_ndorden = $('#hdnasondorden').val();
    var_accion = $('#hdnasoaccion').val();
    
    Swal.fire({
        title: 'Asociar Item',
        text: "¿Está seguro de asociar esta norma?",
        icon: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Agregar!'
    }).then((result) => {
        if (result.value) {
            $.post(baseurl+"lab/resultados/cregresult/setasocianorma/", 
            {
                cinternoordenservicio   : var_cinternoordenservicio,
                nordenproducto   : var_nordenproducto,
                cmuestra   : var_cmuestra,
                zctipoinforme   : var_zctipoinforme,
                zctipoensayo : var_zctipoensayo,
                cnormalab : var_cnormalab,
                cgruponormalab : var_cgruponormalab,
                ndorden :var_ndorden,
                accion : var_accion,
            },      
            function(data){                    
                Vtitle = 'Se Adiciono Correctamente';
                Vtype = 'success';
                sweetalert(Vtitle,Vtype);
                otblAddnormas.ajax.reload(null,false);  
                $('#mbtnCretornarnormas').click();      
            });
        }
    }) 

};
agregarReg = function(cinternoordenservicio,nordenproducto,cmuestra,zctipoinforme,zctipoensayo){
    $('#frmaddnormas').trigger("reset");
    $('#hdnasocinternoordenservicio').val(cinternoordenservicio);
    $('#hdnasonordenproducto').val(nordenproducto);
    $('#hdnasocmuestra').val(cmuestra);
    $('#hdnasozctipoinforme').val(zctipoinforme);
    $('#hdnasozctipoensayo').val(zctipoensayo);
    $('#hdnasoaccion').val('A');

    listarnormas(zctipoensayo);
    $('#divtblAddnormas').hide();
    $('#divtblFindnormas').show();
    $('#mbtnCretornarnormas').show();
      
}
eliminarReg = function(cinternoordenservicio,nordenproducto,cmuestra,zctipoinforme,zctipoensayo,ndorden){
    var params = { 
        "cinternoordenservicio"    : cinternoordenservicio,
        "nordenproducto"        : nordenproducto,
        "cmuestra"  : cmuestra,
        "zctipoinforme"         : zctipoinforme,
        "zctipoensayo"         : zctipoensayo,
        "ndorden"         : ndorden,
    };
    
    Swal.fire({
        title: 'Confirmar Eliminación',
        text: "¿Está seguro de eliminar la Norma asociada?",
        type: 'error',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, bórralo!'
    }).then((result) => {
        if (result.value) {
            var request = $.ajax({
                type: 'POST',
                url: baseurl+"lab/resultados/cregresult/deleteasocianorma",
                async: true,
                data: params,
                error: function(){
                    Vtitle = 'Error en Eliminar!!!';
                    Vtype = 'error';
                    sweetalert(Vtitle,Vtype); 
                }
            });
            request.done(function( respuesta ) {
                    otblAddnormas.ajax.reload(null,false); 
                    Vtitle = 'Se Elimino Correctamente';
                    Vtype = 'success';
                    sweetalert(Vtitle,Vtype);
            });
        }
    })
}

$('#btnObservamuestra').click(function(){
    $('#modalObservamuestras').modal({show:true});
    v_idot = $('#txtidordenservicio').val();
    
    listarobsmuestra(v_idot)
});
listarobsmuestra = function(cinternoordenservicio){
   
    var parametros = {
        "cinternoordenservicio" : cinternoordenservicio,
    };    

    otblObsmuestra = $('#tblObsmuestra').DataTable({ 
        "processing"  	: true,
        "bDestroy"    	: true,
        "stateSave"     : true,
        "bJQueryUI"     : true,
        "scrollResize"  : true,
        "scrollY"     	: "300px",
        "scrollX"     	: true, 
        'AutoWidth'     : false,
        "paging"      	: false,
        "info"        	: false,
        "filter"      	: false, 
        "ordering"		: false,
        "responsive"    : false,
        "select"        : false,
        "ajax"	: {
            "url"   : baseurl+"lab/resultados/cregresult/getlistobsmuestra",
            "type"  : "POST", 
            "data"  : parametros,     
            dataSrc : ''      
        }, 
        'columns'	: [
            {data: 'CMUESTRA', "className":"col-xs"},
            {data: 'DOBSERVMUESTRA', "className":"col-sm"},
            {data: 'ID', "className":"ocultar col-xxxs"},
        ]
    }); 
    otblObsmuestra.column(0).visible(true);  
}
$('#tblObsmuestra').on('draw.dt', function(){  
    $('#tblObsmuestra').Tabledit({
        url:'lab/resultados/cregresult/setobsmuestra',
        dataType:'json',
        eventType: 'click',
        editButton: false,
        deleteButton: false,
        saveButton: false,
        autoFocus: false,
        hideIdentifier: true,
		restoreButton: false,
        buttons: {
            edit: {
                class: 'btn btn-sm btn-primary',
                html: '<span class="fas fa-pencil-alt"></span>',
                action: 'edit'
            }
        },
        columns:{
            identifier : [2,'ID'],
            editable:[[1,'DOBSERVMUESTRA']]
        },
        onSuccess: function(data, textStatus, jqXHR) {            
            var v_cinternoordenservicio = $('#txtidordenservicio').val();
            otblObsmuestra.ajax.reload(null,false);            
            //listarobsmuestra(v_cinternoordenservicio);
        },
    });
});


$('#btnLCmuestra').click(function(){
    $('#modalLCmuestras').modal({show:true});
    v_idot = $('#txtidordenservicio').val();
    
    listarLCmuestra(v_idot)
});
listarLCmuestra = function(cinternoordenservicio){
   
    var parametros = {
        "cinternoordenservicio" : cinternoordenservicio,
    };    

    otblLCmuestra = $('#tblLCmuestra').DataTable({ 
        "processing"  	: true,
        "bDestroy"    	: true,
        "stateSave"     : true,
        "bJQueryUI"     : true,
        "scrollResize"  : true,
        "scrollY"     	: "300px",
        "scrollX"     	: true, 
        'AutoWidth'     : false,
        "paging"      	: false,
        "info"        	: false,
        "filter"      	: false, 
        "ordering"		: false,
        "responsive"    : false,
        "select"        : false,
        "ajax"	: {
            "url"   : baseurl+"lab/resultados/cregresult/getlistLCmuestra",
            "type"  : "POST", 
            "data"  : parametros,     
            dataSrc : ''      
        }, 
        'columns'	: [
            {data: 'CMUESTRA', "className":"col-xs"},
            {data: 'DLCLAB', "className":"col-sm"},
            {data: 'ID', "className":"ocultar col-xxxs"},
        ]
    }); 
    otblLCmuestra.column(0).visible(true);  
}
$('#tblLCmuestra').on('draw.dt', function(){  
    $('#tblLCmuestra').Tabledit({
        url:'lab/resultados/cregresult/setLCmuestra',
        dataType:'json',
        eventType: 'click',
        editButton: false,
        deleteButton: false,
        saveButton: false,
        autoFocus: false,
        hideIdentifier: true,
		restoreButton: false,
        buttons: {
            edit: {
                class: 'btn btn-sm btn-primary',
                html: '<span class="fas fa-pencil-alt"></span>',
                action: 'edit'
            }
        },
        columns:{
            identifier : [2,'ID'],
            editable:[[1,'DLCLAB']]
        },
        onSuccess: function(data, textStatus, jqXHR) {            
            var v_cinternoordenservicio = $('#txtidordenservicio').val();
            otblLCmuestra.ajax.reload(null,false);            
            //listarobsmuestra(v_cinternoordenservicio);
        },
    });
});

$('#btnRetornarLista').click(function(){
    $('#tablab a[href="#tablab-list"]').tab('show');  
});
