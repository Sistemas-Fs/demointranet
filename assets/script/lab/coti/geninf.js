const objListaresult = {};
const objListaresultold = {};
var collapsedGroupsEq = {},collapsedGroupsEq1 = {};
var otblListconsinf, otblListinforme, otblListensayoperso, otblListResultados, otblListResultadosold
var tipotramite = 'I';

$(document).ready(function() {
    
    $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"lab/coti/ccotizacion/getcboclieserv",
        dataType: "JSON",
        async: true,
        success:function(result){
            $('#cboclieserv').html(result);
        },
        error: function(){
            alert('Error, No se puede autenticar por error');
        }
    });
});

function pulsarListarCoti(e) {
    if (e.keyCode === 13 && !e.shiftKey) {
        e.preventDefault();
        
        listarBusqueda();
    }
}  

$("#btnBuscar").click(function (){
    listarBusqueda();
});

listarBusqueda = function(){
    otblListconsinf = $('#tblListgeninf').DataTable({  
        'responsive'    : false,
        'bJQueryUI'     : true,
        'scrollY'     	: '600px',
        'scrollX'     	: true, 
        'paging'      	: true,
        'processing'  	: true,     
        'bDestroy'    	: true,
        'AutoWidth'     : false,
        'info'        	: true,
        'filter'      	: true, 
        'ordering'		: false,  
        'stateSave'     : true,
        'select'        : true,
        'ajax'	: {
            "url"   : baseurl+"lab/coti/cgeninf/generarformatos/",
            "type"  : "POST", 
            "data": function ( d ) {
                d.ccliente          = $('#cboclieserv').val(); 
                d.descripcion       = $('#txtbuscar').val();  
                d.tipobuscar        = $('#cbotipobuscar').val(); 
            },     
            dataSrc : ''        
        },
        'columns'	: [
            {data : null, "className" : "details-control col-xxs"},            
            {data : null, "className": "col-s"},
            {data: 'DCLIENTE', "className" : "col-lm"},
            {"orderable": false,
              render:function(data, type, row){  
                if(row.SINF == 0){ 
                  return ' <div class="btn-group">'+
                    ' &nbsp; &nbsp;'+
                    ' <button type="button" class="btn btn-default">'+row.TIPOINF+'</button>'+
                    ' <button type="button" class="btn btn-default dropdown-toggle dropdown-icon" data-toggle="dropdown">'+
                    ' <span class="sr-only">Toggle Dropdown</span>'+
                    ' <div class="dropdown-menu" role="menu">'+
                    ' <a class="dropdown-item" onClick="cambiarTipoInf(\''+row.cinternocotizacion+'\',\''+'067'+'\');">&nbsp;&nbsp;Muestra</a>'+
                    ' <a class="dropdown-item" onClick="cambiarTipoInf(\''+row.cinternocotizacion+'\',\''+'K50'+'\');">&nbsp;&nbsp;Personalizado</a>'+
                    ' </div>'+
                    ' </button>'+ 
                 '</div>'
                }else{
                    return '<div></div>'  
                }  
              }
            },         
            {"orderable": false,   
                render:function(data, type, row){ 
                    return '<div>'+
                    '    <p><a title="Lista Resultados" style="cursor:pointer;" onClick="verListresultado(\'' + row.cinternoordenservicio + '\',\'' + row.concc + '\');"><span class="fas fa-receipt" style="color:#448252;" aria-hidden="true">&nbsp;&nbsp;</span>'+row.selectTotal+'</a><p>' +
                    '<div>';
                }
            },
            {data : null, "className": "col-xxs"},
            {data: 'dcotizacion'},
            {data: 'nordenservicio'},
        ],
        "columnDefs": [
            {
                "targets": [0], 
                "data": null, 
                "createdCell": function (td, cellData, rowData, row, col) {
                        if (rowData.VERDET == 0) {
                            $(td).removeClass( 'details-control' );
                        }
                }
            },
            {
                "targets": [1],
                "data": null,  
                "render": function(data, type, row) {
                    var var_todos = 'T';
                    var verinf = '';
                    var vercerti = '';
                    var varlabinf = '';
                    if(row.SINF == 0){
                        verinf = '<a title="Generar Informe" style="cursor:pointer;" onclick="genInforme(\'' + row.cinternocotizacion + '\',\'' + row.cinternoordenservicio + '\',\'' + row.zctipoinforme + '\',\'' + row.concc + '\',\'' + row.TIPOCERTI + '\');"  class="pull-left"><i class="fas fa-file-contract fa-2x" style="color:blue;"></i></a><small style="color:black;">INF.</small>';
                    }else{
                        verinf = '<a title="Informe" style="cursor:pointer;" onclick="pdfInfensayo(\'' + row.cinternoordenservicio + '\',\'' + row.zctipoinforme + '\',\'' + var_todos + '\',\'' + var_todos + '\');"  class="pull-left"><i class="fas fa-file-pdf fa-2x" style="color:#FF0000;"></i></a><small style="color:black;">INF.</small>';
                    };
                    if(row.SCERTI == 0){
                        vercerti = '<a title="Generar C.C." style="cursor:pointer;" onclick="genCertificado(\'' + row.cinternocotizacion + '\',\'' + row.cinternoordenservicio + '\',\'' + row.TIPOCERTI + '\');" class="pull-left">&nbsp;&nbsp;<i class="fas fa-file-contract fa-2x" style="color:blue;"></i></a><small style="color:black;">CC.</small>';
                    }else{
                        vercerti = '<a title="Certificado" style="cursor:pointer;" onclick="pdfCerticalidad(\'' + row.cinternoordenservicio + '\',\'' + row.zctipoinforme + '\',\'' + var_todos + '\',\'' + varlabinf + '\',\'' + row.TIPOCERTI + '\');" class="pull-left">&nbsp;&nbsp;<i class="fas fa-file-pdf fa-2x" style="color:#FF0000;"></i></a><small style="color:black;">CC.</small>';
                    };
                    if(row.concc == 'S'){
                        return '<div><p>'+verinf+vercerti+'</p></div>';
                    }else{
                        return '<div><p>'+verinf+'</p></div>';
                    };
                }
            },
            {
                "targets": [5], 
                "data": null, 
                "render": function(data, type, row) {
                    var verpruebainf = '';
                    var verpruebacerti = '';
                    var verdetalle = '';
                    if(row.SINF == 0){
                        verpruebainf = '<a title="Borrador Informe" style="cursor:pointer;" onclick="pdfInfensayoBorrador(\'' + row.cinternoordenservicio + '\',\'' + row.zctipoinforme + '\');"  class="pull-left"><i class="fas fa-file-pdf fa-2x" style="color:#FF0000;"></i></a><small style="color:black;">INF.</small>';
                    };
                    if(row.SCERTI == 0){
                        verpruebacerti = '<a title="Borrador C.C." style="cursor:pointer;" onclick="pdfCerticalidadPrevia(\'' + row.cinternoordenservicio + '\',\'' + row.zctipoinforme + '\');"  class="pull-left">&nbsp;&nbsp;<i class="fas fa-file-pdf fa-2x" style="color:#FF0000;"></i></a><small style="color:black;">CC.</small>';
                    };
                    if(row.SINF == 0){
                        verdetalle = '<a title="Por Muestras" style="cursor:pointer;" onclick="borradorIndvidual(\'' + row.cinternoordenservicio + '\');"  class="pull-left">Por muestras&nbsp;&nbsp;<i class="fas fa-table"></i></a>';
                    };
                    if(row.concc == 'S'){
                        return '<div><p>'+verpruebainf+verpruebacerti+'</p>'+verdetalle+'</div>';
                    }else{
                        return '<div><p>'+verpruebainf+'</p>'+verdetalle+'</div>';
                    };
                }
            },
            {
                "targets": [6], 
                "data": null, 
                "render": function(data, type, row) {
                    return '<div>'+
                    '    <p><a title="Cotizacion" style="cursor:pointer;" onclick="pdfCoti(\'' + row.cinternocotizacion + '\');"  class="pull-left">'+row.dcotizacion+'&nbsp;&nbsp;<i class="fas fa-file-pdf" style="color:#FF0000;"></i></a>&nbsp;&nbsp;<br><small style="color:black;">('+row.fcotizacion+')</small><p>' +
                    '</div>';
                }
            },
            {
                "targets": [7], 
                "data": null, 
                "render": function(data, type, row) {
                    return '<div>'+
                    '    <p><a title="OT" style="cursor:pointer;" onclick="pdfOT(\'' + row.cinternoordenservicio + '\');" class="pull-left">'+row.nordenservicio+'&nbsp;&nbsp;<i class="fas fa-file-pdf" style="color:#FF0000;"></i></a>&nbsp;&nbsp;<br><small style="color:black;">('+row.fordenservicio+')</small><p>' +
                    '</div>';
                }
            }
        ]
    });    
    // Enumeracion 
    otblListconsinf.on( 'order.dt search.dt', function () { 
        otblListconsinf.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i+1;
          } );
    }).draw();  
}; 
/* DETALLE TRAMITES */
$('#tblListgeninf tbody').on( 'click', 'td.details-control', function () {
            
    var tr = $(this).parents('tr');
    var row = otblListconsinf.row(tr);
    var rowData = row.data();
    
    if ( row.child.isShown() ) {                    
        row.child.hide();
        tr.removeClass( 'details' );
    }
    else {
        otblListconsinf.rows().every(function(){
            if(this.child.isShown()){
                this.child.hide();
                $(this.node()).removeClass('details');
            }
        })
        row.child( 
           '<table id="tblListinforme" class="display compact" style="width:100%;  background-color:#D3DADF; padding-top: -10px; border-bottom: 2px solid black;">'+
           '<thead style="background-color:#FFFFFF;"><tr><th>NRO INFORME</th><th>FECHA INFORME</th><th>NRO CERTIFICADO</th><th>FECHA CERTIFICADO</th></thead><tbody>' +
            '</tbody></table>').show();
            if(rowData.VERDET == 0){
                
            }else{
            otblListinforme = $('#tblListinforme').DataTable({
                "bJQueryUI"     : true,
                'bStateSave'    : true,
                'scrollY'       : false,
                'scrollX'       : true,
                'scrollCollapse': false,
                'bDestroy'    : true,
                'paging'      : false,
                'info'        : false,
                'filter'      : false,   
                'stateSave'   : true,
                'ajax'        : {
                    "url"   : baseurl+"lab/coti/cgeninf/detgenerarformatos/",
                    "type"  : "POST", 
                    "data": function ( d ) {
                        d.cinternoordenservicio = rowData.cinternoordenservicio;
                        d.zctipoinforme = rowData.zctipoinforme;
                    },     
                    dataSrc : ''        
                },
                'columns'     : [
                    {"orderable": false, data: 'dinfensayo'},
                    {"orderable": false, data: 'finfensayo'},
                    {"orderable": false, data: 'dnumeroinforme'},
                    {"orderable": false, data: 'fcertificado'},
                              
                ], 
                "columnDefs": [
                    {
                        "targets": [0], 
                        "data": null, 
                        "render": function(data, type, row) {
                            return '<div>'+
                            '    <p><a title="Informe" style="cursor:pointer;" onclick="pdfInfensayo(\'' + row.cinternoordenservicio + '\',\'' + row.tipoinforme + '\',\'' + row.cmuestra + '\',\'' + row.idlabinformes + '\');"  class="pull-left">'+row.dinfensayo+'&nbsp;&nbsp;<i class="fas fa-file-pdf" style="color:#FF0000;"></i></a><p>' +
                            '</div>';
                        }
                    },
                    {
                        "targets": [2], 
                        "data": null, 
                        "render": function(data, type, row) {
                            var var_tipocer = '762';
                            if(row.dnumeroinforme == ''){
                                return '<div></div>'
                            }else{
                                return '<div>'+
                                '    <p><a title="Certificado" style="cursor:pointer;" onclick="pdfCerticalidad(\'' + row.cinternoordenservicio + '\',\'' + row.tipoinforme + '\',\'' + row.cmuestra + '\',\'' + row.idlabinformes + '\',\'' + var_tipocer + '\');" class="pull-left">'+row.dnumeroinforme+'&nbsp;&nbsp;<i class="fas fa-file-pdf" style="color:#FF0000;"></i></a><p>' +
                                '</div>';
                            }
                        }
                    }
                ],
            }); 
            }

        tr.addClass('details');
    }
});

verListresultado = function(cinternoordenservicio,concc){
    $('#modalResultados').modal({show:true});
    if (concc == 'S'){
        $('#divtblListResultados').show();           
        $('#divtblListResultadosold').hide();
        objListaresult.viewList(cinternoordenservicio,'%','%','%')
    }else{      
        $('#divtblListResultados').hide();           
        $('#divtblListResultadosold').show();
        objListaresultold.viewListold(cinternoordenservicio,'%','%','%')     
    } 
};

objListaresult.viewList = function (id, tipoensayo, acreditado, areaserv) {   
    var parametros = {
        "cinternoordenservicio" : id,
        "zctipoensayo" : tipoensayo,
        "sacnoac" : acreditado,
        "areaserv" : areaserv,
    };

    otblListResultados = $('#tblListResultados').DataTable({ 
        "processing"  	: true,
        "bDestroy"    	: true,
        "stateSave"     : true,
        "bJQueryUI"     : true,
        "scrollResize"  : true,
        "scrollY"     	: "540px",
        "scrollX"     	: true, 
        'AutoWidth'     : false,
        "paging"      	: false,
        "info"        	: false,
        "filter"      	: false, 
        "ordering"		: false,
        "responsive"    : false,
        "select"        : true,
        "ajax"	: {
            "url"   : baseurl+"lab/resultados/cregresult/getlistresultados",
            "type"  : "POST", 
            "data"  : parametros,     
            dataSrc : ''      
        }, 
        'columns'	: [
            {data: 'BLANCO', "className":"col-xxxs"},
            {data: 'codmuestra'},
            {"orderable": false, 
                render:function(data, type, row){
                    
                    if(row.zctipoensayo == "068" || row.zctipoensayo == "069" || row.zctipoensayo == "A61"){ // normal                         
                        return '<div>'+row.codensayo+'</div>';
                    } else if (row.zctipoensayo == "070"){ // sensorial
                        return '<div>'+row.codensayo+'&nbsp;&nbsp;&nbsp;<a title="Ingresar" style="cursor:pointer; color:navy;" onClick="regSensorial('+row.cinternoordenservicio+',\''+row.cinternocotizacion+'\','+row.nordenproducto+',\''+row.cmuestra+'\',\''+row.censayo+'\','+row.nviausado+',\''+row.concc+'\');"><span class="fas fa-folder-plus" aria-hidden="true"></span></a></div>'
                    } else if(row.zctipoensayo == "736"){ // esterilidad
                        return '<div>'+row.codensayo+'&nbsp;&nbsp;&nbsp;<a title="Ingresar" style="cursor:pointer; color:navy;" onClick="regEsterilidad('+row.cinternoordenservicio+',\''+row.cinternocotizacion+'\','+row.nordenproducto+',\''+row.cmuestra+'\',\''+row.censayo+'\','+row.nviausado+','+row.idaccion+');"><span class="fas fa-folder-plus" aria-hidden="true"></span></a></div>'
                    } else { // elementos
                        return '<div>'+row.codensayo+'&nbsp;&nbsp;&nbsp;<a title="Ingresar" style="cursor:pointer; color:navy;" onClick="regElementos('+row.cinternoordenservicio+',\''+row.cinternocotizacion+'\','+row.nordenproducto+',\''+row.cmuestra+'\',\''+row.censayo+'\','+row.nviausado+',\''+row.zctipoensayo+'\',\''+row.concc+'\');"><span class="fas fa-folder-plus" aria-hidden="true"></span></a></div>'
                    }
                }
            },
            {data: 'densayo'},
            {data: 'unidadmedida'},
            {data: 'condi_espe'},
            {data: 'valor_espe', render: $.fn.dataTable.render.number( ',', '.', 3) },
            {data: 'valexpo_espe', render: $.fn.dataTable.render.number( ',', '.', 0) },
            {data: 'condi_resul'},
            {data: 'valor_resul', render: $.fn.dataTable.render.number( ',', '.', 3) },
            {data: 'valexpo_resul', render: $.fn.dataTable.render.number( ',', '.', 0) },
            {data: 'dobservacion'},
            {data: 'sresultado'},
            {data: 'dselect'},
            {data: 'dvistobueno'},
        ],
        rowGroup: {
            startRender : function ( rows, group, level ) {                
                if ( level == 0 ) {
                    var varcodmuestra = rows
                        .data()
                        .reduce( function (a, b) {
                            return b.codmuestra;
                        }, 0) ; 

                    var collapsed = !!collapsedGroupsEq[varcodmuestra];
        
                    rows.nodes().each(function (r) {
                        r.style.display = collapsed ? 'none' : '';
                    });                 
                    
                    return $('<tr/>')
                    .append('<td colspan="13" style="background-color:#D5D8DC;">' + group + '</td>')
                    .attr('data-name', varcodmuestra)
                    .toggleClass('collapsed', collapsed);

                } else if ( level == 1 ){
                    var vartipoensayo = rows
                        .data()
                        .reduce( function (a, b) {
                            return b.tipoensayo;
                        }, 0) ;

                    var collapsedid = !!collapsedGroupsEq1[vartipoensayo];        
                    rows.nodes().each(function (r) {
                        r.style.display = collapsedid ? 'none' : '';
                    }); 

                    return $('<tr/>')
                    .append('<td colspan="13" style="background-color:#EEEBEB;">' + group + '</td>')
                    .attr('data-name', vartipoensayo)
                    .toggleClass('collapsed', collapsed);
                    
                } 
            },
            dataSrc: ["codmuestra","tipoensayo"],
        }, 
        "rowCallback":function(row,data){           
            if(data.sselect == "1"){
                $(row).css("background-color","#A7D1AE");
            }else{
                $(row).css("background-color","#FFFFFF"); 
            }
        },
    });
    // Enumeracion 
    otblListResultados.on( 'order.dt search.dt', function () { 
        otblListResultados.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i+1;
          } );
    }).draw();  

    otblListResultados.column(0).visible(true); 
    otblListResultados.column(1).visible(false); 
};
objListaresultold.viewListold = function (id, tipoensayo, acreditado, areaserv) { 
    var parametros = {
        "cinternoordenservicio" : id,
        "zctipoensayo" : tipoensayo,
        "sacnoac" : acreditado,
        "areaserv" : areaserv,
    };

    otblListResultadosold = $('#tblListResultadosold').DataTable({ 
        "processing"  	: true,
        "bDestroy"    	: true,
        "stateSave"     : true,
        "bJQueryUI"     : true,
        "scrollResize"  : true,
        "scrollY"     	: "540px",
        "scrollX"     	: true, 
        'AutoWidth'     : false,
        "paging"      	: false,
        "info"        	: false,
        "filter"      	: false, 
        "ordering"		: false,
        "responsive"    : false,
        "select"        : true,
        "ajax"	: {
            "url"   : baseurl+"lab/resultados/cregresult/getlistresultadosold",
            "type"  : "POST", 
            "data"  : parametros,     
            dataSrc : ''      
        }, 
        'columns'	: [
            {data: 'BLANCO', "className":"col-xxxs"},
            {data: 'codmuestra'},
            {"orderable": false, 
                render:function(data, type, row){
                    if(row.zctipoensayo == "068" || row.zctipoensayo == "069" || row.zctipoensayo == "A61"){ // normal
                        return '<div>'+row.codensayo+'</div>';
                    } else if (row.zctipoensayo == "070"){ // sensorial
                        return '<div>'+row.codensayo+'&nbsp;&nbsp;&nbsp;<a title="Ingresar" style="cursor:pointer; color:navy;" onClick="regSensorial('+row.cinternoordenservicio+',\''+row.cinternocotizacion+'\','+row.nordenproducto+',\''+row.cmuestra+'\',\''+row.censayo+'\','+row.nviausado+',\''+row.concc+'\');"><span class="fas fa-folder-plus" aria-hidden="true"></span></a></div>'
                    } else if(row.zctipoensayo == "736"){ // esterilidad
                        return '<div>'+row.codensayo+'&nbsp;&nbsp;&nbsp;<a title="Ingresar" style="cursor:pointer; color:navy;" onClick="regEsterilidad('+row.cinternoordenservicio+',\''+row.cinternocotizacion+'\','+row.nordenproducto+',\''+row.cmuestra+'\',\''+row.censayo+'\','+row.nviausado+','+row.idaccion+');"><span class="fas fa-folder-plus" aria-hidden="true"></span></a></div>'
                    } else { // elementos
                        return '<div>'+row.codensayo+'&nbsp;&nbsp;&nbsp;<a title="Ingresar" style="cursor:pointer; color:navy;" onClick="regElementos('+row.cinternoordenservicio+',\''+row.cinternocotizacion+'\','+row.nordenproducto+',\''+row.cmuestra+'\',\''+row.censayo+'\','+row.nviausado+',\''+row.zctipoensayo+'\',\''+row.concc+'\');"><span class="fas fa-folder-plus" aria-hidden="true"></span></a></div>'
                    }
                }
            },
            {data: 'densayo'},
            {data: 'unidadmedida'},
            {data: 'dresultado'},
            {data: 'dresultadoexp'},
            {data: 'dselect'},
            {data: 'dvistobueno'},
        ],
        rowGroup: {
            startRender : function ( rows, group, level ) {                
                if ( level == 0 ) {
                    var varcodmuestra = rows
                        .data()
                        .reduce( function (a, b) {
                            return b.codmuestra;
                        }, 0) ; 

                    var collapsed = !!collapsedGroupsEq[varcodmuestra];
        
                    rows.nodes().each(function (r) {
                        r.style.display = collapsed ? 'none' : '';
                    });                 
                    
                    return $('<tr/>')
                    .append('<td colspan="9" style="background-color:#D5D8DC;">' + group + '</td>')
                    .attr('data-name', varcodmuestra)
                    .toggleClass('collapsed', collapsed);

                } else if ( level == 1 ){
                    var vartipoensayo = rows
                        .data()
                        .reduce( function (a, b) {
                            return b.tipoensayo;
                        }, 0) ;

                    var collapsedid = !!collapsedGroupsEq1[vartipoensayo];        
                    rows.nodes().each(function (r) {
                        r.style.display = collapsedid ? 'none' : '';
                    }); 

                    return $('<tr/>')
                    .append('<td colspan="9" style="background-color:#EEEBEB;">' + group + '</td>')
                    .attr('data-name', vartipoensayo)
                    .toggleClass('collapsed', collapsed);
                    
                } 
            },
            dataSrc: ["codmuestra","tipoensayo"],
        }, 
        "rowCallback":function(row,data){           
            if(data.sselect == "1"){
                $(row).css("background-color","#A7D1AE");
            }else{
                $(row).css("background-color","#FFFFFF"); 
            }
        },
    });
    // Enumeracion 
    otblListResultadosold.on( 'order.dt search.dt', function () { 
        otblListResultadosold.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i+1;
          } );
    }).draw();  

    otblListResultadosold.column(0).visible(true); 
    otblListResultadosold.column(1).visible(false); 
};


cambiarTipoInf = function(idcoti, idtipo){
    var params = { "idcoti":idcoti,"idtipo":idtipo };
    $.ajax({
      type: 'ajax',
      method: 'post',
      url: baseurl+"lab/coti/cgeninf/cambiartipoinf",
      async: true,
      data: params,
      success:function(result)
      {
        otblListconsinf.ajax.reload(null,false); 
        Vtitle = 'Se Actualizo correctamente el Tipo de ensayo';
        Vtype = 'success';
        sweetalert(Vtitle,Vtype);  
      },
      error: function(){          
        otblListconsinf.ajax.reload(null,false); 
        Vtitle = 'No se puede actualizar el Tipo de ensayo';
        Vtype = 'error';
        sweetalert(Vtitle,Vtype); 
      }
    });
  
};

pdfInfensayoBorrador = function(var_idot,var_tipoinf){        
    var_idmuestra = 'T';

    window.open(baseurl+"lab/formatospdf/pdfinformes/pdfInfensayoBorrador/"+var_idot+"/"+var_idmuestra);
    
}
pdfCerticalidadPrevia = function(var_idot,var_tipoinf){    
    var_idmuestra = 'T';
   
    window.open(baseurl+"lab/formatospdf/pdfinformes/pdfCertificadoBorrador/"+var_idot+"/"+var_idmuestra);
    
}

pdfCoti = function(idcoti){
    var nversion = 0;
    window.open(baseurl+"lab/formatospdf/pdfcotizacion/pdfCoti/"+idcoti+"/"+nversion);
};
pdfOT = function(cinternoordenservicio){
    window.open(baseurl+"lab/formatospdf/pdfrecepcion/pdfOrderServ/"+cinternoordenservicio);
};

borradorIndvidual = function(cinternoordenservicio){
    $('#modalPormuestras').modal({show:true});
        
    $('#mhdnidordenserviciomuestra').val(cinternoordenservicio);
    listmuestrasborrador(cinternoordenservicio);
    /*var params = { "cinternoordenservicio":cinternoordenservicio};
    $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"lab/coti/cgeninf/getcbomuestrasborrador",
        dataType: "JSON",
        async: true,
        data: params,
        success:function(result){
            $('#cbomuestrasborrador').html(result);
        },
        error: function(){
            alert('Error, No se puede autenticar por error');
        }
    });*/
}


listmuestrasborrador = function(vcinternoordenservicio){
    otblListmuestrasborrador = $('#tblListmuestrasborrador').DataTable({         
        "processing"  	: true,
        "bDestroy"    	: true,
        "stateSave"     : true,
        "bJQueryUI"     : true,
        "scrollResize"  : true,
        "scrollY"     	: "400px",
        "scrollX"     	: true,
        "scrollCollapse": true, 
        'AutoWidth'     : true,
        "paging"      	: false,
        "info"        	: false,
        "filter"      	: false, 
        "ordering"		: false,
        "responsive"    : false,
        "select"        : false,  
        'ajax'	: {
            "url"   : baseurl+"lab/coti/cgeninf/getcbomuestrasborrador/",
            "type"  : "POST", 
            "data": function ( d ) {
                d.cinternoordenservicio    = vcinternoordenservicio;  
            },     
            dataSrc : ''        
        },
        'columns'	: [
            {data: 'cmuestra'},
            {data: 'SPACE'},
        ],
        "columnDefs": [{
            "targets": [1],
            "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).addClass('index select-checkbox');
            },
            "orderable": false
        }
        ],
        "select": {
            style:    'multi',  
            selector: 'td:nth-child(2)'      
        }
    });
}; 
$('#mbtnBorrador').click(function(){ 
    var table = $('#tblListmuestrasborrador').DataTable();
    var seleccionados = table.rows({ selected: true }).indexes();

    var array1 = table.cells(seleccionados, [0]).data().toArray();
    var array2 = table.cells(seleccionados, [1]).data().toArray();    
    var plainArray = [array1, array2];

    var lenArray = array1.length;
    
    var vcinternoordenservicio = $('#mhdnidordenserviciomuestra').val();

    for (var i = 0; i < lenArray; i++) {
        window.open(baseurl+"lab/formatospdf/pdfinformes/pdfInfensayoBorrador/"+vcinternoordenservicio+"/"+array1[i]);
     }

    //
    /*var form = getForm(baseurl+"lab/formatospdf/pdfrecepcion/pdfEtiquetagen", "_blank", plainArray, vcinternoordenservicio+ncopias, "post");

    document.body.appendChild(form);
    form.submit();
    form.parentNode.removeChild(form);*/
});

$("#cbomuestrasborrador").change(function(){ 
    var v_cmuestra = $('#cbomuestrasborrador').val();
    var v_cinternoordenservicio = $('#mhdnidordenserviciomuestra').val();
    var v_tipotramite = tipotramite;
    if (v_cmuestra != '0'){
        if(v_tipotramite == 'I'){
            window.open(baseurl+"lab/formatospdf/pdfinformes/pdfInfensayoBorrador/"+v_cinternoordenservicio+"/"+v_cmuestra);
        }else if(v_tipotramite == 'C'){
            window.open(baseurl+"lab/formatospdf/pdfinformes/pdfCertificadoBorrador/"+v_cinternoordenservicio+"/"+v_cmuestra);
        }
    }
})

$('input[type=radio][name=restado]').change(function() {    
    if($('#rdinf').prop('checked')){
        tipotramite = 'I';
    }else if ($('#rdcc').prop('checked')){
        tipotramite = 'C';
    } 
});

genInforme = function(cinternocotizacion,cinternoordenservicio,zctipoinforme,concc,zctipocertificado){
    if(zctipoinforme == 'K50'){
        $('#modalPersonalizar').modal({show:true});
        
        $('#mhdnidordenservicio').val(cinternoordenservicio);
        $('#mhdnidcotizacion').val(cinternocotizacion);
        $('#mhdnconcc').val(concc);
        
    }else{
        var parametros = { 
            "cinternocotizacion":cinternocotizacion,
            "cinternoordenservicio":cinternoordenservicio,
            "zctipoinforme":zctipoinforme,
        };
        var request = $.ajax({
            type: 'ajax',
            method: 'post',
            url: baseurl+"lab/coti/cgeninf/setgeninforme",
            dataType: "JSON",
            async: true,
            data: parametros,
            error: function(){
                alert('Error, no se puede cargar el servicio');
            }
        });      
        request.done(function( respuesta ) {            
            $.each(respuesta, function() { 
                if(concc == 'S'){
                    genCertificado(cinternocotizacion,cinternoordenservicio,zctipocertificado);
                }else{
                    listarBusqueda();
                    Vtitle = 'Se Genero Correctamente';
                    Vtype = 'success';
                    sweetalert(Vtitle,Vtype); 
                }      
            });
        });
    }
}
genCertificado = function(cinternocotizacion,cinternoordenservicio,zctipocertificado){    
    var parametros = { 
        "cinternocotizacion":cinternocotizacion,
        "cinternoordenservicio":cinternoordenservicio,
        "zctipocertificado":zctipocertificado,
    };
    var request = $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"lab/coti/cgeninf/setgencertificado",
        dataType: "JSON",
        async: true,
        data: parametros,
        error: function(){
            alert('Error, no se puede cargar el servicio');
        }
    });      
    request.done(function( respuesta ) {            
        $.each(respuesta, function() { 
            listarBusqueda();  
            Vtitle = 'Se Genero Correctamente';
            Vtype = 'success';
            sweetalert(Vtitle,Vtype);     
        });
    });

}

pdfInfensayo = function(cinternoordenservicio,tipoinforme,cmuestra,idlabinformes){    
    var_idot = cinternoordenservicio;
    var_tipoinf = tipoinforme;    
    var_idmuestra = cmuestra;   
    var_idlabinformes = idlabinformes;

    if(var_tipoinf == '067'){ // 
        window.open(baseurl+"lab/formatospdf/pdfinformes/pdfInfensayoMuestra/"+var_idot+"/"+var_idmuestra);
    }else if(var_tipoinf == 'K50'){ // 
        window.open(baseurl+"lab/formatospdf/pdfinformes/pdfInfensayoPersonalizado/"+var_idot+"/"+var_idmuestra+"/"+var_idlabinformes);
    }
    
}
pdfCerticalidad = function(cinternoordenservicio,tipoinforme,cmuestra,idlabinformes,zctipocertificado){    
    var_idot = cinternoordenservicio;
    var_tipoinforme = tipoinforme;    
    var_idmuestra = cmuestra;
    var_idlabinformes = idlabinformes;
    var_tipocerti = zctipocertificado;
   
    if(var_tipoinforme == '067'){ 
        if(var_tipocerti == '762'){
            window.open(baseurl+"lab/formatospdf/pdfinformes/pdfCertificadoIndiv/"+var_idot+"/"+var_idmuestra);
        }else if(var_tipocerti == 'A40'){
            window.open(baseurl+"lab/formatospdf/pdfinformes/pdfCertificadoUnSoloNro/"+var_idot);
        }        
    }else if(var_tipoinforme == 'K50'){ 
        window.open(baseurl+"lab/formatospdf/pdfinformes/pdfCertificadoPersonalizado/"+var_idot+"/"+var_idmuestra+"/"+var_idlabinformes);
    }
    
}


$('#modalPersonalizar').on('shown.bs.modal', function (e) {
    var cordenservicio = $('#mhdnidordenservicio').val(); 
    var params = { "cordenservicio":cordenservicio};
    $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"lab/coti/cgeninf/cbogenmuestras",
        dataType: "JSON",
        async: true,
        data: params,
        success:function(result){
            $('#cbogenmuestras').html(result);
        },
        error: function(){
            alert('Error, No se puede autenticar por error');
        }
    });
    var parametros = {
        "cinternoordenservicio"      : cordenservicio,
        "cmuestra"      : '0',
    };  
    getListarBusqueda(parametros);

    /**/
});

$("#cbogenmuestras").change(function(){ 
    var parametros = paramListarBusqueda();
    getListarBusqueda(parametros);
});

paramListarBusqueda = function (){         
    var parametros = {
        "cinternoordenservicio"      : $('#mhdnidordenservicio').val(),
        "cmuestra"      : $('#cbogenmuestras').val(),
    };  
    return parametros;    
};
getListarBusqueda = function(parametros){    

    otblListensayoperso = $('#tblListensayoperso').DataTable({ 
        "processing"  	: true,
        "bDestroy"    	: true,
        "stateSave"     : true,
        "bJQueryUI"     : true,
        "scrollResize"  : true,
        "scrollY"     	: "540px",
        "scrollX"     	: true, 
        'AutoWidth'     : true,
        "paging"      	: false,
        "info"        	: false,
        "filter"      	: true, 
        "ordering"		: false,
        "responsive"    : false,
        "select"        : true,
        "ajax"	: {
            "url"   : baseurl+"lab/coti/cgeninf/getlistensayosperso/",
            "type"  : "POST", 
            "data"  : parametros,     
            dataSrc : ''      
        }, 
        'columns'	: [
            {data: 'codmuestra'},
            {data: 'tipoensayo'},
            {data: 'BLANCO'},
            {data: 'codensayo'},
            {data: 'densayo'},
        ],
        "columnDefs": [
            {
                "targets": [2], 
                    "createdCell": function (td, cellData, rowData, row, col) {
                        if (rowData.IDLABINF == 0) {
                            $(td).addClass('index select-checkbox');
                        }else{
                            $(td).removeClass('select-checkbox');
                        }
                },
                "orderable": false
            }
        ],
        "select": {
            style:    'multi',  
            selector: 'td:nth-child(1)'      
        },
        rowGroup: {
            startRender : function ( rows, group, level ) {
                
                if ( level == 0 ) {
                    var varcodmuestra = rows
                        .data()
                        .reduce( function (a, b) {
                            return b.codmuestra;
                        }, 0) ; 

                    var collapsed = !!collapsedGroupsEq[varcodmuestra];
        
                    rows.nodes().each(function (r) {
                        r.style.display = collapsed ? 'none' : '';
                    });   
              
                    
                    return $('<tr/>')
                    .append('<td colspan="3" style="background-color:#D5D8DC;">' + group + '</td>')
                    .attr('data-name', varcodmuestra)
                    .toggleClass('collapsed', collapsed);

                } else if ( level == 1 ){
                    var vartipoensayo = rows
                        .data()
                        .reduce( function (a, b) {
                            return b.tipoensayo;
                        }, 0) ;

                    /*var collapsedid = !!collapsedGroupsEq1[vartipoensayo];        
                    rows.nodes().each(function (r) {
                        r.style.display = collapsedid ? '' : 'none';
                    }); */


                    return $('<tr/>')
                    .append('<td colspan="3" style=" background-color:#EEEBEB;"><h5>' + group + '</h5></td>')
                    .attr('data-name', vartipoensayo);
                    //.toggleClass('collapsed', collapsedid)
                    //.toggleClass('details', collapsedid);
                    
                }                 
            },
            dataSrc: ["codmuestra","tipoensayo"]
        },
    });     
    // Enumeracion 
    otblListensayoperso.on( 'order.dt search.dt', function () { 
        otblListensayoperso.column(2, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i+1;
          } );
    }).draw();
    otblListensayoperso.column(0).visible( false ); 
    otblListensayoperso.column(1).visible( false ); 
};

$('#mbtnGGenInfPersonalizado').click(function(){
    event.preventDefault();
    
    Swal.fire({
        title: 'Confirmar Generar los Seleccionados',
        text: "¿Está seguro de Generar Informe?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, generar!'
    }).then((result) => {
        if (result.value) {  
            var IDORDENSERVICIO = $('#mhdnidordenservicio').val();
            genidinf(IDORDENSERVICIO);
        }
    }) 
});
genidinf = function(IDORDENSERVICIO){  
    var parametros = { 
        "cinternoordenservicio":IDORDENSERVICIO,
    };
    var request = $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"lab/coti/cgeninf/setidinforme",
        dataType: "JSON",
        async: true,
        data: parametros,
        error: function(){
            alert('Error, No se puede autenticar por error = getresumeninspeccion');
        }
    });      
    request.done(function( respuesta ) {            
        $.each(respuesta, function() {
            $('#mhdnidinforme').val(this.IDINFORME); 
            updensayos(); 
        });
    });
}   
updensayos = function(){
    var table = $('#tblListensayoperso').DataTable();
            var seleccionados = table.rows({ selected: true });
                    
            seleccionados.every(function(key,data){
                var_cinternoordenservicio   = this.data().cinternoordenservicio;
                var_cinternocotizacion      = this.data().cinternocotizacion;
                var_nordenproducto          = this.data().nordenproducto;
                var_cmuestra                = this.data().cmuestra;
                var_censayo                 = this.data().censayo;
                var_nviausado               = this.data().nviausado;
                
                geninfoOK(var_cinternoordenservicio,var_cinternocotizacion,var_nordenproducto,var_cmuestra,var_censayo,var_nviausado); 
            });
            geninfoMensajeOK(var_cinternoordenservicio);
}
geninfoOK = function(IDORDENTRABAJO,IDCOTIZACION,IDPRODUCTO,CMUESTRA,CENSAYO,NVIAS){  
    var IDINFORMELAB = $('#mhdnidinforme').val();
    
    $.post(baseurl+"lab/coti/cgeninf/geninfoseleccionados", 
    {
        cinternoordenservicio   : IDORDENTRABAJO,
        cinternocotizacion      : IDCOTIZACION,
        nordenproducto          : IDPRODUCTO,
        cmuestra                : CMUESTRA,
        censayo                 : CENSAYO,
        nviausado               : NVIAS,
        idlabinformes           : IDINFORMELAB,
    });
}
geninfoMensajeOK = function(IDORDENTRABAJO){ 
    var idlabinformes = $('#mhdnidinforme').val();
    var cinternoordenservicio = IDORDENTRABAJO;
    var zctipoinforme = 'K50';
    var var_concc = $('#mhdnconcc').val();
    var parametros = { 
        "idlabinformes":idlabinformes,
        "cinternoordenservicio":cinternoordenservicio,
        "concc":var_concc,
    };
    var request = $.ajax({
        type: 'ajax',
        method: 'post',
        url: baseurl+"lab/coti/cgeninf/setgeninformepersonalizado",
        dataType: "JSON",
        async: true,
        data: parametros,
        error: function(){
            alert('Error, no se puede cargar el servicio');
        }
    });      
    request.done(function( respuesta ) {            
        $.each(respuesta, function() {
            
            var parametros = paramListarBusqueda(); 
            getListarBusqueda(parametros); 
            listarBusqueda();
            Vtitle = 'Se Genero Correctamente';
            Vtype = 'success';
            sweetalert(Vtitle,Vtype);                 
        });
    });
    
}
